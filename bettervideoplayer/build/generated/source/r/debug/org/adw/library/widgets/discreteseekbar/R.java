/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * gradle plugin from the resource data it found. It
 * should not be modified by hand.
 */
package org.adw.library.widgets.discreteseekbar;

public final class R {
    public static final class attr {
        public static int discreteSeekBarStyle = 0x7f040089;
        public static int dsb_allowTrackClickToDrag = 0x7f040093;
        public static int dsb_indicatorColor = 0x7f040094;
        public static int dsb_indicatorElevation = 0x7f040095;
        public static int dsb_indicatorFormatter = 0x7f040096;
        public static int dsb_indicatorPopupEnabled = 0x7f040097;
        public static int dsb_indicatorSeparation = 0x7f040098;
        public static int dsb_indicatorTextAppearance = 0x7f040099;
        public static int dsb_max = 0x7f04009a;
        public static int dsb_min = 0x7f04009b;
        public static int dsb_mirrorForRtl = 0x7f04009c;
        public static int dsb_progressColor = 0x7f04009d;
        public static int dsb_rippleColor = 0x7f04009e;
        public static int dsb_scrubberHeight = 0x7f04009f;
        public static int dsb_thumbSize = 0x7f0400a0;
        public static int dsb_trackColor = 0x7f0400a1;
        public static int dsb_trackHeight = 0x7f0400a2;
        public static int dsb_value = 0x7f0400a3;
    }
    public static final class color {
        public static int dsb_disabled_color = 0x7f060038;
        public static int dsb_progress_color = 0x7f060039;
        public static int dsb_progress_color_list = 0x7f06003a;
        public static int dsb_ripple_color_focused = 0x7f06003b;
        public static int dsb_ripple_color_list = 0x7f06003c;
        public static int dsb_ripple_color_pressed = 0x7f06003d;
        public static int dsb_track_color = 0x7f06003e;
        public static int dsb_track_color_list = 0x7f06003f;
    }
    public static final class style {
        public static int Widget_DiscreteIndicatorTextAppearance = 0x7f1601b6;
        public static int Widget_DiscreteSeekBar = 0x7f1601b7;
    }
    public static final class styleable {
        public static int[] DiscreteSeekBar = { 0x7f040093, 0x7f040094, 0x7f040095, 0x7f040096, 0x7f040097, 0x7f040098, 0x7f040099, 0x7f04009a, 0x7f04009b, 0x7f04009c, 0x7f04009d, 0x7f04009e, 0x7f04009f, 0x7f0400a0, 0x7f0400a1, 0x7f0400a2, 0x7f0400a3 };
        public static int DiscreteSeekBar_dsb_allowTrackClickToDrag = 0;
        public static int DiscreteSeekBar_dsb_indicatorColor = 1;
        public static int DiscreteSeekBar_dsb_indicatorElevation = 2;
        public static int DiscreteSeekBar_dsb_indicatorFormatter = 3;
        public static int DiscreteSeekBar_dsb_indicatorPopupEnabled = 4;
        public static int DiscreteSeekBar_dsb_indicatorSeparation = 5;
        public static int DiscreteSeekBar_dsb_indicatorTextAppearance = 6;
        public static int DiscreteSeekBar_dsb_max = 7;
        public static int DiscreteSeekBar_dsb_min = 8;
        public static int DiscreteSeekBar_dsb_mirrorForRtl = 9;
        public static int DiscreteSeekBar_dsb_progressColor = 10;
        public static int DiscreteSeekBar_dsb_rippleColor = 11;
        public static int DiscreteSeekBar_dsb_scrubberHeight = 12;
        public static int DiscreteSeekBar_dsb_thumbSize = 13;
        public static int DiscreteSeekBar_dsb_trackColor = 14;
        public static int DiscreteSeekBar_dsb_trackHeight = 15;
        public static int DiscreteSeekBar_dsb_value = 16;
        public static int[] Theme = { 0x7f040089 };
        public static int Theme_discreteSeekBarStyle = 0;
    }
}
