
package com.music.nyasaa.manager;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.PowerManager;
import android.util.Log;
import android.view.View;
import android.view.Window;


import com.music.nyasaa.R;
import com.music.nyasaa.adapter.GifImageView;
import com.music.nyasaa.controller.MyApplication;
import com.music.nyasaa.dbhandler.FavoritePlayTableHelper;
import com.music.nyasaa.dbhandler.MostAndRecentPlayTableHelper;
import com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity;
import com.music.nyasaa.models.SongDetail;
import com.music.nyasaa.phonemidea.PlayerUtility;
import com.music.nyasaa.utility.SharedPreferenceManager;
import com.music.nyasaa.utility.Utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import static com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity.FirstTime;
import static com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity.appcontectext;
import static com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity.context;
import static com.music.nyasaa.retrofit.Config.BASE_URL_FOR_PLAY;

public class MediaController implements NotificationManager.NotificationCenterDelegate, SensorEventListener {

    private boolean isPaused = true;

    private Dialog dialog;
    private Handler mHandler;
    private MediaPlayer audioPlayer = null;
    private AudioTrack audioTrackPlayer = null;
    private int lastProgress = 0;
    private boolean useFrontSpeaker;

    Handler mainHandler = new Handler(context.getMainLooper());

    private Thread thread;

    private SensorManager sensorManager;
    private Sensor proximitySensor;
    private boolean ignoreProximity;
    private PowerManager.WakeLock proximityWakeLock;

    private final Object playerSync = new Object();
    private final Object playerSongDetailSync = new Object();
    private boolean playMusicAgain = false;

    private int lastTag = 0;
    public int currentPlaylistNum;
    private boolean shuffleMusic;

    private final Object progressTimerSync = new Object();
    private Timer progressTimer = null;

    private final Object sync = new Object();
    private int ignoreFirstProgress = 0;
    private long lastPlayPcm;
    private long currentTotalPcmDuration;
    public Handler handler;

    public int type = 0;
    public int id = -1;
    public String path = "";
    private int repeatMode;

    private static volatile MediaController Instance = null;


    public static MediaController getInstance() {
        MediaController localInstance = Instance;
        if (localInstance == null) {
            synchronized (MediaController.class) {
                localInstance = Instance;
                if (localInstance == null) {
                    Instance = localInstance = new MediaController();
                }
            }
        }
        return localInstance;


    }

    @Override
    public void onSensorChanged(SensorEvent event) {

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public void didReceivedNotification(int id, Object... args) {

    }

    @Override
    public void newSongLoaded(Object... args) {

    }

    public int generateObserverTag() {
        return lastTag++;
    }

    public SongDetail getPlayingSongDetail() {
        return MusicPreferance.playingSongDetail;
    }

    public boolean isPlayingAudio(SongDetail messageObject) {
        return !(audioTrackPlayer == null && audioPlayer == null || messageObject == null || MusicPreferance.playingSongDetail == null || MusicPreferance.playingSongDetail != null);
    }

    public boolean isAudioPaused() {
        return isPaused;
    }


    public void playNextSong() {
        playNextSong(false);
    }

    public void playPreviousSong() {
        ArrayList<SongDetail> currentPlayList = shuffleMusic ? MusicPreferance.shuffledPlaylist : MusicPreferance.playlist;

        currentPlaylistNum--;
        if (currentPlaylistNum < 0) {
            currentPlaylistNum = currentPlayList.size() - 1;
        }
        if (currentPlaylistNum < 0 || currentPlaylistNum >= currentPlayList.size()) {
            return;
        }
        playMusicAgain = true;
        MusicPreferance.playingSongDetail.audioProgress = 0.0f;
        MusicPreferance.playingSongDetail.audioProgressSec = 0;
        playAudio(currentPlayList.get(currentPlaylistNum));
    }

    private void stopProgressTimer() {
        synchronized (progressTimerSync) {
            if (progressTimer != null) {
                try {
                    progressTimer.cancel();
                    progressTimer = null;
                } catch (Exception e) {
                    Log.e("tmessages", e.toString());
                }
            }
        }
    }

    private void stopProximitySensor() {
        if (ignoreProximity) {
            return;
        }
        try {
            useFrontSpeaker = false;
            NotificationManager.getInstance().postNotificationName(NotificationManager.audioRouteChanged, useFrontSpeaker);
            if (sensorManager != null && proximitySensor != null) {
                sensorManager.unregisterListener(this);
            }
            if (proximityWakeLock != null && proximityWakeLock.isHeld()) {
                proximityWakeLock.release();
            }
        } catch (Throwable e) {
            Log.e("tmessages", e.toString());
        }
    }



    public boolean playMUSICFORDOWNload(final SongDetail mSongDetail) {
        stopOtherSOngs();
        calc_stanicaDownload calc_stanica = new calc_stanicaDownload();
        calc_stanica.calc_stanica(mSongDetail);
        calc_stanica.execute();

        return true;

    }

    public boolean playMUSIC(final SongDetail mSongDetail) {
        stopOtherSOngs();
        calc_stanica calc_stanica = new calc_stanica();
        calc_stanica.calc_stanica(mSongDetail);
        calc_stanica.execute();
        if(FirstTime)
        {
            calc_stanica = new calc_stanica();
            calc_stanica.calc_stanica(mSongDetail);
            calc_stanica.execute();
            FirstTime=false;
        }
        return true;

    }

    public boolean playAudio(final SongDetail mSongDetail) {


//        demo(mSongDetail);

//        calc_stanica calc_stanica = new calc_stanica();
//        calc_stanica.calc_stanica(mSongDetail);
//        calc_stanica.execute();
//        context.runOnUiThread(new Runnable() {
//
//            @Override
//            public void run() {
//
//                // Stuff that updates the UI
//
//
//            }
//        });


        if (mSongDetail == null) {
            return false;
        }
        if ((audioTrackPlayer != null || audioPlayer != null) && MusicPreferance.playingSongDetail != null && mSongDetail.getId() == MusicPreferance.playingSongDetail.getId()) {
            if (isPaused) {
                resumeAudio(mSongDetail);
            }
            return true;
        }
        if (audioTrackPlayer != null) {
            MusicPlayerService.setIgnoreAudioFocus();
        }
        cleanupPlayer(!playMusicAgain, false);


        playMusicAgain = false;
        File file = null;

        try {


            audioPlayer = new MediaPlayer();
            audioPlayer.setAudioStreamType(useFrontSpeaker ? AudioManager.STREAM_VOICE_CALL : AudioManager.STREAM_MUSIC);


//            audioPlayer.setDataSource(mSongDetail.getPath());
            String mode = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_MUSIC_MODE, "none");
            String URL = "";

                URL = BASE_URL_FOR_PLAY + Uri.encode(mSongDetail.getFile());




            // audioPlayer.setDataSource(BASE_URL_FOR_PLAY+mSongDetail.getFile());

            audioPlayer.setDataSource(URL);

            audioPlayer.setOnInfoListener(new MediaPlayer.OnInfoListener() {
                @Override
                public boolean onInfo(MediaPlayer mp, int what, int extra) {

                    switch (what) {
                        case MediaPlayer.MEDIA_INFO_BUFFERING_START:
                            if (DashBoardActivity.progressBarUpper.getVisibility() == View.GONE)
                                DashBoardActivity.progressBarUpper.setVisibility(View.VISIBLE);
                            DashBoardActivity.progressBarMain.setVisibility(View.VISIBLE);
                            DashBoardActivity.btn_playpausePanel.setVisibility(View.GONE);
                            DashBoardActivity.btn_playpause.setVisibility(View.GONE);

                            break;
                        case MediaPlayer.MEDIA_INFO_BUFFERING_END:
                            if (DashBoardActivity.progressBarUpper.getVisibility() == View.VISIBLE)
                                DashBoardActivity.progressBarUpper.setVisibility(View.GONE);
                            DashBoardActivity.progressBarMain.setVisibility(View.GONE);
                            DashBoardActivity.btn_playpausePanel.setVisibility(View.VISIBLE);
                            DashBoardActivity.btn_playpause.setVisibility(View.VISIBLE);
                            break;
                    }
                    return false;

                }
            });
            audioPlayer.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
                @Override
                public void onBufferingUpdate(MediaPlayer mp, int percent) {

                }
            });
            audioPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    MusicPreferance.playingSongDetail.audioProgress = 0.0f;
                    MusicPreferance.playingSongDetail.audioProgressSec = 0;
                    if (!MusicPreferance.playlist.isEmpty() && MusicPreferance.playlist.size() > 1) {
                        playNextSong(true);
                    } else {
                        cleanupPlayer(true, true);
                    }
                }
            });

            audioPlayer.prepare();
            audioPlayer.start();
            startProgressTimer();


        } catch (Exception e) {
            if (audioPlayer != null) {
                audioPlayer.release();
                audioPlayer = null;
                isPaused = false;
                MusicPreferance.playingSongDetail = null;
            }
            return false;
        }
        isPaused = false;
        lastProgress = 0;
        MusicPreferance.playingSongDetail = mSongDetail;
        NotificationManager.getInstance().postNotificationName(NotificationManager.audioDidStarted, mSongDetail);

        if (audioPlayer != null) {
            try {
                if (MusicPreferance.playingSongDetail.audioProgress != 0) {
                    int seekTo = (int) (audioPlayer.getDuration() * MusicPreferance.playingSongDetail.audioProgress);
                    audioPlayer.seekTo(seekTo);
                }
            } catch (Exception e2) {
                MusicPreferance.playingSongDetail.audioProgress = 0;
                MusicPreferance.playingSongDetail.audioProgressSec = 0;
            }
        } else if (audioTrackPlayer != null) {
            if (MusicPreferance.playingSongDetail.audioProgress == 1) {
                MusicPreferance.playingSongDetail.audioProgress = 0;
            }

        }

//        if (MusicPreferance.playingSongDetail != null) {
//            Intent intent = new Intent(MyApplication.applicationContext, MusicPlayerService.class);
//            MyApplication.applicationContext.startService(intent);
//        } else {
//            Intent intent = new Intent(MyApplication.applicationContext, MusicPlayerService.class);
//            MyApplication.applicationContext.stopService(intent);
//        }
//
//        storeResendPlay(MyApplication.applicationContext, mSongDetail);
//        NotificationManager.getInstance().notifyNewSongLoaded(NotificationManager.newaudioloaded, mSongDetail);

        return true;


    }


    String decrypt(String outPaNameth) throws IOException, NoSuchAlgorithmException,
            NoSuchPaddingException, InvalidKeyException {

        File filei = new File(appcontectext.getFilesDir(), "tempfilenametoplay");

        String inPath = filei.getAbsolutePath();
        // deleteit

        File fileo = new File(appcontectext.getFilesDir(), outPaNameth);
//
        String outPath = fileo.getAbsolutePath();

        FileInputStream fis = new FileInputStream(outPath);
        FileOutputStream fos = new FileOutputStream(inPath);
        SecretKeySpec sks = new SecretKeySpec("ABCDEFdhdhdhghgfghfghgfg".getBytes(),
                "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, sks);
        CipherInputStream cis = new CipherInputStream(fis, cipher);
        int b;
        byte[] d = new byte[1024];
        while ((b = cis.read(d)) != -1) {
            fos.write(d, 0, b);
        }
        fos.flush();
        fos.close();
        cis.close();
        return inPath;
    }


    public boolean playAudioForDownload(SongDetail mSongDetail) {

        Utility.getSharedInstance().showProgressDialog(appcontectext);

        if (mSongDetail == null) {
            return false;
        }
        if ((audioTrackPlayer != null || audioPlayer != null) && MusicPreferance.playingSongDetail != null && mSongDetail.getId() == MusicPreferance.playingSongDetail.getId()) {
            if (isPaused) {
                resumeAudio(mSongDetail);
            }
            return true;
        }
        if (audioTrackPlayer != null) {
            MusicPlayerService.setIgnoreAudioFocus();
        }
        cleanupPlayer(!playMusicAgain, false);
        playMusicAgain = false;
        File file = null;

        try {
            audioPlayer = new MediaPlayer();
            audioPlayer.setAudioStreamType(useFrontSpeaker ? AudioManager.STREAM_VOICE_CALL : AudioManager.STREAM_MUSIC);

//            audioPlayer.setDataSource(mSongDetail.getPath());


            String url = decrypt(mSongDetail.getFile());
            // audioPlayer.setDataSource(BASE_URL_FOR_PLAY+mSongDetail.getFile());
            Utility.getSharedInstance().dismissProgressDialog();

            audioPlayer.setDataSource(url.replace(" ", "%20"));

            audioPlayer.setOnInfoListener(new MediaPlayer.OnInfoListener() {
                @Override
                public boolean onInfo(MediaPlayer mp, int what, int extra) {

                    switch (what) {
                        case MediaPlayer.MEDIA_INFO_BUFFERING_START:
                            if (DashBoardActivity.progressBarUpper.getVisibility() == View.GONE)
                                DashBoardActivity.progressBarUpper.setVisibility(View.VISIBLE);
                            DashBoardActivity.progressBarMain.setVisibility(View.VISIBLE);
                            DashBoardActivity.btn_playpausePanel.setVisibility(View.GONE);
                            DashBoardActivity.btn_playpause.setVisibility(View.GONE);

                            break;
                        case MediaPlayer.MEDIA_INFO_BUFFERING_END:
                            if (DashBoardActivity.progressBarUpper.getVisibility() == View.VISIBLE)
                                DashBoardActivity.progressBarUpper.setVisibility(View.GONE);
                            DashBoardActivity.progressBarMain.setVisibility(View.GONE);
                            DashBoardActivity.btn_playpausePanel.setVisibility(View.VISIBLE);
                            DashBoardActivity.btn_playpause.setVisibility(View.VISIBLE);
                            break;
                    }
                    return false;

                }
            });
            audioPlayer.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
                @Override
                public void onBufferingUpdate(MediaPlayer mp, int percent) {

                }
            });
            audioPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    MusicPreferance.playingSongDetail.audioProgress = 0.0f;
                    MusicPreferance.playingSongDetail.audioProgressSec = 0;
                    if (!MusicPreferance.playlist.isEmpty() && MusicPreferance.playlist.size() > 1) {
                        playNextSong(true);
                    } else {
                        cleanupPlayer(true, true);
                    }
                }
            });
            audioPlayer.prepare();
            audioPlayer.start();
            startProgressTimer();
        } catch (Exception e) {
            if (audioPlayer != null) {
                audioPlayer.release();
                audioPlayer = null;
                isPaused = false;
                MusicPreferance.playingSongDetail = null;
            }
            return false;
        }
        isPaused = false;
        lastProgress = 0;
        MusicPreferance.playingSongDetail = mSongDetail;
//        NotificationManager.getInstance().postNotificationName(NotificationManager.audioDidStarted, mSongDetail);

        if (audioPlayer != null) {
            try {
                if (MusicPreferance.playingSongDetail.audioProgress != 0) {
                    int seekTo = (int) (audioPlayer.getDuration() * MusicPreferance.playingSongDetail.audioProgress);
                    audioPlayer.seekTo(seekTo);
                }
            } catch (Exception e2) {
                MusicPreferance.playingSongDetail.audioProgress = 0;
                MusicPreferance.playingSongDetail.audioProgressSec = 0;
            }
        } else if (audioTrackPlayer != null) {
            if (MusicPreferance.playingSongDetail.audioProgress == 1) {
                MusicPreferance.playingSongDetail.audioProgress = 0;
            }

        }
//
//        if (MusicPreferance.playingSongDetail != null) {
//            Intent intent = new Intent(MyApplication.applicationContext, MusicPlayerService.class);
//            MyApplication.applicationContext.startService(intent);
//        } else {
//            Intent intent = new Intent(MyApplication.applicationContext, MusicPlayerService.class);
//            MyApplication.applicationContext.stopService(intent);
//        }
//
//        storeResendPlay(MyApplication.applicationContext, mSongDetail);
//        NotificationManager.getInstance().notifyNewSongLoaded(NotificationManager.newaudioloaded, mSongDetail);

        return true;
    }

    private void playNextSong(boolean byStop) {
        ArrayList<SongDetail> currentPlayList = shuffleMusic ? MusicPreferance.shuffledPlaylist : MusicPreferance.playlist;

        if (byStop && repeatMode == 2) {
            cleanupPlayer(false, false);
            playAudio(currentPlayList.get(currentPlaylistNum));
            return;
        }
        currentPlaylistNum++;
        if (currentPlaylistNum >= currentPlayList.size()) {
            currentPlaylistNum = 0;
            if (byStop && repeatMode == 0) {
                stopProximitySensor();
                if (audioPlayer != null || audioTrackPlayer != null) {
                    if (audioPlayer != null) {
                        try {
                            audioPlayer.stop();
                        } catch (Exception e) {
                        }
                        try {
                            audioPlayer.release();
                            audioPlayer = null;
                        } catch (Exception e) {
                        }
                    } else if (audioTrackPlayer != null) {
                        synchronized (playerSongDetailSync) {
                            try {
                                audioTrackPlayer.pause();
                                audioTrackPlayer.flush();
                            } catch (Exception e) {
                            }
                            try {
                                audioTrackPlayer.release();
                                audioTrackPlayer = null;
                            } catch (Exception e) {
                            }
                        }
                    }
                    stopProgressTimer();
                    lastProgress = 0;
                    isPaused = true;
                    MusicPreferance.playingSongDetail.audioProgress = 0.0f;
                    MusicPreferance.playingSongDetail.audioProgressSec = 0;
                    NotificationManager.getInstance().postNotificationName(NotificationManager.audioPlayStateChanged, MusicPreferance.playingSongDetail.getId());
                }
                return;
            }
        }
        if (currentPlaylistNum < 0 || currentPlaylistNum >= currentPlayList.size()) {
            return;
        }
        playMusicAgain = true;
        MusicPreferance.playingSongDetail.audioProgress = 0.0f;
        MusicPreferance.playingSongDetail.audioProgressSec = 0;
        playAudio(currentPlayList.get(currentPlaylistNum));
    }

    public boolean pauseAudio(SongDetail messageObject) {
        stopProximitySensor();
        if (audioTrackPlayer == null && audioPlayer == null || messageObject == null || MusicPreferance.playingSongDetail == null || MusicPreferance.playingSongDetail != null
                && MusicPreferance.playingSongDetail.getId() != messageObject.getId()) {
            return false;
        }
        stopProgressTimer();
        try {
            if (audioPlayer != null) {
                audioPlayer.pause();
            } else if (audioTrackPlayer != null) {
                audioTrackPlayer.pause();
            }
            isPaused = true;
            NotificationManager.getInstance().postNotificationName(NotificationManager.audioPlayStateChanged, MusicPreferance.playingSongDetail.getId());
        } catch (Exception e) {
            Log.e("tmessages", e.toString());
            isPaused = true;
            return false;
        }
        return true;
    }


    public boolean resumeAudio(SongDetail messageObject) {
        if (audioTrackPlayer == null && audioPlayer == null || messageObject == null || MusicPreferance.playingSongDetail == null || MusicPreferance.playingSongDetail != null
                && MusicPreferance.playingSongDetail.getId() != messageObject.getId()) {
            return false;
        }
        try {
            startProgressTimer();
            if (audioPlayer != null) {
                audioPlayer.start();
            } else if (audioTrackPlayer != null) {
                audioTrackPlayer.play();
            }
            isPaused = false;
            NotificationManager.getInstance().postNotificationName(NotificationManager.audioPlayStateChanged, MusicPreferance.playingSongDetail.getId());
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public void stopAudio() {

        stopProximitySensor();
        if (audioTrackPlayer == null && audioPlayer == null || MusicPreferance.playingSongDetail == null) {
            return;
        }
        try {
            if (audioPlayer != null) {
                audioPlayer.stop();
            } else if (audioTrackPlayer != null) {
                audioTrackPlayer.pause();
                audioTrackPlayer.flush();
            }
        } catch (Exception e) {
        }
        try {
            if (audioPlayer != null) {
                audioPlayer.release();
                audioPlayer = null;
            } else if (audioTrackPlayer != null) {
                synchronized (playerSongDetailSync) {
                    audioTrackPlayer.release();
                    audioTrackPlayer = null;
                }
            }
        } catch (Exception e) {
        }
        stopProgressTimer();
        isPaused = false;

        Intent intent = new Intent(MyApplication.applicationContext, MusicPlayerService.class);
        MyApplication.applicationContext.stopService(intent);
    }

    private void startProgressTimer() {
        synchronized (progressTimerSync) {
            if (progressTimer != null) {
                try {
                    progressTimer.cancel();
                    progressTimer = null;
                } catch (Exception e) {
                    // FileLog.e("tmessages", e);
                }
            }
            progressTimer = new Timer();
            progressTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    synchronized (sync) {
                        PlayerUtility.runOnUIThread(new Runnable() {
                            @Override
                            public void run() {
                                if (MusicPreferance.playingSongDetail != null && (audioPlayer != null || audioTrackPlayer != null) && !isPaused) {
                                    try {
                                        if (ignoreFirstProgress != 0) {
                                            ignoreFirstProgress--;
                                            return;
                                        }
                                        int progress;
                                        float value;
                                        if (audioPlayer != null) {
                                            progress = audioPlayer.getCurrentPosition();

                                            //  totalTime  = audioPlayer.getDuration();
                                            value = (float) lastProgress / (float) audioPlayer.getDuration();
                                            if (progress <= lastProgress) {
                                                return;
                                            }
                                        } else {
                                            progress = (int) (lastPlayPcm / 48.0f);
                                            value = (float) lastPlayPcm / (float) currentTotalPcmDuration;
                                            if (progress == lastProgress) {
                                                return;
                                            }
                                        }
                                        lastProgress = progress;
                                        MusicPreferance.playingSongDetail.duration= String.valueOf(audioPlayer.getDuration()/1000);
                                        MusicPreferance.playingSongDetail.audioProgress = value;
                                        MusicPreferance.playingSongDetail.audioProgressSec = lastProgress / 1000;
                                        NotificationManager.getInstance().postNotificationName(NotificationManager.audioProgressDidChanged,
                                                MusicPreferance.playingSongDetail.getId(), value);
                                    } catch (Exception e) {
                                    }
                                }
                            }
                        });
                    }
                }
            }, 0, 17);
        }
    }


    public boolean setPlaylist(ArrayList<SongDetail> allSongsList, SongDetail current, int type_, int id_) {
        type = type_;
        id = id_;

        if (MusicPreferance.playingSongDetail == current) {
            return playAudio(current);
        }
        playMusicAgain = !MusicPreferance.playlist.isEmpty();
        MusicPreferance.playlist.clear();
        if (allSongsList != null && allSongsList.size() >= 1) {
            MusicPreferance.playlist.addAll(allSongsList);
        }

        currentPlaylistNum = MusicPreferance.playlist.indexOf(current);
        if (currentPlaylistNum == -1) {
            MusicPreferance.playlist.clear();
            MusicPreferance.shuffledPlaylist.clear();
            return false;
        }
        if (shuffleMusic) {
            currentPlaylistNum = 0;
        }
        return playAudio(current);
    }

    public boolean seekToProgress(SongDetail mSongDetail, float progress) {
        if (audioTrackPlayer == null && audioPlayer == null) {
            return false;
        }
        try {
            if (audioPlayer != null) {
                int seekTo = (int) (audioPlayer.getDuration() * progress);
                audioPlayer.seekTo(seekTo);
                lastProgress = seekTo;
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public void cleanupPlayer(Context context, boolean notify, boolean stopService) {
        MusicPreferance.saveLastSong(context, getPlayingSongDetail());
        MusicPreferance.saveLastSongListType(context, type);
        MusicPreferance.saveLastAlbID(context, id);
        MusicPreferance.saveLastPosition(context, currentPlaylistNum);
        MusicPreferance.saveLastPath(context, path);
        cleanupPlayer(notify, stopService);
    }

    public void cleanupPlayer(boolean notify, boolean stopService) {

        pauseAudio(getPlayingSongDetail());
        stopProximitySensor();
        if (audioPlayer != null) {
            try {
                audioPlayer.reset();
            } catch (Exception e) {
            }
            try {
                audioPlayer.stop();
            } catch (Exception e) {
            }
            try {
                audioPlayer.release();
                audioPlayer = null;
            } catch (Exception e) {
            }
        } else if (audioTrackPlayer != null) {
            synchronized (playerSongDetailSync) {
                try {
                    audioTrackPlayer.pause();
                    audioTrackPlayer.flush();
                } catch (Exception e) {
                }
                try {
                    audioTrackPlayer.release();
                    audioTrackPlayer = null;
                } catch (Exception e) {
                }
            }
        }
        stopProgressTimer();
        isPaused = true;
        if (stopService) {
            Intent intent = new Intent(MyApplication.applicationContext, MusicPlayerService.class);
            MyApplication.applicationContext.stopService(intent);
        }

    }


    /**
     * Store Rcent Play Data
     */
    public synchronized void storeResendPlay(final Context context, final SongDetail mDetail) {

        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    MostAndRecentPlayTableHelper.getInstance(context).inserSong(mDetail);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }
        };

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.HONEYCOMB) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            task.execute();
        }
    }

    /**
     * Store Favorite Play Data
     */
    public synchronized void storeFavoritePlay(final Context context, final SongDetail mDetail, final int isFav) {

        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    FavoritePlayTableHelper.getInstance(context).inserSong(mDetail, isFav);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }
        };

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.HONEYCOMB) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            task.execute();
        }
    }

    public synchronized void checkIsFavorite(final Context context, final SongDetail mDetail, final View v) {

        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            boolean isFavorite = false;

            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    // isFavorite = FavoritePlayTableHelper.getInstance(context).getIsFavorite(mDetail);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                v.setSelected(isFavorite);
            }
        };

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.HONEYCOMB) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            task.execute();
        }
    }

    public void slowRun(final SongDetail msongs)


    {
        Utility.getSharedInstance().showProgressDialog(context);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                Utility.getSharedInstance().dismissProgressDialog();

                //Do something after 100ms

                MediaController.getInstance().playAudio(msongs);
            }
        }, 3000);
    }



    public class calc_stanica extends AsyncTask<ArrayList<String>, Void, Boolean> {


        private SongDetail mSongDetail;
        private boolean mCheck = false;

        public void calc_stanica(SongDetail mSongDetail) {

            this.mSongDetail = mSongDetail;
        }

        @Override
        protected Boolean doInBackground(ArrayList<String>... arrayLists) {


            if (mSongDetail == null) {
                mCheck = false;
                return false;
            }
            if ((audioTrackPlayer != null || audioPlayer != null) && MusicPreferance.playingSongDetail != null && mSongDetail.getId() == MusicPreferance.playingSongDetail.getId()) {
                if (isPaused) {
                    resumeAudio(mSongDetail);
                }
                mCheck = true;
                return true;
            }
            if (audioTrackPlayer != null) {
                MusicPlayerService.setIgnoreAudioFocus();
            }
            cleanupPlayer(!playMusicAgain, false);


            playMusicAgain = false;
            File file = null;

            try {


                audioPlayer = new MediaPlayer();
                audioPlayer.setAudioStreamType(useFrontSpeaker ? AudioManager.STREAM_VOICE_CALL : AudioManager.STREAM_MUSIC);


//            audioPlayer.setDataSource(mSongDetail.getPath());
                String mode = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_MUSIC_MODE, "none");
                String URL = "";

//                  BASE_URL_FOR_PLAY=mSongDetail.getTrack_url();

                    URL = BASE_URL_FOR_PLAY + Uri.encode(mSongDetail.getFile());



                // audioPlayer.setDataSource(BASE_URL_FOR_PLAY+mSongDetail.getFile());

//            String encode=    Uri.encode(mSongDetail.getFile());
//
//                try {
//                    encode = URLEncoder.encode(mSongDetail.getFile(), "utf-8");
//                } catch (UnsupportedEncodingException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }

                Log.e(":::::::Encode URL::::",URL);

                Log.e("::::Original URL::::::",URL.replace(" ", "%20"));

//                audioPlayer.setDataSource(URL.replace(" ", "%20"));
                audioPlayer.setDataSource(URL);

                audioPlayer.setOnInfoListener(new MediaPlayer.OnInfoListener() {
                    @Override
                    public boolean onInfo(MediaPlayer mp, int what, int extra) {

                        switch (what) {
                            case MediaPlayer.MEDIA_INFO_BUFFERING_START:
                                if (DashBoardActivity.progressBarUpper.getVisibility() == View.GONE)
                                    DashBoardActivity.progressBarUpper.setVisibility(View.VISIBLE);
                                DashBoardActivity.progressBarMain.setVisibility(View.VISIBLE);
                                DashBoardActivity.btn_playpausePanel.setVisibility(View.GONE);
                                DashBoardActivity.btn_playpause.setVisibility(View.GONE);

                                break;
                            case MediaPlayer.MEDIA_INFO_BUFFERING_END:
                                if (DashBoardActivity.progressBarUpper.getVisibility() == View.VISIBLE)
                                    DashBoardActivity.progressBarUpper.setVisibility(View.GONE);
                                DashBoardActivity.progressBarMain.setVisibility(View.GONE);
                                DashBoardActivity.btn_playpausePanel.setVisibility(View.VISIBLE);
                                DashBoardActivity.btn_playpause.setVisibility(View.VISIBLE);
                                break;
                        }
                        mCheck = false;
                        return false;

                    }
                });
                audioPlayer.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
                    @Override
                    public void onBufferingUpdate(MediaPlayer mp, int percent) {

                    }
                });
                audioPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mediaPlayer) {
                        MusicPreferance.playingSongDetail.audioProgress = 0.0f;
                        MusicPreferance.playingSongDetail.audioProgressSec = 0;
                        if (!MusicPreferance.playlist.isEmpty() && MusicPreferance.playlist.size() > 1) {
                            playNextSong(true);
                        } else {
                            cleanupPlayer(true, true);
                        }
                    }
                });

                audioPlayer.prepare();
                audioPlayer.start();
                startProgressTimer();


            } catch (Exception e) {
                if (audioPlayer != null) {
                    audioPlayer.release();
                    audioPlayer = null;
                    isPaused = false;
                    MusicPreferance.playingSongDetail = null;
                }

                mCheck = false;
                return false;
            }


            return mCheck;

        }


        @Override
        protected void onPreExecute() {


            dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialog.setContentView(R.layout.dialog_progress);
            GifImageView gifImageView = (GifImageView) dialog.findViewById(R.id.gif);

            // dialog.getWindow().getAttributes().width = ActionBar.LayoutParams.FILL_PARENT;

            dialog.show();
            dialog.setCancelable(true);
        }

        @Override
        protected void onPostExecute(Boolean result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);

            if (dialog.isShowing()) {
                dialog.dismiss();
            }


            isPaused = false;
            lastProgress = 0;
            MusicPreferance.playingSongDetail = mSongDetail;
//            NotificationManager.getInstance().postNotificationName(NotificationManager.audioDidStarted, mSongDetail);

            if (audioPlayer != null) {
                try {


//                    Toast.makeText(context, "Time "+MusicPreferance.playingSongDetail.audioProgress, Toast.LENGTH_SHORT).show();
                    if (MusicPreferance.playingSongDetail.audioProgress != 0) {
                        int seekTo = (int) (audioPlayer.getDuration() * MusicPreferance.playingSongDetail.audioProgress);
                        audioPlayer.seekTo(seekTo);
                    }

                } catch (Exception e2) {
                    MusicPreferance.playingSongDetail.audioProgress = 0;
                    MusicPreferance.playingSongDetail.audioProgressSec = 0;
                }
            } else if (audioTrackPlayer != null) {
                if (MusicPreferance.playingSongDetail.audioProgress == 1) {
                    MusicPreferance.playingSongDetail.audioProgress = 0;
                }

            }

//            if (MusicPreferance.playingSongDetail != null) {
//                Intent intent = new Intent(MyApplication.applicationContext, MusicPlayerService.class);
//                MyApplication.applicationContext.startService(intent);
//            } else {
//                Intent intent = new Intent(MyApplication.applicationContext, MusicPlayerService.class);
//                MyApplication.applicationContext.stopService(intent);
//            }
//
//            storeResendPlay(MyApplication.applicationContext, mSongDetail);
//            NotificationManager.getInstance().notifyNewSongLoaded(NotificationManager.newaudioloaded, mSongDetail);

//           return  result;
        }


    }

//    UI Thread


    public class calc_stanicaDownload extends AsyncTask<ArrayList<String>, Void, Boolean> {


        private SongDetail mSongDetail;
        private boolean mCheck = false;

        public void calc_stanica(SongDetail mSongDetail) {

            this.mSongDetail = mSongDetail;
        }

        @Override
        protected Boolean doInBackground(ArrayList<String>... arrayLists) {
            Utility.getSharedInstance().showProgressDialog(appcontectext);

            if (mSongDetail == null) {
                return false;
            }
            if ((audioTrackPlayer != null || audioPlayer != null) && MusicPreferance.playingSongDetail != null && mSongDetail.getId() == MusicPreferance.playingSongDetail.getId()) {
                if (isPaused) {
                    resumeAudio(mSongDetail);
                }
                return true;
            }
            if (audioTrackPlayer != null) {
                MusicPlayerService.setIgnoreAudioFocus();
            }
            cleanupPlayer(!playMusicAgain, false);
            playMusicAgain = false;
            File file = null;

            try {
                audioPlayer = new MediaPlayer();
                audioPlayer.setAudioStreamType(useFrontSpeaker ? AudioManager.STREAM_VOICE_CALL : AudioManager.STREAM_MUSIC);

//            audioPlayer.setDataSource(mSongDetail.getPath());


                String url = decrypt(mSongDetail.getFile());
                // audioPlayer.setDataSource(BASE_URL_FOR_PLAY+mSongDetail.getFile());
                Utility.getSharedInstance().dismissProgressDialog();

                audioPlayer.setDataSource(url);

                audioPlayer.setOnInfoListener(new MediaPlayer.OnInfoListener() {
                    @Override
                    public boolean onInfo(MediaPlayer mp, int what, int extra) {

                        switch (what) {
                            case MediaPlayer.MEDIA_INFO_BUFFERING_START:
                                if (DashBoardActivity.progressBarUpper.getVisibility() == View.GONE)
                                    DashBoardActivity.progressBarUpper.setVisibility(View.VISIBLE);
                                DashBoardActivity.progressBarMain.setVisibility(View.VISIBLE);
                                DashBoardActivity.btn_playpausePanel.setVisibility(View.GONE);
                                DashBoardActivity.btn_playpause.setVisibility(View.GONE);

                                break;
                            case MediaPlayer.MEDIA_INFO_BUFFERING_END:
                                if (DashBoardActivity.progressBarUpper.getVisibility() == View.VISIBLE)
                                    DashBoardActivity.progressBarUpper.setVisibility(View.GONE);
                                DashBoardActivity.progressBarMain.setVisibility(View.GONE);
                                DashBoardActivity.btn_playpausePanel.setVisibility(View.VISIBLE);
                                DashBoardActivity.btn_playpause.setVisibility(View.VISIBLE);
                                break;
                        }
                        return false;

                    }
                });
                audioPlayer.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
                    @Override
                    public void onBufferingUpdate(MediaPlayer mp, int percent) {

                    }
                });
                audioPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mediaPlayer) {
                        MusicPreferance.playingSongDetail.audioProgress = 0.0f;
                        MusicPreferance.playingSongDetail.audioProgressSec = 0;
                        if (!MusicPreferance.playlist.isEmpty() && MusicPreferance.playlist.size() > 1) {
                            playNextSong(true);
                        } else {
                            cleanupPlayer(true, true);
                        }
                    }
                });
                audioPlayer.prepare();
                audioPlayer.start();
                startProgressTimer();
            } catch (Exception e) {
                if (audioPlayer != null) {
                    audioPlayer.release();
                    audioPlayer = null;
                    isPaused = false;
                    MusicPreferance.playingSongDetail = null;
                }
                return false;
            }
            isPaused = false;
            lastProgress = 0;
            MusicPreferance.playingSongDetail = mSongDetail;
//        NotificationManager.getInstance().postNotificationName(NotificationManager.audioDidStarted, mSongDetail);

            if (audioPlayer != null) {
                try {
                    if (MusicPreferance.playingSongDetail.audioProgress != 0) {
                        int seekTo = (int) (audioPlayer.getDuration() * MusicPreferance.playingSongDetail.audioProgress);
                        audioPlayer.seekTo(seekTo);
                    }
                } catch (Exception e2) {
                    MusicPreferance.playingSongDetail.audioProgress = 0;
                    MusicPreferance.playingSongDetail.audioProgressSec = 0;
                }
            } else if (audioTrackPlayer != null) {
                if (MusicPreferance.playingSongDetail.audioProgress == 1) {
                    MusicPreferance.playingSongDetail.audioProgress = 0;
                }

            }
//
//        if (MusicPreferance.playingSongDetail != null) {
//            Intent intent = new Intent(MyApplication.applicationContext, MusicPlayerService.class);
//            MyApplication.applicationContext.startService(intent);
//        } else {
//            Intent intent = new Intent(MyApplication.applicationContext, MusicPlayerService.class);
//            MyApplication.applicationContext.stopService(intent);
//        }
//
//        storeResendPlay(MyApplication.applicationContext, mSongDetail);
//        NotificationManager.getInstance().notifyNewSongLoaded(NotificationManager.newaudioloaded, mSongDetail);

            return true;
        }


        @Override
        protected void onPreExecute() {


            dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialog.setContentView(R.layout.dialog_progress);
            GifImageView gifImageView = (GifImageView) dialog.findViewById(R.id.gif);

            // dialog.getWindow().getAttributes().width = ActionBar.LayoutParams.FILL_PARENT;

            dialog.show();
            dialog.setCancelable(true);
        }

        @Override
        protected void onPostExecute(Boolean result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);

            if (dialog.isShowing()) {
                dialog.dismiss();
            }


            isPaused = false;
            lastProgress = 0;
            MusicPreferance.playingSongDetail = mSongDetail;
//            NotificationManager.getInstance().postNotificationName(NotificationManager.audioDidStarted, mSongDetail);

            if (audioPlayer != null) {
                try {


//                    Toast.makeText(context, "Time "+MusicPreferance.playingSongDetail.audioProgress, Toast.LENGTH_SHORT).show();
                    if (MusicPreferance.playingSongDetail.audioProgress != 0) {
                        int seekTo = (int) (audioPlayer.getDuration() * MusicPreferance.playingSongDetail.audioProgress);
                        audioPlayer.seekTo(seekTo);
                    }

                } catch (Exception e2) {
                    MusicPreferance.playingSongDetail.audioProgress = 0;
                    MusicPreferance.playingSongDetail.audioProgressSec = 0;
                }
            } else if (audioTrackPlayer != null) {
                if (MusicPreferance.playingSongDetail.audioProgress == 1) {
                    MusicPreferance.playingSongDetail.audioProgress = 0;
                }

            }

//            if (MusicPreferance.playingSongDetail != null) {
//                Intent intent = new Intent(MyApplication.applicationContext, MusicPlayerService.class);
//                MyApplication.applicationContext.startService(intent);
//            } else {
//                Intent intent = new Intent(MyApplication.applicationContext, MusicPlayerService.class);
//                MyApplication.applicationContext.stopService(intent);
//            }
//
//            storeResendPlay(MyApplication.applicationContext, mSongDetail);
//            NotificationManager.getInstance().notifyNewSongLoaded(NotificationManager.newaudioloaded, mSongDetail);

//           return  result;
        }


    }

    private void stopOtherSOngs()
    {

        AudioManager am = (AudioManager)context.getSystemService(Context.AUDIO_SERVICE);

// Request audio focus for playback
        int result = am.requestAudioFocus(focusChangeListener,
// Use the music stream.
                AudioManager.STREAM_MUSIC,
// Request permanent focus.
                AudioManager.AUDIOFOCUS_GAIN);


        if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
// other app had stopped playing song now , so u can do u stuff now .
        }
    }


    private AudioManager.OnAudioFocusChangeListener focusChangeListener =
            new AudioManager.OnAudioFocusChangeListener() {
                public void onAudioFocusChange(int focusChange) {
                    AudioManager am =(AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
                    switch (focusChange) {

                        case (AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK) :
                            // Lower the volume while ducking.

                            // Return the volume to normal and resume if paused.
                            try
                            {
                                if(MediaController.getInstance().isAudioPaused())
                                {
                                    audioPlayer.pause();
                                }
                                else {
                                    audioPlayer.setVolume(1f, 1f);
                                    audioPlayer.start();
                                }
                            }
                            catch (Exception e)
                            {}
                            break;
                        case (AudioManager.AUDIOFOCUS_LOSS_TRANSIENT) :

                            // Return the volume to normal and resume if paused.
                            try
                            {
                                audioPlayer.pause();
                            }
                            catch (Exception e)
                            {}
                            break;

                        case (AudioManager.AUDIOFOCUS_LOSS) :

                            // Return the volume to normal and resume if paused.
                            try
                            {
                                audioPlayer.pause();
                            }
                            catch (Exception e)
                            {}
                            // ComponentName component =new ComponentName(AudioPlayerActivity.this,MediaControlReceiver.class);
                            //am.unregisterMediaButtonEventReceiver(component);
                            break;

                        case (AudioManager.AUDIOFOCUS_GAIN) :
                            // Return the volume to normal and resume if paused.
                            try
                            {
                                if(MediaController.getInstance().isAudioPaused())
                                {
                                    audioPlayer.pause();
                                }
                                else {
                                    audioPlayer.setVolume(1f, 1f);
                                    audioPlayer.start();
                                }

                            }
                            catch (Exception e)
                            {}

                            break;
                        default: break;
                    }
                }
            };

}

