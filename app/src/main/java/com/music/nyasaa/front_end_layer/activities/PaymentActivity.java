package com.music.nyasaa.front_end_layer.activities;

import android.support.v7.app.AppCompatActivity;

//import com.stripe.android.PaymentConfiguration;
//import com.stripe.android.Stripe;
//import com.stripe.android.TokenCallback;
//import com.stripe.android.model.Card;
//import com.stripe.android.model.Token;
//import com.stripe.android.view.CardInputWidget;


public class PaymentActivity extends AppCompatActivity {
//    private final static int LOAD_PAYMENT_DATA_REQUEST_CODE = 53;
//    private Button butSaveCard;
//    private CardInputWidget mCardInputWidget;
//    private PaymentsClient paymentsClient;
//    private LinearLayout imgBackLayout;
//    private TextView mTextViewSubDate;
//    private LinearLayout mBackLayout, mImgRightLayout;
//
//    private TextView mTxtPayAmount;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_payment);
//        initView();
//        onClick();
//    }
//
//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
//        finish();
//        overridePendingTransition(R.anim.left_anim, R.anim.right_anim);
//    }
//
//    private void initView() {
//        TextView textView = (TextView) findViewById(R.id.txtTitleToolbar);
//        textView.setText(" Subscribe");
//        mCardInputWidget = (CardInputWidget) findViewById(R.id.card_input_widget);
//        mTextViewSubDate = (TextView) findViewById(R.id.textViewSubDate);
//        butSaveCard = (Button) findViewById(R.id.saveCard);
//        paymentsClient =
//                Wallet.getPaymentsClient(PaymentActivity.this,
//                        new Wallet.WalletOptions.Builder().setEnvironment(WalletConstants.ENVIRONMENT_PRODUCTION)
//                                .build());
//
//        isReadyToPay();
//        getUserInfo();
//
//        mBackLayout = findViewById(R.id.imgBackLayout);
//        mBackLayout.setVisibility(View.VISIBLE);
//
//        mImgRightLayout = findViewById(R.id.img_right_layout);
//
//        mTxtPayAmount = findViewById(R.id.txtPayAMount);
//
//
//        mImgRightLayout.setVisibility(View.GONE);
//
//
//        mBackLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onBackPressed();
//            }
//        });
//
//
//        Bundle extras = getIntent().getExtras();
//        if (extras != null) {
//
//            String amount = extras.getString("pay");
//            String id = extras.getString("id");
//
//            mTxtPayAmount.setText("₹ "+ amount);
//
//        }
//    }
//
//    private void onClick() {
//
//
//        butSaveCard.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Toast.makeText(PaymentActivity.this, "Please wait...", Toast.LENGTH_SHORT).show();
//                Utility.getSharedInstance().showProgressDialog(PaymentActivity.this);
//                Card cardToSave = mCardInputWidget.getCard();
//                /*cardToSave = new Card(
//                        "4242424242424242", //card number
//                        12, //expMonth
//                        2025,//expYear
//                        "123"//cvc
//                );*/
//                if (cardToSave != null) {
//                    if (!cardToSave.validateCard()) {
//                        Utility.getSharedInstance().dismissProgressDialog();
//                        // Do not continue token creation.
//                        Toast.makeText(PaymentActivity.this, "Card is not valid", Toast.LENGTH_SHORT).show();
//                    } else {
//
//                        Stripe stripe = new Stripe(PaymentActivity.this, "pk_test_yjvfnSqfOtZ68zZ2sykXMMoo");
////                        Stripe stripe = new Stripe(PaymentActivity.this, "pk_live_HWbArqG6EA1pyMEkrRsmF15e");
//                        stripe.createToken(
//                                cardToSave,
//                                new TokenCallback() {
//                                    public void onSuccess(Token token) {
//                                        // Send token to your server
////                                        Toast.makeText(PaymentActivity.this, "Success : Token is" + token.getId(), Toast.LENGTH_SHORT).show();
//                                        updateSubscriptionStatusAPICall(token.getId());
//                                    }
//
//                                    public void onError(Exception error) {
//                                        // Show localized error message
////                                    Toast.makeText(getContext(),
////                                            error.getLocalizedString(getContext()),
////                                            Toast.LENGTH_LONG
////                                    ).show();
//                                        Utility.getSharedInstance().dismissProgressDialog();
//                                        error.printStackTrace();
//                                        Toast.makeText(PaymentActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
//                                    }
//                                }
//                        );
//                    }
//                } else {
//                    Utility.getSharedInstance().dismissProgressDialog();
//                    Toast.makeText(PaymentActivity.this, "Enter valid card details..", Toast.LENGTH_SHORT).show();
//                }
//                /*PaymentDataRequest request = createPaymentDataRequest();
//                if (request != null) {
//                    AutoResolveHelper.resolveTask(
//                            paymentsClient.loadPaymentData(request),
//                            PaymentActivity.this,
//                            LOAD_PAYMENT_DATA_REQUEST_CODE);
//                }
//                createPaymentDataRequest();*/
//            }
//        });
//
//
//        imgBackLayout = (LinearLayout) findViewById(R.id.imgBackLayout);
//        imgBackLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                onBackPressed();
//            }
//        });
//    }
//
//    private void payWithGoogle() {
//        PaymentDataRequest request = createPaymentDataRequest();
//        if (request != null) {
//            AutoResolveHelper.resolveTask(
//                    paymentsClient.loadPaymentData(request),
//                    PaymentActivity.this,
//                    LOAD_PAYMENT_DATA_REQUEST_CODE);
//        }
//    }
//
//    private PaymentMethodTokenizationParameters createTokenizationParameters() {
//        return PaymentMethodTokenizationParameters.newBuilder()
//                .setPaymentMethodTokenizationType(WalletConstants.PAYMENT_METHOD_TOKENIZATION_TYPE_PAYMENT_GATEWAY)
//                .addParameter("gateway", "stripe")
////                .addParameter("stripe:publishableKey", "pk_test_yjvfnSqfOtZ68zZ2sykXMMoo")
//                .addParameter("stripe:publishableKey", "pk_live_HWbArqG6EA1pyMEkrRsmF15e")
//                .addParameter("stripe:version", "5.1.0")
//                .build();
//    }
//
//    //  .addParameter("stripe:publishableKey", "pk_test_oxfjkCb8X9EzozO6cf2gUtQ9")
//    private PaymentDataRequest createPaymentDataRequest() {
//        PaymentDataRequest.Builder request =
//                PaymentDataRequest.newBuilder()
//                        .setTransactionInfo(
//                                TransactionInfo.newBuilder()
//                                        .setTotalPriceStatus(WalletConstants.TOTAL_PRICE_STATUS_FINAL)
//                                        .setTotalPrice("1.00")
//                                        .setCurrencyCode("INR")
//                                        .build())
//                        .addAllowedPaymentMethod(WalletConstants.PAYMENT_METHOD_CARD)
//                        .addAllowedPaymentMethod(WalletConstants.PAYMENT_METHOD_TOKENIZED_CARD)
//                        .setCardRequirements(
//                                CardRequirements.newBuilder()
//                                        .addAllowedCardNetworks(Arrays.asList(
//                                                WalletConstants.CARD_NETWORK_AMEX,
//                                                WalletConstants.CARD_NETWORK_DISCOVER,
//                                                WalletConstants.CARD_NETWORK_VISA,
//                                                WalletConstants.CARD_NETWORK_MASTERCARD))
//                                        .build());
//
//        request.setPaymentMethodTokenizationParameters(createTokenizationParameters());
//        return request.build();
//    }
//
//
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        switch (requestCode) {
//            case LOAD_PAYMENT_DATA_REQUEST_CODE: {
//                switch (resultCode) {
//                    case Activity.RESULT_OK:
//                        PaymentData paymentData = PaymentData.getFromIntent(data);
//                        // You can get some data on the user's card, such as the brand and last 4 digits
//                        assert paymentData != null;
//                        CardInfo info = paymentData.getCardInfo();
//                        // You can also pull the user address from the PaymentData object.
//                        UserAddress address = paymentData.getShippingAddress();
//                        // This is the raw JSON string version of your Stripe token.
//                        String rawToken = paymentData.getPaymentMethodToken().getToken();
//                        Toast.makeText(PaymentActivity.this,
//                                "Paid Successfully ", Toast.LENGTH_LONG).show();
//                        Intent intent = new Intent(PaymentActivity.this, DashBoardActivity.class);
//                        // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                        startActivity(intent);
//                        overridePendingTransition(R.anim.left_anim, R.anim.right_anim);
//                        finish();
//                        // Now that you have a Stripe token object, charge that by using the id
//                        Token stripeToken = Token.fromString(rawToken);
//                        if (stripeToken != null) {
//                            // This chargeToken function is a call to your own server, which should then connect
//                            // to Stripe's API to finish the charge.
//                            //chargeToken(stripeToken.getId());
//                            Toast.makeText(PaymentActivity.this,
//                                    "Got token " + stripeToken.toString(), Toast.LENGTH_LONG).show();
//                            //updateSubscriptionStatusAPICall(stripeToken.getId(), "INR", "1");
//                        }
//                        break;
//                    case Activity.RESULT_CANCELED:
//                        Toast.makeText(PaymentActivity.this,
//                                "Payment cancelled ", Toast.LENGTH_LONG).show();
//                        //updateSubscriptionStatusAPICall("", "", "2");
//                        break;
//                    case AutoResolveHelper.RESULT_ERROR:
//                        Status status = AutoResolveHelper.getStatusFromIntent(data);
//                        // Log the status for debugging
//                        // Generally there is no need to show an error to
//                        // the user as the Google Payment API will do that
//                        Toast.makeText(PaymentActivity.this,
//                                "Result error ", Toast.LENGTH_LONG).show();
//                        //updateSubscriptionStatusAPICall("", "", "3");
//                        break;
//
//                    default:
//                        Toast.makeText(PaymentActivity.this,
//                                "Failed subscription ", Toast.LENGTH_LONG).show();
//                        //updateSubscriptionStatusAPICall("", "", "3");
//
//                }
//
//
//            }
//
//
////                break; // Breaks the case LOAD_PAYMENT_DATA_REQUEST_CODE
//            // Handle any other startActivityForResult calls you may have made.
//            default:
////                Toast.makeText(PaymentActivity.this,
////                        "User cancelled  payment process ", Toast.LENGTH_LONG).show();
//                // Do nothing.
//        }
//    }
//
//    private void isReadyToPay() {
//
//        IsReadyToPayRequest request = IsReadyToPayRequest.newBuilder()
//                .addAllowedPaymentMethod(WalletConstants.PAYMENT_METHOD_CARD)
//                .addAllowedPaymentMethod(WalletConstants.PAYMENT_METHOD_TOKENIZED_CARD)
//                .build();
//        Task<Boolean> task = paymentsClient.isReadyToPay(request);
//        task.addOnCompleteListener(
//                new OnCompleteListener<Boolean>() {
//                    public void onComplete(@NonNull Task<Boolean> task) {
//                        try {
//                            boolean result =
//                                    task.getResult(ApiException.class);
//
//                            if (result) {
////                                Toast.makeText(PaymentActivity.this, "Ready", Toast.LENGTH_SHORT).show();
//
//                            } else {
//                                Toast.makeText(PaymentActivity.this, "Please wait you can not pay this amount", Toast.LENGTH_SHORT).show();
//                                //hide Google as payment option
//                            }
//                        } catch (ApiException exception) {
//                            Toast.makeText(PaymentActivity.this,
//                                    "Exception: " + exception.getLocalizedMessage(),
//                                    Toast.LENGTH_LONG).show();
//                        }
//                    }
//                });
//
//    }
//
//    public void updateSubscriptionStatusAPICall(String token) {
//
//        MyApplication myApplication = new MyApplication();
//
//        String userID = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "none");
//
//        AddSubscriptionRequest addSubscriptionRequest = new AddSubscriptionRequest();
//        addSubscriptionRequest.setUserId(userID);
//        addSubscriptionRequest.setTokenId(token);
//
//        if (Utility.getSharedInstance().isConnectedToInternet(this)) {
//            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().addSubscription(X_API_KEY, addSubscriptionRequest);
//            callbackLogin.enqueue(new Callback<JsonObject>() {
//                @Override
//                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//
//                    JsonObject responseData = response.body();
//
//                    if (responseData != null) {
//                        if (responseData.get("status").getAsBoolean()) {
//                            Utility.getSharedInstance().dismissProgressDialog();
//                            Gson gson = new Gson();
//                            Type listType = new TypeToken<List<SongDetail>>() {
//                            }.getType();
//                            Toast.makeText(PaymentActivity.this, responseData.get("message").getAsString(), Toast.LENGTH_SHORT).show();
//                            Intent intent = new Intent(PaymentActivity.this, DashBoardActivity.class);
//                            startActivity(intent);
//                            overridePendingTransition(R.anim.left_anim, R.anim.right_anim);
//                            finish();
//                        /*JsonObject jsonObject = responseData.get("resultObject").getAsJsonObject();
//                        if ( jsonObject != null) {
//                            Toast.makeText(PaymentActivity.this, "Success", Toast.LENGTH_SHORT).show();
//                        }*/
//                        } else {
//                            Utility.getSharedInstance().dismissProgressDialog();
//                            Toast.makeText(PaymentActivity.this, "Failed: " + responseData.get("message").getAsString(), Toast.LENGTH_SHORT).show();
//                        }
//                    } else {
//                        Utility.getSharedInstance().dismissProgressDialog();
//                        Toast.makeText(PaymentActivity.this, "Failed: " + response.message(), Toast.LENGTH_SHORT).show();
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<JsonObject> call, Throwable t) {
//                    Utility.getSharedInstance().dismissProgressDialog();
//                    Toast.makeText(PaymentActivity.this, "Failed", Toast.LENGTH_SHORT).show();
//                }
//
//            });
//        } else {
//            Utility.getSharedInstance().dismissProgressDialog();
//            Toast.makeText(this, "No internet connection available", Toast.LENGTH_SHORT).show();
//        }
//    }
//
//    public void getUserInfo() {
//
//        MyApplication myApplication = new MyApplication();
//
//        if (Utility.getSharedInstance().isConnectedToInternet(PaymentActivity.this)) {
//
//            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");
//
//            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().getUserInfo(X_API_KEY, id);
//            callbackLogin.enqueue(new Callback<JsonObject>() {
//                @Override
//                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//
//                    JsonObject responseData = response.body();
//                    if (responseData != null) {
//                        String status = responseData.get("subscriptionStatus").getAsString();
//                        if (status.equalsIgnoreCase("2")) {
//                            mTextViewSubDate.setVisibility(View.VISIBLE);
//                            SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
//                            SimpleDateFormat output = new SimpleDateFormat("dd MMM, yyyy", Locale.ENGLISH);
//
//                            Date d = null;
//                            String date = responseData.get("subscriptionEndDate").getAsString();
//                            try {
//                                d = input.parse(date);
//                                String formatted = output.format(d);
//                                mTextViewSubDate.setText("Your current subscription end date is :\n" + formatted
//                                        + "\n By completing the transaction you will add 30 more days to your subscription");
//                            } catch (ParseException e) {
//                                e.printStackTrace();
//                            }
//                        } else {
//                            mTextViewSubDate.setVisibility(View.GONE);
//                        }
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<JsonObject> call, Throwable t) {
//                    //Toast.makeText(getActivity(), "Server error .. Please try again after some time", Toast.LENGTH_SHORT).show();
//                }
//
//            });
//        } else {
//            Toast.makeText(this, "No internet connection available", Toast.LENGTH_SHORT).show();
//        }
//    }

}
