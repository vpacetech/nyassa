package com.music.nyasaa.front_end_layer.activities;

import android.os.Bundle;
import android.app.Activity;
import instamojo.library.InstapayListener;
import instamojo.library.InstamojoPay;

import org.json.JSONObject;
import org.json.JSONException;
import android.content.IntentFilter;
import android.widget.Toast;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.music.nyasaa.R;

public class ProfileActivity extends AppCompatActivity {

    private ImageView mImgBack;
    private LinearLayout imgBackLayout;

    
    private void callInstamojoPay(String email, String phone, String amount, String purpose, String buyername) {
        final Activity activity = this;
        InstamojoPay instamojoPay = new InstamojoPay();
        IntentFilter filter = new IntentFilter("ai.devsupport.instamojo");
        registerReceiver(instamojoPay, filter);
        JSONObject pay = new JSONObject();
        try {
            pay.put("email", email);
            pay.put("phone", phone);
            pay.put("purpose", purpose);
            pay.put("amount", amount);
            pay.put("name", buyername);
       pay.put("send_sms", true);
      pay.put("send_email", true);
 } catch (JSONException e) {
            e.printStackTrace();
        }
        initListener();
        instamojoPay.start(activity, pay, listener);
    }
    
    InstapayListener listener;

    
    private void initListener() {
        listener = new InstapayListener() {
            @Override
            public void onSuccess(String response) {
                Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG)
                        .show();
            }

            @Override
            public void onFailure(int code, String reason) {
                Toast.makeText(getApplicationContext(), "Failed: " + reason, Toast.LENGTH_LONG)
                        .show();
            }
        };
    }
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        // Call the function callInstamojo to start payment here
        initView();
        onClick();
    }

    private void initView() {
        TextView mTxtToolbaTxt = (TextView) findViewById(R.id.txtTitleToolbar);
        mImgBack = (ImageView) findViewById(R.id.id_img_back);
        imgBackLayout=(LinearLayout)findViewById(R.id.imgBackLayout);

        mImgBack.setVisibility(View.VISIBLE);

        mImgBack.setImageResource(0);

        mImgBack.setImageResource(R.drawable.ic_ab_back);
        mTxtToolbaTxt.setText("Profile");
    }

    private void onClick() {

        imgBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_anim, R.anim.right_anim);
    }
}
