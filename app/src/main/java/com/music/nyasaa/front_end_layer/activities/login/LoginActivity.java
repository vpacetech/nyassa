package com.music.nyasaa.front_end_layer.activities.login;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.music.nyasaa.R;

import com.music.nyasaa.controller.MyApplication;
import com.music.nyasaa.front_end_layer.activities.GetPackageActivity;
import com.music.nyasaa.front_end_layer.activities.OTP.OTPActivity;
import com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity;
import com.music.nyasaa.models.User_DAO;
import com.music.nyasaa.utility.SharedPreferenceManager;
import com.music.nyasaa.utility.Utility;
import com.music.nyasaa.widgets.edittext.CustomEdittextRegular;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



import static com.music.nyasaa.retrofit.Config.X_API_KEY;
import static com.music.nyasaa.retrofit.Config.clientService;


public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private Button mButLogin, mButSignUp;
    private Intent intent;
    CustomEdittextRegular edt_Email, edt_Password;
    MyApplication myApplication;
    private TextView mTxtForgotPassword;
    private Dialog dialog;
    private Dialog dialog_sign_up;

    private EditText mEditEmail, mEditPassword, mEditUserName, mEditFname, mEditLname, mEditMobile;
    private String sEditEmai = "", sEditPassword = "", sEditUserName = "", sEditFname = "", sEditLname = "", sEditMobile = "";
//    AVLoadingIndicatorView avi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        myApplication = new MyApplication();
        initView();
        onClick();

    }

    void startAnim() {
//        avi.show();
        // or avi.smoothToShow();
    }

    void stopAnim() {
//        avi.hide();
        // or avi.smoothToHide();
    }


    private void initView() {

        mButLogin = (Button) findViewById(R.id.butLogin);
        mButSignUp = (Button) findViewById(R.id.butSignUp);
        edt_Email = (CustomEdittextRegular) findViewById(R.id.edt_Email);
        edt_Password = (CustomEdittextRegular) findViewById(R.id.edt_Password);

        mTxtForgotPassword = (TextView) findViewById(R.id.txtForgotPassword);

//        avi= (AVLoadingIndicatorView)findViewById(R.id.avi);

    }

    private void onClick() {

        mButSignUp.setOnClickListener(this);
        mButLogin.setOnClickListener(this);
        mTxtForgotPassword.setOnClickListener(this);
    }

    @TargetApi(Build.VERSION_CODES.FROYO)
    @RequiresApi(api = Build.VERSION_CODES.ECLAIR)
    @Override
    public void onClick(View v) {


        switch (v.getId()) {
            case R.id.butLogin:


                validationForLogin(v);

                //  finish();
                break;

            case R.id.butSignUp:
//                intent = new Intent(LoginActivity.this, SignUpActivity.class);
////                intent = new Intent(LoginActivity.this, SignUpActivityNew.class);
//                startActivity(intent);
//                overridePendingTransition(R.anim.left, R.anim.right);
//                finish();

                dialog_sign_up = new Dialog(LoginActivity.this);
                dialog_sign_up.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog_sign_up.setContentView(R.layout.dialog_sign_up);
                dialog_sign_up.getWindow().getAttributes().width = ActionBar.LayoutParams.FILL_PARENT;
                dialog_sign_up.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog_sign_up.show();
                dialog_sign_up.setCancelable(true);

                mEditEmail = dialog_sign_up.findViewById(R.id.edtEmail);
                mEditFname = dialog_sign_up.findViewById(R.id.editFname);
                mEditLname = dialog_sign_up.findViewById(R.id.editLastName);
                mEditUserName = dialog_sign_up.findViewById(R.id.editUsername);
                mEditMobile = dialog_sign_up.findViewById(R.id.editMobile);
                mEditPassword = dialog_sign_up.findViewById(R.id.editPassword);


                Button mButSub = dialog_sign_up.findViewById(R.id.butYes);

                mButSub.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        sEditEmai = mEditEmail.getText().toString().trim();
                        sEditFname = mEditFname.getText().toString().trim();
                        sEditLname = mEditLname.getText().toString().trim();
                        sEditUserName = mEditUserName.getText().toString().trim();
                        sEditMobile = mEditMobile.getText().toString().trim();
                        sEditPassword = mEditPassword.getText().toString().trim();
                        userSIGNUP();
                    }
                });
                break;
            case R.id.txtForgotPassword:

                dialog = new Dialog(LoginActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialod_forget_password);
                dialog.getWindow().getAttributes().width = ActionBar.LayoutParams.FILL_PARENT;
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
                dialog.setCancelable(true);

                final EditText edtEmail = dialog.findViewById(R.id.edtEmail);

                Button mButYes = (Button) dialog.findViewById(R.id.butYes);

                mButYes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (edtEmail.getText().toString().trim().equalsIgnoreCase("") ||edtEmail.getText().length()<10) {
                            Toast.makeText(LoginActivity.this, "Enter valid mobile number", Toast.LENGTH_LONG).show();
                        } else {
                           //
                             forgotPassword(edtEmail.getText().toString().trim());
                        }

                    }
                });
                break;
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.FROYO)
    public static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    @RequiresApi(api = Build.VERSION_CODES.FROYO)
    private void validationForLogin(View v) {

//
//        intent = new Intent(LoginActivity.this, OTPActivity.class);
//        startActivity(intent);
//        overridePendingTransition(R.anim.left, R.anim.right);
//        finish();

//        if (edt_Email.getText().toString().equalsIgnoreCase("") || !isValidEmail(edt_Email.getText().toString())) {
//            Toast.makeText(LoginActivity.this, "Enter valid email id", Toast.LENGTH_SHORT).show();
//        }

        if (edt_Email.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(LoginActivity.this, "Enter username", Toast.LENGTH_SHORT).show();
        } else if (edt_Password.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(LoginActivity.this, "Enter password", Toast.LENGTH_SHORT).show();
        } else {

            if (Utility.getSharedInstance().isConnectedToInternet(LoginActivity.this)) {

                userLogin();


                // Utilites.getSharedInstance().ShowProgressBar(LogInActivity.this, "Please wait", "Signing in");

                //Login(mEditEmail.getText().toString(), mEditPassword.getText().toString());
                // userLogin();
//                        Intent intent = new Intent(LogInActivity.this, HomeActivity.class);
//                        startActivity(intent);


                // finish();
            } else {
                Utility.getSharedInstance().Snackbar(v, "No internet connection available");
            }


        }
    }


    private void userLogin() {

        Utility.getSharedInstance().showProgressDialog(this);

        startAnim();

//        User_DAO loginDAO = new User_DAO();
////        loginDAO.setEmailId(mEditEmail.getText().toString().trim());
//        loginDAO.setEmail(edt_Email.getText().toString().trim());
//        loginDAO.setX_API_KEY(Config.X_API_KEY);
//        loginDAO.setPassword(edt_Password.getText().toString().trim());
        Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(clientService, X_API_KEY, edt_Email.getText().toString().trim(), edt_Password.getText().toString().trim());
        //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
        callbackLogin.enqueue(new Callback<JsonObject>() {
            @RequiresApi(api = Build.VERSION_CODES.ECLAIR)
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                JsonObject responseData = response.body();


                if (responseData != null) {
                    Utility.getSharedInstance().dismissProgressDialog();

                    if (responseData.get("status").getAsInt() == 203) {
//                        JsonObject userJsonObject = responseData.getAsJsonObject("respon");


                        try
                        {
                            SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_MOB,responseData.get("user_phone").getAsString());
                            SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_FNAME,responseData.get("user_first_name").getAsString());
                            SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_LASTNAMES,responseData.get("user_last_name").getAsString());
                        }
                        catch (Exception e)
                        {

                            e.printStackTrace();

                        }





                        intent = new Intent(LoginActivity.this, OTPActivity.class);
                        intent.putExtra("id",responseData.get("user_id").getAsString());
                        intent.putExtra("otp",responseData.get("user_otp_no").getAsString());
                        SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_USER_EMAIL,responseData.get("user_email").getAsString());
                        startActivity(intent);
                        overridePendingTransition(R.anim.left, R.anim.right);
                        finish();


                    } else if ((responseData.get("status").getAsInt() == 200)) {

                        try {


                            try
                            {
                                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_MOB,responseData.get("user_phone").getAsString());
                                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_FNAME,responseData.get("user_first_name").getAsString());
                                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_LASTNAMES,responseData.get("user_last_name").getAsString());
                            }
                            catch (Exception e)
                            {

                                e.printStackTrace();

                            }

                            if (responseData.get("is_otp_verified").getAsInt() == 1 /*&& responseData.get("is_package_active").getAsInt() == 1*/) {
                                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_IS_LOGIN, "1");

                                if(responseData.has("user_id"))
                                    SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_USER_ID, responseData.get("user_id").getAsString());
                                else if(responseData.has("id"))
                                    SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_USER_ID, responseData.get("id").getAsString());
                                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_USER_EMAIL,responseData.get("user_email").getAsString());
                                intent = new Intent(LoginActivity.this, DashBoardActivity.class);
                                startActivity(intent);

                                overridePendingTransition(R.anim.left, R.anim.right);
                                finish();

                            }
                            /*else {
                                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_USER_EMAIL,responseData.get("user_email").getAsString());
                                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_USER_ID, responseData.get("id").getAsString());
                                intent = new Intent(LoginActivity.this, GetPackageActivity.class);
                                startActivity(intent);
                                overridePendingTransition(R.anim.left, R.anim.right);
                                finish();

                            }*/
                        } catch (Exception e) {






                            e.printStackTrace();
                            SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_USER_EMAIL,responseData.get("user_email").getAsString());
                            SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_USER_ID, responseData.get("user_id").getAsString());
                            intent = new Intent(LoginActivity.this, GetPackageActivity.class);
                            startActivity(intent);
                            overridePendingTransition(R.anim.left, R.anim.right);
                            finish();

                        }


                    }
                  else {
                        Toast.makeText(LoginActivity.this, "Incorrect credentials", Toast.LENGTH_SHORT).show();
                        Utility.getSharedInstance().dismissProgressDialog();
                    }
                } else {
                    Toast.makeText(LoginActivity.this, "Incorrect credentials", Toast.LENGTH_SHORT).show();
                    Utility.getSharedInstance().dismissProgressDialog();

                }


            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Utility.getSharedInstance().dismissProgressDialog();
                Toast.makeText(LoginActivity.this, "Api Crashed or server is down ", Toast.LENGTH_SHORT).show();

                // Utility.getSharedInstance().DismissProgressBar();
                //   Utility.getSharedInstance().Snackbar(toolbarLayout, "Something went wrong!");

            }


        });
    }


    /// Sign Up user


    private void userSIGNUP() {

        Utility.getSharedInstance().showProgressDialog(this);

        startAnim();

        User_DAO loginDAO = new User_DAO();
//        loginDAO.setEmailId(mEditEmail.getText().toString().trim());
        loginDAO.setFirstName(sEditFname);
        loginDAO.setLastName(sEditLname);
        loginDAO.setUsername(sEditUserName);
        loginDAO.setEmail(sEditEmai);
        loginDAO.setMobile(sEditMobile);
        loginDAO.setPassword(sEditPassword);


        Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userSignUp(clientService, X_API_KEY, loginDAO);
        //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
        callbackLogin.enqueue(new Callback<JsonObject>() {
            @RequiresApi(api = Build.VERSION_CODES.ECLAIR)
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                JsonObject responseData = response.body();


                if (responseData != null) {
                    if (responseData.get("status").getAsInt() == 203) {
//                        JsonObject userJsonObject = responseData.getAsJsonObject("respon");

                        Utility.getSharedInstance().dismissProgressDialog();

                        try
                        {
                            SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_USER_EMAIL,mEditEmail.getText().toString());
                            SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_MOB,mEditMobile.getText().toString());
                            SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_FNAME,mEditFname.getText().toString());
                            SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_LASTNAMES,mEditLname.getText().toString());
                        }
                        catch (Exception e)
                        {

                            e.printStackTrace();

                        }


                        intent = new Intent(LoginActivity.this, OTPActivity.class);







                        intent.putExtra("id",responseData.get("user_id").getAsString());
                        intent.putExtra("otp",responseData.get("user_otp_no").getAsString());
                        startActivity(intent);
                        overridePendingTransition(R.anim.left, R.anim.right);
                        finish();

//
                    } else {
                        Toast.makeText(LoginActivity.this, responseData.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                        Utility.getSharedInstance().dismissProgressDialog();
                    }
                } else {
                    Toast.makeText(LoginActivity.this, "Required all details", Toast.LENGTH_SHORT).show();
//                    Utility.getSharedInstance().dismissProgressDialog();
                    Utility.getSharedInstance().dismissProgressDialog();
                }


            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Utility.getSharedInstance().dismissProgressDialog();
                Toast.makeText(LoginActivity.this, "Api error", Toast.LENGTH_SHORT).show();
                // Utility.getSharedInstance().DismissProgressBar();
                //   Utility.getSharedInstance().Snackbar(toolbarLayout, "Something went wrong!");

            }


        });
    }


    /// Forgot Password


    public void forgotPassword(String email) {

        User_DAO user_dao = new User_DAO();


        user_dao.setMobile(email);

        MyApplication myApplication = new MyApplication();
        Utility.getSharedInstance().showProgressDialog(LoginActivity.this);

        if (Utility.getSharedInstance().isConnectedToInternet(LoginActivity.this)) {


            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().forgotPassword(clientService,X_API_KEY,user_dao);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    // JsonObject responseData = response.body();
                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());

                    Utility.getSharedInstance().dismissProgressDialog();
                    if(responseData!=null)
                    {
                        if(responseData.get("status").getAsInt()==203)
                        {

                            dialog.dismiss();
                            Toast.makeText(LoginActivity.this, ""+responseData.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                        }
                        else
                            Toast.makeText(LoginActivity.this, ""+responseData.get("message").getAsString(), Toast.LENGTH_SHORT).show();

                    }


                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Utility.getSharedInstance().dismissProgressDialog();


                    Toast.makeText(LoginActivity.this, "Server error .. Please try again after some time", Toast.LENGTH_SHORT).show();
                }

            });
        } else {
            Toast.makeText(LoginActivity.this, "No network", Toast.LENGTH_SHORT).show();
        }
    }


}
