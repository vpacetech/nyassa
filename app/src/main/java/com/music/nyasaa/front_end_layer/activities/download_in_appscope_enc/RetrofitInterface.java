package com.music.nyasaa.front_end_layer.activities.download_in_appscope_enc;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

public interface RetrofitInterface {

    @GET("DMND%20-%20Navv%20Inder(MyMp3Song).mp3")
    @Streaming
    Call<ResponseBody> downloadFile();



    @GET("music/{token}")
    @Streaming
    Call<ResponseBody> downloadFiles(@Path("token") String file);

    @GET
    @Streaming
    public Call<ResponseBody> downloadFilesURL(@Url String url);


}
