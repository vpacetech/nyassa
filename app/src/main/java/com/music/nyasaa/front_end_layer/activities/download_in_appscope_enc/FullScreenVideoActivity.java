package com.music.nyasaa.front_end_layer.activities.download_in_appscope_enc;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;


import com.music.nyasaa.R;

import android.net.Uri;
import android.widget.MediaController;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

public class FullScreenVideoActivity extends AppCompatActivity {

    private MediaController mediaController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.fullscreen_videoview);
//        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
//
//        TextView mTxtToolbarText = (TextView) findViewById(R.id.txtTitleToolbar);
//        mTxtToolbarText.setText("Downloded Video");
//        LinearLayout imgBackLayout = (LinearLayout) findViewById(R.id.imgBackLayout);
//
//
//        imgBackLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                onBackPressed();
//
//            }
//        });
//
//        videoView = (BetterVideoPlayer) findViewById(R.id.videoView);
//
////        String fullScreen =  getIntent().getStringExtra("fullScreenInd");
////        if("y".equals(fullScreen)){
////            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
////                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
////          //  getSupportActionBar().hide();
//      //  }
//        String fileName="";
//        Intent intent= getIntent();
//        Bundle extras = intent.getExtras();
//        if(extras == null) {
//            fileName=null;
//
//        } else {
//            fileName= extras.getString("path");
//
//        }
//
//        if(fileName!=null) {
//            String url= null;
//            try {
//                url = decrypt(fileName);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//            videoDataNew( url);
////            videoView.setVideoPath(url);
////
////            mediaController = new FullScreenMediaController(this);
////            mediaController.setAnchorView(videoView);
////
////            videoView.setMediaController(mediaController);
////            videoView.start();
//        }
//

    }


    public void videoDataNew(String url) {
//
//
//        videoView.enableSwipeGestures(FullScreenVideoActivity.this.getWindow());
//        videoView.reset();
//        videoView.setSource(Uri.parse(url));
//        //videoView.setSource(Uri.parse("android.resource://" + getActivity().getPackageName() + "/" + R.raw.xp));
//        videoView.setAutoPlay(true);
//
//        videoView.setHideControlsOnPlay(true);
//        // videoView.enableSwipeGestures(context.getWindow());
//       // mLoadingView.setVisibility(View.VISIBLE);
//        videoView.setVisibility(View.VISIBLE);
//
//        Utility.getSharedInstance().dismissProgressDialog();
//
//
//        videoView.getToolbar().inflateMenu(R.menu.exit);
//        videoView.getToolbar().setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
//            @Override
//            public boolean onMenuItemClick(MenuItem item) {
//                switch (item.getItemId()) {
//                    case R.id.exit:
//                        finish();
//                        break;
//
//                }
//                return false;
//            }
//        });
//
//        videoView.setCallback(new BetterVideoCallback() {
//            @Override
//            public void onStarted(BetterVideoPlayer player) {
//                //Log.i(TAG, "Started");
//            }
//
//            @Override
//            public void onPaused(BetterVideoPlayer player) {
//                //Log.i(TAG, "Paused");
////                videoView.start();
//            }
//
//            @Override
//            public void onPreparing(BetterVideoPlayer player) {
//                //Log.i(TAG, "Preparing");
//            }
//
//            @Override
//            public void onPrepared(BetterVideoPlayer player) {
//                //Log.i(TAG, "Prepared");
//            }
//
//            @Override
//            public void onBuffering(int percent) {
//                //Log.i(TAG, "Buffering " + percent);
//            }
//
//            @Override
//            public void onError(BetterVideoPlayer player, Exception e) {
//                //Log.i(TAG, "Error " +e.getMessage());
//            }
//
//            @Override
//            public void onCompletion(BetterVideoPlayer player) {
//                //Log.i(TAG, "Completed");
//            }
//
//            @Override
//            public void onToggleControls(BetterVideoPlayer player, boolean isShowing) {
//
//            }
//        });
////
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.left_anim, R.anim.right_anim);
    }

    String decrypt(String outPaNameth) throws IOException, NoSuchAlgorithmException,
            NoSuchPaddingException, InvalidKeyException {

        File filei = new File(getApplicationContext().getFilesDir(), "tempfilenametoplay");

        String     inPath=filei.getAbsolutePath();
        // deleteit

        File fileo = new File(getApplicationContext().getFilesDir(), outPaNameth);
//
        String   outPath=fileo.getAbsolutePath();

        FileInputStream fis = new FileInputStream(outPath);
        FileOutputStream fos = new FileOutputStream(inPath);
        SecretKeySpec sks = new SecretKeySpec("ABCDEFdhdhdhghgfghfghgfg".getBytes(),
                "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, sks);
        CipherInputStream cis = new CipherInputStream(fis, cipher);
        int b;
        byte[] d = new byte[1024];
        while ((b = cis.read(d)) != -1) {
            fos.write(d, 0, b);
        }
        fos.flush();
        fos.close();
        cis.close();
        inPath= String.valueOf(Uri.fromFile(filei));

        return inPath;
    }
}