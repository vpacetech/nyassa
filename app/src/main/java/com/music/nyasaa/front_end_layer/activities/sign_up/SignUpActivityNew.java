package com.music.nyasaa.front_end_layer.activities.sign_up;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;


import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import com.music.nyasaa.R;
import com.music.nyasaa.adapter.CityAdapter;
import com.music.nyasaa.adapter.CountryAdapter;
import com.music.nyasaa.controller.MyApplication;
import com.music.nyasaa.fragments.terms_and_conditions.ActivityTermsAndConditions;
import com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity;
import com.music.nyasaa.front_end_layer.activities.login.LoginActivity;
import com.music.nyasaa.models.OshoCenterDAO;
import com.music.nyasaa.models.ResponseModel.GetImageUrl.GetImageResponse;
import com.music.nyasaa.models.ResponseModel.GetImageUrl.GetImageResponseData;
import com.music.nyasaa.models.User_DAO;
import com.music.nyasaa.utility.CommonUtils;
import com.music.nyasaa.utility.SharedPreferenceManager;
import com.music.nyasaa.utility.Utility;
import com.music.nyasaa.widgets.edittext.CustomEdittextRegular;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.listeners.IPickCancel;
import com.vansuita.pickimage.listeners.IPickResult;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.music.nyasaa.retrofit.Config.X_API_KEY;
import static com.music.nyasaa.retrofit.Config.clientService;


public class SignUpActivityNew extends AppCompatActivity implements View.OnClickListener {

    private Button mButBack, mButSignUp, mButAvatar;
    private ImageView setImage;
    private TextView mTxtTerms;
    private RequestBody mFile;
    private Intent intent;
    CustomEdittextRegular edt_User_Name, edt_Email, edt_Password, mEditCity, mEditSanaysName, mEditConatc;
    MyApplication myApplication;
    private String getPath = "";
    private int PERMISSION_ALL = 1;
    private String imgURL = "";
    private String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
    private TextView mTxtMember, mTxtRequest, mTxtPost;
    private LinearLayout mLayMembers, mLayRequest, mLayPost;
    private WebView mWebview;
    private Spinner spCountry, spState, spCity;
    String strCountryName = "";
    String strCountryId = "";
    String strStateName = "";
    String strStateId = "";
    String strCityName = "";
    String strCityId = "";
    OshoCenterDAO oshoCenterDAO;
    AutoCompleteTextView atxtCity;

    /* GetAllCountryResponseData getAllCountryResponseData;
     GetAllCountryDetails getAllCountryDetails;
     GetAllStatesByCountryData getAllStatesByCountryData;
 */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_new);
        myApplication = new MyApplication();
        initView();

        oshoCenterDAO = new OshoCenterDAO();

        GetAllCountry();

        onClick();
    }

    private void initView() {

        mButBack = (Button) findViewById(R.id.butBack);
        mTxtTerms = (TextView) findViewById(R.id.txtTerms);
        mButSignUp = (Button) findViewById(R.id.butSignUp);
        mButAvatar = (Button) findViewById(R.id.butAvatar);
        setImage = (ImageView) findViewById(R.id.setImage);
        edt_User_Name = (CustomEdittextRegular) findViewById(R.id.edt_User_Name);
        edt_Email = (CustomEdittextRegular) findViewById(R.id.edt_Email);
        edt_Password = (CustomEdittextRegular) findViewById(R.id.edt_Password);

        mEditCity = (CustomEdittextRegular) findViewById(R.id.edt_City);
        mEditSanaysName = (CustomEdittextRegular) findViewById(R.id.edit_SanaysName);
        mEditConatc = (CustomEdittextRegular) findViewById(R.id.edt_Mobile);

        spCountry = findViewById(R.id.spCountry);
        spState = findViewById(R.id.spState);
        spCity = findViewById(R.id.spCity);
        atxtCity = findViewById(R.id.atxtCity);


        atxtCity.addTextChangedListener(new TextWatcher() {
            int PreviousLength;
            boolean backspace;

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                PreviousLength = charSequence.length();

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void afterTextChanged(Editable editable) {
                backspace = PreviousLength > editable.length();

                if (backspace) {
                    atxtCity.setText("");
                }

            }
        });



       /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            mTxtTerms.setText(Html.fromHtml("<p>By creating account, you accept our <b style=color:blue;>T&C</b></p>", Html.FROM_HTML_MODE_COMPACT));
        } else {
            mTxtTerms.setText(Html.fromHtml("<p>By creating account, you accept our <b style=color:blue;>T&C</b></p>"));
        }*/
        Spannable word = new SpannableString("By creating account, you accept our ");
        word.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorPrimary)), 0, word.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mTxtTerms.setText(word);
        Spannable word1 = new SpannableString("T&C");
        word1.setSpan(new ForegroundColorSpan(Color.WHITE), 0, word1.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mTxtTerms.append(word1);
    }


    private void onClick() {
        mButSignUp.setOnClickListener(this);
        mButBack.setOnClickListener(this);
        mButAvatar.setOnClickListener(this);
        mTxtTerms.setOnClickListener(this);
        //   spCountry.setOnClickListener(this);
        //  spCountry.setOnItemClickListener(this);
        //  spState.setOnClickListener(this);
        //  spState.setOnItemClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.butBack:
                intent = new Intent(SignUpActivityNew.this, LoginActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.left_anim, R.anim.right_anim);
                finish();
                break;

            case R.id.butSignUp:
                validationForLogin(v);
                overridePendingTransition(R.anim.left, R.anim.right);
                // finish();
                break;

            case R.id.butAvatar:
                uploadAvatar();
                break;

            case R.id.txtTerms:
                //dialogTermsAndConditions();
                Intent intent = new Intent(SignUpActivityNew.this, ActivityTermsAndConditions.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        intent = new Intent(SignUpActivityNew.this, LoginActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.left, R.anim.right);
        finish();
    }

    public static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    private void validationForLogin(View v) {


        if (edt_User_Name.getText().toString().trim().equalsIgnoreCase("")) {

            Toast.makeText(SignUpActivityNew.this, "Enter user name", Toast.LENGTH_SHORT).show();
        } else if (edt_Email.getText().toString().trim().equalsIgnoreCase("") || !isValidEmail(edt_Email.getText().toString())) {
//            Utility.getSharedInstance().Snackbar(v, "Please Enter Valid Email");
            Toast.makeText(SignUpActivityNew.this, "Enter valid email", Toast.LENGTH_SHORT).show();
        } else if (edt_Password.getText().toString().trim().equalsIgnoreCase("")) {

//            Utility.getSharedInstance().Snackbar(v, "Please Enter Your Password");

            Toast.makeText(SignUpActivityNew.this, "Enter password", Toast.LENGTH_SHORT).show();

        } else if ( mEditConatc.getText().toString().trim().length() < 6) {
            Toast.makeText(SignUpActivityNew.this, "Enter valid number", Toast.LENGTH_SHORT).show();
        }


        else if (spCountry.getSelectedItemPosition() == 0) {
            Toast.makeText(SignUpActivityNew.this, "Please select country", Toast.LENGTH_SHORT).show();
        }


        else if (spState.getSelectedItemPosition() == 0) {
            Toast.makeText(SignUpActivityNew.this, "Please select  state", Toast.LENGTH_SHORT).show();
        }


        else if (atxtCity.getText().toString().trim().equalsIgnoreCase("")) {

//            Utility.getSharedInstance().Snackbar(v, "Please Enter Your Password");

            Toast.makeText(SignUpActivityNew.this, "Enter city", Toast.LENGTH_SHORT).show();

        }

        /*else if (mEditCity.getText().toString().trim().equalsIgnoreCase("")) {

//            Utility.getSharedInstance().Snackbar(v, "Please Enter Your Password");

            Toast.makeText(SignUpActivityNew.this, "Enter City", Toast.LENGTH_SHORT).show();

        }*/

        else {
            if (Utility.getSharedInstance().isConnectedToInternet(SignUpActivityNew.this)) {
                if (!getPath.equals("")) {
                    sendFile();
                } else {
                    userLogin();
                }
            } else {
                Toast.makeText(SignUpActivityNew.this, "Check your internet connection", Toast.LENGTH_SHORT).show();
            }
        }
    }


    private void userLogin() {

        if (Utility.getSharedInstance().isConnectedToInternet(this)) {
            Utility.getSharedInstance().dismissProgressDialog();
            Utility.getSharedInstance().showProgressDialog(this);
            User_DAO loginDAO = new User_DAO();
//        loginDAO.setEmailId(mEditEmail.getText().toString().trim());
            loginDAO.setEmail(edt_Email.getText().toString().trim());
            loginDAO.setX_API_KEY(X_API_KEY);
            loginDAO.setUsername(edt_User_Name.getText().toString());
            loginDAO.setPassword(edt_Password.getText().toString().trim());
            if (!strCityName.equals("")) {
                loginDAO.setCity(strCityName);
            } else if (!atxtCity.getText().equals("")) {
                loginDAO.setCity(atxtCity.getText().toString().trim());
            } else {
                loginDAO.setCity("");
            }

            loginDAO.setSanyas_name(mEditSanaysName.getText().toString().trim());

            if (mEditConatc.getText().toString().trim().length() > 0 && mEditConatc.getText().toString().trim().length() < 10) {
                Toast.makeText(SignUpActivityNew.this, "Enter valid number", Toast.LENGTH_SHORT).show();
            } else {
                loginDAO.setPhone_number(mEditConatc.getText().toString().trim());
            }

            loginDAO.setProfile_photo(imgURL);
            loginDAO.setCountryId(strCountryId);
            loginDAO.setStateId(strStateId);


            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userSignUp(clientService,X_API_KEY,loginDAO);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    JsonObject responseData = response.body();


                    if (responseData.get("message").getAsInt() == 0) {

                        Utility.getSharedInstance().dismissProgressDialog();

                        Toast.makeText(SignUpActivityNew.this, "Email already exists", Toast.LENGTH_SHORT).show();
                    } else {
                        Utility.getSharedInstance().dismissProgressDialog();
                        SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_USER_NAME, edt_User_Name.getText().toString().trim());
                        SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_USER_ID, String.valueOf(responseData.get("message").getAsInt()));

                        SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_PROFILE_PHOTO, imgURL);
                        SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_IS_LOGIN, "1");
                        Intent intent = new Intent(SignUpActivityNew.this, DashBoardActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Toast.makeText(SignUpActivityNew.this, "Api issue", Toast.LENGTH_SHORT).show();
                    Utility.getSharedInstance().dismissProgressDialog();
                    //   Utility.getSharedInstance().Snackbar(toolbarLayout, "Something went wrong!");
                }

            });
        } else {
            Toast.makeText(SignUpActivityNew.this, "No internet connection", Toast.LENGTH_SHORT).show();

        }
    }

    public void uploadAvatar() {
        getPath = "";
        if (!Utility.hasPermissions(SignUpActivityNew.this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(SignUpActivityNew.this, PERMISSIONS, PERMISSION_ALL);
        } else {
            PickImageDialog.build(new PickSetup())
                    .setOnPickResult(new IPickResult() {
                        @Override
                        public void onPickResult(PickResult r) {

                            try {
                                setImage.setImageURI(null);

                                //Setting the real returned image.
                                setImage.setImageURI(r.getUri());

                                //PickImageDialog.build().dismiss();
                                //If you want the Bitmap.
                                setImage.setImageBitmap(r.getBitmap());
                                setImage.setVisibility(View.VISIBLE);


                                // bitmapToBase64(r.getBitmap());
                                //getPath(r.getUri());
                                getPath = r.getPath();

//                                sendFile();

                                // videoFilePath = (r.getUri()).getData();
                                //videoFilePath=(r.getUri()).getPath();
//
//                                //PickImageDialog.build().dismiss();
//                                //If you want the Bitmap.
//                                imageView.setImageBitmap(r.getBitmap());
                                // Toast.makeText(AddPostActivity.this, r.getError().getMessage(), Toast.LENGTH_LONG).show();
                            } catch (OutOfMemoryError | Exception e) {
                                e.printStackTrace();
                            }
                        }
                    })
                    .setOnPickCancel(new IPickCancel() {
                        @Override
                        public void onCancelClick() {

                        }
                    }).show(SignUpActivityNew.this);
        }
    }

    private void sendFile() {

        myApplication = new MyApplication();
        Utility.getSharedInstance().showProgressDialog(SignUpActivityNew.this);

        File file = new File(getPath);

        if (getPath != null && !getPath.equals("")) {
            file = new File(CommonUtils.compressImage(getPath));
            if (file == null)
                file = new File(getPath);
        }

        mFile = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("file", file.getName(), mFile);

        Call<JsonObject> callbackLogin = (Call<JsonObject>) myApplication.getAPIInstance().uploadSignUp(X_API_KEY, fileToUpload, mFile);
        callbackLogin.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                JsonObject responseData = response.body();

                if (responseData != null) {
                    Gson gson = new Gson();
                    GetImageResponse getImageResponse = gson.fromJson(responseData, GetImageResponse.class);
                    GetImageResponseData getImageResponseData = getImageResponse.getUploadData();

                    imgURL = getImageResponseData.getFileName();
                    userLogin();
                    Utility.getSharedInstance().dismissProgressDialog();
                } else {
                    Utility.getSharedInstance().dismissProgressDialog();
                    Toast.makeText(SignUpActivityNew.this, "Failed to attach image", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Utility.getSharedInstance().dismissProgressDialog();
                Toast.makeText(SignUpActivityNew.this, "Excepion Error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void dialogTermsAndConditions() {
        Dialog dialogTerms = new Dialog(SignUpActivityNew.this);
        dialogTerms.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogTerms.setContentView(R.layout.fragment_terms_and_conditions);
        dialogTerms.getWindow().getAttributes().width = ActionBar.LayoutParams.FILL_PARENT;
        dialogTerms.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        dialogTerms.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogTerms.show();

        mWebview = (WebView) dialogTerms.findViewById(R.id.webView);
        mLayMembers = (LinearLayout) dialogTerms.findViewById(R.id.layMember);
        mLayRequest = (LinearLayout) dialogTerms.findViewById(R.id.layRequest);
        mLayPost = (LinearLayout) dialogTerms.findViewById(R.id.layPost);
        mTxtMember = (TextView) dialogTerms.findViewById(R.id.txtMembers);
        mTxtRequest = (TextView) dialogTerms.findViewById(R.id.txtRequest);
        mTxtPost = (TextView) dialogTerms.findViewById(R.id.txtPost);


       /* WebView mWebview = (WebView) dialogTerms.findViewById(R.id.webView);
        mWebview.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);

        mWebview.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view, String url) {
                Utility.getSharedInstance().dismissProgressDialog();
            }
        });
        String URL = "file:///android_asset/terms.html";
        mWebview.loadUrl(URL);
        Utility.getSharedInstance().showProgressDialog(SignUpActivityNew.this);*/
    }


    private void GetAllCountry() {
        Call<JsonObject> callbackLogin = myApplication.getAPIInstance().getAllCountries(X_API_KEY);
        //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
        callbackLogin.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                JsonObject responseData = response.body();
                if (responseData != null)
                    if (responseData.get("status").getAsBoolean() == true) {

                        Gson gson = new Gson();

                        OshoCenterDAO getAllCountryResponseData = gson.fromJson(responseData, OshoCenterDAO.class);

                        List<OshoCenterDAO> getAllCountryDetailsList = getAllCountryResponseData.getResultObject();
                        oshoCenterDAO.setId("-1");
                        oshoCenterDAO.setName("Select Country");
                        getAllCountryDetailsList.add(oshoCenterDAO);
                        Collections.reverse(getAllCountryDetailsList);
                        setCountry(getAllCountryDetailsList);

                      /*  GetAllCountryResponseData getAllCountryResponseData=gson.fromJson(responseData, GetAllCountryResponseData.class);

                        List<GetAllCountryDetails> getAllCountryDetailsList=getAllCountryResponseData.getResultObject();
*/

                    } else if (responseData.get("status").getAsBoolean() == false) {
                        // Utility.getSharedInstance().DismissProgressBar();
//                        Toast.makeText(SignUpActivityNew.this, "NULL", Toast.LENGTH_LONG).show();
                    } else {

                    }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                //Utility.getSharedInstance().DismissProgressBar();
                Toast.makeText(SignUpActivityNew.this, "Something went wrong!", Toast.LENGTH_LONG).show();
            }

        });
    }


    private void setCountry(final List<OshoCenterDAO> getAllCountryDetailsList) {

        CountryAdapter countryAdapter = new CountryAdapter(SignUpActivityNew.this, getAllCountryDetailsList);
        spCountry.setAdapter(countryAdapter);

        spCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                strCountryName = getAllCountryDetailsList.get(i).getName();
                strCountryId = getAllCountryDetailsList.get(i).getId();

                GetAllStatesByCountry(strCountryId);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }


    private void GetAllStatesByCountry(String id) {
        try {
            oshoCenterDAO.setId(id);

        } catch (Exception e) {
            e.printStackTrace();
        }

        Call<JsonObject> callbackLogin = myApplication.getAPIInstance().getAllStateByCountryId(X_API_KEY, oshoCenterDAO);
        callbackLogin.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                JsonObject responseData = response.body();
                // Log.e("Response::", responseData.toString());
                if (responseData != null)
                    if (responseData.get("status").getAsBoolean() == true) {
                        Gson gson = new Gson();

                        OshoCenterDAO getAllSateResponseData = gson.fromJson(responseData, OshoCenterDAO.class);

                        List<OshoCenterDAO> getAllStateDetailsList= new ArrayList<>();


                        getAllStateDetailsList = getAllSateResponseData.getResultObject();
                        oshoCenterDAO.setId("-1");
                        oshoCenterDAO.setName("Select State");
                        getAllStateDetailsList.add(oshoCenterDAO);
                        Collections.reverse(getAllStateDetailsList);
                        setStates(getAllStateDetailsList);

                    } else if (responseData.get("status").getAsBoolean() == false) {
                        //  Utility.getSharedInstance().DismissProgressBar();
//                        Toast.makeText(SignUpActivityNew.this, "NULL", Toast.LENGTH_LONG).show();

                    } else {

                    }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                //Utility.getSharedInstance().DismissProgressBar();
                Toast.makeText(SignUpActivityNew.this, "Something went wrong!", Toast.LENGTH_LONG).show();

            }

        });
    }


    private void setStates(final List<OshoCenterDAO> getAllStateList) {
        CountryAdapter countryAdapter = new CountryAdapter(SignUpActivityNew.this, getAllStateList);
        spState.setAdapter(countryAdapter);

        spState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                strStateName = getAllStateList.get(i).getName();
                strStateId = getAllStateList.get(i).getId();

                GetAllCityByStateId(strStateId);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }


    private void GetAllCityByStateId(String id) {
        try {
            oshoCenterDAO.setId(id);

        } catch (Exception e) {
            e.printStackTrace();
        }

        Call<JsonObject> callbackLogin = myApplication.getAPIInstance().getAllCityByStateId(X_API_KEY, oshoCenterDAO);
        callbackLogin.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                JsonObject responseData = response.body();
                // Log.e("Response::", responseData.toString());
                if (responseData != null)
                    if (responseData.get("status").getAsBoolean() == true) {
                        Gson gson = new Gson();

                        OshoCenterDAO getAllCityResponseData = gson.fromJson(responseData, OshoCenterDAO.class);

                        List<OshoCenterDAO> getAllCityDetailsList = getAllCityResponseData.getResultObject();

                        //  setCities(getAllCityDetailsList);

                        setCity(getAllCityDetailsList);


                    } else if (responseData.get("status").getAsBoolean() == false) {
                        //  Utility.getSharedInstance().DismissProgressBar();
//                        Toast.makeText(SignUpActivityNew.this, "NULL", Toast.LENGTH_LONG).show();

                    } else {

                    }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                //Utility.getSharedInstance().DismissProgressBar();
                Toast.makeText(SignUpActivityNew.this, "Something went wrong!", Toast.LENGTH_LONG).show();

            }

        });
    }


    private void setCities(final List<OshoCenterDAO> getAllCityList) {
        CountryAdapter countryAdapter = new CountryAdapter(SignUpActivityNew.this, getAllCityList);
        spCity.setAdapter(countryAdapter);

        spCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                strCityName = getAllCityList.get(i).getName();
                strCityId = getAllCityList.get(i).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }


    private void setCity(final List<OshoCenterDAO> getAllCityList) {
        CityAdapter cityAdapter = new CityAdapter(SignUpActivityNew.this, getAllCityList);
        atxtCity.setAdapter(cityAdapter);

        atxtCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                strCityName = getAllCityList.get(i).getName();
                strCityId = getAllCityList.get(i).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }


}