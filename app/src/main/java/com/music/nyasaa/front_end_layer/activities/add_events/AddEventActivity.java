package com.music.nyasaa.front_end_layer.activities.add_events;

import android.Manifest;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.music.nyasaa.R;
import com.music.nyasaa.controller.MyApplication;
import com.music.nyasaa.fragments.DatePickerFragment;
import com.music.nyasaa.models.RequestModel.AddEventRequest;
import com.music.nyasaa.models.ResponseModel.GetImageUrl.GetImageResponse;
import com.music.nyasaa.models.ResponseModel.GetImageUrl.GetImageResponseData;
import com.music.nyasaa.models.UpdateEventRequest;

import com.music.nyasaa.utility.CommonUtils;
import com.music.nyasaa.utility.SharedPreferenceManager;
import com.music.nyasaa.utility.Utility;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.listeners.IPickCancel;
import com.vansuita.pickimage.listeners.IPickResult;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity.getEventResponseData;
import static com.music.nyasaa.retrofit.Config.X_API_KEY;
import static com.music.nyasaa.utility.SharedPreferenceManager.CLICKEDIT;

public class AddEventActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText edtEventName;
    private EditText edtEventAddress;
    private TextView txtEventStartDate;
    private TextView txtEventEndDate;
    private EditText edtPhoto;
    private Button btnSubmit;
    private String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
    private int PERMISSION_ALL = 1;
    private String getPath = "";
    private ImageView setImage;
    private MyApplication myApplication;
    private RequestBody mFile;
    private String imgURL = "";
    String strStartDate = "";
    String strEndDate = "";
    String isStartDate = "-1", isDatevalid = "-2";
    String eventName = "";
    String eventAddress = "";
    String eventStartDate = "";
    String eventEndDate = "";
    String eventImage = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_event);
        // Call the function callInstamojo to start payment here
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView mTxtToolbarText = (TextView) findViewById(R.id.txtTitleToolbar);


        if (CLICKEDIT == 0) {
            mTxtToolbarText.setText("Update event");
        } else {
            mTxtToolbarText.setText("Add event");
        }


        edtEventName = findViewById(R.id.edtEventName);
        edtEventAddress = findViewById(R.id.edtEventAddr);
        txtEventStartDate = findViewById(R.id.txtStartDate);
        txtEventEndDate = findViewById(R.id.txtEndDate);
        edtPhoto = findViewById(R.id.edtPhoto);
        btnSubmit = findViewById(R.id.btnSubmit);
        setImage = findViewById(R.id.setImage);

        btnSubmit.setOnClickListener(this);
        edtPhoto.setOnClickListener(this);
        txtEventStartDate.setOnClickListener(this);
        txtEventEndDate.setOnClickListener(this);


        myApplication = new MyApplication();

        if (CLICKEDIT == 0) {

            btnSubmit.setText("Update");

            edtEventName.setText(getEventResponseData.getName().toString().trim());
            edtEventAddress.setText(getEventResponseData.getAddress().toString().trim());
            txtEventStartDate.setText(getEventResponseData.getFromDate().toString().trim());
            txtEventEndDate.setText(getEventResponseData.getToDate().toString().trim());
            setImage.setVisibility(View.VISIBLE);
//            Picasso.with(AddEventActivity.this).load(Config.PIC_URL_EVENT + Uri.encode(getEventResponseData.getImage())).error(R.drawable.default_music).into(setImage);
        }


        LinearLayout imgBackLayout = (LinearLayout) findViewById(R.id.imgBackLayout);
        imgBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();

            }
        });

    }

    @Override
    public void onClick(View view) {

        Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dialog = null;

        switch (view.getId()) {


            case R.id.btnSubmit:

                if (edtEventName.getText().toString().trim().equalsIgnoreCase("")) {

                    Toast.makeText(this, "Enter event name", Toast.LENGTH_SHORT).show();
                } else if (edtEventAddress.getText().toString().trim().equalsIgnoreCase("")) {

                    Toast.makeText(this, "Enter event address", Toast.LENGTH_SHORT).show();
                } else if (txtEventStartDate.getText().toString().trim().equalsIgnoreCase("")) {

                    Toast.makeText(this, "Enter start date", Toast.LENGTH_SHORT).show();
                } else if (txtEventEndDate.getText().toString().trim().equalsIgnoreCase("")) {

                    Toast.makeText(this, "Enter end date", Toast.LENGTH_SHORT).show();
                } else if (!getPath.equals("")) {
                    sendFile();
                } else {
                    if (CLICKEDIT == 0) {
                        updateEvent();
                        CLICKEDIT = 1;
                    } else {
                        addEvent();
                    }
                }


                break;


            case R.id.edtPhoto:

                getPath = "";
                if (!Utility.hasPermissions(AddEventActivity.this, PERMISSIONS)) {
                    ActivityCompat.requestPermissions(AddEventActivity.this, PERMISSIONS, PERMISSION_ALL);
                } else {
                    PickImageDialog.build(new PickSetup())
                            .setOnPickResult(new IPickResult() {
                                @Override
                                public void onPickResult(PickResult r) {

                                    try {
                                        setImage.setImageURI(null);

                                        //Setting the real returned image.
                                        setImage.setImageURI(r.getUri());

                                        //PickImageDialog.build().dismiss();
                                        //If you want the Bitmap.
                                        setImage.setImageBitmap(r.getBitmap());
                                        setImage.setVisibility(View.VISIBLE);


                                        // bitmapToBase64(r.getBitmap());
                                        //getPath(r.getUri());
                                        getPath = r.getPath();

//                                sendFile();

                                        // videoFilePath = (r.getUri()).getData();
                                        //videoFilePath=(r.getUri()).getPath();
//
//                                //PickImageDialog.build().dismiss();
//                                //If you want the Bitmap.
//                                imageView.setImageBitmap(r.getBitmap());
                                        // Toast.makeText(AddPostActivity.this, r.getError().getMessage(), Toast.LENGTH_LONG).show();
                                    } catch (OutOfMemoryError | Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            })
                            .setOnPickCancel(new IPickCancel() {
                                @Override
                                public void onCancelClick() {

                                }
                            }).show(AddEventActivity.this);
                }
                break;


            case R.id.txtStartDate:

               /* isStartDate="1";
                showDatePicker();*/

                DatePickerDialog.OnDateSetListener dpd = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {

                        int s = monthOfYear + 1;
                        String month = String.valueOf(s);
                        if (month.length() < 2) {
                            month = "0" + month;
                        }

                        String dayDate = String.valueOf(dayOfMonth);


//
//
                        if (dayDate.length() < 2) {
                            dayDate = "0" + dayDate;
                        }
                        String a = year + "-" + month + "-" + dayDate;
                        txtEventStartDate.setText("" + a);
                        strStartDate = a;


                        //getVolunteers(a);
                        //getSupllies(a);


                        //  sDate = a;
                    }
                };

                c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                dialog =
                        new DatePickerDialog(AddEventActivity.this, R.style.DialogTheme, dpd, mYear, mMonth, mDay);

                dialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                dialog.show();

                break;

            case R.id.txtEndDate:

              /*  isStartDate="0";
                showDatePicker();*/


                DatePickerDialog.OnDateSetListener dpd1 = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {

                        int s = monthOfYear + 1;
                        String month = String.valueOf(s);
                        if (month.length() < 2) {
                            month = "0" + month;
                        }

                        String dayDate = String.valueOf(dayOfMonth);


//
//
                        if (dayDate.length() < 2) {
                            dayDate = "0" + dayDate;
                        }
                        String a = year + "-" + month + "-" + dayDate;
                        strEndDate = a;

                        if (CheckDates(strStartDate, strEndDate)) {
                            txtEventEndDate.setText("" + a);
                        } else {
                            Toast.makeText(AddEventActivity.this, "Please ensure end date is not less than start date ", Toast.LENGTH_LONG).show();
                        }

                    }
                };


                c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                dialog =
                        new DatePickerDialog(AddEventActivity.this, R.style.DialogTheme, dpd1, mYear, mMonth, mDay);
                dialog.show();

                break;


        }

    }


    private void sendFile() {
        //File compressedImageFile=null;
        myApplication = new MyApplication();


        Utility.getSharedInstance().showProgressDialog(this);


        //Utility.getSharedInstance().ShowProgessDialog(AddPostActivity.this, "Attaching your file", "Please wait");

        File file = new File(getPath);

        if (getPath != null && !getPath.equals("")) {

            file = new File(CommonUtils.compressImage(getPath));

            if (file == null)
                file = new File(getPath);
        }

        mFile = RequestBody.create(MediaType.parse("image/*"), file);


        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("file", file.getName(), mFile);


        Call<JsonObject> callbackLogin = (Call<JsonObject>) myApplication.getAPIInstance().uploadEventImage(X_API_KEY, fileToUpload, mFile);
        callbackLogin.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                JsonObject responseData = response.body();


                if (responseData != null) {


                    Gson gson = new Gson();

                    GetImageResponse getImageResponse = gson.fromJson(responseData, GetImageResponse.class);
                    GetImageResponseData getCommunityResponseData = getImageResponse.getUploadData();


                    imgURL = getCommunityResponseData.getFileName();

                    if (CLICKEDIT == 0) {
                        updateEvent();
                    } else {
                        addEvent();
                    }


                    //Utility.getSharedInstance().DisMissProgressDialog();


                } else {
                    Utility.getSharedInstance().dismissProgressDialog();
                    //sendPost();
                    Toast.makeText(AddEventActivity.this, "Failed to attach image", Toast.LENGTH_SHORT).show();

                }


            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Utility.getSharedInstance().dismissProgressDialog();
                //sendPost();

                Toast.makeText(AddEventActivity.this, "Excepion Error", Toast.LENGTH_SHORT).show();
            }


        });

    }


    public void addEvent() {


        if (Utility.getSharedInstance().isConnectedToInternet(AddEventActivity.this)) {
//            Utility.getSharedInstance().showProgressDialog(this);
            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");


            myApplication = new MyApplication();

            AddEventRequest addEventRequest = new AddEventRequest();

            addEventRequest.setUserId(id);
            addEventRequest.setName(edtEventName.getText().toString().trim());
            addEventRequest.setAddress(edtEventAddress.getText().toString().trim());
            addEventRequest.setFromDate(txtEventStartDate.getText().toString().trim());
            addEventRequest.setToDate(txtEventEndDate.getText().toString().trim());
            addEventRequest.setImage(imgURL);

            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().addEvent(X_API_KEY, addEventRequest);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());

                    if (responseData != null)

                    {
                        Utility.getSharedInstance().dismissProgressDialog();

                        if (responseData.get("status").getAsString().equalsIgnoreCase("false")) {


                            Utility.getSharedInstance().dismissProgressDialog();
                            Toast.makeText(AddEventActivity.this, "" + responseData.get("message").getAsString(), Toast.LENGTH_SHORT).show();


                        } else {
                            Utility.getSharedInstance().dismissProgressDialog();
                            Toast.makeText(AddEventActivity.this, "Event added to list", Toast.LENGTH_SHORT).show();
                            finish();

                        }

                    } else {
                        Utility.getSharedInstance().dismissProgressDialog();
                        Toast.makeText(AddEventActivity.this, "" + responseData.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Toast.makeText(AddEventActivity.this, "failed", Toast.LENGTH_SHORT).show();

                    Utility.getSharedInstance().dismissProgressDialog();
                }


            });
        } else {

            Utility.getSharedInstance().dismissProgressDialog();

            Toast.makeText(this, "No internet connection available", Toast.LENGTH_SHORT).show();
        }


    }


    private void showDatePicker() {
        DatePickerFragment date = new DatePickerFragment();
        Calendar calender = Calendar.getInstance();
        Bundle args = new Bundle();
        args.putInt("year", calender.get(Calendar.YEAR));
        args.putInt("month", calender.get(Calendar.MONTH));
        args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
        date.setArguments(args);
        date.setCallBack(ondate);
        date.show(getFragmentManager(), "Date Picker");
    }


    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            String month = "";
            String day = "";

            month = (monthOfYear + 1) < 10 ? "0" + (monthOfYear + 1) : "" + (monthOfYear + 1);

            day = dayOfMonth < 10 ? "0" + dayOfMonth : "" + dayOfMonth;

            String Date = (year + "-"
                    + month + "-" + day);

            dateValidation(Date);

            if (isDatevalid.equalsIgnoreCase("-1")) {
                if (isStartDate.equalsIgnoreCase("1")) {
                    String strEnd = txtEventEndDate.getText().toString().trim();
                    if (!strEnd.equalsIgnoreCase("")) {
                        compareDate(Date, strEnd);
                    } else {
                        isDatevalid = "-1";
                    }
                    if (isDatevalid.equalsIgnoreCase("-1")) {
                        txtEventStartDate.setText(setDateToTextview(Date));
                        strStartDate = Date;
                    } else {
                        txtEventStartDate.setText("");
                        Toast.makeText(AddEventActivity.this, getApplicationContext().getString(R.string.msgstartdategreaterstart), Toast.LENGTH_SHORT).show();
                    }
                } else if (isStartDate.equalsIgnoreCase("0")) {
                    String strstart = txtEventStartDate.getText().toString().trim();

                    if (!strstart.equalsIgnoreCase("")) {
                        compareDate(Date, strstart);
                    } else {
                        isDatevalid = "1";
                    }

                    if (isDatevalid.equalsIgnoreCase("1")) {
                        txtEventEndDate.setText(setDateToTextview(Date));
                        strEndDate = Date;
                    } else {
                        txtEventEndDate.setText("");
                        Toast.makeText(AddEventActivity.this, getApplicationContext().getString(R.string.msgstartdatelessend), Toast.LENGTH_SHORT).show();
                    }
                }
            } else {

            }
        }
    };

    private void dateValidation(String selectedDate) {
        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String todaysDate = df.format(c.getTime());
        String sDate = selectedDate;
        compareDate(sDate, todaysDate);
    }

    public void compareDate(String dat1, String dat2) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date date1 = sdf.parse(dat1);
            SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MMM-yyyy");
            Date date2 = sdf2.parse(dat2);

            if (date1.compareTo(date2) > 0) {
                isDatevalid = "1";
                Log.v("app", "Date1 is after Date2");
            } else if (date1.compareTo(date2) < 0) {
                isDatevalid = "-1";
                Log.v("app", "Date1 is before Date2");
            } else if (date1.compareTo(date2) == 0) {
                isDatevalid = "0";
                Log.v("app", "Date1 is equal to Date2");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String setDateToTextview(String Date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        String reportDate = null;
        try {
            date = format.parse(Date);
            DateFormat dff = new SimpleDateFormat("dd-MMM-yyyy");
            reportDate = dff.format(date);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        // strDateArray[k]=String.valueOf(date.sub);
        return reportDate;
    }


    static SimpleDateFormat dfDate = new SimpleDateFormat("yyyy-MM-dd");

    public static boolean CheckDates(String dat1, String dat2) {
        boolean b = false;
        try {
            if (dfDate.parse(dat1).before(dfDate.parse(dat2))) {
                b = true;//If start date is before end date
            } else if (dfDate.parse(dat1).equals(dfDate.parse(dat2))) {
                b = false;//If two dates are equal
            } else {
                b = false; //If start date is after the end date
            }
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return b;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        CLICKEDIT = 1;

        finish();
        overridePendingTransition(R.anim.left_anim, R.anim.right_anim);
    }


    public void updateEvent() {
        if (Utility.getSharedInstance().isConnectedToInternet(AddEventActivity.this)) {
//            Utility.getSharedInstance().showProgressDialog(this);
            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");

            String eventId = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_EVENT_ID, "");


            myApplication = new MyApplication();

            UpdateEventRequest updateEventRequest = new UpdateEventRequest();

            updateEventRequest.setUserId(id);
            updateEventRequest.setEventId(eventId);
            updateEventRequest.setName(edtEventName.getText().toString().trim());
            updateEventRequest.setAddress(edtEventAddress.getText().toString().trim());
            updateEventRequest.setFromDate(txtEventStartDate.getText().toString().trim());
            updateEventRequest.setToDate(txtEventEndDate.getText().toString().trim());
            updateEventRequest.setImage(imgURL);

            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().updatEevent(X_API_KEY, updateEventRequest);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());

                    if (responseData != null)

                    {
                        Utility.getSharedInstance().dismissProgressDialog();

                        if (responseData.get("status").getAsString().equalsIgnoreCase("false")) {


                            Utility.getSharedInstance().dismissProgressDialog();
                            Toast.makeText(AddEventActivity.this, "" + responseData.get("message").getAsString(), Toast.LENGTH_SHORT).show();


                        } else {
                            Utility.getSharedInstance().dismissProgressDialog();
                            Toast.makeText(AddEventActivity.this, "Event added to list", Toast.LENGTH_SHORT).show();
                            finish();

                        }

                    } else {
                        Utility.getSharedInstance().dismissProgressDialog();
                        Toast.makeText(AddEventActivity.this, "" + responseData.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Toast.makeText(AddEventActivity.this, "failed", Toast.LENGTH_SHORT).show();

                    Utility.getSharedInstance().dismissProgressDialog();
                }


            });
        } else {

            Utility.getSharedInstance().dismissProgressDialog();

            Toast.makeText(this, "No internet connection available", Toast.LENGTH_SHORT).show();
        }
    }

}
