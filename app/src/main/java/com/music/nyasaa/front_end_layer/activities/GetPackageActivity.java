package com.music.nyasaa.front_end_layer.activities;

import android.content.Intent;

import android.app.Activity;

import org.json.JSONObject;
import org.json.JSONException;

import android.content.IntentFilter;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.widget.Toast;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.music.nyasaa.R;
import com.music.nyasaa.adapter.GetPackagesAdapter;
import com.music.nyasaa.controller.MyApplication;
import com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity;
import com.music.nyasaa.models.GetPackageList;
import com.music.nyasaa.models.RequestModel.SelectPackageModel;
import com.music.nyasaa.utility.SharedPreferenceManager;
import com.music.nyasaa.utility.Utility;

import java.util.List;

//import instamojo.library.InstamojoPay;
//import instamojo.library.InstapayListener;
import instamojo.library.InstamojoPay;
import instamojo.library.InstapayListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.music.nyasaa.retrofit.Config.X_API_KEY;
import static com.music.nyasaa.retrofit.Config.clientService;

public class GetPackageActivity extends AppCompatActivity {

    private RecyclerView mGridView;
    private TextView mTxtWarn;
    private View toolbarView;
    private LinearLayout skipLayout;
    private TextView mTxtToolbarText;
    InstamojoPay instamojoPay = new InstamojoPay();


    public void callInstamojoPay(String email, String phone, String amount, String purpose, String buyername) {
        final Activity activity = this;


        IntentFilter filter = new IntentFilter("ai.devsupport.instamojo");
        registerReceiver(instamojoPay, filter);
        JSONObject pay = new JSONObject();
        try {
            pay.put("email", email);
            pay.put("phone", phone);
            pay.put("purpose", purpose);
            pay.put("amount", amount);
            pay.put("name", buyername);
            pay.put("send_sms", true);
            pay.put("send_email", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        initListener();
        instamojoPay.start(activity, pay, listener);
    }

    InstapayListener listener;


    private void initListener() {
        listener = new InstapayListener() {
            @Override
            public void onSuccess(String response) {
//                Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG)
//                        .show();

                selectPackage(response, "Success");
            }

            @Override
            public void onFailure(int code, String reason) {
                Toast.makeText(getApplicationContext(), "Failed: " + reason, Toast.LENGTH_LONG)
                        .show();
            }
        };
    }

//

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_getpackage);
        // Call the function callInstamojo to start payment here

        initView();
        onClick();

        getAllEvents();
    }


    public void getAllEvents() {

        MyApplication myApplication = new MyApplication();


        if (Utility.getSharedInstance().isConnectedToInternet(GetPackageActivity.this)) {
            Utility.getSharedInstance().showProgressDialog(GetPackageActivity.this);
            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");

            // id="1";
            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");
//            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().getAllEvents( X_API_KEY,"10","0");
            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().getAllPackages(clientService, X_API_KEY);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();

                    if (responseData != null) {
                        Utility.getSharedInstance().dismissProgressDialog();

                        Gson gson = new Gson();

                        List<GetPackageList> getEvents = null;
                        getEvents = (List<GetPackageList>) gson.fromJson(responseData.get("respon"), new TypeToken<List<GetPackageList>>() {
                        }.getType());
                        if (getEvents != null) {
                            GetPackagesAdapter eventAdapter = new GetPackagesAdapter(GetPackageActivity.this, getEvents);
                            mGridView.setAdapter(eventAdapter);


                            mGridView.setVisibility(View.VISIBLE);
                            mTxtWarn.setVisibility(View.GONE);
                        }
                    } else {
                        Utility.getSharedInstance().dismissProgressDialog();
                    }
                    // Log.e("Response::", responseData.toString());


                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                    Utility.getSharedInstance().dismissProgressDialog();
                    mTxtWarn.setVisibility(View.VISIBLE);
                    mGridView.setVisibility(View.GONE);


                }


            });
        } else {

            Utility.getSharedInstance().dismissProgressDialog();
        }
    }


    private void initView() {

        toolbarView = findViewById(R.id.toolbar_layout);

        mGridView = findViewById(R.id.gridList);
        mGridView.setLayoutManager(new GridLayoutManager(this, 2));

        mTxtWarn = findViewById(R.id.txtWarn);
        mTxtToolbarText = findViewById(R.id.txtTitleToolbar);
        skipLayout = findViewById(R.id.img_right_layout);


        mTxtToolbarText.setText("Packages");


    }


    private void onClick() {

    }


    private void selectPackage(String paymentID, String result) {

        MyApplication myApplication = new MyApplication();


        String packageID = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_PACKAGE_ID, "");
        String userID = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");


        SelectPackageModel selectPackageModel = new SelectPackageModel();

        selectPackageModel.setPackageId(packageID);
        selectPackageModel.setUserId(userID);
        selectPackageModel.setPaymentId(paymentID);
        selectPackageModel.setPaymentStatus(result);
        selectPackageModel.setDuration(SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_DURATION, ""));

        Utility.getSharedInstance().showProgressDialog(this);


        Call<JsonObject> callbackLogin = myApplication.getAPIInstance().SelectPackage(clientService, X_API_KEY, selectPackageModel);
        //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
        callbackLogin.enqueue(new Callback<JsonObject>() {
            @RequiresApi(api = Build.VERSION_CODES.ECLAIR)
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                JsonObject responseData = response.body();



                try{

                    if (responseData != null) {
                        if (responseData.get("status").getAsInt() == 200) {
//                        JsonObject userJsonObject = responseData.getAsJsonObject("respon");

                            Utility.getSharedInstance().dismissProgressDialog();


                            Toast.makeText(GetPackageActivity.this, responseData.get("message").getAsString(), Toast.LENGTH_SHORT).show();

                            SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_IS_LOGIN, "1");


                            Intent intent = new Intent(GetPackageActivity.this, DashBoardActivity.class);
                            startActivity(intent);
                            overridePendingTransition(R.anim.left, R.anim.right);
                            finish();

//
                        } else {
                            Toast.makeText(GetPackageActivity.this, responseData.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                            Utility.getSharedInstance().dismissProgressDialog();
                        }
                    } else {
                        Toast.makeText(GetPackageActivity.this, "Failed to select package", Toast.LENGTH_SHORT).show();
//                    Utility.getSharedInstance().dismissProgressDialog();
                        Utility.getSharedInstance().dismissProgressDialog();
                    }

                }
                catch ( Exception e )
                {

                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Utility.getSharedInstance().dismissProgressDialog();
                Toast.makeText(GetPackageActivity.this, "Api error", Toast.LENGTH_SHORT).show();
                // Utility.getSharedInstance().DismissProgressBar();
                //   Utility.getSharedInstance().Snackbar(toolbarLayout, "Something went wrong!");

            }


        });
    }


    @Override
    protected void onPause() {
//        unregisterReceiver(instamojoPay);

        super.onPause();
    }
}
