package com.music.nyasaa.front_end_layer.activities.OTP;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.music.nyasaa.R;
import com.music.nyasaa.controller.MyApplication;
import com.music.nyasaa.front_end_layer.activities.GetPackageActivity;

import com.music.nyasaa.models.RequestModel.OTPmodel;
import com.music.nyasaa.models.User_DAO;
import com.music.nyasaa.utility.SharedPreferenceManager;
import com.music.nyasaa.utility.Utility;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.music.nyasaa.retrofit.Config.X_API_KEY;
import static com.music.nyasaa.retrofit.Config.clientService;

public class OTPActivity extends AppCompatActivity {

    private TextView mTxtToolbarText;
    private EditText mEditOTP;
    private Button mButSubmit;
    private LinearLayout skipLayout;
    String id;
    String otp;
    Intent intent;

    private TextView mtxtResend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        initView();
        onClick();
    }

    private void initView() {

        mTxtToolbarText = findViewById(R.id.txtTitleToolbar);
        skipLayout = findViewById(R.id.img_right_layout);
        mTxtToolbarText.setText("OTP");

        mButSubmit = findViewById(R.id.butSubmit);
        mEditOTP = findViewById(R.id.editOTP);

        skipLayout.setVisibility(View.INVISIBLE);

        Intent intent = getIntent();
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            id = extras.getString("id");
            otp = extras.getString("otp");
        }



        mtxtResend=findViewById(R.id.txtResend);


    }

    private void onClick() {

        mtxtResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resendOTP();
            }
        });
        mButSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                Toast.makeText(OTPActivity.this, "" + otp, Toast.LENGTH_SHORT).show();
                if (mEditOTP.getText().toString().trim().equalsIgnoreCase("")) {
                    Toast.makeText(OTPActivity.this, "Enter valid OTP", Toast.LENGTH_SHORT).show();
                } else if (mEditOTP.getText().toString().trim().equals(otp)) {
                    OTP_verfty(mEditOTP.getText().toString().trim());
                } else {
                    Toast.makeText(OTPActivity.this, "Invalid OTP try after sometime", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }


    private void OTP_verfty(String otp) {

        MyApplication myApplication = new MyApplication();


        OTPmodel otPmodel = new OTPmodel();

        otPmodel.setOtp_status("1");
        otPmodel.setUserId(id);

        Utility.getSharedInstance().showProgressDialog(this);


        Call<JsonObject> callbackLogin = myApplication.getAPIInstance().OTP(clientService, X_API_KEY, otPmodel);
        //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
        callbackLogin.enqueue(new Callback<JsonObject>() {
            @RequiresApi(api = Build.VERSION_CODES.ECLAIR)
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                JsonObject responseData = response.body();


                if (responseData != null) {
                    if (responseData.get("status").getAsInt() == 200) {
//                        JsonObject userJsonObject = responseData.getAsJsonObject("respon");

                        Utility.getSharedInstance().dismissProgressDialog();


                        SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_USER_ID, id);

                        Intent intent = new Intent(OTPActivity.this, GetPackageActivity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.left, R.anim.right);
                        finish();

//
                    } else {
                        Toast.makeText(OTPActivity.this, responseData.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                        Utility.getSharedInstance().dismissProgressDialog();
                    }
                } else {
                    Toast.makeText(OTPActivity.this, "Required all details", Toast.LENGTH_SHORT).show();
//                    Utility.getSharedInstance().dismissProgressDialog();
                    Utility.getSharedInstance().dismissProgressDialog();
                }


            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Utility.getSharedInstance().dismissProgressDialog();
                Toast.makeText(OTPActivity.this, "Api error", Toast.LENGTH_SHORT).show();
                // Utility.getSharedInstance().DismissProgressBar();
                //   Utility.getSharedInstance().Snackbar(toolbarLayout, "Something went wrong!");

            }


        });
    }


    private void resendOTP() {

        Utility.getSharedInstance().showProgressDialog(this);
        MyApplication myApplication = new MyApplication();

        User_DAO loginDAO = new User_DAO();
//        loginDAO.setEmailId(mEditEmail.getText().toString().trim());
        loginDAO.setUser_id(id);


        Call<JsonObject> callbackLogin = myApplication.getAPIInstance().resend_otp(clientService, X_API_KEY, loginDAO);
        callbackLogin.enqueue(new Callback<JsonObject>() {
            @RequiresApi(api = Build.VERSION_CODES.ECLAIR)
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                JsonObject responseData = response.body();


                try {
                    if (responseData != null) {
                        if (responseData.get("status").getAsInt() == 200) {
//                        JsonObject userJsonObject = responseData.getAsJsonObject("respon");

                            Utility.getSharedInstance().dismissProgressDialog();

                            otp = responseData.get("user_otp_no").getAsString();


                            Toast.makeText(OTPActivity.this, responseData.get("message").getAsString(), Toast.LENGTH_SHORT).show();
//
                        } else {
                            Toast.makeText(OTPActivity.this, responseData.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                            Utility.getSharedInstance().dismissProgressDialog();
                        }
                    } else {
                        Toast.makeText(OTPActivity.this, "Required all details", Toast.LENGTH_SHORT).show();
//                    Utility.getSharedInstance().dismissProgressDialog();
                        Utility.getSharedInstance().dismissProgressDialog();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Utility.getSharedInstance().dismissProgressDialog();
                Toast.makeText(OTPActivity.this, "Api error", Toast.LENGTH_SHORT).show();
                // Utility.getSharedInstance().DismissProgressBar();
                //   Utility.getSharedInstance().Snackbar(toolbarLayout, "Something went wrong!");

            }


        });
    }


}
