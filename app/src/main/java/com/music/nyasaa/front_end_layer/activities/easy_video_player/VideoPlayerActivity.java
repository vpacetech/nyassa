package com.music.nyasaa.front_end_layer.activities.easy_video_player;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;

import com.music.nyasaa.R;
import com.music.nyasaa.front_end_layer.activities.PaymentActivity;
import com.music.nyasaa.utility.video_view_helper.ResizeSurfaceView;
import com.music.nyasaa.utility.video_view_helper.VideoControllerView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import static com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity.FromDownoadFile;
import static com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity.appcontectext;
import static com.music.nyasaa.retrofit.Config.BASE_VIDEO_URL;

public class VideoPlayerActivity extends Activity implements SurfaceHolder.Callback, MediaPlayer.OnPreparedListener, VideoControllerView.MediaPlayerControlListener, MediaPlayer.OnVideoSizeChangedListener, MediaPlayer.OnCompletionListener {

    private final static String TAG = "MainActivity";
    ResizeSurfaceView mVideoSurface;
    MediaPlayer mMediaPlayer;
    VideoControllerView controller;
    private int mVideoWidth;
    private int mVideoHeight;
    private View mContentView;
    private View mLoadingView;
    private boolean mIsComplete;
    private String urlVideo = "";
    private Dialog subscribe;
    private Button mSubscribeBut;
    Handler handler = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.e(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);


        Intent intent = getIntent();
        urlVideo = intent.getStringExtra("url");

        mVideoSurface = (ResizeSurfaceView) findViewById(R.id.videoSurface);
        mContentView = findViewById(R.id.video_container);
        mLoadingView = findViewById(R.id.loading);
        SurfaceHolder videoHolder = mVideoSurface.getHolder();
        videoHolder.addCallback(this);


        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        mMediaPlayer = new MediaPlayer();
        mMediaPlayer.setOnVideoSizeChangedListener(this);
        //(FrameLayout) findViewById(R.id.videoSurfaceContainer)
        controller = new VideoControllerView.Builder(this, this)
                .withVideoTitle("Ask O.S.H.O.")
                .withVideoSurfaceView(mVideoSurface)//to enable toggle display controller view
                .canControlBrightness(true)
                .canControlVolume(true)
                .canSeekVideo(true)
//                .exitIcon(R.drawable.video_top_back)
                .pauseIcon(R.drawable.ic_media_pause)
                .playIcon(R.drawable.ic_media_play)
                .shrinkIcon(R.drawable.ic_media_fullscreen_shrink)
                .stretchIcon(R.drawable.ic_media_fullscreen_stretch)
                .build((FrameLayout) findViewById(R.id.videoSurfaceContainer));//layout container that hold video play view

        mLoadingView.setVisibility(View.VISIBLE);


        try {
            mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
//<<<<<<< HEAD
//            mMediaPlayer.setDataSource(this, Uri.parse(BASE_VIDEO_URL + Uri.encode( urlVideo)));
//=======


            if(FromDownoadFile){
                String url = null;
                FromDownoadFile=false;
                try {
                     url = decrypt(urlVideo);
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (NoSuchPaddingException e) {
                    e.printStackTrace();
                } catch (InvalidKeyException e) {
                    e.printStackTrace();
                }
                mMediaPlayer.setDataSource(this, Uri.parse(url));

            }
            else {
                mMediaPlayer.setDataSource(this, Uri.parse(BASE_VIDEO_URL + Uri.encode(urlVideo)));
            }



            mMediaPlayer.setOnPreparedListener(this);
            mMediaPlayer.setOnCompletionListener(this);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        mVideoSurface.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                controller.toggleControllerView();
                return false;
            }
        });

        subscribe = new Dialog(VideoPlayerActivity.this);
        subscribe.requestWindowFeature(Window.FEATURE_NO_TITLE);
        subscribe.setContentView(R.layout.dialog_subscribe);
        subscribe.getWindow().getAttributes().width = ActionBar.LayoutParams.FILL_PARENT;
        subscribe.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        subscribe.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mSubscribeBut = (Button) subscribe.findViewById(R.id.butNo);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
//        controller.show();
        return false;
    }

    @Override
    public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
        mVideoHeight = mp.getVideoHeight();
        mVideoWidth = mp.getVideoWidth();
        if (mVideoHeight > 0 && mVideoWidth > 0)
            mVideoSurface.adjustSize(mContentView.getWidth(), mContentView.getHeight(), mMediaPlayer.getVideoWidth(), mMediaPlayer.getVideoHeight());
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (mVideoWidth > 0 && mVideoHeight > 0)
            mVideoSurface.adjustSize(getDeviceWidth(this), getDeviceHeight(this), mVideoSurface.getWidth(), mVideoSurface.getHeight());
    }

    private void resetPlayer() {
        if (mMediaPlayer != null) {
            mMediaPlayer.reset();
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
    }

    public static int getDeviceWidth(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics mDisplayMetrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(mDisplayMetrics);
        return mDisplayMetrics.widthPixels;
    }

    public static int getDeviceHeight(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics mDisplayMetrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(mDisplayMetrics);
        return mDisplayMetrics.heightPixels;
    }


    // Implement SurfaceHolder.Callback
    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {

        if (holder != null && mMediaPlayer != null) {

            try {
                mMediaPlayer.setDisplay(holder);
                mMediaPlayer.prepareAsync();
            }
            catch (Exception e)
            {}

        } else {
            finish();
        }

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        resetPlayer();
    }
// End SurfaceHolder.Callback


    // Implement MediaPlayer.OnPreparedListener
    @Override
    public void onPrepared(MediaPlayer mp) {
        //setup video controller view
        mLoadingView.setVisibility(View.GONE);
        mVideoSurface.setVisibility(View.VISIBLE);
        mMediaPlayer.start();
        mIsComplete = false;

        paymentBlink();

//        Toast.makeText(VideoPlayerActivity.this, "Func Called Prpare", Toast.LENGTH_SHORT).show();
    }
// End MediaPlayer.OnPreparedListener

    /**
     * Implement VideoMediaController.MediaPlayerControl
     */

    @Override
    public int getBufferPercentage() {
        return 0;
    }

    @Override
    public int getCurrentPosition() {
        if (null != mMediaPlayer)
            return mMediaPlayer.getCurrentPosition();
        else
            return 0;
    }

    @Override
    public int getDuration() {
        if (null != mMediaPlayer)
            return mMediaPlayer.getDuration();
        else
            return 0;
    }

    @Override
    public boolean isPlaying() {
        if (null != mMediaPlayer) {
            paymentBlink();
            //Toast.makeText(VideoPlayerActivity.this, "Func Called Is Playing", Toast.LENGTH_SHORT).show();
            return mMediaPlayer.isPlaying();

        } else {
            return false;
        }


    }

    @Override
    public boolean isComplete() {
        return mIsComplete;
    }

    @Override
    public void pause() {
        if (null != mMediaPlayer) {
            mMediaPlayer.pause();
        }

    }

    @Override
    public void seekTo(int i) {
        if (null != mMediaPlayer) {
            mMediaPlayer.seekTo(i);
//            paymentBlink();
        }
    }

    @Override
    public void start() {
        if (null != mMediaPlayer) {
            mMediaPlayer.start();
            mIsComplete = false;
//            paymentBlink();
        }
    }

    @Override
    public boolean isFullScreen() {
        return getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE ? true : false;
    }

    @Override
    public void toggleFullScreen() {
        if (isFullScreen()) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
    }

    @Override
    public void exit() {
        resetPlayer();
        finish();
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        mIsComplete = true;
    }

    // End VideoMediaController.MediaPlayerControl


    @Override
    protected void onPause() {
        super.onPause();
        if (null != mMediaPlayer) {
            mMediaPlayer.pause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    /*@Override
    public void onBackPressed() {
        super.onBackPressed();
        if(handler!=null)

    }*/

    public void paymentBlink(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {


                if(mMediaPlayer!=null)
                {

                    try {
//                        mMediaPlayer.pause();
                    }
                    catch (Exception e)
                    {}

                }

//                subscribe.show();
                mSubscribeBut.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(VideoPlayerActivity.this, PaymentActivity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.left, R.anim.right);
                        subscribe.dismiss();
                    }
                });
            }
        }, 30000);
    }

   /* public void paymentBlink(){
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //your code here
                    }
                }, 60000);
            }
        });
    }*/


    String decrypt(String outPaNameth) throws IOException, NoSuchAlgorithmException,
            NoSuchPaddingException, InvalidKeyException {

        File filei = new File(appcontectext.getFilesDir(), "tempfilenametoplay");

        String inPath = filei.getAbsolutePath();
        // deleteit

        File fileo = new File(appcontectext.getFilesDir(), outPaNameth);
//
        String outPath = fileo.getAbsolutePath();

        FileInputStream fis = new FileInputStream(outPath);
        FileOutputStream fos = new FileOutputStream(inPath);
        SecretKeySpec sks = new SecretKeySpec("ABCDEFdhdhdhghgfghfghgfg".getBytes(),
                "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, sks);
        CipherInputStream cis = new CipherInputStream(fis, cipher);
        int b;
        byte[] d = new byte[1024];
        while ((b = cis.read(d)) != -1) {
            fos.write(d, 0, b);
        }
        fos.flush();
        fos.close();
        cis.close();
        return inPath;
    }

}
