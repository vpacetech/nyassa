package com.music.nyasaa.front_end_layer.activities.download_in_appscope_enc;

public class TempDOWNLOAD {

//
//    package com.av.front_end_layer.activity;
//
//import android.Manifest;
//import android.app.DownloadManager;
//import android.app.ProgressDialog;
//import android.content.BroadcastReceiver;
//import android.content.ContentValues;
//import android.content.Context;
//import android.content.Intent;
//import android.content.IntentFilter;
//import android.content.pm.PackageManager;
//import android.database.Cursor;
//import android.database.sqlite.SQLiteDatabase;
//import android.media.MediaPlayer;
//import android.net.Uri;
//import android.os.Bundle;
//import android.support.v4.app.ActivityCompat;
//import android.support.v4.content.ContextCompat;
//import android.support.v7.app.AppCompatActivity;
//import android.support.v7.widget.DefaultItemAnimator;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.view.View;
//import android.widget.TextView;
//import android.widget.VideoView;
//
//import org.json.JSONObject;
//
//import java.io.BufferedInputStream;
//import java.io.BufferedOutputStream;
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.FileNotFoundException;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.OutputStream;
//import java.security.InvalidKeyException;
//import java.security.NoSuchAlgorithmException;
//import java.security.SecureRandom;
//import java.security.spec.InvalidKeySpecException;
//import java.security.spec.KeySpec;
//import java.util.ArrayList;
//import java.util.List;
//
//import javax.crypto.Cipher;
//import javax.crypto.CipherInputStream;
//import javax.crypto.CipherOutputStream;
//import javax.crypto.KeyGenerator;
//import javax.crypto.NoSuchPaddingException;
//import javax.crypto.SecretKey;
//import javax.crypto.SecretKeyFactory;
//import javax.crypto.spec.IvParameterSpec;
//import javax.crypto.spec.PBEKeySpec;
//import javax.crypto.spec.SecretKeySpec;
//
//import com.av.R;
//import com.av.integration_layer.helper.DataSQLHelper;
//import com.av.integration_layer.helper.MoviesAdapter;
//import com.av.wrapper_layer.data_model.Movie;
//
//import static com.av.integration_layer.helper.constant.Table_Details.AV_DATA_FILE;
//import static com.av.integration_layer.helper.constant.Table_Details.AV_DATA_TYPE;
//import static com.av.integration_layer.helper.constant.Table_Details.AV_DATA_URL;
//import static com.av.integration_layer.helper.constant.Table_Details.AV_ENCR_DCR_KEY;
//import static com.av.integration_layer.helper.constant.Table_Details.JSON_COL;
//import static com.av.integration_layer.helper.constant.Table_Details.TABLE_NAME;
//
//    public class DownloadActivity extends AppCompatActivity {
//        VideoView video;
//        String video_url = "http://file2.video9.in/english/movie/2014/x-men-_days_of_future_past/X-Men-%20Days%20of%20Future%20Past%20Trailer%20-%20[Webmusic.IN].3gp";
//        ProgressDialog pd;
//        String vdo_name_urls[]={"V1","V2","V3","V4"};
//        String vdo_urls[]={
//                "https://ak9.picdn.net/shutterstock/videos/14842069/preview/stock-footage-happy-family-father-mother-and-two-kids-lying-down-on-the-sofa-and-using-tablet-pc-at-night.mp4",
//                "https://ak8.picdn.net/shutterstock/videos/27850738/preview/stock-footage-woman-laughing-uncontrollably-while-watching-a-comedy-movie-at-the-cinema.mp4",
//                "https://ak2.picdn.net/shutterstock/videos/27850702/preview/stock-footage-a-woman-reacts-impressed-to-a-moment-in-a-film-while-at-the-cinema.mp4",
//                "https://ak5.picdn.net/shutterstock/videos/27851035/preview/stock-footage-a-beautiful-young-woman-sitting-in-a-movie-theatre-reacting-to-something-incredible-on-screen.mp4"    };
//
//        public static DownloadActivity mainActivity;
//
//        private long enqueue;
//        private DownloadManager dm;
//        private int REQUEST_READ_PHONE_STATE=0;
//        String filename="myfile1";
//        TextView play;
//        Uri uriToPlay;
//
//        private String encryptedFileName = "Enc_File.mp4";
//        private static String algorithm = "AES";
//        static SecretKey yourKey = null;
//
//        //temp file for download i.e encrpted // delete this
//        private static String filePath = "file1";
//
//
//        //  Encrypted file ***** next its use as path will save as database
//        private static String outPath = "myfile2";
//
//        // temp file for play i.e decrpited , hi run time change hoil
//        //  Encrypted and decrypted files ,***** next its use as path
//        // same name is used for play VDO
//        private static String inPath = "myfile4fsdfsd";
//
//        private List<Movie> movieList = new ArrayList<>();
//        private RecyclerView recyclerView;
//        private MoviesAdapter mAdapter;
//
//
//        @Override
//        protected void onCreate(Bundle savedInstanceState) {
//            super.onCreate(savedInstanceState);
//            setContentView(R.layout.activity_main);
//            //  Licensing.allow(getApplicationContext());
//            mainActivity= DownloadActivity.this;
//
//            File fileo = new File(getApplicationContext().getFilesDir(), outPath);
//            File filei = new File(getApplicationContext().getFilesDir(), inPath);
//            outPath=fileo.getAbsolutePath();
//            inPath=filei.getAbsolutePath();
//
//
//            video = (VideoView)findViewById(R.id.video);
//            play= (TextView) findViewById(R.id.play);
//
//            recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
//
//            mAdapter = new MoviesAdapter(movieList);
//            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
//            recyclerView.setLayoutManager(mLayoutManager);
//            recyclerView.setItemAnimator(new DefaultItemAnimator());
//            recyclerView.setAdapter(mAdapter);
//
//            prepareMovieData();
//            //  play.setVisibility(View.GONE);
//
//            DataSQLHelper dataSQLHelper = new DataSQLHelper(getApplicationContext());
//            SQLiteDatabase sqLiteDatabase;
//            sqLiteDatabase = dataSQLHelper.getWritableDatabase();
//            String getJson = "";
//
//            pd = new ProgressDialog(DownloadActivity.this);
//            pd.setMessage("Buffering video please wait...");
//
//            dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
//            BroadcastReceiver receiver = new BroadcastReceiver() {
//                @Override
//                public void onReceive(Context context, Intent intent) {
//                    String action = intent.getAction();
//                    if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
//                        long downloadId = intent.getLongExtra(
//                                DownloadManager.EXTRA_DOWNLOAD_ID, 1);
//                        DownloadManager.Query query = new DownloadManager.Query();
//                        query.setFilterById(enqueue);
//                        Cursor c = dm.query(query);
//                        if (c.moveToFirst()) {
//                            int columnIndex = c
//                                    .getColumnIndex(DownloadManager.COLUMN_STATUS);
//                            if (DownloadManager.STATUS_SUCCESSFUL == c
//                                    .getInt(columnIndex)) {
//
//                                //  ImageView view = (ImageView) findViewById(R.id.imageView1);
//                                String uriString = c
//                                        .getString(c
//                                                .getColumnIndex(DownloadManager.COLUMN_LOCAL_URI));
//
//                                Uri a = Uri.parse(uriString);
//                                File d = new File(a.getPath());
//                                // copy file from external to internal will esaily avalible on net use google.
//                                //  view.setImageURI(a);
//
//                                filename = "myfile1";
//                                File file = new File(getApplicationContext().getFilesDir(), filename);
//                                filePath=file.getAbsolutePath();
//
//                                try {
//
//                                    //   File fileoo = new File(getApplicationContext().getFilesDir(), outPath);
//                                    copyFile(d,file);
//                                    encrypt();
//                                    // delete filename
//
//                                    // addtoDatabase(fileoo.getName(),video_url,AV_DATA_VDO);
//
//                                } catch (Exception e) {
//                                    e.printStackTrace();
//                                }
//
//                                String url= file.getAbsolutePath();
//
//                                uriToPlay = Uri.parse(url);
//
//                                file.delete();
//                                File files = new File(a.getPath());
//                                boolean deleted = files.delete();
//                                play.setVisibility(View.VISIBLE);
//                            }
//                        }
//                    }
//                }
//            };
//
//            play.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    try {
//                        decrypt();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    } catch (NoSuchAlgorithmException e) {
//                        e.printStackTrace();
//                    } catch (NoSuchPaddingException e) {
//                        e.printStackTrace();
//                    } catch (InvalidKeyException e) {
//                        e.printStackTrace();
//                    }
//
//                    File file = new File(getApplicationContext().getFilesDir(), "myfile4fsdfsd");
//                    String url= file.getAbsolutePath();
//                    uriToPlay = Uri.parse(url);
//
//                    video.setVideoURI(uriToPlay);
//                    video.start();
//                }
//            });
//            registerReceiver(receiver, new IntentFilter(
//                    DownloadManager.ACTION_DOWNLOAD_COMPLETE));
//            int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
//
//            if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
//                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_READ_PHONE_STATE);
//            } else {
//                //TODO
//
//
//                DownloadManager.Request request = new DownloadManager.Request(
//                        Uri.parse(video_url)).setDestinationInExternalPublicDir("", "VDO.mp4");
//                enqueue = dm.enqueue(request);
//
//            }
//
//
//        }
//
//        void addtoDatabase(String FileName,String URL,String TYPE)
//        {
//            DataSQLHelper dataSQLHelper = new DataSQLHelper(getApplicationContext());
//            SQLiteDatabase sqLiteDatabase;
//            sqLiteDatabase = dataSQLHelper.getWritableDatabase();
//            String getJson = "";
//
//            JSONObject deviceList = new JSONObject();
//            try {
//                deviceList.put(AV_DATA_FILE, FileName);
//                deviceList.put(AV_DATA_URL, URL);
//                deviceList.put(AV_DATA_TYPE, TYPE);
//            }
//            catch (Exception ex)
//            {
//
//            }
//
//            ContentValues contentValues = new ContentValues();
//            contentValues.put(JSON_COL, deviceList.toString());
//            contentValues.put(AV_DATA_FILE, FileName);
//            contentValues.put(AV_DATA_URL, URL);
//            contentValues.put(AV_DATA_TYPE, TYPE);
//
//
//            String query = "SELECT DEVICE_ADDRESS FROM " + TABLE_NAME + " WHERE AV_DATA_FILE='" + FileName + "'";
//            Cursor cursor = sqLiteDatabase.rawQuery(query, null);
//
/////////////////////////////////////////////////////
//
//
//            if (cursor.getCount() > 0)
//            {
//                dataSQLHelper.update_rows(sqLiteDatabase, TABLE_NAME, contentValues, "AV_DATA_FILE=?",FileName);
//
//            } else
//            {
//                dataSQLHelper.insert_rows(sqLiteDatabase, TABLE_NAME, contentValues);
//            }
//
//
//
//
//        }
//
//        public void PlayVDO(int i )
//        {
//            File fileo = new File(getApplicationContext().getFilesDir(), vdo_name_urls[i]);
//            //  File filei = new File(getApplicationContext().getFilesDir(), inPath);
//            outPath=fileo.getAbsolutePath();
//            try {
//                decrypt();
//            } catch (IOException e) {
//                e.printStackTrace();
//            } catch (NoSuchAlgorithmException e) {
//                e.printStackTrace();
//            } catch (NoSuchPaddingException e) {
//                e.printStackTrace();
//            } catch (InvalidKeyException e) {
//                e.printStackTrace();
//            }
//
//            File file = new File(getApplicationContext().getFilesDir(), "myfile4fsdfsd");
//            String url= file.getAbsolutePath();
//            uriToPlay = Uri.parse(url);
//
//            video.setVideoURI(uriToPlay);
//            video.start();
//
//        }
//
//        public void download(int i )
//        {
//            // setNext outpath name from database
//
//            File fileo = new File(getApplicationContext().getFilesDir(), vdo_name_urls[i]);
//            //  File filei = new File(getApplicationContext().getFilesDir(), inPath);
//            outPath=fileo.getAbsolutePath();
//
//            DownloadManager.Request request = new DownloadManager.Request(
//                    Uri.parse(vdo_urls[i])).setDestinationInExternalPublicDir("", "VDO.mp4");
//            enqueue = dm.enqueue(request);
//        }
//
//        @Override
//        public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
//            switch (requestCode) {
//                case 0:
//                    if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
//                        //TODO
//
////                    registerReceiver(receiver, new IntentFilter(
////                            DownloadManager.ACTION_DOWNLOAD_COMPLETE));
//                    }
//                    break;
//
//                default:
//                    break;
//            }
//        }
//
//
//        static void encrypt() throws IOException, NoSuchAlgorithmException,
//                NoSuchPaddingException, InvalidKeyException {
//
//
//            // Here you read the cleartext.
//            FileInputStream fis = new FileInputStream(filePath);
//            // This stream write the encrypted text. This stream will be wrapped by
//            // another stream.
//            FileOutputStream fos = new FileOutputStream(outPath);
//            // Length is 16 byte
//            // AV_ENCR_DCR_KEY
//            SecretKeySpec sks = new SecretKeySpec(AV_ENCR_DCR_KEY.getBytes(),
//                    "AES");
//            // Create cipher
//            Cipher cipher = Cipher.getInstance("AES");
//            cipher.init(Cipher.ENCRYPT_MODE, sks);
//            // Wrap the output stream
//            CipherOutputStream cos = new CipherOutputStream(fos, cipher);
//            // Write bytes
//            int b;
//            byte[] d = new byte[1024];
//            while ((b = fis.read(d)) != -1) {
//                cos.write(d, 0, b);
//            }
//            // Flush and close streams.
//            cos.flush();
//            cos.close();
//            fis.close();
//
//
//        }
//
//        static void decrypt() throws IOException, NoSuchAlgorithmException,
//                NoSuchPaddingException, InvalidKeyException {
//            FileInputStream fis = new FileInputStream(outPath);
//            FileOutputStream fos = new FileOutputStream(inPath);
//            SecretKeySpec sks = new SecretKeySpec(AV_ENCR_DCR_KEY.getBytes(),
//                    "AES");
//            Cipher cipher = Cipher.getInstance("AES");
//            cipher.init(Cipher.DECRYPT_MODE, sks);
//            CipherInputStream cis = new CipherInputStream(fis, cipher);
//            int b;
//            byte[] d = new byte[1024];
//            while ((b = cis.read(d)) != -1) {
//                fos.write(d, 0, b);
//            }
//            fos.flush();
//            fos.close();
//            cis.close();
//        }
//
//
//        // pd.show();
//
////        Uri uri = Uri.parse(video_url);
////        video.setVideoURI(uri);
////        video.start();
////
////        video.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
////            @Override
////            public void onPrepared(MediaPlayer mp) {
////                //close the progress dialog when buffering is done
////                pd.dismiss();
////            }
////        });
////        String filename = "myfile";
////        File file = new File(getApplicationContext().getFilesDir(), filename);
////        DownLoadManager DownLoadManager= new DownLoadManager();
////        DownLoadManager.startDownLoad(getApplicationContext(),video_url,filename);
////
////       // DownLoadFileThread(Context context, String sourceUrl,String destinationPath)
////        try { URL url = new URL("https://ak1.picdn.net/shutterstock/videos/19408801/preview/stock-footage-man-put-a-case-in-the-trunk-movie-style.mp4");
////           // File file = new File(fileName);
////
////            long sTime = System.currentTimeMillis();
////            URLConnection URLcon = url.openConnection();
////
////            InputStream is = URLcon.getInputStream();
////            BufferedInputStream bis = new BufferedInputStream(is);
////
////            ByteArrayBuffer baf = new ByteArrayBuffer(50);
////            int current = 0;
////            while ((current = bis.read()) != -1) {
////                baf.append((byte) current);
////                pd.dismiss();
////            }
////
////            FileOutputStream fos = new FileOutputStream(file);
////            fos.write(baf.toByteArray());
////            fos.close();
////
////        } catch (Exception e) {
////            Log.d("ERROR.......",e+"");
////        }
//
//
//        private boolean copyFile(File src,File dst)throws IOException{
//            if(src.getAbsolutePath().toString().equals(dst.getAbsolutePath().toString())){
//
//                return true;
//
//            }else{
//                InputStream is=new FileInputStream(src);
//                OutputStream os=new FileOutputStream(dst);
//                byte[] buff=new byte[1024];
//                int len;
//                while((len=is.read(buff))>0){
//                    os.write(buff,0,len);
//                }
//                is.close();
//                os.close();
//            }
//            return true;
//        }
//
//        public void Play()
//        {
//            Uri uri = Uri.parse(video_url);
//            video.setVideoURI(uri);
//            video.start();
//
//            video.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
//                @Override
//                public void onPrepared(MediaPlayer mp) {
//                    //close the progress dialog when buffering is done
//                    //  pd.dismiss();
//                }
//            });
//        }
//
//        public static SecretKey generateKey(char[] passphraseOrPin, byte[] salt) throws NoSuchAlgorithmException, InvalidKeySpecException {
//            // Number of PBKDF2 hardening rounds to use. Larger values increase
//            // computation time. You should select a value that causes computation
//            // to take >100ms.
//            final int iterations = 1000;
//
//            // Generate a 256-bit key
//            final int outputKeyLength = 256;
//
//            SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance(AV_ENCR_DCR_KEY);
//            KeySpec keySpec = new PBEKeySpec(passphraseOrPin, salt, iterations, outputKeyLength);
//            SecretKey secretKey = secretKeyFactory.generateSecret(keySpec);
//            return secretKey;
//        }
//
//        public static SecretKey generateKey() throws NoSuchAlgorithmException {
//            // Generate a 256-bit key
//            final int outputKeyLength = 256;
//            SecureRandom secureRandom = new SecureRandom();
//            // Do *not* seed secureRandom! Automatically seeded from system entropy.
//            KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
//            keyGenerator.init(outputKeyLength, secureRandom);
//            yourKey = keyGenerator.generateKey();
//            return yourKey;
//        }
//
//        public static byte[] encodeFile(SecretKey yourKey, byte[] fileData)
//                throws Exception {
//            byte[] encrypted = null;
//            byte[] data = yourKey.getEncoded();
//            SecretKeySpec skeySpec = new SecretKeySpec(data, 0, data.length,
//                    algorithm);
//            Cipher cipher = Cipher.getInstance(algorithm);
//            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, new IvParameterSpec(
//                    new byte[cipher.getBlockSize()]));
//            encrypted = cipher.doFinal(fileData);
//            return encrypted;
//        }
//
//        public static byte[] decodeFile(SecretKey yourKey, byte[] fileData)
//                throws Exception {
//            byte[] decrypted = null;
//            Cipher cipher = Cipher.getInstance(algorithm);
//            cipher.init(Cipher.DECRYPT_MODE, yourKey, new IvParameterSpec(
//                    new byte[cipher.getBlockSize()]));
//            decrypted = cipher.doFinal(fileData);
//            return decrypted;
//        }
//
//        void saveFile(String stringToSave) {
//            try {
//                File file = new File(getApplicationContext().getFilesDir(), encryptedFileName);
//                BufferedOutputStream bos = new BufferedOutputStream(
//                        new FileOutputStream(file));
//                yourKey = generateKey();
//                byte[] filesBytes = encodeFile(yourKey, stringToSave.getBytes());
//                bos.write(filesBytes);
//                bos.flush();
//                bos.close();
//            } catch (FileNotFoundException e) {
//                e.printStackTrace();
//            } catch (IOException e) {
//                e.printStackTrace();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//
//        void decodeFile() {
//
//            try {
//                byte[] decodedData = decodeFile(yourKey, readFile());
//                String str = new String(decodedData);
//                System.out.println("DECODED FILE CONTENTS : " + str);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//
//        public byte[] readFile() {
//            byte[] contents = null;
//
//            File file = new File(getApplicationContext().getFilesDir(), encryptedFileName);
//            int size = (int) file.length();
//            contents = new byte[size];
//            try {
//                BufferedInputStream buf = new BufferedInputStream(
//                        new FileInputStream(file));
//                try {
//                    buf.read(contents);
//                    buf.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            } catch (FileNotFoundException e) {
//                e.printStackTrace();
//            }
//            return contents;
//        }
//
//        private void prepareMovieData() {
//            Movie movie = new Movie("expression 1", "Action & Adventure", "2015",vdo_urls[0]);
//            movieList.add(movie);
//
//
//            movie = new Movie("expression 2", "Action & Adventure", "2008",vdo_urls[1]);
//            movieList.add(movie);
//
//            movie = new Movie("expression 3", "Science Fiction", "1986",vdo_urls[2]);
//            movieList.add(movie);
//
//            movie = new Movie("expression 4", "Animation", "2000",vdo_urls[3]);
//            movieList.add(movie);
//
//
//
//            mAdapter.notifyDataSetChanged();
//        }
//
//
//    }


}
