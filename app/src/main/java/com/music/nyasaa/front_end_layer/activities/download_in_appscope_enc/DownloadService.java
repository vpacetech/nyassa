package com.music.nyasaa.front_end_layer.activities.download_in_appscope_enc;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.Toast;


import com.music.nyasaa.R;
import com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Retrofit;

import static com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity.isdownloading;
import static com.music.nyasaa.front_end_layer.activities.download_in_appscope_enc.Table_Details.Album_name;
import static com.music.nyasaa.front_end_layer.activities.download_in_appscope_enc.Table_Details.Cover_image;
import static com.music.nyasaa.front_end_layer.activities.download_in_appscope_enc.Table_Details.Image_url;
import static com.music.nyasaa.front_end_layer.activities.download_in_appscope_enc.Table_Details.id;
import static com.music.nyasaa.front_end_layer.activities.download_in_appscope_enc.Table_Details.File_Name;
import static com.music.nyasaa.front_end_layer.activities.download_in_appscope_enc.Table_Details.Image;
import static com.music.nyasaa.front_end_layer.activities.download_in_appscope_enc.Table_Details.JSON_COL;
import static com.music.nyasaa.front_end_layer.activities.download_in_appscope_enc.Table_Details.TABLE_NAME;
import static com.music.nyasaa.front_end_layer.activities.download_in_appscope_enc.Table_Details.Type;
import static com.music.nyasaa.front_end_layer.activities.download_in_appscope_enc.Table_Details.URL;
import static com.music.nyasaa.front_end_layer.activities.download_in_appscope_enc.Table_Details.name_c;
import static com.music.nyasaa.retrofit.Config.BASE_URL_FOR_DOWNLOAD;


public class DownloadService extends IntentService {

    // com.music.av.front_end_layer.activities.download_in_appscope_enc.DownloadService
    public DownloadService() {
        super("Download Service");
    }

    private NotificationCompat.Builder notificationBuilder;
    private NotificationManager notificationManager;
    private int totalFileSize;
    File fileNO;

    String fileName,Types,URLs,Images,ids,Image_urls,names,album_name,coverimage;


    @Override
    protected void onHandleIntent(Intent intent) {

        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_download)
                .setContentTitle("Download")
                .setContentText("Downloading File")
                .setAutoCancel(true);
        notificationManager.notify(0, notificationBuilder.build());


        Bundle extras = intent.getExtras();
        if(extras == null) {
            fileName= null;
            Types=null;
            URLs=null;
            Images=null;
            ids=null;
            Image_urls=null;
            names=null;
            album_name=null;
        } else {
            fileName= extras.getString("File_Name");
            Types= extras.getString("Type");
            URLs=extras.getString("URL");
            Images=extras.getString("Image");
            ids=extras.getString("id");
            Image_urls=extras.getString("Image_URL");
            names=extras.getString("name");
            album_name=extras.getString("album_name");
            coverimage=extras.getString("coverimage");
        }

        initDownloadImage(URLs,fileName,Types,Images,ids,Image_urls,names,album_name);
        initDownload(URLs,fileName,Types,Images,ids,Image_urls,names,album_name,coverimage);

    }

    private void initDownload(String URL, String Filename, String Type, String Image, String ids, String image_url, String name, String album_name, String coverimage){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL_FOR_DOWNLOAD)
                .build();


        //String[] path = URL.split("/");

        RetrofitInterface retrofitInterface = retrofit.create(RetrofitInterface.class);



        Call<ResponseBody> request = retrofitInterface.downloadFilesURL(URL);
        try {

            downloadFile(request.execute().body(),URL, Filename,Type,Image,ids,image_url,  name,album_name,coverimage);

        } catch (Exception e) {
            isdownloading=false;
            if(fileNO!=null)
                fileNO.delete();

            e.printStackTrace();
            Toast.makeText(getApplicationContext(),"No Data Found",Toast.LENGTH_SHORT).show();

        }
    }



    private void initDownloadImage(String URL, String Filename, String Type, String Image, String ids, String image_urls, String name, String album_name){
        try {
//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl("http://olavideos.s3.amazonaws.com/")
//                .build();


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL_FOR_DOWNLOAD)
                    .build();




            //String[] path = URL.split("/");

            RetrofitInterface retrofitInterface = retrofit.create(RetrofitInterface.class);



            Call<ResponseBody> request = retrofitInterface.downloadFilesURL(image_urls);


            downloadFileImage(request.execute().body(),URL, Filename,Type,Image,ids,image_urls,name, album_name);

        } catch (Exception e) {

            if(fileNO!=null)
                fileNO.delete();

            e.printStackTrace();
            Toast.makeText(getApplicationContext(),"No Data Found",Toast.LENGTH_SHORT).show();

        }
    }


    private void downloadFileImage(ResponseBody body, String URL, String Filenames, String Type, String Image, String ids, String image_url, String name, String album_name) throws IOException {
        try{
            int count;
            byte data[] = new byte[1024 * 4];
            long fileSize = body.contentLength();
            InputStream bis = new BufferedInputStream(body.byteStream(), 1024 * 8);
            File outputFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), Image);
            OutputStream output = new FileOutputStream(outputFile);
            long total = 0;
            long startTime = System.currentTimeMillis();
            int timeCount = 1;
            while ((count = bis.read(data)) != -1) {

                total += count;
                totalFileSize = (int) (fileSize / (Math.pow(1024, 2)));
                double current = Math.round(total / (Math.pow(1024, 2)));

                int progress = (int) ((total * 100) / fileSize);

                long currentTime = System.currentTimeMillis() - startTime;

                // Download download = new Download();
                // download.setTotalFileSize(totalFileSize);

                if (currentTime > 1000 * timeCount) {

                    //download.setCurrentFileSize((int) current);
                    //  download.setProgress(progress);
                    // sendNotification(download);
                    timeCount++;
                }

                output.write(data, 0, count);
            }


            File fileo = new File(getApplicationContext().getFilesDir(), Image);


            copyFile(outputFile, fileo);

            //File fileo = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "ddfileENC");
            //  File filei = new File(getApplicationContext().getFilesDir(), inPath);
//        String  outPath=fileo.getAbsolutePath();
//        String   filePath=outputFile.getAbsolutePath();
//
//        try {
//
//            encrypt(filePath,outPath, URL,Filenames,Type,Image,ids,image_url,  name);
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//        } catch (NoSuchPaddingException e) {
//            e.printStackTrace();
//        } catch (InvalidKeyException e) {
//            e.printStackTrace();
//        }
            output.flush();
            output.close();
            bis.close();

            outputFile.delete();
        }catch (Exception e) {
            Toast.makeText(getApplicationContext(),"No Image Found",Toast.LENGTH_SHORT).show();

        }

        //  onDownloadComplete();


    }



    private void downloadFile(ResponseBody body, String URL, String Filenames, String Type, String Image, String ids, String image_url, String name, String album_name, String coverimage) throws IOException {

        int count;
        byte data[] = new byte[1024 * 4];
        long fileSize = body.contentLength();
        InputStream bis = new BufferedInputStream(body.byteStream(), 1024 * 8);
        File outputFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), Filenames);

        fileNO=outputFile;
        OutputStream output = new FileOutputStream(outputFile);
        long total = 0;
        long startTime = System.currentTimeMillis();
        int timeCount = 1;
        while ((count = bis.read(data)) != -1) {

            total += count;
            totalFileSize = (int) (fileSize / (Math.pow(1024, 2)));
            double current = Math.round(total / (Math.pow(1024, 2)));

            int progress = (int) ((total * 100) / fileSize);

            long currentTime = System.currentTimeMillis() - startTime;

            Download download = new Download();
            download.setTotalFileSize(totalFileSize);

            if (currentTime > 1000 * timeCount) {

                download.setCurrentFileSize((int) current);
                download.setProgress(progress);
                sendNotification(download);
                timeCount++;
            }

            output.write(data, 0, count);
        }


        File fileo = new File(getApplicationContext().getFilesDir(), Filenames);


        // copyFile(outputFile, fileo)

        //File fileo = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "ddfileENC");
        //  File filei = new File(getApplicationContext().getFilesDir(), inPath);
        String  outPath=fileo.getAbsolutePath();
        String   filePath=outputFile.getAbsolutePath();

        try {

            encrypt(filePath,outPath, URL,Filenames,Type,Image,ids,image_url, name,album_name,coverimage);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            outputFile.delete();
        } catch (NoSuchPaddingException e) {
            outputFile.delete();
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            outputFile.delete();
            e.printStackTrace();
        }
        output.flush();
        output.close();
        bis.close();

        outputFile.delete();



//        onDownloadComplete();


    }



    private boolean copyFile(File src,File dst)throws IOException{
        if(src.getAbsolutePath().toString().equals(dst.getAbsolutePath().toString())){

            return true;

        }else{
            InputStream is=new FileInputStream(src);
            OutputStream os=new FileOutputStream(dst);
            byte[] buff=new byte[1024];
            int len;
            while((len=is.read(buff))>0){
                os.write(buff,0,len);
            }
            is.close();
            os.close();
        }
        return true;
    }


    // outPath is name fileo.getAbsolutePath( from name of file in Application
    static void decrypt(String outPath,String inPath) throws IOException, NoSuchAlgorithmException,
            NoSuchPaddingException, InvalidKeyException {

        //File filei = new File(getApplicationContext().getFilesDir(), "tempfilenametoplay");
//
//            inPath=filei.getAbsolutePath();
        // deleteit

        // File fileo = new File(getApplicationContext().getFilesDir(), vdo_name_urls[i]);
//            //  File filei = new File(getApplicationContext().getFilesDir(), inPath);
//            outPath=fileo.getAbsolutePath();

        FileInputStream fis = new FileInputStream(outPath);
        FileOutputStream fos = new FileOutputStream(inPath);
        SecretKeySpec sks = new SecretKeySpec("ABCDEFdhdhdhghgfghfghgfg".getBytes(),
                "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, sks);
        CipherInputStream cis = new CipherInputStream(fis, cipher);
        int b;
        byte[] d = new byte[1024];
        while ((b = cis.read(d)) != -1) {
            fos.write(d, 0, b);
        }
        fos.flush();
        fos.close();
        cis.close();
    }


    void encrypt(String filePath, String outPath, String URL, String Filenames, String Type, String Image, String ids, String image_url, String name, String album_name, String coverimage) throws IOException, NoSuchAlgorithmException,
            NoSuchPaddingException, InvalidKeyException {

        // Here you read the cleartext.
        FileInputStream fis = new FileInputStream(filePath);
        // This stream write the encrypted text. This stream will be wrapped by
        // another stream.
        FileOutputStream fos = new FileOutputStream(outPath);
        // Length is 16 byte
        // AV_ENCR_DCR_KEY
        SecretKeySpec sks = new SecretKeySpec("ABCDEFdhdhdhghgfghfghgfg".getBytes(),
                "AES");

        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, sks);
        // Wrap the output stream
        CipherOutputStream cos = new CipherOutputStream(fos, cipher);
        // Write bytes
        int b;
        byte[] d = new byte[1024];
        while ((b = fis.read(d)) != -1) {
            cos.write(d, 0, b);
        }
        // Flush and close streams.
        cos.flush();
        cos.close();
        fis.close();

        ///????? check in Database the file anme set status as completed

        AddToDB( URL, Filenames, Type, Image,ids,image_url, name, album_name,coverimage);



    }

    private void AddToDB(String URLs, String Filenames, String Types, String Images, String ids, String image_urls, String names, String album_name, String coverimage)
    {
        String type="ADO";
        if(Filenames.toLowerCase().contains(".mp3".toLowerCase())||Filenames.toLowerCase().contains(".pcm".toLowerCase())||
                Filenames.toLowerCase().contains(".mid".toLowerCase())||Filenames.toLowerCase().contains(".wav".toLowerCase())||
                Filenames.toLowerCase().contains(".mid".toLowerCase())||Filenames.toLowerCase().contains(".aif".toLowerCase())) {
            type="ADO";
        }
        else {
            type="VDO";
        }


        JSONObject deviceList = new JSONObject();
        try {
            deviceList.put("File_Name", Filenames);
            deviceList.put("Type", type);
            deviceList.put("URL", URLs);
            deviceList.put("Image",Images);
            deviceList.put("id",ids);
            deviceList.put("name",names);
            deviceList.put("Image_url",image_urls);
            deviceList.put("Album_name", album_name);
            deviceList.put("Cover_image", coverimage);



//            public  static  String File_Name = "File_Name";
//            public  static  String Type = "Type";
//            public  static  String URL = "URL";
//            public  static  String Image = "Image";
//            public static String JSON_COL = "JSON_DATA"


            //deviceData.sub_device_type=device.optString("sub_device_type");


        } catch (JSONException e) {
            e.printStackTrace();
        }
        // Print("json data.." + deviceList.toString());


        DataSQLHelper dataSQLHelper = new DataSQLHelper(getApplicationContext());
        SQLiteDatabase sqLiteDatabase;
        sqLiteDatabase = dataSQLHelper.getWritableDatabase();
        String getJson = "";



        ContentValues contentValues = new ContentValues();
        contentValues.put(JSON_COL, deviceList.toString());
        contentValues.put(File_Name, Filenames);
        contentValues.put(Type, Types);
        contentValues.put(URL, URLs);
        contentValues.put(Image, Images);
        contentValues.put(id, ids);
        contentValues.put(Image_url, Images);
        contentValues.put(name_c, names);
        contentValues.put(Album_name, album_name);
        contentValues.put(Cover_image, coverimage);

        //name


        String query = "SELECT * FROM " + TABLE_NAME + " WHERE id='" + ids + "'";
        Cursor cursor = sqLiteDatabase.rawQuery(query, null);

        if (cursor.getCount() > 0)
        {
            cursor.close();
            dataSQLHelper.update_rows(sqLiteDatabase, TABLE_NAME, contentValues, "id=?", ids);

            //deviceDatas.add(deviceData);

        } else {
            cursor.close();
            dataSQLHelper.insert_rows(sqLiteDatabase, TABLE_NAME, contentValues);

        }

        onDownloadComplete();

        stopSelf();
    }


    private void sendNotification(Download download){

        sendIntent(download);
        notificationBuilder.setProgress(100,download.getProgress(),false);
        notificationBuilder.setContentText(String.format("Downloaded (%d/%d) MB",download.getCurrentFileSize(),download.getTotalFileSize()));
        notificationManager.notify(0, notificationBuilder.build());
    }


    private void sendIntent(Download download){

        Intent intent = new Intent(DashBoardActivity.MESSAGE_PROGRESS);
        intent.putExtra("download",download);
        LocalBroadcastManager.getInstance(DownloadService.this).sendBroadcast(intent);
    }

    private void onDownloadComplete(){

        Download download = new Download();
        download.setProgress(100);
        sendIntent(download);

        notificationManager.cancel(0);
        notificationBuilder.setProgress(0,0,false);
        notificationBuilder.setContentText("File Downloaded");
        notificationManager.notify(0, notificationBuilder.build());
//        stopSelf();
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        notificationManager.cancel(0);
    }

}
