package com.music.nyasaa.front_end_layer.activities.community_members;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.music.nyasaa.R;
import com.music.nyasaa.adapter.CommunityMembersAdapter;
import com.music.nyasaa.adapter.SerachMemberAdapter;
import com.music.nyasaa.controller.MyApplication;
import com.music.nyasaa.models.RequestModel.AddToCommunity.RequestToAcceptCommunity;
import com.music.nyasaa.models.RequestModel.DeleteMember.RequestDeleteMember;
import com.music.nyasaa.models.RequestModel.GetCommunityRequest.GetRequestSentbyCommunity;
import com.music.nyasaa.models.RequestModel.GetCommunityRequest.GetRequestSentbyCommunityData;
import com.music.nyasaa.models.RequestModel.SerachUsers.RequestToSearchUser;
import com.music.nyasaa.models.ResponseModel.GetMembersofCommunity.ResponseGetCommunityMem;
import com.music.nyasaa.models.ResponseModel.GetMembersofCommunity.ResponseGetCommunityMemData;
import com.music.nyasaa.models.ResponseModel.GetSeacrchedMembers.GetSearchMemberResponse;
import com.music.nyasaa.models.ResponseModel.GetSeacrchedMembers.GetSearchMemberResponseData;
import com.music.nyasaa.utility.SharedPreferenceManager;
import com.music.nyasaa.utility.SwipeForMembers;
import com.music.nyasaa.utility.Utility;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.music.nyasaa.retrofit.Config.X_API_KEY;

public class CommunityMembersActivity extends AppCompatActivity implements View.OnClickListener, SwipeForMembers.RecyclerItemTouchHelperListener {


    private RecyclerView mListMembers, mListSeacrList;
    private TextView mTxtWarn;
    private SwipyRefreshLayout swipyRefreshLayoutDirection;
    private Dialog confrimDeleteDialog;
    private List<ResponseGetCommunityMemData> responseGetCommunityMemDataLis;
    private CommunityMembersAdapter communityMembersAdapter;
    private EditText mEditSearch;
    private String userID, CommParentID;
    private LinearLayout mLayMembers, mLayRequest, mLayMainBar;

    private TextView mTxtMember, mTxtRequest;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_community_members);

        initView();
        firstColor();
        onClick();
//        getAllMembers();
    }

    @Override
    public void onClick(View v) {

        int id = v.getId();

        switch (id) {

            case R.id.layMember:

                getAllMembers();

                mTxtRequest.setTextColor(getResources().getColor(R.color.white));
                mTxtMember.setTextColor(getResources().getColor(R.color.black));

                mLayMembers.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                mLayRequest.setBackgroundColor(getResources().getColor(R.color.darker_grey));
                break;
            case R.id.layRequest:

                getRequetstData(userID);
                mTxtRequest.setTextColor(getResources().getColor(R.color.black));
                mTxtMember.setTextColor(getResources().getColor(R.color.white));

                mLayMembers.setBackgroundColor(getResources().getColor(R.color.darker_grey));
                mLayRequest.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                break;


            default:

                break;

        }


    }

    private void initView() {

        mLayMainBar = (LinearLayout) findViewById(R.id.menuLayBar);
        mLayMembers = (LinearLayout) findViewById(R.id.layMember);
        mLayRequest = (LinearLayout) findViewById(R.id.layRequest);


        mTxtMember = (TextView) findViewById(R.id.txtMembers);

        mTxtRequest = (TextView) findViewById(R.id.txtRequest);

        TextView mTxtToolbaTxt = (TextView) findViewById(R.id.txtTitleToolbar);
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.toolLayout);

        linearLayout.setBackgroundColor(Color.parseColor("#050505"));
        mTxtToolbaTxt.setText("Friends");

        mTxtWarn = (TextView) findViewById(R.id.txtWarn);
        mEditSearch = (EditText) findViewById(R.id.editSearch);
        mTxtWarn.setVisibility(View.GONE);

        swipyRefreshLayoutDirection = (SwipyRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        swipyRefreshLayoutDirection.setDirection(SwipyRefreshLayoutDirection.BOTTOM);


        ImageView imgEdit = (ImageView) findViewById(R.id.img_notification);

        imgEdit.setImageResource(0);

        imgEdit.setImageResource(R.drawable.ic_action_followers);

        userID = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");
        CommParentID = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_IS_OWN_COMMUNITY, "none");

        if (userID.equalsIgnoreCase(CommParentID)) {
            imgEdit.setVisibility(View.VISIBLE);
            mLayMainBar.setVisibility(View.VISIBLE);
        } else {
            imgEdit.setVisibility(View.GONE);
            mLayMainBar.setVisibility(View.GONE);
        }


        imgEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String owner = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_IS_OWN_COMMUNITY, "none");
                if (owner.equalsIgnoreCase("0")) {

                    Toast.makeText(CommunityMembersActivity.this, "Your not owner for this community", Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(CommunityMembersActivity.this, AddMemberActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.left, R.anim.right);
                }

            }
        });


        mListMembers = (RecyclerView) findViewById(R.id.listViewMembers);
        mListSeacrList = (RecyclerView) findViewById(R.id.listViewSearchMembers);
        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mListMembers.setLayoutManager(linearLayoutManager);
        mListMembers.setItemAnimator(new DefaultItemAnimator());
        mListSeacrList = (RecyclerView) findViewById(R.id.listViewSearchMembers);
        RecyclerView.LayoutManager linearLayoutManagers = new LinearLayoutManager(this);
        mListSeacrList.setLayoutManager(linearLayoutManagers);
        mListSeacrList.setItemAnimator(new DefaultItemAnimator());


        String parentID = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_IS_OWN_COMMUNITY, "none");
        String userID = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "noe");

        if (parentID.equalsIgnoreCase(userID)) {
            ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new SwipeForMembers(0, ItemTouchHelper.LEFT, (SwipeForMembers.RecyclerItemTouchHelperListener) this);
            new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(mListMembers);
        }


        swipyRefreshLayoutDirection.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                getAllMembers();
            }
        });


    }

    private void onClick() {


        mLayRequest.setOnClickListener(this);
        mLayMembers.setOnClickListener(this);

        swipyRefreshLayoutDirection.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                getAllMembers();
            }
        });
        LinearLayout imgBackLayout = (LinearLayout) findViewById(R.id.imgBackLayout);

        imgBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();

            }
        });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.left_anim, R.anim.right_anim);
    }


    public void getAllMembers() {


        MyApplication myApplication = new MyApplication();

        if (Utility.getSharedInstance().isConnectedToInternet(this)) {


            Utility.getSharedInstance().showProgressDialog(this);


            String commId = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_COMM_ID, "");


            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().getallCommunityMembers(X_API_KEY, commId, "10", "0");
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());

                    if (responseData != null) {


                        if (responseData.get("status").getAsString().equalsIgnoreCase("true")) {
                            // Config.getPlanDAO = new GetPlanDAO();
                            Gson gson = new Gson();
                            Utility.getSharedInstance().dismissProgressDialog();

                            ResponseGetCommunityMem responseGetCommunityMem = gson.fromJson(responseData, ResponseGetCommunityMem.class);
                            responseGetCommunityMemDataLis = responseGetCommunityMem.getResultObject();

                            List<ResponseGetCommunityMemData> sortedDataList= new ArrayList<>();

                            if(responseGetCommunityMemDataLis!=null)
                            {

                                for(int i=0;i<responseGetCommunityMemDataLis.size();i++)
                                {

                                    ResponseGetCommunityMemData responseGetCommunityMemData = new ResponseGetCommunityMemData();
                                    if(responseGetCommunityMemDataLis.get(i).getAction().equalsIgnoreCase("2"))
                                    {

                                        responseGetCommunityMemData.setAction(responseGetCommunityMemDataLis.get(i).getAction());
                                        responseGetCommunityMemData.setCommunityId(responseGetCommunityMemDataLis.get(i).getCommunityId());
                                        responseGetCommunityMemData.setCommunityName(responseGetCommunityMemDataLis.get(i).getCommunityName());
                                        responseGetCommunityMemData.setCreatedAt(responseGetCommunityMemDataLis.get(i).getCreatedAt());
                                        responseGetCommunityMemData.setId(responseGetCommunityMemDataLis.get(i).getId());
                                        responseGetCommunityMemData.setRequest(responseGetCommunityMemDataLis.get(i).getRequest());

                                        responseGetCommunityMemData.setUserId(responseGetCommunityMemDataLis.get(i).getUserId());
                                        responseGetCommunityMemData.setUsername(responseGetCommunityMemDataLis.get(i).getUsername());
                                        responseGetCommunityMemData.setStatus(responseGetCommunityMemDataLis.get(i).getStatus());




                                        sortedDataList.add(responseGetCommunityMemData);
                                    }

                                }
                                if(sortedDataList!=null)
                                {
                                    getMembersList(sortedDataList);
                                }

                            }


                            swipyRefreshLayoutDirection.setRefreshing(false);


                            firstColor();
                            // JsonObject jsonObject=responseData.getAsJsonObject("respon");


                        } else {
                            Utility.getSharedInstance().dismissProgressDialog();
                            mTxtWarn.setText("No Friends found");
                            swipyRefreshLayoutDirection.setRefreshing(false);
                            Utility.getSharedInstance().dismissProgressDialog();
                            mTxtWarn.setVisibility(View.VISIBLE);
                            mListMembers.setVisibility(View.GONE);
                        }

                    } else {
                        mTxtWarn.setText("No Friends found");
                        swipyRefreshLayoutDirection.setRefreshing(false);
                        Utility.getSharedInstance().dismissProgressDialog();
                        mTxtWarn.setVisibility(View.VISIBLE);
                        mListMembers.setVisibility(View.GONE);
                    }


                    // Utility.getSharedInstance().Snackbar(toolbarLayout, "Password Changed Sucessfully");


                }

                {


                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                    Utility.getSharedInstance().dismissProgressDialog();
                    mTxtWarn.setVisibility(View.VISIBLE);
                    mListMembers.setVisibility(View.GONE);

                }


            });
        } else

        {

            Utility.getSharedInstance().dismissProgressDialog();
            mTxtWarn.setVisibility(View.VISIBLE);
            mListMembers.setVisibility(View.GONE);


        }
    }

    private void getMembersList(List<ResponseGetCommunityMemData> responseGetCommunityMemDataList) {
        if (responseGetCommunityMemDataList != null && responseGetCommunityMemDataList.size() > 0) {
            mTxtWarn.setVisibility(View.GONE);
            mListMembers.setVisibility(View.VISIBLE);
            communityMembersAdapter = new CommunityMembersAdapter(CommunityMembersActivity.this, responseGetCommunityMemDataList);
            mListMembers.setAdapter(communityMembersAdapter);
        } else {
            mTxtWarn.setVisibility(View.VISIBLE);
            mListMembers.setVisibility(View.GONE);
        }

    }

    @Override
    public void onSwiped(final RecyclerView.ViewHolder viewHolder, int direction, int position) {

        if (viewHolder instanceof CommunityMembersAdapter.HeaderHolder) {
            // get the removed item name to display it in snack bar
            final String name = responseGetCommunityMemDataLis.get(viewHolder.getAdapterPosition()).getUsername();

            // backup of removed item for undo purpose
            final ResponseGetCommunityMemData deletedItem = responseGetCommunityMemDataLis.get(viewHolder.getAdapterPosition());
            final int deletedIndex = viewHolder.getAdapterPosition();

            // remove the item from recycler view

            confrimDeleteDialog = new Dialog(this);
            confrimDeleteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            confrimDeleteDialog.setContentView(R.layout.dialog_confrim_delete);
            confrimDeleteDialog.getWindow().getAttributes().width = ActionBar.LayoutParams.FILL_PARENT;
            confrimDeleteDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
            confrimDeleteDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            confrimDeleteDialog.show();
            confrimDeleteDialog.setCancelable(false);

            Button mButYes = (Button) confrimDeleteDialog.findViewById(R.id.butYes);
            Button mButNo = (Button) confrimDeleteDialog.findViewById(R.id.butNo);

            TextView mTxtMsg = (TextView) confrimDeleteDialog.findViewById(R.id.txtMsg);
            mTxtMsg.setText("Are you sure you want to delete?");


            TextView mTxtTitle = (TextView) confrimDeleteDialog.findViewById(R.id.txtDialogTitle);
            mTxtTitle.setText("Friends");


            mButNo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    confrimDeleteDialog.dismiss();

                    communityMembersAdapter.notifyDataSetChanged();


                }
            });

            mButYes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    // showing snack bar with Undo option

                    deletMembers(responseGetCommunityMemDataLis.get(viewHolder.getAdapterPosition()).getUserId());

                    communityMembersAdapter.removeItem(viewHolder.getAdapterPosition());
//            snackbar.setAction("UNDO", new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    // undo is selected, restore the deleted item
//
//                }
//            });
//            snackbar.setActionTextColor(Color.YELLOW);
                    //snackbar.show();

                }
            });


        }

    }

    ////////////// Search Members //////////////////


    public void searchMembers(String name) {

        MyApplication myApplication = new MyApplication();


        if (Utility.getSharedInstance().isConnectedToInternet(this)) {
            Utility.getSharedInstance().showProgressDialog(this);
            RequestToSearchUser requestToSearchUser = new RequestToSearchUser();

            requestToSearchUser.setLimit("10");
            requestToSearchUser.setOffset("0");
            requestToSearchUser.setSearchterm(name);


            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().searchUsers(X_API_KEY, requestToSearchUser);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());

                    if (responseData != null) {


                        if (responseData.get("respon") != null) {

                            JsonArray jsonArray = responseData.get("respon").getAsJsonArray();


                            Gson gson = new Gson();
                            // JsonObject jsonObject=responseData.getAsJsonObject("respon");

                            if (jsonArray.size() > 0) {

                                Utility.getSharedInstance().dismissProgressDialog();

                                mTxtWarn.setVisibility(View.GONE);
                                mListMembers.setVisibility(View.GONE);
                                mListSeacrList.setVisibility(View.VISIBLE);

                                GetSearchMemberResponse getSearchMemberResponse = gson.fromJson(responseData, GetSearchMemberResponse.class);
                                List<GetSearchMemberResponseData> getSearchMemberResponseDataList = getSearchMemberResponse.getRespon();
                                SerachMemberAdapter serachMemberAdapter = new SerachMemberAdapter(CommunityMembersActivity.this, getSearchMemberResponseDataList);


                            } else {
                                Utility.getSharedInstance().dismissProgressDialog();
                                mListMembers.setVisibility(View.GONE);
                                mTxtWarn.setVisibility(View.VISIBLE);
                                mListSeacrList.setVisibility(View.GONE);
                                // getAllMembers();
                            }


                        }

                    } else {
                        Utility.getSharedInstance().dismissProgressDialog();
                        Toast.makeText(CommunityMembersActivity.this, "Api issue", Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                    Utility.getSharedInstance().dismissProgressDialog();

                    Toast.makeText(CommunityMembersActivity.this, "Api failure", Toast.LENGTH_SHORT).show();
                }


            });
        } else {

            Toast.makeText(CommunityMembersActivity.this, "Check internet connection", Toast.LENGTH_SHORT).show();
        }
    }


    /////////////////// Delete Members FROM Community ///////////////////////////////

    public void deletMembers(String memID) {

        MyApplication myApplication = new MyApplication();

        Utility.getSharedInstance().showProgressDialog(this);


        if (Utility.getSharedInstance().isConnectedToInternet(this)) {

            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");

            String comID = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_COMM_ID, "");
            // id="1";
            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");

            RequestDeleteMember requestDeleteMember = new RequestDeleteMember();

            requestDeleteMember.setCommunityId(comID);
            requestDeleteMember.setParentUserId(id);
            requestDeleteMember.setUserId(memID);

            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().deleteMembers(X_API_KEY, requestDeleteMember);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());

                    if (responseData != null) {


                        if (responseData.get("status").getAsString().equalsIgnoreCase("true")) {

                            confrimDeleteDialog.dismiss();
                            Utility.getSharedInstance().dismissProgressDialog();
                            Toast.makeText(CommunityMembersActivity.this, "Removed from community", Toast.LENGTH_SHORT).show();


                        } else {

                            Utility.getSharedInstance().dismissProgressDialog();
                            Toast.makeText(CommunityMembersActivity.this, "" + responseData.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                        }


                        communityMembersAdapter.notifyDataSetChanged();


                    } else {
                        Utility.getSharedInstance().dismissProgressDialog();
                        Toast.makeText(CommunityMembersActivity.this, "Api issue", Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                    Utility.getSharedInstance().dismissProgressDialog();

                    Toast.makeText(CommunityMembersActivity.this, "Api failure", Toast.LENGTH_SHORT).show();
                }


            });
        } else {

            Toast.makeText(CommunityMembersActivity.this, "Check internet connection", Toast.LENGTH_SHORT).show();
        }
    }


    public void getRequetstData(String userID) {

        MyApplication myApplication = new MyApplication();



        if (Utility.getSharedInstance().isConnectedToInternet(CommunityMembersActivity.this)) {
            Utility.getSharedInstance().showProgressDialog(CommunityMembersActivity.this);

            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_COMM_ID, "");

            // id="1";
            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");
//            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().getAllEvents( X_API_KEY,"10","0");
            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().getCommunitySentRequest(X_API_KEY, id, "10", "0");
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());

                    if (responseData.get("status").getAsBoolean()==true) {
                        // Config.getPlanDAO = new GetPlanDAO();
                        Gson gson = new Gson();

                        Utility.getSharedInstance().dismissProgressDialog();
;


                        GetRequestSentbyCommunity getEventPeopleResponse = gson.fromJson(responseData,GetRequestSentbyCommunity.class);

                        List<GetRequestSentbyCommunityData> getEventPeopleResponseDataList = getEventPeopleResponse.getResultObject();

                        List<GetRequestSentbyCommunityData> soretedListData = new ArrayList<>();

                        if(getEventPeopleResponseDataList!=null)
                        {
                            for(int i=0;i<getEventPeopleResponseDataList.size();i++)
                            {

                                GetRequestSentbyCommunityData getRequestSentbyCommunityData = new GetRequestSentbyCommunityData();

                                if(getEventPeopleResponseDataList.get(i).getAction().equalsIgnoreCase("1")||getEventPeopleResponseDataList.get(i).getAction().equalsIgnoreCase("3"))
                                {

                                    getRequestSentbyCommunityData.setAction(getEventPeopleResponseDataList.get(i).getAction());
                                    getRequestSentbyCommunityData.setRequest(getEventPeopleResponseDataList.get(i).getRequest());
                                    getRequestSentbyCommunityData.setCommunityId(getEventPeopleResponseDataList.get(i).getCommunityId());
                                    getRequestSentbyCommunityData.setCommunityName(getEventPeopleResponseDataList.get(i).getCommunityName());
                                    getRequestSentbyCommunityData.setCreatedAt(getEventPeopleResponseDataList.get(i).getCreatedAt());
                                    getRequestSentbyCommunityData.setId(getEventPeopleResponseDataList.get(i).getId());

                                    getRequestSentbyCommunityData.setUserId(getEventPeopleResponseDataList.get(i).getUserId());
                                    getRequestSentbyCommunityData.setUsername(getEventPeopleResponseDataList.get(i).getUsername());
                                    getRequestSentbyCommunityData.setStatus(getEventPeopleResponseDataList.get(i).getStatus());

                                    soretedListData.add(getRequestSentbyCommunityData);
                                }
                            }
                        }




                        if(soretedListData!=null)
                        {
                            if(soretedListData.size()>0)
                            {
                                mListMembers.setVisibility(View.VISIBLE);
                                mTxtWarn.setVisibility(View.GONE);
//                                CommunityRequestAdapter communityRequestAdapter = new CommunityRequestAdapter(g,soretedListData);
//                                mListMembers.setAdapter(communityRequestAdapter);
                            }
                            else {
                                mTxtWarn.setText("No request found");
                                mTxtWarn.setVisibility(View.VISIBLE);
                                mListMembers.setVisibility(View.GONE);
                            }
                        }
                        else {

                            mTxtWarn.setText("No request found");
                            mTxtWarn.setVisibility(View.VISIBLE);
                            mListMembers.setVisibility(View.GONE);

                        }









                        // JsonObject jsonObject=responseData.getAsJsonObject("respon");








                        // Utility.getSharedInstance().Snackbar(toolbarLayout, "Password Changed Sucessfully");


                    } else {

                        Toast.makeText(CommunityMembersActivity.this, "No request found", Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                    Utility.getSharedInstance().dismissProgressDialog();
                    Toast.makeText(CommunityMembersActivity.this, "API Issue", Toast.LENGTH_SHORT).show();


                }


            });
        } else {

            Toast.makeText(CommunityMembersActivity.this, "Check Internet Connection", Toast.LENGTH_SHORT).show();

        }
    }



    private void firstColor() {
        mTxtRequest.setTextColor(getResources().getColor(R.color.white));
        mTxtMember.setTextColor(getResources().getColor(R.color.black));

        mLayMembers.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        mLayRequest.setBackgroundColor(getResources().getColor(R.color.darker_grey));
    }



    public void acceptOrRejectRequest(String comId, String actionID,String ID) {


//        Utility.getSharedInstance().showProgressDialog(this);
        MyApplication myApplication = new MyApplication();


        if (Utility.getSharedInstance().isConnectedToInternet(this)) {
            Utility.getSharedInstance().showProgressDialog(this);

            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");

            // id="1";
            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");

            RequestToAcceptCommunity requestGetMyPlayList = new RequestToAcceptCommunity();

            requestGetMyPlayList.setAction(actionID);
            requestGetMyPlayList.setCommunityId(comId);
            requestGetMyPlayList.setUserId(ID);

            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().acceptOrRejectRequest(X_API_KEY, requestGetMyPlayList);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());

                    if (responseData.get("status").getAsBoolean() == true) {

                        Utility.getSharedInstance().dismissProgressDialog();

                        Toast.makeText(CommunityMembersActivity.this, "" + responseData.get("message").getAsString(), Toast.LENGTH_SHORT).show();

                    } else {
                        Utility.getSharedInstance().dismissProgressDialog();

                        Toast.makeText(CommunityMembersActivity.this, "" + responseData.get("message").getAsString(), Toast.LENGTH_SHORT).show();

                    }


                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                    Utility.getSharedInstance().dismissProgressDialog();

                    Toast.makeText(CommunityMembersActivity.this, "Api failure", Toast.LENGTH_SHORT).show();
                }


            });
        } else {

            Toast.makeText(CommunityMembersActivity.this, "No Internet coonection", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        getAllMembers();
    }
}
