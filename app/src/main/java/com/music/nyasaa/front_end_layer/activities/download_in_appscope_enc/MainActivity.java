package com.music.nyasaa.front_end_layer.activities.download_in_appscope_enc;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;


import com.music.nyasaa.R;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;




public class MainActivity extends AppCompatActivity {

    public static final String MESSAGE_PROGRESS = "message_progress";
    private static final int PERMISSION_REQUEST_CODE = 1;
    ProgressBar mProgressBar;
    TextView  mProgressText;
    Button  btn_download;
    public static  Cipher cipher = null;



//    @BindView(R.id.progress) ProgressBar mProgressBar;
//    @BindView(R.id.progress_text) TextView mProgressText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //ButterKnife.bind(this);

        SecretKeySpec sks = new SecretKeySpec("ABCDEF".getBytes(),
                "AES");

        try {
            cipher = Cipher.getInstance("AES");
            try {
                cipher.init(Cipher.ENCRYPT_MODE, sks);
            } catch (InvalidKeyException e) {
                e.printStackTrace();
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }

         mProgressBar=(ProgressBar) findViewById(R.id.progress);
          mProgressText =(TextView) findViewById(R.id.progress_text);
         btn_download =(Button) findViewById(R.id.btn_download);

        btn_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(checkPermission()){
                    startDownload();
                } else {
                    requestPermission();
                }

            }
        });


        registerReceiver();
    }



    private void startDownload(){

        // Add to Databse and put extra name of file
        // set JOB NO.
        //Add to Databse and put extra name of file ,file  URL, type of file, base url

        String file_Name="DMND%20-%20Navv%20Inder(MyMp3Song).mp3";
        Intent intent = new Intent(this,DownloadService.class);
        String URL="http://olavideos.s3.amazonaws.com/music/DMND%20-%20Navv%20Inder(MyMp3Song).mp3";

        intent.putExtra("File_Name", file_Name.replace(" ","%20"));
        intent.putExtra("Type", "mp3");
        intent.putExtra("URL", URL);
        intent.putExtra("Image", "ImageURL");

        startService(intent);

    }

    void getDownloadedFiles()
    {
//        File fileo = new File(getApplicationContext().getFilesDir(), Filenames+"."+Type);
//
//        //File fileo = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "ddfileENC");
//        //  File filei = new File(getApplicationContext().getFilesDir(), inPath);
//        String  outPath=fileo.getAbsolutePath();

      //  file name from DB
    }



    private void registerReceiver(){

        LocalBroadcastManager bManager = LocalBroadcastManager.getInstance(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(MESSAGE_PROGRESS);
        bManager.registerReceiver(broadcastReceiver, intentFilter);

    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if(intent.getAction().equals(MESSAGE_PROGRESS)){

                Download download = intent.getParcelableExtra("download");
                mProgressBar.setProgress(download.getProgress());
                if(download.getProgress() == 100){

                    mProgressText.setText("File Download Complete");

                } else {

                    mProgressText.setText(String.format("Downloaded (%d/%d) MB",download.getCurrentFileSize(),download.getTotalFileSize()));

                }
            }
        }
    };

    private boolean checkPermission(){
        int result = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED){

            return true;

        } else {

            return false;
        }
    }

    private void requestPermission(){

        ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},PERMISSION_REQUEST_CODE);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                        startDownload();
                } else {

                    Snackbar.make(findViewById(R.id.coordinatorLayout),"Permission Denied, Please allow to proceed !", Snackbar.LENGTH_LONG).show();

                }
                break;
        }
    }




}
