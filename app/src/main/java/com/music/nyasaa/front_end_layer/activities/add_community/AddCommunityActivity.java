package com.music.nyasaa.front_end_layer.activities.add_community;

import android.Manifest;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.music.nyasaa.R;
import com.music.nyasaa.controller.MyApplication;
import com.music.nyasaa.models.RequestModel.AddCommunityModel;
import com.music.nyasaa.models.RequestModel.AddToCommunity.UpdateCommunity;
import com.music.nyasaa.models.ResponseModel.GetImageUrl.GetImageResponse;
import com.music.nyasaa.models.ResponseModel.GetImageUrl.GetImageResponseData;
import com.music.nyasaa.utility.CommonUtils;
import com.music.nyasaa.utility.SharedPreferenceManager;
import com.music.nyasaa.utility.Utility;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.listeners.IPickCancel;
import com.vansuita.pickimage.listeners.IPickResult;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.music.nyasaa.retrofit.Config.X_API_KEY;

public class AddCommunityActivity extends AppCompatActivity implements View.OnClickListener {

    private MyApplication myApplication;
    private Button mButSubmit;

    private EditText mEditName, mEditDescription, mEditShortDes;
    private String getPath = "";
    private RequestBody mFile;
    private int REQUEST_WRITE_PERMISSION = 1;
    private int PERMISSION_ALL = 1;

    private String imgURL = "";
    private String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};

    @BindView(R.id.editPhoto)
    private EditText mEditPhoto;

    @BindView(R.id.setImage)
    private ImageView setImage;
    private Bundle intent;
    private String sCommName="";
    private String comID="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_community);
        ButterKnife.bind(this);

        initView();
        onClick();
    }

    private void initView() {

        intent = getIntent().getExtras();


        TextView mTxtToolbarText = (TextView) findViewById(R.id.txtTitleToolbar);
        mTxtToolbarText.setText("Add Community");


        mEditName = (EditText) findViewById(R.id.editname);
        mEditDescription = (EditText) findViewById(R.id.editDescription);
        mEditShortDes = (EditText) findViewById(R.id.editShortDescription);
        mButSubmit = (Button) findViewById(R.id.butSubmit);
        mEditPhoto = (EditText) findViewById(R.id.editPhoto);
        setImage = (ImageView) findViewById(R.id.setImage);


        if(intent!=null)
        {
            sCommName = intent.getString("name");
            comID = intent.getString("id");
            mEditName.setText(""+sCommName);
            mTxtToolbarText.setText("Update");
        }
        else {
            mTxtToolbarText.setText("Add Community");
        }



    }

    private void onClick() {


        LinearLayout imgBackLayout = (LinearLayout) findViewById(R.id.imgBackLayout);
        imgBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();

            }
        });

        mButSubmit.setOnClickListener(this);
        mEditPhoto.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.butSubmit:

                if (mEditName.getText().toString().trim().equalsIgnoreCase("")) {

                    Toast.makeText(this, "Enter community name", Toast.LENGTH_SHORT).show();
                }
                else {

                    if(intent!=null)
                    {

                        if (!getPath.equals("")) {
                            sendFileUpdate();
                        } else {
                            updateCommunity();
                        }


                    }
                    else {
                        if (!getPath.equals("")) {
                            sendFile();
                        } else {
                            addCommunity();
                        }

                    }

                }

//                else if (mEditDescription.getText().toString().trim().equalsIgnoreCase("")) {
//
//                    Toast.makeText(this, "Enter long descrition", Toast.LENGTH_SHORT).show();
//
//                } else if (mEditShortDes.getText().toString().trim().equalsIgnoreCase("")) {
//
//                    Toast.makeText(this, "Enter short descrition", Toast.LENGTH_SHORT).show();
//                }










                break;

            case R.id.editPhoto:

                getPath = "";
                if (!Utility.hasPermissions(AddCommunityActivity.this, PERMISSIONS)) {
                    ActivityCompat.requestPermissions(AddCommunityActivity.this, PERMISSIONS, PERMISSION_ALL);
                } else {
                    PickImageDialog.build(new PickSetup())
                            .setOnPickResult(new IPickResult() {
                                @Override
                                public void onPickResult(PickResult r) {

                                    try {
                                        setImage.setImageURI(null);

                                        //Setting the real returned image.
                                        setImage.setImageURI(r.getUri());

                                        //PickImageDialog.build().dismiss();
                                        //If you want the Bitmap.
                                        setImage.setImageBitmap(r.getBitmap());
                                        setImage.setVisibility(View.VISIBLE);


                                        // bitmapToBase64(r.getBitmap());
                                        //getPath(r.getUri());
                                        getPath = r.getPath();

//                                sendFile();

                                        // videoFilePath = (r.getUri()).getData();
                                        //videoFilePath=(r.getUri()).getPath();
//
//                                //PickImageDialog.build().dismiss();
//                                //If you want the Bitmap.
//                                imageView.setImageBitmap(r.getBitmap());
                                        // Toast.makeText(AddPostActivity.this, r.getError().getMessage(), Toast.LENGTH_LONG).show();
                                    } catch (OutOfMemoryError | Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            })
                            .setOnPickCancel(new IPickCancel() {
                                @Override
                                public void onCancelClick() {

                                }
                            }).show(AddCommunityActivity.this);
                }
                break;
        }

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.left_anim, R.anim.right_anim);
    }


    public void addCommunity() {


        if (Utility.getSharedInstance().isConnectedToInternet(AddCommunityActivity.this)) {
//            Utility.getSharedInstance().showProgressDialog(this);
            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");


            myApplication = new MyApplication();
            AddCommunityModel addCommunityModel = new AddCommunityModel();


            addCommunityModel.setParentUserId(id);
            addCommunityModel.setType("1");
            addCommunityModel.setImageUrl(imgURL);
            addCommunityModel.setLongInfo("Android OSHO");
            addCommunityModel.setShortInfo("Android OSHO");
            addCommunityModel.setName(mEditName.getText().toString().trim());


            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().addCommunity(X_API_KEY, addCommunityModel);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());

                    if (responseData != null)

                    {
                        Utility.getSharedInstance().dismissProgressDialog();

                        if (responseData.get("status").getAsString().equalsIgnoreCase("false")) {


                            Utility.getSharedInstance().dismissProgressDialog();
                            Toast.makeText(AddCommunityActivity.this, "" + responseData.get("message").getAsString(), Toast.LENGTH_SHORT).show();


                        } else {
                            Utility.getSharedInstance().dismissProgressDialog();
                            Toast.makeText(AddCommunityActivity.this, "Community added to list", Toast.LENGTH_SHORT).show();
                            finish();

                        }

                    } else {

                        Utility.getSharedInstance().dismissProgressDialog();
                        Toast.makeText(AddCommunityActivity.this, "" + responseData.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Toast.makeText(AddCommunityActivity.this, "failed", Toast.LENGTH_SHORT).show();

                    Utility.getSharedInstance().dismissProgressDialog();
                }


            });
        } else {

            Utility.getSharedInstance().dismissProgressDialog();

            Toast.makeText(this, "No internet connection available", Toast.LENGTH_SHORT).show();
        }
    }


    // update Community

    public void updateCommunity() {


        if (Utility.getSharedInstance().isConnectedToInternet(AddCommunityActivity.this)) {
//            Utility.getSharedInstance().showProgressDialog(this);
            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");


            myApplication = new MyApplication();
            UpdateCommunity updateCommunity = new UpdateCommunity();


            updateCommunity.setParentUserId(Integer.parseInt(id));
            updateCommunity.setType(1);
            updateCommunity.setCommunityId(Integer.parseInt(comID));
            updateCommunity.setImageUrl(imgURL);
            updateCommunity.setLongInfo("Android OSHO");
            updateCommunity.setShortInfo("Android OSHO");
            updateCommunity.setName(mEditName.getText().toString().trim());


            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().updateComm(X_API_KEY, updateCommunity);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());

                    if (responseData != null)

                    {
                        Utility.getSharedInstance().dismissProgressDialog();

                        if (responseData.get("status").getAsString().equalsIgnoreCase("false")) {


                            Utility.getSharedInstance().dismissProgressDialog();
                            Toast.makeText(AddCommunityActivity.this, "" + responseData.get("message").getAsString(), Toast.LENGTH_SHORT).show();


                        } else {
                            Utility.getSharedInstance().dismissProgressDialog();
                            Toast.makeText(AddCommunityActivity.this, "Community updated", Toast.LENGTH_SHORT).show();
                            finish();

                        }

                    } else {

                        Utility.getSharedInstance().dismissProgressDialog();
                        Toast.makeText(AddCommunityActivity.this, "" + responseData.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Toast.makeText(AddCommunityActivity.this, "failed", Toast.LENGTH_SHORT).show();

                    Utility.getSharedInstance().dismissProgressDialog();
                }


            });
        } else {

            Utility.getSharedInstance().dismissProgressDialog();

            Toast.makeText(this, "No internet connection available", Toast.LENGTH_SHORT).show();
        }
    }


    /// send File

    private void sendFile() {
        //File compressedImageFile=null;
        myApplication = new MyApplication();


        Utility.getSharedInstance().showProgressDialog(this);


        //Utility.getSharedInstance().ShowProgessDialog(AddPostActivity.this, "Attaching your file", "Please wait");

        File file = new File(getPath);

        if (getPath != null && !getPath.equals("")) {

            file = new File(CommonUtils.compressImage(getPath));

            if (file == null)
                file = new File(getPath);
        }

        mFile = RequestBody.create(MediaType.parse("image/*"), file);
//        if (mTxtDocName.getText().toString().trim().equalsIgnoreCase("image attached")) {
//            mFile = RequestBody.create(MediaType.parse("image/*"), file);
//        } else if (mTxtDocName.getText().toString().trim().equalsIgnoreCase("document attached")) {
//            mFile = RequestBody.create(MediaType.parse("*/*"), file);
//        } else {
//            mFile = RequestBody.create(MediaType.parse("video/*"), file);
//        }


        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("file", file.getName(), mFile);


        //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());


        Call<JsonObject> callbackLogin = (Call<JsonObject>) myApplication.getAPIInstance().uploadDocument(X_API_KEY, fileToUpload, mFile);
        callbackLogin.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                JsonObject responseData = response.body();


                if (responseData != null) {


                    Gson gson = new Gson();

                    GetImageResponse getImageResponse = gson.fromJson(responseData, GetImageResponse.class);
                    GetImageResponseData getCommunityResponseData = getImageResponse.getUploadData();


                    imgURL = getCommunityResponseData.getFileName();


                    addCommunity();


                    //Utility.getSharedInstance().DisMissProgressDialog();


                } else {
                    Utility.getSharedInstance().dismissProgressDialog();
                    //sendPost();
                    Toast.makeText(AddCommunityActivity.this, "Failed to attach image", Toast.LENGTH_SHORT).show();

                }


            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Utility.getSharedInstance().dismissProgressDialog();
                //sendPost();

                Toast.makeText(AddCommunityActivity.this, "Excepion Error", Toast.LENGTH_SHORT).show();
            }


        });

    }

    // send Update Images

    private void sendFileUpdate() {
        //File compressedImageFile=null;
        myApplication = new MyApplication();


        Utility.getSharedInstance().showProgressDialog(this);


        //Utility.getSharedInstance().ShowProgessDialog(AddPostActivity.this, "Attaching your file", "Please wait");

        File file = new File(getPath);

        if (getPath != null && !getPath.equals("")) {

            file = new File(CommonUtils.compressImage(getPath));

            if (file == null)
                file = new File(getPath);
        }

        mFile = RequestBody.create(MediaType.parse("image/*"), file);
//        if (mTxtDocName.getText().toString().trim().equalsIgnoreCase("image attached")) {
//            mFile = RequestBody.create(MediaType.parse("image/*"), file);
//        } else if (mTxtDocName.getText().toString().trim().equalsIgnoreCase("document attached")) {
//            mFile = RequestBody.create(MediaType.parse("*/*"), file);
//        } else {
//            mFile = RequestBody.create(MediaType.parse("video/*"), file);
//        }


        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("file", file.getName(), mFile);


        //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());


        Call<JsonObject> callbackLogin = (Call<JsonObject>) myApplication.getAPIInstance().uploadDocument(X_API_KEY, fileToUpload, mFile);
        callbackLogin.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                JsonObject responseData = response.body();


                if (responseData != null) {


                    Gson gson = new Gson();

                    GetImageResponse getImageResponse = gson.fromJson(responseData, GetImageResponse.class);
                    GetImageResponseData getCommunityResponseData = getImageResponse.getUploadData();


                    imgURL = getCommunityResponseData.getFileName();


                    updateCommunity();


                    //Utility.getSharedInstance().DisMissProgressDialog();


                } else {
                    Utility.getSharedInstance().dismissProgressDialog();
                    //sendPost();
                    Toast.makeText(AddCommunityActivity.this, "Failed to attach image", Toast.LENGTH_SHORT).show();

                }


            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Utility.getSharedInstance().dismissProgressDialog();
                //sendPost();

                Toast.makeText(AddCommunityActivity.this, "Excepion Error", Toast.LENGTH_SHORT).show();
            }


        });

    }

}
