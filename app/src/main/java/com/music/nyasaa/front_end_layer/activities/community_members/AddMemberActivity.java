package com.music.nyasaa.front_end_layer.activities.community_members;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.music.nyasaa.R;
import com.music.nyasaa.adapter.SerachMemberAdapter;
import com.music.nyasaa.controller.MyApplication;
import com.music.nyasaa.models.RequestModel.AddToCommunity.RequestToAddMember;
import com.music.nyasaa.models.RequestModel.SerachUsers.RequestToSearchUser;
import com.music.nyasaa.models.ResponseModel.GetSeacrchedMembers.GetSearchMemberResponse;
import com.music.nyasaa.models.ResponseModel.GetSeacrchedMembers.GetSearchMemberResponseData;
import com.music.nyasaa.utility.RecyclerItemClickListener;
import com.music.nyasaa.utility.SharedPreferenceManager;
import com.music.nyasaa.utility.Utility;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.music.nyasaa.retrofit.Config.X_API_KEY;

public class AddMemberActivity extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView mListSeacrList;
    private TextView mTxtWarn;
    private SwipyRefreshLayout swipyRefreshLayoutDirection;
    public static List<GetSearchMemberResponseData> mSongss;
    private SerachMemberAdapter serachMemberAdapter;
    private EditText mEditSearch;
    private String userID, commId;
    private Dialog addMemberDialog;
    private int currentPage = 0;
    boolean isSearch = false;

    @Override
    public void onClick(View v) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_members);

        initView();
        onClick();
    }

    private void initView() {

        TextView mTxtToolbarText = (TextView) findViewById(R.id.txtTitleToolbar);
        mTxtToolbarText.setText("Add Friends");

        TextView mTxtToolbaTxt = (TextView) findViewById(R.id.txtTitleToolbar);
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.toolLayout);


        mTxtWarn = (TextView) findViewById(R.id.txtWarn);
        mEditSearch = (EditText) findViewById(R.id.editSearch);
        mTxtWarn.setVisibility(View.VISIBLE);

        mTxtWarn.setText("Search to add friends");

        swipyRefreshLayoutDirection = (SwipyRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        swipyRefreshLayoutDirection.setDirection(SwipyRefreshLayoutDirection.BOTH);

        mListSeacrList = (RecyclerView) findViewById(R.id.listViewSearchMembers);
        RecyclerView.LayoutManager linearLayoutManagers = new LinearLayoutManager(this);
        mListSeacrList.setLayoutManager(linearLayoutManagers);
        mListSeacrList.setItemAnimator(new DefaultItemAnimator());

        userID = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "none");
        commId = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_COMM_ID, "none");
        currentPage = 0;
        mEditSearch.setText("");
    }

    private void onClick() {

        LinearLayout imgBackLayout = (LinearLayout) findViewById(R.id.imgBackLayout);

        imgBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        mEditSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

               /* if ((mEditSearch.getText().toString().trim().length() > 2)) {
                    searchMembers(mEditSearch.getText().toString().trim());
                }*/
                isSearch = true;
                if (mEditSearch.getText().toString().length() == 0)
                    searchMembers(mEditSearch.getText().toString().trim());
                else if (mEditSearch.getText().toString().length() == 1)
                    searchMembers(mEditSearch.getText().toString().trim());
                else if (mEditSearch.getText().toString().length() > 0)
                    searchMembers(mEditSearch.getText().toString().trim());
                else if (mEditSearch.getText().toString().length() > 1)
                    searchMembers(mEditSearch.getText().toString().trim());
                else if (mEditSearch.getText().toString().length() > 2)
                    searchMembers(mEditSearch.getText().toString().trim());
                else if (mEditSearch.getText().toString().length() > 3)
                    searchMembers(mEditSearch.getText().toString().trim());
                else if (mEditSearch.getText().toString().length() > 4)
                    searchMembers(mEditSearch.getText().toString().trim());
                else if (mEditSearch.getText().toString().length() > 5)
                    searchMembers(mEditSearch.getText().toString().trim());
            }

            @Override
            public void afterTextChanged(Editable s) {

//                if((mEditSearch.getText().toString().trim().length()>2))
//                {
//                    searchMembers(mEditSearch.getText().toString().trim());
//                }
            }
        });

        swipyRefreshLayoutDirection.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                if(direction == SwipyRefreshLayoutDirection.TOP){
                    if(isSearch)
                        searchMembers(mEditSearch.getText().toString().trim());
                    else
                        getAllMembers(0);
                    currentPage = 0;
                } else {
                    if(isSearch)
                        searchMembers(mEditSearch.getText().toString().trim());
                    else
                        getAllMembers(currentPage);
                }
            }
        });
    }

    public void searchMembers(String name) {

        MyApplication myApplication = new MyApplication();


        if (Utility.getSharedInstance().isConnectedToInternet(this)) {
            //Utility.getSharedInstance().showProgressDialog(this);
            RequestToSearchUser requestToSearchUser = new RequestToSearchUser();

            requestToSearchUser.setLimit("200");
            requestToSearchUser.setOffset("0");
            requestToSearchUser.setSearchterm(name);


            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().searchUsers(X_API_KEY, requestToSearchUser);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());

                    if (responseData != null) {


                        if (responseData.get("respon") != null) {

                            JsonArray jsonArray = responseData.get("respon").getAsJsonArray();


                            Gson gson = new Gson();
                            // JsonObject jsonObject=responseData.getAsJsonObject("respon");

                            if (jsonArray.size() > 0) {

                                //Utility.getSharedInstance().dismissProgressDialog();
                                swipyRefreshLayoutDirection.setRefreshing(false);
                                mTxtWarn.setVisibility(View.GONE);
                                mListSeacrList.setVisibility(View.VISIBLE);

                                GetSearchMemberResponse getSearchMemberResponse = gson.fromJson(responseData, GetSearchMemberResponse.class);
                                List<GetSearchMemberResponseData> getSearchMemberResponseDataList = getSearchMemberResponse.getRespon();

                                getMembersList(getSearchMemberResponseDataList);


                            } else {
                                //Utility.getSharedInstance().dismissProgressDialog();

                                swipyRefreshLayoutDirection.setRefreshing(false);
                                mListSeacrList.setVisibility(View.GONE);
                                mTxtWarn.setText("Friends not found");
                                mTxtWarn.setVisibility(View.VISIBLE);
                                // getAllMembers();
                            }


                        }

                    } else {
                        swipyRefreshLayoutDirection.setRefreshing(false);
                        mListSeacrList.setVisibility(View.GONE);
                        mTxtWarn.setText("Friends not found");
                        mTxtWarn.setVisibility(View.VISIBLE);
                        //Utility.getSharedInstance().dismissProgressDialog();
                        Toast.makeText(AddMemberActivity.this, "Api issue", Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    swipyRefreshLayoutDirection.setRefreshing(false);
                    //Utility.getSharedInstance().dismissProgressDialog();
                    mListSeacrList.setVisibility(View.GONE);
                    mTxtWarn.setText("Friends not found");
                    mTxtWarn.setVisibility(View.VISIBLE);

                    Toast.makeText(AddMemberActivity.this, "Api failure", Toast.LENGTH_SHORT).show();
                }


            });
        } else {
            swipyRefreshLayoutDirection.setRefreshing(false);
            mListSeacrList.setVisibility(View.GONE);
            mTxtWarn.setText("Friends not found");
            mTxtWarn.setVisibility(View.VISIBLE);
            Toast.makeText(AddMemberActivity.this, "Check internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    public void getAllMembers(final int currentPage) {

        MyApplication myApplication = new MyApplication();

        if (Utility.getSharedInstance().isConnectedToInternet(this)) {
            Utility.getSharedInstance().showProgressDialog(this);
            RequestToSearchUser requestToSearchUser = new RequestToSearchUser();

            requestToSearchUser.setLimit("10");
            requestToSearchUser.setOffset(String.valueOf(currentPage));
            requestToSearchUser.setSearchterm("");

            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().searchUsers(X_API_KEY, requestToSearchUser);
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    JsonObject responseData = response.body();

                    if (responseData != null) {

                        if (responseData.get("respon") != null) {
                            Utility.getSharedInstance().dismissProgressDialog();
                            JsonArray jsonArray = responseData.get("respon").getAsJsonArray();
                            Gson gson = new Gson();

                            if (jsonArray.size() > 0) {
                                mTxtWarn.setVisibility(View.GONE);
                                mListSeacrList.setVisibility(View.VISIBLE);
                                swipyRefreshLayoutDirection.setRefreshing(false);
                                if (currentPage == 0) {
                                    GetSearchMemberResponse getSearchMemberResponse = gson.fromJson(responseData, GetSearchMemberResponse.class);
                                    mSongss = getSearchMemberResponse.getRespon();
                                    getMusicList(mSongss);
                                } else {
                                    GetSearchMemberResponse getSearchMemberResponse = gson.fromJson(responseData, GetSearchMemberResponse.class);
                                    List<GetSearchMemberResponseData> getSearchMemberResponseDataList = getSearchMemberResponse.getRespon();
                                    mSongss.addAll(mSongss.size(), getSearchMemberResponseDataList);
                                    getUpdatedList();
                                }
                                if (mSongss != null) {
                                    if (mSongss.size() < 0) {
                                        swipyRefreshLayoutDirection.setRefreshing(false);
                                        mTxtWarn.setVisibility(View.VISIBLE);
                                        mListSeacrList.setVisibility(View.GONE);
                                    }
                                }

                            } else {
                                //Utility.getSharedInstance().dismissProgressDialog();
                                swipyRefreshLayoutDirection.setRefreshing(false);
                                Utility.getSharedInstance().dismissProgressDialog();
                                if (mSongss != null && mSongss.size() > 0) {
                                    mListSeacrList.setVisibility(View.VISIBLE);
                                    mTxtWarn.setVisibility(View.GONE);
                                    Toast.makeText(AddMemberActivity.this, "No more friends", Toast.LENGTH_SHORT).show();
                                } else {
                                    mListSeacrList.setVisibility(View.GONE);
                                    mTxtWarn.setText("Friends not found");
                                    mTxtWarn.setVisibility(View.VISIBLE);
                                }

                               /* mListSeacrList.setVisibility(View.GONE);
                                mTxtWarn.setText("Friends not found");
                                mTxtWarn.setVisibility(View.VISIBLE);*/
                                // getAllMembers();
                            }


                        }

                    } else {
                        swipyRefreshLayoutDirection.setRefreshing(false);
                        mListSeacrList.setVisibility(View.GONE);
                        mTxtWarn.setText("Friends not found");
                        mTxtWarn.setVisibility(View.VISIBLE);
                        Utility.getSharedInstance().dismissProgressDialog();
                        Toast.makeText(AddMemberActivity.this, "Api issue", Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                    Utility.getSharedInstance().dismissProgressDialog();
                    mListSeacrList.setVisibility(View.GONE);
                    mTxtWarn.setText("Friends not found");
                    mTxtWarn.setVisibility(View.VISIBLE);
                    //Toast.makeText(AddMemberActivity.this, "Api failure", Toast.LENGTH_SHORT).show();
                }


            });
        } else {
            /*mListSeacrList.setVisibility(View.GONE);
            mTxtWarn.setText("Friends not found");
            mTxtWarn.setVisibility(View.VISIBLE);*/
            Utility.getSharedInstance().dismissProgressDialog();
            Toast.makeText(AddMemberActivity.this, "Check internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void getMusicList(List<GetSearchMemberResponseData> songDetailList) {
        serachMemberAdapter = new SerachMemberAdapter(AddMemberActivity.this, songDetailList);
        mListSeacrList.setAdapter(serachMemberAdapter);

        if(serachMemberAdapter!=null)
            serachMemberAdapter.notifyDataSetChanged();
        currentPage = currentPage+10;
    }

    private void getUpdatedList() {
        if(serachMemberAdapter!=null)
            serachMemberAdapter.notifyDataSetChanged();
        currentPage = currentPage+10;
    }

    ////// Add Members

    public void addMemberToCommunity(String personID) {

        MyApplication myApplication = new MyApplication();


        if (Utility.getSharedInstance().isConnectedToInternet(this)) {
            Utility.getSharedInstance().showProgressDialog(this);
            RequestToAddMember requestToSearchUser = new RequestToAddMember();

            requestToSearchUser.setCommunityId(commId);
            requestToSearchUser.setParentUserId(userID);
            requestToSearchUser.setUserId(personID);
            requestToSearchUser.setRequest("1");


            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().addMemberToComm(X_API_KEY, requestToSearchUser);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());

                    if (responseData != null) {


                        if (responseData.get("status").getAsString().equalsIgnoreCase("true")) {

                            addMemberDialog.dismiss();
                            Utility.getSharedInstance().dismissProgressDialog();
                            Toast.makeText(AddMemberActivity.this, "" + responseData.get("message").getAsString(), Toast.LENGTH_SHORT).show();

                        } else {

                            Utility.getSharedInstance().dismissProgressDialog();
                            Toast.makeText(AddMemberActivity.this, "" + responseData.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                        }


                    } else {

                        Utility.getSharedInstance().dismissProgressDialog();
                        Toast.makeText(AddMemberActivity.this, "Api issue", Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                    Utility.getSharedInstance().dismissProgressDialog();


                    Toast.makeText(AddMemberActivity.this, "Api failure", Toast.LENGTH_SHORT).show();
                }


            });

        } else {
            mListSeacrList.setVisibility(View.GONE);
            mTxtWarn.setText("Friends not found");
            mTxtWarn.setVisibility(View.VISIBLE);
            Toast.makeText(AddMemberActivity.this, "Check internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void getMembersList(final List<GetSearchMemberResponseData> getSearchMemberResponseDataList) {

        SerachMemberAdapter serachMemberAdapter = new SerachMemberAdapter(AddMemberActivity.this, getSearchMemberResponseDataList);
        mListSeacrList.setAdapter(serachMemberAdapter);

        mListSeacrList.addOnItemTouchListener(
                new RecyclerItemClickListener(AddMemberActivity.this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, final int position) {
                        // TODO Handle item click

//                        addMemberToCommunity(getSearchMemberResponseDataList.get(position).getId());


                        //getMyPlayListResponseData.get(position).getName();

                        // Toast.makeText(fragmentMySongs.getContext(), "ID:-" +songID+ "PlayListName: " + getMyPlayListResponseData.get(position).getName(), Toast.LENGTH_SHORT).show();
                    }
                })
        );
    }


    public void showMembersDialog(String sName, final String sID) {


        addMemberDialog = new Dialog(AddMemberActivity.this);
        addMemberDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        addMemberDialog.setContentView(R.layout.dialog_confrim_delete);
        addMemberDialog.getWindow().getAttributes().width = ActionBar.LayoutParams.FILL_PARENT;
        addMemberDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        addMemberDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        addMemberDialog.show();

        Button mButYes = (Button) addMemberDialog.findViewById(R.id.butYes);
        Button mButNo = (Button) addMemberDialog.findViewById(R.id.butNo);

        TextView mTxtMsg = (TextView) addMemberDialog.findViewById(R.id.txtMsg);
        mTxtMsg.setText("Are you sure you want to add " + sName + " ?");


        TextView mTxtTitle = (TextView) addMemberDialog.findViewById(R.id.txtDialogTitle);
        mTxtTitle.setText("Add Friend");


        mButNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addMemberDialog.dismiss();


            }
        });

        mButYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                addMemberToCommunity(sID);


            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.left_anim, R.anim.right_anim);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getAllMembers(0);
    }
}