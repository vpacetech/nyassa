package com.music.nyasaa.front_end_layer.activities.dashbaord;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;


import com.music.nyasaa.R;

import com.music.nyasaa.adapter.AutoPlayInit;
import com.music.nyasaa.adapter.LanguageAdapter;
import com.music.nyasaa.adapter.MyPlayListAdapters;
import com.music.nyasaa.adapter.NavAdapter;
import com.music.nyasaa.adapter.NotificationAdapter;
import com.music.nyasaa.adapter.ShareToCommunityAdapter;
import com.music.nyasaa.controller.MyApplication;
import com.music.nyasaa.dbhandler.DBModel.TblUserMst;
import com.music.nyasaa.dbhandler.MusicSQLiteHelper;
import com.music.nyasaa.fragments.FragmentAlbumList;
import com.music.nyasaa.fragments.FragmentDashBoard;
import com.music.nyasaa.fragments.FragmentHowToUse;
import com.music.nyasaa.fragments.FragmentReturnPolicy;
import com.music.nyasaa.fragments.FragmentTerms;
import com.music.nyasaa.fragments.Privcay_policy;
import com.music.nyasaa.fragments.community.FragmentCommunityDetails;
import com.music.nyasaa.fragments.community.FragmentCommunityDetailsMem;
import com.music.nyasaa.fragments.discourses.FragmentAudioDiscourses;
import com.music.nyasaa.fragments.discourses.FragmentVideoDiscoureses;
import com.music.nyasaa.fragments.downloads.FragmentDownloads;
import com.music.nyasaa.fragments.interviews.FragmentInterview;
import com.music.nyasaa.fragments.meditation_center.FragmentMediatitationTch;
import com.music.nyasaa.fragments.music.FragmentMusic;
import com.music.nyasaa.fragments.mysongs.FragmentMySongs;
import com.music.nyasaa.fragments.playlist.FragmentPlayList;
import com.music.nyasaa.fragments.playlistSongs.FragmentPlayListSongs;
import com.music.nyasaa.fragments.profile.FragmentProfile;
import com.music.nyasaa.fragments.terms_and_conditions.FragmentTermsAndConditions;
import com.music.nyasaa.front_end_layer.activities.PaymentActivity;
import com.music.nyasaa.front_end_layer.activities.SearchCommunityActivity;
import com.music.nyasaa.front_end_layer.activities.add_community.AddCommunityActivity;
import com.music.nyasaa.front_end_layer.activities.community_members.AddMemberActivity;
import com.music.nyasaa.front_end_layer.activities.download_in_appscope_enc.DataSQLHelper;
import com.music.nyasaa.front_end_layer.activities.download_in_appscope_enc.Download;
import com.music.nyasaa.front_end_layer.activities.download_in_appscope_enc.DownloadService;

import com.music.nyasaa.front_end_layer.activities.login.LoginActivity;
import com.music.nyasaa.manager.MediaController;
import com.music.nyasaa.manager.MusicPreferance;
import com.music.nyasaa.manager.NotificationManager;
import com.music.nyasaa.models.Community_DAO;
import com.music.nyasaa.models.DrawerItem;
import com.music.nyasaa.models.RequestModel.AddToCommunity.RequestShareCommunityPost;
import com.music.nyasaa.models.RequestModel.AddToCommunity.RequestToAcceptCommunity;
import com.music.nyasaa.models.RequestModel.Create_myplaylist.RequestCreatePlayList;

import com.music.nyasaa.models.RequestModel.GetMyPlayList.RequestGetMyPlayList;
import com.music.nyasaa.models.RequestModel.SaveToPlayList.RequestSaveToPlayList;

import com.music.nyasaa.models.ResponseModel.GetAllVideos.GetVideoResponseData;
import com.music.nyasaa.models.ResponseModel.GetBannerResponse;
import com.music.nyasaa.models.ResponseModel.GetBannerResponseData;
import com.music.nyasaa.models.ResponseModel.GetCategoryAll.GetResponseCategory;
import com.music.nyasaa.models.ResponseModel.GetCategoryAll.GetResponseCategoryData;
import com.music.nyasaa.models.ResponseModel.GetEvents.GetEventResponseData;
import com.music.nyasaa.models.ResponseModel.GetMyPlayList.GetMyPlayListResponse;
import com.music.nyasaa.models.ResponseModel.GetMyPlayList.GetMyPlayListResponseData;
import com.music.nyasaa.models.ResponseModel.getCommunity.GetCommunityResponse;
import com.music.nyasaa.models.ResponseModel.getCommunity.GetCommunityResponseData;
import com.music.nyasaa.models.ResponseModel.getCommunity.SoretedCommunityData;
import com.music.nyasaa.models.SongDetail;
import com.music.nyasaa.models.User_DAO;
import com.music.nyasaa.phonemidea.PlayerUtility;
import com.music.nyasaa.retrofit.Config;
import com.music.nyasaa.slidinguppanelhelper.SlidingUpPanelLayout;
import com.music.nyasaa.uicomponent.PlayPauseView;
import com.music.nyasaa.uicomponent.Slider;
import com.music.nyasaa.utility.LogWriter;
import com.music.nyasaa.utility.RecyclerItemClickListener;
import com.music.nyasaa.utility.SharedPreferenceManager;
import com.music.nyasaa.utility.SystemBarTintManager;
import com.music.nyasaa.utility.TbsindUtil;
import com.music.nyasaa.utility.Utility;

import com.mxn.soul.flowingdrawer_core.ElasticDrawer;
import com.mxn.soul.flowingdrawer_core.FlowingDrawer;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.music.nyasaa.fragments.FragmentDashBoard.fragmentDashBoards;
import static com.music.nyasaa.fragments.community.FragmentCommunityDetails.fragmentCommunityDetails;
import static com.music.nyasaa.fragments.discourses.FragmentVideoDiscoureses.fragmentVideoDiscoureses;
import static com.music.nyasaa.fragments.downloads.FragmentDownloads.fragmentDownloads;
import static com.music.nyasaa.fragments.interviews.FragmentInterview.fragmentInterview;
import static com.music.nyasaa.fragments.jokes.JokesFragment.jokesFragment;
import static com.music.nyasaa.fragments.meditation.FragmentMeditation.fragmentMeditation;
import static com.music.nyasaa.fragments.music.FragmentMusic.fragmentMusics;
import static com.music.nyasaa.fragments.mysongs.FragmentMySongs.fragmentMySongss;
import static com.music.nyasaa.fragments.playlistSongs.FragmentPlayListSongs.FragmentPlayListSongss;
import static com.music.nyasaa.front_end_layer.activities.download_in_appscope_enc.Table_Details.TABLE_NAME;
import static com.music.nyasaa.retrofit.Config.BASE_URL_FOR_PLAY;

import static com.music.nyasaa.retrofit.Config.BASE_VIDEO_URL;
import static com.music.nyasaa.retrofit.Config.X_API_KEY;
import static com.music.nyasaa.retrofit.Config.clientService;


//import static com.music.av.retrofit.Config.isdownloading;


@android.support.annotation.RequiresApi(api = Build.VERSION_CODES.O)
public class DashBoardActivity extends AppCompatActivity implements View.OnClickListener, Slider.OnValueChangedListener,
        NotificationManager.NotificationCenterDelegate, FragmentManager.OnBackStackChangedListener {
    private static final int PERMISSION_REQUEST_CODE = 1;
    public static int positionsofDashbord = 0;
    private PopupWindow popupWindow, popNotification;
    List<SoretedCommunityData> getCommunitySotredResponseData = new ArrayList<>();
    public static Boolean isdownloading = false;
    boolean first = false;
    private boolean magoo = false;


    private SongDetail mNewSOngsDetails;
    public static List<GetVideoResponseData> getEventResponseDataResponQue = new ArrayList<>();


    public static Boolean FromDownoadFile = false;
    private boolean mIsResumed = true;

    public static int PostionOfPlay = 0;
    private TextView cart_badge;
    public static Boolean FirstTime = false;

    private LinearLayout imgLayNotify;
    private String sUsername, sUserInfo;

    private String mode = "";

    private Button mSubscribeBut;

    public static final String MESSAGE_PROGRESS = "message_progress";
    //private static final int PERMISSION_REQUEST_CODE = 1;
    ProgressBar mProgressBar;
    TextView mProgressText;
    ImageView bottombar_play_new_1;
    ImageView bottombar_play_new;
    Button btn_download;
    private RelativeLayout homeFirstPlayer;

    public static SongDetail ForDOWNLOadSongDetail;
    public static Context appcontectext;

    private ImageView imageAnim, btn_dowloads, profileImage;
    private Dialog contactUsDialog, dialogPlayList;
    private EditText mEditDialogPlayListName;
    private Button mButDialogCreate;
    private Intent intent;

    private static boolean RUN_ONCE = false;
    private static final String TAG = "ActivityPlayerBase";
    public static DashBoardActivity context;
    private SharedPreferences sharedPreferences;
    private ActionBarDrawerToggle mDrawerToggle;
    private int theme;
    private SongDetail addSongs;
    private FrameLayout statusBar;
    private Toolbar mToolbar;

    private RecyclerView recyclerViewDrawer, mListCommunity;
    private TextView mTxtProfileName, mTxtProfileInfo, mTxtWarn;
    private RecyclerView.Adapter adapterDrawer;
    TextView btnDownload;
    private static SlidingUpPanelLayout mLayout;
    //    public static  RelativeLayout slidepanelchildtwo_topviewone;
    public RelativeLayout slidepanelchildtwo_topviewone;
    private RelativeLayout slidepanelchildtwo_topviewtwo;
    private boolean isExpand = false;
    public static ProgressBar progressBarUpper;
    public static ProgressBar progressBarMain;
    private DisplayImageOptions options;


    private ImageLoader imageLoader = ImageLoader.getInstance();
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    private static ImageView songAlbumbg;
    private static RelativeLayout imgMusicLayout;
    private static RelativeLayout relativeLayoutDragView;
    private ImageView img_bottom_slideone, ivLike;
    private ImageView imgCrossMark;
    private ImageView img_bottom_slidetwo;
    private TextView txt_playesongname;
    private TextView txt_songartistname;
    private TextView txt_time;
    private ImageView imgEdit;
    private TextView btnMore;
    private TextView bgTitle;
    private List<GetBannerResponseData> getBannerResponseData;

    // private TextView txt_songartistname_slidetoptwo;
    private TextView txt_timeprogress;
    private static TextView txt_timetotal;
    private ImageView imgbtn_backward;
    private ImageView imgbtn_forward;
    private ImageView imgbtn_toggle;
    private ImageView imgbtn_suffel;
    private ImageView img_Favorite;
    private ImageView bottombar_moreicon;

    private Dialog dialog;
    public static PlayPauseView btn_playpause;
    public static TextView tvName;
    public static PlayPauseView btn_playpausePanel;
    private Slider audio_progress;
    private boolean isDragingStart = false;
    private int TAG_Observer;
    private static final String BACK_STACK_ROOT_TAG = "root_fragment";
    FragmentManager fragmentManager;
    Calendar calendarDate;
    private String date;
    DatePickerDialog.OnDateSetListener txtDatePicker;

    // init by adapter and index
    // shuffle when current time == endtime i.e duration ; except 00
    // increase index play action like adapter still
    public static int AutoIndex = 0;
    public static ArrayList<SongDetail> Autoplaylist = new ArrayList<>();
    public static ArrayList<SongDetail> mSongss = new ArrayList<>();

    public static ArrayList<String> AutoplaylistVDOURL = new ArrayList<>();

    public static ArrayList<SongDetail> mSongssSeries = new ArrayList<>();
    private List<GetResponseCategoryData> getResponseCategoryData;

    private FlowingDrawer mDrawer;
    private NavigationView navigationView;
    //Download Method
    private MenuItem mSearchItem;
    public static RelativeLayout slidepanelchildtwo_topviewoneLatest;
    private Dialog confrimDeleteDialog, subscribe, myPlayListDialog, dialogResume;
    private RecyclerView mMyPlayList;
    private List<GetMyPlayListResponseData> getMyPlayListResponseData;
    private MyPlayListAdapters myPlayListAdapter;

    private RecyclerView mMenuListView, mMenuNotifcation;
    private ArrayList<String> menuArrayList;
    public static RotateAnimation rotate = new RotateAnimation(
            0, 360,
            Animation.RELATIVE_TO_SELF, 0.5f,
            Animation.RELATIVE_TO_SELF, 0.5f
    );
    private static DashBoardActivity sInstance;
    public static GetEventResponseData getEventResponseData;


    // for Nxt Song

    public ArrayList<SongDetail> mNxtSongDetailsArraylist;

    private int mNxtSgPOs = 0;
    public SongDetail mNxtSongDetails;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Set your theme first
        context = DashBoardActivity.this;
        FirstTime = false;
        isdownloading = false;


        File filei = new File(getApplicationContext().getFilesDir(), "tempfilenametoplay");
        filei.delete();
        theme();
        // temp download
        //DoonloadEXample();
        //Set your Layout view
        appcontectext = getApplicationContext();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        sUserInfo = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_ABOUT_ME, "");
        sUsername = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_NAME, "");
        mode = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_MUSIC_MODE, "none");
        getLoginData();

        blink();

        //System bar color set
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//            systembartiteniam();
        }

        toolbarStatusBar(" Nyasaa");

        initiSlidingUpPanel();
        //setFragment(0);
        getSupportFragmentManager().addOnBackStackChangedListener(DashBoardActivity.this);
        //Handle when activity is recreated like on orientation Change
        //shouldDisplayHomeUp();
        getIntentData();

        Badge();
        getLoginData();

        registerReceiver(broadcastReceivers, new IntentFilter("INTERNET_LOST"));

        slidepanelchildtwo_topviewoneLatest = (RelativeLayout) findViewById(R.id.slidepanelchildtwo_topviewone);
        SlidingUpPanelLayout mLayout = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);

        ivLike = (ImageView) findViewById(R.id.ivLike);
        slidepanelchildtwo_topviewone.setVisibility(View.GONE);

        registerReceiver();

        // make Visible

        mProgressBar.setVisibility(View.GONE);
        mProgressText.setVisibility(View.GONE);


        //initializeDraggablePanel();


        subscribe = new Dialog(DashBoardActivity.this);
        subscribe.requestWindowFeature(Window.FEATURE_NO_TITLE);
        subscribe.setContentView(R.layout.dialog_subscribe);
        subscribe.getWindow().getAttributes().width = ActionBar.LayoutParams.FILL_PARENT;
        subscribe.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        subscribe.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mSubscribeBut = (Button) subscribe.findViewById(R.id.butNo);

        bottombar_play_new.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (MediaController.getInstance().getPlayingSongDetail() != null)
                    PlayPauseEvent(view);
            }
        });

        threads();
    }


    private void startDownload() {

        // Add to Databse and put extra name of file
        // set JOB NO.
        //Add to Databse and put extra name of file ,file  URL, type of file, base url


        String file_Name = "DMND%20-%20Navv%20Inder(MyMp3Song).mp3";
        Intent intent = new Intent(this, DownloadService.class);
        String URL = "http://olavideos.s3.amazonaws.com/music/DMND%20-%20Navv%20Inder(MyMp3Song).mp3";

        intent.putExtra("File_Name", file_Name.replace(" ", "%20"));
        intent.putExtra("Type", "mp3"); // ADO Or VDO
        intent.putExtra("URL", URL);
        intent.putExtra("Image", "ImageURL");

        startService(intent);

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    int t = SharedPreferenceManager.getInstance().readInteger("dl_type", 1);
                    if(t == 1 || t == 3)
                        downloadfromOtherFragment();
                    else if(t == 2)
                        downloadfromOtherFragmentSerise(ForDOWNLOadSongDetail);
                    //startDownload();
                } else {

                    /// Snackbar.make(findViewById(R.id.coordinatorLayout), "Permission Denied, Please allow to proceed !", Snackbar.LENGTH_LONG).show();

                }
                break;
        }
    }

    private void DoonloadEXample() {

        if (checkPermission()) {
            startDownload();
        } else {
            requestPermission();
        }
    }

    private void requestPermission() {

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);

    }


    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {

            return true;

        } else {

            return false;
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        sUsername = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_NAME, "");
        mTxtProfileName.setText(sUsername);
        mIsResumed = true;
        addObserver();
        loadAlreadyPlayng();


//        toolbarStatusBar("Nyasaa");

//        loadFrag();
    }

    @Override
    protected void onPause() {
        mIsResumed = false;
        super.onPause();

        //commanon audio temp file while play is deleted.
        File filei = new File(getApplicationContext().getFilesDir(), "tempfilenametoplay");
        filei.delete();
        removeObserver();
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(broadcastReceivers);
        removeObserver();
        if (MediaController.getInstance().isAudioPaused()) {
            MediaController.getInstance().cleanupPlayer(context, true, true);
        }
        super.onDestroy();
    }

    public void addFragmentOnTop(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment, fragment)
                .addToBackStack(null)
                .commit();
        //navigationDrawer();


    }

    public void setDownloadForImage(File File) {
        // img_bottom_slideone

        Picasso.with(DashBoardActivity.this).load(File).error(R.drawable.default_music).into(img_bottom_slideone);
        //  Picasso.with(DashBoardActivity.this).load(File).error(R.drawable.default_music).into(img_bottom_slidetwo);

        Picasso.with(DashBoardActivity.this).load(File).error(R.drawable.default_music).into(ivLike);


    }

    public void downloadfromOtherFragment() {
        final String URL;
        if (isdownloading == false) {
            if (checkPermission()) {

                if (ForDOWNLOadSongDetail != null) {

                    if (checkAvaiblity(ForDOWNLOadSongDetail)) {
                        Toast.makeText(DashBoardActivity.this, "Saving Offline...", Toast.LENGTH_SHORT).show();
                        String file_Name = Uri.encode(ForDOWNLOadSongDetail.getFile());
                        Intent intent = new Intent(this, DownloadService.class);

                        try
                        {

                            String url=ForDOWNLOadSongDetail.getTrack_url()+"/";

                            BASE_URL_FOR_PLAY=url;
                        }
                        catch (Exception e)
                        {

                        }

                        URL = BASE_URL_FOR_PLAY + Uri.encode(ForDOWNLOadSongDetail.getFile());


                        String ids = " ";
                        if (ForDOWNLOadSongDetail.getMedia_type_id() != null) {
                            ids = ForDOWNLOadSongDetail.getId() + ForDOWNLOadSongDetail.getMedia_type_id();
                        } else {
                            ids = String.valueOf(ForDOWNLOadSongDetail.getId());

                        }
                        intent.putExtra("Image_URL", (Config.PIC_URL + Uri.encode(ForDOWNLOadSongDetail.getCover())));

                        String name =""+ ForDOWNLOadSongDetail.getName().replace(" ", "%20");


                        intent.putExtra("name", name);
                        intent.putExtra("Image", ForDOWNLOadSongDetail.getCover().replace(" ", "%20"));
                        //  intent.putExtra("Image_URL", Config.PIC_URL + """  + ForDOWNLOadSongDetail.getCover());
                        intent.putExtra("id", ids);
                        intent.putExtra("File_Name", file_Name);
                        intent.putExtra("Type", "mp3"); // ADO Or VDO
                        intent.putExtra("URL", URL);
                        intent.putExtra("album_name", ForDOWNLOadSongDetail.getAlbum_name());
                        intent.putExtra("cover_image", ForDOWNLOadSongDetail.getCover_image());

                        startService(intent);
                    } else {
                        Toast.makeText(DashBoardActivity.this, "Already downloaded..", Toast.LENGTH_SHORT).show();

                        mProgressBar.setVisibility(View.GONE);
                        mProgressText.setVisibility(View.GONE);

                    }
                }

            } else {
                SharedPreferenceManager.getInstance().writeInteger("dl_type", 1);
                requestPermission();
            }
        } else {
            Toast.makeText(DashBoardActivity.this, "Previous file downloading please wait...", Toast.LENGTH_SHORT).show();

        }

    }

    public void downloadfromOtherFragmentSerise(SongDetail ForDOWNLOadSongDetail) {
        this.ForDOWNLOadSongDetail = ForDOWNLOadSongDetail;
        final String URL;
        if (isdownloading == false) {
            if (checkPermission()) {

                if (this.ForDOWNLOadSongDetail != null) {

                    if (checkAvaiblity(this.ForDOWNLOadSongDetail)) {
                        Toast.makeText(DashBoardActivity.this, "Saving all songs in album...", Toast.LENGTH_SHORT).show();
                        String file_Name = Uri.encode(this.ForDOWNLOadSongDetail.getFile());
                        Intent intent = new Intent(this, DownloadService.class);

                        try
                        {

                            String url=this.ForDOWNLOadSongDetail.getTrack_url()+"/";

                            BASE_URL_FOR_PLAY=url;
                        }
                        catch (Exception e)
                        {

                        }
                        URL = BASE_URL_FOR_PLAY + Uri.encode(this.ForDOWNLOadSongDetail.getFile());


                        String ids = " ";
                        if (this.ForDOWNLOadSongDetail.getMedia_type_id() != null) {
                            ids = this.ForDOWNLOadSongDetail.getId() + this.ForDOWNLOadSongDetail.getMedia_type_id();
                        } else {
                            ids = String.valueOf(ForDOWNLOadSongDetail.getId());

                        }
                        intent.putExtra("Image_URL", (Config.PIC_URL + Uri.encode(this.ForDOWNLOadSongDetail.getCover())));


                        intent.putExtra("name", this.ForDOWNLOadSongDetail.getName().replace(" ", "%20"));
                        intent.putExtra("Image", this.ForDOWNLOadSongDetail.getCover().replace(" ", "%20"));
                        //  intent.putExtra("Image_URL", Config.PIC_URL + """  + ForDOWNLOadSongDetail.getCover());
                        intent.putExtra("id", ids);
                        intent.putExtra("File_Name", file_Name);
                        intent.putExtra("Type", "mp3"); // ADO Or VDO
                        intent.putExtra("URL", URL);
                        intent.putExtra("album_name", this.ForDOWNLOadSongDetail.getAlbum_name());
                        intent.putExtra("cover_image", this.ForDOWNLOadSongDetail.getCover_image());

                        startService(intent);
                    } else {
                        if (mSongssSeries != null) {
                            if (mSongssSeries.size() > 0) {
                                mSongssSeries.remove(0);

                            }
                        }

                        if (mSongssSeries != null) {
                            if (mSongssSeries.size() > 0) {
                                downloadfromOtherFragmentSerise(mSongssSeries.get(0));
                            }
                        }
                        mProgressBar.setVisibility(View.GONE);
                        mProgressText.setVisibility(View.GONE);

                    }
                }

            } else {
                SharedPreferenceManager.getInstance().writeInteger("dl_type", 2);
                requestPermission();
            }
        } else {
            Toast.makeText(DashBoardActivity.this, "Previous file downloading please wait...", Toast.LENGTH_SHORT).show();

        }

    }


    @Override
    public void onBackPressed() {




        /*FragmentManager fragments = getSupportFragmentManager();
        Fragment homeFrag = fragments.findFragmentByTag("0");
        if (fragments.getBackStackEntryCount() > 1) {

            fragments.popBackStack();
            Log.d("TAG", "visiblae " + homeFrag);

            *//*String mainTance = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_FRAG_MAINTAIN, "0");


            if (mainTance.equals("1")) {
                toolbarStatusBar(" Community");
            } else {
                toolbarStatusBar(" Playlist");
            }*//*

        } else if (homeFrag == null || !homeFrag.isVisible()) {
            Log.d("TAG", "this ");
            // We aren't showing the home screen, so that is the next stop on the back journey
        }*/


//        if (isExpand) {
//            isExpand=false;
//            mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
//        }


        if (mDrawer.isMenuVisible()) {
            mDrawer.closeMenu();
        } else if (mLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED && isExpand) {
            mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            isExpand = false;
        } else if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            getSupportFragmentManager().popBackStack();
            Log.i(TAG, "onBackPressed: backstack >1");
            FragmentManager fragmentManager = getSupportFragmentManager();
            String fragmentTag = fragmentManager.getBackStackEntryAt(fragmentManager.getBackStackEntryCount() - 1).getName();
            //Fragment currentFragment = fragmentManager.findFragmentByTag(fragmentTag);
            if (fragmentTag.equalsIgnoreCase("PlayList")) {
                toolbarStatusBar(" Playlist");
            } else if (fragmentTag.equalsIgnoreCase("AudioNew")) {
                toolbarStatusBar(" Return policy");
            } else if (fragmentTag.equalsIgnoreCase("Details")) {
                toolbarStatusBar(" Community");
            } else if (fragmentTag.equalsIgnoreCase("DetailsMem")) {
                //toolbarStatusBar(" Community");
            } else if (fragmentTag.equalsIgnoreCase("Song")) {
                toolbarStatusBar(" Audio");
                mToolbar.setVisibility(View.VISIBLE);
            } else if (fragmentTag.equalsIgnoreCase("Video")) {
                toolbarStatusBar(" Video");
                mToolbar.setVisibility(View.VISIBLE);
            } else if (fragmentTag.equalsIgnoreCase("Meditation")) {
                toolbarStatusBar(" Meditation");
            } else {
                //getSupportFragmentManager().popBackStack();
                toolbarStatusBar(" Nyasaa");
            }


        }

//

        else {

            // We are already showing the home screen, so the next stop is out of the app.

//


            if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
//                Utility.getSharedInstance().showProgressDialog(this);
                getSupportFragmentManager().popBackStack();
                toolbarStatusBar(" Nyasaa");
                Badge();
                loadFrag();
                getLoginData();

            } else if (getSupportFragmentManager().getBackStackEntryCount() == 0) {


                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                alertDialogBuilder.setTitle("Nyasaa  exit");
                alertDialogBuilder.setMessage("Are you sure, You want to exit?");

                alertDialogBuilder.setPositiveButton(Html.fromHtml("<font color='#de1f1f'>Yes</font>"), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        //commanon audio temp file while play is deleted.

                        MediaController.getInstance().stopAudio();
//                        File filei = new File(getApplicationContext().getFilesDir(), "tempfilenametoplay");
//                        filei.delete();
//
//                        DashBoardActivity.super.onBackPressed();
//                        overridePendingTransition(0, 0);

                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//***Change Here***
                        startActivity(intent);
                        finish();
                        System.exit(0);

//                        finish();
                    }
                });

                alertDialogBuilder.setNegativeButton(Html.fromHtml("<font color='#de1f1f'>No</font>"), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                AlertDialog alert = alertDialogBuilder.create();
                alert.show();
                Button buttonPositive = alert.getButton(DialogInterface.BUTTON_POSITIVE);
                buttonPositive.setTextColor(ContextCompat.getColor(this, R.color.red_color_primary));
                Button buttonNegative = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
                buttonNegative.setTextColor(ContextCompat.getColor(this, R.color.red_color_primary));

            }
            Log.d("TAG", "OnBackPress");


        }
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.bottombar_play:
                if (MediaController.getInstance().getPlayingSongDetail() != null)
                    PlayPauseEvent(v);
                if (fragmentDownloads != null) {
                    fragmentDownloads.playPoase(PostionOfPlay);
                }
                break;

            case R.id.btn_play:
                if (MediaController.getInstance().getPlayingSongDetail() != null)
                    PlayPauseEvent(v);
                if (fragmentDownloads != null) {
                    fragmentDownloads.playPoase(PostionOfPlay);
                }
                break;


            case R.id.bottombar_play_new:
                if (MediaController.getInstance().getPlayingSongDetail() != null)
                    PlayPauseEvent(v);
                if (fragmentDownloads != null) {
                    fragmentDownloads.playPoase(PostionOfPlay);
                }
                break;

            case R.id.bottombar_play_new_1:
                if (MediaController.getInstance().getPlayingSongDetail() != null)
                    PlayPauseEvent(v);

                if (fragmentDownloads != null) {
                    fragmentDownloads.playPoase(PostionOfPlay);
                }
                break;
            case R.id.btn_dowloads:
                mProgressBar.setProgress(0);
                mProgressBar.setVisibility(View.VISIBLE);
                mProgressText.setVisibility(View.VISIBLE);
                mProgressText.setText("Downloading...");

                if (isdownloading == false) {
                    if (checkPermission()) {

                        String URL;

                        if (ForDOWNLOadSongDetail != null) {

                            if (checkAvaiblity(ForDOWNLOadSongDetail)) {
                                Toast.makeText(DashBoardActivity.this, "Saving Offline...", Toast.LENGTH_SHORT).show();
                                String file_Name = ForDOWNLOadSongDetail.getFile();
                                Intent intent = new Intent(this, DownloadService.class);

                                URL = BASE_URL_FOR_PLAY + ForDOWNLOadSongDetail.getFile();


                                String ids = " ";
                                if (ForDOWNLOadSongDetail.getMedia_type_id() != null) {
                                    ids = ForDOWNLOadSongDetail.getId() + ForDOWNLOadSongDetail.getMedia_type_id();
                                } else {
                                    ids = String.valueOf(ForDOWNLOadSongDetail.getId());

                                }
                                intent.putExtra("name", ForDOWNLOadSongDetail.getName().replace(" ", "%20"));
                                intent.putExtra("Image", ForDOWNLOadSongDetail.getCover().replace(" ", "%20"));
                                intent.putExtra("Image_URL", (Config.PIC_URL + ForDOWNLOadSongDetail.getCover()).replace(" ", "%20"));

                                intent.putExtra("id", ids);
                                intent.putExtra("File_Name", file_Name.replace(" ", "%20"));
                                intent.putExtra("Type", "mp3"); // ADO Or VDO
                                intent.putExtra("URL", URL.replace(" ", "%20"));
                                intent.putExtra("album_name", ForDOWNLOadSongDetail.getAlbum_name());
                                intent.putExtra("cover_image", ForDOWNLOadSongDetail.getCover_image());
                                // intent.putExtra("Image", "http://olavideos.s3.amazonaws.com/" + ForDOWNLOadSongDetail.getUrl());

                                startService(intent);
                            } else {
                                Toast.makeText(DashBoardActivity.this, "Already downloaded..", Toast.LENGTH_SHORT).show();

                                mProgressBar.setVisibility(View.GONE);
                                mProgressText.setVisibility(View.GONE);

                            }
                        }


                    } else {
                        SharedPreferenceManager.getInstance().writeInteger("dl_type", 3);
                        requestPermission();
                    }
                } else {
                    Toast.makeText(DashBoardActivity.this, "Previous file downloading please wait...", Toast.LENGTH_SHORT).show();

                }
                break;


            case R.id.btn_forward:
                /*if (MediaController.getInstance().getPlayingSongDetail() != null)
                    MediaController.getInstance().playNextSong();*/
                //TODO: Add to playlist

                SongDetail audioInfo = MediaController.getInstance().getPlayingSongDetail();

                if (audioInfo.audioProgressSec != 0) {


                    String trackID = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_TRACK_ID, "none");
                    if (!trackID.equalsIgnoreCase("none"))
                        //showDialog(trackID);
                        mNxtSgPOs++;
                    SongDetail nxtSongD = new SongDetail();


                    try {


                        nxtSongD = mNxtSongDetailsArraylist.get(mNxtSgPOs);
                        DashBoardActivity.context.setStatus(true);
//                        DashBoardActivity.context.setPlayer(true);
                        DashBoardActivity.context.onResume();
                        loadSongsDetails(nxtSongD);

                        if (nxtSongD.getFile().contains(".mp3")) {
                            if (!MediaController.getInstance().isPlayingAudio(nxtSongD) || MediaController.getInstance().isAudioPaused()) {
                                MediaController.getInstance().stopAudio();

                                if(nxtSongD.getTrack_url() == null)
                                    MediaController.getInstance().playMUSICFORDOWNload(nxtSongD);
                                else
                                    MediaController.getInstance().playMUSIC(nxtSongD);

//                                MediaController.getInstance().playMUSIC(nxtSongD);


                                ForDOWNLOadSongDetail = nxtSongD;


                            }
                        }


                    } catch (Exception e) {

                        Toast.makeText(DashBoardActivity.this, "No More Songs In Queue", Toast.LENGTH_SHORT).show();


                    }
                }

                break;

            case R.id.btn_backward:
                audioInfo = MediaController.getInstance().getPlayingSongDetail();

                if (audioInfo.audioProgressSec != 0) {

                    if (MediaController.getInstance().getPlayingSongDetail() != null)
                        MediaController.getInstance().playPreviousSong();
                }
                break;

            case R.id.btn_suffel:

                break;

            case R.id.btn_toggle:

                break;
            case R.id.btnMore:
                try {
                    PopupMenu popup = new PopupMenu(context, v);
                    popup.getMenuInflater().inflate(R.menu.list_item_option, popup.getMenu());
                    popup.show();
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {

                            switch (item.getItemId()) {
                                case R.id.addToPlay:
                                    break;

                                case R.id.like:
                                    break;

                                case R.id.share:

                                    break;

                                default:
                                    break;
                            }

                            return true;
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case R.id.img_bottom_slidetwo://back
                mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                break;
            case R.id.bottombar_moreicon://queue

            case R.id.bottombar_img_Favorite:
                mNxtSgPOs--;
                SongDetail nxtSongDs = new SongDetail();

                try {


                    nxtSongDs = mNxtSongDetailsArraylist.get(mNxtSgPOs);
                    DashBoardActivity.context.setStatus(true);
//                    DashBoardActivity.context.setPlayer(false);
                    DashBoardActivity.context.onResume();
                    loadSongsDetails(nxtSongDs);

                    if (nxtSongDs.getFile().contains(".mp3")) {
                        if (!MediaController.getInstance().isPlayingAudio(nxtSongDs) || MediaController.getInstance().isAudioPaused()) {
                            MediaController.getInstance().stopAudio();

                            if(nxtSongDs.getTrack_url() == null)
                                MediaController.getInstance().playMUSICFORDOWNload(nxtSongDs);
                            else
                                MediaController.getInstance().playMUSIC(nxtSongDs);

//                            MediaController.getInstance().playMUSIC(nxtSongDs);


                            ForDOWNLOadSongDetail = nxtSongDs;


                        }
                    }


                } catch (Exception e) {

                    Toast.makeText(DashBoardActivity.this, "No More Songs In Queue", Toast.LENGTH_SHORT).show();


                }


//                if (MediaController.getInstance().getPlayingSongDetail() != null) {
//                    MediaController.getInstance().storeFavoritePlay(context, MediaController.getInstance().getPlayingSongDetail(), v.isSelected() ? 0 : 1);
//                    v.setSelected(!v.isSelected());
//                    PlayerUtility.animateHeartButton(v);
//                    findViewById(R.id.ivLike).setSelected(v.isSelected());
//                    PlayerUtility.animatePhotoLike(findViewById(R.id.vBgLike), findViewById(R.id.ivLike));
//                    showCommDialog(DashBoardActivity.this);
//                    // Toast.makeText(context, "Song add into favorite list ", Toast.LENGTH_SHORT).show();
//
//                }
                break;

            default:
                break;
        }
    }

    private boolean checkAvaiblity(GetVideoResponseData forDOWNLOadSongDetail) {
        // boolean notAvaible = true;

        boolean notAvaible = true;
        try {


            DataSQLHelper dataSQLHelper = new DataSQLHelper(context.getApplicationContext());
            SQLiteDatabase sqLiteDatabase;
            sqLiteDatabase = dataSQLHelper.getWritableDatabase();
            String getJson = "";

            String query = "SELECT * FROM " + TABLE_NAME + " WHERE id='" + forDOWNLOadSongDetail.getId() + "VDO" + "'";
            Cursor cursor = sqLiteDatabase.rawQuery(query, null);

            if (cursor.getCount() > 0) {
                notAvaible = false;

            } else {
                notAvaible = true;
            }
        } catch (Exception e) {

        }

        return notAvaible;
    }


    private boolean checkAvaiblity(SongDetail forDOWNLOadSongDetail) {
        boolean notAvaible = true;

        String ids = " ";
        if (forDOWNLOadSongDetail.getMedia_type_id() != null) {
            ids = forDOWNLOadSongDetail.getId() + forDOWNLOadSongDetail.getMedia_type_id();
        } else {
            ids = String.valueOf(forDOWNLOadSongDetail.getId());

        }
        try {


            DataSQLHelper dataSQLHelper = new DataSQLHelper(getApplicationContext());
            SQLiteDatabase sqLiteDatabase;
            sqLiteDatabase = dataSQLHelper.getWritableDatabase();
            String getJson = "";

            String query = "SELECT * FROM " + TABLE_NAME + " WHERE id='" + ids + "'";
            Cursor cursor = sqLiteDatabase.rawQuery(query, null);

            if (cursor.getCount() > 0) {
                notAvaible = false;

            } else {
                notAvaible = true;
            }
        } catch (Exception e) {

        }

        return notAvaible;
    }

    /**
     * Get intent data from music choose option
     */
    private void getIntentData() {
        try {
            Uri data = getIntent().getData();
            if (data != null) {
                if (data.getScheme().equalsIgnoreCase("file")) {
                    String path = data.getPath();
                    if (!TextUtils.isEmpty(path)) {
                        MediaController.getInstance().cleanupPlayer(context, true, true);
                        MusicPreferance.getPlaylist(context, path);
                        updateTitle(false);
                        MediaController.getInstance().playAudio(MusicPreferance.playingSongDetail);
                        mLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                    }
                }
                if (data.getScheme().equalsIgnoreCase("http"))
                    LogWriter.info(TAG, data.getPath());
                if (data.getScheme().equalsIgnoreCase("content"))
                    LogWriter.info(TAG, data.getPath());

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void toolbarStatusBar(final String toolbarName) {
        profileImage = (ImageView) findViewById(R.id.profileImage);
        String imageUrl = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_PROFILE_PHOTO, "");
        Picasso.with(DashBoardActivity.this).load(Config.BASE_PROFILE_PIC + imageUrl).error(R.drawable.default_music).into(profileImage);
        statusBar = (FrameLayout) findViewById(R.id.statusBar);
        //statusBar.getForeground().setAlpha(0);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);


        TextView mTxtToolbaTxt = (TextView) findViewById(R.id.txtTitleToolbar);
        LinearLayout linearLayouToggle = (LinearLayout) findViewById(R.id.toogleMenu);

        imgEdit = (ImageView) findViewById(R.id.img_notification);
        cart_badge = (TextView) findViewById(R.id.cart_badge);
        ImageView imgSerach = (ImageView) findViewById(R.id.imgSearch);
        imgLayNotify = (LinearLayout) findViewById(R.id.LayImagNotifi);
        LinearLayout imgSerachLayout = (LinearLayout) findViewById(R.id.laySearch);
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.toolLayout);

        linearLayout.setBackgroundColor(Color.parseColor("#050505"));
        mTxtToolbaTxt.setText(toolbarName);
        linearLayouToggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawer.toggleMenu();
            }
        });
        // getBanners();
//        navigationDrawer();


        if (toolbarName.equalsIgnoreCase(" Nyasaa")) {

            mToolbar.setVisibility(View.VISIBLE);
            mTxtToolbaTxt.setGravity(Gravity.RIGHT);
//            p.setMargins(l, t, r, b);
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mTxtToolbaTxt.getLayoutParams();
            params.setMargins(0, 10, 60, 0); //substitute parameters for left, top, right, bottom
            mTxtToolbaTxt.setLayoutParams(params);

//            Animation anim = AnimationUtils.loadAnimation(this, R.anim.slide_text);
//            mTxtToolbaTxt.startAnimation(anim);
//            mTxtToolbaTxt.setTextSize(15);

            imgSerach.setVisibility(View.GONE);
            imgSerachLayout.setClickable(false);
            imgEdit.setVisibility(View.GONE);
            imgEdit.setImageResource(0);
            imgEdit.setImageResource(R.drawable.ic_notifications_new);


            imgLayNotify.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    getAllCoummity(view);

                }
            });
        } else if (toolbarName.contains(" Work")) {
            mTxtToolbaTxt.setGravity(Gravity.LEFT);

            imgEdit.setVisibility(View.VISIBLE);
            imgEdit.setImageResource(0);
            imgEdit.setImageResource(R.drawable.ic_filter_list_black_24dp);

            imgLayNotify.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

//                    Toast.makeText(DashBoardActivity.this,"Hi",Toast.LENGTH_SHORT).show();


                    getAllFilterCategory(view);

                }
            });


        } else if (toolbarName.equalsIgnoreCase(" Details")) {
            mToolbar.setVisibility(View.VISIBLE);
            mTxtToolbaTxt.setGravity(Gravity.LEFT);

            String getCommName = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_COMMUNITY_NAME, " Details");
            mTxtToolbaTxt.setText(getCommName);


//            String communityName=SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_COMMUNITY_NAME,"Details");
//
//            mTxtToolbaTxt.setText(communityName);


            final String userID = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");
            final String CommParentID = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_IS_OWN_COMMUNITY, "none");

            //        ImageView imgEdit = (ImageView) myView.(R.id.img_notification);
//
            imgEdit.setImageResource(0);
            imgEdit.setImageResource(R.drawable.ic_action_add_community);
            cart_badge.setVisibility(View.GONE);

            if (userID.equalsIgnoreCase(CommParentID)) {
                imgEdit.setVisibility(View.VISIBLE);


                imgSerach.setImageResource(0);
                imgSerach.setImageResource(R.drawable.ic_action_friends);
                imgSerach.setVisibility(View.VISIBLE);
                imgSerachLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showCommunityDetails();
                    }
                });

            } else {
                imgEdit.setVisibility(View.VISIBLE);
                imgSerach.setVisibility(View.GONE);
                imgSerachLayout.setClickable(false);


//                imgSerach.setImageResource(0);
//                imgSerach.setImageResource(R.drawable.ic_person_outline_white_24dp);
//                imgSerach.setVisibility(View.VISIBLE);
//                imgSerachLayout.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        showCommunityDetails();
//                    }
//                });

            }

            imgLayNotify.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if (userID.equalsIgnoreCase(CommParentID)) {
                        String owner = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_IS_OWN_COMMUNITY, "none");
                        if (owner.equalsIgnoreCase("0")) {

                            Toast.makeText(DashBoardActivity.this, "Your not owner for this community", Toast.LENGTH_SHORT).show();
                        } else {
                            Intent intent = new Intent(DashBoardActivity.this, AddMemberActivity.class);
                            startActivity(intent);
                            overridePendingTransition(R.anim.left, R.anim.right);
                        }

                    } else {


                        showCommunityDetails();


                    }


                }
            });


        } else if (toolbarName.equalsIgnoreCase(" community")) {
            mToolbar.setVisibility(View.VISIBLE);
            mTxtToolbaTxt.setGravity(Gravity.LEFT);
            imgSerach.setImageResource(0);
            imgSerach.setImageResource(R.drawable.ic_action_search);

            imgSerach.setVisibility(View.VISIBLE);
            imgSerachLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(DashBoardActivity.this, SearchCommunityActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.left, R.anim.right);

                }
            });
            imgEdit.setVisibility(View.VISIBLE);
            imgEdit.setImageResource(0);
            imgEdit.setImageResource(R.drawable.ic_action_add_community);


            imgLayNotify.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    intent = new Intent(DashBoardActivity.this, AddCommunityActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.left, R.anim.right);
                }
            });


        } else if (toolbarName.equalsIgnoreCase(" playlist")) {
            mToolbar.setVisibility(View.VISIBLE);
            mTxtToolbaTxt.setGravity(Gravity.LEFT);
            imgSerachLayout.setClickable(false);
            imgSerach.setVisibility(View.GONE);
            imgEdit.setVisibility(View.VISIBLE);
            imgEdit.setImageResource(0);
            imgEdit.setImageResource(R.drawable.ic_action_add_community);


            imgLayNotify.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (toolbarName.equalsIgnoreCase(" playlist")) {
                        dialogPlayList = new Dialog(DashBoardActivity.this);
                        dialogPlayList.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialogPlayList.setContentView(R.layout.activity_create_myplaylist);
                        dialogPlayList.getWindow().getAttributes().width = ActionBar.LayoutParams.FILL_PARENT;
                        dialogPlayList.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
                        dialogPlayList.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        dialogPlayList.show();


                        mButDialogCreate = (Button) dialogPlayList.findViewById(R.id.butCreate);
                        mEditDialogPlayListName = (EditText) dialogPlayList.findViewById(R.id.editPlayListName);


                        mButDialogCreate.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (mEditDialogPlayListName.getText().toString().trim().equalsIgnoreCase("")) {

                                    Toast.makeText(DashBoardActivity.this, "Enter playlist name", Toast.LENGTH_SHORT).show();
                                } else {

                                    createMyPlayList(mEditDialogPlayListName.getText().toString().trim());
                                }
                            }
                        });
                    } else {


                        intent = new Intent(DashBoardActivity.this, AddCommunityActivity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.left, R.anim.right);


                    }


                }
            });


        } else if (toolbarName.equalsIgnoreCase(" Jokes")) {
            mTxtToolbaTxt.setText("Jokes");
            mTxtToolbaTxt.setGravity(Gravity.LEFT);
            mToolbar.setVisibility(View.VISIBLE);
            imgEdit.setVisibility(View.VISIBLE);
            imgEdit.setImageResource(0);
            imgEdit.setImageResource(R.drawable.ic_filter_list_black_24dp);
            imgSerach.setImageResource(0);
            imgSerach.setVisibility(View.INVISIBLE);
            imgLayNotify.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

//                    Toast.makeText(DashBoardActivity.this,"Hi",Toast.LENGTH_SHORT).show();


                    getAllFilterCategory(view);

                }
            });
        } else if (toolbarName.equalsIgnoreCase(" Audio") || toolbarName.equalsIgnoreCase(" videoCat")) {

            String toolname = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT_NAME, "Audio");
            mTxtToolbaTxt.setText(" " + toolname);

            imgEdit.setVisibility(View.VISIBLE);
            imgEdit.setImageResource(0);
            imgEdit.setImageResource(R.drawable.ic_filter_list_black_24dp);

            imgLayNotify.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getAllFilterCategory(view);
                }
            });

        } else if (toolbarName.equalsIgnoreCase(" Interviews")) {
            mTxtToolbaTxt.setGravity(Gravity.LEFT);
            mTxtToolbaTxt.setText(" Interviews");

            mTxtToolbaTxt.setGravity(Gravity.LEFT);
            mToolbar.setVisibility(View.VISIBLE);
            imgEdit.setVisibility(View.VISIBLE);
            imgEdit.setImageResource(0);
            imgEdit.setImageResource(R.drawable.ic_filter_list_black_24dp);
            imgSerach.setImageResource(0);
            imgSerach.setVisibility(View.INVISIBLE);

            imgLayNotify.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getAllFilterCategory(view);
                }
            });


        } else if (toolbarName.equalsIgnoreCase(" Audio")) {
            mTxtToolbaTxt.setGravity(Gravity.LEFT);

            String draweType = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_DRAWER_TYPE, "");
            if (draweType.equalsIgnoreCase("3")) {
                imgEdit.setVisibility(View.VISIBLE);
                imgEdit.setImageResource(0);
                imgEdit.setImageResource(R.drawable.ic_filter_list_black_24dp);

                imgLayNotify.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        getAllFilterCategory(view);
                    }
                });
            } else {
                imgEdit.setVisibility(View.GONE);
                imgSerach.setVisibility(View.GONE);
                imgSerachLayout.setClickable(false);
                imgLayNotify.setClickable(false);
            }

        } else if (toolbarName.equalsIgnoreCase(" Video")) {
            mTxtToolbaTxt.setGravity(Gravity.LEFT);

            String draweType = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_DRAWER_TYPE, "");
            if (draweType.equalsIgnoreCase("4")) {
                imgEdit.setVisibility(View.VISIBLE);
                imgEdit.setImageResource(0);
                imgEdit.setImageResource(R.drawable.ic_filter_list_black_24dp);

                imgLayNotify.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        getAllFilterCategory(view);
                    }
                });
            } else {
                imgLayNotify.setClickable(false);
                imgEdit.setVisibility(View.GONE);
                imgSerach.setVisibility(View.GONE);
                imgSerachLayout.setClickable(false);
            }

        } else {

            mTxtToolbaTxt.setGravity(Gravity.LEFT);

            imgEdit.setVisibility(View.GONE);
            imgSerach.setVisibility(View.GONE);
            imgLayNotify.setClickable(false);
            imgSerachLayout.setClickable(false);
        }

//        if (toolbarName.equalsIgnoreCase(" community")) {
//
//            imgSerach.setVisibility(View.VISIBLE);
//            imgSerachLayout.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    Intent intent = new Intent(DashBoardActivity.this, SearchCommunityActivity.class);
//                    startActivity(intent);
//                    overridePendingTransition(R.anim.left, R.anim.right);
//
//                }
//            });
//
//        } else {
//
////            imgEdit.setVisibility(View.GONE);
////
////            imgSerach.setVisibility(View.GONE);
//
//        }
    }


    public PopupWindow popUpNotiifcation() {
        popNotification = new PopupWindow(this);

        // inflate your layout or dynamically add view
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.dialog_notification, null);

        Button but = (Button) view.findViewById(R.id.butYes);

        popNotification.setFocusable(true);
        popNotification.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        popNotification.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popNotification.setBackgroundDrawable(new

                ColorDrawable(Color.TRANSPARENT));
        popNotification.setContentView(view);

        mMenuNotifcation = (RecyclerView) view.findViewById(R.id.menuList);
        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mMenuNotifcation.setLayoutManager(linearLayoutManager);
        mMenuNotifcation.setItemAnimator(new

                DefaultItemAnimator());

        return popNotification;

    }


    public PopupWindow popupDisplay() {


        popupWindow = new PopupWindow(this);

        // inflate your layout or dynamically add view
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.dialog_menu, null);

        Button but = (Button) view.findViewById(R.id.butYes);

        popupWindow.setFocusable(true);
        popupWindow.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        popupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        popupWindow.setContentView(view);

        mMenuListView = (RecyclerView) view.findViewById(R.id.menuList);
        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mMenuListView.setLayoutManager(linearLayoutManager);
        mMenuListView.setItemAnimator(new DefaultItemAnimator());


//

        return popupWindow;
    }

    public void navigationDrawer() {
        // Cast drawer
        mDrawer = (FlowingDrawer) findViewById(R.id.drawerlayout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);

//        navigationView.setItemIconTintList(null);
        mDrawer.setTouchMode(ElasticDrawer.TOUCH_MODE_BEZEL);


        // Setup RecyclerView inside drawer
        final TypedValue typedValue = new TypedValue();
        getTheme().resolveAttribute(R.attr.colorPrimary, typedValue, true);
        final int color = typedValue.data;

        recyclerViewDrawer = (RecyclerView) findViewById(R.id.recyclerViewDrawer);

        mTxtProfileInfo = (TextView) findViewById(R.id.txtProfileInfor);
        mTxtProfileName = (TextView) findViewById(R.id.txtProfileName);


        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                fragmentManager = getSupportFragmentManager();
                fragmentManager.popBackStack(BACK_STACK_ROOT_TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                // Add the new tab fragment
                fragmentManager.beginTransaction()
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .replace(R.id.fragment, FragmentProfile.newInstance(0), String.valueOf(0))
                        .addToBackStack(BACK_STACK_ROOT_TAG)
                        // .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .commit();
                mDrawer.closeMenu();
                toolbarStatusBar(" Profile");
                cart_badge.setVisibility(View.GONE);

            }
        });


        mTxtProfileInfo.setText("support@ask-osho.com");
        mTxtProfileInfo.setVisibility(View.INVISIBLE);
        recyclerViewDrawer.setHasFixedSize(true);
        recyclerViewDrawer.setLayoutManager(new LinearLayoutManager(DashBoardActivity.this));

        ArrayList<DrawerItem> drawerItems = new ArrayList<>();
        final String[] drawerTitles = getResources().getStringArray(R.array.drawer);
        final TypedArray drawerIcons = getResources().obtainTypedArray(R.array.drawerIcons);
        for (int i = 0; i < drawerTitles.length; i++) {
            drawerItems.add(new DrawerItem(drawerTitles[i], drawerIcons.getDrawable(i)));
        }
        drawerIcons.recycle();
//        adapterDrawer = new DrawerAdapter(drawerItems);
//        recyclerViewDrawer.setAdapter(adapterDrawer);

//        recyclerViewDrawer.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//            @Override
//            public void onGlobalLayout() {
//
//                for (int i = 0; i < drawerTitles.length; i++) {
        //  if (i == sharedPreferences.getInt("FRAGMENT", 0)) {
//                        ImageView imageViewDrawerIcon = (ImageView) recyclerViewDrawer.getChildAt(i).findViewById(R.id.imageViewDrawerIcon);
//                        TextView textViewDrawerTitle = (TextView) recyclerViewDrawer.getChildAt(i).findViewById(R.id.textViewDrawerItemTitle);
//                        imageViewDrawerIcon.setColorFilter(color);
//                        if (Build.VERSION.SDK_INT > 19) {
//                            imageViewDrawerIcon.setImageAlpha(255);
//                        } else {
//                            imageViewDrawerIcon.setAlpha(255);
//                        }
//                        textViewDrawerTitle.setTextColor(color);
//                        RelativeLayout relativeLayoutDrawerItem = (RelativeLayout) recyclerViewDrawer.getChildAt(i).findViewById(R.id.relativeLayoutDrawerItem);
//                        TypedValue typedValueDrawerSelected = new TypedValue();
//                        getTheme().resolveAttribute(R.attr.colorPrimary, typedValueDrawerSelected, true);
//                        int colorDrawerItemSelected = typedValueDrawerSelected.data;
//                        colorDrawerItemSelected = (colorDrawerItemSelected & 0x00FFFFFF) | 0x30000000;
//                        relativeLayoutDrawerItem.setBackgroundColor(colorDrawerItemSelected);

//                    } else {
//                        try {
//                            ImageView imageViewDrawerIcon = (ImageView) recyclerViewDrawer.getChildAt(i).findViewById(R.id.imageViewDrawerIcon);
//                            TextView textViewDrawerTitle = (TextView) recyclerViewDrawer.getChildAt(i).findViewById(R.id.textViewDrawerItemTitle);
//                            imageViewDrawerIcon.setColorFilter(getResources().getColor(R.color.md_text_white));
//                            if (Build.VERSION.SDK_INT > 19) {
//                                imageViewDrawerIcon.setImageAlpha(138);
//                            } else {
//                                imageViewDrawerIcon.setAlpha(138);
//                            }
//                            textViewDrawerTitle.setTextColor(getResources().getColor(R.color.md_text_white));
//                            RelativeLayout relativeLayoutDrawerItem = (RelativeLayout) recyclerViewDrawer.getChildAt(i).findViewById(R.id.relativeLayoutDrawerItem);
//                            //   relativeLayoutDrawerItem.setBackgroundColor(getResources().getColor(R.color.md_text));
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//
//                    }
//                }
//
//                // unregister listener (this is important)
//                recyclerViewDrawer.getViewTreeObserver().removeGlobalOnLayoutListener(this);
//            }
//        });


        // RecyclerView item listener.
//        ItemClickSupport itemClickSupport = ItemClickSupport.addTo(recyclerViewDrawer);
//        itemClickSupport.setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
//            @Override
//            public void onItemClick(RecyclerView parent, View view, final int position, long id) {
//
//                for (int i = 0; i < drawerTitles.length; i++) {
//                    if (i == position) {
//                        ImageView imageViewDrawerIcon = (ImageView) recyclerViewDrawer.getChildAt(i).findViewById(R.id.imageViewDrawerIcon);
//                        TextView textViewDrawerTitle = (TextView) recyclerViewDrawer.getChildAt(i).findViewById(R.id.textViewDrawerItemTitle);
//                        imageViewDrawerIcon.setColorFilter(color);
//                        if (Build.VERSION.SDK_INT > 19) {
//                            imageViewDrawerIcon.setImageAlpha(255);
//                        } else {
//                            imageViewDrawerIcon.setAlpha(255);
//                        }
//                        textViewDrawerTitle.setTextColor(color);
//                        RelativeLayout relativeLayoutDrawerItem = (RelativeLayout) recyclerViewDrawer.getChildAt(i).findViewById(R.id.relativeLayoutDrawerItem);
//                        TypedValue typedValueDrawerSelected = new TypedValue();
//                        getTheme().resolveAttribute(R.attr.colorPrimary, typedValueDrawerSelected, true);
//                        int colorDrawerItemSelected = typedValueDrawerSelected.data;
//                        colorDrawerItemSelected = (colorDrawerItemSelected & 0x00FFFFFF) | 0x30000000;
//                        relativeLayoutDrawerItem.setBackgroundColor(colorDrawerItemSelected);

//                    } else {
//                        ImageView imageViewDrawerIcon = (ImageView) recyclerViewDrawer.getChildAt(i).findViewById(R.id.imageViewDrawerIcon);
//                        TextView textViewDrawerTitle = (TextView) recyclerViewDrawer.getChildAt(i).findViewById(R.id.textViewDrawerItemTitle);
//                        imageViewDrawerIcon.setColorFilter(getResources().getColor(R.color.md_text_white));
//                        if (Build.VERSION.SDK_INT > 19) {
//                            imageViewDrawerIcon.setImageAlpha(138);
//                        } else {
//                            imageViewDrawerIcon.setAlpha(138);
//                        }
//                        textViewDrawerTitle.setTextColor(getResources().getColor(R.color.md_text_white));
//                        RelativeLayout relativeLayoutDrawerItem = (RelativeLayout) recyclerViewDrawer.getChildAt(i).findViewById(R.id.relativeLayoutDrawerItem);
//                        // relativeLayoutDrawerItem.setBackgroundColor(getResources().getColor(R.color.semi_trans));
//                    }
//                }

//                final Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        // Do something after some time
//                        setFragment(position);
//                        if (isExpand) {
//                            mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
//                        }
//                    }
//                }, 250);
//                mDrawerLayout.closeDrawers();
//            }
//        });


        ArrayList<String> stringArrayList = new ArrayList<>();
        stringArrayList.add("Home");
        stringArrayList.add("Sessions");
//        stringArrayList.add("Video");
        stringArrayList.add("Playlist");
        stringArrayList.add("Downloads");
//        stringArrayList.add("Community");
//        stringArrayList.add("Events");
//        stringArrayList.add("Meditations");
//        stringArrayList.add("Music");
//        stringArrayList.add("Interviews");
//        stringArrayList.add("Jokes");
//        stringArrayList.add("OSHO Centers");
        stringArrayList.add("About");
        stringArrayList.add("Contact us");
//        stringArrayList.add("Return Policy");
//        stringArrayList.add("Privacy Policy");
        stringArrayList.add("Reset password");
        stringArrayList.add("Logout");
//        R.drawable.ic_action_money,

//        R.drawable.ic_action_videow,
//        R.drawable.ic_action_meditation,
//                R.drawable.ic_action_music,
//                R.drawable.ic_action_interview,
//                R.drawable.ic_action_joke,
//                R.drawable.ic_action_osho_center,
//        R.drawable.ic_action_comunity,
//                R.drawable.help,
        int img[] = {R.drawable.ic_action_home, R.drawable.ic_action_audio,
                R.drawable.ic_action_playlist,
                R.drawable.ic_action_download,
//                R.drawable.ic_action_event,
                R.drawable.ic_action_terms,
                R.drawable.ic_contact_us,
//                R.drawable.ic_terms_cond,
//                R.drawable.ic_contact_us,
                R.drawable.ic_terms,
                R.drawable.ic_action_logout,
        };

        NavAdapter navAdapter = new NavAdapter(DashBoardActivity.this, stringArrayList, DashBoardActivity.this, img);
        recyclerViewDrawer.setAdapter(navAdapter);
    }


    private void registerReceiver() {

        LocalBroadcastManager bManager = LocalBroadcastManager.getInstance(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(MESSAGE_PROGRESS);
        bManager.registerReceiver(broadcastReceiver, intentFilter);

    }

    BroadcastReceiver broadcastReceivers = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // internet lost alert dialog method call from here...
            mLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
            MediaController.getInstance().pauseAudio(mNewSOngsDetails);
        }
    };
    private boolean downoaing = true;
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (downoaing)
                Utility.getSharedInstance().showProgressDialog(appcontectext);


            if (intent.getAction().equals(MESSAGE_PROGRESS)) {

                Download download = intent.getParcelableExtra("download");
                mProgressBar.setProgress(download.getProgress());


                Toast.makeText(DashBoardActivity.this, "Downloading  " + String.valueOf(download.getProgress() + " %"), Toast.LENGTH_SHORT).show();


                if (download.getProgress() == 100) {

                    downoaing = false;
                    isdownloading = false;
                    mProgressText.setText("Download complete");
                    mProgressBar.setVisibility(View.INVISIBLE);
                    mProgressText.setVisibility(View.VISIBLE);
                    mProgressText.setText("Downloaded");
                    Utility.getSharedInstance().dismissProgressDialog();
                    if (fragmentVideoDiscoureses != null) {
                        if (getEventResponseDataResponQue != null) {
                            if (getEventResponseDataResponQue.size() > 0) {
                                getEventResponseDataResponQue.remove(0);
                            }

                        }
                        if (getEventResponseDataResponQue != null) {
                            if (getEventResponseDataResponQue.size() > 0) {

                                downloadVDOSerise(getEventResponseDataResponQue.get(0));
                            }

                        }

                    }

                    if (mSongssSeries != null) {
                        if (mSongssSeries.size() > 0) {
                            mSongssSeries.remove(0);

                        }
                    }

                    if (mSongssSeries != null) {
                        if (mSongssSeries.size() > 0) {
                            downloadfromOtherFragmentSerise(mSongssSeries.get(0));
                        }
                    }

                } else {
                    downoaing = false;
                    isdownloading = true;
                    mProgressText.setText(String.format("Downloaded (%d/%d) MB", download.getCurrentFileSize(), download.getTotalFileSize()));

                }
            }
        }
    };


    private void initiSlidingUpPanel() {
        homeFirstPlayer = (RelativeLayout) findViewById(R.id.relayout);
        mLayout = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);
        songAlbumbg = (ImageView) findViewById(R.id.image_songAlbumbg_mid);
//        songAlbumbg.setAlpha(127);
        imgMusicLayout = (RelativeLayout) findViewById(R.id.imgMusicLayout);
        img_bottom_slideone = (ImageView) findViewById(R.id.img_bottom_slideone);
        imgCrossMark = (ImageView) findViewById(R.id.crossImg);
        img_bottom_slidetwo = (ImageView) findViewById(R.id.img_bottom_slidetwo);

        btn_dowloads = (ImageView) findViewById(R.id.btn_dowloads);


        mProgressBar = (ProgressBar) findViewById(R.id.progress);
        mProgressText = (TextView) findViewById(R.id.progress_text);

//        LinearLayout slideBttomLayput=(LinearLayout)findViewById(R.id.bottomLayout);
//        slideBttomLayput.setBackgroundColor(Color.parseColor("#050505"));
//        LinearLayout slideBttomLayputs=(LinearLayout) findViewById(R.id.slideBottomLayoutNew);
//        slideBttomLayputs.setBackgroundColor(Color.parseColor("#050505"));
//
//        LinearLayout bottomViewLayout=(LinearLayout) findViewById(R.id.bottomViewLayout);
//        bottomViewLayout.setBackgroundColor(Color.parseColor("#050505"));

        LinearLayout musicLayout = (LinearLayout) findViewById(R.id.musicLayout);
        musicLayout.setBackgroundColor(Color.parseColor("#050505"));


        imgCrossMark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isExpand = false;
                magoo = true;
                mLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
                MediaController.getInstance().stopAudio();
            }
        });


//    relativeLayoutDragView=(RelativeLayout)findViewById(R.id.dragView);
//        relativeLayoutDragView.setBackgroundColor(getResources().getColor(R.color.black));


        btnMore = (TextView) findViewById(R.id.btnMore);
        txt_timeprogress = (TextView) findViewById(R.id.slidepanel_time_progress);
        txt_timetotal = (TextView) findViewById(R.id.slidepanel_time_total);
        bgTitle = (TextView) findViewById(R.id.bgTitle);
        imgbtn_backward = (ImageView) findViewById(R.id.btn_backward);
        imgbtn_forward = (ImageView) findViewById(R.id.btn_forward);
        imgbtn_toggle = (ImageView) findViewById(R.id.btn_toggle);
        imgbtn_suffel = (ImageView) findViewById(R.id.btn_suffel);
        btn_playpause = (PlayPauseView) findViewById(R.id.btn_play);
        audio_progress = (Slider) findViewById(R.id.audio_progress_control);
        btn_playpausePanel = (PlayPauseView) findViewById(R.id.bottombar_play);
        bottombar_play_new = (ImageView) findViewById(R.id.bottombar_play_new);
        bottombar_play_new_1 = (ImageView) findViewById(R.id.bottombar_play_new_1);
        img_Favorite = (ImageView) findViewById(R.id.bottombar_img_Favorite);
        bottombar_moreicon = (ImageView) findViewById(R.id.bottombar_moreicon);
        progressBarMain = (ProgressBar) findViewById(R.id.progressMain);
        progressBarUpper = (ProgressBar) findViewById(R.id.progressUpper);
        TypedValue typedvaluecoloraccent = new TypedValue();
        getTheme().resolveAttribute(R.attr.colorAccent, typedvaluecoloraccent, true);
        final int coloraccent = typedvaluecoloraccent.data;
        audio_progress.setBackgroundColor(coloraccent);
        audio_progress.setValue(0);


        btn_dowloads.setOnClickListener(this);
        audio_progress.setOnValueChangedListener(this);
        imgbtn_backward.setOnClickListener(this);
        imgbtn_forward.setOnClickListener(this);
        imgbtn_toggle.setOnClickListener(this);
        imgbtn_suffel.setOnClickListener(this);
        img_Favorite.setOnClickListener(this);
        bottombar_moreicon.setOnClickListener(this);
        img_bottom_slidetwo.setOnClickListener(this);
        btnMore.setOnClickListener(this);

        btn_playpausePanel.Pause();
        btn_playpause.Pause();
        txt_time = (TextView) findViewById(R.id.txt_time);
        txt_playesongname = (TextView) findViewById(R.id.txt_playesongname);
        txt_songartistname = (TextView) findViewById(R.id.txt_songartistname);


        // txt_playesongname_slidetoptwo = (TextView) findViewById(R.id.txt_playesongname_slidetoptwo);
        //txt_songartistname_slidetoptwo = (TextView) findViewById(R.id.txt_songartistname_slidetoptwo);
        btnDownload = (TextView) findViewById(R.id.btnSongDowload);
        slidepanelchildtwo_topviewone = (RelativeLayout) findViewById(R.id.slidepanelchildtwo_topviewone);
        slidepanelchildtwo_topviewtwo = (RelativeLayout) findViewById(R.id.slidepanelchildtwo_topviewtwo);
        setPlayer(true);

//        }else {
//  mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);           slidepanelchildtwo_topviewone.setVisibility(View.VISIBLE);//INVISIBLE
//
//        }
        slidepanelchildtwo_topviewtwo.setVisibility(View.GONE);


        slidepanelchildtwo_topviewone.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //mLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);

            }
        });


//        slidepanelchildtwo_topviewtwo.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
//
//            }
//        });

        findViewById(R.id.bottombar_play).setOnClickListener(this);
        findViewById(R.id.btn_play).setOnClickListener(this);
        findViewById(R.id.bottombar_play_new).setOnClickListener(this);
        findViewById(R.id.bottombar_play_new_1).setOnClickListener(this);


        mLayout.setPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
                Log.i(TAG, "onPanelSlide, offset " + slideOffset);

                if (slideOffset == 0.0f) {
                    isExpand = false;
                    slidepanelchildtwo_topviewone.setVisibility(View.VISIBLE);
                    slidepanelchildtwo_topviewtwo.setVisibility(View.GONE);
                } else if (slideOffset > 0.0f && slideOffset < 1.0f) {


                } else {
                    isExpand = true;
                    slidepanelchildtwo_topviewone.setVisibility(View.INVISIBLE);
                    slidepanelchildtwo_topviewtwo.setVisibility(View.VISIBLE);

                    // Makes VISIBLE IF NEDED
                }

            }

            @Override
            public void onPanelExpanded(View panel) {
                Log.i(TAG, "onPanelExpanded");
                isExpand = true;
            }

            @Override
            public void onPanelCollapsed(View panel) {
                Log.i(TAG, "onPanelCollapsed");
                isExpand = false;
            }

            @Override
            public void onPanelAnchored(View panel) {
                Log.i(TAG, "onPanelAnchored");
            }

            @Override
            public void onPanelHidden(View panel) {
                Log.i(TAG, "onPanelHidden");
            }
        });

    }

    public void setFragment(int position) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        switch (position) {
            case 0:
                Badge();
                getLoginData();
                sharedPreferences.edit().putInt("FRAGMENT", position).apply();
                fragmentTransaction.replace(R.id.fragment, FragmentDashBoard.newInstance(0), String.valueOf(0));
                fragmentTransaction.commit();

                overridePendingTransition(
                        R.anim.slide_in_right_speed,
                        R.anim.slide_out_left_speed);
                break;


        }
    }

    private void giveAlertForSignOut() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Logout");
        alertDialogBuilder.setMessage("Are you sure,You want to logout?");

        alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                MusicSQLiteHelper.deleteAdUserMst();
                adapterDrawer.notifyDataSetChanged();
            }
        });

        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });


        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void openLoginDialog() {
        final Dialog dialog = new Dialog(DashBoardActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);

        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);


        final EditText email = (EditText) dialog.findViewById(R.id.txtEmail);
        final EditText pass = (EditText) dialog.findViewById(R.id.txtPass);

        TextView btnLogin = (TextView) dialog.findViewById(R.id.button_create_profile_ok);
        TextView btnForgot = (TextView) dialog.findViewById(R.id.btnForgot);
        TextView btnClickRegister = (TextView) dialog.findViewById(R.id.tv_resend);
        TextView btnFacebook = (TextView) dialog.findViewById(R.id.button_create_profile_facebook);
        TextView btnGoogle = (TextView) dialog.findViewById(R.id.button_create_profile_google);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TbsindUtil.isNull(email.getText())) {
                    email.setError("Enter your email and try again!");
                    email.requestFocus();
                } else if (TbsindUtil.isNull(pass.getText())) {
                    pass.setError("Enter your password and try again!");
                    pass.requestFocus();
                } else {
                    checkUserExist(dialog, email.getText().toString(), pass.getText().toString());
                }
            }
        });
        btnGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        btnFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        btnForgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showForgetPasswordDialog();
                dialog.dismiss();
            }
        });
        btnClickRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showRegisterDialog();
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    //Forget Password Dialog
    private void showForgetPasswordDialog() {
        final Dialog newDialog = new Dialog(DashBoardActivity.this);
        newDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        newDialog.setContentView(R.layout.dialog_forget);

        newDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);


        final EditText email = (EditText) newDialog.findViewById(R.id.txtForgetEmail);


        TextView btnSend = (TextView) newDialog.findViewById(R.id.btnSend);
        TextView btnCancel = (TextView) newDialog.findViewById(R.id.btnCancel);

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TbsindUtil.isNull(email.getText())) {
                    email.setError("Please enter your email!");
                    email.requestFocus();
                } else {
                    verifyEmailOnServer(newDialog, email.getText().toString());
                }
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newDialog.dismiss();
            }
        });
        newDialog.show();
    }

    //Forget Password Network Call
    private void verifyEmailOnServer(final Dialog newDialog, String s) {
        AndroidNetworking.post(TbsindUtil.SERVER_URL_FORGET).addBodyParameter("email", s).setTag("forgot").setPriority(Priority.LOW).build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("success").equals("1")) {
                                newDialog.dismiss();
                                Toast.makeText(DashBoardActivity.this, "Password reset check your email and login", Toast.LENGTH_SHORT).show();
                            } else if (response.getString("success").equals("0")) {
                                Toast.makeText(DashBoardActivity.this, response.getString("message"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });

    }

    //Login Network call
    private void checkUserExist(final Dialog dialog, final String email, String pass) {

        AndroidNetworking.post(TbsindUtil.SERVER_URL_LOGIN).addBodyParameter("email", email).addBodyParameter("password", pass).setTag("login").setPriority(Priority.LOW).build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.d("TAG", "RESPONSE " + response.toString());
                            if (response.getString("success").equals("1")) {
                                JSONArray array = response.getJSONArray("userData");
                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject object = array.getJSONObject(i);
                                    TblUserMst userMst = new TblUserMst();
                                    userMst.setUsername(object.getString("name"));
                                    userMst.setEmail(object.getString("email"));
                                    userMst.setDob(object.getString("dob"));
                                    userMst.setMobile(object.getString("mobile"));
                                    userMst.setId(object.getString("id"));
                                    MusicSQLiteHelper.insertAdUserMst(userMst);
                                    tvName.setText(object.getString("name").replace("%20", " "));
                                }
                                dialog.dismiss();
                                adapterDrawer.notifyDataSetChanged();
                            } else if (response.getString("success").equals("0")) {
                                Toast.makeText(DashBoardActivity.this, response.getString("message"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });

    }

    private void showRegisterDialog() {

        final Dialog dialog = new Dialog(DashBoardActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_register);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        final EditText name = (EditText) dialog.findViewById(R.id.txtRgName);
        final EditText email = (EditText) dialog.findViewById(R.id.txtRgEmail);
        final EditText mobile = (EditText) dialog.findViewById(R.id.txtRgMobile);
        final EditText dob = (EditText) dialog.findViewById(R.id.txtRgDt);
        final EditText pass = (EditText) dialog.findViewById(R.id.txtRgPass);
        final EditText txtSecurityCode = (EditText) dialog.findViewById(R.id.txtRgSecurity);
        final TextView security = (TextView) dialog.findViewById(R.id.txtSecurity);
        Integer random = TbsindUtil.randInt(0, 99999);

        security.setText(String.valueOf(random).replace("%20", " "));

        //SET DOB
        calendarDate = Calendar.getInstance();

        txtDatePicker = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year,
                                  int month, int day) {
                calendarDate.set(Calendar.YEAR, year);
                calendarDate.set(Calendar.MONTH, month);
                calendarDate.set(Calendar.DAY_OF_MONTH, day);
                month += 1;
                String month1;

                if (month < 10) {
                    month1 = "0" + month;
                } else {
                    month1 = "" + month;
                }
                if (day < 10) {
                    date = "" + "0" + day + "-" + month1 + "-" +
                            year;
                    System.out.println(date);
                } else {
                    date = "" + day + "-" + month1 + "-" + year;
                    System.out.println(date);

                }

                dob.setText(date.replace("%20", " "));
            }
        };
        dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDateClick(v);
            }
        });


        TextView btnRefresh = (TextView) dialog.findViewById(R.id.txtRefresh);
        TextView btnClickLogin = (TextView) dialog.findViewById(R.id.tv_go_login);
        TextView btnClickRegister = (TextView) dialog.findViewById(R.id.button_register);
        btnClickLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openLoginDialog();
                dialog.dismiss();
            }
        });
        btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer randomNew = TbsindUtil.randInt(0, 99999);
                security.setText(String.valueOf(randomNew).replace("%20", " "));
            }
        });

        btnClickRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TbsindUtil.isNull(name.getText())) {
                    name.setError("Please enter your name!");

                } else if (TbsindUtil.isNull(email.getText())) {
                    email.setError("Please enter your email id!");
                } else if (TbsindUtil.isNull(mobile.getText())) {
                    mobile.setError("Please enter your mobile number!");
                } else if (TbsindUtil.isNull(pass.getText())) {
                    pass.setError("Please enter password must be 1 digit and alphabets!");
                } else if (TbsindUtil.isNull(dob.getText())) {
                    dob.setError("Select your birth date!");
                } else if (TbsindUtil.isNull(txtSecurityCode.getText())) {
                    txtSecurityCode.setError("Invalid Security code");

                } else {
                    sendUserDataToServer(dialog, name.getText().toString(),
                            email.getText().toString(),
                            mobile.getText().toString(),
                            dob.getText().toString(), pass.getText().toString());
                }
            }
        });
        dialog.show();
    }

    public void onDateClick(View vw) {
        DatePickerDialog dialog = new DatePickerDialog(vw.getContext(),
                txtDatePicker, calendarDate.get(Calendar.YEAR),
                calendarDate.get(Calendar.MONTH),
                calendarDate.get(Calendar.DAY_OF_MONTH));
        dialog.getDatePicker().setMaxDate(calendarDate.getTime().getTime());
        dialog.show();
    }

    //Registration Dialog Network call
    private void sendUserDataToServer(final Dialog dialog, final String name, final String email, final String mobile, final String dob, String pass) {
        String SERVER_URL = TbsindUtil.SERVER_URL_REGISTER;
        HashMap<String, String> params = new HashMap<>();
        params.put("name", name);
        params.put("email", email);
        params.put("mobile", mobile);
        params.put("dob", dob);
        params.put("password", pass);
        Log.d("TAG", "PARAMS VALUES =" + SERVER_URL + params);

        AndroidNetworking.post(SERVER_URL).addBodyParameter(params).setTag("register").setPriority(Priority.MEDIUM).build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.d("TAG", "RESPONSE " + response.toString());
                            if (response.getString("success").equals("1")) {
                                TblUserMst userMst = new TblUserMst();
                                userMst.setUsername(name);
                                userMst.setEmail(email);
                                userMst.setDob(dob);
                                userMst.setMobile(mobile);
                                userMst.setId(response.getString("userId"));
                                MusicSQLiteHelper.insertAdUserMst(userMst);
                                Toast.makeText(DashBoardActivity.this, "Thank you!", Toast.LENGTH_SHORT).show();
                                dialog.dismiss();
                                tvName.setText(name.replace("%20", " "));
                                adapterDrawer.notifyDataSetChanged();

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });


    }


    //Catch  theme changed from settings
    public void theme() {
        sharedPreferences = getSharedPreferences("VALUES", Context.MODE_PRIVATE);
        theme = sharedPreferences.getInt("THEME", 0);
        PlayerUtility.settingTheme(context, theme);
    }

    private void loadImageLoaderOption() {
        this.options = new DisplayImageOptions.Builder().showImageOnLoading(R.drawable.bg_default_album_art)
                .showImageForEmptyUri(R.drawable.bg_default_album_art).showImageOnFail(R.drawable.bg_default_album_art).cacheInMemory(true)
                .cacheOnDisk(true).considerExifParams(true).bitmapConfig(Bitmap.Config.RGB_565).build();
    }

    public void deleteFileData(SongDetail songDetail) {


        DataSQLHelper dataSQLHelper = new DataSQLHelper(getApplicationContext());
        SQLiteDatabase sqLiteDatabase;
        sqLiteDatabase = dataSQLHelper.getWritableDatabase();
        String getJson = "";
        dataSQLHelper.delete_Row_where(sqLiteDatabase, TABLE_NAME, "id", songDetail.getIds());
        File fileo = new File(getApplicationContext().getFilesDir(), songDetail.getFile());
        fileo.delete();
        if (fragmentDownloads != null)
            fragmentDownloads.getDownlodedSong();

    }


    private static class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

        static final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            if (loadedImage != null) {
                ImageView imageView = (ImageView) view;
                boolean firstDisplay = !displayedImages.contains(imageUri);
                if (firstDisplay) {
                    FadeInBitmapDisplayer.animate(imageView, 500);
                    displayedImages.add(imageUri);
                }
            }
        }

    }

    private void loadAlreadyPlayng() {
        SongDetail mSongDetail = MusicPreferance.getLastSong(context);
        ArrayList<SongDetail> playlist = MusicPreferance.getPlaylist(context);
        if (mSongDetail != null) {
            updateTitle(false);
        }
        MediaController.getInstance().checkIsFavorite(context, mSongDetail, img_Favorite);
    }

    public void addObserver() {
        TAG_Observer = MediaController.getInstance().generateObserverTag();
        NotificationManager.getInstance().addObserver(this, NotificationManager.audioDidReset);
        NotificationManager.getInstance().addObserver(this, NotificationManager.audioPlayStateChanged);
        NotificationManager.getInstance().addObserver(this, NotificationManager.audioDidStarted);
        NotificationManager.getInstance().addObserver(this, NotificationManager.audioProgressDidChanged);
        NotificationManager.getInstance().addObserver(this, NotificationManager.newaudioloaded);
    }

    public void removeObserver() {
        NotificationManager.getInstance().removeObserver(this, NotificationManager.audioDidReset);
        NotificationManager.getInstance().removeObserver(this, NotificationManager.audioPlayStateChanged);
        NotificationManager.getInstance().removeObserver(this, NotificationManager.audioDidStarted);
        NotificationManager.getInstance().removeObserver(this, NotificationManager.audioProgressDidChanged);
        NotificationManager.getInstance().removeObserver(this, NotificationManager.newaudioloaded);
    }

    public void loadSongsDetails(final ArrayList<SongDetail> mDetail, final int pos) {


        //  mNxtSongDetails = mDetail.get(pos);
//

        mNxtSongDetailsArraylist = mDetail;
        mNxtSgPOs = pos;


    }


    public void loadSongsDetails(final List<SongDetail> mDetail, final int pos) {


//        mNxtSongDetails = mDetail.get(pos);
//
        mNxtSongDetailsArraylist = new ArrayList<>();

        //  mNxtSongDetailsArraylist=mDetail


        mNxtSongDetailsArraylist.addAll(mDetail);
        mNxtSgPOs = pos;


    }


    public void loadSongsDetails(final SongDetail mDetail) {

        mNewSOngsDetails = mDetail;
//        mNxtSongDetailsArraylist.add(mDetail);
//        mNxtSongDetailsArraylist.addAll(mDetail);
        mProgressBar.setVisibility(View.GONE);
        mProgressText.setVisibility(View.GONE);

        isExpand = true;
        slidepanelchildtwo_topviewone.setVisibility(View.INVISIBLE);
        slidepanelchildtwo_topviewtwo.setVisibility(View.VISIBLE);
        mLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);


        SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_TRACK_ID, String.valueOf(mDetail.getId()));

        String mediaID = "";
        mediaID = mDetail.getMedia_type_id();
        if (mediaID != null && mediaID != "") {
            if (mediaID.equalsIgnoreCase("26")) {
                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_TRACK_TYPE, "2");
            } else if (mediaID.equalsIgnoreCase("27")) {

                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_TRACK_TYPE, "4");
            } else if (mediaID.equalsIgnoreCase("28")) {
                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_TRACK_TYPE, "3");
            } else {
                /*if(mDetail.getQuedisc().equalsIgnoreCase("1")) {
                    SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_TRACK_TYPE, "1");
                } else {
                    SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_TRACK_TYPE, "6");
                }*/
                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_TRACK_TYPE, "1");
            }
        }
        // startAnim();

        // Utility.getSharedInstance().showProgressDialog(this);


        mode = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_MUSIC_MODE, "none");


        // addSongs=mDetail;
//        FFmpegMediaMetadataRetriever retriever = new FFmpegMediaMetadataRetriever();
//        retriever.setDataSource(mDetail.getPath());
//        String time = retriever.extractMetadata(FFmpegMediaMetadataRetriever.METADATA_KEY_DURATION);
//        Log.d("TAG","time "+time);
//        long timeInmillisec = Long.parseLong( time );
//        long duration1 = timeInmillisec / 1000;
//
//        byte [] data = retriever.getEmbeddedPicture();


// convert the byte array to a bitmap
//        Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
//// do something with the image ...
// songAlbumbg.setImageBitmap(bitmap);
//
//        retriever.release();


        if (mode.equalsIgnoreCase("music")) {
            Picasso.with(DashBoardActivity.this).load(Config.PIC_URL + Uri.encode(mDetail.getCover())).error(R.drawable.default_music).into(songAlbumbg);
            Picasso.with(DashBoardActivity.this).load(Config.PIC_URL + Uri.encode(mDetail.getCover())).error(R.drawable.default_music).into(songAlbumbg);
        } else if (mode.equalsIgnoreCase("meditation")) {
            Picasso.with(DashBoardActivity.this).load(Config.PIC_URL + Uri.encode(mDetail.getCover())).error(R.drawable.default_music).into(songAlbumbg);
            Picasso.with(DashBoardActivity.this).load(Config.PIC_URL + Uri.encode(mDetail.getCover())).error(R.drawable.default_music).into(songAlbumbg);
        } else if (mode.equalsIgnoreCase("interview")) {
            Picasso.with(DashBoardActivity.this).load(Config.PIC_URL + Uri.encode(mDetail.getCover())).error(R.drawable.default_music).into(songAlbumbg);
            Picasso.with(DashBoardActivity.this).load(Config.PIC_URL + Uri.encode(mDetail.getCover())).error(R.drawable.default_music).into(songAlbumbg);
        } else {
            Picasso.with(DashBoardActivity.this).load(com.music.nyasaa.retrofit.Config.PIC_URL + Uri.encode(mDetail.getCover())).error(R.drawable.default_music).into(songAlbumbg);
            Picasso.with(DashBoardActivity.this).load(com.music.nyasaa.retrofit.Config.PIC_URL + Uri.encode(mDetail.getCover())).error(R.drawable.default_music).into(songAlbumbg);

//            Picasso.with(DashBoardActivity.this).load(com.music.av.retrofit.Config.PIC_URL + Uri.encode(mDetail.getCover())).error(R.drawable.default_music).into(imgMusicLayout);
        }

        if (mDetail.getUrl().length() > 0)


            if (mode.equalsIgnoreCase("music")) {
                Picasso.with(DashBoardActivity.this).load(Config.PIC_URL + Uri.encode(mDetail.getCover())).error(R.drawable.default_music).into(img_bottom_slideone);
            } else if (mode.equalsIgnoreCase("meditation")) {
                Picasso.with(DashBoardActivity.this).load(Config.PIC_URL + Uri.encode(mDetail.getCover())).error(R.drawable.default_music).into(img_bottom_slideone);
            } else if (mode.equalsIgnoreCase("interview")) {
                Picasso.with(DashBoardActivity.this).load(Config.PIC_URL + Uri.encode(mDetail.getCover())).error(R.drawable.default_music).into(img_bottom_slideone);
            } else {
                Picasso.with(DashBoardActivity.this).load(com.music.nyasaa.retrofit.Config.PIC_URL + Uri.encode(mDetail.getCover())).error(R.drawable.default_music).into(img_bottom_slideone);
            }


//        imageLoader.displayImage(mDetail.getUrl(), songAlbumbg, options);
//        imageLoader.displayImage(com.music.av.retrofit.Config.BASE_URL_FOR_PLAY + """, img_bottom_slideone, options);


        if (mDetail.getName() != null) {
            bgTitle.setSelected(true);
            bgTitle.setText(mDetail.getName().replace("%20", " "));
            txt_playesongname.setText(mDetail.getName().replace("%20", " "));
            txt_songartistname.setText("Nyasaa");
        }


        if (mode.equalsIgnoreCase("music")) {
            Picasso.with(DashBoardActivity.this).load(Config.PIC_URL + Uri.encode(mDetail.getCover())).error(R.drawable.default_music).into(img_bottom_slideone);
        } else if (mode.equalsIgnoreCase("meditation")) {
            Picasso.with(DashBoardActivity.this).load(Config.PIC_URL + Uri.encode(mDetail.getCover())).error(R.drawable.default_music).into(img_bottom_slideone);
        } else if (mode.equalsIgnoreCase("interview")) {
            Picasso.with(DashBoardActivity.this).load(Config.PIC_URL + Uri.encode(mDetail.getCover())).error(R.drawable.default_music).into(img_bottom_slideone);
        } else {
            Picasso.with(DashBoardActivity.this).load(com.music.nyasaa.retrofit.Config.PIC_URL + Uri.encode(mDetail.getCover())).error(R.drawable.default_music).into(img_bottom_slideone);
        }


        // txt_playesongname_slidetoptwo.setText(mDetail.getTitle());
        // txt_songartistname_slidetoptwo.setText(mDetail.getArtist());
        btnDownload.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
//                if (!mDetail.getType().equals("html")) {
//                    TbsindUtil.startDownload(DashBoardActivity.this, mDetail.getPath(), mDetail.getTitle());
//                } else {
//                    Toast.makeText(DashBoardActivity.this, "can`t save offline this song, only mp3 songs save in offline", Toast.LENGTH_SHORT).show();
//                }
            }
        });


        if (txt_timetotal != null && !mDetail.getDuration().equals("00:00")) {
            if (mDetail.getDuration().length() > 0) {
                try {
                    long duration = Long.valueOf(mDetail.getDuration());
                    txt_timetotal.setText(duration != 0 ? String.format("%d:%02d", duration / 60, duration % 60) : "-:--");
                } catch (Exception e) {
                }


//                formateMilliSeccond(Long.valueOf(mDetail.getDuration()));
            }


        }

        // load data file
        MediaMetadataRetriever metaRetriever = new MediaMetadataRetriever();

        try {
            metaRetriever.setDataSource("" + mDetail.getFile());
        } catch (Exception e) {
        }

        try {

            String out = "";
            // get mp3 info

            // convert duration to minute:seconds
            String duration =
                    metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
//        Log.v("time", duration);
            long dur = Long.parseLong(duration);
            String seconds = String.valueOf((dur % 60000) / 1000);

            Log.v("seconds", seconds);
            String minutes = String.valueOf(dur / 60000);
            out = minutes + ":" + seconds;
            if (seconds.length() == 1) {
//            txtTime.setText("0" + minutes + ":0" + seconds);
                Toast.makeText(DashBoardActivity.this, "" + "0" + minutes + ":0" + seconds, Toast.LENGTH_SHORT).show();


            } else {
//            txtTime.setText("0" + minutes + ":" + seconds);
                Toast.makeText(DashBoardActivity.this, "" + "0" + minutes + ":0" + seconds, Toast.LENGTH_SHORT).show();
            }
            Log.v("minutes", minutes);
        } catch (Exception e) {

        }

        // close object
        metaRetriever.release();
        updateProgress(mDetail);

    }

    public void setDownloadToADOPlayer(final SongDetail mDetail) {

        if (mDetail.getName() != null) {
            bgTitle.setText(mDetail.getName().replace("%20", " "));
            txt_playesongname.setText(mDetail.getName().replace("%20", " "));
            txt_songartistname.setText("Downloaded");
        }
    }

    public void loadSongsDetailsForDownload(final SongDetail mDetail) {


        isExpand = true;
        slidepanelchildtwo_topviewone.setVisibility(View.INVISIBLE);
        slidepanelchildtwo_topviewtwo.setVisibility(View.VISIBLE);


//        Utility.getSharedInstance().showProgressDialog(getApplicationContext());
//        final Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                //Do something after 100ms
//                Utility.getSharedInstance().dismissProgressDialog();
//            }
//        }, 3000);


//        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.setMessage("Look at this dialog!")
//                .setCancelable(false)
//                .setPositiveButton("", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        //do things
//                    }
//                });
//        final AlertDialog alert = builder.create();
//        alert.show();
//
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                //Do something after 100ms
//                alert.dismiss();
//            }
//        }, 5000);


//        mProgressBar.setVisibility(View.VISIBLE);
//        mProgressText.setVisibility(View.VISIBLE);


        // addSongs=mDetail;
//        FFmpegMediaMetadataRetriever retriever = new FFmpegMediaMetadataRetriever();
//        retriever.setDataSource(mDetail.getPath());
//        String time = retriever.extractMetadata(FFmpegMediaMetadataRetriever.METADATA_KEY_DURATION);
//        Log.d("TAG","time "+time);
//        long timeInmillisec = Long.parseLong( time );
//        long duration1 = timeInmillisec / 1000;
//
//        byte [] data = retriever.getEmbeddedPicture();


// convert the byte array to a bitmap
//        Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
//// do something with the image ...
// songAlbumbg.setImageBitmap(bitmap);
//
//        retriever.release();
        //Picasso.with(DashBoardActivity.this).load(mDetail.getUrl()).error(R.drawable.disc).into(songAlbumbg);
//        Picasso.with(DashBoardActivity.this).load(mDetail.getUrl()).error(R.drawable.disc).into(songAlbumbg);
        if (mDetail.getUrl().length() > 0)

            if (mode.equalsIgnoreCase("music")) {
                Picasso.with(DashBoardActivity.this).load(Config.PIC_URL + Uri.encode(mDetail.getCover())).error(R.drawable.default_music).into(img_bottom_slideone);
            } else if (mode.equalsIgnoreCase("meditation")) {
                Picasso.with(DashBoardActivity.this).load(Config.PIC_URL + Uri.encode(mDetail.getCover())).error(R.drawable.default_music).into(img_bottom_slideone);
            } else if (mode.equalsIgnoreCase("interview")) {
                Picasso.with(DashBoardActivity.this).load(Config.PIC_URL + Uri.encode(mDetail.getCover())).error(R.drawable.default_music).into(img_bottom_slideone);
            } else {
                Picasso.with(DashBoardActivity.this).load(Config.PIC_URL + Uri.encode(mDetail.getCover())).error(R.drawable.default_music).into(img_bottom_slideone);
            }
//        imageLoader.displayImage(mDetail.getUrl(), songAlbumbg, options);
//        imageLoader.displayImage(mDetail.getUrl(), img_bottom_slideone, options);


        if ((mDetail.getFile() != null)) {
            bgTitle.setText(mDetail.getFile().replace("%20", " "));
            txt_playesongname.setText(mDetail.getName().replace("%20", " "));
        }

        txt_songartistname.setText("Song no: " + mDetail.getId());


        // txt_playesongname_slidetoptwo.setText(mDetail.getTitle());
        // txt_songartistname_slidetoptwo.setText(mDetail.getArtist());
        btnDownload.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!mDetail.getType().equals("html")) {
                    TbsindUtil.startDownload(DashBoardActivity.this, mDetail.getPath(), mDetail.getTitle());
                } else {
                    Toast.makeText(DashBoardActivity.this, "can`t save offline this song, only mp3 songs save in offline", Toast.LENGTH_SHORT).show();
                }
            }
        });


        try {
            if (txt_timetotal != null && !mDetail.getDuration().equals("00:00")) {
                if (mDetail.getDuration().length() > 0) {
                    long duration = Long.valueOf(mDetail.getDuration());
                    txt_timetotal.setText(duration != 0 ? String.format("%d:%02d", duration / 60, duration % 60) : "-:--");
                }
            }
        } catch (Exception e) {
        }


        updateProgress(mDetail);
    }

    @Override
    public void didReceivedNotification(int id, Object... args) {
        if (id == NotificationManager.audioDidStarted || id == NotificationManager.audioPlayStateChanged || id == NotificationManager.audioDidReset) {
            updateTitle(id == NotificationManager.audioDidReset && (Boolean) args[1]);
            //startProgressDiloag();


        } else if (id == NotificationManager.audioProgressDidChanged) {
            SongDetail mSongDetail = MediaController.getInstance().getPlayingSongDetail();
            updateProgress(mSongDetail);

            //stopDiloagProgress();
        }


    }

//    void startProgressDiloag()
//    {
//        Utility.getSharedInstance().showProgressDialog(this);
//    }
//
//    void stopDiloagProgress()
//    {
//        Utility.getSharedInstance().dismissProgressDialog();
//    }

    @Override
    public void newSongLoaded(Object... args) {
        MediaController.getInstance().checkIsFavorite(context, (SongDetail) args[0], img_Favorite);
    }


    private void updateTitle(boolean shutdown) {
        SongDetail mSongDetail = MediaController.getInstance().getPlayingSongDetail();
        if (mSongDetail == null && shutdown) {
            return;
        } else {
            updateProgress(mSongDetail);
            if (MediaController.getInstance().isAudioPaused()) {
                btn_playpausePanel.Pause();
                btn_playpause.Pause();

            } else {
                btn_playpausePanel.Play();
                btn_playpause.Play();
            }
            SongDetail audioInfo = MediaController.getInstance().getPlayingSongDetail();
            loadSongsDetails(audioInfo);


            try {
                if (txt_timetotal != null) {
                    long duration = Long.valueOf(audioInfo.getDuration());
                    txt_timetotal.setText(duration != 0 ? String.format("%d:%02d", duration / 60, duration % 60) : "-:--");
                }
            } catch (Exception e) {
            }

        }
    }

    private void updateProgress(SongDetail mSongDetail) {
        txt_timeprogress = (TextView) findViewById(R.id.slidepanel_time_progress);
        txt_timetotal = (TextView) findViewById(R.id.slidepanel_time_total);
        txt_time = (TextView) findViewById(R.id.txt_time);


        if (audio_progress != null) {
            // When SeekBar Draging Don't Show Progress


            if (!isDragingStart) {

                // loadSongsDetails(audioInfo);


                // Progress Value comming in point it range 0 to 1
                audio_progress.setValue((int) (mSongDetail.audioProgress * 100));
//                Toast.makeText(DashBoardActivity.this, "" + (int) (mSongDetail.audioProgress * 100), Toast.LENGTH_SHORT).show();
            }
            String timeString = String.format("%d:%02d", mSongDetail.audioProgressSec / 60, mSongDetail.audioProgressSec % 60);
            txt_timeprogress.setText(timeString);
            txt_time.setText(timeString);

            SongDetail audioInfo = MediaController.getInstance().getPlayingSongDetail();

            if (mSongDetail.audioProgressSec != 0) {

                try {
                    if (txt_timetotal != null) {

                        long duration = Long.valueOf(audioInfo.getDuration());
                        txt_timetotal.setText(duration != 0 ? String.format("%d:%02d", (duration - mSongDetail.audioProgressSec) / 60, (duration - mSongDetail.audioProgressSec) % 60) : "-:--");

                        if (duration == mSongDetail.audioProgressSec) {
                            //Autoplaylist.clear();
                            //AutoIndex
                            // positionsofDashbord

                            if (first) {
                                first = false;
                                int ind = AutoIndex + 1;
                                if (ind < Autoplaylist.size())
                                    new AutoPlayInit(Autoplaylist, ind, positionsofDashbord);
                                autoPlay();
                            }


                        } else {
                            first = true;
                        }
                    }
                } catch (Exception e) {

                }


            }

            double time = 0.0;
            timeString = timeString.replace(":", ".");
//            timeString=timeString.replace(":",".");
            try {
                time = Double.parseDouble(timeString);

            } catch (Exception e) {

            }


            //Toast.makeText(DashBoardActivity.this, ""+time, Toast.LENGTH_SHORT).show();

            if (time == 0.55 || time > 0.55) {
////
//                MediaController.getInstance().stopAudio();
//                mLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);


                if (subscribe != null && subscribe.isShowing()) {

                } else {

//                    subscribe.show();
                    mSubscribeBut.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            Intent intent = new Intent(DashBoardActivity.this, PaymentActivity.class);
                            startActivity(intent);
                            overridePendingTransition(R.anim.left, R.anim.right);
                            subscribe.dismiss();

                        }
                    });
                }


            }


        }


    }

    public void setStatus(Boolean play) {
        if (play) {
            bottombar_play_new.setImageResource(R.drawable.ic_pause_black_18dp);
            bottombar_play_new_1.setImageResource(R.drawable.ic_pause_black_18dp);


        } else {
            bottombar_play_new.setImageResource(R.drawable.ic_play_arrow_18dp);
            bottombar_play_new_1.setImageResource(R.drawable.ic_play_arrow_18dp);
        }
    }

    public void firstTimePlay() {

    }

    private void PlayPauseEvent(View v) {

        mProgressBar.setVisibility(View.GONE);
        mProgressText.setVisibility(View.GONE);


//
        if (jokesFragment != null) {
            jokesFragment.playPoase(PostionOfPlay);
        }
        if (fragmentMySongss != null) {
            fragmentMySongss.playPoase(PostionOfPlay);
        }

        if (fragmentCommunityDetails != null) {
            fragmentCommunityDetails.playPoase(PostionOfPlay);
        }

        if (fragmentVideoDiscoureses != null) {
            fragmentVideoDiscoureses.playPoase(PostionOfPlay);
        }
        if (fragmentDownloads != null) {
            fragmentDownloads.playPoase(PostionOfPlay);
        }

        if (FragmentPlayListSongss != null) {
            FragmentPlayListSongss.playPoase(PostionOfPlay);
        }

        if (fragmentDownloads != null) {
            fragmentDownloads.playPoase(PostionOfPlay);
        }

        if (fragmentMeditation != null) {
            fragmentMeditation.playPoase(PostionOfPlay);
        }


        if (fragmentInterview != null) {
            fragmentInterview.playPoase(PostionOfPlay);
        }

        if (fragmentMusics != null) {
            fragmentMusics.playPoase(PostionOfPlay);
        }


        if (fragmentDashBoards != null) {
            if (positionsofDashbord == 1) {
                fragmentDashBoards.playPoaseAudio(PostionOfPlay);
            }
            if (positionsofDashbord == 2) {
                fragmentDashBoards.playPoaseAudioDis(PostionOfPlay);
            }

            if (positionsofDashbord == 3) {
                fragmentDashBoards.playPoaseMusic(PostionOfPlay);
            }

            if (positionsofDashbord == 4) {
                fragmentDashBoards.playPoaseInterview(PostionOfPlay);
            }

            if (positionsofDashbord == 5) {
                fragmentDashBoards.playPoaseMidition(PostionOfPlay);
            }

        }


        if (MediaController.getInstance().isAudioPaused()) {


            MediaController.getInstance().playAudio(MediaController.getInstance().getPlayingSongDetail());
            // ((PlayPauseView) v).Play();
            bottombar_play_new.setImageResource(R.drawable.ic_pause_black_18dp);
            bottombar_play_new_1.setImageResource(R.drawable.ic_pause_black_18dp);


        } else {
            MediaController.getInstance().pauseAudio(MediaController.getInstance().getPlayingSongDetail());
            // ((PlayPauseView) v).Pause();
            bottombar_play_new.setImageResource(R.drawable.ic_play_arrow_18dp);
            bottombar_play_new_1.setImageResource(R.drawable.ic_play_arrow_18dp);
        }
    }

    @Override
    public void onValueChanged(int value) {
        MediaController.getInstance().seekToProgress(MediaController.getInstance().getPlayingSongDetail(), (float) value / 100);
    }


    private void systembartiteniam() {
        try {
            setTranslucentStatus(true);
            TypedValue typedValueStatusBarColor = new TypedValue();
            getTheme().resolveAttribute(R.attr.colorPrimaryDark, typedValueStatusBarColor, true);
            final int colorStatusBar = typedValueStatusBarColor.data;

            SystemBarTintManager tintManager = new SystemBarTintManager(this);
            tintManager.setStatusBarTintEnabled(true);
            tintManager.setStatusBarTintColor(colorStatusBar);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setTranslucentStatus(boolean on) {
        Window win = getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        final int bits = WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS;
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_search) {
            // startActivity(new Intent(this, FragmentSearch.class));
            // overridePendingTransition(R.anim.slide_in_right_speed, R.anim.slide_out_left_speed);
            return true;

        } else if (item.getItemId() == android.R.id.home) {
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    public void setPlayer(boolean b) {


        if (b) {
            slidepanelchildtwo_topviewone.setVisibility(View.GONE);
            mLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
        } else {
            slidepanelchildtwo_topviewone.setVisibility(View.VISIBLE);
            mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        }
    }

    public void hidePlayer() {


        // mLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);

    }


    private void getBannerImgList(GetBannerResponse getBannerResponse) {

        getBannerResponseData = new ArrayList<>();
        getBannerResponseData = getBannerResponse.getResultObject();


        //AnimationDrawable animation = new AnimationDrawable();
        // animation.addFrame(getBannerResponseData.get(0).getImage(), 1500);
//        animation.addFrame(getResources().getDrawable(R.drawable.sun), 1500);
//        animation.addFrame(getResources().getDrawable(R.drawable.idea_ad), 1500);
//        animation.addFrame(getResources().getDrawable(R.drawable.docomo), 1500);
//        animation.addFrame(getResources().getDrawable(R.drawable.add), 1500);
        // animation.setOneShot(false);

        imageAnim = (ImageView) findViewById(R.id.imgbanner);
        //imageAnim.setBackgroundDrawable(animation);

        // start the animation!
        //animation.start();


        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            int i = 0;

            public void run() {
                Picasso.with(DashBoardActivity.this).load("http://52.8.218.210/music_beta/admin/uploads/banner/" + getBannerResponseData.get(i).getImage()).into(imageAnim);


                i++;
                if (i > getBannerResponseData.size() - 1) {
                    i = 0;
                }
                handler.postDelayed(this, 2500);
            }
        };
        handler.postDelayed(runnable, 2500);


    }

//    private void initializeDraggablePanel() throws Resources.NotFoundException {
//        draggablePanel.setFragmentManager(getSupportFragmentManager());
//
//        // draggablePanel.setFragmentManager();
//        draggablePanel.setTopFragment(fragmentAllSongs);
//        draggablePanel.setBottomFragment(demoFragment);
//        draggablePanel.initializeView();
//    }


    public static void rotateImage() {
//        rotate.setDuration(5000);
//        rotate.setRepeatCount(Animation.INFINITE);
//        songAlbumbg.startAnimation(rotate);
    }

    public static void stop_rotateImage() {
//        rotate.setDuration(5000);
//        rotate.setRepeatCount(1);
//        songAlbumbg.startAnimation(rotate);
    }


    public void openshorAudio(String fragval) {
        cart_badge.setVisibility(View.GONE);


        if (fragval.equalsIgnoreCase("video")) {

//            mLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
//            MediaController.getInstance().stopAudio();
////            isExpand = true;
//
//            fragmentManager = getSupportFragmentManager();
//            fragmentManager.popBackStack(BACK_STACK_ROOT_TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);
//
//            // Add the new tab fragment
//            fragmentManager.beginTransaction()
//                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
//                    .replace(R.id.fragment, FragmentMyVideos.newInstance(0), String.valueOf(0))
//                    .addToBackStack(BACK_STACK_ROOT_TAG)
//                    // .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
//                    .commit();
//            mDrawer.closeMenu();
//            toolbarStatusBar(" Videos");

        }/* else if (fragval.equalsIgnoreCase("meditation")) {


            fragmentManager = getSupportFragmentManager();
            fragmentManager.popBackStack(BACK_STACK_ROOT_TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);

            // Add the new tab fragment
            fragmentManager.beginTransaction()
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .replace(R.id.fragment, FragmentMeditation.newInstance(0), String.valueOf(0))
                    .addToBackStack(BACK_STACK_ROOT_TAG)
                    // .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .commit();
            mDrawer.closeMenu();
            toolbarStatusBar(" Meditation");

        }*/ else if (fragval.equalsIgnoreCase("music")) {


            fragmentManager = getSupportFragmentManager();
            fragmentManager.popBackStack(BACK_STACK_ROOT_TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);

            // Add the new tab fragment
            fragmentManager.beginTransaction()
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .replace(R.id.fragment, FragmentMusic.newInstance(0), String.valueOf(0))
                    .addToBackStack(BACK_STACK_ROOT_TAG)
                    // .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .commit();
            mDrawer.closeMenu();
            toolbarStatusBar(" Music");

        } else if (fragval.equalsIgnoreCase("interview")) {


            fragmentManager = getSupportFragmentManager();
            fragmentManager.popBackStack(BACK_STACK_ROOT_TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);

            // Add the new tab fragment
            fragmentManager.beginTransaction()
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .replace(R.id.fragment, FragmentInterview.newInstance(0), String.valueOf(0))
                    .addToBackStack(BACK_STACK_ROOT_TAG)
                    // .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .commit();
            mDrawer.closeMenu();
            toolbarStatusBar(" Interviews");

        } else {
            fragmentManager = getSupportFragmentManager();
            fragmentManager.popBackStack(BACK_STACK_ROOT_TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);

            // Add the new tab fragment
            fragmentManager.beginTransaction()
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .replace(R.id.fragment, FragmentMySongs.newInstance(0), String.valueOf(0))
                    .addToBackStack(BACK_STACK_ROOT_TAG)
                    // .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .commit();
            mDrawer.closeMenu();
            toolbarStatusBar(" Audio");
        }


    }


    public void openVidioDiscourses() {


        fragmentManager = getSupportFragmentManager();
        fragmentManager.popBackStack("videoss", FragmentManager.POP_BACK_STACK_INCLUSIVE);

        // Add the new tab fragment
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .replace(R.id.fragment, FragmentVideoDiscoureses.newInstance(0), String.valueOf(0))
                .addToBackStack("videoss")
                // .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
        mDrawer.closeMenu();
        toolbarStatusBar(" Discourse");
        cart_badge.setVisibility(View.GONE);
        mToolbar.setVisibility(View.GONE);
    }


    public void openAudioDiscourses() {


        fragmentManager = getSupportFragmentManager();
        fragmentManager.popBackStack("Song", FragmentManager.POP_BACK_STACK_INCLUSIVE);

        // Add the new tab fragment
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .replace(R.id.fragment, FragmentAudioDiscourses.newInstance(0), String.valueOf(0))
                .addToBackStack("Song")
                // .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
        mDrawer.closeMenu();
        toolbarStatusBar(" Discourse");
        toolbarStatusBar(" Discourse");
        cart_badge.setVisibility(View.GONE);

        mToolbar.setVisibility(View.GONE);

    }


    public void openAudioFragment() {


        fragmentManager = getSupportFragmentManager();
        fragmentManager.popBackStack("Song", FragmentManager.POP_BACK_STACK_INCLUSIVE);

        // Add the new tab fragment
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .replace(R.id.fragment, FragmentMySongs.newInstance(0), String.valueOf(0))
                .addToBackStack("Song")
                // .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
        mDrawer.closeMenu();
        toolbarStatusBar(" AudioCat");
        cart_badge.setVisibility(View.GONE);

    }

    public void openVideoFragment() {


//        fragmentManager = getSupportFragmentManager();
//        fragmentManager.popBackStack("Video", FragmentManager.POP_BACK_STACK_INCLUSIVE);
//
//        // Add the new tab fragment
//        fragmentManager.beginTransaction()
//                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
//                .replace(R.id.fragment, FragmentMyVideos.newInstance(0), String.valueOf(0))
//                .addToBackStack("Video")
//                // .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
//                .commit();
//        mDrawer.closeMenu();
//        toolbarStatusBar(" VideoCat");
//        cart_badge.setVisibility(View.GONE);

    }

    public void openLongAudio(String fragval) {
        cart_badge.setVisibility(View.GONE);
        if (fragval.equalsIgnoreCase("video")) {

//            mLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
//            MediaController.getInstance().stopAudio();
//            fragmentManager = getSupportFragmentManager();
//            fragmentManager.popBackStack(BACK_STACK_ROOT_TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);
//
//            // Add the new tab fragment
//            fragmentManager.beginTransaction()
//                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
//                    .replace(R.id.fragment, FragmentMyVideos.newInstance(0), String.valueOf(0))
//                    .addToBackStack(BACK_STACK_ROOT_TAG)
//                    // .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
//                    .commit();
//            mDrawer.closeMenu();
//            toolbarStatusBar(" Videos");

        } else if (fragval.equalsIgnoreCase("music")) {


            fragmentManager = getSupportFragmentManager();
            fragmentManager.popBackStack(BACK_STACK_ROOT_TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);

            // Add the new tab fragment
            fragmentManager.beginTransaction()
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .replace(R.id.fragment, FragmentMusic.newInstance(0), String.valueOf(0))
                    .addToBackStack(BACK_STACK_ROOT_TAG)
                    // .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .commit();
            mDrawer.closeMenu();

            String getCat = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT, "");

            toolbarStatusBar(" Music");
        }/* else if (fragval.equalsIgnoreCase("meditation")) {

            fragmentManager = getSupportFragmentManager();
            fragmentManager.popBackStack(BACK_STACK_ROOT_TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);

            // Add the new tab fragment
            fragmentManager.beginTransaction()
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .replace(R.id.fragment, FragmentMeditation.newInstance(0), String.valueOf(0))
                    .addToBackStack(BACK_STACK_ROOT_TAG)
                    // .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .commit();
            mDrawer.closeMenu();

            String getCat = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT, "");
            if (getCat.equalsIgnoreCase(""))
                toolbarStatusBar(" Meditation");
        }*/ else if (fragval.equalsIgnoreCase("interview")) {


            fragmentManager = getSupportFragmentManager();
            fragmentManager.popBackStack(BACK_STACK_ROOT_TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);

            // Add the new tab fragment
            fragmentManager.beginTransaction()
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .replace(R.id.fragment, FragmentInterview.newInstance(0), String.valueOf(0))
                    .addToBackStack(BACK_STACK_ROOT_TAG)
                    // .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .commit();
            mDrawer.closeMenu();

            String getCat = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT, "");
            if (getCat.equalsIgnoreCase(""))
                toolbarStatusBar(" Interviews");


        } else {
            fragmentManager = getSupportFragmentManager();
            fragmentManager.popBackStack(BACK_STACK_ROOT_TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);

            // Add the new tab fragment
            fragmentManager.beginTransaction()
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .replace(R.id.fragment, FragmentMySongs.newInstance(0), String.valueOf(0))
                    .addToBackStack(BACK_STACK_ROOT_TAG)
                    // .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .commit();
            mDrawer.closeMenu();

            String getCat = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT, "");

            toolbarStatusBar(" Audio");
        }


    }


    public void openFragment(int pos) {

        switch (pos) {
            case 0:
                Badge();
                getLoginData();
                fragmentManager = getSupportFragmentManager();
                fragmentManager.popBackStack(BACK_STACK_ROOT_TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);

                // Add the new tab fragment
                fragmentManager.beginTransaction()

                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .replace(R.id.fragment, FragmentDashBoard.newInstance(0), String.valueOf(0))
//                        .addToBackStack(BACK_STACK_ROOT_TAG)
                        // .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .commit();
                overridePendingTransition(R.anim.left, R.anim.right);
                mDrawer.closeMenu();
                toolbarStatusBar(" Nyasaa");
                if (getCommunitySotredResponseData != null) {

                    if (getCommunitySotredResponseData.size() > 0) {
                        cart_badge.setVisibility(View.GONE);
                    }

                }


                break;

            case 1:
                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_DRAWER_TYPE, "");
                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_DRAWER_TYPE, "1");
                fragmentManager = getSupportFragmentManager();
                fragmentManager.popBackStack(BACK_STACK_ROOT_TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);

                // Add the new tab fragment
                fragmentManager.beginTransaction()
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .replace(R.id.fragment, FragmentAlbumList.newInstance(1), String.valueOf(0))
//                        .replace(R.id.fragment, FragmentMySongs.newInstance(1), String.valueOf(0))
                        .addToBackStack(BACK_STACK_ROOT_TAG)
                        // .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .commit();
                overridePendingTransition(R.anim.left, R.anim.right);
                mDrawer.closeMenu();
                toolbarStatusBar(" Album");
//                toolbarStatusBar(" Audio");
                cart_badge.setVisibility(View.GONE);
                break;
//            case 2:
//                mLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
//                MediaController.getInstance().stopAudio();
//                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_DRAWER_TYPE, "");
//                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_DRAWER_TYPE, "2");
//                fragmentManager = getSupportFragmentManager();
//                fragmentManager.popBackStack(BACK_STACK_ROOT_TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);
//
//                // Add the new tab fragment
//                fragmentManager.beginTransaction()
//                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
//                        .replace(R.id.fragment, FragmentVideoCommon.newInstance(0), String.valueOf(0))
//                        .addToBackStack(BACK_STACK_ROOT_TAG)
//                        // .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
//                        .commit();
//                mDrawer.closeMenu();
//                toolbarStatusBar(" Videos");
//                cart_badge.setVisibility(View.GONE);
//
//                break;

            case 2:

//                Toast.makeText(DashBoardActivity.this, "Work in progress", Toast.LENGTH_SHORT).show();
                fragmentManager = getSupportFragmentManager();
                fragmentManager.popBackStack(BACK_STACK_ROOT_TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);

                // Add the new tab fragment
                fragmentManager.beginTransaction()
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .replace(R.id.fragment, FragmentPlayList.newInstance(0), String.valueOf(0))
                        .addToBackStack(BACK_STACK_ROOT_TAG)
                        // .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .commit();
                mDrawer.closeMenu();
                toolbarStatusBar(" Playlist");
                cart_badge.setVisibility(View.GONE);
                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_FRAG_MAINTAIN, "0");

                break;

            case 3:
                fragmentManager = getSupportFragmentManager();
                fragmentManager.popBackStack(BACK_STACK_ROOT_TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                // Add the new tab fragment
                fragmentManager.beginTransaction()
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .replace(R.id.fragment, FragmentDownloads.newInstance(0), String.valueOf(0))
                        .addToBackStack(BACK_STACK_ROOT_TAG)
                        // .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .commit();
                mDrawer.closeMenu();
                toolbarStatusBar(" Downloaded");
                cart_badge.setVisibility(View.GONE);

                break;

//            case 5:
//                fragmentManager = getSupportFragmentManager();
//                fragmentManager.popBackStack(BACK_STACK_ROOT_TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);
//                // Add the new tab fragment
//                fragmentManager.beginTransaction()
//                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
//                        .replace(R.id.fragment, FragmentCommunity.newInstance(0), String.valueOf(0))
//                        .addToBackStack(BACK_STACK_ROOT_TAG)
//                        // .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
//                        .commit();
//                mDrawer.closeMenu();
//                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_FRAG_MAINTAIN, "1");
//                toolbarStatusBar(" Community");
//                cart_badge.setVisibility(View.GONE);
//
//                break;
//
//            case 4:
//
//                fragmentManager = getSupportFragmentManager();
//                fragmentManager.popBackStack(BACK_STACK_ROOT_TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);
//                // Add the new tab fragment
//                fragmentManager.beginTransaction()
//                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
//                        .replace(R.id.fragment, FragmentEvents.newInstance(0), String.valueOf(0))
//                        .addToBackStack(BACK_STACK_ROOT_TAG)
//                        // .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
//                        .commit();
//                mDrawer.closeMenu();
//                toolbarStatusBar(" Events");
//                cart_badge.setVisibility(View.GONE);
//
////                Toast.makeText(DashBoardActivity.this, "Work in progress", Toast.LENGTH_SHORT).show();
//
//                break;

//
//            case 7:
//
//                fragmentManager = getSupportFragmentManager();
//                fragmentManager.popBackStack(BACK_STACK_ROOT_TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);
//                // Add the new tab fragment
//                fragmentManager.beginTransaction()
//                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
//                        .replace(R.id.fragment, FragmentMeditation.newInstance(0), String.valueOf(0))
//                        .addToBackStack(BACK_STACK_ROOT_TAG)
//                        // .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
//                        .commit();
//                mDrawer.closeMenu();
//                toolbarStatusBar(" Meditation");
//                cart_badge.setVisibility(View.GONE);
//
//                break;

//            case 8:
//
//                fragmentManager = getSupportFragmentManager();
//                fragmentManager.popBackStack(BACK_STACK_ROOT_TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);
//                // Add the new tab fragment
//                fragmentManager.beginTransaction()
//                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
//                        .replace(R.id.fragment, FragmentMusic.newInstance(0), String.valueOf(0))
//                        .addToBackStack(BACK_STACK_ROOT_TAG)
//                        // .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
//                        .commit();
//                mDrawer.closeMenu();
//                toolbarStatusBar(" Music");
//                cart_badge.setVisibility(View.GONE);
//
//
//                break;
//            case 4:
//                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_DRAWER_TYPE, "9");
//
//                fragmentManager = getSupportFragmentManager();
//                fragmentManager.popBackStack(BACK_STACK_ROOT_TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);
//                // Add the new tab fragment
//                fragmentManager.beginTransaction()
//                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
//                        .replace(R.id.fragment, FragmentInterview.newInstance(0), String.valueOf(0))
//                        .addToBackStack(BACK_STACK_ROOT_TAG)
//                        // .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
//                        .commit();
//                mDrawer.closeMenu();
//                toolbarStatusBar(" Interviews");
//                cart_badge.setVisibility(View.GONE);
//
//
//                break;

//            case 18:
//
//                fragmentManager = getSupportFragmentManager();
//                fragmentManager.popBackStack(BACK_STACK_ROOT_TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);
//                // Add the new tab fragment
//                fragmentManager.beginTransaction()
//                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
//                        .replace(R.id.fragment, FragmentSubscription.newInstance(0), String.valueOf(0))
//                        .addToBackStack(BACK_STACK_ROOT_TAG)
//                        // .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
//                        .commit();
//                mDrawer.closeMenu();
//                //  toolbarStatusBar(" Subscription");
//                Intent intent = new Intent(DashBoardActivity.this, PaymentActivity.class);
//                startActivity(intent);
//                overridePendingTransition(R.anim.left, R.anim.right);
////                cart_badge.setVisibility(View.GONE);
//
//                break;
//            case 9:
//
//                break;

//            case 4:
//
//                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_DRAWER_TYPE, "7");
//
//                fragmentManager = getSupportFragmentManager();
//                fragmentManager.popBackStack(BACK_STACK_ROOT_TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);
//                // Add the new tab fragment
//                fragmentManager.beginTransaction()
//                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
//                        .replace(R.id.fragment, JokesFragment.newInstance(0), String.valueOf(0))
//                        .addToBackStack(BACK_STACK_ROOT_TAG)
//                        // .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
//                        .commit();
//                mDrawer.closeMenu();
//                toolbarStatusBar(" Jokes");
//                cart_badge.setVisibility(View.GONE);
//
////                Toast.makeText(DashBoardActivity.this, "Coming Soon", Toast.LENGTH_SHORT).show();
//                break;

//            case 11:
//
//                fragmentManager = getSupportFragmentManager();
//                fragmentManager.popBackStack(BACK_STACK_ROOT_TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);
//                // Add the new tab fragment
//                fragmentManager.beginTransaction()
//                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
//                        .replace(R.id.fragment, FragmentOshoCenter.newInstance(0), String.valueOf(0))
//                        .addToBackStack(BACK_STACK_ROOT_TAG)
//                        // .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
//                        .commit();
//                mDrawer.closeMenu();
//                toolbarStatusBar(" OSHO Centers");
//                cart_badge.setVisibility(View.GONE);
//                break;


            case 4:

                fragmentManager = getSupportFragmentManager();
                fragmentManager.popBackStack(BACK_STACK_ROOT_TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                // Add the new tab fragment
                fragmentManager.beginTransaction()
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .replace(R.id.fragment, FragmentTermsAndConditions.newInstance(0), String.valueOf(0))
                        .addToBackStack(BACK_STACK_ROOT_TAG)
                        // .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .commit();
                mDrawer.closeMenu();

                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_ABOUT,"0");
                toolbarStatusBar("About");
                cart_badge.setVisibility(View.GONE);
                break;

            case 5:
                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_ABOUT,"1");

                fragmentManager = getSupportFragmentManager();
                fragmentManager.popBackStack(BACK_STACK_ROOT_TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                // Add the new tab fragment
                fragmentManager.beginTransaction()
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .replace(R.id.fragment, FragmentTermsAndConditions.newInstance(0), String.valueOf(0))
                        .addToBackStack(BACK_STACK_ROOT_TAG)
                        // .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .commit();
                mDrawer.closeMenu();
                toolbarStatusBar(" Contact Us ");
                cart_badge.setVisibility(View.GONE);
//                Toast.makeText(DashBoardActivity.this, "We are happy to help you please post your message through EMAIL", Toast.LENGTH_LONG).show();
//
//                Intent i = new Intent(Intent.ACTION_SEND);
//                i.setType("message/rfc822");
//                i.putExtra(Intent.EXTRA_EMAIL, new String[]{"pmyana@yahoo.in"});
//                i.putExtra(Intent.EXTRA_SUBJECT, "NYASSA SUPPORT ");
//                i.putExtra(Intent.EXTRA_TEXT, "");
//                i.setType("message/rfc822");
//                try {
//                    startActivity(Intent.createChooser(i, "Send mail..."));
//                } catch (android.content.ActivityNotFoundException ex) {
//
//                    Toast.makeText(this, "" + "There are no email clients installed.", Toast.LENGTH_SHORT).show();
//                }
//                contactUsDialog = new Dialog(this);
//                contactUsDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                contactUsDialog.setContentView(R.layout.dialog_contact_us);
//                contactUsDialog.getWindow().getAttributes().width = ActionBar.LayoutParams.FILL_PARENT;
//                contactUsDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
//                contactUsDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//
//                LinearLayout mEmailsend = (LinearLayout) contactUsDialog.findViewById(R.id.SendBankLayout);
//                LinearLayout mCallUs = (LinearLayout) contactUsDialog.findViewById(R.id.SendPaytmLayout);
//                //contactUsDialog.show();
//
//                mEmailsend.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//
//                        sendEmail();
//                        contactUsDialog.dismiss();
//
//                    }
//                });
//
//
//                mCallUs.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//
//                        makePhoneCall();
//                        contactUsDialog.dismiss();
//
//                    }
//                });


                break;
            case 14:


                fragmentManager = getSupportFragmentManager();
                fragmentManager.popBackStack(BACK_STACK_ROOT_TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                // Add the new tab fragment
                fragmentManager.beginTransaction()
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .replace(R.id.fragment, FragmentHowToUse.newInstance(0), String.valueOf(0))
                        .addToBackStack(BACK_STACK_ROOT_TAG)
                        // .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .commit();
                mDrawer.closeMenu();
                toolbarStatusBar(" How to use");
                cart_badge.setVisibility(View.GONE);


                break;

            case 61:


                fragmentManager = getSupportFragmentManager();
                fragmentManager.popBackStack(BACK_STACK_ROOT_TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                // Add the new tab fragment
                fragmentManager.beginTransaction()
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .replace(R.id.fragment, FragmentReturnPolicy.newInstance(0), String.valueOf(0))
                        .addToBackStack(BACK_STACK_ROOT_TAG)
                        // .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .commit();
                mDrawer.closeMenu();
                toolbarStatusBar(" Return policy");
                cart_badge.setVisibility(View.GONE);

                break;


            case 71:


                fragmentManager = getSupportFragmentManager();
                fragmentManager.popBackStack(BACK_STACK_ROOT_TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                // Add the new tab fragment
                fragmentManager.beginTransaction()
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .replace(R.id.fragment, Privcay_policy.newInstance(0), String.valueOf(0))
                        .addToBackStack(BACK_STACK_ROOT_TAG)
                        // .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .commit();
                mDrawer.closeMenu();
                toolbarStatusBar(" Privacy policy");
                cart_badge.setVisibility(View.GONE);

                break;


            case 65:


                fragmentManager = getSupportFragmentManager();
                fragmentManager.popBackStack(BACK_STACK_ROOT_TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                // Add the new tab fragment
                fragmentManager.beginTransaction()
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .replace(R.id.fragment, FragmentTerms.newInstance(0), String.valueOf(0))
                        .addToBackStack(BACK_STACK_ROOT_TAG)
                        // .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .commit();
                mDrawer.closeMenu();
                toolbarStatusBar(" Terms & Conditions");
                cart_badge.setVisibility(View.GONE);

                break;


            case 6:


                dialog = new Dialog(DashBoardActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_reset_pass);
                dialog.getWindow().getAttributes().width = ActionBar.LayoutParams.FILL_PARENT;
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
                dialog.setCancelable(true);

                final EditText edtEmail = dialog.findViewById(R.id.edtEmail);

                Button mButYes = (Button) dialog.findViewById(R.id.butYes);

                mButYes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (edtEmail.getText().toString().trim().equalsIgnoreCase("")) {
                            Toast.makeText(DashBoardActivity.this, "Enter new password", Toast.LENGTH_LONG).show();
                        } else {
                            //
                            forgotPassword(edtEmail.getText().toString().trim());
                        }

                    }
                });

                break;

            case 7:

                confrimDeleteDialog = new Dialog(this);
                confrimDeleteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                confrimDeleteDialog.setContentView(R.layout.dialog_confrim_delete);
                confrimDeleteDialog.getWindow().getAttributes().width = ActionBar.LayoutParams.FILL_PARENT;
                confrimDeleteDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
                confrimDeleteDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                confrimDeleteDialog.show();

                Button mButYesN = (Button) confrimDeleteDialog.findViewById(R.id.butYes);
                Button mButNo = (Button) confrimDeleteDialog.findViewById(R.id.butNo);

                TextView mTxtMsg = (TextView) confrimDeleteDialog.findViewById(R.id.txtMsg);
                mTxtMsg.setText("Are you sure you want to logout?");


                TextView mTxtTitle = (TextView) confrimDeleteDialog.findViewById(R.id.txtDialogTitle);
                mTxtTitle.setText("Logout");


                mButNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        confrimDeleteDialog.dismiss();


                    }
                });

                mButYesN.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        MediaController.getInstance().stopAudio();
                        //commanon audio temp file while play is deleted.
                        File filei = new File(getApplicationContext().getFilesDir(), "tempfilenametoplay");
                        filei.delete();

                        SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_FNAME, "");
                        SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_MOB, "");
                        SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_LASTNAMES, "");
                        String login = SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_IS_LOGIN, "0");

                        Intent intent = new Intent(DashBoardActivity.this, LoginActivity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.left_anim, R.anim.right_anim);
                        finish();


                    }
                });
                break;

        }


    }


    public void sendEmail() {

//        Intent sendIntent = new Intent(Intent.ACTION_SEND);
//        sendIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"support@ask-osho.com"});
//        sendIntent.putExtra(Intent.EXTRA_SUBJECT, "ASK O.S.H.O SUPPORT");
//
//        sendIntent.setType("text/html");
//        startActivity(sendIntent);
//        try {
//            startActivity(Intent.createChooser(sendIntent, "Send mail..."));
//        } catch (android.content.ActivityNotFoundException ex) {
//
//            Toast.makeText(this, "" + "There are no email clients installed.", Toast.LENGTH_SHORT).show();
//        }


    }



    public void createMyPlayList(String name) {

        MyApplication myApplication = new MyApplication();

        Utility.getSharedInstance().showProgressDialog(this);


        if (Utility.getSharedInstance().isConnectedToInternet(this)) {

            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");

            // id="1";
            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");

            RequestCreatePlayList requestGetMyPlayList = new RequestCreatePlayList();

            requestGetMyPlayList.setUserId(Integer.parseInt(id));
            requestGetMyPlayList.setPlaylist_name(name);

            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().createMyPlayList(clientService, X_API_KEY, requestGetMyPlayList);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());

                    if (responseData != null) {


                        // JsonObject jsonObject=responseData.getAsJsonObject("respon");
                        Utility.getSharedInstance().dismissProgressDialog();
                        dialogPlayList.dismiss();
                        Toast.makeText(DashBoardActivity.this, "New playlist added to list", Toast.LENGTH_SHORT).show();
                        // Utility.getSharedInstance().Snackbar(toolbarLayout, "Password Changed Sucessfully");
                        fragmentManager = getSupportFragmentManager();
                        fragmentManager.popBackStack(BACK_STACK_ROOT_TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);

                        // Add the new tab fragment
                        fragmentManager.beginTransaction()
                                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                                .replace(R.id.fragment, FragmentPlayList.newInstance(0), String.valueOf(0))
                                .addToBackStack(BACK_STACK_ROOT_TAG)
                                // .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                                .commit();
//                        mDrawerLayout.closeDrawers();
                        toolbarStatusBar(" Playlist");

                    } else {
                        Utility.getSharedInstance().dismissProgressDialog();

                    }

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                    Utility.getSharedInstance().dismissProgressDialog();


                }


            });
        } else {
            Utility.getSharedInstance().dismissProgressDialog();
        }
    }

    public void showPlayListSongs(String playListName) {
        SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_FRAG_MAINTAIN, "2");

        fragmentManager = getSupportFragmentManager();
        fragmentManager.popBackStack("PlayList", FragmentManager.POP_BACK_STACK_INCLUSIVE);

        // Add the new tab fragment
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .replace(R.id.fragment, FragmentPlayListSongs.newInstance(0), String.valueOf(0))
                .addToBackStack("PlayList")
                // .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();

        toolbarStatusBar(" " + playListName);

    }


    public void loadFrag() {
        getLoginData();
        Badge();
//        Utility.getSharedInstance().dismissProgressDialog();

        fragmentManager = getSupportFragmentManager();


        // Add the new tab fragment
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .replace(R.id.fragment, FragmentDashBoard.newInstance(0), String.valueOf(0))
//                .addToBackStack(BACK_STACK_ROOT_TAG)
                // .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
        //  ClearFragments();
    }


    private void blink() {
        getLoginData();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {


//                Utility.getSharedInstance().dismissProgressDialog();

                if (mIsResumed) {

                    FragmentManager fragmentManager = getSupportFragmentManager();
                    fragmentManager.popBackStack(BACK_STACK_ROOT_TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);

                    // Add the new tab fragment
                    fragmentManager.beginTransaction()
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                            .replace(R.id.fragment, FragmentDashBoard.newInstance(0), String.valueOf(0))
//                            .addToBackStack(BACK_STACK_ROOT_TAG)
                            // .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                            .commit();
                } else {

                    dialogResume = new Dialog(context);
                    dialogResume.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialogResume.setContentView(R.layout.dialog_refresh);
                    dialogResume.getWindow().getAttributes().width = ActionBar.LayoutParams.FILL_PARENT;
                    dialogResume.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
                    dialogResume.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialogResume.setCancelable(false);
                    dialogResume.show();

                    Button buRefresh = (Button) dialogResume.findViewById(R.id.butYes);

                    buRefresh.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
//                            Intent intent = new Intent(DashBoardActivity.this,DashBoardActivity.class);
//                            startActivity(intent);
//                            finish();
                            dialogResume.dismiss();

                            FragmentManager fragmentManager = getSupportFragmentManager();
                            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

                            // Add the new tab fragment
                            fragmentManager.beginTransaction()
                                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                                    .replace(R.id.fragment, FragmentDashBoard.newInstance(0), String.valueOf(0))
                                    //.addToBackStack(null)
                                    // .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                                    .commit();
                        }
                    });
                }


            }
        }, 500);
//
// DashBoardActivity.this.runOnUiThread(new Runnable() {
//
//            @Override
//            public void run() {
//
//                // Stuff that updates the UIOn
//
//


//
////                Utility.getSharedInstance().dismissProgressDialog();
//
//                if (mIsResumed) {
//
//                    FragmentManager fragmentManager = getSupportFragmentManager();
//                    fragmentManager.popBackStack(BACK_STACK_ROOT_TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);
//
//                    // Add the new tab fragment
//                    fragmentManager.beginTransaction()
//                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
//                            .replace(R.id.fragment, FragmentDashBoard.newInstance(0), String.valueOf(0))
//                            .addToBackStack(BACK_STACK_ROOT_TAG)
//                            // .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
//                            .commit();
//                } else {
//
//                    dialogResume = new Dialog(context);
//                    dialogResume.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                    dialogResume.setContentView(R.layout.dialog_refresh);
//                    dialogResume.getWindow().getAttributes().width = ActionBar.LayoutParams.FILL_PARENT;
//                    dialogResume.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
//                    dialogResume.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//                    dialogResume.setCancelable(false);
//                    dialogResume.show();
//
//                    Button buRefresh = (Button) dialogResume.findViewById(R.id.butYes);
//
//                    buRefresh.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
////                            Intent intent = new Intent(DashBoardActivity.this,DashBoardActivity.class);
////                            startActivity(intent);
////                            finish();
//                            dialogResume.dismiss();
//
//                            FragmentManager fragmentManager = getSupportFragmentManager();
//                            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
//
//                            // Add the new tab fragment
//                            fragmentManager.beginTransaction()
//                                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
//                                    .replace(R.id.fragment, FragmentDashBoard.newInstance(0), String.valueOf(0))
//                                    //.addToBackStack(null)
//                                    // .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
//                                    .commit();
//                        }
//                    });
//                }
//
//
//            }
//        });
//


    }


    public void ClearFragments() {
        FragmentManager fragments = getSupportFragmentManager();
        fragments.popBackStack();

    }

    private void listMenus() {
        menuArrayList = new ArrayList<>();
        menuArrayList.add("Love");
        menuArrayList.add("Spiritual");
        menuArrayList.add("Religion");
        menuArrayList.add("Relationship");
        menuArrayList.add("Parenting");
        menuArrayList.add("Motherhood");
        menuArrayList.add("Freedom");
        menuArrayList.add("Friendship");
        menuArrayList.add("Education");
        menuArrayList.add("Sex");
        menuArrayList.add("Marriage");
        menuArrayList.add("Politics");
        menuArrayList.add("Mystics");
        menuArrayList.add("Buddha");
        menuArrayList.add("Feminism");
        menuArrayList.add("Birth Control");
        menuArrayList.add("Sufism");
        menuArrayList.add("Tantra");
        menuArrayList.add("Zen");
        menuArrayList.add("Science");
        menuArrayList.add("Astrology");
        menuArrayList.add("Nature");
        menuArrayList.add("God");
        menuArrayList.add("Family");
        menuArrayList.add("Communism");
        menuArrayList.add("Meditations");
        menuArrayList.add("Music");
        menuArrayList.add("Interviews");

    }


    //

    // API


    public void getAllFilterCategory(final View view) {

        MyApplication myApplication = new MyApplication();


        if (Utility.getSharedInstance().isConnectedToInternet(DashBoardActivity.this)) {


            Utility.getSharedInstance().showProgressDialog(DashBoardActivity.this);
            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");

            // id="1";
            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");

            String cat = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT, "1");
            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().getAllLangiages(clientService, X_API_KEY);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());


                    // Config.getPlanDAO = new GetPlanDAO();
                    Gson gson = new Gson();

                    Utility.getSharedInstance().dismissProgressDialog();


                    if (responseData.get("respon") != null) {

                        JsonArray jsonArray = responseData.get("respon").getAsJsonArray();


                        // JsonObject jsonObject=responseData.getAsJsonObject("respon");

                        if (jsonArray.size() > 0) {
                            PopupWindow popupwindow_obj = popupDisplay();
                            popupwindow_obj.showAsDropDown(view, -40, 18);


                            //connectionAdapter = new CommunityAdapter(getActivity(), community_dao);
                            // mMySongsListView.setAdapter(connectionAdapter);

                            final GetResponseCategory getResponseCategory = gson.fromJson(responseData, GetResponseCategory.class);
                            getResponseCategoryData = getResponseCategory.getRespon();

                            LanguageAdapter menuListAdapter = new LanguageAdapter(DashBoardActivity.this, getResponseCategoryData);
                            mMenuListView.setAdapter(menuListAdapter);


                            mMenuListView.addOnItemTouchListener(
                                    new RecyclerItemClickListener(context, new RecyclerItemClickListener.OnItemClickListener() {
                                        @Override
                                        public void onItemClick(View view, int position) {
                                            // TODO Handle item click
                                            //getMyPlayListResponseData.get(position).getName();


                                            String draweType = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_DRAWER_TYPE, "");
                                            String tab = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_AUDIO_TAB, "");

                                            popupWindow.dismiss();
//                                            if (draweType.equalsIgnoreCase("1")) {
//                                                try {
//
//
////         SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_CAT_ORIGINAL, "");
//                                                    SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_LANG_ID, getResponseCategoryData.get(position).getId());
//                                                    FragmentMySongs fragmentsongs = (FragmentMySongs) getSupportFragmentManager().findFragmentById(R.id.fragment);
//
//                                                    fragmentsongs.getSearchedSOngs("", 0);
//                                                } catch (Exception e) {
//                                                    e.printStackTrace();
//                                                }
//                                            } else if (draweType.equalsIgnoreCase("2")) {
//
//                                                try {
////         SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_CAT_ORIGINAL, "");
//                                                    SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_LANG_ID, getResponseCategoryData.get(position).getId());
//                                                    FragmentMyVideos fragmentsongs = (FragmentMyVideos) getSupportFragmentManager().findFragmentById(R.id.fragment);
//
//                                                    fragmentsongs.getSearchedVideos("", 0);
//                                                } catch (Exception e) {
//                                                    e.printStackTrace();
//                                                }
//
//
//                                            } else if (draweType.equalsIgnoreCase("3")) {
//
//                                                try {
////         SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_CAT_ORIGINAL, "");
//                                                    SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_LANG_ID, getResponseCategoryData.get(position).getId());
//         /*FragmentAudioDiscourses fragmentAudioDiscourses = (FragmentAudioDiscourses) getSupportFragmentManager().findFragmentById(R.id.fragment);
//         fragmentAudioDiscourses.firstTimeAPICALL();*/
//                                                    FragmentAudioCommon fragmentAudioCommon = (FragmentAudioCommon) getSupportFragmentManager().findFragmentById(R.id.fragment);
//                                                    fragmentAudioCommon.getAllDiscoursesAudio(false);
//                                                } catch (Exception e) {
//                                                    e.printStackTrace();
//                                                }
//
//
//                                            } else if (draweType.equalsIgnoreCase("7")) {
//
//                                                try {
////         SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_CAT_ORIGINAL, "");
//                                                    SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_LANG_ID, getResponseCategoryData.get(position).getId());
//                                                    JokesFragment fragmentMeditation = (JokesFragment) getSupportFragmentManager().findFragmentById(R.id.fragment);
//
//                                                    fragmentMeditation.getSearchedSOngs("");
//                                                } catch (Exception e) {
//                                                }
//
//
//                                            } else if (draweType.equalsIgnoreCase("5")) {
//
//                                                try {
////         SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_CAT_ORIGINAL, "");
//                                                    SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_LANG_ID, getResponseCategoryData.get(position).getId());
//                                                    FragmentMeditation fragmentMeditation = (FragmentMeditation) getSupportFragmentManager().findFragmentById(R.id.fragment);
//
//                                                    fragmentMeditation.getMedOnLangSelectd(getResponseCategoryData.get(position).getId());
//                                                } catch (Exception e) {
//                                                }
//
//
//                                            } else if (draweType.equalsIgnoreCase("4")) {
//
//                                                try {
////         SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_CAT_ORIGINAL, "");
//                                                    SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_LANG_ID, getResponseCategoryData.get(position).getId());
//         /*FragmentVideoDiscoureses fragmentAudioDiscourses = (FragmentVideoDiscoureses) getSupportFragmentManager().findFragmentById(R.id.fragment);
//         fragmentAudioDiscourses.firstTimeCall();*/
//                                                    FragmentVideoCommon fragmentVideoCommon = (FragmentVideoCommon) getSupportFragmentManager().findFragmentById(R.id.fragment);
//                                                    fragmentVideoCommon.getAllDiscoursesVideo(false);
//                                                } catch (Exception e) {
//                                                    e.printStackTrace();
//                                                }
//
//
//                                            } else if (draweType.equalsIgnoreCase("9")) {
//
//                                                try {
////         SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_CAT_ORIGINAL, "");
//                                                    SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_LANG_ID, getResponseCategoryData.get(position).getId());
//         /*FragmentVideoDiscoureses fragmentAudioDiscourses = (FragmentVideoDiscoureses) getSupportFragmentManager().findFragmentById(R.id.fragment);
//         fragmentAudioDiscourses.firstTimeCall();*/
//
//
//                                                    String getID = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_INETR_TAB, "1");
//
//
//                                                    FragmentInterview fragmentMeditation = (FragmentInterview) getSupportFragmentManager().findFragmentById(R.id.fragment);
//
//
//                                                    if (getID.equalsIgnoreCase("1")) {
//                                                        fragmentMeditation.getSearchedSOngs("", getResponseCategoryData.get(position).getId());
//                                                    } else
//                                                        fragmentMeditation.getSearchedVideso("", getResponseCategoryData.get(position).getId());
//
//                                                } catch (Exception e) {
//                                                    e.printStackTrace();
//                                                }
//
//
//                                            }


                                            try {
                                                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_LANG_ID, getResponseCategoryData.get(position).getId());
                                                FragmentMySongs fragmentsongs = (FragmentMySongs) getSupportFragmentManager().findFragmentById(R.id.fragment);

                                                fragmentsongs.getSearchedSOngs("", 0);
                                            } catch (Exception e) {

                                            }


                                            // Toast.makeText(fragmentMySongs.getContext(), "ID:-" +songID+ "PlayListName: " + getMyPlayListResponseData.get(position).getName(), Toast.LENGTH_SHORT).show();
                                        }
                                    })
                            );


                        } else {


                        }


                        // Utility.getSharedInstance().Snackbar(toolbarLayout, "Password Changed Sucessfully");


                    } else {


                    }

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {


                }


            });
        } else {


            Toast.makeText(DashBoardActivity.this, "No internet", Toast.LENGTH_SHORT).show();
        }
    }


    /// Accept or Reject


    public void acceptOrRejectRequest(String comId, String actionID) {


//        Utility.getSharedInstance().showProgressDialog(this);
        MyApplication myApplication = new MyApplication();


        if (Utility.getSharedInstance().isConnectedToInternet(this)) {
            Utility.getSharedInstance().showProgressDialog(this);

            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");

            // id="1";
            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");

            RequestToAcceptCommunity requestGetMyPlayList = new RequestToAcceptCommunity();

            requestGetMyPlayList.setAction(actionID);
            requestGetMyPlayList.setCommunityId(comId);
            requestGetMyPlayList.setUserId(id);

            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().acceptOrRejectRequest(X_API_KEY, requestGetMyPlayList);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());

                    if (responseData != null) {

                        if (responseData.get("status").getAsBoolean() == true) {

                            Utility.getSharedInstance().dismissProgressDialog();
                            popNotification.dismiss();
                            Badge();
                            getLoginData();
                            Toast.makeText(DashBoardActivity.this, "" + responseData.get("message").getAsString(), Toast.LENGTH_SHORT).show();

                        } else {
                            Utility.getSharedInstance().dismissProgressDialog();

                            Toast.makeText(DashBoardActivity.this, "" + responseData.get("message").getAsString(), Toast.LENGTH_SHORT).show();

                        }
                    }


                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                    Utility.getSharedInstance().dismissProgressDialog();

                    Toast.makeText(context, "Api failure", Toast.LENGTH_SHORT).show();
                }


            });
        } else {

            Toast.makeText(DashBoardActivity.this, "No internet connection ", Toast.LENGTH_SHORT).show();
        }
    }


    /// Get All Community


    public void getAllCoummity(final View view) {

        MyApplication myApplication = new MyApplication();


        final String userID = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "0");


        if (Utility.getSharedInstance().isConnectedToInternet(this)) {

            Utility.getSharedInstance().showProgressDialog(this);

            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");

            // id="1";
            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");

            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().allcombyusrid(id, X_API_KEY);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());


                    // Config.getPlanDAO = new GetPlanDAO();
                    Gson gson = new Gson();


                    Type listType = new TypeToken<List<Community_DAO>>() {
                    }.getType();

                    if (responseData != null) {
                        if (responseData.get("status").getAsString().equalsIgnoreCase("true")) {


                            Utility.getSharedInstance().dismissProgressDialog();


                            // JsonObject jsonObject=responseData.getAsJsonObject("respon");


                            GetCommunityResponse getCommunityResponse = gson.fromJson(responseData, GetCommunityResponse.class);

                            List<GetCommunityResponseData> getCommunityResponseData = getCommunityResponse.getResultObject();
                            List<SoretedCommunityData> getCommunitySotredResponseData = new ArrayList<>();


                            if (getCommunityResponseData != null) {

                                for (int i = 0; i < getCommunityResponseData.size(); i++) {
                                    if (getCommunityResponseData.get(i).getAction().equalsIgnoreCase("1") && getCommunityResponseData.get(i).getRequest().equalsIgnoreCase("1")) {

                                        SoretedCommunityData soretedCommunityData = new SoretedCommunityData();
                                        soretedCommunityData.setAction(getCommunityResponseData.get(i).getAction());
                                        soretedCommunityData.setCommunityName(getCommunityResponseData.get(i).getCommunityName());
                                        soretedCommunityData.setId(getCommunityResponseData.get(i).getId());
                                        soretedCommunityData.setImageUrl(getCommunityResponseData.get(i).getImageUrl());
                                        soretedCommunityData.setLongInfo(getCommunityResponseData.get(i).getLongInfo());
                                        soretedCommunityData.setShortInfo(getCommunityResponseData.get(i).getShortInfo());
                                        soretedCommunityData.setParentUserId(Integer.parseInt(getCommunityResponseData.get(i).getParentUserId()));
                                        soretedCommunityData.setUserId(getCommunityResponseData.get(i).getUserId());


                                        getCommunitySotredResponseData.add(soretedCommunityData);


                                    }


//                                if (getCommunityResponseData.get(i).getAction().equalsIgnoreCase("1") && getCommunityResponseData.get(i).getRequest().equalsIgnoreCase("2")) {
//
//                                    SoretedCommunityData soretedCommunityData = new SoretedCommunityData();
//                                    soretedCommunityData.setAction(getCommunityResponseData.get(i).getAction());
//                                    soretedCommunityData.setCommunityName(getCommunityResponseData.get(i).getCommunityName());
//                                    soretedCommunityData.setId(getCommunityResponseData.get(i).getId());
//                                    soretedCommunityData.setImageUrl(getCommunityResponseData.get(i).getImageUrl());
//                                    soretedCommunityData.setLongInfo(getCommunityResponseData.get(i).getLongInfo());
//                                    soretedCommunityData.setShortInfo(getCommunityResponseData.get(i).getShortInfo());
//                                    soretedCommunityData.setParentUserId(getCommunityResponseData.get(i).getParentUserId());
//                                    soretedCommunityData.setUserId(getCommunityResponseData.get(i).getUserId());
//
//
//                                    getCommunitySotredResponseData.add(soretedCommunityData);
//
//
//                                }


                                }

                                if (getCommunitySotredResponseData.size() > 0) {
                                    PopupWindow popupwindow_obj = popUpNotiifcation();
                                    popupwindow_obj.showAsDropDown(view, -40, 18);
                                    Utility.getSharedInstance().dismissProgressDialog();
                                    NotificationAdapter connectionAdapter = new NotificationAdapter(DashBoardActivity.this, getCommunitySotredResponseData);
                                    mMenuNotifcation.setAdapter(connectionAdapter);

                                } else {
//                            Utility.getSharedInstance().dismissProgressDialog();
                                    Toast.makeText(DashBoardActivity.this, "No request found", Toast.LENGTH_SHORT).show();

                                }
                            }

                        }

                    } else {

                        Utility.getSharedInstance().dismissProgressDialog();
                    }


                    // Utility.getSharedInstance().Snackbar(toolbarLayout, "Password Changed Sucessfully");


                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                    Utility.getSharedInstance().dismissProgressDialog();


                }


            });
        } else {

            Toast.makeText(this, "No internet coonection", Toast.LENGTH_SHORT).show();


        }
    }


    // Badge


    public void Badge() {

        MyApplication myApplication = new MyApplication();
        final String userID = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "0");

        if (Utility.getSharedInstance().isConnectedToInternet(this)) {

//            Utility.getSharedInstance().showProgressDialog(this);

            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");

            // id="1";
            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");

            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().allcombyusrid(id, X_API_KEY);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());


                    // Config.getPlanDAO = new GetPlanDAO();
                    Gson gson = new Gson();


                    Type listType = new TypeToken<List<Community_DAO>>() {
                    }.getType();
                    if (responseData != null) {
                        if (responseData.get("status").getAsString().equalsIgnoreCase("true")) {


//                        Utility.getSharedInstance().dismissProgressDialog();


                            // JsonObject jsonObject=responseData.getAsJsonObject("respon");


                            GetCommunityResponse getCommunityResponse = gson.fromJson(responseData, GetCommunityResponse.class);

                            List<GetCommunityResponseData> getCommunityResponseData = getCommunityResponse.getResultObject();
                            getCommunitySotredResponseData = new ArrayList<>();


                            if (getCommunityResponseData != null) {

                                for (int i = 0; i < getCommunityResponseData.size(); i++) {
                                    if (getCommunityResponseData.get(i).getAction().equalsIgnoreCase("1") && getCommunityResponseData.get(i).getRequest().equalsIgnoreCase("1")) {

                                        SoretedCommunityData soretedCommunityData = new SoretedCommunityData();
                                        soretedCommunityData.setAction(getCommunityResponseData.get(i).getAction());
                                        soretedCommunityData.setCommunityName(getCommunityResponseData.get(i).getCommunityName());
                                        soretedCommunityData.setId(getCommunityResponseData.get(i).getId());
                                        soretedCommunityData.setImageUrl(getCommunityResponseData.get(i).getImageUrl());
                                        soretedCommunityData.setLongInfo(getCommunityResponseData.get(i).getLongInfo());
                                        soretedCommunityData.setShortInfo(getCommunityResponseData.get(i).getShortInfo());
                                        soretedCommunityData.setParentUserId(Integer.parseInt(getCommunityResponseData.get(i).getParentUserId()));
                                        soretedCommunityData.setUserId(getCommunityResponseData.get(i).getUserId());


                                        getCommunitySotredResponseData.add(soretedCommunityData);


                                    }


//                                if (getCommunityResponseData.get(i).getAction().equalsIgnoreCase("1") && getCommunityResponseData.get(i).getRequest().equalsIgnoreCase("2")) {
//
//                                    SoretedCommunityData soretedCommunityData = new SoretedCommunityData();
//                                    soretedCommunityData.setAction(getCommunityResponseData.get(i).getAction());
//                                    soretedCommunityData.setCommunityName(getCommunityResponseData.get(i).getCommunityName());
//                                    soretedCommunityData.setId(getCommunityResponseData.get(i).getId());
//                                    soretedCommunityData.setImageUrl(getCommunityResponseData.get(i).getImageUrl());
//                                    soretedCommunityData.setLongInfo(getCommunityResponseData.get(i).getLongInfo());
//                                    soretedCommunityData.setShortInfo(getCommunityResponseData.get(i).getShortInfo());
//                                    soretedCommunityData.setParentUserId(getCommunityResponseData.get(i).getParentUserId());
//                                    soretedCommunityData.setUserId(getCommunityResponseData.get(i).getUserId());
//
//
//                                    getCommunitySotredResponseData.add(soretedCommunityData);
//
//
//                                }


                                }

                                if (getCommunitySotredResponseData.size() > 0) {

                                    imgEdit.setVisibility(View.VISIBLE);
                                    imgEdit.setImageResource(0);

                                    imgEdit.setImageResource(R.drawable.ic_notifications_black_36dp);
                                    cart_badge.setVisibility(View.GONE);
                                    cart_badge.setText("" + getCommunitySotredResponseData.size());

                                    NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(DashBoardActivity.this)
                                            .setSmallIcon(R.drawable.osho_logo) // notification icon
                                            .setContentTitle("Ask O.S.H.O") // title for notification
                                            .setContentText("Hey you have new notfication , check it by clicking bell icon") // message for notification
                                            .setAutoCancel(true)
                                            .setDefaults(Notification.DEFAULT_SOUND);

                                    // clear notification after click
                                    Intent intent = new Intent(DashBoardActivity.this, DashBoardActivity.class);
                                    @SuppressLint("WrongConstant") PendingIntent pi = PendingIntent.getActivity(DashBoardActivity.this, 0, intent, Intent.FLAG_ACTIVITY_NEW_TASK);
                                    mBuilder.setContentIntent(pi);
                                    android.app.NotificationManager mNotificationManager =
                                            (android.app.NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                                    mNotificationManager.notify(0, mBuilder.build());

//amdsasmda
                                } else {
                                    cart_badge.setVisibility(View.GONE);
//                            Utility.getSharedInstance().dismissProgressDialog();
//                                imgEdit.setVisibility(View.GONE);
//                                imgEdit.setImageResource(0);
//                                imgEdit.setImageResource(R.drawable.ic_notifications_new);

                                }
                            }


                        }

                    } else {

                        cart_badge.setVisibility(View.GONE);
                        imgEdit.setImageResource(0);
                        Utility.getSharedInstance().dismissProgressDialog();
                        imgEdit.setImageResource(R.drawable.ic_notifications_new);
                        cart_badge.setVisibility(View.GONE);
                    }


                    // Utility.getSharedInstance().Snackbar(toolbarLayout, "Password Changed Sucessfully");


                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

//                    Utility.getSharedInstance().dismissProgressDialog();
                }


            });
        } else {

            Toast.makeText(this, "No internet coonection", Toast.LENGTH_SHORT).show();


        }
    }


    //// Get Community to Share POST


    private void getAllCommunity(final Context fcontext) {

        final MyApplication myApplication = new MyApplication();


        if (Utility.getSharedInstance().isConnectedToInternet(fcontext)) {


            Utility.getSharedInstance().showProgressDialog(fcontext);

            final String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");

            // id="1";
            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");

            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().allcombyusrid(id, X_API_KEY);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());


                    // Config.getPlanDAO = new GetPlanDAO();
                    Gson gson = new Gson();

                    if (responseData != null) {
                        Type listType = new TypeToken<List<Community_DAO>>() {
                        }.getType();
                        if (responseData.get("status").getAsString().equalsIgnoreCase("true")) {


                            Utility.getSharedInstance().dismissProgressDialog();


                            // JsonObject jsonObject=responseData.getAsJsonObject("respon");


                            GetCommunityResponse getCommunityResponse = gson.fromJson(responseData, GetCommunityResponse.class);

                            List<GetCommunityResponseData> getCommunityResponseData = getCommunityResponse.getResultObject();

                            final List<GetCommunityResponseData> sortedCommunityList = new ArrayList<>();


                            for (int i = 0; i < getCommunityResponseData.size(); i++) {
                                if (getCommunityResponseData.get(i).getAction().equalsIgnoreCase("2") || id.equalsIgnoreCase(String.valueOf(getCommunityResponseData.get(i).getParentUserId()))) {
                                    GetCommunityResponseData getCommunityResponseData1 = new GetCommunityResponseData();
                                    getCommunityResponseData1.setAction(getCommunityResponseData.get(i).getAction());
                                    getCommunityResponseData1.setCommunityName(getCommunityResponseData.get(i).getCommunityName());
                                    getCommunityResponseData1.setCreatedAt(getCommunityResponseData.get(i).getCreatedAt());
                                    getCommunityResponseData1.setId(getCommunityResponseData.get(i).getId());
                                    getCommunityResponseData1.setImageUrl(getCommunityResponseData.get(i).getImageUrl());
                                    getCommunityResponseData1.setParentUserId(getCommunityResponseData.get(i).getParentUserId());
                                    getCommunityResponseData1.setLongInfo(getCommunityResponseData.get(i).getLongInfo());
                                    getCommunityResponseData1.setShortInfo(getCommunityResponseData.get(i).getShortInfo());
                                    getCommunityResponseData1.setUsername(getCommunityResponseData.get(i).getUsername());
                                    getCommunityResponseData1.setUserId(getCommunityResponseData.get(i).getUserId());
                                    sortedCommunityList.add(getCommunityResponseData1);
                                }


                            }

                            if (sortedCommunityList.size() > 0) {
                                ShareToCommunityAdapter connectionAdapter = new ShareToCommunityAdapter(fcontext, sortedCommunityList);
                                mListCommunity.setAdapter(connectionAdapter);
                                myPlayListDialog.show();
                                if (mLayout != null) {
                                    mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                                }

                                mListCommunity.addOnItemTouchListener(
                                        new RecyclerItemClickListener(fcontext, new RecyclerItemClickListener.OnItemClickListener() {
                                            @Override
                                            public void onItemClick(View view, int position) {
                                                // TODO Handle item click
                                                //getMyPlayListResponseData.get(position).getName();

                                                sharePostCommunmity(sortedCommunityList.get(position).getId());
                                                // Toast.makeText(fragmentMySongs.getContext(), "ID:-" +songID+ "PlayListName: " + getMyPlayListResponseData.get(position).getName(), Toast.LENGTH_SHORT).show();
                                            }
                                        })
                                );
                            } else {


                                Toast.makeText(fcontext, "No Community found", Toast.LENGTH_SHORT).show();

                            }


                        }

                    } else {


                        Utility.getSharedInstance().dismissProgressDialog();
                    }


                    // Utility.getSharedInstance().Snackbar(toolbarLayout, "Password Changed Sucessfully");


                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                    Utility.getSharedInstance().dismissProgressDialog();

                }


            });
        } else {

            Toast.makeText(fcontext, "Opps no internet connection", Toast.LENGTH_SHORT).show();


        }
    }


    public void showCommDialog(Context context) {
        if (context == null) {
            context = getApplication();
        }

        myPlayListDialog = new Dialog(context);
        myPlayListDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myPlayListDialog.setContentView(R.layout.dialog_add_songto_playlist);
        myPlayListDialog.getWindow().getAttributes().width = ActionBar.LayoutParams.FILL_PARENT;
        myPlayListDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        myPlayListDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        mTxtWarn = (TextView) myPlayListDialog.findViewById(R.id.txtWarn);
        TextView mTxtTile = (TextView) myPlayListDialog.findViewById(R.id.txtDialogTitle);
        mTxtTile.setText("Community");
        mListCommunity = (RecyclerView) myPlayListDialog.findViewById(R.id.listViewAlbum);


        mTxtWarn.setVisibility(View.GONE);

        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(context);
        mListCommunity.setLayoutManager(linearLayoutManager);
        mListCommunity.setItemAnimator(new DefaultItemAnimator());


        getAllCommunity(context);

    }


    /// Community Sgareing


    private void sharePostCommunmity(String commid) {

        MyApplication myApplication = new MyApplication();


        if (Utility.getSharedInstance().isConnectedToInternet(context)) {
            Utility.getSharedInstance().showProgressDialog(context);

            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");

            // id="1";
            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");

            String trackType = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_TRACK_TYPE, "none");
            String trackID = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_TRACK_ID, "none");

            RequestShareCommunityPost requestGetMyPlayList = new RequestShareCommunityPost();
            requestGetMyPlayList.setCommunityId(commid);
            requestGetMyPlayList.setTrackId(trackID);
            requestGetMyPlayList.setTrackType(trackType);
            requestGetMyPlayList.setUserId(id);
            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().addPostToCommunity(X_API_KEY, requestGetMyPlayList);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());

                    if (responseData != null) {
                        if (responseData.get("status").getAsBoolean() == true) {


                            Toast.makeText(context, "Shared succesfully", Toast.LENGTH_SHORT).show();


                            myPlayListDialog.dismiss();
                            Utility.getSharedInstance().dismissProgressDialog();

                        } else {
                            Utility.getSharedInstance().dismissProgressDialog();
                            Toast.makeText(context, "" + responseData.get("message").getAsString(), Toast.LENGTH_SHORT).show();


                            myPlayListDialog.dismiss();
                        }
                    } else {
                        Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
                    }


                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                    Utility.getSharedInstance().dismissProgressDialog();


                }


            });
        } else {
            Utility.getSharedInstance().dismissProgressDialog();
            Toast.makeText(context, "No internet connection", Toast.LENGTH_SHORT).show();
        }
    }


    public void openCommunityDetails(String communityName) {


        SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_FRAG_MAINTAIN, "1");
        fragmentManager = getSupportFragmentManager();
        fragmentManager.popBackStack("Details", FragmentManager.POP_BACK_STACK_INCLUSIVE);
        // Add the new tab fragment
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .replace(R.id.fragment, FragmentCommunityDetails.newInstance(0), String.valueOf(0))
                .addToBackStack("Details")
                // .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
        mDrawer.closeMenu();

        toolbarStatusBar(" Details");

        cart_badge.setVisibility(View.GONE);


        String commName = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_COMMUNITY_NAME, "");

//        toolbarStatusBar(" Details");

    }

    public void showDialog(final String songID) {
        myPlayListDialog = new Dialog(DashBoardActivity.this);
        myPlayListDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myPlayListDialog.setContentView(R.layout.dialog_add_songto_playlist);
        myPlayListDialog.getWindow().getAttributes().width = ActionBar.LayoutParams.FILL_PARENT;
        myPlayListDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        myPlayListDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        mTxtWarn = (TextView) myPlayListDialog.findViewById(R.id.txtWarn);
        mMyPlayList = (RecyclerView) myPlayListDialog.findViewById(R.id.listViewAlbum);

        mTxtWarn.setVisibility(View.GONE);

        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(context);
        mMyPlayList.setLayoutManager(linearLayoutManager);
        mMyPlayList.setItemAnimator(new DefaultItemAnimator());

        getAllMyList(DashBoardActivity.this);

        mMyPlayList.addOnItemTouchListener(
                new RecyclerItemClickListener(context, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        // TODO Handle item click
                        //getMyPlayListResponseData.get(position).getName();
                        savetoMyPlayList(songID, getMyPlayListResponseData.get(position).getId());
                        // Toast.makeText(fragmentMySongs.getContext(), "ID:-" +songID+ "PlayListName: " + getMyPlayListResponseData.get(position).getName(), Toast.LENGTH_SHORT).show();
                    }
                })
        );
    }


    public void getAllMyList(final Context context) {

        MyApplication myApplication = new MyApplication();

        Utility.getSharedInstance().showProgressDialog(context);


        if (Utility.getSharedInstance().isConnectedToInternet(context)) {

            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");

            // id="1";
            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");

            RequestGetMyPlayList requestGetMyPlayList = new RequestGetMyPlayList();

            requestGetMyPlayList.setUserId(id);
            requestGetMyPlayList.setxAPIKEY(X_API_KEY);

            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().getMyPlayList(requestGetMyPlayList);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());

                    if (responseData.get("error").getAsInt() == 0) {
                        // Config.getPlanDAO = new GetPlanDAO();
                        Gson gson = new Gson();

                        Utility.getSharedInstance().dismissProgressDialog();


                        if (responseData.get("respon") != null) {

                            JsonArray jsonArray = responseData.get("respon").getAsJsonArray();


                            if (jsonArray.size() > 0) {
                                myPlayListDialog.show();

                                mTxtWarn.setVisibility(View.GONE);
                                mMyPlayList.setVisibility(View.VISIBLE);


                                gson = new Gson();
                                GetMyPlayListResponse getMyPlayListResponse = gson.fromJson(responseData, GetMyPlayListResponse.class);
                                getMyPlayListResponseData = getMyPlayListResponse.getRespon();
                                myPlayListAdapter = new MyPlayListAdapters(context, getMyPlayListResponseData);
                                mMyPlayList.setAdapter(myPlayListAdapter);
                            } else {

                                Toast.makeText(context, "You do not have playlists", Toast.LENGTH_SHORT).show();
                                mTxtWarn.setVisibility(View.VISIBLE);
                                mMyPlayList.setVisibility(View.GONE);
                                Utility.getSharedInstance().dismissProgressDialog();

                            }


                        }


                    } else {
                        Utility.getSharedInstance().dismissProgressDialog();
                        mTxtWarn.setVisibility(View.VISIBLE);
                        mMyPlayList.setVisibility(View.GONE);

                    }

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                    Utility.getSharedInstance().dismissProgressDialog();
                    mTxtWarn.setVisibility(View.VISIBLE);
                    mMyPlayList.setVisibility(View.GONE);
                }


            });
        } else {
            Utility.getSharedInstance().dismissProgressDialog();
            mTxtWarn.setVisibility(View.VISIBLE);
            mMyPlayList.setVisibility(View.GONE);
        }
    }


    public void savetoMyPlayList(String trackId, String playListID) {

        MyApplication myApplication = new MyApplication();
        Utility.getSharedInstance().showProgressDialog(context);

        if (Utility.getSharedInstance().isConnectedToInternet(context)) {
            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");
            String trackType = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_TRACK_TYPE, "none");
            // id="1";
            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");
            RequestSaveToPlayList requestGetMyPlayList = new RequestSaveToPlayList();
            requestGetMyPlayList.setPlaylistId(playListID);
            requestGetMyPlayList.setTrackId(trackId);
            requestGetMyPlayList.setTrack_type(trackType);
            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().saveToPlayList(X_API_KEY, requestGetMyPlayList);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());
                    if (responseData != null) {
                        Toast.makeText(context, "Added successfully", Toast.LENGTH_SHORT).show();
                        myPlayListDialog.dismiss();
                        Utility.getSharedInstance().dismissProgressDialog();

                    } else {
                        Utility.getSharedInstance().dismissProgressDialog();
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Utility.getSharedInstance().dismissProgressDialog();
                }
            });
        } else {
            Utility.getSharedInstance().dismissProgressDialog();
            Toast.makeText(context, "No internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    public void hideBadge() {
        cart_badge.setVisibility(View.GONE);
    }

    @Override
    public void onBackStackChanged() {

    }

    /*public void shouldDisplayHomeUp(){
        //Enable Up button only  if there are entries in the back stack
        boolean canback = getSupportFragmentManager().getBackStackEntryCount()>1;
        if(getSupportActionBar()!=null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(canback);
    }*/

    @Override
    public boolean onSupportNavigateUp() {
        //This method is called when the up button is pressed. Just the pop back stack.
        getSupportFragmentManager().popBackStack();
        return true;
    }


    public void openMedTEch() {


        fragmentManager = getSupportFragmentManager();
        fragmentManager.popBackStack("Meditation", FragmentManager.POP_BACK_STACK_INCLUSIVE);
        // Add the new tab fragment
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .replace(R.id.fragment, FragmentMediatitationTch.newInstance(0), String.valueOf(0))
                .addToBackStack("Meditation")
                // .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
        mDrawer.closeMenu();

        String toolName = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_MED_TOOLNAME, " Meditation");
        toolbarStatusBar("Techniques");

    }

    public void openMedtation(int typeID) {
        SharedPreferenceManager.getInstance().writeInteger(SharedPreferenceManager.KEY_MED_TYPE_ID, typeID);
        SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_DRAWER_TYPE, "5");


//        fragmentManager = getSupportFragmentManager();
//        fragmentManager.popBackStack("Meditation", FragmentManager.POP_BACK_STACK_INCLUSIVE);
//        // Add the new tab fragment
//        fragmentManager.beginTransaction()
//                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
//                .replace(R.id.fragment, FragmentMeditation.newInstance(0), String.valueOf(0))
//                .addToBackStack("Meditation")
//                // .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
//                .commit();
//        mDrawer.closeMenu();

        String toolName = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_MED_TOOLNAME, " Meditation");
        toolbarStatusBar(toolName);
        //cart_badge.setVisibility(View.GONE);
    }


    private void showCommunityDetails() {


        fragmentManager = getSupportFragmentManager();
        fragmentManager.popBackStack("DetailsMem", FragmentManager.POP_BACK_STACK_INCLUSIVE);
        // Add the new tab fragment
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .replace(R.id.fragment, FragmentCommunityDetailsMem.newInstance(0), String.valueOf(0))
                .addToBackStack("DetailsMem")
                // .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
        mDrawer.closeMenu();

        // toolbarStatusBar(" Details");

        cart_badge.setVisibility(View.GONE);

    }


    private void threads() {

        DashBoardActivity.this.runOnUiThread(new Runnable() {
            //
            @Override
            public void run() {

                // Stuff that updates the UI


            }
        }, 3000);
    }

    private void runOnUiThread(Runnable runnable, int i) {

        navigationDrawer();

    }

    public void openDrawerTab() {
        mDrawer.openMenu();
    }


    public void downloadVDOSerise(GetVideoResponseData getVideoResponseData) {

        if (getEventResponseDataResponQue != null)
            if (getEventResponseDataResponQue.size() > 0) {
                if (checkAvaiblity(getVideoResponseData)) {
                    //  Toast.makeText(getActivity(), "Saving Video Offline...", Toast.LENGTH_SHORT).show();
                    String file_Name = Uri.encode(getVideoResponseData.getFile());
                    Intent intent = new Intent(DashBoardActivity.this, DownloadService.class);
                    String URL = BASE_VIDEO_URL + Uri.encode(getVideoResponseData.getFile());

                    intent.putExtra("id", getVideoResponseData.getId() + "VDO"); // ADO Or VDO
                    intent.putExtra("File_Name", file_Name);

//            intent.putExtra("File_Name", file_Name.replace(" ", "%20"));
                    intent.putExtra("Type", "mp3"); // ADO Or VDO
                    intent.putExtra("URL", URL);

                    intent.putExtra("name", getVideoResponseData.getName());
                    intent.putExtra("Image", getVideoResponseData.getCover());
                    intent.putExtra("Image_URL", Config.BASE_VIDEO_URL_COVER + "" + Uri.encode(getVideoResponseData.getCover()));
                    intent.putExtra("Image", getVideoResponseData.getCover());

                    startService(intent);
                } else {

                    if (getEventResponseDataResponQue != null)
                        if (getEventResponseDataResponQue.size() > 0) {
                            getEventResponseDataResponQue.remove(0);
                        }
                    if (getEventResponseDataResponQue != null) {
                        if (getEventResponseDataResponQue.size() > 0) {
                            downloadVDOSerise(getEventResponseDataResponQue.get(0));
                        }
                    }


                }

            }


    }


    /// check user avalible in database or not


    public void getLoginData() {

        MyApplication myApplication = new MyApplication();


        if (Utility.getSharedInstance().isConnectedToInternet(this)) {

            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");

            // id="1";
            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");
//            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().getAllEvents( X_API_KEY,"10","0");
            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().checkUserAvaliableOrNot(X_API_KEY, id);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();

                    if (responseData != null) {
                        if (responseData.get("status").getAsString().equalsIgnoreCase("2")) {
                            // Config.getPlanDAO = new GetPlanDAO();
                            Toast.makeText(DashBoardActivity.this, "Admin removed you from the application ", Toast.LENGTH_LONG).show();
                            SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_IS_LOGIN, "0");
                            Intent intent = new Intent(DashBoardActivity.this, DashBoardActivity.class);
                            startActivity(intent);
                            overridePendingTransition(R.anim.left_anim, R.anim.right_anim);
                            finish();

                        } else {

                        }
                    }
                    // Log.e("Response::", responseData.toString());


                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {


                }


            });
        } else {

        }
    }


    public void autoPlay() {

        String trackID = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_TRACK_ID, "none");
        if (!trackID.equalsIgnoreCase("none"))
            //showDialog(trackID);
            mNxtSgPOs++;
        SongDetail nxtSongD = new SongDetail();


        try {


            nxtSongD = mNxtSongDetailsArraylist.get(mNxtSgPOs);
            DashBoardActivity.context.setStatus(true);
//            DashBoardActivity.context.setPlayer(false);
            DashBoardActivity.context.onResume();
            loadSongsDetails(nxtSongD);

            if (nxtSongD.getFile().contains(".mp3")) {
                if (!MediaController.getInstance().isPlayingAudio(nxtSongD) || MediaController.getInstance().isAudioPaused()) {
                    MediaController.getInstance().stopAudio();
                    MediaController.getInstance().playMUSIC(nxtSongD);


                    ForDOWNLOadSongDetail = nxtSongD;


                }
            }


        } catch (Exception e) {

            Toast.makeText(DashBoardActivity.this, "No More Songs In Queue", Toast.LENGTH_SHORT).show();


        }


    }


    public static void removeSlidePanel() {
        mLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
    }


    public void forgotPassword(String email) {

        User_DAO user_dao = new User_DAO();
        user_dao.setUser_id(SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, ""));

        user_dao.setPassword(email);

        MyApplication myApplication = new MyApplication();
        Utility.getSharedInstance().showProgressDialog(DashBoardActivity.this);

        if (Utility.getSharedInstance().isConnectedToInternet(DashBoardActivity.this)) {


            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().resetPass(clientService, X_API_KEY, user_dao);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    // JsonObject responseData = response.body();
                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());

                    Utility.getSharedInstance().dismissProgressDialog();
                    if (responseData != null) {
                        if (responseData.get("status").getAsInt() == 203) {

                            dialog.dismiss();
                            Toast.makeText(DashBoardActivity.this, "" + responseData.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                        } else
                            Toast.makeText(DashBoardActivity.this, "" + responseData.get("message").getAsString(), Toast.LENGTH_SHORT).show();

                    }


                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Utility.getSharedInstance().dismissProgressDialog();


                    Toast.makeText(DashBoardActivity.this, "Server error .. Please try again after some time", Toast.LENGTH_SHORT).show();
                }

            });
        } else {
            Toast.makeText(DashBoardActivity.this, "No network", Toast.LENGTH_SHORT).show();
        }
    }


}





