package com.music.nyasaa.front_end_layer.activities;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.music.nyasaa.R;
import com.music.nyasaa.adapter.GetSeaechCommunityAdapter;
import com.music.nyasaa.controller.MyApplication;
import com.music.nyasaa.models.Community_DAO;
import com.music.nyasaa.models.RequestModel.AddToCommunity.RequestToAddMember;
import com.music.nyasaa.models.RequestModel.SearchMedia.RequestSearchMedia;
import com.music.nyasaa.models.ResponseModel.getCommunity.GetSearchedCommunityData;
import com.music.nyasaa.models.ResponseModel.getCommunity.GetSerachedCommunity;
import com.music.nyasaa.models.SongDetail;
import com.music.nyasaa.utility.SharedPreferenceManager;
import com.music.nyasaa.utility.Utility;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.music.nyasaa.retrofit.Config.X_API_KEY;

public class SearchCommunityActivity extends AppCompatActivity {

    private RecyclerView mGridAlbumview;
    private Dialog mCommunityDialog;
    private EditText mEditSearch;
    private TextView mTxtWarn;
    private String userID;
    List<Community_DAO> community_dao;
    MyApplication myApplication;
    private RecyclerView mListCommunity;
    GetSeaechCommunityAdapter communityAdapter;
    public static List<GetSearchedCommunityData> mGetCommunityResponseData = new ArrayList<>();
    private int currentPage = 0;
    boolean isSearch = false;
    private SwipyRefreshLayout swipyRefreshLayoutDirection;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_community);
        initView();
        onClick();
    }

    private void initView() {


        TextView mTxtToolbaTxt = (TextView) findViewById(R.id.txtTitleToolbar);

        mTxtToolbaTxt.setText("Search Community");

        mTxtWarn = (TextView) findViewById(R.id.txtWarn);
        mEditSearch = (EditText) findViewById(R.id.editSearch);
        mTxtWarn.setVisibility(View.GONE);

        swipyRefreshLayoutDirection = (SwipyRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        swipyRefreshLayoutDirection.setDirection(SwipyRefreshLayoutDirection.BOTH);

        mListCommunity = (RecyclerView) findViewById(R.id.listViewMembers);
        mListCommunity.setLayoutManager(new GridLayoutManager(SearchCommunityActivity.this, 2));
        mListCommunity.setItemAnimator(new DefaultItemAnimator());

        userID = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");
//        CommParentID = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_IS_OWN_COMMUNITY, "none");


    }

    private void onClick() {

        LinearLayout imgBackLayout = (LinearLayout) findViewById(R.id.imgBackLayout);
        imgBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();

            }
        });

        currentPage = 0;

        mEditSearch.setText("");
        mEditSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                /*if (mEditSearch.getText().toString().trim().length() >= 3) {
                    getSearchCommunity(mEditSearch.getText().toString().trim());
                }*/
                isSearch = true;
                if (mEditSearch.getText().toString().length() == 0)
                    getSearchCommunity(mEditSearch.getText().toString().trim());
                else if (mEditSearch.getText().toString().length() == 1)
                    getSearchCommunity(mEditSearch.getText().toString().trim());
                else if (mEditSearch.getText().toString().length() > 0)
                    getSearchCommunity(mEditSearch.getText().toString().trim());
                else if (mEditSearch.getText().toString().length() > 1)
                    getSearchCommunity(mEditSearch.getText().toString().trim());
                else if (mEditSearch.getText().toString().length() > 2)
                    getSearchCommunity(mEditSearch.getText().toString().trim());
                else if (mEditSearch.getText().toString().length() > 3)
                    getSearchCommunity(mEditSearch.getText().toString().trim());
                else if (mEditSearch.getText().toString().length() > 4)
                    getSearchCommunity(mEditSearch.getText().toString().trim());
                else if (mEditSearch.getText().toString().length() > 5)
                    getSearchCommunity(mEditSearch.getText().toString().trim());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        currentPage = 0;
        firstTimeCallCommunity(0);


        swipyRefreshLayoutDirection.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                if (direction == SwipyRefreshLayoutDirection.TOP) {
//                    firstTimeCall(0);
                    if(isSearch)
                        getSearchCommunity(mEditSearch.getText().toString().trim());
                    else
                        firstTimeCallCommunity(0);
                    currentPage = 0;
                } else {
//                    firstTimeCall(currentPage);
                    if(isSearch)
                        getSearchCommunity(mEditSearch.getText().toString().trim());
                    else
                        firstTimeCallCommunity(currentPage);
                }
            }
        });
    }


    /////////////////////GET SEARCHD COMMUNITY//////////////////////////


    public void getSearchCommunity(String searchText) {

        if(searchText.equalsIgnoreCase("")){
            isSearch = false;
        }
        MyApplication myApplication = new MyApplication();

        RequestSearchMedia requestSearchMedia = new RequestSearchMedia();
        requestSearchMedia.setLimit("10000");
        requestSearchMedia.setOffset("0");
        requestSearchMedia.setSearchterm(searchText);

        if (Utility.getSharedInstance().isConnectedToInternet(SearchCommunityActivity.this)) {
            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().searchCommunity(X_API_KEY, requestSearchMedia);
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    JsonObject responseData = response.body();

                    if (responseData.get("error").getAsInt() == 0) {
                        Gson gson = new Gson();
                        Type listType = new TypeToken<List<SongDetail>>() {
                        }.getType();
                        if (responseData.get("respon") != null) {

                            JsonArray jsonArray = responseData.get("respon").getAsJsonArray();

                            if (jsonArray.size() > 0) {
                                mTxtWarn.setVisibility(View.GONE);
                                mListCommunity.setVisibility(View.VISIBLE);
                                swipyRefreshLayoutDirection.setRefreshing(false);
                                GetSerachedCommunity getCommunityResponse = gson.fromJson(responseData, GetSerachedCommunity.class);
                                List<GetSearchedCommunityData> getCommunityResponseData = getCommunityResponse.getRespon();
                                List<GetSearchedCommunityData> sortedCommunityList = new ArrayList<>();

                                for (int i = 0; i < getCommunityResponseData.size(); i++) {
                                    if (!userID.equalsIgnoreCase(String.valueOf(getCommunityResponseData.get(i).getParentUserId()))) {
                                        GetSearchedCommunityData getCommunityResponseData1 = new GetSearchedCommunityData();

                                        getCommunityResponseData1.setName(getCommunityResponseData.get(i).getName());
                                        getCommunityResponseData1.setCreatedAt(getCommunityResponseData.get(i).getCreatedAt());
                                        getCommunityResponseData1.setId(getCommunityResponseData.get(i).getId());
                                        getCommunityResponseData1.setImageUrl(getCommunityResponseData.get(i).getImageUrl());
                                        getCommunityResponseData1.setParentUserId(getCommunityResponseData.get(i).getParentUserId());
                                        getCommunityResponseData1.setLongInfo(getCommunityResponseData.get(i).getLongInfo());
                                        getCommunityResponseData1.setShortInfo(getCommunityResponseData.get(i).getShortInfo());

                                        sortedCommunityList.add(getCommunityResponseData1);
                                    }
                                }
                                communityAdapter = new GetSeaechCommunityAdapter(SearchCommunityActivity.this, sortedCommunityList);
                                mListCommunity.setAdapter(communityAdapter);

                            } else {
                                swipyRefreshLayoutDirection.setRefreshing(false);
                                mTxtWarn.setVisibility(View.VISIBLE);
                                mListCommunity.setVisibility(View.GONE);
                            }

                        }

                    } else {
                        swipyRefreshLayoutDirection.setRefreshing(false);
                        mTxtWarn.setVisibility(View.VISIBLE);
                        mListCommunity.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    swipyRefreshLayoutDirection.setRefreshing(false);
                    mTxtWarn.setVisibility(View.VISIBLE);
                    mListCommunity.setVisibility(View.GONE);
                }
            });
        } else {
            swipyRefreshLayoutDirection.setRefreshing(false);
            Toast.makeText(SearchCommunityActivity.this, "No internet connection available", Toast.LENGTH_SHORT).show();
        }
    }

    public void firstTimeCallCommunity(final int currentPage) {
        isSearch = false;
        MyApplication myApplication = new MyApplication();

        RequestSearchMedia requestSearchMedia = new RequestSearchMedia();
        requestSearchMedia.setLimit("10000");
        requestSearchMedia.setOffset(String.valueOf(currentPage));
        requestSearchMedia.setSearchterm("");

        if (Utility.getSharedInstance().isConnectedToInternet(SearchCommunityActivity.this)) {
            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().searchCommunity(X_API_KEY, requestSearchMedia);
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    JsonObject responseData = response.body();

                    if (responseData.get("error").getAsInt() == 0) {
                        Gson gson = new Gson();
                        Type listType = new TypeToken<List<SongDetail>>() {
                        }.getType();
                        if (responseData.get("respon") != null) {

                            JsonArray jsonArray = responseData.get("respon").getAsJsonArray();

                            if (jsonArray.size() > 0) {
                                mTxtWarn.setVisibility(View.GONE);
                                mListCommunity.setVisibility(View.VISIBLE);
                                if (currentPage == 0) {
                                    GetSerachedCommunity getCommunityResponse = gson.fromJson(responseData, GetSerachedCommunity.class);
                                    mGetCommunityResponseData = getCommunityResponse.getRespon();
                                    List<GetSearchedCommunityData> sortedCommunityList = new ArrayList<>();
                                    for (int i = 0; i < mGetCommunityResponseData.size(); i++) {
                                        if (!userID.equalsIgnoreCase(String.valueOf(mGetCommunityResponseData.get(i).getParentUserId()))) {
                                            GetSearchedCommunityData mGetCommunityResponseData1 = new GetSearchedCommunityData();

                                            mGetCommunityResponseData1.setName(mGetCommunityResponseData.get(i).getName());
                                            mGetCommunityResponseData1.setCreatedAt(mGetCommunityResponseData.get(i).getCreatedAt());
                                            mGetCommunityResponseData1.setId(mGetCommunityResponseData.get(i).getId());
                                            mGetCommunityResponseData1.setImageUrl(mGetCommunityResponseData.get(i).getImageUrl());
                                            mGetCommunityResponseData1.setParentUserId(mGetCommunityResponseData.get(i).getParentUserId());
                                            mGetCommunityResponseData1.setLongInfo(mGetCommunityResponseData.get(i).getLongInfo());
                                            mGetCommunityResponseData1.setShortInfo(mGetCommunityResponseData.get(i).getShortInfo());

                                            sortedCommunityList.add(mGetCommunityResponseData1);
                                        }
                                    }
                                    swipyRefreshLayoutDirection.setRefreshing(false);
                                    mGetCommunityResponseData = sortedCommunityList;
                                    getList(mGetCommunityResponseData);
                                } else {
                                    GetSerachedCommunity getCommunityResponse = gson.fromJson(responseData, GetSerachedCommunity.class);
                                    List<GetSearchedCommunityData> datumList = getCommunityResponse.getRespon();
                                    List<GetSearchedCommunityData> sortedCommunityList = new ArrayList<>();
                                    for (int i = 0; i < mGetCommunityResponseData.size(); i++) {
                                        if (!userID.equalsIgnoreCase(String.valueOf(mGetCommunityResponseData.get(i).getParentUserId()))) {
                                            GetSearchedCommunityData mGetCommunityResponseData1 = new GetSearchedCommunityData();

                                            mGetCommunityResponseData1.setName(mGetCommunityResponseData.get(i).getName());
                                            mGetCommunityResponseData1.setCreatedAt(mGetCommunityResponseData.get(i).getCreatedAt());
                                            mGetCommunityResponseData1.setId(mGetCommunityResponseData.get(i).getId());
                                            mGetCommunityResponseData1.setImageUrl(mGetCommunityResponseData.get(i).getImageUrl());
                                            mGetCommunityResponseData1.setParentUserId(mGetCommunityResponseData.get(i).getParentUserId());
                                            mGetCommunityResponseData1.setLongInfo(mGetCommunityResponseData.get(i).getLongInfo());
                                            mGetCommunityResponseData1.setShortInfo(mGetCommunityResponseData.get(i).getShortInfo());

                                            sortedCommunityList.add(mGetCommunityResponseData1);
                                        }
                                    }
                                    swipyRefreshLayoutDirection.setRefreshing(false);
                                    mGetCommunityResponseData = sortedCommunityList;
                                    mGetCommunityResponseData.addAll(mGetCommunityResponseData.size(), datumList);
                                    getUpdatedList();
                                }

                                if(mGetCommunityResponseData!=null) {
                                    if(mGetCommunityResponseData.size()<0) {
                                        swipyRefreshLayoutDirection.setRefreshing(false);
                                        mTxtWarn.setVisibility(View.VISIBLE);
                                        mListCommunity.setVisibility(View.GONE);
                                    }
                                }

                                /*GetSerachedCommunity getCommunityResponse = gson.fromJson(responseData, GetSerachedCommunity.class);
                                List<GetSearchedCommunityData> getCommunityResponseData = getCommunityResponse.getRespon();
                                List<GetSearchedCommunityData> sortedCommunityList = new ArrayList<>();

                                for (int i = 0; i < getCommunityResponseData.size(); i++) {
                                    if (!userID.equalsIgnoreCase(String.valueOf(getCommunityResponseData.get(i).getParentUserId()))) {
                                        GetSearchedCommunityData getCommunityResponseData1 = new GetSearchedCommunityData();

                                        getCommunityResponseData1.setName(getCommunityResponseData.get(i).getName());
                                        getCommunityResponseData1.setCreatedAt(getCommunityResponseData.get(i).getCreatedAt());
                                        getCommunityResponseData1.setId(getCommunityResponseData.get(i).getId());
                                        getCommunityResponseData1.setImageUrl(getCommunityResponseData.get(i).getImageUrl());
                                        getCommunityResponseData1.setParentUserId(getCommunityResponseData.get(i).getParentUserId());
                                        getCommunityResponseData1.setLongInfo(getCommunityResponseData.get(i).getLongInfo());
                                        getCommunityResponseData1.setShortInfo(getCommunityResponseData.get(i).getShortInfo());

                                        sortedCommunityList.add(getCommunityResponseData1);
                                    }
                                }
                                communityAdapter = new GetSeaechCommunityAdapter(SearchCommunityActivity.this, sortedCommunityList);
                                mListCommunity.setAdapter(communityAdapter);*/

                            } else {
                                swipyRefreshLayoutDirection.setRefreshing(false);
                                if (mGetCommunityResponseData != null && mGetCommunityResponseData.size() > 0 ) {
                                    mTxtWarn.setVisibility(View.GONE);
                                    mListCommunity.setVisibility(View.VISIBLE);
                                    Toast.makeText(SearchCommunityActivity.this, "No more communities", Toast.LENGTH_SHORT).show();
                                } else {
                                    mTxtWarn.setVisibility(View.VISIBLE);
                                    mListCommunity.setVisibility(View.GONE);
                                }
                            }

                        }

                    } else {
                        swipyRefreshLayoutDirection.setRefreshing(false);
                        mTxtWarn.setVisibility(View.VISIBLE);
                        mListCommunity.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    swipyRefreshLayoutDirection.setRefreshing(false);
                    mTxtWarn.setVisibility(View.VISIBLE);
                    mListCommunity.setVisibility(View.GONE);
                }
            });
        } else {
            swipyRefreshLayoutDirection.setRefreshing(false);
            Toast.makeText(SearchCommunityActivity.this, "No internet connection available", Toast.LENGTH_SHORT).show();
        }
    }

    private void getList(List<GetSearchedCommunityData> getSearchedCommunityDataList) {
        communityAdapter = new GetSeaechCommunityAdapter(SearchCommunityActivity.this, getSearchedCommunityDataList);
        mListCommunity.setAdapter(communityAdapter);

        if (communityAdapter != null)
            communityAdapter.notifyDataSetChanged();
        currentPage += 10;
    }

    private void getUpdatedList() {
        if (communityAdapter != null)
            communityAdapter.notifyDataSetChanged();
        currentPage += 10;
    }

    public void showMembersDialog(final String parentID, final String commID, final String sName) {

        mCommunityDialog = new Dialog(SearchCommunityActivity.this);
        mCommunityDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mCommunityDialog.setContentView(R.layout.dialog_confrim_delete);
        mCommunityDialog.getWindow().getAttributes().width = ActionBar.LayoutParams.FILL_PARENT;
        mCommunityDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        mCommunityDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mCommunityDialog.show();

        Button mButYes = (Button) mCommunityDialog.findViewById(R.id.butYes);
        Button mButNo = (Button) mCommunityDialog.findViewById(R.id.butNo);

        TextView mTxtMsg = (TextView) mCommunityDialog.findViewById(R.id.txtMsg);
        mTxtMsg.setText("Are you sure you want to join this " + sName + " ?");


        TextView mTxtTitle = (TextView) mCommunityDialog.findViewById(R.id.txtDialogTitle);
        mTxtTitle.setText("Join Community");


        mButNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCommunityDialog.dismiss();
            }
        });

        mButYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addMemberToCommunity(parentID, commID);
            }
        });
    }

    ////// Add Members

    public void addMemberToCommunity(String parentID, String commId) {

        MyApplication myApplication = new MyApplication();

        if (Utility.getSharedInstance().isConnectedToInternet(this)) {
            String id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"none");
            Utility.getSharedInstance().showProgressDialog(this);
            RequestToAddMember requestToSearchUser = new RequestToAddMember();
            requestToSearchUser.setRequest("2");
            requestToSearchUser.setCommunityId(commId);
            requestToSearchUser.setParentUserId(parentID);
            requestToSearchUser.setUserId(id);


            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().addMemberToComm(X_API_KEY, requestToSearchUser);
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    JsonObject responseData = response.body();

                    if (responseData != null) {

                        if (responseData.get("status").getAsString().equalsIgnoreCase("true")) {
                            mCommunityDialog.dismiss();
                            Utility.getSharedInstance().dismissProgressDialog();
                            Toast.makeText(SearchCommunityActivity.this, "" + responseData.get("message").getAsString(), Toast.LENGTH_SHORT).show();

                        } else {
                            Utility.getSharedInstance().dismissProgressDialog();
                            Toast.makeText(SearchCommunityActivity.this, "" + responseData.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Utility.getSharedInstance().dismissProgressDialog();
                        Toast.makeText(SearchCommunityActivity.this, "Api issue", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Utility.getSharedInstance().dismissProgressDialog();
                    Toast.makeText(SearchCommunityActivity.this, "Api failure", Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(SearchCommunityActivity.this, "No internet connection available", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.left_anim, R.anim.right_anim);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //getSearchCommunity("");
//        firstTimeCallCommunity(0);
    }
}
