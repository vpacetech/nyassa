package com.music.nyasaa.front_end_layer.activities.splash_screen;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.music.nyasaa.R;

import com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity;
import com.music.nyasaa.front_end_layer.activities.login.LoginActivity;
import com.music.nyasaa.utility.SharedPreferenceManager;
import com.music.nyasaa.utility.TbsindUtil;

import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.HashMap;


public class SplashScreenActivity extends AppCompatActivity {
    public static final int MY_PERMISSIONS = 127;

    private MediaPlayer player;
    private String currentVersion = "";
    private Dialog dialog;
    private boolean isUpdate=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_splash_screen);
       // showGif();
      //  getAllLanguages();
        doRegularProcess();
//        if (Build.VERSION.SDK_INT >= 23) {
//            takePermissionsForMarsh();
//        } else {
//
//        }
    //    forceUpdate();
    }



    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void takePermissionsForMarsh() {


        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED

                || ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_NETWORK_STATE)
                != PackageManager.PERMISSION_GRANTED

                || ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED

                || ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.ACCESS_NETWORK_STATE
                            , Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.VIBRATE,
                            Manifest.permission.RECEIVE_BOOT_COMPLETED},
                    MY_PERMISSIONS);


        } else {
            doRegularProcess();
        }

    }

    private void doRegularProcess() {
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {


                if(isUpdate==false)
                {
                    String login = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_IS_LOGIN, "");
                    if(login.equals("1"))
                    {
                        Intent intent = new Intent(SplashScreenActivity.this,
                                DashBoardActivity.class) ;
//                        Intent intent = new Intent(SplashScreenActivity.this,
//                                DashBoardActivity.class);
                        startActivity(intent);
                    }
                    else
                    {
                        Intent intent = new Intent(SplashScreenActivity.this,
                                LoginActivity.class);
                        startActivity(intent);
                    }
                    overridePendingTransition(R.anim.left, R.anim.right);
                    finish();
                }


                //overridePendingTransition(R.anim.left, R.anim.right);


            }
        }, 2000);


    }


    private void showGif() {


    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    doRegularProcess();

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    public void insertAllSongsLocal() {
        String SERVER_URL = TbsindUtil.SERVER_URL_GET_ALL_SONGS;
        HashMap<String, String> params = new HashMap<>();
        AndroidNetworking.post(TbsindUtil.SERVER_URL_GET_ALL_SONGS).setTag("local").setPriority(Priority.HIGH).build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
//        CustomRequest request=new CustomRequest(Request.Method.POST, SERVER_URL, params, new Response.Listener<JSONObject>() {
//            @Override
//            public void onResponse(JSONObject json) {
//                try{
//                    if (json.getString("success").equals("1")){
//
//                    }
//
//                }catch (Exception e){
//                    e.printStackTrace();
//                }
//
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//
//            }
//        });
//        MyApplication.getInstance().addToRequestQueue(request);
    }

    public void forceUpdate() {
        PackageManager packageManager = getPackageManager();
        PackageInfo packageInfo = null;
        try {

            packageInfo = packageManager.getPackageInfo(getPackageName(), 0);
            currentVersion = packageInfo.versionName;
            new GetVersionCode().execute();
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

    }

    private class GetVersionCode extends AsyncTask<Void, String, String> {

        @Override
        protected String doInBackground(Void... voids) {

            String newVersion = null;

            try {
                Document document = Jsoup.connect("https://play.google.com/store/apps/details?id=" + SplashScreenActivity.this.getPackageName() + "&hl=en")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get();
                if (document != null) {
                    Elements element = document.getElementsContainingOwnText("Current Version");
                    for (Element ele : element) {
                        if (ele.siblingElements() != null) {
                            Elements sibElemets = ele.siblingElements();
                            for (Element sibElemet : sibElemets) {
                                newVersion = sibElemet.text();
                            }
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return newVersion;

        }

        @Override
        protected void onPostExecute(String onlineVersion) {

            super.onPostExecute(onlineVersion);

            if (onlineVersion != null && !onlineVersion.isEmpty()) {

                /*if (Float.valueOf(currentVersion) < Float.valueOf(onlineVersion)) {
                    //show anything
                    showForceUpdateDialog();
                }*/
                try {
                    if (compareVersion(onlineVersion, currentVersion) == 1) {
                        showForceUpdateDialog(onlineVersion);
                        isUpdate =true;
                    }
                    else {
                        isUpdate =false;
                    }
                }
                catch (Exception e)
                {

                }

            }

            Log.d("update", "Current version " + currentVersion + "playstore version " + onlineVersion);

        }
    }

    public int compareVersion(String version1, String version2) {
        String[] arr1 = version1.split("\\.");
        String[] arr2 = version2.split("\\.");

        int i = 0;
        while (i < arr1.length || i < arr2.length) {
            if (i < arr1.length && i < arr2.length) {
                if (Integer.parseInt(arr1[i]) < Integer.parseInt(arr2[i])) {
                    return -1;
                } else if (Integer.parseInt(arr1[i]) > Integer.parseInt(arr2[i])) {
                    return 1;
                }
            } else if (i < arr1.length) {
                if (Integer.parseInt(arr1[i]) != 0) {
                    return 1;
                }
            } else if (i < arr2.length) {
                if (Integer.parseInt(arr2[i]) != 0) {
                    return -1;
                }
            }

            i++;
        }

        return 0;
    }

    public void showForceUpdateDialog(String newVersion){
        dialog = new Dialog(SplashScreenActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_version_update);
        dialog.getWindow().getAttributes().width = ActionBar.LayoutParams.FILL_PARENT;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.setCancelable(false);

        final TextView txtDialogMessage=dialog.findViewById(R.id.txtDialogMessage);

        txtDialogMessage.setText(" Wow!! Latest Bhajan/Kirtan , Evening Satsang And Many more latest music UPDATE NOW ");

        Button mButYes = (Button) dialog.findViewById(R.id.butYes);
        Button mButNo = (Button) dialog.findViewById(R.id.butNo);

        mButYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + "com.music.nyasaa")));
                dialog.cancel();
            }
        });


        mButNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String login = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_IS_LOGIN, "");
                if(login.equals("0"))
                {

//                    Intent intent = new Intent(SplashScreenActivity.this,
//                            DashBoardActivity.class);

                    Intent intent = new Intent(SplashScreenActivity.this,
                            DashBoardActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.left, R.anim.right);
                    finish();
                }
                else
                {
                    Intent intent = new Intent(SplashScreenActivity.this,
                            LoginActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.left, R.anim.right);
                    finish();
                }

            }
        });
    }


}
