package com.music.nyasaa.front_end_layer.activities.download_in_appscope_enc;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


public class DataSQLHelper extends SQLiteOpenHelper implements Table_Details {
    private static final String DATABASE_NAME = "Track.db";
    private static final int DATABASE_VERSION = 1;    //20/06/2017
    private Context con;

//    public  static  String Type = "Type";
//    public  static  String URL = "URL";
//    public  static  String Image = "Image";


    public DataSQLHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        con = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

       // PRIMARY KEY (column1, column2)
        db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" + Type + " TEXT NOT NULL, "+URL+" TEXT NOT NULL,"+Image+" TEXT NOT NULL, "+File_Name + "  TEXT NOT NULL  ,"+name_c + "  TEXT NOT NULL  ,"+Image_url + "  TEXT NOT NULL  ,"+id+" TEXT PRIMARY KEY , " + JSON_COL + " TEXT,"+ Album_name + "  TEXT NOT NULL,"+ Cover_image + "  TEXT);");
        System.out.println("table created...");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion >= newVersion)
            return;
        if (newVersion > oldVersion) {
            System.out.println("column added");
        }

        onCreate(db);
    }

    public boolean rowsValueExist(SQLiteDatabase db, String table_name) {
        Cursor cursor = db
                .query(table_name, null, null, null, null, null, null);
        // System.out.println("Count " + cursor.getCount());
        if (cursor.getCount() < 1) {
            cursor.close();

            return false;
        }
        cursor.close();

        return true;
    }

    public long insert_rows(SQLiteDatabase db, String table_name,
                            ContentValues values) {
        // inset into table_name(col1,col2,....)
        // values (val1,val2,....);
        long n = db.insert(table_name, null, values);
        System.out.println("data inserted....");
        return n;
    }

    public Cursor getvalue_By_Where(SQLiteDatabase db, String table_name,
                                    String select_clause_colunms, String where_clause_column,
                                    String where_clause_vals) {

        Cursor cursor = db.query(table_name,
                new String[]{select_clause_colunms}, where_clause_column,
                new String[]{where_clause_vals}, null, null, null);
        return cursor;
    }

    public Cursor getvalue_By_columsSort(SQLiteDatabase db, String table_name,
                                         String colume, String oder) {
//        Cursor cursor = db
//                .query(table_name, null, null, null, null, null, null);
        Cursor cursor = db.query(table_name, null, null, null, null, null, colume + " " + oder);
        // System.out.println("Count " + cursor.getCount());

        return cursor;
    }

    public Cursor getvalue_By_colums(SQLiteDatabase db, String table_name,
                                     String select_clause_colunms, String where_clause_column) {
        Cursor cursor = db
                .query(table_name, null, null, null, null, null, null);

        // System.out.println("Count " + cur0sor.getCount());

        return cursor;
    }

    public Cursor getvalueWithQuery(SQLiteDatabase db,
                                    String query) {

        //String query ="SELECT DISTINCT category FROM event";
        Cursor cursor = db.rawQuery(query, null);

        return cursor;
    }

    public int delete_Row_where(SQLiteDatabase db, String table_name,
                                String col_names, String col_val) {
        // delete from table_name where col_name=val;
        // db.delete(table_name, col_name + "=?", new String[] { col_val });
        String q = "delete from " + table_name + " where " + col_names + "="
                +"'" +col_val+"'";
        System.out.println("Query " + q);

        db.execSQL(q);

        return 0;// db.delete(table_name, col_names, new String[] { col_val });

    }

    public int update_colum_where(SQLiteDatabase db, String table_name,
                                  String col_names, String col_val) {
        // delete from table_name where col_name=val;
        // db.delete(table_name, col_name + "=?", new String[] { col_val });
        String q = "update " + table_name + " SET " + " where " + col_names + "="
                +"\"" +col_val+"\"";
        System.out.println("Query " + q);

       q = "UPDATE "+TABLE_NAME +" SET " + "ready_To_Communicate"+ " = '"+"true"+"' WHERE "+col_names+ " = "+"'"+col_val+"'";

       // "
        db.execSQL(q);

        return 0;// db.delete(table_name, col_names, new String[] { col_val });

    }


    public int delete_Row_where(SQLiteDatabase db, String table_name) {
        // delete from table_name where col_name=val;
        // db.delete(table_name, col_name + "=?", new String[] { col_val });
        String q = "delete from " + table_name;
        System.out.println("Query " + q);

        db.execSQL(q);

        return 0;// db.delete(table_name, col_names, new String[] { col_val });

    }

    public void update_row(SQLiteDatabase db, String table_name,
                           String set_columename, String set_colume_value, String whereClause,
                           String whereArgs) {
        // update table_name
        // set col_name1="val_1",col_name2="val_2",....
        // where some_col_name="some_val";
        // db.update(table_name, values, whereClause, null);
        String q = "UPDATE " + table_name + " SET " + set_columename + "='"
                + set_colume_value + "' where " + whereClause + "=" + whereArgs;
        System.out.println("Query " + q);
        db.execSQL(q);
        // / db.update(table_name, values, whereClause, new String[] { whereArgs
        // });
    }

    public void add_new_column_in_Table(SQLiteDatabase db, String table_name,
                                        String column_name, String data_type) {
        // alter table table_name add column colum_name data_type
        db.execSQL("ALTER TABLE " + table_name + " ADD COLUMN " + column_name
                + data_type);
    }

    public long update_rows(SQLiteDatabase db, String table_name,
                            ContentValues values, String whereClause,
                            String whereArgs) {
        // inset into table_name(col1,col2,....)
        // values (val1,val2,....);
        System.out.println("Update values.size() :" + values.size());
        long n = db.update(table_name, values, whereClause, new String[]{whereArgs});
        System.out.println("Update Inserted :" + n);
        return n;
    }

    public void resetDB(SQLiteDatabase db) {


    }

    public void close_db(SQLiteDatabase db) {
        db.close();
    }
}
