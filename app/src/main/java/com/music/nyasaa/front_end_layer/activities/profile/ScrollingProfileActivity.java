package com.music.nyasaa.front_end_layer.activities.profile;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;

import com.music.nyasaa.R;
import com.music.nyasaa.adapter.ProfilePostAdapter;

public class ScrollingProfileActivity extends AppCompatActivity implements View.OnClickListener {


    private String name[] = {"Dimpu Singh", "Rahul Mishra"};

    private String collegeName[] = {"Bollywood Song", "Telugu Song"};

    private RecyclerView mListPost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        initView();
        onClick();


    }

    private void initView() {


        mListPost = (RecyclerView) findViewById(R.id.recyclerview_post);
        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(ScrollingProfileActivity.this);
        mListPost.setLayoutManager(linearLayoutManager);
        mListPost.setItemAnimator(new DefaultItemAnimator());


        ProfilePostAdapter profilePostAdapter = new ProfilePostAdapter(this, name);
        mListPost.setAdapter(profilePostAdapter);
    }

    private void onClick() {
        LinearLayout imgBackLayout = (LinearLayout) findViewById(R.id.imgBackLayout);

        imgBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();

            }
        });
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.left_anim, R.anim.right_anim);
    }
}
