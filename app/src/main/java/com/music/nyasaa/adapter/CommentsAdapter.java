package com.music.nyasaa.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.music.nyasaa.R;
import com.music.nyasaa.models.ResponseModel.GetComments.GetCommentsModelData;
import com.music.nyasaa.retrofit.Config;
import com.music.nyasaa.utility.SharedPreferenceManager;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.HeaderHolder> {

    LayoutInflater inflater;
    boolean header = false;

    Context context;
    private String[] name;
    private Integer[] img;
    private int lastPosition = -1;

    private List<GetCommentsModelData> getAllCommentsArrayListData;


    public CommentsAdapter(Context context, ArrayList<GetCommentsModelData> getMyPlayListResponseData) {

        this.context = context;
        this.getAllCommentsArrayListData = getMyPlayListResponseData;


    }


    @Override
    public CommentsAdapter.HeaderHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_items_chat, parent, false);

        return new CommentsAdapter.HeaderHolder(itemView);


        // return new HeaderHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.chapter_item, null));
    }

    @Override
    public void onBindViewHolder(CommentsAdapter.HeaderHolder holder, int position) {



        String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");

        if(id.equalsIgnoreCase(getAllCommentsArrayListData.get(position).getUserId()))
        {

            holder.layRecevier.setVisibility(View.GONE);
            holder.laySender.setVisibility(View.VISIBLE);
//            holder.mTxtsenderDate.setText(getAllCommentsArrayListData.get(position).getUsername());
            holder.mTxtSenderMessage.setText(getAllCommentsArrayListData.get(position).getComment());

        }
        else {

            Picasso.with(context)
                    .load(Config.BASE_PROFILE_PIC + Uri.encode(getAllCommentsArrayListData.get(position).getProfile_photo()))
                    .placeholder(R.drawable.default_music)
                    .error(R.drawable.default_music)
                    .into(holder.mRececierImage);


            holder.layRecevier.setVisibility(View.VISIBLE);
            holder.laySender.setVisibility(View.GONE);

            holder.mTxtRecevierName.setText(getAllCommentsArrayListData.get(position).getUsername());
            holder.mTxtReceicerMessage.setText(getAllCommentsArrayListData.get(position).getComment());

        }



//        if (position > lastPosition) {
//
//            Animation animation = AnimationUtils.loadAnimation(context,
//                    R.anim.list_items);
//            holder.itemView.startAnimation(animation);
//            lastPosition = position;
//        }


    }

    @Override
    public int getItemCount() {
        return getAllCommentsArrayListData.size();
    }

    public void removeItem(int position) {
        getAllCommentsArrayListData.remove(position);
        // notify the item removed by position
        // to perform recycler view delete animations
        // NOTE: don't call notifyDataSetChanged()
        notifyItemRemoved(position);
    }



    public class HeaderHolder extends RecyclerView.ViewHolder {
        private TextView mTxtSenderName,mTxtRecevierName,mTxtsenderDate,mTxtRecevierDate,mTxtSenderMessage,mTxtReceicerMessage,mtxtDes;
        public RelativeLayout layoutPlayList,laySender;
        private ImageView imgWallaper,mRececierImage;
        private LinearLayout layRecevier;


        public HeaderHolder(View itemView) {
            super(itemView);



            // recevier
            mTxtReceicerMessage = (TextView) itemView.findViewById(R.id.text_message_body);
            mTxtRecevierDate = (TextView) itemView.findViewById(R.id.text_message_time);
            mTxtRecevierName = (TextView) itemView.findViewById(R.id.text_message_name);
            mRececierImage = (ImageView) itemView.findViewById(R.id.image_message_profile);

            // semder

            mTxtSenderMessage = (TextView) itemView.findViewById(R.id.text_message_bodySender);
            mTxtsenderDate = (TextView) itemView.findViewById(R.id.text_message_timeSender);
//            mTxtSenderName = (TextView) itemView.findViewById(R.id.text);


            laySender=(RelativeLayout)itemView.findViewById(R.id.laySender);
            layRecevier=(LinearLayout) itemView.findViewById(R.id.layRecevier);


        }


    }


}



