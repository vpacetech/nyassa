package com.music.nyasaa.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.music.nyasaa.R;
import com.music.nyasaa.controller.MyApplication;
import com.music.nyasaa.fragments.meditation.FragmentMeditation;
import com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity;
import com.music.nyasaa.manager.MediaController;
import com.music.nyasaa.models.RequestModel.GetMyPlayList.RequestGetMyPlayList;
import com.music.nyasaa.models.RequestModel.SaveToPlayList.RequestSaveToPlayList;
import com.music.nyasaa.models.ResponseModel.GetMyPlayList.GetMyPlayListResponse;
import com.music.nyasaa.models.ResponseModel.GetMyPlayList.GetMyPlayListResponseData;
import com.music.nyasaa.models.SongDetail;
import com.music.nyasaa.retrofit.Config;
import com.music.nyasaa.utility.RecyclerItemClickListener;
import com.music.nyasaa.utility.SharedPreferenceManager;
import com.music.nyasaa.utility.Utility;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity.ForDOWNLOadSongDetail;
import static com.music.nyasaa.retrofit.Config.X_API_KEY;

public class MediatationMoreAdapter extends RecyclerView.Adapter<MediatationMoreAdapter.ViewHolder> {
    private Context context;
    private ArrayList<SongDetail> mSongsList;
    private DisplayImageOptions options;
    private int lastPosition = -1;

    private Dialog myPlayListDialog;
    private RecyclerView mMyPlayList;
    private TextView mTxtWarn;
    private SwipyRefreshLayout swipyRefreshLayout;
    private List<GetMyPlayListResponseData> getMyPlayListResponseData;
    private MyPlayListAdapters myPlayListAdapter;
    private FragmentMeditation fragmentMeditation;


    RotateAnimation rotate = new RotateAnimation(
            0, 360,
            Animation.RELATIVE_TO_SELF, 0.5f,
            Animation.RELATIVE_TO_SELF, 0.5f
    );


    public MediatationMoreAdapter(FragmentActivity activity, ArrayList<SongDetail> mSongss, FragmentMeditation fragmentMusic) {
        this.context = activity;
        this.mSongsList = mSongss;
        this.fragmentMeditation = fragmentMusic;

    }

    public MediatationMoreAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MediatationMoreAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_playlist, parent, false));
    }

    public void onBindViewHolder(final MediatationMoreAdapter.ViewHolder holder, final int position) {

        SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_MUSIC_MODE, "meditation");
        final SongDetail mDetail = mSongsList.get(position);


        if (mSongsList.get(position).getPlayStop()) {

            holder.btn_song_play.setImageResource(R.drawable.ic_pause);
        } else {
            holder.btn_song_play.setImageResource(R.drawable.play);
        }

        holder.btnDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ForDOWNLOadSongDetail = mSongsList.get(position);
                DashBoardActivity.context.downloadfromOtherFragment();

            }
        });
        holder.songTitle.setText(mDetail.getName());
        holder.mTxtTypeSong.setText(mDetail.getFile());

        Picasso.with(context)
                .load(Config.PIC_URL + "cover/" + Uri.encode(  mDetail.getCover()))
                .placeholder(R.drawable.default_music)
                .error(R.drawable.default_music)
                .into(holder.cover);

        holder.btn_song_play.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // holder.loadingSong.setVisibility(View.VISIBLE);


                fragmentMeditation.playPoase(position);

                if (mSongsList.get(position).getPlayStop()) {
                    DashBoardActivity.context. setStatus(true);
                    DashBoardActivity.context.setPlayer(false);
                    DashBoardActivity.context.onResume();
                    SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_TRACK_TYPE,"2");
                    SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_TRACK_ID,String.valueOf(mDetail.getId()));
                    SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_MUSIC_MODE, "meditation");
                    DashBoardActivity.context.loadSongsDetails(mDetail);
                    DashBoardActivity.context.loadSongsDetails(mSongsList,position);
//
//                }
                    //new DownloadMp3CacheTask().execute(mDetail.getUrl(),mDetail.display_name);
                    if (!MediaController.getInstance().isPlayingAudio(mDetail) || MediaController.getInstance().isAudioPaused()) {
                        MediaController.getInstance().stopAudio();
                        MediaController.getInstance().playMUSIC(mDetail);

                        //  holder.loadingSong.setVisibility(View.GONE);


//                    rotate.setDuration(900);
//                    rotate.setRepeatCount(Animation.INFINITE);
//                    itemImage.startAnimation(rotate);
                        // MediaController.getInstance().setPlaylist(mSongsList, mDetail, PhoneMediaControl.SonLoadFor.MostPlay.ordinal(), -1);
                    } else {
                        MediaController.getInstance().pauseAudio(mDetail);
                        DashBoardActivity.stop_rotateImage();
                    }
                    /*if (MusicSQLiteHelper.isExistQueueSong(mDetail)) {
                        MusicSQLiteHelper.updateQueue(mDetail);
                    } else {
                        MusicSQLiteHelper.insertSongInQueue(mDetail);
                    }*/
                } else {
                    DashBoardActivity.context.setPlayer(true);
                    DashBoardActivity.context. setStatus(false);
                    if (!MediaController.getInstance().isPlayingAudio(mDetail) || MediaController.getInstance().isAudioPaused()) {
                        MediaController.getInstance().stopAudio();
                        MediaController.getInstance().pauseAudio(mDetail);
//                        DashBoardActivity.stop_rotateImage();
                        // MediaController.getInstance().playAudio(mDetail);
                        //  holder.loadingSong.setVisibility(View.GONE);


//                    rotate.setDuration(900);
//                    rotate.setRepeatCount(Animation.INFINITE);
//                    itemImage.startAnimation(rotate);
                        // MediaController.getInstance().setPlaylist(mSongsList, mDetail, PhoneMediaControl.SonLoadFor.MostPlay.ordinal(), -1);
                    } else {
                        MediaController.getInstance().pauseAudio(mDetail);
                        DashBoardActivity.stop_rotateImage();
                    }
                }
            }
        });


        holder.mLinearLaypout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showDialog(String.valueOf(mDetail.getId()));

            }
        });


        holder.btn_song_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_TRACK_TYPE,"2");
                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_TRACK_ID,String.valueOf(mDetail.getId()));

                DashBoardActivity dash = new DashBoardActivity();
                dash.showCommDialog(context);
            }
        });





    }

    public int getItemCount() {
        return mSongsList.size();
    }

    public class ViewHolder extends android.support.v7.widget.RecyclerView.ViewHolder {
        ImageView btn_song_play, btnDownload;
        ImageView cover;
        TextView songTitle,mTxtTypeSong;
        ProgressBar loadingSong;
        private LinearLayout mLinearLaypout,btn_song_share;

        public ViewHolder(View itemView) {
            super(itemView);
            btn_song_share = (LinearLayout) itemView.findViewById(R.id.btn_share);
            songTitle = (TextView) itemView.findViewById(R.id.txtPlayListName);
            mTxtTypeSong = (TextView) itemView.findViewById(R.id.txtTypeSOng);
            btn_song_play = (ImageView) itemView.findViewById(R.id.btn_play);
            btnDownload = (ImageView) itemView.findViewById(R.id.btnDownload);
            cover = (ImageView) itemView.findViewById(R.id.playlsitImage);
            loadingSong = (ProgressBar) itemView.findViewById(R.id.loadingSong);

            mLinearLaypout = (LinearLayout) itemView.findViewById(R.id.playListLayout);
        }
    }


    public void showDialog(final String songID)

    {
        myPlayListDialog = new Dialog(fragmentMeditation.getContext());
        myPlayListDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myPlayListDialog.setContentView(R.layout.dialog_add_songto_playlist);
        myPlayListDialog.getWindow().getAttributes().width = ActionBar.LayoutParams.FILL_PARENT;
        myPlayListDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        myPlayListDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        mTxtWarn = (TextView) myPlayListDialog.findViewById(R.id.txtWarn);
        mMyPlayList = (RecyclerView) myPlayListDialog.findViewById(R.id.listViewAlbum);
        swipyRefreshLayout = (SwipyRefreshLayout) myPlayListDialog.findViewById(R.id.swipe_refresh_layout);

        swipyRefreshLayout.setDirection(SwipyRefreshLayoutDirection.BOTTOM);
        mTxtWarn.setVisibility(View.GONE);

        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(context);
        mMyPlayList.setLayoutManager(linearLayoutManager);
        mMyPlayList.setItemAnimator(new DefaultItemAnimator());

        getAllMyList(fragmentMeditation.getContext());

        mMyPlayList.addOnItemTouchListener(
                new RecyclerItemClickListener(context, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        // TODO Handle item click
                        //getMyPlayListResponseData.get(position).getName();
                        savetoMyPlayList(songID, getMyPlayListResponseData.get(position).getId());
                        // Toast.makeText(fragmentMySongs.getContext(), "ID:-" +songID+ "PlayListName: " + getMyPlayListResponseData.get(position).getName(), Toast.LENGTH_SHORT).show();
                    }
                })
        );
    }


    public void getAllMyList(final Context context) {

        MyApplication myApplication = new MyApplication();

        Utility.getSharedInstance().showProgressDialog(context);


        if (Utility.getSharedInstance().isConnectedToInternet(context)) {

            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");

            // id="1";
            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");

            RequestGetMyPlayList requestGetMyPlayList = new RequestGetMyPlayList();

            requestGetMyPlayList.setUserId(id);
            requestGetMyPlayList.setxAPIKEY(X_API_KEY);

            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().getMyPlayList(requestGetMyPlayList);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());

                    if (responseData.get("error").getAsInt() == 0) {
                        // Config.getPlanDAO = new GetPlanDAO();
                        Gson gson = new Gson();

                        Utility.getSharedInstance().dismissProgressDialog();


                        if (responseData.get("respon") != null) {

                            JsonArray jsonArray = responseData.get("respon").getAsJsonArray();


                            if (jsonArray.size() > 0) {
                                myPlayListDialog.show();

                                mTxtWarn.setVisibility(View.GONE);
                                mMyPlayList.setVisibility(View.VISIBLE);

                                swipyRefreshLayout.setRefreshing(false);


                                gson = new Gson();
                                GetMyPlayListResponse getMyPlayListResponse = gson.fromJson(responseData, GetMyPlayListResponse.class);
                                getMyPlayListResponseData = getMyPlayListResponse.getRespon();
                                myPlayListAdapter = new MyPlayListAdapters(context, getMyPlayListResponseData);
                                mMyPlayList.setAdapter(myPlayListAdapter);
                            } else {

                                Toast.makeText(context, "You do not have playlists", Toast.LENGTH_SHORT).show();
                                swipyRefreshLayout.setRefreshing(false);
                                mTxtWarn.setVisibility(View.VISIBLE);
                                mMyPlayList.setVisibility(View.GONE);
                                Utility.getSharedInstance().dismissProgressDialog();

                            }


                        }


                    } else {
                        swipyRefreshLayout.setRefreshing(false);
                        Utility.getSharedInstance().dismissProgressDialog();
                        mTxtWarn.setVisibility(View.VISIBLE);
                        mMyPlayList.setVisibility(View.GONE);

                    }

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                    Utility.getSharedInstance().dismissProgressDialog();
                    mTxtWarn.setVisibility(View.VISIBLE);
                    mMyPlayList.setVisibility(View.GONE);
                    swipyRefreshLayout.setRefreshing(false);

                }


            });
        } else {
            Utility.getSharedInstance().dismissProgressDialog();
            mTxtWarn.setVisibility(View.VISIBLE);
            mMyPlayList.setVisibility(View.GONE);
            swipyRefreshLayout.setRefreshing(false);
        }
    }



    public void savetoMyPlayList(String trackId, String playListID) {

        MyApplication myApplication = new MyApplication();


        Utility.getSharedInstance().showProgressDialog(context);

        if (Utility.getSharedInstance().isConnectedToInternet(context)) {


            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");

            // id="1";
            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");


            RequestSaveToPlayList requestGetMyPlayList = new RequestSaveToPlayList();
            requestGetMyPlayList.setPlaylistId(playListID);
            requestGetMyPlayList.setTrackId(trackId);
            requestGetMyPlayList.setTrack_type("2");
            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().saveToPlayList(X_API_KEY, requestGetMyPlayList);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());

                    if (responseData != null) {


                        Toast.makeText(context, "Added successfully", Toast.LENGTH_SHORT).show();


                        myPlayListDialog.dismiss();
                        Utility.getSharedInstance().dismissProgressDialog();

                    } else {
                        Utility.getSharedInstance().dismissProgressDialog();
                    }

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                    Utility.getSharedInstance().dismissProgressDialog();


                }


            });
        } else {
            Utility.getSharedInstance().dismissProgressDialog();
            Toast.makeText(context, "No internet connection available", Toast.LENGTH_SHORT).show();
        }
    }
}