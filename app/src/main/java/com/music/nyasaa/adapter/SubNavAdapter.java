package com.music.nyasaa.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.music.nyasaa.R;
import com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity;
import com.music.nyasaa.utility.SharedPreferenceManager;

import java.util.ArrayList;

public class SubNavAdapter extends RecyclerView.Adapter<SubNavAdapter.HeaderHolder> {

    LayoutInflater inflater;
    boolean header = false;

    Context context;
DashBoardActivity dashBoardActivity;
    private ArrayList<String> stringArrayList;
    int lastPosition = -1;
    private String fragVal="";
    private String category="";


    public SubNavAdapter(Context context, ArrayList<String> subList, DashBoardActivity dashBoardActivity, String fragVal) {
        this.context = context;
        this.stringArrayList = subList;
        this.context=context;
        this.fragVal=fragVal;
        this.dashBoardActivity=dashBoardActivity;
    }


    @Override
    public SubNavAdapter.HeaderHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_items_sub_nav, parent, false);

        return new SubNavAdapter.HeaderHolder(itemView);


        // return new HeaderHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.chapter_item, null));
    }

    @Override
    public void onBindViewHolder(final SubNavAdapter.HeaderHolder holder, final int position) {


        holder.txtTitle.setText(stringArrayList.get(position).toString());

        holder.txtTitle.setTextColor(context.getResources().getColor(R.color.colorPrimary));
        holder.txtTitle.setGravity(Gravity.LEFT);



        holder.downArrow.setVisibility(View.GONE);
        holder.downArrow.setVisibility(View.GONE);
        holder.View.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(position==0)
                {
                    // short
                    dashBoardActivity.openshorAudio(fragVal);
                    SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_CAT,"1");

                }
                else {
                    // long
                    SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_CAT,"2");
                    dashBoardActivity.openLongAudio(fragVal);

                }
            }
        });




//
//        if (position > lastPosition) {
//
//            Animation animation = AnimationUtils.loadAnimation(context,
//                    R.anim.list_items);
//            holder.itemView.startAnimation(animation);
//            lastPosition = position;
//        }


    }

    @Override
    public int getItemCount() {
        return stringArrayList.size();
    }


    class HeaderHolder extends RecyclerView.ViewHolder {
        private TextView txtTitle;
        private LinearLayout mNotificationLayout;
        private ImageView downArrow;
        private RecyclerView subListView;
        private  View View;


        public HeaderHolder(View itemView) {
            super(itemView);

            //   imgWallaper = (ImageView) itemView.findViewById(R.id.imgWall);
            txtTitle = (TextView) itemView.findViewById(R.id.txtNavItem);

            downArrow = (ImageView) itemView.findViewById(R.id.downArrow);
            subListView = (RecyclerView) itemView.findViewById(R.id.subListView);
            View=itemView;


        }


    }
}



