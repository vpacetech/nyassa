package com.music.nyasaa.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.music.nyasaa.R;
import com.music.nyasaa.controller.MyApplication;
import com.music.nyasaa.fragments.community.FragmentCommunityDetails;
import com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity;
import com.music.nyasaa.manager.MediaController;
import com.music.nyasaa.models.RequestModel.evnt.RequestParticiptateEvent;
import com.music.nyasaa.models.ResponseModel.GetEventPeople.GetEventPeopleResponse;
import com.music.nyasaa.models.ResponseModel.GetEventPeople.GetEventPeopleResponseData;
import com.music.nyasaa.models.ResponseModel.GetMyPlayList.GetMyPlayListResponseData;
import com.music.nyasaa.models.SongDetail;
import com.music.nyasaa.retrofit.Config;
import com.music.nyasaa.utility.SharedPreferenceManager;
import com.music.nyasaa.utility.Utility;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.music.nyasaa.fragments.community.FragmentCommunityDetails.fragmentCommunityDetails;
import static com.music.nyasaa.retrofit.Config.X_API_KEY;

public class CommunityPostAdapter extends RecyclerView.Adapter<CommunityPostAdapter.ViewHolder> {
    private Context context;
    private List<SongDetail> mSongsList;
    private DisplayImageOptions options;
    private int lastPosition = -1;

    private Dialog myPlayListDialog;
    private RecyclerView mMyPlayList;
    private TextView mTxtWarn;
    private SwipyRefreshLayout swipyRefreshLayout;
    private List<GetMyPlayListResponseData> getMyPlayListResponseData;
    private MyPlayListAdapters myPlayListAdapter;
    private FragmentCommunityDetails fragmentCommunityDetailsNew;
    private Dialog confrimDeleteDialog, mediaDialog;
    String name;
    String address;
    String fromdate;
    String todate;
    String image;


    // Events


    RotateAnimation rotate = new RotateAnimation(
            0, 360,
            Animation.RELATIVE_TO_SELF, 0.5f,
            Animation.RELATIVE_TO_SELF, 0.5f
    );


    public CommunityPostAdapter(Context activity, List<SongDetail> getMyPlayListResponseData, FragmentCommunityDetails fragmentCommunityDetails) {

        this.context = activity;
        this.mSongsList = getMyPlayListResponseData;
        this.fragmentCommunityDetailsNew = fragmentCommunityDetails;
        options = new DisplayImageOptions.Builder().showImageOnLoading(R.drawable.bg_default_album_art).showImageForEmptyUri((int) R.drawable.bg_default_album_art).showImageOnFail((int) R.drawable.bg_default_album_art).cacheInMemory(true).cacheOnDisk(true).considerExifParams(true).bitmapConfig(Bitmap.Config.RGB_565).build();
    }

    public CommunityPostAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CommunityPostAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_community_post_latest, parent, false));
    }

    public void onBindViewHolder(final CommunityPostAdapter.ViewHolder holder, final int position) {


        final SongDetail mDetail = mSongsList.get(position);


        if (mSongsList.get(position).getTrackType().equalsIgnoreCase("6")) {

            holder.mLinearEvent.setVisibility(View.VISIBLE);
            holder.mLinearMedia.setVisibility(View.GONE);
//
//
//
//    /// code starts
//

            Picasso.with(context)
                    .load(Config.PIC_URL_EVENT + Uri.encode(mSongsList.get(position).getCover()))
                    .placeholder(R.drawable.bg_default_album_art)
                    .error(R.drawable.bg_default_album_art)
                    .into(holder.imgWallaper);
            // holder.imgWallaper.setImageResource(img[position]);

            String start = mSongsList.get(position).getFrom_date();
            String end = mSongsList.get(position).getTo_date();

            String startnew = setDateToTextview(start);

            String endnew = setDateToTextview(end);


            holder.mTxtStartDate.setText("Start: " + startnew);
            holder.mTxtEndDate.setText("End:   " + endnew);

/*
        holder.mTxtStartDate.setText("Start: "+mSongsList.get(position).getFrom_date());
        holder.mTxtEndDate.setText("End:   "+mSongsList.get(position).getTo_date());*/
            holder.txtEventName.setText(mSongsList.get(position).getTrack_name());
            holder.mTxtEventAdd.setText(mSongsList.get(position).getFile());


            holder.imgLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    showMediaDialog(mSongsList.get(position).getCover());
                }
            });


            // End of Codes


        } else {
            holder.mLinearEvent.setVisibility(View.GONE);
            holder.mLinearMedia.setVisibility(View.VISIBLE);
            if (mSongsList.get(position).getPlayStop()) {

                holder.btn_song_play.setImageResource(R.drawable.ic_pause);
            } else {
                holder.btn_song_play.setImageResource(R.drawable.ic_home_play);
            }


            holder.mediaTitle.setText(mDetail.getTrack_name());

            String time = mDetail.getCreatedAt();
//        holder.mTxtMediaTyape.setText(mDetail.get());
            String inputPattern = "yyyy-MM-dd HH:mm:ss";
            String outputPattern = "dd-MMM-yyyy h:mm a";
            SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
            SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

            Date date = null;
            String str = null;

            try {
                date = inputFormat.parse(time);
                str = outputFormat.format(date);
            } catch (Exception e) {
                e.printStackTrace();
            }

            holder.txtMediapplName.setText("Post shared by  " + mDetail.getUsername());
            holder.mTxtDate.setText("on " + str);


            if (mDetail.getFile().contains(".mp4") || mDetail.getFile().contains(".m4v")) {

                Picasso.with(context)
                        .load(Config.BASE_VIDEO_URL_COVER + Uri.encode(mDetail.getCover()))
                        .placeholder(R.drawable.default_music)
                        .error(R.drawable.default_music)
                        .into(holder.cover);

            } else {
                Picasso.with(context)
                        .load(Config.PIC_URL + "cover/" + Uri.encode(mDetail.getCover()))
                        .placeholder(R.drawable.default_music)
                        .error(R.drawable.default_music)
                        .into(holder.cover);
            }


            holder.btn_song_play.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    // holder.loadingSong.setVisibility(View.VISIBLE);

                    if (mDetail.getFile().contains(".mp4") || mDetail.getFile().contains(".m4v"))


                    {


                        MediaController.getInstance().stopAudio();

//
//                        Intent intent = new Intent(context, VideoPlayerActivity.class);
//                        intent.putExtra("url", mDetail.getFile());
//                        context.startActivity(intent);


                    } else {


                        fragmentCommunityDetails.playPoase(position);


                        if (mSongsList.get(position).getPlayStop()) {
                            DashBoardActivity.context.setPlayer(false);
                            DashBoardActivity.context.setStatus(true);
                            DashBoardActivity.context.onResume();

                            SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_MUSIC_MODE, "music");
                            DashBoardActivity.context.loadSongsDetails(mDetail);
//
//                }
                            //new DownloadMp3CacheTask().execute(mDetail.getUrl(),mDetail.display_name);
                            if (!MediaController.getInstance().isPlayingAudio(mDetail) || MediaController.getInstance().isAudioPaused()) {
                                MediaController.getInstance().stopAudio();
                                MediaController.getInstance().playMUSIC(mDetail);

                                //  holder.loadingSong.setVisibility(View.GONE);


//                    rotate.setDuration(900);
//                    rotate.setRepeatCount(Animation.INFINITE);
//                    itemImage.startAnimation(rotate);
                                // MediaController.getInstance().setPlaylist(mSongsList, mDetail, PhoneMediaControl.SonLoadFor.MostPlay.ordinal(), -1);
                            } else {
                                MediaController.getInstance().pauseAudio(mDetail);
//                        DashBoardActivity.stop_rotateImage();
                            }


                        } else {
                            DashBoardActivity.context.setPlayer(true);
                            DashBoardActivity.context.setStatus(false);
                            if (!MediaController.getInstance().isPlayingAudio(mDetail) || MediaController.getInstance().isAudioPaused()) {
                                MediaController.getInstance().stopAudio();
                                MediaController.getInstance().pauseAudio(mDetail);

//                        DashBoardActivity.stop_rotateImage();
                                // MediaController.getInstance().playAudio(mDetail);
                                //  holder.loadingSong.setVisibility(View.GONE);


//                    rotate.setDuration(900);
//                    rotate.setRepeatCount(Animation.INFINITE);
//                    itemImage.startAnimation(rotate);
                                // MediaController.getInstance().setPlaylist(mSongsList, mDetail, PhoneMediaControl.SonLoadFor.MostPlay.ordinal(), -1);
                            } else {
                                MediaController.getInstance().pauseAudio(mDetail);
                                DashBoardActivity.stop_rotateImage();
                            }
                        }
                    }


                }
            });
        }


    }

    public int getItemCount() {
        return mSongsList.size();
    }

    public class ViewHolder extends android.support.v7.widget.RecyclerView.ViewHolder {

        private ImageView cover, btn_song_play;
        private TextView mediaTitle, txtMediapplName, mTxtDate, mTxtMediaTyape;
        private LinearLayout mLinearEvent, mLinearMedia;
        private LinearLayout mLinearLaypout;

        // Evensts

        private TextView txtEventName, mTxtAttend, mTxtStartDate, mTxtEventAdd, mTxtEndDate;
        private LinearLayout mNotificationLayout, attendLayout, peopleLayout, imgLayout, shareLayout;
        private ImageView imgWallaper;
        private ImageView imgTick;
        private View viewVertical;
        private LinearLayout layoutEvent;


        public ViewHolder(View itemView) {
            super(itemView);
            mediaTitle = (TextView) itemView.findViewById(R.id.txtMediaName);
            txtMediapplName = (TextView) itemView.findViewById(R.id.txtMediaPplName);
            mTxtDate = (TextView) itemView.findViewById(R.id.txtMediaDate);
            mTxtMediaTyape = (TextView) itemView.findViewById(R.id.txtMediaType);
            btn_song_play = (ImageView) itemView.findViewById(R.id.btn_song_play);

            cover = (ImageView) itemView.findViewById(R.id.mediaImg);


            mLinearLaypout = (LinearLayout) itemView.findViewById(R.id.layoutPlayList);
            mLinearEvent = (LinearLayout) itemView.findViewById(R.id.layoutEvent);
            mLinearMedia = (LinearLayout) itemView.findViewById(R.id.layMedia);


            // Events

            imgWallaper = (ImageView) itemView.findViewById(R.id.imgWall);
            txtEventName = (TextView) itemView.findViewById(R.id.txtEventName);
            mTxtAttend = (TextView) itemView.findViewById(R.id.txtAttend);
            mTxtEventAdd = (TextView) itemView.findViewById(R.id.txtEventAdd);
            mTxtStartDate = (TextView) itemView.findViewById(R.id.txtEventStartDate);

            mTxtEndDate = (TextView) itemView.findViewById(R.id.txtEventEndDate);
            shareLayout = (LinearLayout) itemView.findViewById(R.id.shareLayout);

            imgLayout = (LinearLayout) itemView.findViewById(R.id.imgLayout);

            attendLayout = (LinearLayout) itemView.findViewById(R.id.attendLayout);

            peopleLayout = (LinearLayout) itemView.findViewById(R.id.EpeopleLayout);

            imgTick = itemView.findViewById(R.id.imgTick);
            viewVertical = itemView.findViewById(R.id.viewVertical);
            layoutEvent = itemView.findViewById(R.id.layoutEvent);
        }
    }

    /// API FOR EVENTs


    //////////////////// API Connection ///////////////////////


    private void attendEvent(String evID, final int mypos) {


        MyApplication myApplication = new MyApplication();


        if (Utility.getSharedInstance().isConnectedToInternet(context)) {

            Utility.getSharedInstance().showProgressDialog(context);

            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "none");

            RequestParticiptateEvent requestParticiptateEvent = new RequestParticiptateEvent();

            requestParticiptateEvent.setEventId(evID);
            requestParticiptateEvent.setUserId(id);


            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().attendEvent(Config.X_API_KEY, requestParticiptateEvent);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    JsonObject responseData = response.body();

                    if (responseData.get("status").getAsString().equalsIgnoreCase("true")) {

                        Toast.makeText(context, "Great your attending this event", Toast.LENGTH_SHORT).show();

//                        getEventResponseDataList.get(mypos).setLike("1");
                        notifyDataSetChanged();
                        Utility.getSharedInstance().dismissProgressDialog();
                        confrimDeleteDialog.dismiss();


                    } else {

                        Utility.getSharedInstance().dismissProgressDialog();
                        Toast.makeText(context, "" + responseData.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                    }


                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Toast.makeText(context, "Api issue", Toast.LENGTH_SHORT).show();
                    Utility.getSharedInstance().dismissProgressDialog();
                    //   Utility.getSharedInstance().Snackbar(toolbarLayout, "Something went wrong!");
                }


            });
        } else {
            Toast.makeText(context, "No internet connection available", Toast.LENGTH_SHORT).show();

        }
    }

    ///// Remove Event From


    private void removeEvent(String evID, final int mypos) {


        MyApplication myApplication = new MyApplication();


        if (Utility.getSharedInstance().isConnectedToInternet(context)) {

            Utility.getSharedInstance().showProgressDialog(context);

            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "none");

            RequestParticiptateEvent requestParticiptateEvent = new RequestParticiptateEvent();

            requestParticiptateEvent.setEventId(evID);
            requestParticiptateEvent.setUserId(id);


            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().removeEvent(Config.X_API_KEY, requestParticiptateEvent);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    JsonObject responseData = response.body();

                    if (responseData.get("status").getAsString().equalsIgnoreCase("true")) {

                        Toast.makeText(context, "Your not attending this event", Toast.LENGTH_SHORT).show();

//                        getEventResponseDataList.get(mypos).setLike("0");
                        notifyDataSetChanged();
                        Utility.getSharedInstance().dismissProgressDialog();
                        confrimDeleteDialog.dismiss();


                    } else {

                        Utility.getSharedInstance().dismissProgressDialog();
                        Toast.makeText(context, "" + responseData.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                    }


                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Toast.makeText(context, "Api issue", Toast.LENGTH_SHORT).show();
                    Utility.getSharedInstance().dismissProgressDialog();
                    //   Utility.getSharedInstance().Snackbar(toolbarLayout, "Something went wrong!");
                }


            });
        } else {
            Toast.makeText(context, "No internet connection", Toast.LENGTH_SHORT).show();

        }
    }

    //// Get All User Lits

    public void getAllPeoples(String evID) {

        MyApplication myApplication = new MyApplication();


        if (Utility.getSharedInstance().isConnectedToInternet(context)) {
            Utility.getSharedInstance().showProgressDialog(context);

            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");

            // id="1";
            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");
//            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().getAllEvents( X_API_KEY,"10","0");
            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().getALluserforParticluarEvent(X_API_KEY, evID);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());

                    if (responseData.get("status").getAsString().equalsIgnoreCase("true")) {
                        // Config.getPlanDAO = new GetPlanDAO();
                        Gson gson = new Gson();

                        Utility.getSharedInstance().dismissProgressDialog();


                        confrimDeleteDialog = new Dialog(context);
                        confrimDeleteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        confrimDeleteDialog.setContentView(R.layout.dialog_show_people_list);
                        confrimDeleteDialog.getWindow().getAttributes().width = ActionBar.LayoutParams.FILL_PARENT;
                        confrimDeleteDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
                        confrimDeleteDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


                        RecyclerView recyclerView = (RecyclerView) confrimDeleteDialog.findViewById(R.id.peopleList);
                        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(context);
                        recyclerView.setLayoutManager(linearLayoutManager);
                        recyclerView.setItemAnimator(new DefaultItemAnimator());


                        GetEventPeopleResponse getEventPeopleResponse = gson.fromJson(responseData, GetEventPeopleResponse.class);

                        List<GetEventPeopleResponseData> getEventPeopleResponseDataList = getEventPeopleResponse.getResultObject();

                        GetPeopleListAdapter getPeopleListAdapter = new GetPeopleListAdapter(context, getEventPeopleResponseDataList);

                        recyclerView.setAdapter(getPeopleListAdapter);

                        if (getEventPeopleResponseDataList != null) {
                            if (getEventPeopleResponseDataList.size() > 0)


                            {
                                confrimDeleteDialog.show();
                            }
                        }


                        // JsonObject jsonObject=responseData.getAsJsonObject("respon");


                        // Utility.getSharedInstance().Snackbar(toolbarLayout, "Password Changed Sucessfully");


                    } else {

                        Toast.makeText(context, "Opps! no one is list", Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                    Utility.getSharedInstance().dismissProgressDialog();
                    Toast.makeText(context, "API Issue", Toast.LENGTH_SHORT).show();


                }


            });
        } else {

            Toast.makeText(context, "No internet connection available", Toast.LENGTH_SHORT).show();

        }
    }


    private void showMediaDialog(String url) {
        mediaDialog = new Dialog(context, android.R.style.Theme_Dialog);
        mediaDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        mediaDialog.setContentView(R.layout.dialog_media);
        mediaDialog.getWindow().getAttributes().width = ActionBar.LayoutParams.FILL_PARENT;
        mediaDialog.getWindow().setBackgroundDrawable(new ColorDrawable(context.getResources().getColor(R.color.blackTransnew)));
        WindowManager.LayoutParams wmlps = mediaDialog.getWindow().getAttributes();
        // mediaDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        Window window = mediaDialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        window.setGravity(Gravity.CENTER);
        ImageView imgWallPost = (ImageView) mediaDialog.findViewById(R.id.imageViewWallImage);


        Picasso.with(context)
                .load(Config.PIC_URL_EVENT + Uri.encode(url))
                .placeholder(R.drawable.bg_default_album_art)
                .error(R.drawable.bg_default_album_art)
                .into(imgWallPost);


        mediaDialog.show();
    }
    // VideoView myVideoView = (VideoView) mediaDialog.findViewById(R.id.videoView1);


    public String setDateToTextview(String Date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        String reportDate = null;
        try {
            date = format.parse(Date);
            DateFormat dff = new SimpleDateFormat("dd-MMM-yyyy");
            reportDate = dff.format(date);
        } catch (java.text.ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        // strDateArray[k]=String.valueOf(date.sub);
        return reportDate;
    }
}