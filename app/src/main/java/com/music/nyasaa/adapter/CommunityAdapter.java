package com.music.nyasaa.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.music.nyasaa.R;
import com.music.nyasaa.front_end_layer.activities.add_community.AddCommunityActivity;
import com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity;
import com.music.nyasaa.models.ResponseModel.getCommunity.GetCommunityResponseData;
import com.music.nyasaa.retrofit.Config;
import com.music.nyasaa.utility.SharedPreferenceManager;
import com.squareup.picasso.Picasso;

import java.util.List;


public class CommunityAdapter extends RecyclerView.Adapter<CommunityAdapter.HeaderHolder> {

    LayoutInflater inflater;
    boolean header = false;

    Context context;

    private List<GetCommunityResponseData> community_dao;

    private int lastPosition = -1;


    public CommunityAdapter(Context context, List<GetCommunityResponseData> Community_DAO) {

        this.context = context;
//        this.img = img;
//        this.name = name;

        this.community_dao = Community_DAO;


    }


    @Override
    public CommunityAdapter.HeaderHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_items_community, parent, false);

        return new CommunityAdapter.HeaderHolder(itemView);


        // return new HeaderHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.chapter_item, null));
    }

    @Override
    public void onBindViewHolder(CommunityAdapter.HeaderHolder holder, final int position) {


        // use pisco and check null url :'

        // holder.imgWallaper.setImageResource(community_dao.get(position).getImage_url());

        if (community_dao != null)
            if (community_dao.size() > 0) {


                holder.txtTitle.setText(community_dao.get(position).getCommunityName());
                holder.txtTitle.setVisibility(View.VISIBLE);

                String userID = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "none");


                if (userID.equalsIgnoreCase(String.valueOf(community_dao.get(position).getParentUserId()))) {
                    holder.imgparentImg.setVisibility(View.VISIBLE);

                } else {
                    holder.imgparentImg.setVisibility(View.GONE);
                }


                Picasso.with(context)
                        .load(Config.PIC_URL_COMMUNITY + Uri.encode(community_dao.get(position).getImageUrl()))
                        .placeholder(R.drawable.community_blank)
                        .error(R.drawable.community_blank)
                        .into(holder.imgWallaper);
//


            }

//        holder.txtTitle.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(context, "Text", Toast.LENGTH_SHORT).show();
//            }
//        });

        holder.imgparentImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AddCommunityActivity.class);
                intent.putExtra("comm", "ad");
                intent.putExtra("name", community_dao.get(position).getCommunityName());
                intent.putExtra("id", community_dao.get(position).getId());
                context.startActivity(intent);

            }
        });

        if (community_dao.get(position).getAction().equalsIgnoreCase("2")) {

            holder.mLayPending.setVisibility(View.GONE);
            holder.imgWallaper.setClickable(true);
            holder.imgWallaper.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_FRAG_MAINTAIN, "1");

                    SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_COMMUNITY_NAME, community_dao.get(position).getCommunityName());
                    SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_COMM_PARENT_ID_COMMUNITY, community_dao.get(position).getParentUserId());

                    SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_IS_OWN_COMMUNITY,community_dao.get(position).getParentUserId());
                    SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_COMM_ID, community_dao.get(position).getId());
                    //Toast.makeText(context, "Image", Toast.LENGTH_SHORT).show();
//                Intent intent = new Intent(context, CommunityMembersActivity.class);
//                context.startActivity(intent);

                    ((DashBoardActivity) context).openCommunityDetails(community_dao.get(position).getCommunityName());

                    ((DashBoardActivity) context).overridePendingTransition(R.anim.left, R.anim.right);
                }
            });
        } else {
            holder.imgWallaper.setClickable(true);
            holder.mLayPending.setVisibility(View.VISIBLE);
            holder.imgWallaper.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Toast.makeText(context, "Your request sent to Community Admin , please wait for approval", Toast.LENGTH_SHORT).show();
                }
            });


        }


//        if (position > lastPosition) {
//
//            Animation animation = AnimationUtils.loadAnimation(context,
//                    R.anim.list_items);
//            holder.itemView.startAnimation(animation);
//            lastPosition = position;
//        }

    }

    @Override
    public int getItemCount() {
        return community_dao.size();
    }


    class HeaderHolder extends RecyclerView.ViewHolder {
        private TextView txtTitle;
        private RelativeLayout relativelayout;
        private LinearLayout mLayPending;
        private ImageView imgWallaper, imgparentImg;


        public HeaderHolder(View itemView) {
            super(itemView);

            imgWallaper = (ImageView) itemView.findViewById(R.id.imgWall);
            txtTitle = (TextView) itemView.findViewById(R.id.txtImgText);
            imgparentImg = (ImageView) itemView.findViewById(R.id.txtCrossMark);
            mLayPending = (LinearLayout) itemView.findViewById(R.id.layPending);


        }


    }
}


