package com.music.nyasaa.adapter;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.music.nyasaa.R;
import com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity;


import java.util.ArrayList;

public class NavAdapter extends RecyclerView.Adapter<NavAdapter.HeaderHolder> {

    LayoutInflater inflater;
    boolean header = false;

    Context context;

    private ArrayList<String> stringArrayList;
    int lastPosition = -1;
    private boolean subListFlagSong = false;
    private boolean subListFlag = false;
    DashBoardActivity dashBoardActivity;
    public FragmentManager f_manager;
    private String fragVal = "";
    private int img[];


    public NavAdapter(Context context, ArrayList<String> strings, DashBoardActivity dashBoardActivity, int[] img) {

        this.context = context;
        this.img = img;
        this.stringArrayList = strings;
        this.dashBoardActivity = dashBoardActivity;

    }


    @Override
    public NavAdapter.HeaderHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_items_navigation, parent, false);

        return new NavAdapter.HeaderHolder(itemView);


        // return new HeaderHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.chapter_item, null));
    }

    @Override
    public void onBindViewHolder(final NavAdapter.HeaderHolder holder, final int position) {

        final ArrayList<String> subList = new ArrayList<>();

        subList.add("English");
        subList.add("Hindi");


        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(context);
        holder.subListView.setLayoutManager(linearLayoutManager);
        holder.subListView.setItemAnimator(new DefaultItemAnimator());


        holder.txtTitle.setText(stringArrayList.get(position).toString());

        if (position == 1 || position == 2) {
            holder.downArrow.setVisibility(View.VISIBLE);
        } else {
            holder.downArrow.setVisibility(View.GONE);
        }


        holder.downArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//

            }
        });

        holder.mNavTitile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((DashBoardActivity) context).openFragment(position);
            }
        });

        holder.icons.setImageResource(img[position]);

        if (position > lastPosition) {

            Animation animation = AnimationUtils.loadAnimation(context,
                    R.anim.list_items);
            holder.itemView.startAnimation(animation);
            lastPosition = position;
        }


    }

    @Override
    public int getItemCount() {
        return stringArrayList.size();
    }


    class HeaderHolder extends RecyclerView.ViewHolder {
        private TextView txtTitle;
        private LinearLayout mNavTitile;
        private ImageView downArrow, icons;
        private RecyclerView subListView;


        public HeaderHolder(View itemView) {
            super(itemView);

            //   imgWallaper = (ImageView) itemView.findViewById(R.id.imgWall);
            txtTitle = (TextView) itemView.findViewById(R.id.txtNavItem);
            mNavTitile = (LinearLayout) itemView.findViewById(R.id.navNameLayout);

            downArrow = (ImageView) itemView.findViewById(R.id.downArrow);
            icons = (ImageView) itemView.findViewById(R.id.icons);
            subListView = (RecyclerView) itemView.findViewById(R.id.subListView);


        }


    }
}


//listview.setSelection(listview.getAdapter().getCount()-1);