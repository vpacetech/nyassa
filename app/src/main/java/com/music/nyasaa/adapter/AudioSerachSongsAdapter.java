package com.music.nyasaa.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.music.nyasaa.R;
import com.music.nyasaa.dbhandler.MusicSQLiteHelper;
import com.music.nyasaa.fragments.mysongs.FragmentMySongs;
import com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity;
import com.music.nyasaa.manager.MediaController;
import com.music.nyasaa.models.ResponseModel.GetMyPlayList.GetMyPlayListResponseData;
import com.music.nyasaa.models.SongDetail;
import com.music.nyasaa.retrofit.Config;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class AudioSerachSongsAdapter extends RecyclerView.Adapter<AudioSerachSongsAdapter.ViewHolder> {
    private Context context;
    private ArrayList<SongDetail> mSongsList;
    private DisplayImageOptions options;
    private int lastPosition = -1;

    private Dialog myPlayListDialog;
    private RecyclerView mMyPlayList;
    private TextView mTxtWarn;
    private SwipyRefreshLayout swipyRefreshLayout;
    private List<GetMyPlayListResponseData> getMyPlayListResponseData;
    private MyPlayListAdapters myPlayListAdapter;
    private FragmentMySongs fragmentMySongs;


    RotateAnimation rotate = new RotateAnimation(
            0, 360,
            Animation.RELATIVE_TO_SELF, 0.5f,
            Animation.RELATIVE_TO_SELF, 0.5f
    );

    public AudioSerachSongsAdapter(Context context, ArrayList<SongDetail> mSongsList) {
        this.context = context;
        this.mSongsList = mSongsList;
        this.fragmentMySongs = fragmentMySongs;
        options = new DisplayImageOptions.Builder().showImageOnLoading(R.drawable.bg_default_album_art).showImageForEmptyUri((int) R.drawable.bg_default_album_art).showImageOnFail((int) R.drawable.bg_default_album_art).cacheInMemory(true).cacheOnDisk(true).considerExifParams(true).bitmapConfig(Bitmap.Config.RGB_565).build();


    }

    public AudioSerachSongsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AudioSerachSongsAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_playlist, parent, false));
    }

    public void onBindViewHolder(final AudioSerachSongsAdapter.ViewHolder holder, final int position) {


        final SongDetail mDetail = mSongsList.get(position);
        holder.songTitle.setText(mDetail.getName());
        if (mDetail.getUrl().length() != 0)


            Picasso.with(context)
                    .load(Config.PIC_URL + "cover/"+ mDetail.getCover())
                    .placeholder(R.drawable.bg_default_album_art)
                    .error(R.drawable.bg_default_album_art)
                    .into(holder.cover);

        holder.btn_song_play.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // holder.loadingSong.setVisibility(View.VISIBLE);
                DashBoardActivity.context.setPlayer(false);
                DashBoardActivity.context. setStatus(true);
                DashBoardActivity.context.onResume();
                DashBoardActivity.context.loadSongsDetails(mDetail);
//                if (mDetail.getType().equals("html")) {
//                    LyricsActivity activity = new LyricsActivity();
//                    Bundle bundle = new Bundle();
//                    bundle.putString("title", mDetail.getTitle());
//                    bundle.putString("lyrics", mDetail.getLyrics());
//                    activity.setArguments(bundle);
//                    DashBoardActivity.context.addFragmentOnTop(activity);
//                    return;
//                }
                //new DownloadMp3CacheTask().execute(mDetail.getUrl(),mDetail.display_name);
                if (!MediaController.getInstance().isPlayingAudio(mDetail) || MediaController.getInstance().isAudioPaused()) {
                    MediaController.getInstance().stopAudio();
                    MediaController.getInstance().playAudio(mDetail);
                    //  holder.loadingSong.setVisibility(View.GONE);


//                    rotate.setDuration(900);
//                    rotate.setRepeatCount(Animation.INFINITE);
//                    itemImage.startAnimation(rotate);
                    // MediaController.getInstance().setPlaylist(mSongsList, mDetail, PhoneMediaControl.SonLoadFor.MostPlay.ordinal(), -1);
                } else {
                    MediaController.getInstance().pauseAudio(mDetail);
                    DashBoardActivity.stop_rotateImage();
                }
                if (MusicSQLiteHelper.isExistQueueSong(mDetail)) {
                    MusicSQLiteHelper.updateQueue(mDetail);
                } else {
                    MusicSQLiteHelper.insertSongInQueue(mDetail);
                }
            }
        });

        holder.songTitle.setText(mDetail.getName());


    }

    public int getItemCount() {
        return mSongsList.size();
    }

    public class ViewHolder extends android.support.v7.widget.RecyclerView.ViewHolder {
        ImageView btn_song_play;
        ImageView cover;
        TextView songTitle;
        ProgressBar loadingSong;
        private LinearLayout mLinearLaypout;

        public ViewHolder(View itemView) {
            super(itemView);
            songTitle = (TextView) itemView.findViewById(R.id.txtPlayListName);
            btn_song_play = (ImageView) itemView.findViewById(R.id.btn_play);
            cover = (ImageView) itemView.findViewById(R.id.songImage);
            loadingSong = (ProgressBar) itemView.findViewById(R.id.loadingSong);

            mLinearLaypout = (LinearLayout) itemView.findViewById(R.id.layoutPlayList);
        }
    }
}





