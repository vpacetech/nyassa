package com.music.nyasaa.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.music.nyasaa.R;
import com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity;
import com.music.nyasaa.models.GetEventsModel;
import com.music.nyasaa.retrofit.Config;
import com.music.nyasaa.utility.SharedPreferenceManager;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class EventAdapter extends RecyclerView.Adapter<EventAdapter.HeaderHolder> {

    LayoutInflater inflater;
    boolean header = false;

    String name;
    String address;
    String fromdate;
    String todate;
    String image;

    Context context;
    private Dialog confrimDeleteDialog, mediaDialog;

    private Integer[] img;
    int lastPosition = -1;
    private List<GetEventsModel> getEventResponseDataList;
    private List<GetEventsModel> getEventResponseDataListNew;

    public EventAdapter(Context context, List<GetEventsModel> img) {

        this.context = context;
        this.getEventResponseDataList = img;
        this.getEventResponseDataListNew=img;


    }


    @Override
    public EventAdapter.HeaderHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_items_event_latest, parent, false);

        return new EventAdapter.HeaderHolder(itemView);


        // return new HeaderHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.chapter_item, null));
    }

    @Override
    public void onBindViewHolder(final EventAdapter.HeaderHolder holder, final int position) {


        Picasso.with(context)
                .load(Config.PIC_URL + Uri.encode(getEventResponseDataList.get(position).getImage()))
                .placeholder(R.drawable.bg_default_album_art)
                .error(R.drawable.bg_default_album_art)
                .into(holder.imgWallaper);
        // holder.imgWallaper.setImageResource(img[position]);

        String start = getEventResponseDataList.get(position).getEventDateTime();
        String end = getEventResponseDataList.get(position).getEventDateTime();

        String startnew = setDateToTextview(start);

        String endnew = setDateToTextview(end);


        holder.mTxtStartDate.setText("Start: " + startnew);
//        holder.mTxtEndDate.setText("End:   " + endnew);


        holder.mTxtEndDate.setVisibility(View.GONE);

/*
        holder.mTxtStartDate.setText("Start: "+getEventResponseDataList.get(position).getFrom_date());
        holder.mTxtEndDate.setText("End:   "+getEventResponseDataList.get(position).getTo_date());*/
        holder.txtEventName.setText(getEventResponseDataList.get(position).getTitle());
        holder.mTxtEventAdd.setText(getEventResponseDataList.get(position).getId());


        holder.shareLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_TRACK_TYPE, "6");
                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_TRACK_ID, String.valueOf(getEventResponseDataList.get(position).getId()));
                DashBoardActivity dash = new DashBoardActivity();
               // dash.showCommDialog(context);
            }
        });

        // ((AddEventActivity)context).getEventDetails(name,address,fromdate,todate,image);

    }

    @Override
    public int getItemCount() {
        return getEventResponseDataList.size();
    }





    class HeaderHolder extends RecyclerView.ViewHolder {
        private TextView txtEventName, mTxtAttend, mTxtStartDate, mTxtEventAdd, mTxtEndDate;
        private LinearLayout mNotificationLayout, attendLayout, peopleLayout, imgLayout, shareLayout;
        private ImageView imgWallaper;
        private ImageView imgTick;
        private View viewVertical;
        private View viewSVertical;
        private LinearLayout layoutEvent,bottomLayout,editLayout;



        public HeaderHolder(View itemView) {
            super(itemView);
            editLayout=(LinearLayout)itemView.findViewById(R.id.editLayout);

            bottomLayout=(LinearLayout)itemView.findViewById(R.id.bottomLayoutEvnet);
            imgWallaper = (ImageView) itemView.findViewById(R.id.imgWall);
            txtEventName = (TextView) itemView.findViewById(R.id.txtEventName);
            mTxtAttend = (TextView) itemView.findViewById(R.id.txtAttend);
            mTxtEventAdd = (TextView) itemView.findViewById(R.id.txtEventAdd);
            mTxtStartDate = (TextView) itemView.findViewById(R.id.txtEventStartDate);

            mTxtEndDate = (TextView) itemView.findViewById(R.id.txtEventEndDate);
            shareLayout = (LinearLayout) itemView.findViewById(R.id.shareLayout);

            imgLayout = (LinearLayout) itemView.findViewById(R.id.imgLayout);

            attendLayout = (LinearLayout) itemView.findViewById(R.id.attendLayout);

            peopleLayout = (LinearLayout) itemView.findViewById(R.id.peopleLayout);

            imgTick = itemView.findViewById(R.id.imgTick);
            viewVertical = itemView.findViewById(R.id.viewVertical);
            viewSVertical = itemView.findViewById(R.id.viewSVertical);
            layoutEvent = itemView.findViewById(R.id.layoutEvent);


        }


    }


    //////////////////// API Connection ///////////////////////


//    private void attendEvent(String evID, final int mypos) {
//
//
//        MyApplication myApplication = new MyApplication();
//
//
//        if (Utility.getSharedInstance().isConnectedToInternet(context)) {
//
//            Utility.getSharedInstance().showProgressDialog(context);
//
//            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "none");
//
//            RequestParticiptateEvent requestParticiptateEvent = new RequestParticiptateEvent();
//
//            requestParticiptateEvent.setEventId(evID);
//            requestParticiptateEvent.setUserId(id);
//
//
//            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().attendEvent(Config.X_API_KEY, requestParticiptateEvent);
//            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
//            callbackLogin.enqueue(new Callback<JsonObject>() {
//                @Override
//                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//
//                    JsonObject responseData = response.body();
//
//                    if (responseData.get("status").getAsString().equalsIgnoreCase("true")) {
//
//                        Toast.makeText(context, "Great your attending this event", Toast.LENGTH_SHORT).show();
//
//                        getEventResponseDataList.get(mypos).setLike("1");
//                        notifyDataSetChanged();
//                        Utility.getSharedInstance().dismissProgressDialog();
//                        confrimDeleteDialog.dismiss();
//
//
//                    } else {
//
//                        Utility.getSharedInstance().dismissProgressDialog();
//                        Toast.makeText(context, "" + responseData.get("message").getAsString(), Toast.LENGTH_SHORT).show();
//                    }
//
//
//                }
//
//                @Override
//                public void onFailure(Call<JsonObject> call, Throwable t) {
//                    Toast.makeText(context, "Api issue", Toast.LENGTH_SHORT).show();
//                    Utility.getSharedInstance().dismissProgressDialog();
//                    //   Utility.getSharedInstance().Snackbar(toolbarLayout, "Something went wrong!");
//                }
//
//
//            });
//        } else {
//            Toast.makeText(context, "No internet connection available", Toast.LENGTH_SHORT).show();
//
//        }
//    }
//
//    ///// Remove Event From
//
//
//    private void removeEvent(String evID, final int mypos) {
//
//
//        MyApplication myApplication = new MyApplication();
//
//
//        if (Utility.getSharedInstance().isConnectedToInternet(context)) {
//
//            Utility.getSharedInstance().showProgressDialog(context);
//
//            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "none");
//
//            RequestParticiptateEvent requestParticiptateEvent = new RequestParticiptateEvent();
//
//            requestParticiptateEvent.setEventId(evID);
//            requestParticiptateEvent.setUserId(id);
//
//
//            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().removeEvent(Config.X_API_KEY, requestParticiptateEvent);
//            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
//            callbackLogin.enqueue(new Callback<JsonObject>() {
//                @Override
//                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//
//                    JsonObject responseData = response.body();
//
//                    if (responseData.get("status").getAsString().equalsIgnoreCase("true")) {
//
//                        Toast.makeText(context, "Your not attending this event", Toast.LENGTH_SHORT).show();
//
//                        getEventResponseDataList.get(mypos).setLike("0");
//                        notifyDataSetChanged();
//                        Utility.getSharedInstance().dismissProgressDialog();
//                        confrimDeleteDialog.dismiss();
//
//
//                    } else {
//
//                        Utility.getSharedInstance().dismissProgressDialog();
//                        Toast.makeText(context, "" + responseData.get("message").getAsString(), Toast.LENGTH_SHORT).show();
//                    }
//
//
//                }
//
//                @Override
//                public void onFailure(Call<JsonObject> call, Throwable t) {
//                    Toast.makeText(context, "Api issue", Toast.LENGTH_SHORT).show();
//                    Utility.getSharedInstance().dismissProgressDialog();
//                    //   Utility.getSharedInstance().Snackbar(toolbarLayout, "Something went wrong!");
//                }
//
//
//            });
//        } else {
//            Toast.makeText(context, "No internet connection", Toast.LENGTH_SHORT).show();
//
//        }
//    }
//
//    //// Get All User Lits
//
//    public void getAllPeoples(String evID) {
//
//        MyApplication myApplication = new MyApplication();
//
//
//        if (Utility.getSharedInstance().isConnectedToInternet(context)) {
//            Utility.getSharedInstance().showProgressDialog(context);
//
//            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");
//
//            // id="1";
//            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");
////            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().getAllEvents( X_API_KEY,"10","0");
//            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().getALluserforParticluarEvent(X_API_KEY, evID);
//            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
//            callbackLogin.enqueue(new Callback<JsonObject>() {
//                @Override
//                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//
//                    // JsonObject responseData = response.body();
//
//                    JsonObject responseData = response.body();
//                    // Log.e("Response::", responseData.toString());
//
//                    if (responseData.get("status").getAsString().equalsIgnoreCase("true")) {
//                        // Config.getPlanDAO = new GetPlanDAO();
//                        Gson gson = new Gson();
//
//                        Utility.getSharedInstance().dismissProgressDialog();
//
//
//                        confrimDeleteDialog = new Dialog(context);
//                        confrimDeleteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                        confrimDeleteDialog.setContentView(R.layout.dialog_show_people_list);
//                        confrimDeleteDialog.getWindow().getAttributes().width = ActionBar.LayoutParams.FILL_PARENT;
//                        confrimDeleteDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
//                        confrimDeleteDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//
//
//                        RecyclerView recyclerView = (RecyclerView) confrimDeleteDialog.findViewById(R.id.peopleList);
//                        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(context);
//                        recyclerView.setLayoutManager(linearLayoutManager);
//                        recyclerView.setItemAnimator(new DefaultItemAnimator());
//
//
//                        GetEventPeopleResponse getEventPeopleResponse = gson.fromJson(responseData, GetEventPeopleResponse.class);
//
//                        List<GetEventPeopleResponseData> getEventPeopleResponseDataList = getEventPeopleResponse.getResultObject();
//
//                        GetPeopleListAdapter getPeopleListAdapter = new GetPeopleListAdapter(context, getEventPeopleResponseDataList);
//
//                        recyclerView.setAdapter(getPeopleListAdapter);
//
//                        if (getEventPeopleResponseDataList != null) {
//                            if (getEventPeopleResponseDataList.size() > 0)
//
//
//                            {
//                                confrimDeleteDialog.show();
//                            }
//                        }
//
//
//                        // JsonObject jsonObject=responseData.getAsJsonObject("respon");
//
//
//                        // Utility.getSharedInstance().Snackbar(toolbarLayout, "Password Changed Sucessfully");
//
//
//                    } else {
//
//                        Toast.makeText(context, "Opps! no one is list", Toast.LENGTH_SHORT).show();
//                    }
//
//                }
//e
//                @Override
//                public void onFailure(Call<JsonObject> call, Throwable t) {
//
//                    Utility.getSharedInstance().dismissProgressDialog();
//                    Toast.makeText(context, "API Issue", Toast.LENGTH_SHORT).show();
//
//
//                }
//
//
//            });
//        } else {
//
//            Toast.makeText(context, "No internet connection available", Toast.LENGTH_SHORT).show();
//
//        }
//    }


    private void showMediaDialog(String url) {
        mediaDialog = new Dialog(context, android.R.style.Theme_Dialog);
        mediaDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        mediaDialog.setContentView(R.layout.dialog_media);
        mediaDialog.getWindow().getAttributes().width = ActionBar.LayoutParams.FILL_PARENT;
        mediaDialog.getWindow().setBackgroundDrawable(new ColorDrawable(context.getResources().getColor(R.color.blackTransnew)));
        WindowManager.LayoutParams wmlps = mediaDialog.getWindow().getAttributes();
        // mediaDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        Window window = mediaDialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        window.setGravity(Gravity.CENTER);
        ImageView imgWallPost = (ImageView) mediaDialog.findViewById(R.id.imageViewWallImage);


        Picasso.with(context)
                .load(Config.PIC_URL_EVENT + Uri.encode(url))
                .placeholder(R.drawable.bg_default_album_art)
                .error(R.drawable.bg_default_album_art)
                .into(imgWallPost);


        mediaDialog.show();
    }
    // VideoView myVideoView = (VideoView) mediaDialog.findViewById(R.id.videoView1);


    public String setDateToTextview(String Date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        String reportDate = null;
        try {
            date = format.parse(Date);
            DateFormat dff = new SimpleDateFormat("dd-MMM-yyyy");
            reportDate = dff.format(date);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        // strDateArray[k]=String.valueOf(date.sub);
        return reportDate;
    }


}


