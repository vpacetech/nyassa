package com.music.nyasaa.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.music.nyasaa.R;
import com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity;
import com.music.nyasaa.models.SongDetail;
import com.music.nyasaa.retrofit.Config;
import com.music.nyasaa.utility.SharedPreferenceManager;
import com.squareup.picasso.Picasso;

import java.util.List;

public class GetAllDiscouresAdapter extends RecyclerView.Adapter<GetAllDiscouresAdapter.HeaderHolder> {

    LayoutInflater inflater;
    boolean header = false;
    String sDrawerID;
    Context context;

    private List<SongDetail> menuListArrayList;
    int lastPosition = -1;
    private boolean subListFlagSong = false;
    private boolean subListFlag = false;

    public FragmentManager f_manager;
    private String fragVal = "";
    private DashBoardActivity dashBoardActivity;


    public GetAllDiscouresAdapter(Context context, List<SongDetail> strings) {


        this.context = context;
        this.menuListArrayList = strings;


    }


    @Override
    public GetAllDiscouresAdapter.HeaderHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_items_media_type, parent, false);

        return new GetAllDiscouresAdapter.HeaderHolder(itemView);


        // return new HeaderHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.chapter_item, null));
    }

    @Override
    public void onBindViewHolder(final GetAllDiscouresAdapter.HeaderHolder holder, final int position) {


        holder.txtTitle.setText(menuListArrayList.get(position).getName());

String drawer=SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_DRAWER_TYPE,"0");
        if(drawer.equalsIgnoreCase("1")||drawer.equalsIgnoreCase("3"))
        {

            Picasso.with(context)
                    .load(Config.PIC_URL+ "cover/" + Uri.encode(  menuListArrayList.get(position).getCover()))
                    .error(R.drawable.default_music)
                    .into(holder.imgMedia);
        }
        else {

            Picasso.with(context)
                    .load(Config.BASE_VIDEO_URL_COVER +""+Uri.encode(  menuListArrayList.get(position).getCover()))
                    .error(R.drawable.default_music)
                    .into(holder.imgMedia);

        }


        holder.imgMedia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//
//
                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_DISCOURSE_AUDIO, menuListArrayList.get(position).getName());
//                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_CAT_ORIGINAL, menuListArrayList.get(position).getId());
//                sDrawerID = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_DRAWER_TYPE, "none");
//
//                if (sDrawerID.equalsIgnoreCase("1")) {

                String drawr= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_DRAWER_TYPE,"");
               if(drawr.equalsIgnoreCase("3"))
               {
                   ((DashBoardActivity)context).openAudioDiscourses();
               }
               else if(drawr.equalsIgnoreCase("4"))
               {
                   ((DashBoardActivity)context).openVidioDiscourses();
               }


//
//                } else if (sDrawerID.equalsIgnoreCase("2")) {
//                    ((DashBoardActivity) context).openVideoFragment();
//                }

            }
        });


        if (position > lastPosition) {

            Animation animation = AnimationUtils.loadAnimation(context,
                    R.anim.list_items);
            holder.itemView.startAnimation(animation);
            lastPosition = position;
        }


    }

    @Override
    public int getItemCount() {
        return menuListArrayList.size();
    }


    class HeaderHolder extends RecyclerView.ViewHolder {
        private TextView txtTitle;
        private LinearLayout mMenuLayout;
        private ImageView imgMedia;


        public HeaderHolder(View itemView) {
            super(itemView);

            //   imgWallaper = (ImageView) itemView.findViewById(R.id.imgWall);
            txtTitle = (TextView) itemView.findViewById(R.id.txtMediaType);
            mMenuLayout = (LinearLayout) itemView.findViewById(R.id.menulayout);
            imgMedia = (ImageView) itemView.findViewById(R.id.mediaImage);


        }


    }
}



