package com.music.nyasaa.adapter;

import android.content.Context;
import android.graphics.Bitmap.Config;
import android.net.Uri;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.music.nyasaa.R;
import com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity;
import com.music.nyasaa.manager.MediaController;
import com.music.nyasaa.models.SongDetail;
import com.music.nyasaa.utility.SharedPreferenceManager;
import com.music.nyasaa.utility.Utility;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions.Builder;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static com.music.nyasaa.fragments.FragmentDashBoard.fragmentDashBoards;

import static com.music.nyasaa.fragments.FragmentDashBoard.mSongsMedi;
import static com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity.ForDOWNLOadSongDetail;

public class DashboardMeditationAdapter extends Adapter<DashboardMeditationAdapter.ViewHolder> {
    Context context;
   // ArrayList<SongDetail> mSongsList;
    private DisplayImageOptions options;
    int lastPosition = -1;
    // private ProgressDialog downloadProgressDialog;
    //PowerManager.WakeLock mWakeLock;

    RotateAnimation rotate = new RotateAnimation(
            0, 360,
            Animation.RELATIVE_TO_SELF, 0.5f,
            Animation.RELATIVE_TO_SELF, 0.5f
    );

    public DashboardMeditationAdapter(Context context, ArrayList<SongDetail> mSongsList) {
        this.context = context;
      //  this.mSongsList = mSongsList;
        options = new Builder().showImageOnLoading(R.drawable.bg_default_album_art).showImageForEmptyUri((int) R.drawable.bg_default_album_art).showImageOnFail((int) R.drawable.bg_default_album_art).cacheInMemory(true).cacheOnDisk(true).considerExifParams(true).bitmapConfig(Config.RGB_565).build();

//        downloadProgressDialog = new ProgressDialog(context);
//        downloadProgressDialog.setMessage("Downloading in progress...");
//        downloadProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
//        downloadProgressDialog.setProgress(0);
    }

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.song_list_item, parent, false));
    }

    public void onBindViewHolder(final ViewHolder holder, final int position) {


        final SongDetail mDetail = mSongsMedi.get(position);
      //  ForDOWNLOadSongDetail = mSongsMedi.get(position);
        holder.songTitle.setText(mDetail.getName());
        Picasso.with(context)
                .load(com.music.nyasaa.retrofit.Config.PIC_URL + "cover/" + Uri.encode( mDetail.getCover()))
                .placeholder(R.drawable.default_music)
                .error(R.drawable.default_music)
                .into(holder.cover);

        if (mSongsMedi.get(position).getPlayStop()) {

            holder.btn_song_play.setImageResource(R.drawable.ic_pause);
        } else {
            holder.btn_song_play.setImageResource(R.drawable.ic_home_play);
        }

        holder.btn_song_play.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {

                ForDOWNLOadSongDetail = mSongsMedi.get(position);

                fragmentDashBoards.playPoaseMidition(position);
                DashBoardActivity.context.positionsofDashbord = 5;
                holder.loadingSong.setVisibility(View.GONE);

                if (mSongsMedi.get(position).getPlayStop()) {
                    DashBoardActivity.context. setStatus(true);
                DashBoardActivity.context.setPlayer(false);
                DashBoardActivity.context.onResume();
                    SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_TRACK_TYPE,"2");
                    SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_TRACK_ID,String.valueOf(mDetail.getId()));
                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_MUSIC_MODE, "meditation");
                DashBoardActivity.context.loadSongsDetails(mDetail);
                    DashBoardActivity.context.loadSongsDetails(mSongsMedi,position);

//                if (mDetail.getType().equals("html")) {
//                    LyricsActivity activity = new LyricsActivity();
//                    Bundle bundle = new Bundle();
//                    bundle.putString("title", mDetail.getTitle());
//                    bundle.putString("lyrics", mDetail.getLyrics());
//                    activity.setArguments(bundle);
//                    DashBoardActivity.context.addFragmentOnTop(activity);
//                    return;
//                }
                //new DownloadMp3CacheTask().execute(mDetail.getUrl(),mDetail.display_name);


                //this will cancel the timer of the system



                    if (!MediaController.getInstance().isPlayingAudio(mDetail) || MediaController.getInstance().isAudioPaused()) {
                        MediaController.getInstance().stopAudio();
                        MediaController.getInstance().playMUSIC(mDetail);
                        holder.loadingSong.setVisibility(View.GONE);
                        ForDOWNLOadSongDetail = mDetail;

                    }


//                    rotate.setDuration(900);
//                    rotate.setRepeatCount(Animation.INFINITE);
//                    itemImage.startAnimation(rotate);
                    // MediaController.getInstance().setPlaylist(mSongsList, mDetail, PhoneMediaControl.SonLoadFor.MostPlay.ordinal(), -1);
                    else {
                        MediaController.getInstance().pauseAudio(mDetail);
                        Utility.getSharedInstance().dismissProgressDialog();
                    }


                   /* if (MusicSQLiteHelper.isExistQueueSong(mDetail)) {
                        MusicSQLiteHelper.updateQueue(mDetail);
                    } else {
                        MusicSQLiteHelper.insertSongInQueue(mDetail);
                    }*/
                }
                else{
                    ForDOWNLOadSongDetail = mDetail;
                    DashBoardActivity.context. setStatus(false);
                    DashBoardActivity.context.setPlayer(true);
                    if (!MediaController.getInstance().isPlayingAudio(mDetail) || MediaController.getInstance().isAudioPaused()) {
                        MediaController.getInstance().stopAudio();
                        MediaController.getInstance().pauseAudio(mDetail);
                        DashBoardActivity.stop_rotateImage();
                        // MediaController.getInstance().playAudio(mDetail);
                        //  holder.loadingSong.setVisibility(View.GONE);


//                    rotate.setDuration(900);
//                    rotate.setRepeatCount(Animation.INFINITE);
//                    itemImage.startAnimation(rotate);
                        // MediaController.getInstance().setPlaylist(mSongsList, mDetail, PhoneMediaControl.SonLoadFor.MostPlay.ordinal(), -1);
                    } else {
                        MediaController.getInstance().pauseAudio(mDetail);
                        DashBoardActivity.stop_rotateImage();
                    }

                }
            }

        });


        if (position > lastPosition) {

            Animation animation = AnimationUtils.loadAnimation(context,
                    R.anim.list_items);
            holder.itemView.startAnimation(animation);
            lastPosition = position;
        }
    }

    public int getItemCount() {
        int size=0;

        if(mSongsMedi!=null)
        {

            if(mSongsMedi.size()>10)
            {
                size=10;
            }
            else {
                size=mSongsMedi.size();
            }
        }

        return size;
    }

    public class ViewHolder extends android.support.v7.widget.RecyclerView.ViewHolder {
        ImageView btn_song_play;
        ImageView cover;
        TextView songTitle;
        ProgressBar loadingSong;

        public ViewHolder(View itemView) {
            super(itemView);
            songTitle = (TextView) itemView.findViewById(R.id.songTitle);
            btn_song_play = (ImageView) itemView.findViewById(R.id.btn_song_play);
            cover = (ImageView) itemView.findViewById(R.id.songImage);
            loadingSong = (ProgressBar) itemView.findViewById(R.id.loadingSong);
        }
    }

  /*  private class DownloadMp3CacheTask extends AsyncTask<String,Integer,String>{

        @Override
        protected void onPreExecute() {
            PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                    getClass().getName());
            mWakeLock.acquire();
            downloadProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String wallpaperURLStr = params[0];
            String localFileName = params[1];
            try {
                URL wallpaperURL = new URL(wallpaperURLStr);
                int filesize = wallpaperURL.openConnection().getContentLength();
                if (filesize < 0) {
                   downloadProgressDialog.setMax(1000000);
                } else {
                   downloadProgressDialog.setMax(filesize);
                }
                InputStream inputStream = new BufferedInputStream(wallpaperURL.openStream(), 10240);
                FileOutputStream outputStream = new FileOutputStream(new File(SongCacheUtil.instance().getCacheFolder(context), localFileName));
                byte[] buffer = new byte[1024];
                int loadedSize = 0;
                while (true) {
                    int dataSize = inputStream.read(buffer);
                    if (dataSize == -1) {
                        break;
                    }
                    loadedSize += dataSize;
                    publishProgress(loadedSize);
                    outputStream.write(buffer, 0, dataSize);
                }
                outputStream.close();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
            return localFileName;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            downloadProgressDialog.setProgress(values[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            mWakeLock.release();
            downloadProgressDialog.hide();

        }


    }*/
}
