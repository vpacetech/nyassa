package com.music.nyasaa.adapter;



import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.music.nyasaa.R;
import com.music.nyasaa.models.ResponseModel.GetMyPlayList.GetMyPlayListResponseData;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MyPlayListAdapters extends RecyclerView.Adapter<MyPlayListAdapters.HeaderHolder> {

    LayoutInflater inflater;
    boolean header = false;

    Context context;
    private String[] name;
    private Integer[] img;
    private int lastPosition = -1;

    private List<GetMyPlayListResponseData> getMyPlayListResponseData;


    public MyPlayListAdapters(Context context, List<GetMyPlayListResponseData> getMyPlayListResponseData) {

        this.context = context;
        this.getMyPlayListResponseData = getMyPlayListResponseData;


    }


    @Override
    public MyPlayListAdapters.HeaderHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_dialog_playlist, parent, false);

        return new MyPlayListAdapters.HeaderHolder(itemView);


        // return new HeaderHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.chapter_item, null));
    }

    @Override
    public void onBindViewHolder(MyPlayListAdapters.HeaderHolder holder, int position) {


//        ..holder.imgWallaper.setImageResource(img[position]);
//
//        Picasso.with(context)
//                .load(Config.PIC_URL+getMyPlayListResponseData.get(position).get())
//                .placeholder(R.drawable.disc)
//                .error(R.drawable.disc)
//                .into(holder.imgWallaper);

        holder.txtTitle.setText(getMyPlayListResponseData.get(position).getName());
        holder.mtxtDes.setText("Playlist");
        holder.txtTitle.setVisibility(View.VISIBLE);

        Picasso.with(context)
                .load(com.music.nyasaa.retrofit.Config.PIC_URL + "cover/"+ Uri.encode( getMyPlayListResponseData.get(position).getName()))
                .placeholder(R.drawable.default_music)
                .error(R.drawable.default_music)
                .into(holder.imgWallaper);

        holder.layoutPlayList.setBackgroundColor(context.getResources().getColor(R.color.md_black_1000));


//        if (position > lastPosition) {
//
//            Animation animation = AnimationUtils.loadAnimation(context,
//                    R.anim.list_items);
//            holder.itemView.startAnimation(animation);
//            lastPosition = position;
//        }


    }

    @Override
    public int getItemCount() {
        return getMyPlayListResponseData.size();
    }

    public void removeItem(int position) {
        getMyPlayListResponseData.remove(position);
        // notify the item removed by position
        // to perform recycler view delete animations
        // NOTE: don't call notifyDataSetChanged()
        notifyItemRemoved(position);
    }



    public class HeaderHolder extends RecyclerView.ViewHolder {
        private TextView txtTitle,mtxtDes;
        public RelativeLayout layoutPlayList;
        private ImageView imgWallaper;


        public HeaderHolder(View itemView) {
            super(itemView);

            imgWallaper = (ImageView) itemView.findViewById(R.id.playlsitImage);
            txtTitle = (TextView) itemView.findViewById(R.id.txtPlayListName);
            mtxtDes = (TextView) itemView.findViewById(R.id.description);
            layoutPlayList = (RelativeLayout) itemView.findViewById(R.id.layoutPlayList);


        }


    }


}



