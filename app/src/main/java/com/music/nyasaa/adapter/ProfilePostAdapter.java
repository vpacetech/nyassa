package com.music.nyasaa.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.music.nyasaa.R;
import com.music.nyasaa.front_end_layer.activities.profile.ScrollingProfileActivity;

public class ProfilePostAdapter  extends RecyclerView.Adapter<ProfilePostAdapter.HeaderHolder> {

    LayoutInflater inflater;
    boolean header = false;

    Context context;
    private String[] name;
    private Integer[] img;
    private   int lastPosition = -1;


    public ProfilePostAdapter(Context context, Integer[] img, String[] name) {

        this.context = context;
        this.img = img;
        this.name = name;


    }

    public ProfilePostAdapter(ScrollingProfileActivity scrollingProfileActivity, String[] name) {

        this.context = scrollingProfileActivity;

        this.name = name;
    }


    @Override
    public ProfilePostAdapter.HeaderHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_items_profile_post, parent, false);

        return new ProfilePostAdapter.HeaderHolder(itemView);


        // return new HeaderHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.chapter_item, null));
    }

    @Override
    public void onBindViewHolder(ProfilePostAdapter.HeaderHolder holder, int position) {





        holder.txtTitle.setText(name[position]);
        holder.txtTitle.setVisibility(View.VISIBLE);

//        holder.imgWallaper.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(context, ProfileActivity.class);
//                context.startActivity(intent);
//                ((DashBoardActivity)context).overridePendingTransition(R.anim.left,R.anim.right);
//            }
//        });

        if (position > lastPosition) {

            Animation animation = AnimationUtils.loadAnimation(context,
                    R.anim.list_items);
            holder.itemView.startAnimation(animation);
            lastPosition = position;
        }




    }

    @Override
    public int getItemCount() {
        return 2 ;
    }


    class HeaderHolder extends RecyclerView.ViewHolder {
        private TextView txtTitle;
        private LinearLayout mNotificationLayout;
        private ImageView imgWallaper;


        public HeaderHolder(View itemView) {
            super(itemView);

            imgWallaper = (ImageView) itemView.findViewById(R.id.imgWall);
            txtTitle = (TextView) itemView.findViewById(R.id.id_txt_title);


        }


    }
}


