package com.music.nyasaa.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.music.nyasaa.R;

import com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity;
import com.music.nyasaa.manager.MediaController;
import com.music.nyasaa.models.SongDetail;
import com.music.nyasaa.phonemidea.PhoneMediaControl;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

public class MyItemRecyclerViewAdapter extends RecyclerView.Adapter<MyItemRecyclerViewAdapter.ViewHolder> {

    private ArrayList<SongDetail> mList;
    Context context;
    private DisplayImageOptions options;
    private ImageLoader imageLoader = ImageLoader.getInstance();

    public MyItemRecyclerViewAdapter(ArrayList<SongDetail> mList, Context context) {
        this.mList = mList;
        this.context = context;
        this.options = new DisplayImageOptions.Builder().showImageOnLoading(R.drawable.bg_default_album_art)
                .showImageForEmptyUri(R.drawable.bg_default_album_art).showImageOnFail(R.drawable.bg_default_album_art).cacheInMemory(true)
                .cacheOnDisk(true).considerExifParams(true).bitmapConfig(Bitmap.Config.RGB_565).build();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.grid_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        SongDetail items = mList.get(position);
        holder.songTitle.setText(items.getTitle());

        try {

            String contentURI = "content://media/external/audio/media/" + items.getId() + "/albumart";
            imageLoader.displayImage(items.getUrl(), holder.cover, options);

        } catch (Exception e) {
            e.printStackTrace();
        }
        holder.iv_song_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SongDetail mDetail = mList.get(position);
                ((DashBoardActivity) context).loadSongsDetails(mDetail);

                if (mDetail != null) {
                    if (MediaController.getInstance().isPlayingAudio(mDetail) && !MediaController.getInstance().isAudioPaused()) {
                        MediaController.getInstance().pauseAudio(mDetail);
                    } else {
                        MediaController.getInstance().setPlaylist(mList, mDetail, PhoneMediaControl.SonLoadFor.All.ordinal(), -1);
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView songTitle;
        public final ImageView iv_song_play;
        public final ImageView cover;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            songTitle = (TextView) view.findViewById(R.id.gridTitle);
            cover = (ImageView) view.findViewById(R.id.coverIcon);
            iv_song_play = (ImageView) view.findViewById(R.id.iv_song_play);
        }
    }

}
