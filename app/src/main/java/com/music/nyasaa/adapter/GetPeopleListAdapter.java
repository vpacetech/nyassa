package com.music.nyasaa.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.music.nyasaa.R;
import com.music.nyasaa.models.ResponseModel.GetEventPeople.GetEventPeopleResponseData;
import com.music.nyasaa.retrofit.Config;
import com.squareup.picasso.Picasso;

import java.util.List;

public class GetPeopleListAdapter  extends RecyclerView.Adapter<GetPeopleListAdapter.HeaderHolder> {

    LayoutInflater inflater;
    boolean header = false;

    Context context;
    // private String[] name;
    // private Integer[] img;

    // songDetail;


    List<GetEventPeopleResponseData> getEventPeopleResponseData;
    private int lastPosition = -1;


    public GetPeopleListAdapter(Context context, List<GetEventPeopleResponseData> getEventPeopleResponseData) {

        this.context = context;
        this.getEventPeopleResponseData=getEventPeopleResponseData;
        // this.img = img;
        // this.name = name;



    }



    @Override
    public GetPeopleListAdapter.HeaderHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_people_list, parent, false);

        return new GetPeopleListAdapter.HeaderHolder(itemView);


        // return new HeaderHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.chapter_item, null));
    }

    @Override
    public void onBindViewHolder(final GetPeopleListAdapter.HeaderHolder holder, final int position) {




        Picasso.with(context)
                .load(Config.BASE_PROFILE_PIC + Uri.encode( getEventPeopleResponseData.get(position).getProfilePhoto()))
                .placeholder(R.drawable.bg_default_album_art)
                .error(R.drawable.bg_default_album_art)
                .into(holder.imgWallaper);

        holder.txtTitle.setText(getEventPeopleResponseData.get(position).getUsername());
        holder.btn_play.setVisibility(View.GONE);
        holder.btnDownload.setVisibility(View.GONE);
        holder.mTtxType.setText(getEventPeopleResponseData.get(position).getName());








    }






    @Override
    public int getItemCount() {

        int size=0;

        if(getEventPeopleResponseData!=null)
        {

            size=getEventPeopleResponseData.size();
        }
        return size ;
    }


    class HeaderHolder extends RecyclerView.ViewHolder {
        private TextView txtTitle,mTtxType;
        private LinearLayout layoutPlayList;
        private ImageView imgWallaper, btn_play,btnDownload;


        public HeaderHolder(View itemView) {
            super(itemView);

            imgWallaper = (ImageView) itemView.findViewById(R.id.playlsitImage);
            btn_play = (ImageView) itemView.findViewById(R.id.btn_play);
            txtTitle = (TextView) itemView.findViewById(R.id.txtPlayListName);
            mTtxType = (TextView) itemView.findViewById(R.id.txtTypeSOng);
            btnDownload=(ImageView) itemView.findViewById(R.id.btnDownload);

            layoutPlayList = (LinearLayout) itemView.findViewById(R.id.layoutPlayList);


        }

    }
    }