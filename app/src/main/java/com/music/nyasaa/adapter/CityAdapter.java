package com.music.nyasaa.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.music.nyasaa.R;
import com.music.nyasaa.models.OshoCenterDAO;

import java.util.ArrayList;
import java.util.List;

public class CityAdapter extends BaseAdapter implements Filterable {

    private Context context;
    private List<OshoCenterDAO> getCityList;
    private List<OshoCenterDAO> suggestions=new ArrayList<>();
    //private TextView txtSpinName;
    private Filter filter = new CustomFilter();
    //private AutoCompleteTextView atxtBookName;

    public CityAdapter(Context context, List<OshoCenterDAO> data)
    {
        this.getCityList = data;
        this.context = context;
    }

    @Override
    public int getCount()
    {
        return suggestions.size();
    }

    @Override
    public OshoCenterDAO getItem(int i) {
        return suggestions.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup)
    {
        ViewHolder holder;

        if (view == null)
        {
            view = LayoutInflater.from(context).
                    inflate(R.layout.list_items_spinners, viewGroup, false);

            holder = new ViewHolder();
            holder.BookNo = ((TextView) view.findViewById(R.id.spin_txt));
            ImageView img_spin = (ImageView)
                    view.findViewById(R.id.spin_img);

            view.setTag(holder);
        }

        else
        {
            holder = (ViewHolder) view.getTag();
        }
        holder.BookNo.setText(suggestions.get(i).getName());
        //txtSpinName.setText(getCityList.get(i).getBookNo());
        return view;
    }


    private static class ViewHolder {
        TextView BookNo;
    }

    @Override
    public Filter getFilter() {
        return filter;
    }


    private class CustomFilter extends Filter
    {
        @Override
        protected FilterResults performFiltering(CharSequence constraint)
        {
            suggestions.clear();
            // Check if the Original List and Constraint aren't null.
            if (getCityList != null && constraint != null)
            {
                for (int i = 0; i < getCityList.size(); i++) {
                    // Compare item in original list if it contains constraints.
                    if (getCityList.get(i).getName().toLowerCase().contains(constraint))
                    {
                        // If TRUE add item in Suggestions.
                        suggestions.add(getCityList.get(i));
                    }
                }
            }
            // Create new Filter Results and return this to publishResults;
            FilterResults results = new FilterResults();
            results.values = suggestions;
            results.count = suggestions.size();

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (results.count > 0) {
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }
    }
}

