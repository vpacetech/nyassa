package com.music.nyasaa.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.music.nyasaa.R;
import com.music.nyasaa.fragments.community.FragmentCommunityDetailsMem;
import com.music.nyasaa.models.RequestModel.GetCommunityRequest.GetRequestSentbyCommunityData;
import com.music.nyasaa.models.ResponseModel.getCommunity.GetCommunityResponseData;
import com.music.nyasaa.retrofit.Config;
import com.music.nyasaa.utility.SharedPreferenceManager;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class CommunityRequestAdapter extends RecyclerView.Adapter<CommunityRequestAdapter.HeaderHolder> {

    LayoutInflater inflater;
    boolean header = false;
    private Dialog mediaDialog;
    Context context;
    private List<GetRequestSentbyCommunityData> community_dao;
    private FragmentCommunityDetailsMem fragmentCommunityDetails;
    private int lastPosition = -1;
    private List<GetCommunityResponseData> sortedDAO_Data;
    int cnt = 0;
    String sComID = "";


    public CommunityRequestAdapter(Context context, List<GetRequestSentbyCommunityData> getCommunityResponseData, FragmentCommunityDetailsMem fragmentCommunityDetails) {

        this.context = context;
        this.fragmentCommunityDetails = fragmentCommunityDetails;
//        this.img = img;
//        this.name = name;

        this.community_dao = getCommunityResponseData;
        sComID = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_COMM_ID, "none");
    }


    @Override
    public CommunityRequestAdapter.HeaderHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_items_community_request, parent, false);

        return new CommunityRequestAdapter.HeaderHolder(itemView);


        // return new HeaderHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.chapter_item, null));
    }

    @Override
    public void onBindViewHolder(CommunityRequestAdapter.HeaderHolder holder, final int position) {


        sortedDAO_Data = new ArrayList<>();

        // use pisco and check null url :'

        // holder.imgWallaper.setImageResource(community_dao.get(position).getImage_url());

        if (community_dao != null)
            if (community_dao.size() > 0) {


                holder.linearLayout.setVisibility(View.VISIBLE);
                holder.txtTitle.setText(community_dao.get(position).getCommunityName());
                holder.txtTitle.setVisibility(View.VISIBLE);
                holder.txtLongInfo.setText(community_dao.get(position).getUsername());

                Picasso.with(context)
                        .load(Config.BASE_PROFILE_PIC + Uri.encode(community_dao.get(position).getProfilePhoto()))
                        .placeholder(R.drawable.bg_default_album_art)
                        .error(R.drawable.bg_default_album_art)
                        .into(holder.imgparentImg);

                holder.mButStatus.setVisibility(View.VISIBLE);
                String action = (community_dao.get(position).getAction());

                if (action.equalsIgnoreCase("1")) {
                    holder.mButStatus.setText("Pending");
                    holder.mButStatus.setBackgroundResource(R.drawable.but_pending);
                    holder.mButStatus.setTextColor(context.getResources().getColor(R.color.black));
                } else if (action.equalsIgnoreCase("2")) {

                    holder.mButStatus.setText("Accepted");
                    holder.mButStatus.setBackgroundResource(R.drawable.but_accept);
                    holder.mButStatus.setTextColor(context.getResources().getColor(R.color.white));

                } else if (action.equalsIgnoreCase("3")) {

                    holder.mButStatus.setText("Rejected");
                    holder.mButStatus.setTextColor(context.getResources().getColor(R.color.white));
                    holder.mButStatus.setBackgroundResource(R.drawable.but_reject_red);

                } else {
                    holder.mButStatus.setText("Status");
                }


                holder.imgparentImg.setVisibility(View.VISIBLE);

                if (community_dao.get(position).getRequest().equalsIgnoreCase("1") && community_dao.get(position).getAction().equalsIgnoreCase("1")) {

                    holder.mButStatus.setVisibility(View.VISIBLE);
                    holder.mButAccept.setVisibility(View.GONE);
                    holder.mButReject.setVisibility(View.GONE);

                } else if (community_dao.get(position).getRequest().equalsIgnoreCase("2") && community_dao.get(position).getAction().equalsIgnoreCase("1")) {
                    holder.mButStatus.setVisibility(View.GONE);
                    holder.mButAccept.setVisibility(View.VISIBLE);
                    holder.mButReject.setVisibility(View.VISIBLE);

                    holder.mButAccept.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            fragmentCommunityDetails.acceptOrRejectRequest(sComID, "2", community_dao.get(position).getUserId());
                        }
                    });

                    holder.mButReject.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            fragmentCommunityDetails.acceptOrRejectRequest(sComID, "3", community_dao.get(position).getUserId());
                        }
                    });
                } else {

                    holder.mButAccept.setVisibility(View.GONE);
                    holder.mButReject.setVisibility(View.GONE);
                    holder.mButStatus.setVisibility(View.VISIBLE);

                }


            }


//        Toast.makeText(context,"size:"+sortedDAO_Data.size(),Toast.LENGTH_SHORT).show();


        if (position > lastPosition) {

            Animation animation = AnimationUtils.loadAnimation(context,
                    R.anim.list_items);
            holder.itemView.startAnimation(animation);
            lastPosition = position;
        }

        holder.imgparentImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMediaDialog(community_dao.get(position).getProfilePhoto());
            }
        });
    }

    @Override
    public int getItemCount() {
        return community_dao.size();
    }


    class HeaderHolder extends RecyclerView.ViewHolder {
        private TextView txtTitle, txtLongInfo;
        private LinearLayout linearLayout;
        private ImageView imgWallaper, imgparentImg;
        private Button mButAccept, mButReject, mButStatus;


        public HeaderHolder(View itemView) {
            super(itemView);


            txtLongInfo = (TextView) itemView.findViewById(R.id.txtCommInfo);
            linearLayout = (LinearLayout) itemView.findViewById(R.id.layoutPlayList);
            txtTitle = (TextView) itemView.findViewById(R.id.txtCommunityName);
            imgparentImg = (ImageView) itemView.findViewById(R.id.playlsitImage);

            mButAccept = (Button) itemView.findViewById(R.id.butAccept);

            mButStatus = (Button) itemView.findViewById(R.id.butStatus);
            mButReject = (Button) itemView.findViewById(R.id.butReject);


        }
    }

    private void showMediaDialog(String url) {
        mediaDialog = new Dialog(context, android.R.style.Theme_Dialog);
        mediaDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        mediaDialog.setContentView(R.layout.dialog_media);
        mediaDialog.getWindow().getAttributes().width = ActionBar.LayoutParams.FILL_PARENT;
        mediaDialog.getWindow().setBackgroundDrawable(new ColorDrawable(context.getResources().getColor(R.color.blackTransnew)));
        WindowManager.LayoutParams wmlps = mediaDialog.getWindow().getAttributes();
        // mediaDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        Window window = mediaDialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        window.setGravity(Gravity.CENTER);
        ImageView imgWallPost = (ImageView) mediaDialog.findViewById(R.id.imageViewWallImage);


        Picasso.with(context)
                .load(Config.BASE_PROFILE_PIC + Uri.encode(url))
                .placeholder(R.drawable.bg_default_album_art)
                .error(R.drawable.bg_default_album_art)
                .into(imgWallPost);


        mediaDialog.show();
    }

}