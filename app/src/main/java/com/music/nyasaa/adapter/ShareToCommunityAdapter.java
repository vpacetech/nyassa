package com.music.nyasaa.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.music.nyasaa.R;
import com.music.nyasaa.models.ResponseModel.getCommunity.GetCommunityResponseData;
import com.music.nyasaa.retrofit.Config;
import com.music.nyasaa.utility.SharedPreferenceManager;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ShareToCommunityAdapter extends RecyclerView.Adapter<ShareToCommunityAdapter.HeaderHolder> {

    LayoutInflater inflater;
    boolean header = false;

    Context context;

    private List<GetCommunityResponseData> community_dao;

    private int lastPosition = -1;


    public ShareToCommunityAdapter(Context context, List<GetCommunityResponseData> Community_DAO) {

        this.context = context;
//        this.img = img;
//        this.name = name;

        this.community_dao = Community_DAO;


    }


    @Override
    public ShareToCommunityAdapter.HeaderHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_community_share, parent, false);

        return new ShareToCommunityAdapter.HeaderHolder(itemView);


        // return new HeaderHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.chapter_item, null));
    }

    @Override
    public void onBindViewHolder(ShareToCommunityAdapter.HeaderHolder holder, final int position) {


        // use pisco and check null url :'

        // holder.imgWallaper.setImageResource(community_dao.get(position).getImage_url());

        if (community_dao != null)
            if (community_dao.size() > 0) {


                holder.txtTitle.setText(community_dao.get(position).getCommunityName());
                holder.txtTitle.setVisibility(View.VISIBLE);

                String userID = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "none");


                Picasso.with(context)
                        .load(Config.PIC_URL_COMMUNITY + Uri.encode(  community_dao.get(position).getImageUrl()))
                        .placeholder(R.drawable.bg_default_album_art)
                        .error(R.drawable.bg_default_album_art)
                        .into(holder.imgWallaper);
//


            }


        holder.mTxtType.setVisibility(View.GONE);
        holder.mButDownload.setVisibility(View.GONE);
        holder.mButPlay.setVisibility(View.GONE);
//        holder.txtTitle.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(context, "Text", Toast.LENGTH_SHORT).show();
//            }
//        });


//        if (position > lastPosition) {
//
//            Animation animation = AnimationUtils.loadAnimation(context,
//                    R.anim.list_items);
//            holder.itemView.startAnimation(animation);
//            lastPosition = position;
//        }

    }

    @Override
    public int getItemCount() {
        return community_dao.size();
    }


    class HeaderHolder extends RecyclerView.ViewHolder {
        private TextView txtTitle, mTxtType;
        private RelativeLayout relativelayout;
        private ImageView imgWallaper, imgparentImg;
        private ImageView mButDownload, mButPlay;


        public HeaderHolder(View itemView) {
            super(itemView);

            imgWallaper = (ImageView) itemView.findViewById(R.id.playlsitImage);
            txtTitle = (TextView) itemView.findViewById(R.id.txtPlayListName);
            mTxtType = (TextView) itemView.findViewById(R.id.txtTypeSOng);

            mButDownload = (ImageView) itemView.findViewById(R.id.btnDownload);
            mButPlay = (ImageView) itemView.findViewById(R.id.btn_play
            );


        }


    }
}


