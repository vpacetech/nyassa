package com.music.nyasaa.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.music.nyasaa.R;
import com.music.nyasaa.fragments.FragmentAlbumList;
import com.music.nyasaa.models.ResponseModel.GetAlbumsListData;
import com.music.nyasaa.utility.SharedPreferenceManager;
import com.squareup.picasso.Picasso;

import java.util.List;

public class GetAlbumListAdapter extends RecyclerView.Adapter<GetAlbumListAdapter.HeaderHolder> {

    LayoutInflater inflater;
    boolean header = false;
    List<GetAlbumsListData> getAlbumsListDataArrayList;
    Context context;
    FragmentAlbumList fragmentAlbumList;
    FragmentManager fragmentManager;
    // private String[] name;
    // private Integer[] img;

    // songDetail;


    private int lastPosition = -1;

    public GetAlbumListAdapter(FragmentAlbumList activity, List<GetAlbumsListData> getAlbumsListData) {

        this.fragmentAlbumList = activity;
        this.getAlbumsListDataArrayList = getAlbumsListData;

    }


    @Override
    public GetAlbumListAdapter.HeaderHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_album, parent, false);

        return new GetAlbumListAdapter.HeaderHolder(itemView);


        // return new HeaderHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.chapter_item, null));
    }

    @Override
    public void onBindViewHolder(final GetAlbumListAdapter.HeaderHolder holder, final int position) {


//        holder.mTxtDate.setText("" + getAlbumsListDataArrayList.get(position).getCreatedOn());
        holder.mTxtAlbumName.setText("" + getAlbumsListDataArrayList.get(position).getTitle());


        try {


            String Url = getAlbumsListDataArrayList.get(position).getImage_url() + "/";

            Picasso.with(context)
                    .load(Url + Uri.encode(getAlbumsListDataArrayList.get(position).getImage()))
                    .placeholder(R.drawable.default_music)
                    .error(R.drawable.default_music)
                    .into(holder.imgWallaper);
        } catch (Exception e) {
            e.printStackTrace();
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_LANG_ID, getAlbumsListDataArrayList.get(position).getId());


                fragmentAlbumList.callMysongs();
            }
        });

        holder.btnDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_LANG_ID, getAlbumsListDataArrayList.get(position).getId());


                fragmentAlbumList.downloadAllSongsInAlbum();
            }
        });

//
//        if (position > lastPosition) {
//
//            Animation animation = AnimationUtils.loadAnimation(context,
//                    R.anim.list_items);
//            holder.itemView.startAnimation(animation);
//            lastPosition = position;
//        }


    }


    @Override
    public int getItemCount() {


        return getAlbumsListDataArrayList.size();
    }


    class HeaderHolder extends RecyclerView.ViewHolder {

        private ImageView imgWallaper, btnDownload;
        private TextView mTxtAlbumName, mTxtDate;


        public HeaderHolder(View itemView) {
            super(itemView);

            imgWallaper = (ImageView) itemView.findViewById(R.id.imgAbum);
            btnDownload = (ImageView) itemView.findViewById(R.id.btnDownload);

            mTxtAlbumName = itemView.findViewById(R.id.txtAlbumName);
//            mTxtDate = itemView.findViewById(R.id.txtAlbumDate);


        }

    }
}