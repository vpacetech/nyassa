package com.music.nyasaa.adapter;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.net.Uri;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.music.nyasaa.R;

import com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity;
import com.music.nyasaa.manager.MediaController;
import com.music.nyasaa.models.SongDetail;
import com.music.nyasaa.utility.SharedPreferenceManager;
import com.music.nyasaa.utility.Utility;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions.Builder;
import com.squareup.picasso.Picasso;

import java.io.InputStream;
import java.util.ArrayList;

import static com.music.nyasaa.fragments.FragmentDashBoard.fragmentDashBoards;

import static com.music.nyasaa.fragments.FragmentDashBoard.mSongsAudio;
import static com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity.ForDOWNLOadSongDetail;
import static com.music.nyasaa.retrofit.Config.BASE_URL_FOR_PLAY;


public class DashboardAudioAdapter extends Adapter<DashboardAudioAdapter.ViewHolder> {
    Context context;
  //  ArrayList<SongDetail> mSongsList;
    private DisplayImageOptions options;
    int lastPosition = -1;
    private ColorStateList colorPlay;
    private ColorStateList colorPause;
    private Bitmap bmp;
    private InputStream in;
    // private ProgressDialog downloadProgressDialog;
    //PowerManager.WakeLock mWakeLock;

    RotateAnimation rotate = new RotateAnimation(
            0, 360,
            Animation.RELATIVE_TO_SELF, 0.5f,
            Animation.RELATIVE_TO_SELF, 0.5f
    );

    public DashboardAudioAdapter(Context context, ArrayList<SongDetail> mSongsList) {
        this.context = context;
       // this.mSongsList = mSongsList;
        options = new Builder().showImageOnLoading(R.drawable.default_music).showImageForEmptyUri((int) R.drawable.default_music).showImageOnFail((int) R.drawable.default_music).cacheInMemory(true).cacheOnDisk(true).considerExifParams(true).bitmapConfig(Config.RGB_565).build();

//        downloadProgressDialog = new ProgressDialog(context);
//        downloadProgressDialog.setMessage("Downloading in progress...");
//        downloadProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
//        downloadProgressDialog.setProgress(0);
    }

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.song_list_item, parent, false));
    }

    public void onBindViewHolder(final ViewHolder holder, final int position) {


        final SongDetail mDetail = mSongsAudio.get(position);
      //  ForDOWNLOadSongDetail = mSongsAudio.get(position);

         String url= Uri.encode( mDetail.getCover());
        holder.songTitle.setText(mDetail.getName());
        Picasso.with(context)
                .load(com.music.nyasaa.retrofit.Config.PIC_URL + url)
                .placeholder(R.drawable.default_music)
                .error(R.drawable.default_music)
                .into(holder.cover);
//
//        if (mSongsAudio.get(position).getPlayStop()) {
//
//            holder.btn_song_play.setImageResource(R.drawable.ic_pause);
//        } else {
//            holder.btn_song_play.setImageResource(R.drawable.ic_home_play);
//        }
        holder.imgProgress.setVisibility(View.GONE);
        holder.btn_song_play.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {


                try
                {

                    String url=mDetail.getTrack_url()+"/";

                    BASE_URL_FOR_PLAY=url;
                }
                catch (Exception e)
                {
e.printStackTrace();
                }



                fragmentDashBoards.playPoaseAudio(position);
                ForDOWNLOadSongDetail = mSongsAudio.get(position);

                DashBoardActivity.context. positionsofDashbord=1;

//                Utility.getSharedInstance().showProgressDialog(context);


                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_TRACK_TYPE,"1");
                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_TRACK_ID,String.valueOf(mDetail.getId()));
                    SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_MUSIC_MODE, "track");

                        holder.imgProgress.setVisibility(View.GONE);
                       // holder.btn_song_play.setVisibility(View.GONE);

                if (mSongsAudio.get(position).getPlayStop()) {

                    DashBoardActivity.context. setStatus(true);
                        DashBoardActivity.context.setPlayer(false);
                        DashBoardActivity.context.onResume();
                        SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_MUSIC_MODE, "track");



                        DashBoardActivity.context.loadSongsDetails(mDetail);
                        DashBoardActivity.context.loadSongsDetails(mSongsAudio,position);
                        holder.loadingSong.setVisibility(View.VISIBLE);


                    if (mDetail.getFile().contains(".mp3")) {
                        if (!MediaController.getInstance().isPlayingAudio(mDetail) || MediaController.getInstance().isAudioPaused()) {
                            //  MediaController.getInstance().stopAudio();
                            MediaController.getInstance().playMUSIC(mDetail);

                            holder.loadingSong.setVisibility(View.GONE);
                            ForDOWNLOadSongDetail = mDetail;

                            holder.imgProgress.setVisibility(View.GONE);
                            holder.btn_song_play.setVisibility(View.VISIBLE);


                        } else {
                            MediaController.getInstance().pauseAudio(mDetail);
                            Utility.getSharedInstance().dismissProgressDialog();
                        }

//
//                        if (MusicSQLiteHelper.isExistQueueSong(mDetail)) {
//                            MusicSQLiteHelper.updateQueue(mDetail);
//                        } else {
//                            MusicSQLiteHelper.insertSongInQueue(mDetail);
//                        }
                    } else {
                        Toast.makeText(context, "Invalid Mp3 file", Toast.LENGTH_SHORT).show();
                    }


//                if (mDetail.getType().equals("html")) {
//                    LyricsActivity activity = new LyricsActivity();
//                    Bundle bundle = new Bundle();
//                    bundle.putString("title", mDetail.getTitle());
//                    bundle.putString("lyrics", mDetail.getLyrics());
//                    activity.setArguments(bundle);
//                    DashBoardActivity.context.addFragmentOnTop(activity);
//                    return;
//                }
                    //new DownloadMp3CacheTask().execute(mDetail.getUrl(),mDetail.display_name);


                    //this will cancel the timer of the system

                }
                else
                {
                    ForDOWNLOadSongDetail = mDetail;
                    DashBoardActivity.context.setPlayer(true);
                    DashBoardActivity.context. setStatus(false);
                    if (!MediaController.getInstance().isPlayingAudio(mDetail) || MediaController.getInstance().isAudioPaused()) {
                        MediaController.getInstance().stopAudio();
                        MediaController.getInstance().pauseAudio(mDetail);
                        DashBoardActivity.stop_rotateImage();
                        // MediaController.getInstance().playAudio(mDetail);
                        //  holder.loadingSong.setVisibility(View.GONE);


//                    rotate.setDuration(900);
//                    rotate.setRepeatCount(Animation.INFINITE);
//                    itemImage.startAnimation(rotate);
                        // MediaController.getInstance().setPlaylist(mSongsList, mDetail, PhoneMediaControl.SonLoadFor.MostPlay.ordinal(), -1);
                    } else {
                        MediaController.getInstance().pauseAudio(mDetail);
                        DashBoardActivity.stop_rotateImage();
                    }
                }
            }
        });


        if (position > lastPosition) {

            Animation animation = AnimationUtils.loadAnimation(context,
                    R.anim.list_items);
            holder.itemView.startAnimation(animation);
            lastPosition = position;
        }
        holder.imgProgress.setVisibility(View.GONE);
    }

    public int getItemCount() {

        int size=0;

        if(mSongsAudio!=null)
        {

            if(mSongsAudio.size()>10)
            {
                size=10;
            }
            else {
                size=mSongsAudio.size();
            }
        }

        return size;
    }

    public class ViewHolder extends android.support.v7.widget.RecyclerView.ViewHolder {
        ImageView btn_song_play, imgProgress;
        ImageView cover;
        TextView songTitle;
        ProgressBar loadingSong;

        public ViewHolder(View itemView) {
            super(itemView);
            songTitle = (TextView) itemView.findViewById(R.id.songTitle);
            btn_song_play = (ImageView) itemView.findViewById(R.id.btn_song_play);
            cover = (ImageView) itemView.findViewById(R.id.songImage);
            loadingSong = (ProgressBar) itemView.findViewById(R.id.loadingSong);
            imgProgress = (ImageView) itemView.findViewById(R.id.progressImg);
        }
    }


}
