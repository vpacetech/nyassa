package com.music.nyasaa.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Environment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.music.nyasaa.R;
import com.music.nyasaa.fragments.downloads.FragmentDownloadSongList;
import com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity;
import com.music.nyasaa.front_end_layer.activities.easy_video_player.VideoPlayerActivity;
import com.music.nyasaa.manager.MediaController;
import com.music.nyasaa.models.ResponseModel.GetMyPlayList.GetMyPlayListResponseData;
import com.music.nyasaa.models.SongDetail;
import com.music.nyasaa.utility.SharedPreferenceManager;
import com.music.nyasaa.utility.Utility;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import static com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity.ForDOWNLOadSongDetail;
import static com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity.FromDownoadFile;
import static com.music.nyasaa.fragments.downloads.FragmentDownloadSongList.fragmentDownloadSongList;
import static com.music.nyasaa.fragments.downloads.FragmentDownloadSongList.songDetailDownloadList;
import static com.music.nyasaa.retrofit.Config.BASE_URL_FOR_PLAY;

public class DownloadPlayListAdapter extends RecyclerView.Adapter<DownloadPlayListAdapter.HeaderHolder> {

    LayoutInflater inflater;
    boolean header = false;

    Context context;
    // private String[] name;
    // private Integer[] img;

    // songDetail;

    //  List<SongDetail> songDetail;
    private   int lastPosition = -1;


    public DownloadPlayListAdapter(Context context, List<SongDetail> songDetail) {

        this.context = context;
        // this.img = img;
        // this.name = name;
        //this.songDetail=songDetail;


    }

    public DownloadPlayListAdapter(FragmentActivity activity, List<GetMyPlayListResponseData> getMyPlayListResponseData) {
    }


    @Override
    public DownloadPlayListAdapter.HeaderHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_items_downloads, parent, false);

        return new DownloadPlayListAdapter.HeaderHolder(itemView);


        // return new HeaderHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.chapter_item, null));
    }

    @Override
    public void onBindViewHolder(final DownloadPlayListAdapter.HeaderHolder holder, final int position) {
        final SongDetail mDetail=songDetailDownloadList.get(position);

        holder.mLinearShare.setVisibility(View.GONE);

        //holder.imgWallaper.setImageResource();
        File fileo = new File(context.getApplicationContext().getFilesDir(), songDetailDownloadList.get(position).getCover());
        String url=fileo.getAbsolutePath();

        File outputFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), songDetailDownloadList.get(position).getCover());
        // String url=outputFile.getAbsolutePath();

        try {


            if (!url.trim().equalsIgnoreCase("")) {
                Picasso.with(context).load(fileo).error(R.drawable.default_music).into(holder.imgWallaper);
            }


        }catch (Exception e)

        {

        }


        // for Image get COVER
        // for play decrpt  in temp file give url to player and delete it after finsh Activity

//        String newTitle = songDetailDownloadList.get(position).getFile().replace("%20"," ");
        String newTitle_name = songDetailDownloadList.get(position).getName().replace("%20"," ");
        holder.txtTitle.setText(newTitle_name);
        holder.txtTitle.setVisibility(View.VISIBLE);

        holder.layoutPlayList.setBackgroundColor(context.getResources().getColor(R.color.md_black_1000));
        holder.imgWallaper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });




        holder.btnDownload.setImageResource(R.drawable.ic_delete_white_24dp);

        holder.mTxtType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                // Showing Alert Message

                AlertDialog.Builder builder = new AlertDialog.Builder(
                        context,AlertDialog.THEME_HOLO_LIGHT);
                builder.setTitle("Do you want to delete File ?");
//                builder.setMessage("Name:"+songDetailDownloadList.get(position).getFile().replace("%20"," "));
                builder.setMessage("Name:"+songDetailDownloadList.get(position).getName().replace("%20"," "));
                builder.setInverseBackgroundForced(true);
                builder.setNegativeButton("NO",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                dialog.dismiss();

                            }
                        });
                builder.setPositiveButton("YES",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {


                                DashBoardActivity.context.deleteFileData(songDetailDownloadList.get(position));
                                songDetailDownloadList.remove(position);
                                notifyDataSetChanged();
                                dialog.dismiss();

                            }
                        });
                builder.show();

            }
        });



        if (songDetailDownloadList.get(position).getPlayStop()) {

            holder.btn_play.setImageResource(R.drawable.ic_pause);
        }
        else {
            holder.btn_play.setImageResource(R.drawable.play);
        }

        SongDetail songDetails=songDetailDownloadList.get(position);
        if(songDetails.getTypeOfSong().equals("ADO"))
        {
            holder.btn_play.setVisibility(View.VISIBLE);
        }
        else
        {
            holder.btn_play.setVisibility(View.VISIBLE);

        }


        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* fragmentDownloads.startLoader();
                SongDetail songDetails=songDetailDownloadList.get(position);

                fragmentDownloads.playPoase(position);


                if(songDetails.getTypeOfSong().equals("ADO"))
                {


                songDetails.setId("1");

                playSong( holder,songDetails,position);
                }else {
                    // Play VDO

                    FromDownoadFile=true;
                    Intent intent = new Intent(context, VideoPlayerActivity.class);
                    intent.putExtra("url",songDetails.getFile());
                    context.startActivity(intent);
                }*/


            }
        });

        holder.playLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentDownloadSongList.startLoader();
                SongDetail songDetails=songDetailDownloadList.get(position);

                fragmentDownloadSongList.playPoase(position);


                if(songDetails.getTypeOfSong().equals("ADO"))
                {


                    songDetails.setId("1");

                    playSong( holder,songDetails,position);
                }else {
                    // Play VDO

                    FromDownoadFile =true;
                    Intent intent = new Intent(context, VideoPlayerActivity.class);
                    intent.putExtra("url",songDetails.getFile());
                    context.startActivity(intent);
                }
            }
        });


        /*holder.playLayout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // holder.loadingSong.setVisibility(View.VISIBLE);
                // holder.loadingSong.setVisibility(View.VISIBLE);

                try
                {

                    String url=mDetail.getTrack_url()+"/";

                    BASE_URL_FOR_PLAY=url;
                }
                catch (Exception e)
                {
                    System.out.println(e.getMessage());
                }

                ForDOWNLOadSongDetail = songDetailDownloadList.get(position);
                fragmentDownloadSongList.playPoase(position);
                if (songDetailDownloadList.get(position).getPlayStop()) {
                    DashBoardActivity.context.setPlayer(false);
                    DashBoardActivity.context.onResume();
                    DashBoardActivity.context.setStatus(true);
                    SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_TRACK_TYPE, "1");
                    SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_TRACK_ID, String.valueOf(mDetail.getId()));

                    SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_MUSIC_MODE, "track");
                    DashBoardActivity.context.loadSongsDetails(mDetail);
                    DashBoardActivity.context.loadSongsDetails(songDetailDownloadList,position);
//
                    //new DownloadMp3CacheTask().execute(mDetail.getUrl(),mDetail.display_name);
                    if (!MediaController.getInstance().isPlayingAudio(mDetail) || MediaController.getInstance().isAudioPaused()) {
                        MediaController.getInstance().stopAudio();
                        MediaController.getInstance().playMUSIC(mDetail);
                        //  holder.loadingSong.setVisibility(View.GONE);


//                    rotate.setDuration(900);
//                    rotate.setRepeatCount(Animation.INFINITE);
//                    itemImage.startAnimation(rotate);
                        // MediaController.getInstance().setPlaylist(mSongsList, mDetail, PhoneMediaControl.SonLoadFor.MostPlay.ordinal(), -1);
                    } else {
                        MediaController.getInstance().pauseAudio(mDetail);
                        DashBoardActivity.stop_rotateImage();

                    }
                    *//*if (MusicSQLiteHelper.isExistQueueSong(mDetail)) {
                        MusicSQLiteHelper.updateQueue(mDetail);
                    } else {
                        MusicSQLiteHelper.insertSongInQueue(mDetail);
                    }*//*
                } else {
                    DashBoardActivity.context.setStatus(false);
                    DashBoardActivity.context.setPlayer(true);
                    if (!MediaController.getInstance().isPlayingAudio(mDetail) || MediaController.getInstance().isAudioPaused()) {
                        MediaController.getInstance().stopAudio();
                        MediaController.getInstance().pauseAudio(mDetail);
                        DashBoardActivity.stop_rotateImage();
                        // MediaController.getInstance().playAudio(mDetail);
                        //  holder.loadingSong.setVisibility(View.GONE);


//                    rotate.setDuration(900);
//                    rotate.setRepeatCount(Animation.INFINITE);
//                    itemImage.startAnimation(rotate);
                        // MediaController.getInstance().setPlaylist(mSongsList, mDetail, PhoneMediaControl.SonLoadFor.MostPlay.ordinal(), -1);
                    } else {
                        MediaController.getInstance().pauseAudio(mDetail);
                        DashBoardActivity.stop_rotateImage();
                    }
                }
            }
        });*/
//        if (position > lastPosition) {
//
//            Animation animation = AnimationUtils.loadAnimation(context,
//                    R.anim.list_items);
//            holder.itemView.startAnimation(animation);
//            lastPosition = position;
//        }

    }


    void playSong(DownloadPlayListAdapter.HeaderHolder holder, SongDetail songDetail,int position)
    {

        // holder.loadingSong.setVisibility(View.VISIBLE);





        if (songDetail.getPlayStop()) {

            DashBoardActivity.context. setStatus(true);
            DashBoardActivity.context.setPlayer(false);
            DashBoardActivity.context.onResume();
//            DashBoardActivity.context.loadSongsDetailsForDownload(songDetail);



            SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_TRACK_TYPE, "1");
            SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_TRACK_ID, String.valueOf(songDetail.getId()));

            SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_MUSIC_MODE, "track");

            DashBoardActivity.context.loadSongsDetails(songDetail);
            DashBoardActivity.context.loadSongsDetails(songDetailDownloadList,position);

//                if (mDetail.getType().equals("html")) {
//                    LyricsActivity activity = new LyricsActivity();
//                    Bundle bundle = new Bundle();
//                    bundle.putString("title", mDetail.getTitle());
//                    bundle.putString("lyrics", mDetail.getLyrics());
//                    activity.setArguments(bundle);
//                    DashBoardActivity.context.addFragmentOnTop(activity);
//                    return;
//                }
            //new DownloadMp3CacheTask().execute(mDetail.getUrl(),mDetail.display_name);
            //this will cancel the timer of the system

            if (!MediaController.getInstance().isPlayingAudio(songDetail) || MediaController.getInstance().isAudioPaused()) {
                MediaController.getInstance().stopAudio();
                //  MediaController.getInstance().playAudioForDownload(songDetail);

                MediaController.getInstance().playMUSICFORDOWNload(songDetail);




                // holder.loadingSong.setVisibility(View.GONE);
                //   ForDOWNLOadSongDetail =songDetail;

            }


//                    rotate.setDuration(900);
//                    rotate.setRepeatCount(Animation.INFINITE);
//                    itemImage.startAnimation(rotate);
            // MediaController.getInstance().setPlaylist(mSongsList, mDetail, PhoneMediaControl.SonLoadFor.MostPlay.ordinal(), -1);
            else {
                DashBoardActivity.context. setStatus(false);
                MediaController.getInstance().pauseAudio(songDetail);
                Utility.getSharedInstance().dismissProgressDialog();
            }


//        if (MusicSQLiteHelper.isExistQueueSong(songDetail)) {
//            MusicSQLiteHelper.updateQueue(songDetail);
//        } else {
//            MusicSQLiteHelper.insertSongInQueue(songDetail);
//        }

            DashBoardActivity.context.setDownloadToADOPlayer(songDetail);
            holder.mTxtType.setVisibility(View.VISIBLE);
        }
        else {
            // ForDOWNLOadSongDetail = mDetail;
            DashBoardActivity.context.setPlayer(true);
            if (!MediaController.getInstance().isPlayingAudio(songDetail) || MediaController.getInstance().isAudioPaused()) {
                MediaController.getInstance().stopAudio();
                MediaController.getInstance().pauseAudio(songDetail);
                DashBoardActivity.stop_rotateImage();

            } else {
                MediaController.getInstance().pauseAudio(songDetail);
                DashBoardActivity.stop_rotateImage();
            }

        }

        File fileo = new File(context.getApplicationContext().getFilesDir(), songDetailDownloadList.get(position).getCover());
        String url=fileo.getAbsolutePath();

        File outputFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), songDetailDownloadList.get(position).getCover());
        // String url=outputFile.getAbsolutePath();

        try {


            if (!url.trim().equalsIgnoreCase("")) {

                DashBoardActivity.context.setDownloadForImage(fileo);
            }
        }catch (Exception e)

        {

        }




    }

    @Override
    public int getItemCount() {
        return songDetailDownloadList.size() ;
    }


    class HeaderHolder extends RecyclerView.ViewHolder {
        private TextView txtTitle,mTxtType;
        private LinearLayout layoutPlayList, playLayout;
        private ImageView imgWallaper,btn_play,btnDownload;
        private  View view;
        private LinearLayout mLinearShare;

        public HeaderHolder(View itemView) {
            super(itemView);

            imgWallaper = (ImageView) itemView.findViewById(R.id.playlsitImage);
            mLinearShare=(LinearLayout)itemView.findViewById(R.id.btn_share);
            btn_play= (ImageView) itemView.findViewById(R.id.btn_play);
            mTxtType =(TextView)itemView.findViewById(R.id.txtTypeSOng);
            btnDownload= (ImageView) itemView.findViewById(R.id.btnDownload);
            txtTitle = (TextView) itemView.findViewById(R.id.txtPlayListName);
            view= itemView;

            layoutPlayList=(LinearLayout)itemView.findViewById(R.id.layoutPlayList);
            playLayout = (LinearLayout)itemView.findViewById(R.id.playLayout);
        }
    }
}