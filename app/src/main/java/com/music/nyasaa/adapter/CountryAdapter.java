package com.music.nyasaa.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.music.nyasaa.R;
import com.music.nyasaa.models.OshoCenterDAO;

import java.util.List;

public class CountryAdapter extends BaseAdapter {

    private Context context;
    private List<OshoCenterDAO> getAllCountryDetails;
    private TextView txtItemName;
    LayoutInflater inflter;

    public CountryAdapter(Context applicationContext, List<OshoCenterDAO> data) {
        this.getAllCountryDetails = data;
        this.context = applicationContext;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return getAllCountryDetails.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return getAllCountryDetails.size();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup)
    {

        if (view == null)
        {
            view = LayoutInflater.from(context).
                    inflate(R.layout.custom_spinner_row, viewGroup, false);

        }

        // get the TextView for item name and item description
        txtItemName = (TextView)
                view.findViewById(R.id.txtItemName);



        txtItemName.setText(getAllCountryDetails.get(i).getName());
        return view;
    }
}