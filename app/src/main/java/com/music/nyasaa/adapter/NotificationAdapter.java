package com.music.nyasaa.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.music.nyasaa.R;
import com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity;
import com.music.nyasaa.models.ResponseModel.getCommunity.GetCommunityResponseData;
import com.music.nyasaa.models.ResponseModel.getCommunity.SoretedCommunityData;
import com.music.nyasaa.retrofit.Config;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.HeaderHolder> {

    LayoutInflater inflater;
    boolean header = false;

    Context context;
    private List<SoretedCommunityData> community_dao;
    private int lastPosition = -1;
    private List<GetCommunityResponseData> sortedDAO_Data;
    int cnt=0;


    public NotificationAdapter(Context context, List<SoretedCommunityData> getCommunityResponseData) {

        this.context = context;
//        this.img = img;
//        this.name = name;

        this.community_dao = getCommunityResponseData;
    }


    @Override
    public NotificationAdapter.HeaderHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_notficationss, parent, false);

        return new NotificationAdapter.HeaderHolder(itemView);


        // return new HeaderHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.chapter_item, null));
    }

    @Override
    public void onBindViewHolder(NotificationAdapter.HeaderHolder holder, final int position) {


        sortedDAO_Data = new ArrayList<>();

        // use pisco and check null url :'

        // holder.imgWallaper.setImageResource(community_dao.get(position).getImage_url());

        if (community_dao != null)
            if (community_dao.size() > 0) {


            if(community_dao.get(position).action.equalsIgnoreCase("1"))
            {


                holder.linearLayout.setVisibility(View.VISIBLE);
                holder.txtTitle.setText(community_dao.get(position).getCommunityName());
                holder.txtTitle.setVisibility(View.VISIBLE);
                holder.txtLongInfo.setText(community_dao.get(position).getShortInfo());


                Picasso.with(context)
                        .load(Config.PIC_URL_COMMUNITY + Uri.encode( community_dao.get(position).getImageUrl()))
                        .placeholder(R.drawable.bg_default_album_art)
                        .error(R.drawable.bg_default_album_art)
                        .into(holder.imgparentImg);

                holder.mButReject.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        ((DashBoardActivity)context).acceptOrRejectRequest(community_dao.get(position).getId(),"3");
                    }
                });


                holder.mButAccept.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        ((DashBoardActivity)context).acceptOrRejectRequest(community_dao.get(position).getId(),"2");

                    }
                });


            }
            else {
                holder.linearLayout.setVisibility(View.GONE);
            }





            }


//        Toast.makeText(context,"size:"+sortedDAO_Data.size(),Toast.LENGTH_SHORT).show();


        if (position > lastPosition) {

            Animation animation = AnimationUtils.loadAnimation(context,
                    R.anim.list_items);
            holder.itemView.startAnimation(animation);
            lastPosition = position;
        }

    }

    @Override
    public int getItemCount() {
        return community_dao.size();
    }


    class HeaderHolder extends RecyclerView.ViewHolder {
        private TextView txtTitle, txtLongInfo;
        private LinearLayout linearLayout;
        private ImageView imgWallaper, imgparentImg;
        private Button mButAccept,mButReject;


        public HeaderHolder(View itemView) {
            super(itemView);


            txtLongInfo = (TextView) itemView.findViewById(R.id.txtCommInfo);
            linearLayout=(LinearLayout)itemView.findViewById(R.id.layoutPlayList);
            txtTitle = (TextView) itemView.findViewById(R.id.txtCommunityName);
            imgparentImg = (ImageView) itemView.findViewById(R.id.playlsitImage);

            mButAccept =(Button)itemView.findViewById(R.id.butAccept);
            mButReject=(Button)itemView.findViewById(R.id.butReject);


        }
    }


}