package com.music.nyasaa.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.music.nyasaa.R;
import com.music.nyasaa.front_end_layer.activities.GetPackageActivity;
import com.music.nyasaa.models.GetPackageList;
import com.music.nyasaa.utility.SharedPreferenceManager;
import com.squareup.picasso.Picasso;

import java.util.List;

public class GetPackagesAdapter extends RecyclerView.Adapter<GetPackagesAdapter.HeaderHolder> {

    LayoutInflater inflater;
    boolean header = false;
    List<GetPackageList> getTuturialModelRespnseData;
    Context context;
    // private String[] name;
    // private Integer[] img;

    // songDetail;


    private int lastPosition = -1;

    String fname="",lName="",Mob="";


    public GetPackagesAdapter(Context context, List<GetPackageList> getTuturialModelRespnseData) {

        this.context = context;
        this.getTuturialModelRespnseData = getTuturialModelRespnseData;


        fname=SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_FNAME,"");
        lName=SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_LASTNAMES,"");
        Mob=SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_MOB,"");


    }


    @Override
    public GetPackagesAdapter.HeaderHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.package_list_items, parent, false);

        return new GetPackagesAdapter.HeaderHolder(itemView);


        // return new HeaderHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.chapter_item, null));
    }

    @Override
    public void onBindViewHolder(final GetPackagesAdapter.HeaderHolder holder, final int position) {

        holder.mTxtPackageCost.setPaintFlags(holder.mTxtPackageCost.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        holder.mTxtPackageName.setText("" + getTuturialModelRespnseData.get(position).getTitle());
        holder.mTxtPackageCost.setText("Price "+"₹ " + getTuturialModelRespnseData.get(position).getPrice());
        holder.mTxtPackageCostDis.setText("Discount Price "+"₹ " + getTuturialModelRespnseData.get(position).getDis_price());
        holder.mtxtPackDuration.setText("For " + getTuturialModelRespnseData.get(position).getDuration() + " Months");


        try {
            String url = getTuturialModelRespnseData.get(position).getImage_url() + "/" + getTuturialModelRespnseData.get(position).getImage();


            Picasso.with(context)
                    .load(url)
//                    .load(com.music.nyassa.retrofit.Config.PIC_URL_)
                    .placeholder(R.drawable.bg_default_album_art)
                    .error(R.drawable.bg_default_album_art)
                    .into(holder.imgPlay);
        } catch (Exception e) {
            e.printStackTrace();
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_PACKAGE_ID, getTuturialModelRespnseData.get(position).getPackageId());

                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_DURATION,getTuturialModelRespnseData.get(position).getDuration());

                ((GetPackageActivity) context).callInstamojoPay(SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_EMAIL,"pmyana@yahoo.in"), Mob, getTuturialModelRespnseData.get(position).getDis_price(), "Nyasaa Music app Product purchase", fname+ "" +lName);
//                Intent intent = new Intent(context, PaymentActivity.class);
//
//                intent.putExtra("pay",getAlbumsListDataArrayList.get(position).getPrice());
//                intent.putExtra("id",getAlbumsListDataArrayList.get(position).getPackageId());
//                context.startActivity(intent);
            }
        });


    }


    @Override
    public int getItemCount() {


        return getTuturialModelRespnseData.size();
    }


    class HeaderHolder extends RecyclerView.ViewHolder {

        private ImageView imgPlay;
        private TextView mTxtPackageName, mTxtPackageCost, mTxtPackageCostDis, mtxtPackDuration;


        public HeaderHolder(View itemView) {
            super(itemView);


            mTxtPackageName = (TextView) itemView.findViewById(R.id.txtPackageName);
            mTxtPackageCost = (TextView) itemView.findViewById(R.id.txtPackageCost);
            mTxtPackageCostDis = (TextView) itemView.findViewById(R.id.txtPackageCostDis);
            mtxtPackDuration = (TextView) itemView.findViewById(R.id.txtDuration);
            imgPlay = (ImageView) itemView.findViewById(R.id.imgPack);


        }

    }
}