package com.music.nyasaa.adapter;

import com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity;
import com.music.nyasaa.manager.MediaController;
import com.music.nyasaa.models.SongDetail;
import com.music.nyasaa.utility.SharedPreferenceManager;
import com.music.nyasaa.utility.Utility;

import java.util.ArrayList;
import java.util.List;

import static com.music.nyasaa.fragments.FragmentDashBoard.fragmentDashBoards;
import static com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity.AutoIndex;
import static com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity.Autoplaylist;
import static com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity.ForDOWNLOadSongDetail;

//import static com.music.osho.fragments.FragmentDashBoard.mSongsAudio;
//import static com.music.osho.fragments.FragmentDashBoard.mSongss;

public class AutoPlayInit {

   // public static ArrayList<SongDetail> Autoplaylist = new ArrayList<>();

    public AutoPlayInit(ArrayList<SongDetail> AutoplaylistbyAdapter, int index, int positionsofDashbord)
    {
        Autoplaylist.clear();
        AutoIndex=index;
        DashBoardActivity.context.positionsofDashbord = positionsofDashbord;
        SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_TRACK_TYPE, String.valueOf(positionsofDashbord));

        for(int i=0;i<AutoplaylistbyAdapter.size();i++)
            Autoplaylist.add(AutoplaylistbyAdapter.get(i));
    }

    public AutoPlayInit(List<SongDetail> songDetailDownload, int position, int positionsofDashbord) {
        Autoplaylist.clear();
        AutoIndex=position;
        DashBoardActivity.context.positionsofDashbord = positionsofDashbord;
        SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_TRACK_TYPE, String.valueOf(positionsofDashbord));

        for(int i=0;i<songDetailDownload.size();i++)
            Autoplaylist.add(songDetailDownload.get(i));
    }


    public void autoPlay()
    {

        {

           // new AutoPlayInit(mSongss,position);
            SongDetail mDetail = Autoplaylist.get(AutoIndex);

            ForDOWNLOadSongDetail = Autoplaylist.get(AutoIndex);

            fragmentDashBoards.playPoaseMusic(AutoIndex);


           // SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_TRACK_TYPE,"3");
            SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_TRACK_ID,String.valueOf(mDetail.getId()));

            SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_MUSIC_MODE, "music");
//                holder.loadingSong.setVisibility(View.VISIBLE);
            if (Autoplaylist.get(AutoIndex).getPlayStop()) {
                DashBoardActivity.context. setStatus(true);
                DashBoardActivity.context.setPlayer(false);
                DashBoardActivity.context.onResume();
                DashBoardActivity.context.loadSongsDetails(mDetail);
                DashBoardActivity.context.loadSongsDetails(Autoplaylist,AutoIndex);


//                if (mDetail.getType().equals("html")) {
//                    LyricsActivity activity = new LyricsActivity();
//                    Bundle bundle = new Bundle();
//                    bundle.putString("title", mDetail.getTitle());
//                    bundle.putString("lyrics", mDetail.getLyrics());
//                    activity.setArguments(bundle);
//                    DashBoardActivity.context.addFragmentOnTop(activity);
//                    return;
//                }
                //new DownloadMp3CacheTask().execute(mDetail.getUrl(),mDetail.display_name);


                //this will cancel the timer of the system


                if (!MediaController.getInstance().isPlayingAudio(mDetail) || MediaController.getInstance().isAudioPaused()) {
                    MediaController.getInstance().stopAudio();
                    MediaController.getInstance().playMUSIC(mDetail);
//                    MediaController.getInstance().slowRun(mDetail);

//                    holder.loadingSong.setVisibility(View.GONE);
                    ForDOWNLOadSongDetail = mDetail;

                }


//                    rotate.setDuration(900);
//                    rotate.setRepeatCount(Animation.INFINITE);
//                    itemImage.startAnimation(rotate);
                // MediaController.getInstance().setPlaylist(mSongsList, mDetail, PhoneMediaControl.SonLoadFor.MostPlay.ordinal(), -1);
                else {
                    MediaController.getInstance().pauseAudio(mDetail);
                    Utility.getSharedInstance().dismissProgressDialog();
                }


                    /*if (MusicSQLiteHelper.isExistQueueSong(mDetail)) {
                        MusicSQLiteHelper.updateQueue(mDetail);
                    } else {
                        MusicSQLiteHelper.insertSongInQueue(mDetail);
                    }*/
            }
            else
            {
                ForDOWNLOadSongDetail = mDetail;
                DashBoardActivity.context. setStatus(false);
                DashBoardActivity.context.setPlayer(true);
                if (!MediaController.getInstance().isPlayingAudio(mDetail) || MediaController.getInstance().isAudioPaused()) {
                    MediaController.getInstance().stopAudio();
                    MediaController.getInstance().pauseAudio(mDetail);
                    DashBoardActivity.stop_rotateImage();
                    // MediaController.getInstance().playAudio(mDetail);
                    //  holder.loadingSong.setVisibility(View.GONE);


//                    rotate.setDuration(900);
//                    rotate.setRepeatCount(Animation.INFINITE);
//                    itemImage.startAnimation(rotate);
                    // MediaController.getInstance().setPlaylist(mSongsList, mDetail, PhoneMediaControl.SonLoadFor.MostPlay.ordinal(), -1);
                } else {
                    MediaController.getInstance().pauseAudio(mDetail);
                    DashBoardActivity.stop_rotateImage();
                }
            }
        }
    }


}
