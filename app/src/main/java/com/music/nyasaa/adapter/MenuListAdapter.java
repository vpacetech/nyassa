package com.music.nyasaa.adapter;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.music.nyasaa.R;
import com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity;
import com.music.nyasaa.models.ResponseModel.GetCategoryAll.GetResponseCategoryData;
import com.music.nyasaa.retrofit.Config;
import com.music.nyasaa.utility.SharedPreferenceManager;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MenuListAdapter extends RecyclerView.Adapter<MenuListAdapter.HeaderHolder> {

    LayoutInflater inflater;
    boolean header = false;
    String sDrawerID;
    Context context;

    private List<GetResponseCategoryData> menuListArrayList;
    int lastPosition = -1;
    private boolean subListFlagSong = false;
    private boolean subListFlag = false;

    public FragmentManager f_manager;
    private String fragVal = "";
    private DashBoardActivity dashBoardActivity;


    public MenuListAdapter(DashBoardActivity context, List<GetResponseCategoryData> strings) {


        this.context = context;
        this.menuListArrayList = strings;


    }


    @Override
    public MenuListAdapter.HeaderHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_items_media_type, parent, false);

        return new MenuListAdapter.HeaderHolder(itemView);


        // return new HeaderHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.chapter_item, null));
    }

    @Override
    public void onBindViewHolder(final MenuListAdapter.HeaderHolder holder, final int position) {


        holder.txtTitle.setText(menuListArrayList.get(position).getName());


        Picasso.with(context)
                .load(Config.BASE_CAT_PIC + menuListArrayList.get(position).getImage_url())
                .error(R.drawable.bcontrol)
                .into(holder.imgMedia);

        holder.imgMedia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_CAT_NAME, menuListArrayList.get(position).getName());
                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_CAT_ORIGINAL, menuListArrayList.get(position).getId());
                sDrawerID = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_DRAWER_TYPE, "none");

                if (sDrawerID.equalsIgnoreCase("1")) {

                    ((DashBoardActivity) context).openAudioFragment();

                } else if (sDrawerID.equalsIgnoreCase("2")) {
                    ((DashBoardActivity) context).openVideoFragment();
                }

            }
        });


        if (position > lastPosition) {

            Animation animation = AnimationUtils.loadAnimation(context,
                    R.anim.list_items);
            holder.itemView.startAnimation(animation);
            lastPosition = position;
        }


    }

    @Override
    public int getItemCount() {
        return menuListArrayList.size();
    }


    class HeaderHolder extends RecyclerView.ViewHolder {
        private TextView txtTitle;
        private LinearLayout mMenuLayout;
        private ImageView imgMedia;


        public HeaderHolder(View itemView) {
            super(itemView);

            //   imgWallaper = (ImageView) itemView.findViewById(R.id.imgWall);
            txtTitle = (TextView) itemView.findViewById(R.id.txtMediaType);
            mMenuLayout = (LinearLayout) itemView.findViewById(R.id.menulayout);
            imgMedia = (ImageView) itemView.findViewById(R.id.mediaImage);


        }


    }
}



