package com.music.nyasaa.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.music.nyasaa.R;

import com.music.nyasaa.models.ResponseModel.GetMembersofCommunity.ResponseGetCommunityMemData;
import com.music.nyasaa.retrofit.Config;
import com.music.nyasaa.utility.SharedPreferenceManager;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CommunityMembersAdapter extends RecyclerView.Adapter<CommunityMembersAdapter.HeaderHolder> {

    LayoutInflater inflater;
    boolean header = false;
    private Dialog mediaDialog;
    private String parentCommuntyUserID;

    Context context;
    List<ResponseGetCommunityMemData> responseGetCommunityMemDataList = null;
    private int lastPosition = -1;


    public CommunityMembersAdapter(Context context, List<ResponseGetCommunityMemData> responseGetCommunityMemDataList) {

        this.context = context;
        this.responseGetCommunityMemDataList = responseGetCommunityMemDataList;
        parentCommuntyUserID =SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_COMM_PARENT_ID_COMMUNITY,"");
    }


    @Override
    public CommunityMembersAdapter.HeaderHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_items_community_members, parent, false);

        return new CommunityMembersAdapter.HeaderHolder(itemView);


        // return new HeaderHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.chapter_item, null));
    }

    @Override
    public void onBindViewHolder(CommunityMembersAdapter.HeaderHolder holder, final int position) {


        Picasso.with(context)
                .load(Config.BASE_PROFILE_PIC + Uri.encode(responseGetCommunityMemDataList.get(position).getProfilePhoto()))
                .placeholder(R.drawable.default_music)
                .error(R.drawable.default_music)
                .into(holder.imgWallaper);

        holder.txtTitle.setText(responseGetCommunityMemDataList.get(position).getUsername());
        holder.txtTitle.setVisibility(View.VISIBLE);
//        holder.mTxtDesc.setText(responseGetCommunityMemDataList.get(position).getUpdatedAt());

//        holder.layoutPlayList.setBackgroundColor(context.getResources().getColor(R.color.black));

        holder.layoutPlayList.setBackgroundColor(context.getResources().getColor(R.color.orBLACK));
        holder.layoutPlayList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(context, ScrollingProfileActivity.class);
//                ((CommunityMembersActivity)context  ).startActivity(intent);

            }
        });



        if(parentCommuntyUserID.equalsIgnoreCase(responseGetCommunityMemDataList.get(position).getUserId()))
        {
            holder.mTxtDesc.setVisibility(View.VISIBLE);
        }
        else {
            holder.mTxtDesc.setVisibility(View.GONE);
        }

//        if (position > lastPosition) {
//
//            Animation animation = AnimationUtils.loadAnimation(context,
//                    R.anim.list_items);
//            holder.itemView.startAnimation(animation);
//            lastPosition = position;
//        }

        holder.imgWallaper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMediaDialog(responseGetCommunityMemDataList.get(position).getProfilePhoto());
            }
        });


    }

    @Override
    public int getItemCount() {
        return responseGetCommunityMemDataList.size();
    }


    public final class HeaderHolder extends RecyclerView.ViewHolder {
        private TextView txtTitle, mTxtDesc;
        public RelativeLayout layoutPlayList;
        private ImageView imgWallaper;


        public HeaderHolder(View itemView) {
            super(itemView);


            imgWallaper = (ImageView) itemView.findViewById(R.id.playlsitImage);
            txtTitle = (TextView) itemView.findViewById(R.id.txtPlayListName);
            mTxtDesc = (TextView) itemView.findViewById(R.id.description);
            layoutPlayList = (RelativeLayout) itemView.findViewById(R.id.layoutPlayList);


        }


    }

    public void removeItem(int position) {
        responseGetCommunityMemDataList.remove(position);
        // notify the item removed by position
        // to perform recycler view delete animations
        // NOTE: don't call notifyDataSetChanged()
        notifyItemRemoved(position);
    }

    private void showMediaDialog(String url) {
        mediaDialog = new Dialog(context, android.R.style.Theme_Dialog);
        mediaDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        mediaDialog.setContentView(R.layout.dialog_media);
        mediaDialog.getWindow().getAttributes().width = ActionBar.LayoutParams.FILL_PARENT;
        mediaDialog.getWindow().setBackgroundDrawable(new ColorDrawable(context.getResources().getColor(R.color.blackTransnew)));
        WindowManager.LayoutParams wmlps = mediaDialog.getWindow().getAttributes();
        // mediaDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        Window window = mediaDialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        window.setGravity(Gravity.CENTER);
        ImageView imgWallPost = (ImageView) mediaDialog.findViewById(R.id.imageViewWallImage);


        Picasso.with(context)
                .load(Config.BASE_PROFILE_PIC + Uri.encode(url))
                .placeholder(R.drawable.bg_default_album_art)
                .error(R.drawable.bg_default_album_art)
                .into(imgWallPost);



        mediaDialog.show();}
}



