package com.music.nyasaa.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.music.nyasaa.R;
import com.music.nyasaa.front_end_layer.activities.SearchCommunityActivity;
import com.music.nyasaa.models.ResponseModel.getCommunity.GetCommunityResponseData;
import com.music.nyasaa.models.ResponseModel.getCommunity.GetSearchedCommunityData;
import com.music.nyasaa.retrofit.Config;
import com.music.nyasaa.utility.SharedPreferenceManager;
import com.squareup.picasso.Picasso;

import java.util.List;

public class GetSeaechCommunityAdapter extends RecyclerView.Adapter<GetSeaechCommunityAdapter.HeaderHolder> {

    LayoutInflater inflater;
    boolean header = false;

    Context context;

    private List<GetSearchedCommunityData> community_dao;

    private int lastPosition = -1;


    public GetSeaechCommunityAdapter(Context context, List<GetCommunityResponseData> Community_DAO) {




    }

    public GetSeaechCommunityAdapter(SearchCommunityActivity context, List<GetSearchedCommunityData> sortedCommunityList) {

        this.context = context;
//        this.img = img;
//        this.name = name;

        this.community_dao = sortedCommunityList;
    }


    @Override
    public GetSeaechCommunityAdapter.HeaderHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_items_community, parent, false);

        return new GetSeaechCommunityAdapter.HeaderHolder(itemView);


        // return new HeaderHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.chapter_item, null));
    }

    @Override
    public void onBindViewHolder(GetSeaechCommunityAdapter.HeaderHolder holder, final int position) {


        // use pisco and check null url :'

        // holder.imgWallaper.setImageResource(community_dao.get(position).getImage_url());

        if (community_dao != null)
            if (community_dao.size() > 0) {


                holder.txtTitle.setText(community_dao.get(position).getName());
                holder.txtTitle.setVisibility(View.VISIBLE);

                String userID= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"none");



                if (userID.equalsIgnoreCase(String.valueOf(community_dao.get(position).getParentUserId()))) {
                    holder.imgparentImg.setVisibility(View.VISIBLE);

                } else {
                    holder.imgparentImg.setVisibility(View.GONE);
                }


                Picasso.with(context)
                        .load(Config.PIC_URL_COMMUNITY  + Uri.encode( community_dao.get(position).getImageUrl()))
                        .placeholder(R.drawable.community_blank)
                        .error(R.drawable.community_blank)
                        .into(holder.imgWallaper);
//


            }

//        holder.txtTitle.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(context, "Text", Toast.LENGTH_SHORT).show();
//            }
//        });

        holder.imgWallaper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//
//

                ((SearchCommunityActivity)context).showMembersDialog(community_dao.get(position).getParentUserId(),community_dao.get(position).getId(),community_dao.get(position).getName());

            }
        });


//        if (position > lastPosition) {
//
//            Animation animation = AnimationUtils.loadAnimation(context,
//                    R.anim.list_items);
//            holder.itemView.startAnimation(animation);
//            lastPosition = position;
//        }

    }

    @Override
    public int getItemCount() {
        return community_dao.size();
    }


    class HeaderHolder extends RecyclerView.ViewHolder {
        private TextView txtTitle;
        private RelativeLayout relativelayout;
        private ImageView imgWallaper, imgparentImg;


        public HeaderHolder(View itemView) {
            super(itemView);

            imgWallaper = (ImageView) itemView.findViewById(R.id.imgWall);
            txtTitle = (TextView) itemView.findViewById(R.id.txtImgText);
            imgparentImg = (ImageView) itemView.findViewById(R.id.txtCrossMark);


        }


    }
}


