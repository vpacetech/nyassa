package com.music.nyasaa.adapter;

import android.content.Context;

import android.content.Intent;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.music.nyasaa.R;
import com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity;
import com.music.nyasaa.front_end_layer.activities.easy_video_player.VideoPlayerActivity;
import com.music.nyasaa.models.ResponseModel.GetAllVideos.GetVideoResponseData;
import com.music.nyasaa.utility.SharedPreferenceManager;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.music.nyasaa.fragments.myvideos.FragmentMyVideos.fragmentMyVideoss;
import static com.music.nyasaa.fragments.myvideos.FragmentMyVideos.getEventResponseDataRespon;
import static com.music.nyasaa.retrofit.Config.BASE_VIDEO_URL;

public class GetAllVideosAdapter extends RecyclerView.Adapter<GetAllVideosAdapter.ViewHolder> {

    private Context context;
  //  private List<GetVideoResponseData> getVideoResponseDataList;



    RotateAnimation rotate = new RotateAnimation(
            0, 360,
            Animation.RELATIVE_TO_SELF, 0.5f,
            Animation.RELATIVE_TO_SELF, 0.5f
    );

    public GetAllVideosAdapter(Context videoPlayerActivity, List<GetVideoResponseData> getVideoResponseDataList) {
        this.context=videoPlayerActivity;
       // this.getVideoResponseDataList=getVideoResponseDataList;
    }


    public GetAllVideosAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new GetAllVideosAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_items_video, parent, false));
    }

    public void onBindViewHolder(final GetAllVideosAdapter.ViewHolder holder, final int position) {




        Picasso.with(context)
                .load(com.music.nyasaa.retrofit.Config.BASE_VIDEO_URL_COVER+ Uri.encode( getEventResponseDataRespon.get(position).getCover()))
                .placeholder(R.drawable.default_music)
                .error(R.drawable.default_music)
                .into(holder.cover);


        holder.cover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(context,VideoPlayerActivity.class);
                intent.putExtra("url",getEventResponseDataRespon.get(position).getFile());
                context.startActivity(intent);
            }
        });

        holder.songTitle.setText(getEventResponseDataRespon.get(position).getName());
//        holder.txtTypeSOng.setText(getEventResponseDataRespon.get(position).getFile());


//        if (getEventResponseDataRespon.get(position).getPlayStop()) {
//
//            holder.btn_song_play.setImageResource(R.drawable.ic_pause);
//        }
//        else {
//            holder.btn_song_play.setImageResource(R.drawable.play);
//        }







        holder.songTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(context,VideoPlayerActivity.class);
                intent.putExtra("url",getEventResponseDataRespon.get(position).getFile());
                context.startActivity(intent);
               if( getEventResponseDataRespon.get(position).getPlayStop()==false)
                fragmentMyVideoss.videoDataNew(BASE_VIDEO_URL+getEventResponseDataRespon.get(position).getFile());

                fragmentMyVideoss.playPoase(position);
            }
        });

        holder.btn_song_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(context,VideoPlayerActivity.class);
                intent.putExtra("url",getEventResponseDataRespon.get(position).getFile());
                context.startActivity(intent);



                if( getEventResponseDataRespon.get(position).getPlayStop()==false)
                fragmentMyVideoss.videoDataNew(BASE_VIDEO_URL+getEventResponseDataRespon.get(position).getFile());
                fragmentMyVideoss.playPoase(position);
            }
        });

        holder.btnDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fragmentMyVideoss.downloadVDO(getEventResponseDataRespon.get(position));
            }
        });

        holder.btn_song_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_TRACK_TYPE, "5");
                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_TRACK_ID, String.valueOf(getEventResponseDataRespon.get(position).id));

                DashBoardActivity dash = new DashBoardActivity();
                dash.showCommDialog(context);
            }
        });




        if(getEventResponseDataRespon.get(position).getQuedisc().equalsIgnoreCase("2"))
        {
            holder.txtTypeSOng.setText("Discourse");
        }
        else {
            holder.txtTypeSOng.setText("Q & A");
        }

        // Picasso.with(context).load("http://olavideos.s3.amazonaws.com/" + getVideoResponseDataList.get(position).getI).error(R.drawable.bg_default_album_art).into(holder.cover);



    }

    public int getItemCount() {
        return getEventResponseDataRespon.size();
    }

    public class ViewHolder extends android.support.v7.widget.RecyclerView.ViewHolder {

        LinearLayout btn_song_play,btn_song_share;

        ImageView btnDownload;

        ImageView cover;
        TextView songTitle,txtTypeSOng;

        ProgressBar loadingSong;
        private LinearLayout mLinearLaypout;

        public ViewHolder(View itemView) {
            super(itemView);
            songTitle = (TextView) itemView.findViewById(R.id.txtPlayListName);
            txtTypeSOng = (TextView) itemView.findViewById(R.id.txtTypeSOng);

            btn_song_play = (LinearLayout) itemView.findViewById(R.id.btn_play);

            btn_song_share = (LinearLayout) itemView.findViewById(R.id.btn_share);

            btnDownload= (ImageView) itemView.findViewById(R.id.btnDownload);
            cover = (ImageView) itemView.findViewById(R.id.playlsitImage);
            loadingSong = (ProgressBar) itemView.findViewById(R.id.loadingSong);

            mLinearLaypout = (LinearLayout) itemView.findViewById(R.id.layoutPlayList);
        }
    }
}