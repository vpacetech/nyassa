package com.music.nyasaa.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.music.nyasaa.R;
import com.music.nyasaa.models.ResponseModel.GetTuturialModelRespnseData;
import com.music.nyasaa.retrofit.Config;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class HowToUseAdapter extends RecyclerView.Adapter<HowToUseAdapter.HeaderHolder> {

    LayoutInflater inflater;
    boolean header = false;
    ArrayList<GetTuturialModelRespnseData> getTuturialModelRespnseData;
    Context context;
    // private String[] name;
    // private Integer[] img;

    // songDetail;



    private int lastPosition = -1;


    public HowToUseAdapter(Context context, ArrayList<GetTuturialModelRespnseData> getTuturialModelRespnseData) {

        this.context = context;
        this.getTuturialModelRespnseData=getTuturialModelRespnseData;



    }



    @Override
    public HowToUseAdapter.HeaderHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_items_tutorials, parent, false);

        return new HowToUseAdapter.HeaderHolder(itemView);


        // return new HeaderHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.chapter_item, null));
    }

    @Override
    public void onBindViewHolder(final HowToUseAdapter.HeaderHolder holder, final int position) {




        Picasso.with(context)
                .load(Config.BASE_TUT_IMGE + Uri.encode( getTuturialModelRespnseData.get(position).getImageUrl()))
                .placeholder(R.drawable.default_music)
                .error(R.drawable.default_music)
                .into(holder.imgWallaper);




//
//        if (position > lastPosition) {
//
//            Animation animation = AnimationUtils.loadAnimation(context,
//                    R.anim.list_items);
//            holder.itemView.startAnimation(animation);
//            lastPosition = position;
//        }




    }






    @Override
    public int getItemCount() {


        return getTuturialModelRespnseData.size() ;
    }


    class HeaderHolder extends RecyclerView.ViewHolder {

        private ImageView imgWallaper;


        public HeaderHolder(View itemView) {
            super(itemView);

            imgWallaper = (ImageView) itemView.findViewById(R.id.tutImg);



        }

    }
}