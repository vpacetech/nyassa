package com.music.nyasaa.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.music.nyasaa.R;
import com.music.nyasaa.fragments.meditation_center.FragmentOshoCenter;
import com.music.nyasaa.models.OshoCenterDAO;

import java.util.List;

public class MediationCenterAdapter extends RecyclerView.Adapter<MediationCenterAdapter.ViewHolder> {
    private List<String> nameList;
    private List<String> addressList;
    private List<String> contactList;
    private List<String> numberList;
    private List<String> stateList;
    private List<String> websiteList;
    private Context context;
    private FragmentOshoCenter fragmentOshoCenter;
    List<OshoCenterDAO> AllOshoCenterList;

    public MediationCenterAdapter(Context context, List<String> nameList, List<String> addressList, List<String> contactList, List<String> numberList, List<String> stateList, List<String> websiteList, FragmentOshoCenter fragmentOshoCenter) {
        this.context = context;
        this.nameList = nameList;
        this.addressList = addressList;
        this.contactList = contactList;
        this.numberList = numberList;
        this.stateList = stateList;
        this.websiteList = websiteList;
        this.fragmentOshoCenter = fragmentOshoCenter;
    }

    public MediationCenterAdapter(FragmentActivity activity, List<OshoCenterDAO> allOshoCenterList, FragmentOshoCenter fragmentOshoCenter) {

        this.context = context;
        this.fragmentOshoCenter = fragmentOshoCenter;
        this.AllOshoCenterList=allOshoCenterList;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView txtName;
        public TextView txtAddress;
        public TextView txtContact;
        public TextView txtNumber;
        public TextView txtState;
        public TextView txtWebsite;
        public View layout;
        public ImageView imgCall;

        public ViewHolder(View v) {
            super(v);
            layout = v;


            txtName = (TextView) v.findViewById(R.id.Name);
            txtAddress = (TextView) v.findViewById(R.id.Address);
            txtContact = (TextView) v.findViewById(R.id.Contact);
            txtNumber = (TextView) v.findViewById(R.id.Number);
            txtState = (TextView) v.findViewById(R.id.State);
            txtWebsite = (TextView) v.findViewById(R.id.Website);
            imgCall = v.findViewById(R.id.imgCall);
        }
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.list_items_mediation_center, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        final String name = AllOshoCenterList.get(position).getName();
        if (name.equalsIgnoreCase("") || name.equalsIgnoreCase("null")) {
            holder.txtName.setText("NA");
        } else {
            holder.txtName.setText(name);
        }


        final String address = AllOshoCenterList.get(position).getAddress();

        if (address.equalsIgnoreCase("") || address.equalsIgnoreCase("null")) {
            holder.txtAddress.setText("NA");
        } else {
            holder.txtAddress.setText(address);
        }


        final String contact = AllOshoCenterList.get(position).getContactName();


        if (contact.equalsIgnoreCase("") || contact.equalsIgnoreCase("null")) {
            holder.txtContact.setText("NA");
        } else {
            holder.txtContact.setText(contact);

        }


        final String number = AllOshoCenterList.get(position).getContactNumber();

        if (number.equalsIgnoreCase("") || number.equalsIgnoreCase("null")) {
            holder.txtNumber.setText("NA");
        } else {
            holder.txtNumber.setText(number);
        }


        final String state = AllOshoCenterList.get(position).getStateId();


        if (!state.equalsIgnoreCase("") || state.equalsIgnoreCase("null")) {
            holder.txtState.setText("NA");
        } else {
            holder.txtState.setText("NA");
        }


        final String website =AllOshoCenterList.get(position).getWebsite();

       /* if (website.equalsIgnoreCase("") || website.equalsIgnoreCase("null")) {
            holder.txtWebsite.setText("NA");
        } else {
            holder.txtWebsite.setText(website);
        }*/

        if (website.equalsIgnoreCase("") || website.equalsIgnoreCase("null")) {
            holder.txtWebsite.setText("NA");
        }
        else {
            holder.txtWebsite.setClickable(true);
            holder.txtWebsite.setMovementMethod(LinkMovementMethod.getInstance());
            String text = "<a href='" + website + "'>" + website + " </a>";
            holder.txtWebsite.setText(Html.fromHtml(text));

           /* SpannableString content = new SpannableString(website);
            content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
            holder.txtWebsite.setText(content);*/

        }








        holder.txtNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                fragmentOshoCenter.makePhoneCall(number);
            }
        });


        holder.imgCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                fragmentOshoCenter.makePhoneCall(number);
            }
        });


        holder.txtWebsite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {

                    Intent browserIntent = new Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse(website));
                    context.startActivity(browserIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });


    }

    @Override
    public int getItemCount() {
        return AllOshoCenterList.size();
    }
}
