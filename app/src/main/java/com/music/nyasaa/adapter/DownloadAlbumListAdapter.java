package com.music.nyasaa.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Environment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.music.nyasaa.R;
import com.music.nyasaa.fragments.downloads.FragmentDownloads;
import com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity;
import com.music.nyasaa.front_end_layer.activities.easy_video_player.VideoPlayerActivity;
import com.music.nyasaa.manager.MediaController;
import com.music.nyasaa.models.ResponseModel.GetMyPlayList.GetMyPlayListResponseData;
import com.music.nyasaa.models.SongDetail;
import com.music.nyasaa.utility.SharedPreferenceManager;
import com.music.nyasaa.utility.Utility;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import static com.music.nyasaa.fragments.downloads.FragmentDownloads.songDetailDownload;

/*public class DownloadAlbumListAdapter {
}*/


public class DownloadAlbumListAdapter extends RecyclerView.Adapter<DownloadAlbumListAdapter.HeaderHolder> {

    LayoutInflater inflater;
    boolean header = false;
    FragmentDownloads fragmentDownloads;
    Context context;
    // private String[] name;
    // private Integer[] img;

    // songDetail;

    //  List<SongDetail> songDetail;
    private int lastPosition = -1;


    public DownloadAlbumListAdapter(FragmentDownloads activity, Context context, List<SongDetail> songDetail) {

        this.fragmentDownloads = activity;
        this.context = context;
        // this.img = img;
        // this.name = name;
        //this.songDetail=songDetail;


    }

    public DownloadAlbumListAdapter(FragmentActivity activity, List<GetMyPlayListResponseData> getMyPlayListResponseData) {
    }


    @Override
    public DownloadAlbumListAdapter.HeaderHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_items_downloads, parent, false);

        return new DownloadAlbumListAdapter.HeaderHolder(itemView);


        // return new HeaderHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.chapter_item, null));
    }

    @Override
    public void onBindViewHolder(final DownloadAlbumListAdapter.HeaderHolder holder, final int position) {


        holder.mLinearShare.setVisibility(View.GONE);

        //holder.imgWallaper.setImageResource();
        File fileo = new File(context.getApplicationContext().getFilesDir(), songDetailDownload.get(position).getCover());
        String url = fileo.getAbsolutePath();

        File outputFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), songDetailDownload.get(position).getCover());
        // String url=outputFile.getAbsolutePath();

        try {


            if (!url.trim().equalsIgnoreCase("")) {
                Picasso.with(context).load(fileo).error(R.drawable.default_music).into(holder.imgWallaper);
            }


        } catch (Exception e) {

        }


        // for Image get COVER
        // for play decrpt  in temp file give url to player and delete it after finsh Activity

        String newTitle = songDetailDownload.get(position).getAlbum_name();
        holder.txtTitle.setText(newTitle);
        holder.txtTitle.setVisibility(View.VISIBLE);

        holder.layoutPlayList.setBackgroundColor(context.getResources().getColor(R.color.md_black_1000));
        holder.imgWallaper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


        holder.btnDownload.setImageResource(R.drawable.ic_delete_white_24dp);

        /*holder.mTxtType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                // Showing Alert Message

                AlertDialog.Builder builder = new AlertDialog.Builder(
                        context, AlertDialog.THEME_HOLO_LIGHT);
                builder.setTitle("Do you want to delete File ?");
                builder.setMessage("Name:" + songDetailDownload.get(position).getFile().replace("%20", " "));
                builder.setInverseBackgroundForced(true);
                builder.setNegativeButton("NO",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                dialog.dismiss();

                            }
                        });
                builder.setPositiveButton("YES",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {

                                DashBoardActivity.context.deleteFileData(songDetailDownload.get(position));
                                dialog.dismiss();

                            }
                        });
                builder.show();

            }
        });*/


        if (songDetailDownload.get(position).getPlayStop()) {

            holder.btn_play.setImageResource(R.drawable.ic_pause);
        } else {
            holder.btn_play.setImageResource(R.drawable.play);
        }

        SongDetail songDetails = songDetailDownload.get(position);
        if (songDetails.getTypeOfSong().equals("ADO")) {
            holder.btn_play.setVisibility(View.VISIBLE);
        } else {
            holder.btn_play.setVisibility(View.VISIBLE);

        }


        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(context, songDetailDownload.get(position).getAlbum_name(), Toast.LENGTH_SHORT).show();

                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_ALBUM_NAME,songDetailDownload.get(position).getAlbum_name());
                fragmentDownloads.callMyAlbumsongs();
            }
        });

        /*holder.playLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentDownloads.startLoader();
                SongDetail songDetails = songDetailDownload.get(position);

                fragmentDownloads.playPoase(position);


                if (songDetails.getTypeOfSong().equals("ADO")) {


                    songDetails.setId("1");

                    playSong(holder, songDetails, position);
                } else {
                    // Play VDO

                    FromDownoadFile = true;
                    Intent intent = new Intent(context, VideoPlayerActivity.class);
                    intent.putExtra("url", songDetails.getFile());
                    context.startActivity(intent);
                }
            }
        });*/
//        if (position > lastPosition) {
//
//            Animation animation = AnimationUtils.loadAnimation(context,
//                    R.anim.list_items);
//            holder.itemView.startAnimation(animation);
//            lastPosition = position;
//        }

    }

    @Override
    public int getItemCount() {
        return songDetailDownload.size();
    }


    class HeaderHolder extends RecyclerView.ViewHolder {
        private TextView txtTitle, mTxtType;
        private LinearLayout layoutPlayList, playLayout;
        private ImageView imgWallaper, btn_play, btnDownload;
        private View view;
        private LinearLayout mLinearShare;

        public HeaderHolder(View itemView) {
            super(itemView);

            imgWallaper = (ImageView) itemView.findViewById(R.id.playlsitImage);
            mLinearShare = (LinearLayout) itemView.findViewById(R.id.btn_share);
            btn_play = (ImageView) itemView.findViewById(R.id.btn_play);
            mTxtType = (TextView) itemView.findViewById(R.id.txtTypeSOng);
            mTxtType.setVisibility(View.GONE);
            btnDownload = (ImageView) itemView.findViewById(R.id.btnDownload);
            txtTitle = (TextView) itemView.findViewById(R.id.txtPlayListName);
            view = itemView;

            layoutPlayList = (LinearLayout) itemView.findViewById(R.id.layoutPlayList);
            playLayout = (LinearLayout) itemView.findViewById(R.id.playLayout);


        }


    }
}




