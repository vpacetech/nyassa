package com.music.nyasaa.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.music.nyasaa.R;

public class MySongsAdapter extends RecyclerView.Adapter<MySongsAdapter.HeaderHolder> {

    LayoutInflater inflater;
    boolean header = false;

    Context context;
    private String[] name;
    private Integer[] img;
    private   int lastPosition = -1;


    public MySongsAdapter(Context context, Integer[] img, String[] name) {

        this.context = context;
        this.img = img;
        this.name = name;


    }


    @Override
    public MySongsAdapter.HeaderHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_items_songs_videos, parent, false);

        return new MySongsAdapter.HeaderHolder(itemView);


        // return new HeaderHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.chapter_item, null));
    }

    @Override
    public void onBindViewHolder(MySongsAdapter.HeaderHolder holder, int position) {



        holder.imgWallaper.setImageResource(img[position]);

        holder.txtTitle.setText(name[position]);
        holder.txtTitle.setVisibility(View.VISIBLE);


        holder.playImag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        holder.imgWallaper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  Intent intent = new Intent(context, ProfileActivity.class);
               // context.startActivity(intent);
                //((DashBoardActivity)context).overridePendingTransition(R.anim.left,R.anim.right);
            }
        });

//        if (position > lastPosition) {
//
//            Animation animation = AnimationUtils.loadAnimation(context,
//                    R.anim.list_items);
//            holder.itemView.startAnimation(animation);
//            lastPosition = position;
//        }




    }

    @Override
    public int getItemCount() {
        return 6 ;
    }


    class HeaderHolder extends RecyclerView.ViewHolder {
        private TextView txtTitle;
        private LinearLayout mNotificationLayout;
        private ImageView imgWallaper,playImag;


        public HeaderHolder(View itemView) {
            super(itemView);

            imgWallaper = (ImageView) itemView.findViewById(R.id.songImage);
            txtTitle = (TextView) itemView.findViewById(R.id.songTitle);
            playImag=(ImageView)itemView.findViewById(R.id.btn_song_play);


        }


    }
}


