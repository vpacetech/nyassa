package com.music.nyasaa.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.music.nyasaa.R;
import com.music.nyasaa.fragments.mysongs.FragmentMySongs;
import com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity;
import com.music.nyasaa.manager.MediaController;
import com.music.nyasaa.models.ResponseModel.GetMyPlayList.GetMyPlayListResponseData;
import com.music.nyasaa.models.SongDetail;
import com.music.nyasaa.utility.SharedPreferenceManager;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.music.nyasaa.fragments.playlistSongs.FragmentPlayListSongs.FragmentPlayListSongss;
import static com.music.nyasaa.fragments.playlistSongs.FragmentPlayListSongs.getMyPlayListResponseDataPlayList;
import static com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity.ForDOWNLOadSongDetail;
import static com.music.nyasaa.retrofit.Config.BASE_URL_FOR_PLAY;

public class PlayListMySongsAdapter extends RecyclerView.Adapter<PlayListMySongsAdapter.ViewHolder> {
    private Context context;
    // private List<List<SongDetail>> mSongsList;
    private DisplayImageOptions options;
    private int lastPosition = -1;

    private Dialog myPlayListDialog;
    private RecyclerView mMyPlayList;
    private TextView mTxtWarn;
    private SwipyRefreshLayout swipyRefreshLayout;
    private List<GetMyPlayListResponseData> getMyPlayListResponseData;
    private MyPlayListAdapters myPlayListAdapter;
    private FragmentMySongs fragmentMySongs;


    RotateAnimation rotate = new RotateAnimation(
            0, 360,
            Animation.RELATIVE_TO_SELF, 0.5f,
            Animation.RELATIVE_TO_SELF, 0.5f
    );


    public PlayListMySongsAdapter(Context activity, List<SongDetail> getMyPlayListResponseData) {

        this.context = activity;
        //  this.mSongsList = getMyPlayListResponseData;
        this.fragmentMySongs = fragmentMySongs;
        options = new DisplayImageOptions.Builder().showImageOnLoading(R.drawable.default_music).showImageForEmptyUri((int) R.drawable.default_music).showImageOnFail((int) R.drawable.bg_default_album_art).cacheInMemory(true).cacheOnDisk(true).considerExifParams(true).bitmapConfig(Bitmap.Config.RGB_565).build();
    }

    public PlayListMySongsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PlayListMySongsAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_playlist, parent, false));
    }

    public void onBindViewHolder(final PlayListMySongsAdapter.ViewHolder holder, final int position) {


        final SongDetail mDetail = getMyPlayListResponseDataPlayList.get(position);


        if (getMyPlayListResponseDataPlayList.get(position).getPlayStop()) {

            holder.btn_song_play.setImageResource(R.drawable.ic_pause);
        } else {
            holder.btn_song_play.setImageResource(R.drawable.play);
        }

        holder.btnDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ForDOWNLOadSongDetail = getMyPlayListResponseDataPlayList.get(position);
                DashBoardActivity.context.downloadfromOtherFragment();

            }
        });
        holder.songTitle.setText(mDetail.getName());

            Picasso.with(context)
                    .load(com.music.nyasaa.retrofit.Config.PIC_URL  + Uri.encode( mDetail.getCover()))
                    .placeholder(R.drawable.default_music)
                    .error(R.drawable.default_music)
                    .into(holder.cover);

        holder.btn_song_play.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                try
                {

                    String url=mDetail.getTrack_url()+"/";

                    BASE_URL_FOR_PLAY=url;
                }
                catch (Exception e)
                {

                }
                // holder.loadingSong.setVisibility(View.VISIBLE);


                FragmentPlayListSongss.playPoase(position);

                if (getMyPlayListResponseDataPlayList.get(position).getPlayStop()) {
                    DashBoardActivity.context. setStatus(true);
                    DashBoardActivity.context.setPlayer(false);
                    DashBoardActivity.context.onResume();
                    SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_MUSIC_MODE, "track");
                    DashBoardActivity.context.loadSongsDetails(mDetail);
//
//                }
                    //new DownloadMp3CacheTask().execute(mDetail.getUrl(),mDetail.display_name);
                    if (!MediaController.getInstance().isPlayingAudio(mDetail) || MediaController.getInstance().isAudioPaused()) {
                        MediaController.getInstance().stopAudio();
                        MediaController.getInstance().playMUSIC(mDetail);

                        //  holder.loadingSong.setVisibility(View.GONE);


//                    rotate.setDuration(900);
//                    rotate.setRepeatCount(Animation.INFINITE);
//                    itemImage.startAnimation(rotate);
                        // MediaController.getInstance().setPlaylist(mSongsList, mDetail, PhoneMediaControl.SonLoadFor.MostPlay.ordinal(), -1);
                    } else {
                        MediaController.getInstance().pauseAudio(mDetail);
                        DashBoardActivity.stop_rotateImage();
                    }

                    /*if (MusicSQLiteHelper.isExistQueueSong(mDetail)) {
                        MusicSQLiteHelper.updateQueue(mDetail);
                    } else {
                        MusicSQLiteHelper.insertSongInQueue(mDetail);
                    }*/
                } else {
                    DashBoardActivity.context. setStatus(false);
                    DashBoardActivity.context.setPlayer(true);
                    if (!MediaController.getInstance().isPlayingAudio(mDetail) || MediaController.getInstance().isAudioPaused()) {
                        MediaController.getInstance().stopAudio();
                        MediaController.getInstance().pauseAudio(mDetail);
//                        DashBoardActivity.stop_rotateImage();
                        // MediaController.getInstance().playAudio(mDetail);
                        //  holder.loadingSong.setVisibility(View.GONE);


//                    rotate.setDuration(900);
//                    rotate.setRepeatCount(Animation.INFINITE);
//                    itemImage.startAnimation(rotate);
                        // MediaController.getInstance().setPlaylist(mSongsList, mDetail, PhoneMediaControl.SonLoadFor.MostPlay.ordinal(), -1);
                    } else {
                        MediaController.getInstance().pauseAudio(mDetail);
                        DashBoardActivity.stop_rotateImage();
                    }
                }
            }
        });


        holder.mLinearLaypout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });

        holder.btn_song_share.setVisibility(View.GONE);
//
//        holder.btn_song_share.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_TRACK_TYPE, "5");
//                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_TRACK_ID, String.valueOf(mDetail.getId()));
//
//                DashBoardActivity dash = new DashBoardActivity();
//                dash.showCommDialog(context);
//            }
//        });




    }

    public int getItemCount() {
        return getMyPlayListResponseDataPlayList.size();
    }

    public class ViewHolder extends android.support.v7.widget.RecyclerView.ViewHolder {
        ImageView btn_song_play, btnDownload;
        ImageView cover;
        TextView songTitle;
        ProgressBar loadingSong;
        private LinearLayout mLinearLaypout,btn_song_share;

        public ViewHolder(View itemView) {
            super(itemView);
            songTitle = (TextView) itemView.findViewById(R.id.txtPlayListName);
            btn_song_play = (ImageView) itemView.findViewById(R.id.btn_play);
            btnDownload = (ImageView) itemView.findViewById(R.id.btnDownload);
            cover = (ImageView) itemView.findViewById(R.id.playlsitImage);
            loadingSong = (ProgressBar) itemView.findViewById(R.id.loadingSong);
            btn_song_share = (LinearLayout) itemView.findViewById(R.id.btn_share);
            mLinearLaypout = (LinearLayout) itemView.findViewById(R.id.layoutPlayList);
        }
    }
}