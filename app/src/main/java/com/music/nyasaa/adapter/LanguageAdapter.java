package com.music.nyasaa.adapter;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.music.nyasaa.R;
import com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity;
import com.music.nyasaa.models.ResponseModel.GetCategoryAll.GetResponseCategoryData;

import java.util.List;

public class LanguageAdapter extends RecyclerView.Adapter<LanguageAdapter.HeaderHolder> {

    LayoutInflater inflater;
    boolean header = false;
    String sDrawerID;
    Context context;

    private List<GetResponseCategoryData> menuListArrayList;
    int lastPosition = -1;
    private boolean subListFlagSong = false;
    private boolean subListFlag = false;

    public FragmentManager f_manager;
    private String fragVal = "";
    private DashBoardActivity dashBoardActivity;


    public LanguageAdapter(DashBoardActivity context, List<GetResponseCategoryData> strings) {


        this.context = context;
        this.menuListArrayList = strings;


    }


    @Override
    public LanguageAdapter.HeaderHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_items_menu, parent, false);

        return new LanguageAdapter.HeaderHolder(itemView);


        // return new HeaderHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.chapter_item, null));
    }

    @Override
    public void onBindViewHolder(final LanguageAdapter.HeaderHolder holder, final int position) {


        holder.txtTitle.setText(menuListArrayList.get(position).getName());
//
//        holder.mMenuLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_CAT_ORIGINAL,"");
//                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_LANG_ID, menuListArrayList.get(position).getId());
//            }
//        });


        if (position > lastPosition) {

            Animation animation = AnimationUtils.loadAnimation(context,
                    R.anim.list_items);
            holder.itemView.startAnimation(animation);
            lastPosition = position;
        }


    }

    @Override
    public int getItemCount() {
        return menuListArrayList.size();
    }


    class HeaderHolder extends RecyclerView.ViewHolder {
        private TextView txtTitle;
        private LinearLayout mMenuLayout;
        private ImageView imgMedia;


        public HeaderHolder(View itemView) {
            super(itemView);

            //   imgWallaper = (ImageView) itemView.findViewById(R.id.imgWall);
            txtTitle = (TextView) itemView.findViewById(R.id.txtMenuName);
            mMenuLayout = (LinearLayout) itemView.findViewById(R.id.menulayout);


        }


    }
}



