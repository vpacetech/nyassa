package com.music.nyasaa.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.music.nyasaa.R;
import com.music.nyasaa.front_end_layer.activities.community_members.AddMemberActivity;
import com.music.nyasaa.models.ResponseModel.GetSeacrchedMembers.GetSearchMemberResponseData;
import com.music.nyasaa.retrofit.Config;
import com.squareup.picasso.Picasso;

import java.util.List;

public class SerachMemberAdapter extends RecyclerView.Adapter<SerachMemberAdapter.HeaderHolder> {

    LayoutInflater inflater;
    boolean header = false;
    private Dialog mediaDialog;

    Context context;
    List<GetSearchMemberResponseData> getresponseSeacrhMembersList =null;
    private int lastPosition = -1;



    public SerachMemberAdapter(Context context, List<GetSearchMemberResponseData> responseGetCommunityMemDataList) {

        this.context=context;
        this.getresponseSeacrhMembersList =responseGetCommunityMemDataList;
    }


    @Override
    public SerachMemberAdapter.HeaderHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_items_community_members, parent, false);

        return new SerachMemberAdapter.HeaderHolder(itemView);


        // return new HeaderHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.chapter_item, null));
    }

    @Override
    public void onBindViewHolder(SerachMemberAdapter.HeaderHolder holder, final int position) {


        // holder.imgWallaper.setImageResource(img[position]);

        Picasso.with(context)
                .load(Config.BASE_PROFILE_PIC + Uri.encode( getresponseSeacrhMembersList.get(position).getProfilePhoto()))
                .placeholder(R.drawable.bg_default_album_art)
                .error(R.drawable.bg_default_album_art)
                .into(holder.imgWallaper);
        holder.txtTitle.setText(getresponseSeacrhMembersList.get(position).getUsername());
        holder.txtTitle.setVisibility(View.VISIBLE);
        holder.mTxtDesc.setText(getresponseSeacrhMembersList.get(position).getEmail());
        holder.imgWallaper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMediaDialog(getresponseSeacrhMembersList.get(position).getProfilePhoto());
            }
        });

       // holder.layoutPlayList.setBackgroundColor(context.getResources().getColor(R.color.black));


        holder.layoutPlayList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((AddMemberActivity)context).showMembersDialog(getresponseSeacrhMembersList.get(position).getUsername(),getresponseSeacrhMembersList.get(position).getId());

            }
        });

//        if (position > lastPosition) {
//
//            Animation animation = AnimationUtils.loadAnimation(context,
//                    R.anim.list_items);
//            holder.itemView.startAnimation(animation);
//            lastPosition = position;
//        }


    }

    @Override
    public int getItemCount() {
        return getresponseSeacrhMembersList.size();
    }


    public final  class HeaderHolder extends RecyclerView.ViewHolder {
        private TextView txtTitle,mTxtDesc;
        public RelativeLayout layoutPlayList;
        private ImageView imgWallaper;


        public HeaderHolder(View itemView) {
            super(itemView);



            imgWallaper = (ImageView) itemView.findViewById(R.id.playlsitImage);
            mTxtDesc=(TextView)itemView.findViewById(R.id.description);
            txtTitle = (TextView) itemView.findViewById(R.id.txtPlayListName);
            layoutPlayList = (RelativeLayout) itemView.findViewById(R.id.view_background);


        }


    }

    public void removeItem(int position) {
        getresponseSeacrhMembersList.remove(position);
        // notify the item removed by position
        // to perform recycler view delete animations
        // NOTE: don't call notifyDataSetChanged()
        notifyItemRemoved(position);
    }

    private void showMediaDialog(String url) {
        mediaDialog = new Dialog(context, android.R.style.Theme_Dialog);
        mediaDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        mediaDialog.setContentView(R.layout.dialog_media);
        mediaDialog.getWindow().getAttributes().width = ActionBar.LayoutParams.FILL_PARENT;
        mediaDialog.getWindow().setBackgroundDrawable(new ColorDrawable(context.getResources().getColor(R.color.blackTransnew)));
        WindowManager.LayoutParams wmlps = mediaDialog.getWindow().getAttributes();
        // mediaDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        Window window = mediaDialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        window.setGravity(Gravity.CENTER);
        ImageView imgWallPost = (ImageView) mediaDialog.findViewById(R.id.imageViewWallImage);


        Picasso.with(context)
                .load(Config.BASE_PROFILE_PIC + Uri.encode(url))
                .placeholder(R.drawable.bg_default_album_art)
                .error(R.drawable.bg_default_album_art)
                .into(imgWallPost);


        mediaDialog.show();
    }

}



