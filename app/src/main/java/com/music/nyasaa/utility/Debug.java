package com.music.nyasaa.utility;

import android.util.Log;

/**
 * Created by android on 1/16/2018.
 */

public class Debug {
    public static final boolean DEBUG_MODEL = false;

    public static void d(String str, String str2) {
        Log.d(str, str2);
    }

    public static void e(String str) {
        Log.e("TAG",str);
    }

    public static void e(String str, String str2) {
        Log.d(str, str2);
    }

    public static void e(String str, String str2, Throwable th) {
        Log.e(str, str2, th);
    }

    public static void e(String str, Throwable th) {
        Log.e("TAG",str,th);
    }

    public static void i(String str, String str2) {
        Log.i(str, str2);
    }



    public static void println(Object obj) {
        System.out.println(obj);
    }

    public static void println(String str) {
        System.out.println(str);
    }
}
