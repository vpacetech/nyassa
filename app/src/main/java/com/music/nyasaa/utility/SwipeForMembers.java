package com.music.nyasaa.utility;

import android.graphics.Canvas;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

import com.music.nyasaa.adapter.CommunityMembersAdapter;

public class SwipeForMembers extends ItemTouchHelper.SimpleCallback {
    private SwipeForMembers.RecyclerItemTouchHelperListener listener;

    public SwipeForMembers(int dragDirs, int swipeDirs, SwipeForMembers.RecyclerItemTouchHelperListener listener) {
        super(dragDirs, swipeDirs);
        this.listener = listener;
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        return true;
    }

    @Override
    public void onSelectedChanged(RecyclerView.ViewHolder viewHolder, int actionState) {
        if (viewHolder != null) {
            try {
                final View foregroundView = ((CommunityMembersAdapter.HeaderHolder) viewHolder).layoutPlayList;
                getDefaultUIUtil().onSelected(foregroundView);
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onChildDrawOver(Canvas c, RecyclerView recyclerView,
                                RecyclerView.ViewHolder viewHolder, float dX, float dY,
                                int actionState, boolean isCurrentlyActive) {
        try {
            final View foregroundView = ((CommunityMembersAdapter.HeaderHolder) viewHolder).layoutPlayList;
            getDefaultUIUtil().onDrawOver(c, recyclerView, foregroundView, dX, dY,
                    actionState, isCurrentlyActive);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        try {
            final View foregroundView = ((CommunityMembersAdapter.HeaderHolder) viewHolder).layoutPlayList;
            getDefaultUIUtil().clearView(foregroundView);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onChildDraw(Canvas c, RecyclerView recyclerView,
                            RecyclerView.ViewHolder viewHolder, float dX, float dY,
                            int actionState, boolean isCurrentlyActive) {
        try {
            final View foregroundView = ((CommunityMembersAdapter.HeaderHolder) viewHolder).layoutPlayList;

            getDefaultUIUtil().onDraw(c, recyclerView, foregroundView, dX, dY,
                    actionState, isCurrentlyActive);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        listener.onSwiped(viewHolder, direction, viewHolder.getAdapterPosition());
    }

    @Override
    public int convertToAbsoluteDirection(int flags, int layoutDirection) {
        return super.convertToAbsoluteDirection(flags, layoutDirection);
    }

    public interface RecyclerItemTouchHelperListener {

        void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position);


    }


}
