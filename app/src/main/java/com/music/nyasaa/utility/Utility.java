package com.music.nyasaa.utility;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.music.nyasaa.R;


/**
 * Created by PrathaM on 2/2/2017.
 */

public class Utility {


    final String TAG = "Utility";
    private static Utility mUtility;
    private static Context mContext;

    private Dialog dialog;
    private ProgressDialog mProgressDialog;


    public static Utility getSharedInstance() {
        if (mUtility == null) {
            mUtility = new Utility();
        }
        return mUtility;
    }


    public boolean isConnectedToInternet(Context context) {
        //mContext = context;
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            @SuppressLint("MissingPermission") NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }


    public void Snackbar(View view, String msg) {

        Snackbar snackbar = Snackbar
                .make(view, msg, Snackbar.LENGTH_LONG);

        View view1 = snackbar.getView();
        TextView tv = (TextView) view1.findViewById(android.support.design.R.id.snackbar_text);
        tv.setGravity(Gravity.CENTER_HORIZONTAL);
        tv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        snackbar.show();

    }


    public void showProgressDialog(Context mContext) {

//        kProgressHUD = new KProgressHUD(mContext)
//                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
//                .setLabel(label)
//                .setDetailsLabel(Dlabel)
//                .setCancellable(true)
//                .setAnimationSpeed(2)
//                .setDimAmount(0.5f)
//                .show();

//        mProgressDialog = new ProgressDialog(mContext);
//        mProgressDialog.setIndeterminate(false);
//        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//        mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//       // mProgressDialog.show(mContext, "", "");
//        mProgressDialog.getWindow().setGravity(Gravity.CENTER);
//        WindowManager.LayoutParams params = mProgressDialog.getWindow().getAttributes();
//        params.y = 500;
//        mProgressDialog.getWindow().setAttributes(params);
//        mProgressDialog.show();

        try {

            dialog = new Dialog(mContext);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialog.setContentView(R.layout.dialog_progress);
            // dialog.getWindow().getAttributes().width = ActionBar.LayoutParams.FILL_PARENT;
            dialog.show();
            dialog.setCancelable(true);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public void dismissProgressDialog() {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }


}
