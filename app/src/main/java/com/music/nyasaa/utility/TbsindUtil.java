package com.music.nyasaa.utility;

import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.util.Log;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Random;

import static android.content.Context.DOWNLOAD_SERVICE;
import static android.content.Context.MODE_PRIVATE;

//import com.coolerfall.download.DownloadCallback;
//import com.coolerfall.download.DownloadManager;
//import com.coolerfall.download.DownloadRequest;
//import com.coolerfall.download.OkHttpDownloader;
//import com.coolerfall.download.Priority;


public class TbsindUtil {
    //http://52.8.218.210/music_beta/AndroidPhp/index.php/AndroidApis/getSongs
    //SERVER CONTROLLER HEADER
    public static final String SERVER_URL_PHP = "http://52.8.218.210/music_beta/AndroidPhp/index.php/AndroidApis/";

    //SERVER CONTROLLER FUNCTION
    public static final String SERVER_URL_GET_SEARCH_DATA = "http://52.8.218.210/music_beta/AndroidPhp/index.php/AndroidApis/getSearchData";
    public static final String SERVER_URL_GET_ALL_ALBUMS = SERVER_URL_PHP + "getHomePageData";
    public static final String SERVER_URL_GET_ALL_SONGS = SERVER_URL_PHP + "getSongs";
    public static final String SERVER_URL_GET_ALL_GENRE = SERVER_URL_PHP + "getCategories";
    public static final String SERVER_URL_GET_ALL_GENRE_ARTISTS_ALBUM_DATA = SERVER_URL_PHP + "getCategoryData";
    public static final String SERVER_URL_GET_ALL_GENRE_ARTISTS_SONGS = SERVER_URL_PHP + "genreSongs";
    public static final String SERVER_URL_GET_ALL_DISCOVER_SONGS = SERVER_URL_PHP + "discoverSongs";
    public static final String SERVER_URL_GET_ARTISTS = SERVER_URL_PHP + "getArtists";
    public static final String SERVER_URL_GET_ARTISTS_SONGS = SERVER_URL_PHP + "getArtistSongs";
    public static final String SERVER_URL_REGISTER = SERVER_URL_PHP + "getRegister";
    public static final String SERVER_URL_LOGIN = SERVER_URL_PHP + "getLogin";
    public static final String SERVER_URL_FORGET = SERVER_URL_PHP + "forgotPassword";
    public static final String SERVER_URL_CONTACT_US = SERVER_URL_PHP + "contactUs";
    public static final String SERVER_URL_CHANGE_PASS = SERVER_URL_PHP + "changePassword";
    public static final String SERVER_URL_GET_LANGUAGES = SERVER_URL_PHP + "getLanguages";

    public static String videoId="B2OuPihKuOw";
   static byte [] incrept;

  static   byte [] decrpt;

    Context ctx;

   static private final String KEY = "abc";
    //Image Directory
    // public static final String SERVER_URL_IMAGE_DIR = "http://52.172.184.250/DEVELOPMENT/music_beta/admin/uploads/song_package/app_image/";
    public static final String SERVER_URL_IMAGE_DIR_GENRE = "http://52.172.184.250/DEVELOPMENT/music_beta/admin/uploads/genre_categories/app_image/";
    public static final String SERVER_URL_IMAGE_DIR_ARTISTS = "http://52.172.184.250/DEVELOPMENT/music_beta/admin/uploads/artists/app_image/";
    private static final float BLUR_RADIUS = 25f;
    private final String LANGID = "langId";
    private final String LANGNAME = "langName";
    private SharedPreferences app_prefs;
    public static boolean ClickPlaySong = true;
    private Context context;

    public static boolean isOnline(Context context) {
        boolean status = false;
        try {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            if (netInfo != null && netInfo.getState() == NetworkInfo.State.CONNECTED) {
                status = true;
            } else {
                netInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
                if (netInfo != null && netInfo.getState() == NetworkInfo.State.CONNECTED)
                    status = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return status;
    }

    public static boolean isNull(Object str) {
        return str == (null) || str.toString().equals("")
                || str.toString().equals(" ")
                || str.toString().trim().length() == 0
                || str.toString().trim().equals("null");
    }

    public static String getConvertedDuration(String time) {

        String[] units = time.split(":"); //will break the string up into an array
        int minutes = Integer.parseInt(units[0]); //first element
        int seconds = Integer.parseInt(units[1]); //second element
        int duration = 60 * minutes + seconds;
        System.out.println(duration);
        Log.d("TAG", "convert time " + duration);
        return String.valueOf(duration);
    }

    public static int randInt(int min, int max) {

        Random rand = new Random();

        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }


    public static Bitmap DownloadImage(String URL) {
        System.out.println("image inside=" + URL);
        Bitmap bitmap = null;
        InputStream in;
        try {
            in = OpenHttpConnection(URL);
            bitmap = BitmapFactory.decodeStream(in);
            in.close();
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        System.out.println("image last" + bitmap);
        return bitmap;
    }

    private static InputStream OpenHttpConnection(String urlString)
            throws IOException {
        InputStream in = null;
        int response = -1;

        URL url = new URL(urlString);
        URLConnection conn = url.openConnection();

        if (!(conn instanceof HttpURLConnection))
            throw new IOException("Not an HTTP connection");

        try {
            HttpURLConnection httpConn = (HttpURLConnection) conn;
            httpConn.setAllowUserInteraction(false);
            httpConn.setInstanceFollowRedirects(true);
            httpConn.setRequestMethod("GET");
            httpConn.connect();

            response = httpConn.getResponseCode();
            if (response == HttpURLConnection.HTTP_OK) {
                in = httpConn.getInputStream();
            }
        } catch (Exception ex) {
            throw new IOException("Error connecting");
        }
        return in;
    }

    public static void CopyStream(InputStream is, OutputStream os) {
        final int buffer_size = 1024;
        try {
            byte[] bytes = new byte[buffer_size];
            for (; ; ) {
                int count = is.read(bytes, 0, buffer_size);
                if (count == -1)
                    break;
                os.write(bytes, 0, count);
            }
        } catch (Exception ex) {
        }
    }

    public static Bitmap retriveVideoFrameFromVideo(String videoPath) throws Throwable {
        Log.d("TAG", "video source path" + videoPath);
        Bitmap bitmap = null;
        MediaMetadataRetriever mediaMetadataRetriever = null;
        try {
            mediaMetadataRetriever = new MediaMetadataRetriever();
            if (Build.VERSION.SDK_INT >= 14)
                mediaMetadataRetriever.setDataSource(videoPath, new HashMap<String, String>());
            else
                mediaMetadataRetriever.setDataSource(videoPath);
            bitmap = mediaMetadataRetriever.getFrameAtTime();
        } catch (Exception e) {
            e.printStackTrace();
            throw new Throwable("Exception in retriveVideoFrameFromVideo(String videoPath)" + e.getMessage());

        } finally {
            if (mediaMetadataRetriever != null) {
                mediaMetadataRetriever.release();
            }
        }
        Log.d("TAG", "video source path converted to bitmap " + bitmap);
        return bitmap;
    }

    public static Bitmap blur(Context context, Bitmap image) {
        if (null == image) return null;

        Bitmap outputBitmap = Bitmap.createBitmap(image);
        final RenderScript renderScript = RenderScript.create(context);
        Allocation tmpIn = Allocation.createFromBitmap(renderScript, image);
        Allocation tmpOut = Allocation.createFromBitmap(renderScript, outputBitmap);

        //Intrinsic Gausian blur filter
        ScriptIntrinsicBlur theIntrinsic = ScriptIntrinsicBlur.create(renderScript, Element.U8_4(renderScript));
        theIntrinsic.setRadius(BLUR_RADIUS);
        theIntrinsic.setInput(tmpIn);
        theIntrinsic.forEach(tmpOut);
        tmpOut.copyTo(outputBitmap);
        return outputBitmap;
    }

    public static void startDownload(Context mContext, String downloadUrl, String filename) {
        File direct = new File(Environment.getExternalStorageDirectory()
                + "/.devotional");

        if (!direct.exists()) {
            direct.mkdirs();
        }


        DownloadManager.Request localRequest = new DownloadManager.Request(Uri.parse(downloadUrl));
        localRequest.allowScanningByMediaScanner();
        localRequest.setAllowedNetworkTypes(
                DownloadManager.Request.NETWORK_WIFI
                        | DownloadManager.Request.NETWORK_MOBILE);
       // localRequest.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        localRequest.setShowRunningNotification(false);
        localRequest.setTitle(filename);
        localRequest.setDestinationInExternalPublicDir("/.devotional", filename);
        ((DownloadManager) mContext.getSystemService(DOWNLOAD_SERVICE)).enqueue(localRequest);
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT); //This is important!
        intent.setType("*/*");
        Toast.makeText(mContext, "Offline Saving...", Toast.LENGTH_SHORT).show();
    }

    public TbsindUtil(Context context) {
        app_prefs = context.getSharedPreferences("langPrefs", MODE_PRIVATE);
    }

    public void saveLang(String langId, String langName) {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putString("langId", langId);
        edit.putString("langName", langName);
        edit.commit();
    }

    public String getLangId() {
        return app_prefs.getString("langId", "");
    }

    public String getLangName() {
        return app_prefs.getString("langName", "");
    }

//    public static void AudioEncrypt(Context mContext, String downloadUrl, String filename){
//        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
//        StrictMode.setThreadPolicy(policy);
//        VincentFileCrypto simpleCrypto = new VincentFileCrypto();
//
//        byte[] inarry=null;
//        try {
//            URL wallpaperURL = new URL(downloadUrl);
//            InputStream inputStream = new BufferedInputStream(wallpaperURL.openStream(), 10240);
//            FileOutputStream outputStream = new FileOutputStream(new File(SongCacheUtil.instance().getCacheFolder(mContext), filename));
//            ByteArrayOutputStream output = new ByteArrayOutputStream();
//            byte[] buffer = new byte[1024];
//            int bytesRead=inputStream.read(buffer);
//            while (true) {
//                int dataSize = inputStream.read(buffer);
//                if (dataSize == -1) {
//                    break;
//                }
//                output.write(buffer, 0, bytesRead);
//                outputStream.write(buffer, 0, dataSize);
//                inarry  = output.toByteArray();
//                incrept = simpleCrypto.encrypt(KEY, inarry);
//            }
//            outputStream.close();
//
//        } catch (IOException e) {
//
//
//
//            e.printStackTrace();
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }




  //  }

    public static byte[] getAudioFileFromSdCard(File file) throws FileNotFoundException
    {
        byte[] inarry = null;

        try {


            //Convert file into array of bytes.

            FileInputStream fileInputStream=null;

            byte[] bFile = new byte[(int) file.length()];

            fileInputStream = new FileInputStream(file);

            fileInputStream.read(bFile);

            fileInputStream.close();

            inarry = bFile;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return inarry;

    }
    private void playMp3(byte[] mp3SoundByteArray) {

        try {

            // create temp file that will hold byte array

            File tempMp3 = File.createTempFile("kurchina", "mp3");

            tempMp3.deleteOnExit();

            FileOutputStream fos = new FileOutputStream(tempMp3);

            fos.write(mp3SoundByteArray);

            fos.close();


            MediaPlayer mediaPlayer = new MediaPlayer();

            FileInputStream fis = new FileInputStream(tempMp3);

         Log.d("TAG",""+fis.getFD());



            mediaPlayer.prepare();

            mediaPlayer.start();

        } catch (IOException ex) {



            ex.printStackTrace();

        }

    }

    public static String getActualMediaThumbnail(Context mContext,String url){
        Log.d("TAG","MEDIA PATH URL IMAGE "+url);
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        retriever.setDataSource(url);
        byte[] data = retriever.getEmbeddedPicture();

        // convert the byte array to a bitmap
        Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);


        retriever.release();
        return getImageUri(mContext,bitmap);
    } public static String getActualMediaDuration(String url){
        Log.d("TAG","MEDIA PATH URL "+url);
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        retriever.setDataSource(url);
        long duration = Long.parseLong(retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION));
        long actual = duration / 1000;



        retriever.release();
        return String.valueOf(actual);
    }
    public static String getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        Log.d("TAG", "Path " + path);
        return path;
    }

}
