package com.music.nyasaa.dbhandler;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import com.music.nyasaa.controller.MyApplication;
import com.music.nyasaa.models.SongDetail;


public class FavoritePlayTableHelper {
    public static final String ALBUM_ID = "album_id";
    public static final String ARTIST = "artist";
    public static final String AUDIOPROGRESS = "audioProgress";
    public static final String AUDIOPROGRESSSEC = "audioProgressSec";
    public static final String DISPLAY_NAME = "display_name";
    public static final String DURATION = "duration";
    public static final String ID = "_id";
    public static final String IMAGE_URL = "url";
    public static final String ISFAVORITE = "isfavorite";
    public static final String LYRICS = "content";
    public static final String LastPlayTime = "lastplaytime";
    public static final String PATH = "path";
    public static final String TABLENAME = "ResentPlay";
    public static final String TITLE = "title";
    public static final String TYPE = "type";
    private static HinduMusicDBHelper dbHelper = null;
    private static FavoritePlayTableHelper mInstance;
    public Context context;
    private SQLiteDatabase sampleDB;

    public static synchronized FavoritePlayTableHelper getInstance(Context context) {
        FavoritePlayTableHelper favoritePlayTableHelper;
        synchronized (FavoritePlayTableHelper.class) {
            if (mInstance == null) {
                mInstance = new FavoritePlayTableHelper(context);
            }
            favoritePlayTableHelper = mInstance;
        }
        return favoritePlayTableHelper;
    }

    public FavoritePlayTableHelper(Context context_) {
        this.context = context_;
        if (dbHelper == null) {
            dbHelper = ((MyApplication) this.context.getApplicationContext()).DB_HELPER;
        }
    }

    public void inserSong(SongDetail songDetail, int isFav) {
        try {

            sampleDB = dbHelper.getDB();
            sampleDB.beginTransaction();

            String sql = "Insert or Replace into " + TABLENAME + " values(?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
            SQLiteStatement insert = sampleDB.compileStatement(sql);

            try {
                if (songDetail != null) {
                    insert.clearBindings();
                    insert.bindLong(1, songDetail.getId());
                    insert.bindLong(2, songDetail.getAlbum_id());
                    insert.bindString(3, songDetail.getArtist());
                    insert.bindString(4, songDetail.getTitle());
                    insert.bindString(5, songDetail.getDisplay_name());
                    insert.bindString(6, songDetail.getUrl());
                    insert.bindString(7, songDetail.getDuration());
                    insert.bindString(8, songDetail.getPath());
                    insert.bindString(9, songDetail.getType());
                    insert.bindString(10, songDetail.getLyrics());
                    insert.bindString(11, songDetail.audioProgress + "");
                    insert.bindString(12, songDetail.audioProgressSec + "");
                    insert.bindString(13, System.currentTimeMillis() + "");
                    insert.bindLong(14, (long) isFav);

                    insert.execute();
                    Log.d("TAG", "Data insert successfully " + songDetail.getId() + "\n" +
                            songDetail.getAlbum_id() + "\n" +
                            songDetail.getArtist() + "\n" +
                            songDetail.getTitle() + "\n" +
                            songDetail.getDisplay_name() + "\n" +
                            songDetail.getUrl() + "\n" +
                            songDetail.getDuration() + "\n" +
                            songDetail.getPath());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            sampleDB.setTransactionSuccessful();

        } catch (Exception e) {
            Log.e("XML:", e.toString());
        } finally {
            sampleDB.endTransaction();
        }
    }


    private void closeCurcor(Cursor cursor) {
        if (cursor != null) {
            cursor.close();
        }
    }

    public Cursor getFavoriteSongList() {
        Cursor mCursor = null;
        try {
          sampleDB = dbHelper.getDB();
            mCursor = this.sampleDB.rawQuery("Select * from ResentPlay where isfavorite=1", null);
        } catch (Exception e) {
            closeCurcor(mCursor);
            e.printStackTrace();
        }
        return mCursor;
    }
}
