package com.music.nyasaa.dbhandler.DBModel;

public class TblUserMst {
    String username,email,dob,mobile,id;

    public TblUserMst() {
    }

    public TblUserMst(String username, String email, String dob, String mobile, String id) {
        this.username = username;
        this.email = email;
        this.dob = dob;
        this.mobile = mobile;
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
