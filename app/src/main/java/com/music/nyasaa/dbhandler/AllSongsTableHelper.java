package com.music.nyasaa.dbhandler;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import com.music.nyasaa.controller.MyApplication;
import com.music.nyasaa.models.SongDetail;


public class AllSongsTableHelper {
    public static final String TABLENAME = "AllSong";

    public static final String ID = "_id";
    public static final String ALBUM_ID = "album_id";
    public static final String ARTIST = "artist";
    public static final String TITLE = "title";
    public static final String DISPLAY_NAME = "display_name";
    public static final String DURATION = "duration";
    public static final String PATH = "path";
    public static final String AUDIOPROGRESS = "audioProgress";
    public static final String AUDIOPROGRESSSEC = "audioProgressSec";


    private static HinduMusicDBHelper dbHelper = null;
    private static AllSongsTableHelper mInstance;
    private SQLiteDatabase sampleDB;


    public static synchronized AllSongsTableHelper getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new AllSongsTableHelper(context);
        }
        return mInstance;
    }

    public Context context;

    public AllSongsTableHelper(Context context_) {
        this.context = context_;
        if (dbHelper == null) {
            dbHelper = ((MyApplication) context.getApplicationContext()).DB_HELPER;
        }
    }

    public void insertSong(SongDetail songDetail) {
        try {

            sampleDB = dbHelper.getDB();
            sampleDB.beginTransaction();

            String sql = "Insert or Replace into " + TABLENAME + " values(?,?,?,?,?,?,?,?,?);";
            SQLiteStatement insert = sampleDB.compileStatement(sql);

            try {
                if (songDetail != null) {
                    insert.clearBindings();
                    insert.bindLong(1, songDetail.getId());
                    insert.bindLong(2, songDetail.getAlbum_id());
                    insert.bindString(3, songDetail.getArtist());
                    insert.bindString(4, songDetail.getTitle());
                    insert.bindString(5, songDetail.getDisplay_name());
                    insert.bindString(6, songDetail.getDuration());
                    insert.bindString(7, songDetail.getPath());
                    insert.bindString(8, songDetail.audioProgress + "");
                    insert.bindString(9, songDetail.audioProgressSec + "");
                    insert.execute();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            sampleDB.setTransactionSuccessful();

        } catch (Exception e) {
            Log.e("XML:", e.toString());
        } finally {
            sampleDB.endTransaction();
        }
    }

    private void closeCurcor(Cursor cursor) {
        if (cursor != null) {
            cursor.close();
            cursor = null;
        }
    }

    public Cursor getFavoriteSongList() {
        Cursor mCursor = null;
        try {
            String sqlQuery = "Select * from " + TABLENAME ;
            sampleDB = dbHelper.getDB();
            mCursor = sampleDB.rawQuery(sqlQuery, null);
        } catch (Exception e) {
            closeCurcor(mCursor);
            e.printStackTrace();
        }
        return mCursor;
    }


}
