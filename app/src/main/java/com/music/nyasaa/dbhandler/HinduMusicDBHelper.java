package com.music.nyasaa.dbhandler;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.music.nyasaa.R;


public class HinduMusicDBHelper extends SQLiteOpenHelper {
    private static String DATABASE_NAME = "";
    private static int DATABASE_VERSION = 0;
    private String DB_PATH = "";
    private Context context_;
    private SQLiteDatabase db;

    public HinduMusicDBHelper(Context context) {
        super(context, context.getResources().getString(R.string.DataBaseName), null, Integer.parseInt(context.getResources().getString(R.string.DataBaseName_Version)));
        this.context_ = context;
        DATABASE_NAME = context.getResources().getString(R.string.DataBaseName);
        DATABASE_VERSION = Integer.parseInt(context.getResources().getString(R.string.DataBaseName_Version));
        this.DB_PATH = context.getDatabasePath(DATABASE_NAME).getPath();
        context.openOrCreateDatabase(DATABASE_NAME, 0, null);
    }

    public SQLiteDatabase getDB() {
        return this.db;
    }

    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL(sqlForCreateMostPlay());
            db.execSQL(sqlForCreateFavoritePlay());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            db.execSQL("DROP TABLE IF EXISTS MostPlay");
            db.execSQL("DROP TABLE IF EXISTS ResentPlay");
            onCreate(db);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public synchronized void close() {
        if (getDB() != null) {
            getDB().close();
        }
        super.close();
    }

    public void openDataBase() throws SQLException {
        try {
            this.db = SQLiteDatabase.openDatabase(this.DB_PATH, null, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static String sqlForCreateMostPlay() {
        String sql = "CREATE TABLE " + MostAndRecentPlayTableHelper.TABLENAME + " (" + MostAndRecentPlayTableHelper.ID + " INTEGER NOT NULL PRIMARY KEY,"
                + MostAndRecentPlayTableHelper.ALBUM_ID + " INTEGER NOT NULL,"

                + MostAndRecentPlayTableHelper.ARTIST + " TEXT NOT NULL,"
                + MostAndRecentPlayTableHelper.TITLE + " TEXT NOT NULL,"
                + MostAndRecentPlayTableHelper.DISPLAY_NAME + " TEXT NOT NULL,"
                + MostAndRecentPlayTableHelper.DURATION + " TEXT NOT NULL,"
                + MostAndRecentPlayTableHelper.PATH + " TEXT NOT NULL,"
                + MostAndRecentPlayTableHelper.TYPE + " TEXT NOT NULL,"
                + MostAndRecentPlayTableHelper.LYRICS + " TEXT NOT NULL,"
                + MostAndRecentPlayTableHelper.AUDIOPROGRESS + " TEXT NOT NULL,"
                + MostAndRecentPlayTableHelper.AUDIOPROGRESSSEC + " INTEGER NOT NULL,"
                + MostAndRecentPlayTableHelper.LastPlayTime + " TEXT NOT NULL,"
                + MostAndRecentPlayTableHelper.PLAYCOUNT + " INTEGER NOT NULL);";
        return sql;
    }
    public static String sqlForCreateFavoritePlay() {
        String sql = "CREATE TABLE " + FavoritePlayTableHelper.TABLENAME + " ("
                + FavoritePlayTableHelper.ID + " INTEGER NOT NULL PRIMARY KEY,"
                + FavoritePlayTableHelper.ALBUM_ID + " INTEGER NOT NULL,"
                + FavoritePlayTableHelper.ARTIST + " TEXT NOT NULL,"
                + FavoritePlayTableHelper.TITLE + " TEXT NOT NULL,"
                + FavoritePlayTableHelper.DISPLAY_NAME + " TEXT NOT NULL,"
                + FavoritePlayTableHelper.DURATION + " TEXT NOT NULL,"
                + FavoritePlayTableHelper.IMAGE_URL + " TEXT NOT NULL,"
                + FavoritePlayTableHelper.PATH + " TEXT NOT NULL,"
                + FavoritePlayTableHelper.TYPE + " TEXT NOT NULL,"
                + FavoritePlayTableHelper.LYRICS + " TEXT NOT NULL,"
                + FavoritePlayTableHelper.AUDIOPROGRESS + " TEXT NOT NULL,"
                + FavoritePlayTableHelper.AUDIOPROGRESSSEC + " INTEGER NOT NULL,"
                + FavoritePlayTableHelper.LastPlayTime + " TEXT NOT NULL,"
                + FavoritePlayTableHelper.ISFAVORITE + " INTEGER NOT NULL);";
        return sql;
    }
    public static String sqlForCreateAllSongs() {
        String sql = "CREATE TABLE " + AllSongsTableHelper.TABLENAME + " (" + AllSongsTableHelper.ID + " INTEGER NOT NULL PRIMARY KEY,"
                + AllSongsTableHelper.ALBUM_ID + " INTEGER NOT NULL,"
                + AllSongsTableHelper.ARTIST + " TEXT NOT NULL,"
                + AllSongsTableHelper.TITLE + " TEXT NOT NULL,"
                + AllSongsTableHelper.DISPLAY_NAME + " TEXT NOT NULL,"
                + AllSongsTableHelper.DURATION + " TEXT NOT NULL,"
                + AllSongsTableHelper.PATH + " TEXT NOT NULL,"
                + AllSongsTableHelper.AUDIOPROGRESS + " TEXT NOT NULL,"
                + AllSongsTableHelper.AUDIOPROGRESSSEC + " INTEGER NOT NULL);";
        return sql;
    }


}
