package com.music.nyasaa.dbhandler;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


import com.music.nyasaa.dbhandler.DBModel.TblUserMst;
import com.music.nyasaa.models.LanguageModel;
import com.music.nyasaa.models.SongDetail;

import java.util.ArrayList;
import java.util.List;

public class MusicSQLiteHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "Music.DB";
    private static final int DATABASE_VERSION = 2;
    private static final boolean DEBUG = true;
    private static final String LOG_TAG = "MusicSQLiteHelper";
    private static final String TBL_ALL_SONGS = "all_songs_list";
    private static final String TBL_ARTISTS = "artists";
    private static final String TBL_BROWSE = "browse";
    private static final String TBL_DISCOVER = "discover";
    private static final String TBL_DOWNLOADED = "downloaded";
    private static final String TBL_GENRE = "genre";
    private static final String TBL_LANGUAGE = "languages";
    private static final String TBL_QUEUE = "queue";
    private static final String TBL_USER = "user";


    private static final String CREATE_TBL_LANGUAGE = "CREATE TABLE IF NOT EXISTS "
            + TBL_LANGUAGE
            + " (\n"
            + "_id               INTEGER NOT NULL PRIMARY KEY,\n"
            + "lang_id           TEXT  NULL,\n"
            + "title             TEXT  NULL\n"
            + ")";

    private static final String CREATE_TBL_USER = "CREATE TABLE IF NOT EXISTS "
            + TBL_USER
            + " (\n"
            + "_id               INTEGER NOT NULL PRIMARY KEY,\n"
            + "user_id           TEXT  NULL,\n"
            + "username          TEXT  NULL,\n"
            + "mobile            TEXT  NULL,\n"
            + "dob               TEXT  NULL,\n"
            + "email             TEXT  NULL\n"
            + ")";

    private static final String CREATE_TBL_ALL_SONGS = "CREATE TABLE IF NOT EXISTS "
            + TBL_ALL_SONGS
            + " (\n"
            + "_id              INTEGER NOT NULL PRIMARY KEY,\n"
            + "album_id         INTEGER NOT NULL,\n"
            + "artist           TEXT NOT NULL,\n"
            + "title            TEXT NOT NULL,\n"
            + "display_name     TEXT NOT NULL,\n"
            + "duration         TEXT NOT NULL,\n"
            + "url              TEXT NOT NULL,\n"
            + "path             TEXT NOT NULL,\n"
            + "content          TEXT NOT NULL,\n"
            + "type             TEXT NOT NULL,\n"
            + "audioProgress    TEXT NOT NULL,\n"
            + "audioProgressSec INTEGER NOT NULL)";

    private static final String CREATE_TBL_ARTISTS = "CREATE TABLE IF NOT EXISTS " +
            TBL_ARTISTS
            + " (\n"
            + " _id              INTEGER NOT NULL PRIMARY KEY,\n"
            + "album_id         INTEGER NOT NULL,\n"
            + "artist           TEXT NOT NULL,\n"
            + "title            TEXT NOT NULL,\n"
            + "display_name     TEXT NOT NULL,\n"
            + "duration         TEXT NOT NULL,\n"
            + "path             TEXT NOT NULL,\n"
            + "content          TEXT NOT NULL,\n"
            + "type             TEXT NOT NULL,\n"
            + "audioProgress    TEXT NOT NULL,\n"
            + "audioProgressSec INTEGER NOT NULL)";

    private static final String CREATE_TBL_BROWSE = "CREATE TABLE IF NOT EXISTS " + TBL_BROWSE
            + " (\n"
            + "_id              INTEGER NOT NULL PRIMARY KEY,\n"
            + "album_id         INTEGER NOT NULL,\n"
            + "artist           TEXT NOT NULL,\n"
            + "title            TEXT NOT NULL,\n"
            + "display_name     TEXT NOT NULL,\n"
            + "duration         TEXT NOT NULL,\n"
            + "path             TEXT NOT NULL,\n"
            + "audioProgress    TEXT NOT NULL,\n"
            + "audioProgressSec INTEGER NOT NULL)";

    private static final String CREATE_TBL_DISCOVER = "CREATE TABLE IF NOT EXISTS "
            + TBL_DISCOVER
            + " (\n"
            + "_id              INTEGER NOT NULL PRIMARY KEY,\n"
            + "album_id         INTEGER NOT NULL,\n"
            + "artist           TEXT NOT NULL,\n"
            + "title            TEXT NOT NULL,\n"
            + "display_name     TEXT NOT NULL,\n"
            + "duration         TEXT NOT NULL,\n"
            + "path             TEXT NOT NULL,\n"
            + "content          TEXT NOT NULL,\n"
            + "type             TEXT NOT NULL,\n"
            + "audioProgress    TEXT NOT NULL,\n"
            + "audioProgressSec INTEGER NOT NULL"
            + ")";

    private static final String CREATE_TBL_DOWNLOADED = "CREATE TABLE IF NOT EXISTS "
            + TBL_DOWNLOADED
            + " (\n"
            + "_id              INTEGER NOT NULL PRIMARY KEY,\n"
            + "album_id         INTEGER NOT NULL,\n"
            + "artist           TEXT NOT NULL,\n"
            + "title            TEXT NOT NULL,\n"
            + "display_name     TEXT NOT NULL,\n"
            + "duration         TEXT NOT NULL,\n"
            + "path             TEXT NOT NULL,\n"
            + "content          TEXT NOT NULL,\n"
            + "type             TEXT NOT NULL,\n"
            + "audioProgress    TEXT NOT NULL,\n"
            + "audioProgressSec INTEGER NOT NULL"
            + ")";

    private static final String CREATE_TBL_GENRE = "CREATE TABLE IF NOT EXISTS "
            + TBL_GENRE
            + " (\n"
            + "_id              INTEGER NOT NULL PRIMARY KEY,\n"
            + "album_id         INTEGER NOT NULL,\n"
            + "artist           TEXT NOT NULL,\n"
            + "title            TEXT NOT NULL,\n"
            + "display_name     TEXT NOT NULL,\n"
            + "duration         TEXT NOT NULL,\n"
            + "path             TEXT NOT NULL,\n"
            + "content          TEXT NOT NULL,\n"
            + "type             TEXT NOT NULL,\n"
            + "audioProgress    TEXT NOT NULL,\n"
            + "audioProgressSec INTEGER NOT NULL"
            + ")";

    private static final String CREATE_TBL_QUEUE = "CREATE TABLE IF NOT EXISTS "
            + TBL_QUEUE
            + " (\n"
            + "_id              INTEGER NOT NULL PRIMARY KEY,\n"
            + "album_id         INTEGER NOT NULL,\n"
            + "artist           TEXT NOT NULL,\n"
            + "title            TEXT NOT NULL,\n"
            + "display_name     TEXT NOT NULL,\n"
            + "duration         TEXT NOT NULL,\n"
            + "url              TEXT NOT NULL,\n"
            + "path             TEXT NOT NULL,\n"
            + "content          TEXT NOT NULL,\n"
            + "type             TEXT NOT NULL,\n"
            + "audioProgress    TEXT NOT NULL,\n"
            + "audioProgressSec INTEGER NOT NULL"
            + ")";

    public MusicSQLiteHelper(Context context, String name,
                             CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    // To open database in synchronized way
    public static MusicSQLiteHelper DBHelper = null;

    public MusicSQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Initialize database
    public static void init(Context context) {
        if (DBHelper == null) {
            if (DEBUG)
                Log.i("MusicSQLiteHelper", context.toString());
            DBHelper = new MusicSQLiteHelper(context);
        }
    }

    public static synchronized MusicSQLiteHelper getDBHelper(Context context) {
        if (DBHelper == null)
            DBHelper = new MusicSQLiteHelper(context);

        return DBHelper;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            // Creating Required Tables
            db.execSQL(CREATE_TBL_LANGUAGE);
            db.execSQL(CREATE_TBL_USER);
            db.execSQL(CREATE_TBL_ALL_SONGS);
            db.execSQL(CREATE_TBL_QUEUE);
            db.execSQL(CREATE_TBL_BROWSE);
            db.execSQL(CREATE_TBL_DISCOVER);
            db.execSQL(CREATE_TBL_GENRE);
            db.execSQL(CREATE_TBL_ARTISTS);
            db.execSQL(CREATE_TBL_DOWNLOADED);

            Log.d(LOG_TAG, "Tables created successfully!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // On Upgrade Drop Older Tables
        if (DEBUG) {
            Log.w(LOG_TAG, "Upgrading database from version " + oldVersion
                    + " to " + newVersion + ", which will destroy all old data");
        }
        try {
            db.execSQL("DROP TABLE IF EXISTS " + TBL_LANGUAGE);
            db.execSQL("DROP TABLE IF EXISTS " + TBL_USER);
            db.execSQL("DROP TABLE IF EXISTS " + TBL_ALL_SONGS);
            db.execSQL("DROP TABLE IF EXISTS " + TBL_ARTISTS);
            db.execSQL("DROP TABLE IF EXISTS " + TBL_BROWSE);
            db.execSQL("DROP TABLE IF EXISTS " + TBL_DISCOVER);
            db.execSQL("DROP TABLE IF EXISTS " + TBL_DOWNLOADED);
            db.execSQL("DROP TABLE IF EXISTS " + TBL_GENRE);
            db.execSQL("DROP TABLE IF EXISTS " + TBL_QUEUE);
            // create new tables
            onCreate(db);
        } catch (Exception exception) {
            if (DEBUG)
                Log.i(LOG_TAG, "Exception onCreate() exception");
        }
    }

    // Open database for insert,update,delete in synchronized manner
    public synchronized static SQLiteDatabase open() throws SQLException {
        SQLiteDatabase db = DBHelper.getWritableDatabase();
        return db;
    }

    @Override
    public void close() {
        SQLiteDatabase db = DBHelper.getReadableDatabase();
        if (db != null && db.isOpen()) {
            db.close();
        }
    }

    // Escape string for single quotes (Insert,Update)
    private static String sqlEscapeString(String str) {
        String retStr = "";

        if (null != str) {
            retStr = DatabaseUtils.sqlEscapeString(str);
            // Remove the enclosing single quotes ...
            retStr = retStr.substring(1, retStr.length() - 1);
        }

        return retStr;
    }

    //**********************************************************************************************
    // TABLE LANGUAGES
    //**********************************************************************************************

    // Insert into LANGUAGES Table
    public static void insertLanguage(LanguageModel model) {

        SQLiteDatabase db = DBHelper.getWritableDatabase();

        String lanId = sqlEscapeString(model.getId());
        String langName = sqlEscapeString(model.getLangName());

        ContentValues cVal = new ContentValues();

        cVal.put("lang_id", lanId);
        cVal.put("title", langName);

        // Insert values in LANGUAGES Table
        db.insert(TBL_LANGUAGE, null, cVal);

        // Close Database Connection
        if (db != null) {
            db.close();
        }
    }

    // List records from LANGUAGES Table
    public static ArrayList<LanguageModel> getLanguages() {
        String sqlStr;
        ArrayList<LanguageModel> listLanguages = new ArrayList<>();
        sqlStr = "SELECT * FROM " + TBL_LANGUAGE;

        SQLiteDatabase db = DBHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery(sqlStr, null);

        // Looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                LanguageModel lang = new LanguageModel();

                lang.setId(cursor.getString(cursor
                        .getColumnIndex("lang_id")));
                lang.setLangName(cursor.getString(cursor
                        .getColumnIndex("title")));


                // Adding to listAdUserMst List
                listLanguages.add(lang);
            } while (cursor.moveToNext());
        }
        // Close cursor
        cursor.close();

        // Close Database Connection
        if (db != null) {
            db.close();
        }

        return listLanguages;
    }

    // Delete All Record from LANGUAGES TABLE
    public static void deleteLanguage() {
        SQLiteDatabase db = DBHelper.getWritableDatabase();
        db.delete(TBL_LANGUAGE, null, null);

        // Close Database Connection
        db.close();
    }

    // Count Records of LANGUAGES TABLE
    public static int countLanguage() {
        String sqlStr = "SELECT * FROM " + TBL_LANGUAGE;
        SQLiteDatabase db = DBHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery(sqlStr, null);

        int count = cursor.getCount();

        // Close cursor
        if (cursor != null) {
            cursor.close();
        }


        // Close Database Connection
        if (db != null) {
            db.close();
        }


        // Return count
        return count;
    }
    //**********************************************************************************************
    // AD_USER_MST
    //**********************************************************************************************

    // Insert into AD_USER_MST Table
    public static void insertAdUserMst(TblUserMst adum) {

        SQLiteDatabase db = DBHelper.getWritableDatabase();

        String user_id = sqlEscapeString(adum.getId());
        String user_name = sqlEscapeString(adum.getUsername());
        String user_mobile = sqlEscapeString(adum.getMobile());
        String user_email = sqlEscapeString(adum.getEmail());
        String dob = sqlEscapeString(adum.getDob());

        ContentValues cVal = new ContentValues();

        cVal.put("user_id", user_id);
        cVal.put("username", user_name);
        cVal.put("mobile", user_mobile);
        cVal.put("email", user_email);
        cVal.put("dob", dob);

        // Insert values in AD_USER_MST Table
        db.insert(TBL_USER, null, cVal);

        // Close Database Connection
        db.close();
    }

    public static String getUserName() {
        String userName = null;
        String sqlStr;
        sqlStr = "SELECT username FROM " + TBL_USER;

        SQLiteDatabase db = DBHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery(sqlStr, null);

        // Looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                userName = cursor.getString(cursor
                        .getColumnIndex("username"));
            } while (cursor.moveToNext());
        }

        // Close cursor
        if (cursor != null) {
            cursor.close();
        }

        // Close Database Connection
        if (db != null) {
            db.close();
        }
        return userName;
    }

    public static String getUserId() {
        String userId = null;
        String sqlStr;
        sqlStr = "SELECT user_id FROM " + TBL_USER;

        SQLiteDatabase db = DBHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery(sqlStr, null);

        // Looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                userId = cursor.getString(cursor
                        .getColumnIndex("user_id"));
            } while (cursor.moveToNext());
        }

        // Close cursor
        if (cursor != null) {
            cursor.close();
        }

        // Close Database Connection
        if (db != null) {
            db.close();
        }
        return userId;
    }

    // List records from AD_USER_MST Table
    public static List<TblUserMst> readAdUserMstUserId(String userId) {
        String sqlStr;
        List<TblUserMst> listAdUserMst = new ArrayList<>();
        sqlStr = "SELECT * FROM " + TBL_USER + " WHERE user_id = '"
                + userId + "'";

        SQLiteDatabase db = DBHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery(sqlStr, null);

        // Looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                TblUserMst adum = new TblUserMst();

                adum.setUsername(cursor.getString(cursor
                        .getColumnIndex("username")));
                adum.setId(cursor.getString(cursor
                        .getColumnIndex("user_id")));
                adum.setEmail(cursor.getString(cursor
                        .getColumnIndex("email")));
                adum.setMobile(cursor.getString(cursor
                        .getColumnIndex("mobile")));
                adum.setDob(cursor.getString(cursor
                        .getColumnIndex("dob")));

                // Adding to listAdUserMst List
                listAdUserMst.add(adum);
            } while (cursor.moveToNext());
        }
        // Close cursor
        cursor.close();

        // Close Database Connection
        if (db != null) {
            db.close();
        }
        return listAdUserMst;
    }

    // Delete All Record from AD_USER_MST TABLE
    public static void deleteAdUserMst() {
        SQLiteDatabase db = DBHelper.getWritableDatabase();
        db.delete(TBL_USER, null, null);

        // Close Database Connection
        // db.close();
    }

    public static void deleteAdUserMstUserId(TblUserMst adum) {
        SQLiteDatabase db = DBHelper.getWritableDatabase();
        db.delete(TBL_USER, "user_id = ?",
                new String[]{String.valueOf(adum.getId())});

        // Close Database Connection
        if (db != null) {
            db.close();
        }
    }

    // Count Records of AD_USER_MST TABLE
    public static int countAdUserMst() {
        String sqlStr = "SELECT * FROM " + TBL_USER;
        SQLiteDatabase db = DBHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery(sqlStr, null);

        int count = cursor.getCount();

        // Close cursor
        cursor.close();

        // Close Database Connection
        if (db != null) {
            db.close();
        }

        // Return count
        return count;
    }

    ////////////////////////////////////////////////////////////////////////////
    //SONG QUEUE
///////////////////////////////////////////////////////////////////////
    public static void insertSongInQueue(SongDetail item) {
        SQLiteDatabase db = DBHelper.getWritableDatabase();
        String title = sqlEscapeString(item.getTitle());
        String artist = sqlEscapeString(item.getArtist());
        String duration = sqlEscapeString(String.valueOf(item.getDuration()));
        String display_name = sqlEscapeString(item.getDisplay_name());
        String path = sqlEscapeString(item.getPath());
        String content = sqlEscapeString(item.getLyrics());
        String type = sqlEscapeString(item.getType());
        String url = sqlEscapeString(item.getUrl());
        String album_id = sqlEscapeString(String.valueOf(item.getAlbum_id()));
        String audioProgress = sqlEscapeString(String.valueOf(item.audioProgress));
        String audioProgressSec = sqlEscapeString(String.valueOf(item.audioProgressSec));
        ContentValues cVal = new ContentValues();
        cVal.put("album_id", album_id);
        cVal.put("artist", artist);
        cVal.put("title", title);
        cVal.put("display_name", display_name);
        cVal.put("duration", duration);
        cVal.put("path", path);
        cVal.put("type", type);
        cVal.put("content", content);
        cVal.put(FavoritePlayTableHelper.IMAGE_URL, url);
        cVal.put("audioProgress", audioProgress);
        cVal.put("audioProgressSec", audioProgressSec);
        db.insert(TBL_QUEUE, null, cVal);
        Log.d("TAG", "insert id: " + cVal.toString());
    }

    public static int updateQueue(SongDetail data) {
        SQLiteDatabase db = DBHelper.getWritableDatabase();
        ContentValues cVal = new ContentValues();
        cVal.put("album_id", data.getAlbum_id());
        cVal.put("artist", data.getArtist());
        cVal.put("title", data.getTitle());
        cVal.put("display_name", data.getDisplay_name());
        cVal.put("duration", data.getDuration());
        cVal.put("path", data.getPath());
        cVal.put("type", data.getType());
        cVal.put("content", data.getLyrics());
        cVal.put(FavoritePlayTableHelper.IMAGE_URL, data.getUrl());
        cVal.put("audioProgress", data.audioProgress);
        cVal.put("audioProgressSec", data.audioProgressSec);
        return db.update(TBL_QUEUE, cVal, "album_id = ? AND title = ?", new String[]{String.valueOf(data.getAlbum_id()), String.valueOf(data.getTitle())});
    }

    public static void deleteSong() {
        DBHelper.getWritableDatabase().delete(TBL_QUEUE, null, null);
    }

    public static ArrayList<SongDetail> getQueueList() {


        String sqlStr;
        ArrayList<SongDetail> listMst = new ArrayList<>();
        sqlStr = "SELECT * FROM " + TBL_QUEUE;

        SQLiteDatabase db = DBHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery(sqlStr, null);

        // Looping through all rows and adding to list
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            for (int i = 0; i < cursor.getCount(); i++) {
                SongDetail mediaItem = new SongDetail();
                mediaItem.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex("_id"))));
                mediaItem.setTitle(cursor.getString(cursor.getColumnIndex("title")));
                mediaItem.setDisplay_name(cursor.getString(cursor.getColumnIndex("display_name")));
                mediaItem.setArtist(cursor.getString(cursor.getColumnIndex("artist")));
                mediaItem.setDuration(cursor.getString(cursor.getColumnIndex("duration")));
                mediaItem.setUrl(cursor.getString(cursor.getColumnIndex(FavoritePlayTableHelper.IMAGE_URL)));
                mediaItem.setPath(cursor.getString(cursor.getColumnIndex("path")));
                mediaItem.setLyrics(cursor.getString(cursor.getColumnIndex("content")));
                mediaItem.setType(cursor.getString(cursor.getColumnIndex("type")));
                mediaItem.setAlbum_id(Integer.parseInt(cursor.getString(cursor.getColumnIndex("album_id"))));
                Log.d("TAG", "View Value " + mediaItem);
                listMst.add(mediaItem);
                cursor.moveToNext();
            }
        }
        cursor.close();
        if (db != null) {
            db.close();
        }

        Log.d("TAG", "List Size" + listMst.size());
        return listMst;


    }

    // Check if record exist in QUEUE TABLE
    public static Boolean isExistQueueSong(SongDetail data) {
        try {
            SQLiteDatabase db = DBHelper.getReadableDatabase();
            String sqlStr = "SELECT * FROM " + TBL_QUEUE
                    + " WHERE album_id = '" + data.getAlbum_id()
                    + "' AND title = '" + data.getTitle() + "'";

            Cursor cursor = db.rawQuery(sqlStr, null);

            int count = cursor.getCount();

            // Close cursor
            cursor.close();

            // Close Database Connection
            if (db != null) {
                db.close();
            }

            return count > 0;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static int countQueueList() {
        String sqlStr = "SELECT * FROM " + TBL_QUEUE;
        SQLiteDatabase db = DBHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery(sqlStr, null);

        int count = cursor.getCount();
        cursor.close();
        if (db != null) {
            db.close();
        }
        return count;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////
    //SONGS LIST
    /////////////////////////////////////////////////////////////////////////////////////////////
    public static void insertSongs(SongDetail item) {
        SQLiteDatabase db = DBHelper.getWritableDatabase();
        String title = sqlEscapeString(item.getTitle());
        String artist = sqlEscapeString(item.getArtist());
        String duration = sqlEscapeString(String.valueOf(item.getDuration()));
        String display_name = sqlEscapeString(item.getDisplay_name());
        String path = sqlEscapeString(item.getPath());
        String content = sqlEscapeString(item.getLyrics());
        String type = sqlEscapeString(item.getType());
        String url = sqlEscapeString(item.getUrl());
        String album_id = sqlEscapeString(String.valueOf(item.getAlbum_id()));
        String audioProgress = sqlEscapeString(String.valueOf(item.audioProgress));
        String audioProgressSec = sqlEscapeString(String.valueOf(item.audioProgressSec));
        ContentValues cVal = new ContentValues();
        cVal.put("album_id", album_id);
        cVal.put("artist", artist);
        cVal.put("title", title);
        cVal.put("display_name", display_name);
        cVal.put("duration", duration);
        cVal.put("path", path);
        cVal.put("type", type);
        cVal.put("content", content);
        cVal.put("url", url);
        cVal.put("audioProgress", audioProgress);
        cVal.put("audioProgressSec", audioProgressSec);
        db.insert(TBL_ALL_SONGS, null, cVal);
        Log.d("TAG", "insert id: " + cVal.toString());
    }

    public static int updateSongs(SongDetail data) {
        SQLiteDatabase db = DBHelper.getWritableDatabase();
        ContentValues cVal = new ContentValues();
        cVal.put("album_id", data.getAlbum_id());
        cVal.put("artist", data.getArtist());
        cVal.put("title", data.getTitle());
        cVal.put("display_name", data.getDisplay_name());
        cVal.put("duration", data.getDuration());
        cVal.put("path", data.getPath());
        cVal.put("type", data.getType());
        cVal.put("content", data.getLyrics());
        cVal.put("url", data.getUrl());
        cVal.put("audioProgress", data.audioProgress);
        cVal.put("audioProgressSec", data.audioProgressSec);
        return db.update(TBL_ALL_SONGS, cVal, "album_id = ? AND title = ?", new String[]{String.valueOf(data.getAlbum_id()), String.valueOf(data.getTitle())});
    }

    public static void delete() {
        DBHelper.getWritableDatabase().delete(TBL_ALL_SONGS, null, null);
    }

    public static ArrayList<SongDetail> getSongsList() {


        String sqlStr;
        ArrayList<SongDetail> listMst = new ArrayList<>();
        sqlStr = "SELECT * FROM " + TBL_ALL_SONGS;

        SQLiteDatabase db = DBHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery(sqlStr, null);

        // Looping through all rows and adding to list
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            for (int i = 0; i < cursor.getCount(); i++) {
                SongDetail mediaItem = new SongDetail();
                mediaItem.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex("_id"))));
                mediaItem.setTitle(cursor.getString(cursor.getColumnIndex("title")));
                mediaItem.setDisplay_name(cursor.getString(cursor.getColumnIndex("display_name")));
                mediaItem.setArtist(cursor.getString(cursor.getColumnIndex("artist")));
                mediaItem.setDuration(cursor.getString(cursor.getColumnIndex("duration")));
                mediaItem.setUrl(cursor.getString(cursor.getColumnIndex("url")));
                mediaItem.setPath(cursor.getString(cursor.getColumnIndex("path")));
                mediaItem.setLyrics(cursor.getString(cursor.getColumnIndex("content")));
                mediaItem.setType(cursor.getString(cursor.getColumnIndex("type")));
                mediaItem.setAlbum_id(Integer.parseInt(cursor.getString(cursor.getColumnIndex("album_id"))));
                Log.d("TAG", "View Value " + mediaItem);
                listMst.add(mediaItem);
                cursor.moveToNext();
            }
        }
        cursor.close();
        if (db != null) {
            db.close();
        }

        Log.d("TAG", "List Size" + listMst.size());
        return listMst;


    }

    // Check if record exist in QUEUE TABLE
    public static Boolean isExistSongs(SongDetail data) {
        try {
            SQLiteDatabase db = DBHelper.getReadableDatabase();
            String sqlStr = "SELECT * FROM " + TBL_ALL_SONGS
                    + " WHERE album_id = '" + data.getAlbum_id()
                    + "' AND title = '" + data.getTitle() + "'";

            Cursor cursor = db.rawQuery(sqlStr, null);

            int count = cursor.getCount();

            // Close cursor
            cursor.close();

            // Close Database Connection
            if (db != null) {
                db.close();
            }

            return count > 0;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static int countSongsList() {
        String sqlStr = "SELECT * FROM " + TBL_ALL_SONGS;
        SQLiteDatabase db = DBHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery(sqlStr, null);

        int count = cursor.getCount();
        cursor.close();
        if (db != null) {
            db.close();
        }
        return count;
    }
}
