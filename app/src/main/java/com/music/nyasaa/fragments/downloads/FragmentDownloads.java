package com.music.nyasaa.fragments.downloads;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.music.nyasaa.R;
import com.music.nyasaa.adapter.DownloadAlbumListAdapter;
import com.music.nyasaa.adapter.DownloadPlayListAdapter;
import com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity;
import com.music.nyasaa.front_end_layer.activities.download_in_appscope_enc.DataSQLHelper;
import com.music.nyasaa.front_end_layer.activities.download_in_appscope_enc.Table_Details;
import com.music.nyasaa.models.SongDetail;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity.PostionOfPlay;
import static com.music.nyasaa.front_end_layer.activities.download_in_appscope_enc.Table_Details.JSON_COL;


/// If we used the the

public class FragmentDownloads extends Fragment {

    private View myView;
    private static final String ARG_KEY_NUMBER = "tab_number";

    private static Context context;
    public static List<SongDetail> songDetailDownload = new ArrayList<SongDetail>();
    private static final String BACK_STACK_ROOT_TAG = "root_fragment";
    private RecyclerView mMySongsListView;
    private TextView mTxtWarn;
    DownloadPlayListAdapter connectionAdapter;
    DownloadAlbumListAdapter albumListAdapter;
    ProgressBar loader;
    AVLoadingIndicatorView avi;
    public static FragmentDownloads fragmentDownloads;


    public static FragmentDownloads newInstance(int number) {
        Bundle args = new Bundle();
        args.putInt(ARG_KEY_NUMBER, number);
        FragmentDownloads frag = new FragmentDownloads();
        frag.setArguments(args);
        return frag;
    }


    public FragmentDownloads() {
        // Required empty public constructor
    }

    public void callMyAlbumsongs() {

        ((DashBoardActivity) getActivity()).hideBadge();
        ((DashBoardActivity) getActivity()).toolbarStatusBar(" AudioNew");

        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.popBackStack("AudioNew", FragmentManager.POP_BACK_STACK_INCLUSIVE);

        // Add the new tab fragment
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .replace(R.id.fragment, FragmentDownloadSongList.newInstance(0), String.valueOf(0))
//                .replace(R.id.fragment, FragmentDSL.newInstance(0), String.valueOf(0))
                .addToBackStack(BACK_STACK_ROOT_TAG)
                // .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
        getActivity().overridePendingTransition(R.anim.left, R.anim.right);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        myView = inflater.inflate(R.layout.fragment_downloads, container, false);
        initView(myView);
        fragmentDownloads = this;
        songDetailDownload = new ArrayList<SongDetail>();
//        getDownlodedSong();
        getDownlodedSongAlbums();
        return myView;
    }

    public void getDownlodedSong() {
        songDetailDownload = new ArrayList<SongDetail>();

        DataSQLHelper dataSQLHelper = new DataSQLHelper(getContext());
        SQLiteDatabase sqLiteDatabase;
        String getJson = "";
        sqLiteDatabase = dataSQLHelper.getWritableDatabase();
        //  Cursor cursor = dataSQLHelper.getvalue_By_colums(sqLiteDatabase, Table_Details.TABLE_NAME, null, null);

        String queryToGetUniqueRecords = "SELECT * from " + Table_Details.TABLE_NAME;
        Cursor cursor = dataSQLHelper.getvalueWithQuery(sqLiteDatabase, queryToGetUniqueRecords);

        while (cursor.moveToNext()) {
            int index1 = cursor.getColumnIndex(JSON_COL);
            getJson = cursor.getString(index1);

            System.out.println("get String json.." + getJson);
            try {
                JSONObject track = new JSONObject(getJson);
                SongDetail songDetails = new SongDetail();
                songDetails.setFile(track.getString("File_Name")); //= track.getString("device_type");
                songDetails.setTitle(track.getString("File_Name")); //= track.getString("device_model");
                songDetails.setCover(track.optString("Image")); //= track.getString("device_firmware");
                songDetails.setTypeOfSong(track.optString("Type"));
                songDetails.setName(track.optString("name"));
                songDetails.setId(track.optString("id"));
                songDetails.setAlbum_name(track.optString("Album_name"));
                // songDetails.setTitle(track.optString("name"));

                File fileo = new File(getActivity().getApplicationContext().getFilesDir(), track.getString("File_Name"));
                String outPath = fileo.getAbsolutePath();
                songDetails.setPath(outPath);

                songDetailDownload.add(songDetails);

            } catch (Exception e) {
            }
        }

        if (songDetailDownload.size() > 0) {
            connectionAdapter = new DownloadPlayListAdapter(getActivity(), songDetailDownload);

            mMySongsListView.setAdapter(connectionAdapter);

            mTxtWarn.setVisibility(View.GONE);
        } else {
            mTxtWarn.setVisibility(View.VISIBLE);
        }
    }

    public void getDownlodedSongAlbums() {
        songDetailDownload = new ArrayList<SongDetail>();

        DataSQLHelper dataSQLHelper = new DataSQLHelper(getContext());
        SQLiteDatabase sqLiteDatabase;
        String getJson = "";
        sqLiteDatabase = dataSQLHelper.getWritableDatabase();
        //  Cursor cursor = dataSQLHelper.getvalue_By_colums(sqLiteDatabase, Table_Details.TABLE_NAME, null, null);

        String queryToGetUniqueRecords = "SELECT * from " + Table_Details.TABLE_NAME;
        Cursor cursor = dataSQLHelper.getvalueWithQuery(sqLiteDatabase, queryToGetUniqueRecords);

        List<String> allUniqueAlbums = new ArrayList<String>();
        while (cursor.moveToNext()) {
            int index1 = cursor.getColumnIndex(JSON_COL);
            getJson = cursor.getString(index1);


            System.out.println("get String json.." + getJson);
            try {
                JSONObject track = new JSONObject(getJson);
                SongDetail songDetails = new SongDetail();
                songDetails.setFile(track.getString("File_Name")); //= track.getString("device_type");
                songDetails.setTitle(track.getString("File_Name")); //= track.getString("device_model");
                songDetails.setCover(track.optString("Image")); //= track.getString("device_firmware");
                songDetails.setTypeOfSong(track.optString("Type"));
                songDetails.setName(track.optString("name"));
                songDetails.setId(track.optString("id"));
                songDetails.setAlbum_name(track.optString("Album_name"));
                songDetails.setCover_image(track.optString("Cover_image"));
                // songDetails.setTitle(track.optString("name"));

                File fileo = new File(getActivity().getApplicationContext().getFilesDir(), track.getString("File_Name"));
                String outPath = fileo.getAbsolutePath();
                songDetails.setPath(outPath);

                if (allUniqueAlbums.contains(track.optString("Album_name")) == false) {
                    allUniqueAlbums.add(track.optString("Album_name"));
                    songDetailDownload.add(songDetails);
                }

            } catch (Exception e) {
            }
        }

        if (songDetailDownload.size() > 0) {
            albumListAdapter = new DownloadAlbumListAdapter(FragmentDownloads.this, getActivity(), songDetailDownload);

            mMySongsListView.setAdapter(albumListAdapter);

            mTxtWarn.setVisibility(View.GONE);
        } else {
            mTxtWarn.setVisibility(View.VISIBLE);
        }
    }


    private void initView(View myView) {

//        myView.getRootView().setBackgroundColor(Color.BLACK);
        myView.getRootView().setBackgroundColor(getResources().getColor(R.color.md_black_1000));
//        myView.getRootView().setBackgroundResource(R.drawable.black_background);

        avi = (AVLoadingIndicatorView) myView.findViewById(R.id.avi);
        mMySongsListView = (RecyclerView) myView.findViewById(R.id.listViewAlbum);
        mTxtWarn = (TextView) myView.findViewById(R.id.txtWarn);
        loader = (ProgressBar) myView.findViewById(R.id.loader);
        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        mMySongsListView.setLayoutManager(linearLayoutManager);
        mMySongsListView.setItemAnimator(new DefaultItemAnimator());
        connectionAdapter = new DownloadPlayListAdapter(getActivity(), songDetailDownload);
        if (songDetailDownload.size() > 0) {
            mMySongsListView.setAdapter(connectionAdapter);
            mTxtWarn.setVisibility(View.GONE);
            mMySongsListView.setVisibility(View.VISIBLE);
        } else {
            mTxtWarn.setVisibility(View.GONE);
            mMySongsListView.setVisibility(View.VISIBLE);
        }
    }

    public void startLoader() {
//        avi.setVisibility(View.VISIBLE);
//        final Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                //Do something after 100ms
//                avi.setVisibility(View.GONE);
//            }
//        }, 3000);
    }

    public void playPoase(int postion) {


        try {
            PostionOfPlay = postion;

            if (songDetailDownload != null)
                for (int i = 0; i < songDetailDownload.size(); i++) {
                    if (i != postion)
                        songDetailDownload.get(i).setPlayStop(false);

                }
            if (songDetailDownload.get(postion).getPlayStop()) {
                songDetailDownload.get(postion).setPlayStop(false);
            } else {
                songDetailDownload.get(postion).setPlayStop(true);
            }

            connectionAdapter.notifyDataSetChanged();
            connectionAdapter.notifyDataSetChanged();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}