package com.music.nyasaa.fragments.about_us;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.music.nyasaa.R;

public class FragmentAboutUs extends Fragment {

    private View myView;
    private static final String ARG_KEY_NUMBER = "tab_number";

    private static Context context;


    public static FragmentAboutUs newInstance(int number) {
        Bundle args = new Bundle();
        args.putInt(ARG_KEY_NUMBER, number);
        FragmentAboutUs frag = new FragmentAboutUs();
        frag.setArguments(args);
        return frag;
    }

    public static FragmentAboutUs newInstance(Context mContext) {
        FragmentAboutUs f = new FragmentAboutUs();
        context = mContext;
        return f;
    }
    public FragmentAboutUs() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        myView= inflater.inflate(R.layout.fragment_about_us, container, false);
        initView(myView);
        return myView;
    }


    private void initView(View myView)
    {


        myView.getRootView().setBackgroundColor(getResources().getColor(R.color.orBLACK));
//        mGridAlbumview =(RecyclerView)myView.findViewById(R.id.listViewAlbum);

//        myView.getRootView().setBackgroundResource(R.drawable.black_gradient);



    }

}
