package com.music.nyasaa.fragments.meditation;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.music.nyasaa.R;
import com.music.nyasaa.adapter.MediatationMoreAdapter;
import com.music.nyasaa.models.SongDetail;
import com.music.nyasaa.utility.SharedPreferenceManager;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;

import java.util.ArrayList;

public class FragmentMediationTypes extends Fragment implements View.OnClickListener {

    private View myView;

    public static FragmentMediationTypes fragmentMeditationtype;
    private static final String ARG_KEY_NUMBER = "tab_number";
    private static Context context;
    public static ArrayList<SongDetail> mSongss;
    private SwipyRefreshLayout swipyRefreshLayoutDirection;
    private TextView mTxtWarn;
    MediatationMoreAdapter mediatationMoreAdapter;
    private RecyclerView mMySongsListView;
    private FragmentMediationTypes fragmentMediationTypes;
    private EditText mEditSearch;
    private int currentPage = 0;

    private LinearLayout mLinearMeditation,mLinearWOrkSHops,mLinearTalksOnMed;


    public static FragmentMediationTypes newInstance(int number) {
        Bundle args = new Bundle();
        args.putInt(ARG_KEY_NUMBER, number);
        FragmentMediationTypes frag = new FragmentMediationTypes();
        frag.setArguments(args);
        return frag;
    }


    public FragmentMediationTypes() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        myView = inflater.inflate(R.layout.fragment_meditation_new, container, false);
        initView(myView);
        fragmentMeditationtype = this;
        return myView;
    }

    private void initView(View myView)
    {
        mLinearMeditation = (LinearLayout) myView.findViewById(R.id.meditation_layout);
        mLinearTalksOnMed = (LinearLayout) myView.findViewById(R.id.talks_on_mediation_layout);
        mLinearWOrkSHops = (LinearLayout) myView.findViewById(R.id.workshop_layout);

        mLinearMeditation.setOnClickListener(this);
        mLinearTalksOnMed.setOnClickListener(this);
        mLinearWOrkSHops.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.meditation_layout:
                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_MED_TOOLNAME," Meditation");
//                ((DashBoardActivity)getActivity()).openMedtation(1);

                break;


            case R.id.talks_on_mediation_layout:
                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_MED_TOOLNAME," Talks on Med..");
//                ((DashBoardActivity)getActivity()).openMedtation(3);


                break;


            case R.id.workshop_layout:
                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_MED_TOOLNAME," Workshop");
//                ((DashBoardActivity)getActivity()).openMedtation(2);


                break;
        }

    }
}