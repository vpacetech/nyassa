package com.music.nyasaa.fragments.jokes;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.music.nyasaa.R;
import com.music.nyasaa.adapter.JokesAdapter;
import com.music.nyasaa.controller.MyApplication;
import com.music.nyasaa.fragments.music.FragmentMusic;
import com.music.nyasaa.models.RequestModel.SearchMedia.RequestSearchMedia;
import com.music.nyasaa.models.SongDetail;
import com.music.nyasaa.utility.SharedPreferenceManager;
import com.music.nyasaa.utility.Utility;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity.PostionOfPlay;
import static com.music.nyasaa.retrofit.Config.X_API_KEY;

public class JokesFragment extends Fragment {

    private View myView;
    public static JokesFragment jokesFragment;
    private static final String ARG_KEY_NUMBER = "tab_number";
    private static Context context;
    public static ArrayList<SongDetail> mSongss;
    public static List<List<SongDetail>> getMyPlayListResponseDataPlayList;
    private SwipyRefreshLayout swipyRefreshLayoutDirection;
    private TextView mTxtWarn;
    private JokesAdapter jokesAdapter;
    private RecyclerView mMySongsListView;
    private FragmentMusic fragmentMySongs;
    private EditText mEditSearch;
    private int currentPage = 0;
    boolean isSearch = false;
    private TextView mTxtLang;

    public static JokesFragment newInstance(int number) {
        Bundle args = new Bundle();
        args.putInt(ARG_KEY_NUMBER, number);
        JokesFragment frag = new JokesFragment();
        frag.setArguments(args);
        return frag;
    }


    public JokesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        myView = inflater.inflate(R.layout.fragment_jokes, container, false);
        initView(myView);
        jokesFragment = this;
        return myView;
    }


    private void initView(View myView) {

//        myView.getRootView().setBackgroundColor(Color.BLACK);
        myView.getRootView().setBackgroundColor(getResources().getColor(R.color.orBLACK));
        mTxtWarn = (TextView) myView.findViewById(R.id.txtWarn);
        mTxtLang = (TextView) myView.findViewById(R.id.txtLang);
//        myView.getRootView().setBackgroundResource(R.drawable.black_gradient);

        mTxtWarn.setVisibility(View.GONE);

        swipyRefreshLayoutDirection = (SwipyRefreshLayout) myView.findViewById(R.id.swipe_refresh_layout);
        swipyRefreshLayoutDirection.setDirection(SwipyRefreshLayoutDirection.BOTH);
        mMySongsListView = (RecyclerView) myView.findViewById(R.id.listViewAlbum);
        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        mMySongsListView.setLayoutManager(linearLayoutManager);
        mMySongsListView.setItemAnimator(new DefaultItemAnimator());

        // MyPlayListAdapter connectionAdapter = new MyPlayListAdapter(getActivity(),img,name);
        // mMySongsListView.setAdapter(connectionAdapter);
//        mMySongsListView =(RecyclerView)myView.findViewById(R.id.listViewAlbum);
        currentPage = 0;

        mEditSearch = (EditText) myView.findViewById(R.id.editSearch);
        mEditSearch.setText("");
        mEditSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                /*if (mEditSearch.getText().toString().trim().length() >= 3) {
                    getSearchedSOngs(mEditSearch.getText().toString().trim());
                }*/
                isSearch = true;
                if (mEditSearch.getText().toString().length() == 0)
                    getSearchedSOngs(mEditSearch.getText().toString().trim());
                else if (mEditSearch.getText().toString().length() == 1)
                    getSearchedSOngs(mEditSearch.getText().toString().trim());
                else if (mEditSearch.getText().toString().length() > 0)
                    getSearchedSOngs(mEditSearch.getText().toString().trim());
                else if (mEditSearch.getText().toString().length() > 1)
                    getSearchedSOngs(mEditSearch.getText().toString().trim());
                else if (mEditSearch.getText().toString().length() > 2)
                    getSearchedSOngs(mEditSearch.getText().toString().trim());
                else if (mEditSearch.getText().toString().length() > 3)
                    getSearchedSOngs(mEditSearch.getText().toString().trim());
                else if (mEditSearch.getText().toString().length() > 4)
                    getSearchedSOngs(mEditSearch.getText().toString().trim());
                else if (mEditSearch.getText().toString().length() > 5)
                    getSearchedSOngs(mEditSearch.getText().toString().trim());

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

//        getSearchedSOngs("  ");
        String more = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_TXT_MORE, "none");

        if (more.equalsIgnoreCase("more")) {
            getSearchedSOngs("");
        } else {
            getAllJokes(0);
        }

        swipyRefreshLayoutDirection.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                if(direction == SwipyRefreshLayoutDirection.TOP){
                    if(isSearch)
                        getSearchedSOngs(mEditSearch.getText().toString().trim());
                    else
                        getAllJokes(0);
                    currentPage = 0;
                } else {
                    if(isSearch)
                        getSearchedSOngs(mEditSearch.getText().toString().trim());
                    else
                        getAllJokes(currentPage);
                }
            }
        });
    }


    public   void getAllJokes(final int currentPage) {

        MyApplication myApplication = new MyApplication();

        if (Utility.getSharedInstance().isConnectedToInternet(getContext())) {
            RequestSearchMedia requestSearchMedia = new RequestSearchMedia();

            requestSearchMedia.setLimit("10");
            requestSearchMedia.setOffset("0");
            requestSearchMedia.setSearchterm("");

            String cat= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_LANG_ID,"1");
            requestSearchMedia.setAlbum_id(cat);


            if (cat.equalsIgnoreCase("1")) {
                mTxtLang.setText("English");
            } else if(cat.equalsIgnoreCase("2")){
                mTxtLang.setText("Hindi");
            }

            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().searchJokes(X_API_KEY, requestSearchMedia);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    // JsonObject responseData = response.body();
                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());
                    if (responseData != null) {
                        if (responseData.get("error").getAsInt() == 0) {
                            // Config.getPlanDAO = new GetPlanDAO();
                            Gson gson = new Gson();
                            Utility.getSharedInstance().dismissProgressDialog();
                            Type listType = new TypeToken<List<SongDetail>>() {
                            }.getType();

                            if (responseData.get("respon") != null) {

                                JsonArray jsonArray = responseData.get("respon").getAsJsonArray();

                                if (jsonArray.size() > 0) {
                                    mTxtWarn.setVisibility(View.GONE);
                                    mMySongsListView.setVisibility(View.VISIBLE);
                                    if (currentPage == 0) {
                                        mSongss = gson.fromJson(responseData.get("respon"), listType);
                                        swipyRefreshLayoutDirection.setRefreshing(false);
                                        getMusicList(mSongss);
                                    } else {
                                        ArrayList<SongDetail> datumList = gson.fromJson(responseData.get("respon"), listType);
                                        mSongss.addAll(mSongss.size(), datumList);
                                        swipyRefreshLayoutDirection.setRefreshing(false);
                                        getUpdatedList();
                                    }

                                    if (mSongss != null) {
                                        if (mSongss.size() < 0) {
                                            swipyRefreshLayoutDirection.setRefreshing(false);
                                            mTxtWarn.setVisibility(View.VISIBLE);
                                            mMySongsListView.setVisibility(View.GONE);
                                        }
                                    }
                                } else {
                                    swipyRefreshLayoutDirection.setRefreshing(false);
                                    Utility.getSharedInstance().dismissProgressDialog();
                                    if (mSongss != null && mSongss.size() > 0) {
                                        mTxtWarn.setVisibility(View.GONE);
                                        mMySongsListView.setVisibility(View.VISIBLE);
                                        Toast.makeText(getActivity(), "No more songs", Toast.LENGTH_SHORT).show();
                                    } else {
                                        mTxtWarn.setVisibility(View.VISIBLE);
                                        mMySongsListView.setVisibility(View.GONE);
                                    }
                                }
                            }
                            // Utility.getSharedInstance().Snackbar(toolbarLayout, "Password Changed Sucessfully");
                        } else {
                            swipyRefreshLayoutDirection.setRefreshing(false);
                            Utility.getSharedInstance().dismissProgressDialog();
                            mTxtWarn.setVisibility(View.VISIBLE);
                            mMySongsListView.setVisibility(View.GONE);
                        }
                    } else {
                        swipyRefreshLayoutDirection.setRefreshing(false);
                        Utility.getSharedInstance().dismissProgressDialog();
                        mTxtWarn.setVisibility(View.VISIBLE);
                        mMySongsListView.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Utility.getSharedInstance().dismissProgressDialog();
                    swipyRefreshLayoutDirection.setRefreshing(false);
                    /*mTxtWarn.setVisibility(View.VISIBLE);
                    mMySongsListView.setVisibility(View.GONE);*/
                    Toast.makeText(getActivity(), "Server error .. Please try again after some time", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            swipyRefreshLayoutDirection.setRefreshing(false);
            Toast.makeText(getActivity(), "No internet connection available", Toast.LENGTH_SHORT).show();
        }
    }

    private void getMusicList(ArrayList<SongDetail> songDetailList) {
        jokesAdapter = new JokesAdapter(getActivity(), songDetailList, JokesFragment.this);
        mMySongsListView.setAdapter(jokesAdapter);

        if(jokesAdapter !=null)
            jokesAdapter.notifyDataSetChanged();
        currentPage = currentPage+10;
    }

    private void getUpdatedList() {
        if(jokesAdapter !=null)
            jokesAdapter.notifyDataSetChanged();
        currentPage = currentPage+10;
    }

    /////////////////////GET SERACHED SONGS//////////////////////////


    public void getSearchedSOngs(String searchText) {


        MyApplication myApplication = new MyApplication();

        //Utility.getSharedInstance().showProgressDialog(getActivity());





        if (Utility.getSharedInstance().isConnectedToInternet(getContext())) {

           RequestSearchMedia requestSearchMedia = new RequestSearchMedia();

        requestSearchMedia.setLimit("100");
        requestSearchMedia.setOffset("0");
        requestSearchMedia.setSearchterm(searchText);

        String cat= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_LANG_ID,"1");

            if (cat.equalsIgnoreCase("1")) {
                mTxtLang.setText("English");
            } else if(cat.equalsIgnoreCase("2")){
                mTxtLang.setText("Hindi");
            }
       requestSearchMedia.setAlbum_id(cat);

            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().searchJokes(X_API_KEY, requestSearchMedia);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());

                    if (responseData.get("error").getAsInt() == 0) {
                        // Config.getPlanDAO = new GetPlanDAO();
                        Gson gson = new Gson();

                        //Utility.getSharedInstance().dismissProgressDialog();


                        Type listType = new TypeToken<List<SongDetail>>() {
                        }.getType();
                        if (responseData.get("respon") != null) {

                            JsonArray jsonArray = responseData.get("respon").getAsJsonArray();


                            // JsonObject jsonObject=responseData.getAsJsonObject("respon");

                            if (jsonArray.size() > 0) {

                                mSongss = gson.fromJson(responseData.get("respon"), listType);
                                mTxtWarn.setVisibility(View.GONE);
                                mMySongsListView.setVisibility(View.VISIBLE);
                                swipyRefreshLayoutDirection.setRefreshing(false);


//                                AudioSerachSongsAdapter jokesAdapter = new AudioSerachSongsAdapter(getActivity(), mSongs);

                                if(mSongss!=null)
                                {
                                    if(mSongss.size()<0)
                                    {
                                        swipyRefreshLayoutDirection.setRefreshing(false);
                                        mTxtWarn.setVisibility(View.VISIBLE);
                                        mMySongsListView.setVisibility(View.GONE);
                                    }
                                }

                                jokesAdapter = new JokesAdapter(getActivity(), mSongss, JokesFragment.this);
                                mMySongsListView.setAdapter(jokesAdapter);
                            } else {
                                swipyRefreshLayoutDirection.setRefreshing(false);
                                mTxtWarn.setVisibility(View.VISIBLE);
                                mMySongsListView.setVisibility(View.GONE);
                                //Utility.getSharedInstance().dismissProgressDialog();
                            }


                        }


                        // Utility.getSharedInstance().Snackbar(toolbarLayout, "Password Changed Sucessfully");


                    } else {
                        swipyRefreshLayoutDirection.setRefreshing(false);
                        //Utility.getSharedInstance().dismissProgressDialog();
                        mTxtWarn.setVisibility(View.VISIBLE);
                        mMySongsListView.setVisibility(View.GONE);

                    }

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                    //Utility.getSharedInstance().dismissProgressDialog();
                    mTxtWarn.setVisibility(View.VISIBLE);
                    mMySongsListView.setVisibility(View.GONE);

                }


            });
        } else {
            swipyRefreshLayoutDirection.setRefreshing(false);
            Toast.makeText(getActivity(), "No internet connection available", Toast.LENGTH_SHORT).show();
        }
    }


    public void getCateGorySongs(String catID) {


        MyApplication myApplication = new MyApplication();

        Utility.getSharedInstance().showProgressDialog(getActivity());


        if (Utility.getSharedInstance().isConnectedToInternet(getContext())) {

            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");
            String cat = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT, "1");
            // id="1";
            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");

            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().getSongsByCatID(X_API_KEY, catID, "10", "0");
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());

                    if (responseData.get("error").getAsInt() == 0) {
                        // Config.getPlanDAO = new GetPlanDAO();
                        Gson gson = new Gson();

                        Utility.getSharedInstance().dismissProgressDialog();


                        Type listType = new TypeToken<List<SongDetail>>() {
                        }.getType();
                        if (responseData.get("respon") != null) {

                            JsonArray jsonArray = responseData.get("respon").getAsJsonArray();


                            // JsonObject jsonObject=responseData.getAsJsonObject("respon");

                            if (jsonArray.size() > 0) {

                                mSongss = gson.fromJson(responseData.get("respon"), listType);
                                mTxtWarn.setVisibility(View.GONE);
                                mMySongsListView.setVisibility(View.VISIBLE);
                                swipyRefreshLayoutDirection.setRefreshing(false);


                                jokesAdapter = new JokesAdapter(getActivity(), mSongss, JokesFragment.this);
                                mMySongsListView.setAdapter(jokesAdapter);
                            } else {
                                swipyRefreshLayoutDirection.setRefreshing(false);
                                mTxtWarn.setVisibility(View.VISIBLE);
                                mMySongsListView.setVisibility(View.GONE);
                                Utility.getSharedInstance().dismissProgressDialog();
                            }


                        }


                        // Utility.getSharedInstance().Snackbar(toolbarLayout, "Password Changed Sucessfully");


                    } else {
                        swipyRefreshLayoutDirection.setRefreshing(false);
                        Utility.getSharedInstance().dismissProgressDialog();
                        mTxtWarn.setVisibility(View.VISIBLE);
                        mMySongsListView.setVisibility(View.GONE);

                    }

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                    Utility.getSharedInstance().dismissProgressDialog();
                    mTxtWarn.setVisibility(View.VISIBLE);
                    mMySongsListView.setVisibility(View.GONE);

                }


            });
        } else {

        }
    }


    public void playPoase(int postion) {
        PostionOfPlay = postion;
        try {
            if (mSongss != null)
                for (int i = 0; i < mSongss.size(); i++) {
                    if (i != postion)
                        mSongss.get(i).setPlayStop(false);

                }
            if (mSongss.get(postion).getPlayStop()) {
                mSongss.get(postion).setPlayStop(false);
            } else {
                mSongss.get(postion).setPlayStop(true);
            }

            jokesAdapter.notifyDataSetChanged();
        } catch (Exception e){
            e.printStackTrace();
        }

    }


}
