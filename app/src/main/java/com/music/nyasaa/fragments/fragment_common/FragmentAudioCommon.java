package com.music.nyasaa.fragments.fragment_common;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.music.nyasaa.R;
import com.music.nyasaa.adapter.GetAllDiscouresAdapter;
import com.music.nyasaa.adapter.MenuListAdapter;
import com.music.nyasaa.controller.MyApplication;
import com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity;
import com.music.nyasaa.models.RequestModel.SearchMedia.RequestSearchMedia;
import com.music.nyasaa.models.ResponseModel.GetCategoryAll.GetResponseCategory;
import com.music.nyasaa.models.ResponseModel.GetCategoryAll.GetResponseCategoryData;
import com.music.nyasaa.models.SongDetail;
import com.music.nyasaa.utility.SharedPreferenceManager;
import com.music.nyasaa.utility.Utility;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.music.nyasaa.retrofit.Config.X_API_KEY;

public class FragmentAudioCommon extends Fragment implements View.OnClickListener {
    boolean post=false,request=false;
    private View myView;
    private static final String ARG_KEY_NUMBER = "tab_number";
    private static final String ARG_KEY_DIS = "";

    private static Context context;
    private List<GetResponseCategoryData> getResponseCategoryData;
    private RecyclerView coomonGridView;
    private TextView mTxtMember, mTxtRequest, mTxtPost;
    private LinearLayout mLayMembers, mLayRequest, mLayMainBar, mLayPost;
    public static ArrayList<SongDetail> mSongss = new ArrayList<>();
    private TextView mTxtLang,mTxtWarn;
    private ImageView mImgLang;

    public static FragmentAudioCommon newInstance(int number, String dis) {
        Bundle args = new Bundle();
        args.putInt(ARG_KEY_NUMBER, number);
        args.putString(ARG_KEY_DIS, dis);
        FragmentAudioCommon frag = new FragmentAudioCommon();
        frag.setArguments(args);
        return frag;
    }


    public FragmentAudioCommon() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        myView = inflater.inflate(R.layout.fragment_common, container, false);
        initView(myView);
        onClick();
        String dis = "";
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            dis = bundle.getString(ARG_KEY_DIS, "");
        }
        if(dis.equalsIgnoreCase("dis")){
            secondColor();
        } else {
            firstColor();
            SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_DRAWER_TYPE, "1");
            ((DashBoardActivity) Objects.requireNonNull(getActivity())).toolbarStatusBar(" Audio");
        }
        return myView;
    }


    private void onClick()
    {
        mLayRequest.setOnClickListener(this);
        mLayMembers.setOnClickListener(this);
        mLayPost.setOnClickListener(this);
    }

    private void initView(View myView) {

//        myView.getRootView().setBackgroundColor(Color.BLACK);
        myView.getRootView().setBackgroundColor(getResources().getColor(R.color.orBLACK));
        coomonGridView =(RecyclerView) myView.findViewById(R.id.commonGrid);
//        myView.getRootView().setBackgroundResource(R.drawable.black_gradient);
        coomonGridView.setLayoutManager(new GridLayoutManager(getActivity(), 3));


        mTxtWarn = (TextView) myView.findViewById(R.id.txtWarn);
        mTxtLang = (TextView) myView.findViewById(R.id.txtLang);
        mImgLang = (ImageView) myView.findViewById(R.id.imgLang);
        mLayMainBar = (LinearLayout) myView.findViewById(R.id.menuLayBar);
        mLayMembers = (LinearLayout) myView.findViewById(R.id.layMember);
        mLayRequest = (LinearLayout) myView.findViewById(R.id.layRequest);

        mLayPost = (LinearLayout) myView.findViewById(R.id.layPost);


        mTxtMember = (TextView) myView.findViewById(R.id.txtMembers);

        mTxtRequest = (TextView) myView.findViewById(R.id.txtRequest);

        mTxtPost = (TextView) myView.findViewById(R.id.txtPost);
//        mGridAlbumview =(RecyclerView)myView.findViewById(R.id.listViewAlbum);
    }


    public void getAllFilterCategory(final View view) {




        getActivity().runOnUiThread(new Runnable() {
//
            @Override
            public void run() {

                // Stuff that updates the UI

                MyApplication myApplication = new MyApplication();


                if (Utility.getSharedInstance().isConnectedToInternet(getActivity())) {


                    Utility.getSharedInstance().showProgressDialog(getActivity());
                    String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");

                    // id="1";
                    // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");

                    String cat = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT, "1");
                    Call<JsonObject> callbackLogin = myApplication.getAPIInstance().getCategoriesOSHO(X_API_KEY, "60", "0");
                    //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
                    callbackLogin.enqueue(new Callback<JsonObject>() {
                        @Override
                        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                            // JsonObject responseData = response.body();

                            JsonObject responseData = response.body();
                            // Log.e("Response::", responseData.toString());


                            // Config.getPlanDAO = new GetPlanDAO();
                            Gson gson = new Gson();

                            Utility.getSharedInstance().dismissProgressDialog();


                            if (responseData.get("respon") != null) {

                                JsonArray jsonArray = responseData.get("respon").getAsJsonArray();


                                // JsonObject jsonObject=responseData.getAsJsonObject("respon");

                                if (jsonArray.size() > 0) {


                                    GetResponseCategory getResponseCategory = gson.fromJson(responseData, GetResponseCategory.class);
                                    getResponseCategoryData = getResponseCategory.getRespon();

                                    MenuListAdapter menuListAdapter = new MenuListAdapter((DashBoardActivity) getActivity(), getResponseCategoryData);
                                    coomonGridView.setAdapter(menuListAdapter);
                                    coomonGridView.setVisibility(View.VISIBLE);
                                    mTxtWarn.setVisibility(View.GONE);

                                } else {
                                    mTxtWarn.setVisibility(View.VISIBLE);
                                    coomonGridView.setVisibility(View.GONE);


                                }


                                // Utility.getSharedInstance().Snackbar(toolbarLayout, "Password Changed Sucessfully");


                            } else {
                                mTxtWarn.setVisibility(View.VISIBLE);
                                coomonGridView.setVisibility(View.GONE);

                            }

                        }

                        @Override
                        public void onFailure(Call<JsonObject> call, Throwable t) {

                            mTxtWarn.setVisibility(View.VISIBLE);
                            coomonGridView.setVisibility(View.GONE);
                        }


                    });
                } else {

                    mTxtWarn.setVisibility(View.VISIBLE);
                    coomonGridView.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), "No internet connection available", Toast.LENGTH_SHORT).show();
                }



            }
        });
    }

    private void firstColor() {
        mTxtRequest.setTextColor(getResources().getColor(R.color.white));
        mTxtMember.setTextColor(getResources().getColor(R.color.black));

        mTxtPost.setTextColor(getResources().getColor(R.color.white));
        mLayPost.setBackgroundColor(getResources().getColor(R.color.darker_grey));
        mLayMembers.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        mLayRequest.setBackgroundColor(getResources().getColor(R.color.darker_grey));
        getAllFilterCategory(myView);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {

            case R.id.layMember:
                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_DRAWER_TYPE,"1");
                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_AUDIO_TAB,"aud1");
                ((DashBoardActivity) Objects.requireNonNull(getActivity())).toolbarStatusBar(" Audio");
                getAllFilterCategory(view);
                post=false;
                request=false;


                post=false;
                mTxtRequest.setTextColor(getResources().getColor(R.color.white));
                mTxtMember.setTextColor(getResources().getColor(R.color.black));

                mTxtPost.setTextColor(getResources().getColor(R.color.white));
                mLayPost.setBackgroundColor(getResources().getColor(R.color.darker_grey));
                mLayMembers.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                mLayRequest.setBackgroundColor(getResources().getColor(R.color.darker_grey));
                mImgLang.setVisibility(View.GONE);
                mTxtLang.setVisibility(View.GONE);
                break;
            case R.id.layRequest:
                post=false;
                request=true;

                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_DRAWER_TYPE,"3");
                ((DashBoardActivity) Objects.requireNonNull(getActivity())).toolbarStatusBar(" Audio");
                getAllDiscoursesAudio(true);

                mTxtRequest.setTextColor(getResources().getColor(R.color.black));
                mTxtMember.setTextColor(getResources().getColor(R.color.white));

                mTxtPost.setTextColor(getResources().getColor(R.color.white));
                mLayPost.setBackgroundColor(getResources().getColor(R.color.darker_grey));

                mLayMembers.setBackgroundColor(getResources().getColor(R.color.darker_grey));
                mLayRequest.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

                mImgLang.setVisibility(View.VISIBLE);
                mTxtLang.setVisibility(View.VISIBLE);
                break;

            case R.id.layPost:
                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_DRAWER_TYPE,"3");
                ((DashBoardActivity) Objects.requireNonNull(getActivity())).toolbarStatusBar(" Audio");
                getAllDiscoursesAudio(true);

                request=false;

                post=true;
//                getRequetstData(userID);
                mTxtRequest.setTextColor(getResources().getColor(R.color.white));
                mTxtMember.setTextColor(getResources().getColor(R.color.white));
                mTxtPost.setTextColor(getResources().getColor(R.color.black));

                mLayMembers.setBackgroundColor(getResources().getColor(R.color.darker_grey));
                mLayRequest.setBackgroundColor(getResources().getColor(R.color.darker_grey));
                mLayPost.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

                mImgLang.setVisibility(View.VISIBLE);
                mTxtLang.setVisibility(View.VISIBLE);

                break;


            default:

                break;

        }

    }

    public void secondColor(){
        request=false;
        post=true;

        SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_DRAWER_TYPE,"3");
        ((DashBoardActivity) Objects.requireNonNull(getActivity())).toolbarStatusBar(" Audio");
        getAllDiscoursesAudio(true);

        mTxtRequest.setTextColor(getResources().getColor(R.color.white));
        mTxtMember.setTextColor(getResources().getColor(R.color.white));
        mTxtPost.setTextColor(getResources().getColor(R.color.black));

        mLayMembers.setBackgroundColor(getResources().getColor(R.color.darker_grey));
        mLayRequest.setBackgroundColor(getResources().getColor(R.color.darker_grey));
        mLayPost.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

        mImgLang.setVisibility(View.VISIBLE);
        mTxtLang.setVisibility(View.VISIBLE);
    }


    public void getAllDiscoursesAudio(final boolean isFirst) {



        getActivity().runOnUiThread(new Runnable() {
//
            @Override
            public void run() {

                // Stuff that updates the UI




                MyApplication myApplication = new MyApplication();

                //Utility.getSharedInstance().showProgressDialog(getActivity());
                String lang = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_LANG_ID, "1");
                if (lang.equalsIgnoreCase("1")) {
                    mTxtLang.setText("English");
                } else if(lang.equalsIgnoreCase("2")) {
                    mTxtLang.setText("Hindi");
                }

                RequestSearchMedia requestSearchMedia = new RequestSearchMedia();
                if(isFirst){
                    requestSearchMedia.setAlbum_id("1");
                    mTxtLang.setText("English");
                    SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_LANG_ID, "1");
                } else {
                    requestSearchMedia.setAlbum_id(lang);
                }

                requestSearchMedia.setLimit("1000");
                requestSearchMedia.setQuedisc(2);
                requestSearchMedia.setShould_orderby_name(true);
//        requestSearchMedia.setAlbum_id(SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_LANG_ID,"none"));
//        requestSearchMedia.setMedia_type_id(SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT_ORIGINAL,"none"));
                requestSearchMedia.setOffset("0");
                requestSearchMedia.setSearchterm("");

                String cat= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT,"0");
//        String cat= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT,"0");
//        requestSearchMedia.setCategory(cat);


                if (Utility.getSharedInstance().isConnectedToInternet(getContext())) {

                    Utility.getSharedInstance().showProgressDialog(getActivity());

//            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");
//            String cat = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT, "1");
//            // id="1";
//            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");

                    Call<JsonObject> callbackLogin = myApplication.getAPIInstance().searchAudioQA(X_API_KEY, requestSearchMedia);
                    //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
                    callbackLogin.enqueue(new Callback<JsonObject>() {
                        @Override
                        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                            // JsonObject responseData = response.body();

                            JsonObject responseData = response.body();


                            if(responseData!=null) {

                                if (responseData.get("error").getAsInt() == 0) {
                                    // Config.getPlanDAO = new GetPlanDAO();
                                    Gson gson = new Gson();

                                    Utility.getSharedInstance().dismissProgressDialog();


                                    Type listType = new TypeToken<List<SongDetail>>() {
                                    }.getType();
                                    if (responseData.get("respon") != null) {

                                        JsonArray jsonArray = responseData.get("respon").getAsJsonArray();


                                        if (jsonArray.size() > 0) {

                                            mTxtWarn.setVisibility(View.GONE);
                                            coomonGridView.setVisibility(View.VISIBLE);

                                            mSongss = gson.fromJson(responseData.get("respon"), listType);
                                            getDiscouseData(mSongss);


                                        } else {

                                            mTxtWarn.setVisibility(View.VISIBLE);
                                            coomonGridView.setVisibility(View.GONE);

                                            Utility.getSharedInstance().dismissProgressDialog();
                                        }
                                    }
                                    else {

                                        mTxtWarn.setVisibility(View.VISIBLE);
                                        coomonGridView.setVisibility(View.GONE);

//                                coomonGridView.setVisibility(View.GONE);
                                    }
                                    // Utility.getSharedInstance().Snackbar(toolbarLayout, "Password Changed Sucessfully");
                                }


                            }
                            else {

                                mTxtWarn.setVisibility(View.VISIBLE);
                                coomonGridView.setVisibility(View.GONE);

//                        coomonGridView.setVisibility(View.GONE);

                            }

                        }

                        @Override
                        public void onFailure(Call<JsonObject> call, Throwable t) {

                            mTxtWarn.setVisibility(View.VISIBLE);
                            coomonGridView.setVisibility(View.GONE);


                        }


                    });
                } else {

                    mTxtWarn.setVisibility(View.VISIBLE);
                    coomonGridView.setVisibility(View.GONE);

//            swipyRefreshLayoutDirection.setRefreshing(false);
                }


            }
        });













    }


    ///  Video Discousers







    private void getDiscouseData(ArrayList<SongDetail> mSongss)
    {

        coomonGridView.setVisibility(View.VISIBLE);
       GetAllDiscouresAdapter shortSongsAdapter = new GetAllDiscouresAdapter(getActivity(), mSongss);
        coomonGridView.setAdapter(shortSongsAdapter);
    }


}