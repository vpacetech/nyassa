package com.music.nyasaa.fragments.community;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.music.nyasaa.R;
import com.music.nyasaa.adapter.CommentsAdapter;
import com.music.nyasaa.adapter.CommunityMembersAdapter;
import com.music.nyasaa.adapter.CommunityPostAdapter;
import com.music.nyasaa.adapter.SerachMemberAdapter;
import com.music.nyasaa.controller.MyApplication;
import com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity;
import com.music.nyasaa.models.RequestModel.AddToCommunity.RequestShareCommunityPost;
import com.music.nyasaa.models.RequestModel.AddToCommunity.RequestToAcceptCommunity;
import com.music.nyasaa.models.RequestModel.DeleteMember.RequestDeleteMember;
import com.music.nyasaa.models.RequestModel.GetCommunityRequest.GetRequestSentbyCommunity;
import com.music.nyasaa.models.RequestModel.GetCommunityRequest.GetRequestSentbyCommunityData;
import com.music.nyasaa.models.RequestModel.SerachUsers.RequestToSearchUser;
import com.music.nyasaa.models.ResponseModel.GetComments.GetCommentsModel;
import com.music.nyasaa.models.ResponseModel.GetComments.GetCommentsModelData;
import com.music.nyasaa.models.ResponseModel.GetMembersofCommunity.ResponseGetCommunityMem;
import com.music.nyasaa.models.ResponseModel.GetMembersofCommunity.ResponseGetCommunityMemData;
import com.music.nyasaa.models.ResponseModel.GetSeacrchedMembers.GetSearchMemberResponse;
import com.music.nyasaa.models.ResponseModel.GetSeacrchedMembers.GetSearchMemberResponseData;
import com.music.nyasaa.models.SongDetail;
import com.music.nyasaa.utility.SharedPreferenceManager;
import com.music.nyasaa.utility.SwipeForMembers;
import com.music.nyasaa.utility.Utility;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity.PostionOfPlay;
import static com.music.nyasaa.retrofit.Config.X_API_KEY;

public class FragmentCommunityDetails extends Fragment implements View.OnClickListener, SwipeForMembers.RecyclerItemTouchHelperListener {
    Handler h = new Handler();
    int delay = 800000000; //1 second=1000 milisecond, 15*1000=15seconds
    Runnable runnable;
    private View myView;
    private static final String ARG_KEY_NUMBER = "tab_number";
    private RecyclerView mListMembers, mListSeacrList;
    private TextView mTxtWarn;
    private SwipyRefreshLayout swipyRefreshLayoutDirection;
    private Dialog confrimDeleteDialog;
    private List<ResponseGetCommunityMemData> responseGetCommunityMemDataLis;
    private CommunityMembersAdapter communityMembersAdapter;
    private EditText mEditSearch;
    private String userID, CommParentID;
    private boolean swipeBoleaan = true;

    public  ArrayList<SongDetail> mSongss;
    private TextView mTxtMember, mTxtRequest, mTxtPost;
    private LinearLayout mLayMembers, mLayRequest, mLayMainBar, mLayPost;
    boolean post = false, request = false;
    private static Context context;
    private CommunityPostAdapter communityPostAdapter;
    public static FragmentCommunityDetails fragmentCommunityDetails;
    private int currentPage = 0;

    private MyApplication myApplication;
    private LinearLayout layoutComments;
    private String name[] = {"Jack DJ", "Sandy DJ", "Pigo Rock n Roll", "Jackson Vintage", "Johnson Blaster", "Jorge Dance"};

    private LinearLayout mButPost;
    private EditText mEditPostComment;

    public static FragmentCommunityDetails newInstance(int number) {
        Bundle args = new Bundle();
        args.putInt(ARG_KEY_NUMBER, number);
        FragmentCommunityDetails frag = new FragmentCommunityDetails();
        frag.setArguments(args);
        return frag;
    }

    public static FragmentCommunityDetails newInstance(Context mContext) {
        FragmentCommunityDetails f = new FragmentCommunityDetails();
        context = mContext;
        return f;
    }

    public FragmentCommunityDetails() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        myView = inflater.inflate(R.layout.activity_community_members, container, false);


        myApplication = new MyApplication();
        fragmentCommunityDetails = this;
        initView(myView);
//        firstColor();
        onClicks();
        return myView;

    }

    private void initView(View myView) {

        userID = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "none");
//        myView.getRootView().setBackgroundColor(Color.BLACK);
        myView.getRootView().setBackgroundColor(getResources().getColor(R.color.orBLACK));

//        myView.getRootView().setBackgroundResource(R.drawable.black_gradient);


        mButPost = (LinearLayout) myView.findViewById(R.id.butPost);
//        mLayScroll = (LinearLayout) myView.findViewById(R.id.layScrollUP);
        mEditPostComment = (EditText) myView.findViewById(R.id.editComment);

        mLayMainBar = (LinearLayout) myView.findViewById(R.id.menuLayBar);
        mLayMembers = (LinearLayout) myView.findViewById(R.id.layMember);
        mLayRequest = (LinearLayout) myView.findViewById(R.id.layRequest);
        layoutComments = (LinearLayout) myView.findViewById(R.id.layoutComments);
        layoutComments.setVisibility(View.GONE);

        mLayPost = (LinearLayout) myView.findViewById(R.id.layPost);


        mTxtMember = (TextView) myView.findViewById(R.id.txtMembers);

        mTxtRequest = (TextView) myView.findViewById(R.id.txtRequest);

        mTxtPost = (TextView) myView.findViewById(R.id.txtPost);

        TextView mTxtToolbaTxt = (TextView) myView.findViewById(R.id.txtTitleToolbar);
        LinearLayout linearLayout = (LinearLayout) myView.findViewById(R.id.toolLayout);

        linearLayout.setBackgroundColor(Color.parseColor("#050505"));
        mTxtToolbaTxt.setText("Friends");

        mTxtWarn = (TextView) myView.findViewById(R.id.txtWarn);
        mEditSearch = (EditText) myView.findViewById(R.id.editSearch);
        mTxtWarn.setVisibility(View.GONE);

        swipyRefreshLayoutDirection = (SwipyRefreshLayout) myView.findViewById(R.id.swipe_refresh_layout);
        swipyRefreshLayoutDirection.setDirection(SwipyRefreshLayoutDirection.BOTH);


        userID = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");
        CommParentID = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_IS_OWN_COMMUNITY, "none");

        //        ImageView imgEdit = (ImageView) myView.(R.id.img_notification);
//
//        imgEdit.setImageResource(0);
//
//        imgEdit.setImageResource(R.drawable.ic_action_followers);
        if (userID.equalsIgnoreCase(CommParentID)) {

            mLayMainBar.setVisibility(View.VISIBLE);
            mLayPost.setVisibility(View.VISIBLE);
            mLayMembers.setVisibility(View.VISIBLE);
            mLayRequest.setVisibility(View.VISIBLE);
            mLayMainBar.setWeightSum(2);
        } else {
            mLayMainBar.setWeightSum(2);
            mLayMainBar.setVisibility(View.VISIBLE);

            mLayPost.setVisibility(View.VISIBLE);
            mLayMembers.setVisibility(View.VISIBLE);
            mLayRequest.setVisibility(View.GONE);
        }


//        imgEdit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                String owner = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_IS_OWN_COMMUNITY, "none");
//                if (owner.equalsIgnoreCase("0")) {
//
//                    Toast.makeText(CommunityMembersActivity.this, "Your not owner for this community", Toast.LENGTH_SHORT).show();
//                } else {
//                    Intent intent = new Intent(CommunityMembersActivity.this, AddMemberActivity.class);
//                    startActivity(intent);
//                    overridePendingTransition(R.anim.left, R.anim.right);
//                }
//
//            }
//        });


        mListMembers = (RecyclerView) myView.findViewById(R.id.listViewMembers);
        mListSeacrList = (RecyclerView) myView.findViewById(R.id.listViewSearchMembers);
        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        mListMembers.setLayoutManager(linearLayoutManager);
        mListMembers.setItemAnimator(new DefaultItemAnimator());


        RecyclerView.LayoutManager linearLayoutManagers = new LinearLayoutManager(getActivity());
        mListSeacrList.setLayoutManager(linearLayoutManagers);
        mListSeacrList.setItemAnimator(new DefaultItemAnimator());


        String parentID = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_IS_OWN_COMMUNITY, "none");
        String userID = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "noe");

        /*if (parentID.equalsIgnoreCase(userID)) {
            ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new SwipeForMembers(0, ItemTouchHelper.LEFT, (SwipeForMembers.RecyclerItemTouchHelperListener) this);
            new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(mListMembers);
        }*/


    }

    private void onClicks() {
        mButPost.setOnClickListener(this);
//        mLayScroll.setOnClickListener(this);
        mLayRequest.setOnClickListener(this);
        mLayMembers.setOnClickListener(this);
        mLayPost.setOnClickListener(this);
        mLayMainBar.setVisibility(View.VISIBLE);
        currentPage = 0;
        getCommunityPost(currentPage);
        swipyRefreshLayoutDirection.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {

                if (swipeBoleaan == true) {
                    if (direction == SwipyRefreshLayoutDirection.TOP) {
                        getCommunityPost(0);
                        currentPage = 0;
                    } else {
                        getCommunityPost(currentPage);
                    }
                    swipyRefreshLayoutDirection.setRefreshing(false);
                }


            }
        });
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {

//            case R.id.layScrollUP:
//                Toast.makeText(getActivity(), "Hey now you can scroll the chat!", Toast.LENGTH_SHORT).show();
//                h.removeCallbacksAndMessages(null);
//                break;

            case R.id.butPost:


                if (mEditPostComment.getText().toString().trim().equalsIgnoreCase("")) {
                    Toast.makeText(getActivity(), "Enter Something to post!", Toast.LENGTH_SHORT).show();
                } else {

                    postCommunity(SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_COMM_ID, ""));
                }
                break;

            case R.id.layMember:
                h.removeCallbacksAndMessages(null);
                swipeBoleaan = true;
                delay = 800000000;
                getCommunityPost(0);
                layoutComments.setVisibility(View.GONE);
                post = false;
                request = false;
//                getAllMembers();

                post = false;
                mTxtRequest.setTextColor(getResources().getColor(R.color.white));
                mTxtMember.setTextColor(getResources().getColor(R.color.black));

                mTxtPost.setTextColor(getResources().getColor(R.color.white));
                mLayPost.setBackgroundColor(getResources().getColor(R.color.darker_grey));
                mLayMembers.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                mLayRequest.setBackgroundColor(getResources().getColor(R.color.darker_grey));
                break;
            case R.id.layRequest:
                delay = 1000;

                h.postDelayed(runnable = new Runnable() {
                    public void run() {
                        //do something

                        getAllComments(0);
                        h.postDelayed(runnable, delay);
                    }
                }, delay);

                swipeBoleaan = false;
                post = false;
                request = true;
                layoutComments.setVisibility(View.VISIBLE);
                getAllComments(0);
//                getRequetstData(userID);
                mTxtRequest.setTextColor(getResources().getColor(R.color.black));
                mTxtMember.setTextColor(getResources().getColor(R.color.white));

                mTxtPost.setTextColor(getResources().getColor(R.color.white));
                mLayPost.setBackgroundColor(getResources().getColor(R.color.darker_grey));

                mLayMembers.setBackgroundColor(getResources().getColor(R.color.darker_grey));
                mLayRequest.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

                break;

            case R.id.layPost:
                getAllComments(0);
                delay = 1000;
                swipeBoleaan = false;
                request = false;
                h.postDelayed(runnable = new Runnable() {
                    public void run() {
                        //do something

                        getAllComments(0);
                        h.postDelayed(runnable, delay);
                    }
                }, delay);
                layoutComments.setVisibility(View.VISIBLE);
                post = true;
//                getRequetstData(userID);
                mTxtRequest.setTextColor(getResources().getColor(R.color.white));
                mTxtMember.setTextColor(getResources().getColor(R.color.white));
                mTxtPost.setTextColor(getResources().getColor(R.color.black));

                mLayMembers.setBackgroundColor(getResources().getColor(R.color.darker_grey));
                mLayRequest.setBackgroundColor(getResources().getColor(R.color.darker_grey));
                mLayPost.setBackgroundColor(getResources().getColor(R.color.colorPrimary));


                break;


            default:

                break;

        }

    }


    public void getAllMembers() {


        MyApplication myApplication = new MyApplication();

        if (Utility.getSharedInstance().isConnectedToInternet(getActivity())) {


            Utility.getSharedInstance().showProgressDialog(getActivity());


            String commId = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_COMM_ID, "");


            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().getallCommunityMembers(X_API_KEY, commId, "10", "0");
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());
                    mTxtWarn.setText("No Friends found");
                    if (responseData != null) {


                        if (responseData.get("status").getAsString().equalsIgnoreCase("true")) {
                            // Config.getPlanDAO = new GetPlanDAO();
                            Gson gson = new Gson();
                            Utility.getSharedInstance().dismissProgressDialog();

                            ResponseGetCommunityMem responseGetCommunityMem = gson.fromJson(responseData, ResponseGetCommunityMem.class);
                            responseGetCommunityMemDataLis = responseGetCommunityMem.getResultObject();

                            List<ResponseGetCommunityMemData> sortedDataList = new ArrayList<>();

                            if (responseGetCommunityMemDataLis != null) {

                                for (int i = 0; i < responseGetCommunityMemDataLis.size(); i++) {

                                    ResponseGetCommunityMemData responseGetCommunityMemData = new ResponseGetCommunityMemData();
                                    if (responseGetCommunityMemDataLis.get(i).getAction().equalsIgnoreCase("2")) {

                                        responseGetCommunityMemData.setAction(responseGetCommunityMemDataLis.get(i).getAction());
                                        responseGetCommunityMemData.setCommunityId(responseGetCommunityMemDataLis.get(i).getCommunityId());
                                        responseGetCommunityMemData.setCommunityName(responseGetCommunityMemDataLis.get(i).getCommunityName());
                                        responseGetCommunityMemData.setCreatedAt(responseGetCommunityMemDataLis.get(i).getCreatedAt());
                                        responseGetCommunityMemData.setId(responseGetCommunityMemDataLis.get(i).getId());
                                        responseGetCommunityMemData.setRequest(responseGetCommunityMemDataLis.get(i).getRequest());

                                        responseGetCommunityMemData.setUserId(responseGetCommunityMemDataLis.get(i).getUserId());
                                        responseGetCommunityMemData.setUsername(responseGetCommunityMemDataLis.get(i).getUsername());
                                        responseGetCommunityMemData.setStatus(responseGetCommunityMemDataLis.get(i).getStatus());


                                        sortedDataList.add(responseGetCommunityMemData);
                                    }

                                }
                                if (sortedDataList != null) {

                                    if (sortedDataList.size() > 0)
                                        getMembersList(sortedDataList);
                                    else {
                                        mTxtWarn.setText("No Friends found");
                                        mTxtWarn.setVisibility(View.VISIBLE);
                                        mListMembers.setVisibility(View.GONE);
                                    }
                                } else {

                                }

                            }


                            swipyRefreshLayoutDirection.setRefreshing(false);


                            firstColor();
                            // JsonObject jsonObject=responseData.getAsJsonObject("respon");


                        } else {


                            swipyRefreshLayoutDirection.setRefreshing(false);
                            Utility.getSharedInstance().dismissProgressDialog();
                            mTxtWarn.setText("No Friends found");
                            mTxtWarn.setVisibility(View.VISIBLE);
                            mListMembers.setVisibility(View.GONE);
                        }

                    } else {
                        mTxtWarn.setText("No Friends found");
                        swipyRefreshLayoutDirection.setRefreshing(false);
                        Utility.getSharedInstance().dismissProgressDialog();
                        mTxtWarn.setVisibility(View.VISIBLE);
                        mListMembers.setVisibility(View.GONE);
                    }


                    // Utility.getSharedInstance().Snackbar(toolbarLayout, "Password Changed Sucessfully");


                }

                {


                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                    Utility.getSharedInstance().dismissProgressDialog();
                    mTxtWarn.setVisibility(View.VISIBLE);
                    mListMembers.setVisibility(View.GONE);

                }


            });
        } else

        {

            Utility.getSharedInstance().dismissProgressDialog();
            mTxtWarn.setVisibility(View.VISIBLE);
            mListMembers.setVisibility(View.GONE);


        }
    }

    private void getMembersList(List<ResponseGetCommunityMemData> responseGetCommunityMemDataList) {
        if (responseGetCommunityMemDataList != null && responseGetCommunityMemDataList.size() > 0) {
            mTxtWarn.setVisibility(View.GONE);
            mListMembers.setVisibility(View.VISIBLE);
            communityMembersAdapter = new CommunityMembersAdapter(getActivity(), responseGetCommunityMemDataList);
            mListMembers.setAdapter(communityMembersAdapter);
        } else {
            mTxtWarn.setVisibility(View.VISIBLE);
            mListMembers.setVisibility(View.GONE);
        }

    }

    @Override
    public void onSwiped(final RecyclerView.ViewHolder viewHolder, int direction, int position) {

        if (viewHolder instanceof CommunityMembersAdapter.HeaderHolder) {
            // get the removed item name to display it in snack bar
            final String name = responseGetCommunityMemDataLis.get(viewHolder.getAdapterPosition()).getUsername();

            // backup of removed item for undo purpose
            final ResponseGetCommunityMemData deletedItem = responseGetCommunityMemDataLis.get(viewHolder.getAdapterPosition());
            final int deletedIndex = viewHolder.getAdapterPosition();

            // remove the item from recycler view

            confrimDeleteDialog = new Dialog(getActivity());
            confrimDeleteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            confrimDeleteDialog.setContentView(R.layout.dialog_confrim_delete);
            confrimDeleteDialog.getWindow().getAttributes().width = ActionBar.LayoutParams.FILL_PARENT;
            confrimDeleteDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
            confrimDeleteDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            confrimDeleteDialog.show();
            confrimDeleteDialog.setCancelable(false);

            Button mButYes = (Button) confrimDeleteDialog.findViewById(R.id.butYes);
            Button mButNo = (Button) confrimDeleteDialog.findViewById(R.id.butNo);

            TextView mTxtMsg = (TextView) confrimDeleteDialog.findViewById(R.id.txtMsg);
            mTxtMsg.setText("Are you sure you want to delete?");


            TextView mTxtTitle = (TextView) confrimDeleteDialog.findViewById(R.id.txtDialogTitle);
            mTxtTitle.setText("Member");


            mButNo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    confrimDeleteDialog.dismiss();

                    communityMembersAdapter.notifyDataSetChanged();


                }
            });

            mButYes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    // showing snack bar with Undo option

                    deletMembers(responseGetCommunityMemDataLis.get(viewHolder.getAdapterPosition()).getUserId());

                    communityMembersAdapter.removeItem(viewHolder.getAdapterPosition());
//            snackbar.setAction("UNDO", new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    // undo is selected, restore the deleted item
//
//                }
//            });
//            snackbar.setActionTextColor(Color.YELLOW);
                    //snackbar.show();

                }
            });


        }

    }

    ////////////// Search Members //////////////////


    public void searchMembers(String name) {

        MyApplication myApplication = new MyApplication();


        if (Utility.getSharedInstance().isConnectedToInternet(getActivity())) {
            Utility.getSharedInstance().showProgressDialog(getActivity());
            RequestToSearchUser requestToSearchUser = new RequestToSearchUser();

            requestToSearchUser.setLimit("10");
            requestToSearchUser.setOffset("0");
            requestToSearchUser.setSearchterm(name);


            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().searchUsers(X_API_KEY, requestToSearchUser);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());

                    if (responseData != null) {


                        if (responseData.get("respon") != null) {

                            JsonArray jsonArray = responseData.get("respon").getAsJsonArray();


                            Gson gson = new Gson();
                            // JsonObject jsonObject=responseData.getAsJsonObject("respon");

                            if (jsonArray.size() > 0) {

                                Utility.getSharedInstance().dismissProgressDialog();

                                mTxtWarn.setVisibility(View.GONE);
                                mListMembers.setVisibility(View.GONE);
//                                mListSeacrList.setVisibility(View.VISIBLE);

                                GetSearchMemberResponse getSearchMemberResponse = gson.fromJson(responseData, GetSearchMemberResponse.class);
                                List<GetSearchMemberResponseData> getSearchMemberResponseDataList = getSearchMemberResponse.getRespon();
                                SerachMemberAdapter serachMemberAdapter = new SerachMemberAdapter(getActivity(), getSearchMemberResponseDataList);


                            } else {
                                Utility.getSharedInstance().dismissProgressDialog();
                                mListMembers.setVisibility(View.GONE);
                                mTxtWarn.setVisibility(View.VISIBLE);
//                                mListSeacrList.setVisibility(View.GONE);
                                // getAllMembers();
                            }


                        }

                    } else {
                        Utility.getSharedInstance().dismissProgressDialog();
                        Toast.makeText(getActivity(), "Api issue", Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                    Utility.getSharedInstance().dismissProgressDialog();

                    Toast.makeText(getActivity(), "Api failure", Toast.LENGTH_SHORT).show();
                }


            });
        } else {

            Toast.makeText(getActivity(), "Check internet connection", Toast.LENGTH_SHORT).show();
        }
    }


    /////////////////// Delete Members FROM Community ///////////////////////////////

    public void deletMembers(String memID) {

        MyApplication myApplication = new MyApplication();

        Utility.getSharedInstance().showProgressDialog(getActivity());


        if (Utility.getSharedInstance().isConnectedToInternet(getActivity())) {

            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");

            String comID = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_COMM_ID, "");
            // id="1";
            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");

            RequestDeleteMember requestDeleteMember = new RequestDeleteMember();

            requestDeleteMember.setCommunityId(comID);
            requestDeleteMember.setParentUserId(id);
            requestDeleteMember.setUserId(memID);

            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().deleteMembers(X_API_KEY, requestDeleteMember);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());

                    if (responseData != null) {


                        if (responseData.get("status").getAsString().equalsIgnoreCase("true")) {

                            confrimDeleteDialog.dismiss();
                            Utility.getSharedInstance().dismissProgressDialog();
                            Toast.makeText(getActivity(), "Removed from community", Toast.LENGTH_SHORT).show();


                        } else {

                            Utility.getSharedInstance().dismissProgressDialog();
                            Toast.makeText(getActivity(), "" + responseData.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                        }


                        communityMembersAdapter.notifyDataSetChanged();


                    } else {
                        Utility.getSharedInstance().dismissProgressDialog();
                        Toast.makeText(getActivity(), "Api issue", Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                    Utility.getSharedInstance().dismissProgressDialog();

                    Toast.makeText(getActivity(), "Api failure", Toast.LENGTH_SHORT).show();
                }


            });
        } else {

            Toast.makeText(getActivity(), "Check internet connection", Toast.LENGTH_SHORT).show();
        }
    }


    public void getRequetstData(String userID) {

        MyApplication myApplication = new MyApplication();


        if (Utility.getSharedInstance().isConnectedToInternet(getActivity())) {
            Utility.getSharedInstance().showProgressDialog(getActivity());

            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_COMM_ID, "");

            // id="1";
            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");
//            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().getAllEvents( X_API_KEY,"10","0");
            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().getCommunitySentRequest(X_API_KEY, id, "10", "0");
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());

                    if (responseData.get("status").getAsBoolean() == true) {
                        // Config.getPlanDAO = new GetPlanDAO();
                        Gson gson = new Gson();

                        Utility.getSharedInstance().dismissProgressDialog();
                        ;


                        GetRequestSentbyCommunity getEventPeopleResponse = gson.fromJson(responseData, GetRequestSentbyCommunity.class);

                        List<GetRequestSentbyCommunityData> getEventPeopleResponseDataList = getEventPeopleResponse.getResultObject();

                        List<GetRequestSentbyCommunityData> soretedListData = new ArrayList<>();

                        if (getEventPeopleResponseDataList != null) {
                            for (int i = 0; i < getEventPeopleResponseDataList.size(); i++) {

                                GetRequestSentbyCommunityData getRequestSentbyCommunityData = new GetRequestSentbyCommunityData();

                                if (getEventPeopleResponseDataList.get(i).getAction().equalsIgnoreCase("1") || getEventPeopleResponseDataList.get(i).getAction().equalsIgnoreCase("3")) {

                                    getRequestSentbyCommunityData.setAction(getEventPeopleResponseDataList.get(i).getAction());
                                    getRequestSentbyCommunityData.setRequest(getEventPeopleResponseDataList.get(i).getRequest());
                                    getRequestSentbyCommunityData.setCommunityId(getEventPeopleResponseDataList.get(i).getCommunityId());
                                    getRequestSentbyCommunityData.setCommunityName(getEventPeopleResponseDataList.get(i).getCommunityName());
                                    getRequestSentbyCommunityData.setCreatedAt(getEventPeopleResponseDataList.get(i).getCreatedAt());
                                    getRequestSentbyCommunityData.setId(getEventPeopleResponseDataList.get(i).getId());

                                    getRequestSentbyCommunityData.setUserId(getEventPeopleResponseDataList.get(i).getUserId());
                                    getRequestSentbyCommunityData.setUsername(getEventPeopleResponseDataList.get(i).getUsername());
                                    getRequestSentbyCommunityData.setStatus(getEventPeopleResponseDataList.get(i).getStatus());

                                    soretedListData.add(getRequestSentbyCommunityData);
                                }
                            }
                        }


                        if (soretedListData != null) {
                            if (soretedListData.size() > 0) {
                                mListMembers.setVisibility(View.VISIBLE);
                                mTxtWarn.setVisibility(View.GONE);
//                                CommunityRequestAdapter communityRequestAdapter = new CommunityRequestAdapter(getActivity(), soretedListData,FragmentCommunityDetailsMem.this);
//                                mListMembers.setAdapter(communityRequestAdapter);
                            } else {
                                mTxtWarn.setText("No request found");
                                mTxtWarn.setVisibility(View.VISIBLE);
                                mListMembers.setVisibility(View.GONE);
                            }
                        } else {

                            mTxtWarn.setText("No request found");
                            mTxtWarn.setVisibility(View.VISIBLE);
                            mListMembers.setVisibility(View.GONE);

                        }


                        // JsonObject jsonObject=responseData.getAsJsonObject("respon");


                        // Utility.getSharedInstance().Snackbar(toolbarLayout, "Password Changed Sucessfully");


                    } else {

                        Toast.makeText(getActivity(), "No request found", Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                    Utility.getSharedInstance().dismissProgressDialog();
                    Toast.makeText(getActivity(), "API Issue", Toast.LENGTH_SHORT).show();


                }


            });
        } else {

            Toast.makeText(getActivity(), "Check Internet Connection", Toast.LENGTH_SHORT).show();

        }
    }


    private void firstColor() {
        mTxtRequest.setTextColor(getResources().getColor(R.color.white));
        mTxtMember.setTextColor(getResources().getColor(R.color.black));

        mTxtPost.setTextColor(getResources().getColor(R.color.white));
        mLayPost.setBackgroundColor(getResources().getColor(R.color.darker_grey));
        mLayMembers.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        mLayRequest.setBackgroundColor(getResources().getColor(R.color.darker_grey));
    }


    public void acceptOrRejectRequest(String comId, String actionID, String ID) {


//        Utility.getSharedInstance().showProgressDialog(this);
        MyApplication myApplication = new MyApplication();


        if (Utility.getSharedInstance().isConnectedToInternet(getActivity())) {
            Utility.getSharedInstance().showProgressDialog(getActivity());

            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");

            // id="1";
            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");

            RequestToAcceptCommunity requestGetMyPlayList = new RequestToAcceptCommunity();

            requestGetMyPlayList.setAction(actionID);
            requestGetMyPlayList.setCommunityId(comId);
            requestGetMyPlayList.setUserId(ID);

            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().acceptOrRejectRequest(X_API_KEY, requestGetMyPlayList);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());

                    if (responseData.get("status").getAsBoolean() == true) {

                        Utility.getSharedInstance().dismissProgressDialog();

//                        getAllMembers();
                        Toast.makeText(getActivity(), "" + responseData.get("message").getAsString(), Toast.LENGTH_SHORT).show();

                    } else {
                        Utility.getSharedInstance().dismissProgressDialog();

                        Toast.makeText(getActivity(), "" + responseData.get("message").getAsString(), Toast.LENGTH_SHORT).show();

                    }


                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                    Utility.getSharedInstance().dismissProgressDialog();

                    Toast.makeText(getActivity(), "Api failure", Toast.LENGTH_SHORT).show();
                }


            });
        } else {

            Toast.makeText(getActivity(), "No internet connection available", Toast.LENGTH_SHORT).show();
        }
    }


    /// Get Coomunity POST


    public void getCommunityPost(final int currentPage) {
        swipeBoleaan = true;
        MyApplication myApplication = new MyApplication();
//        Utility.getSharedInstance().showProgressDialog(getActivity());

        if (Utility.getSharedInstance().isConnectedToInternet(getContext())) {
//            Utility.getSharedInstance().showProgressDialog(getActivity());
            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_COMM_ID, "");
            // id="1";
            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");

            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().getCommunityPost(X_API_KEY, id, "10", String.valueOf(currentPage));
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    // JsonObject responseData = response.body();
                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());

                    if (responseData != null) {
                        mTxtWarn.setText("No posts found");
                        if (responseData.get("status").getAsBoolean()) {
                            // Config.getPlanDAO = new GetPlanDAO();
                            Gson gson = new Gson();
//                            Utility.getSharedInstance().dismissProgressDialog();
                            Type listType = new TypeToken<List<SongDetail>>() {
                            }.getType();

                            if (responseData.get("resultObject") != null) {
                                JsonArray jsonArray = responseData.get("resultObject").getAsJsonArray();
                                if (jsonArray.size() > 0) {
                                    layoutComments.setVisibility(View.GONE);
                                    mTxtRequest.setTextColor(getResources().getColor(R.color.white));
                                    mTxtMember.setTextColor(getResources().getColor(R.color.black));

                                    mTxtPost.setTextColor(getResources().getColor(R.color.white));
                                    mLayPost.setBackgroundColor(getResources().getColor(R.color.darker_grey));
                                    mLayMembers.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                                    mLayRequest.setBackgroundColor(getResources().getColor(R.color.darker_grey));
//                                    Utility.getSharedInstance().dismissProgressDialog();
                                    mTxtWarn.setVisibility(View.GONE);
//                                    mListSeacrList.setVisibility(View.GONE);
                                    mListMembers.setVisibility(View.VISIBLE);
                                    if (currentPage == 0) {
                                        mSongss = gson.fromJson(responseData.get("resultObject"), listType);
                                        swipyRefreshLayoutDirection.setRefreshing(false);
                                        getMusicList(mSongss);
                                    } else {
                                        ArrayList<SongDetail> datumList = gson.fromJson(responseData.get("resultObject"), listType);
                                        mSongss.addAll(mSongss.size(), datumList);
                                        swipyRefreshLayoutDirection.setRefreshing(false);
                                        getUpdatedList();
                                    }
                                    if (mSongss != null) {
                                        if (mSongss.size() < 0) {
                                            swipyRefreshLayoutDirection.setRefreshing(false);
                                            mTxtWarn.setVisibility(View.VISIBLE);
                                            mListMembers.setVisibility(View.GONE);
                                        }
                                    }
                                } else {
                                    swipyRefreshLayoutDirection.setRefreshing(false);
                                    if (mSongss != null && mSongss.size() > 0) {
//                                        Utility.getSharedInstance().dismissProgressDialog();
                                        mTxtWarn.setVisibility(View.GONE);
                                        mListMembers.setVisibility(View.VISIBLE);
                                        if (mSongss.size() < 0) {
                                            mTxtWarn.setVisibility(View.VISIBLE);
                                            mListMembers.setVisibility(View.GONE);
                                        }
                                        Toast.makeText(getActivity(), "No more posts", Toast.LENGTH_SHORT).show();
                                    } else {
//                                        Utility.getSharedInstance().dismissProgressDialog();
                                        mTxtWarn.setVisibility(View.VISIBLE);
                                        mListMembers.setVisibility(View.GONE);
                                    }
                                }
                            }
                            // Utility.getSharedInstance().Snackbar(toolbarLayout, "Password Changed Sucessfully");
                        } else {
                            swipyRefreshLayoutDirection.setRefreshing(false);
//                            Utility.getSharedInstance().dismissProgressDialog();
                            mTxtWarn.setVisibility(View.VISIBLE);
                            mListMembers.setVisibility(View.GONE);
                        }
                    } else {
                        swipyRefreshLayoutDirection.setRefreshing(false);
//                        Utility.getSharedInstance().dismissProgressDialog();
                        Toast.makeText(getActivity(), "No posts found", Toast.LENGTH_SHORT).show();
                    }
                }


                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
//                    Utility.getSharedInstance().dismissProgressDialog();
                    swipyRefreshLayoutDirection.setRefreshing(false);
                    /*mTxtWarn.setVisibility(View.VISIBLE);
                    mMySongsListView.setVisibility(View.GONE);*/
                    Toast.makeText(DashBoardActivity.context, "Server error .. Please try again after some time", Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            swipyRefreshLayoutDirection.setRefreshing(false);
            Toast.makeText(DashBoardActivity.context, "No internet connection available", Toast.LENGTH_SHORT).show();
        }
    }


    private void getMusicList(List<SongDetail> songDetailList) {
        communityPostAdapter = new CommunityPostAdapter(getActivity(), songDetailList, FragmentCommunityDetails.this);
        mListMembers.setAdapter(communityPostAdapter);
        mListMembers.setVisibility(View.VISIBLE);
        if (communityPostAdapter != null)
            communityPostAdapter.notifyDataSetChanged();
        currentPage = currentPage + 10;
    }

    private void getUpdatedList() {
        mListMembers.setVisibility(View.VISIBLE);
        if (communityPostAdapter != null)
            communityPostAdapter.notifyDataSetChanged();
        currentPage = currentPage + 10;
    }

    @Override
    public void onResume() {

//


        ;


        super.onResume();
    }

    public void playPoase(int postion) {
        PostionOfPlay = postion;
        try {
            if (mSongss != null)
                for (int i = 0; i < mSongss.size(); i++) {
                    if (i != postion)
                        mSongss.get(i).setPlayStop(false);

                }
            if (mSongss.get(postion).getPlayStop()) {
                mSongss.get(postion).setPlayStop(false);
            } else {
                mSongss.get(postion).setPlayStop(true);
            }

            communityPostAdapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void postCommunity(String commid) {

        MyApplication myApplication = new MyApplication();


        if (Utility.getSharedInstance().isConnectedToInternet(getActivity())) {


            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");

            // id="1";
            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");


            RequestShareCommunityPost sendPostModel = new RequestShareCommunityPost();
            sendPostModel.setCommunityId(commid);
            sendPostModel.setComment(mEditPostComment.getText().toString().trim());
            sendPostModel.setUserId(id);
            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().postComments(X_API_KEY, sendPostModel);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());
                    if (responseData != null) {
                        if (responseData.get("status").getAsBoolean() == true) {

                            mEditPostComment.setText("");
                            getAllComments(0);


                        } else {

                            Toast.makeText(getActivity(), "" + responseData.get("message").getAsString(), Toast.LENGTH_SHORT).show();


                        }

                    }


                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {


                }


            });
        } else {

            Toast.makeText(getActivity(), "No internet connection", Toast.LENGTH_SHORT).show();
        }
    }


    ///


    public void getAllComments(final int currentPage) {
        swipeBoleaan = false;
        MyApplication myApplication = new MyApplication();
//        Utility.getSharedInstance().showProgressDialog(getActivity());


        String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_COMM_ID, "");
        // id="1";
        // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");

        Call<JsonObject> callbackLogin = myApplication.getAPIInstance().getallComments(X_API_KEY, id, "10000000000000", String.valueOf(0));
        //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
        callbackLogin.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                // JsonObject responseData = response.body();
                JsonObject responseData = response.body();
                // Log.e("Response::", responseData.toString());

                if (responseData != null) {


                    Utility.getSharedInstance().dismissProgressDialog();
                    if (responseData.get("status").getAsBoolean() == true) {
                        // Config.getPlanDAO = new GetPlanDAO();
                        Gson gson = new Gson();
//                            Utility.getSharedInstance().dismissProgressDialog();
                        Type listType = new TypeToken<List<SongDetail>>() {
                        }.getType();

                        if (responseData.get("resultObject") != null) {
                            JsonArray jsonArray = responseData.get("resultObject").getAsJsonArray();
                            GetCommentsModel getCommentsModel = gson.fromJson(responseData, GetCommentsModel.class);
                            ArrayList<GetCommentsModelData> getCommentsModelDataArrayList = getCommentsModel.getResultObject();
                            getCommenstList(getCommentsModelDataArrayList);


                        } else {
                            mListSeacrList.setVisibility(View.GONE);
                            mListMembers.setVisibility(View.GONE);
                            mTxtWarn.setVisibility(View.VISIBLE);
                        }
                        // Utility.getSharedInstance().Snackbar(toolbarLayout, "Password Changed Sucessfully");
                    } else {

                    }
                } else {

                }
            }


            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });


    }


    private void getCommenstList(ArrayList<GetCommentsModelData> getCommentsModelDataArrayList)

    {
        layoutComments.setVisibility(View.VISIBLE);
        swipyRefreshLayoutDirection.setRefreshing(false);
//        mTxtRequest.setTextColor(getResources().getColor(R.color.black));
//        mTxtMember.setTextColor(getResources().getColor(R.color.white));
//        mTxtPost.setTextColor(getResources().getColor(R.color.white));
//
//        mLayMembers.setBackgroundColor(getResources().getColor(R.color.darker_grey));
//        mLayRequest.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
//        mLayPost.setBackgroundColor(getResources().getColor(R.color.darker_grey));

        if (getCommentsModelDataArrayList.size() > 0) {
            RecyclerView.LayoutManager linearLayoutManagers = new LinearLayoutManager(getActivity());
            mListSeacrList.setLayoutManager(linearLayoutManagers);
            mListSeacrList.setItemAnimator(new DefaultItemAnimator());
            mListMembers.setVisibility(View.VISIBLE);
            mListSeacrList.setVisibility(View.GONE);
            mTxtWarn.setVisibility(View.GONE);
            CommentsAdapter commentsAdapter = new CommentsAdapter(getActivity(), getCommentsModelDataArrayList);
            mListMembers.setAdapter(commentsAdapter);

            if (getCommentsModelDataArrayList != null) {
                if (getCommentsModelDataArrayList.size() > 0) {
                    mListMembers.scrollToPosition(getCommentsModelDataArrayList.size() - 1);
                    commentsAdapter.notifyDataSetChanged();
                }
            }

//            mListMembers.addOnItemTouchListener(
//                    new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
//                        @Override
//                        public void onItemClick(View view, final int position) {
//                            // TODO Handle item click
//
////
//                        }
//                    })
//            );
        } else {
            mListSeacrList.setVisibility(View.GONE);
            mListMembers.setVisibility(View.GONE);
            mTxtWarn.setVisibility(View.VISIBLE);
        }


    }



}