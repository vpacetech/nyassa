package com.music.nyasaa.fragments.discourses;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.music.nyasaa.R;
import com.music.nyasaa.adapter.DiscouserSongsAdapter;
import com.music.nyasaa.controller.MyApplication;
import com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity;
import com.music.nyasaa.models.RequestModel.SearchMedia.RequestSearchMedia;
import com.music.nyasaa.models.SongDetail;
import com.music.nyasaa.utility.SharedPreferenceManager;
import com.music.nyasaa.utility.Utility;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity.PostionOfPlay;
import static com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity.mSongssSeries;
import static com.music.nyasaa.retrofit.Config.X_API_KEY;

public class FragmentAudioDiscourses extends Fragment {

    private View myView;
    private static final String ARG_KEY_NUMBER = "tab_number";
    private static Context context;
    public static FragmentAudioDiscourses fragmentAudioDiscourses;
    private ImageView imgDisc;
    private RecyclerView mMySongsListView;
    private TextView mtxtDisCousName, mTxtWarn,txtDescription;
    public static ArrayList<SongDetail> mSongss = new ArrayList<>();
    int currentPage = 0;
    private DiscouserSongsAdapter shortSongsAdapter;
    private TextView mTxtLang;
    private ImageView imgOpenDrawer;
    private TextView DownloadAll;


    public static FragmentAudioDiscourses newInstance(int number) {
        Bundle args = new Bundle();
        args.putInt(ARG_KEY_NUMBER, number);
        FragmentAudioDiscourses frag = new FragmentAudioDiscourses();
        frag.setArguments(args);
        return frag;
    }


    public FragmentAudioDiscourses() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        myView = inflater.inflate(R.layout.fragment_discoures, container, false);
        fragmentAudioDiscourses = FragmentAudioDiscourses.this;
        initView(myView);
        return myView;
    }



    private void initView(View myView) {

//        myView.getRootView().setBackgroundColor(Color.BLACK);
        myView.getRootView().setBackgroundColor(getResources().getColor(R.color.orBLACK));
        mTxtLang = (TextView) myView.findViewById(R.id.txtLang);
        DownloadAll=(TextView)myView.findViewById(R.id.DownloadAll);
        txtDescription=(TextView)myView.findViewById(R.id.txtDescription);

        mMySongsListView = (RecyclerView) myView.findViewById(R.id.FragmentAudioCommon);
        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        mMySongsListView.setLayoutManager(linearLayoutManager);
        mMySongsListView.setItemAnimator(new DefaultItemAnimator());

        DownloadAll=(Button)myView.findViewById(R.id.DownloadAll);

        txtDescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DownloadAllADO();
            }
        });

        DownloadAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DownloadAllADO();
            }
        });

        imgDisc = (ImageView) myView.findViewById(R.id.disImg);
        mtxtDisCousName = (TextView) myView.findViewById(R.id.txtDiscousename);
        mTxtWarn = (TextView) myView.findViewById(R.id.txtWarn);
        thread();


        imgOpenDrawer =(ImageView)myView.findViewById(R.id.imgOpenDrawer);



        imgOpenDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((DashBoardActivity)getActivity()).openDrawerTab();
            }
        });

//        mGridAlbumview =(RecyclerView)myView.findViewById(R.id.listViewAlbum);
    }

    private void DownloadAllADO() {
        if(mSongss!=null){
            if(mSongss.size()>0)
            {
                // callQuecFunction
                //mSongssSeries
              //  mSongssSeries
                mSongssSeries.clear();
               for(int i=0;i<mSongss.size();i++)
               {
                   mSongssSeries.add(mSongss.get(i));
               }

               // mSongssSeries=mSongss;

                if(mSongssSeries!=null)
                {
                    if(mSongssSeries.size()>0)
                    {
                        DashBoardActivity.context.downloadfromOtherFragmentSerise(mSongssSeries.get(0));

                    }
                }
               // downloadVDOSerise(getEventResponseDataResponQue.get(0));
            }
        }
    }


    /// First Time Screen


    public void firstTimeAPICALL() {


        MyApplication myApplication = new MyApplication();

        //Utility.getSharedInstance().showProgressDialog(getActivity());


        String lang = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_LANG_ID, "1");
        /*if (lang.equalsIgnoreCase("1")) {
            mTxtLang.setText("English");

        } else {
            mTxtLang.setText("Hindi");
        }*/

        RequestSearchMedia requestSearchMedia = new RequestSearchMedia();

        requestSearchMedia.setLimit("200");
        requestSearchMedia.setAlbum_id(lang);
        requestSearchMedia.setOffset("0");
        requestSearchMedia.setQuedisc(2);
        requestSearchMedia.setShould_orderby_name(false);
        // requestSearchMedia.setMedia_type_id(SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT_ORIGINAL,"none"));

        requestSearchMedia.setSearchterm(SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_DISCOURSE_AUDIO));
        String cat = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT, "0");
//        String cat= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT,"0");
//        requestSearchMedia.setCategory(cat);


        if (Utility.getSharedInstance().isConnectedToInternet(getContext())) {

            Utility.getSharedInstance().showProgressDialog(getActivity());

//            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");
//            String cat = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT, "1");
//            // id="1";
//            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");

            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().searchAudioQA(X_API_KEY, requestSearchMedia);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();


                    if (responseData != null) {

                        if (responseData.get("error").getAsInt() == 0) {
                            // Config.getPlanDAO = new GetPlanDAO();
                            Gson gson = new Gson();

                            Utility.getSharedInstance().dismissProgressDialog();


                            Type listType = new TypeToken<List<SongDetail>>() {
                            }.getType();
                            if (responseData.get("respon") != null) {

                                JsonArray jsonArray = responseData.get("respon").getAsJsonArray();

                                if (jsonArray.size() > 0) {
                                    mTxtWarn.setVisibility(View.GONE);
                                    mMySongsListView.setVisibility(View.VISIBLE);
                                    if (currentPage == 0) {
                                        mSongss = gson.fromJson(responseData.get("respon"), listType);

//                                        imgDisc.

                                        getMusicList(mSongss);

                                    } else {
                                        ArrayList<SongDetail> datumList = gson.fromJson(responseData.get("respon"), listType);
                                        mSongss.addAll(mSongss.size(), datumList);

                                        getUpdatedList();
                                    }

                                    if (mSongss != null) {
                                        if (mSongss.size() < 0) {

                                            mTxtWarn.setVisibility(View.VISIBLE);
                                            mMySongsListView.setVisibility(View.GONE);
                                        }
                                    }
                                } else {
                                    Toast.makeText(getActivity(), "No episodes found for selected language", Toast.LENGTH_SHORT).show();
                                    mTxtWarn.setVisibility(View.VISIBLE);
                                    mMySongsListView.setVisibility(View.GONE);
                                    if (mSongss != null && mSongss.size() == 0) {
                                        mTxtWarn.setVisibility(View.VISIBLE);
                                        mMySongsListView.setVisibility(View.GONE);
                                    }
                                    Utility.getSharedInstance().dismissProgressDialog();
                                }
                            }
                            // Utility.getSharedInstance().Snackbar(toolbarLayout, "Password Changed Sucessfully");
                        }


                    } else {
//                        swipyRefreshLayoutDirection.setRefreshing(false);
                        Utility.getSharedInstance().dismissProgressDialog();
                        mTxtWarn.setVisibility(View.VISIBLE);
                        mMySongsListView.setVisibility(View.GONE);

                    }

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                    Utility.getSharedInstance().dismissProgressDialog();
                    mTxtWarn.setVisibility(View.VISIBLE);
                    mMySongsListView.setVisibility(View.GONE);
//                    swipyRefreshLayoutDirection.setRefreshing(false);
                }


            });
        } else {
//            swipyRefreshLayoutDirection.setRefreshing(false);
        }
    }

    private void getMusicList(ArrayList<SongDetail> songDetailList) {

        mMySongsListView.setVisibility(View.VISIBLE);
        mTxtWarn.setVisibility(View.GONE);
        mtxtDisCousName.setText("" + songDetailList.get(0).getName());

        Picasso.with(context)
                .load(com.music.nyasaa.retrofit.Config.PIC_URL + "cover/" + Uri.encode( songDetailList.get(0).getCover()))
                .placeholder(R.drawable.default_music)
                .error(R.drawable.default_music)
                .into(imgDisc);
        shortSongsAdapter = new DiscouserSongsAdapter(getActivity(), songDetailList, FragmentAudioDiscourses.this);
        mMySongsListView.setAdapter(shortSongsAdapter);

        if (shortSongsAdapter != null)
            shortSongsAdapter.notifyDataSetChanged();
        currentPage += 10;
    }

    private void getUpdatedList() {
        if (shortSongsAdapter != null)
            shortSongsAdapter.notifyDataSetChanged();

        currentPage += 10;
    }


    public void playPoase(int postion) {
        PostionOfPlay = postion;
        try {
            if (mSongss != null)
                for (int i = 0; i < mSongss.size(); i++) {
                    if (i != postion)
                        mSongss.get(i).setPlayStop(false);

                }
            if (mSongss.get(postion).getPlayStop()) {
                mSongss.get(postion).setPlayStop(false);
            } else {
                mSongss.get(postion).setPlayStop(true);
            }

            if (shortSongsAdapter != null)
                shortSongsAdapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void thread()
    {




        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {

                firstTimeAPICALL();


            }
        });
    }


}