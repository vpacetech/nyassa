package com.music.nyasaa.fragments.playlist;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.music.nyasaa.R;
import com.music.nyasaa.adapter.MyPlayListAdapters;
import com.music.nyasaa.controller.MyApplication;
import com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity;
import com.music.nyasaa.models.RequestModel.DeletePlayList.RequestDeletePlayList;
import com.music.nyasaa.models.RequestModel.GetMyPlayList.RequestGetMyPlayList;
import com.music.nyasaa.models.ResponseModel.GetMyPlayList.GetMyPlayListResponse;
import com.music.nyasaa.models.ResponseModel.GetMyPlayList.GetMyPlayListResponseData;
import com.music.nyasaa.utility.RecyclerItemClickListener;
import com.music.nyasaa.utility.RecyclerItemTouchHelper;
import com.music.nyasaa.utility.SharedPreferenceManager;
import com.music.nyasaa.utility.Utility;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity.PostionOfPlay;
import static com.music.nyasaa.retrofit.Config.X_API_KEY;
import static com.music.nyasaa.retrofit.Config.clientService;

public class FragmentPlayList extends Fragment implements RecyclerItemTouchHelper.RecyclerItemTouchHelperListener {

    private View myView;
    private static final String ARG_KEY_NUMBER = "tab_number";

    private static Context context;

    private RecyclerView mMyPlayList;
    public static FragmentPlayList fragmentPlayList;
    private TextView mTxtWarn;
    private MyApplication myApplication = new MyApplication();
    private MyPlayListAdapters myPlayListAdapter;
    private List<GetMyPlayListResponseData> getMyPlayListResponseData;
    private Dialog confrimDeleteDialog;

    private SwipyRefreshLayout swipyRefreshLayoutDirection;



    public static FragmentPlayList newInstance(int number) {
        Bundle args = new Bundle();
        args.putInt(ARG_KEY_NUMBER, number);
        FragmentPlayList frag = new FragmentPlayList();
        frag.setArguments(args);
        return frag;
    }


    public FragmentPlayList() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        myView = inflater.inflate(R.layout.fragment_playlist, container, false);
        initView(myView);
        fragmentPlayList=this;
        return myView;
    }


    private void initView(View myView) {

        mTxtWarn = (TextView) myView.findViewById(R.id.txtWarn);
        swipyRefreshLayoutDirection = (SwipyRefreshLayout) myView.findViewById(R.id.swipe_refresh_layout);
        swipyRefreshLayoutDirection.setDirection(SwipyRefreshLayoutDirection.BOTH);

        mTxtWarn.setVisibility(View.GONE);
//        myView.getRootView().setBackgroundColor(Color.BLACK);
        myView.getRootView().setBackgroundColor(getResources().getColor(R.color.md_black_1000));
//        myView.getRootView().setBackgroundResource(R.drawable.black_gradient);
        mMyPlayList = (RecyclerView) myView.findViewById(R.id.listViewAlbum);
        //RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        mMyPlayList.setLayoutManager(linearLayoutManager);
        mMyPlayList.setItemAnimator(new DefaultItemAnimator());
        // mMyPlayList.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));


        getAllMyList();

        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(mMyPlayList);


        swipyRefreshLayoutDirection.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                getAllMyList();
            }
        });


        mMyPlayList.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, final int position) {


                        ((DashBoardActivity) getActivity()).showPlayListSongs(getMyPlayListResponseData.get(position).getName());
                        SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_PLAYLIST_ID,getMyPlayListResponseData.get(position).getId());

                    }
                }));


    }


    public void getAllMyList() {

        Utility.getSharedInstance().showProgressDialog(getActivity());


        if (Utility.getSharedInstance().isConnectedToInternet(getContext())) {

            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");

            // id="1";
            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");

            RequestGetMyPlayList requestGetMyPlayList = new RequestGetMyPlayList();

            requestGetMyPlayList.setUserId(id);


            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().getMyPlayList(clientService,X_API_KEY,requestGetMyPlayList);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());


                        // Config.getPlanDAO = new GetPlanDAO();
                        Gson gson = new Gson();

                        Utility.getSharedInstance().dismissProgressDialog();


                        if (responseData.get("respon") != null) {

                            JsonArray jsonArray = responseData.get("respon").getAsJsonArray();


                            if (jsonArray.size() > 0) {

                                mTxtWarn.setVisibility(View.GONE);
                                mMyPlayList.setVisibility(View.VISIBLE);

                                swipyRefreshLayoutDirection.setRefreshing(false);


                                gson = new Gson();
                                GetMyPlayListResponse getMyPlayListResponse = gson.fromJson(responseData, GetMyPlayListResponse.class);
                                getMyPlayListResponseData = getMyPlayListResponse.getRespon();
                                myPlayListAdapter = new MyPlayListAdapters(getActivity(), getMyPlayListResponseData);
                                mMyPlayList.setAdapter(myPlayListAdapter);
                            } else {

                                swipyRefreshLayoutDirection.setRefreshing(false);
                                mTxtWarn.setVisibility(View.VISIBLE);
                                mMyPlayList.setVisibility(View.GONE);
                                Utility.getSharedInstance().dismissProgressDialog();

                            }


                            // JsonObject jsonObject=responseData.getAsJsonObject("respon");


                        // Utility.getSharedInstance().Snackbar(toolbarLayout, "Password Changed Sucessfully");


                    } else {
                        swipyRefreshLayoutDirection.setRefreshing(false);
                        Utility.getSharedInstance().dismissProgressDialog();
                        mTxtWarn.setVisibility(View.VISIBLE);
                        mMyPlayList.setVisibility(View.GONE);

                    }

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                    Utility.getSharedInstance().dismissProgressDialog();
                    mTxtWarn.setVisibility(View.VISIBLE);
                    mMyPlayList.setVisibility(View.GONE);

                }


            });
        } else {

        }
    }

    @Override
    public void onSwiped(final RecyclerView.ViewHolder viewHolder, final int direction, int position) {

        if (viewHolder instanceof MyPlayListAdapters.HeaderHolder) {
            // get the removed item name to display it in snack bar
            final String name = getMyPlayListResponseData.get(viewHolder.getAdapterPosition()).getName();

            // backup of removed item for undo purpose
            final GetMyPlayListResponseData deletedItem = getMyPlayListResponseData.get(viewHolder.getAdapterPosition());
            final int deletedIndex = viewHolder.getAdapterPosition();

            // remove the item from recycler view

            confrimDeleteDialog = new Dialog(getActivity());
            confrimDeleteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            confrimDeleteDialog.setContentView(R.layout.dialog_confrim_delete);
            confrimDeleteDialog.getWindow().getAttributes().width = ActionBar.LayoutParams.FILL_PARENT;
            confrimDeleteDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
            confrimDeleteDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            confrimDeleteDialog.show();
            confrimDeleteDialog.setCancelable(false);

            Button mButYes = (Button) confrimDeleteDialog.findViewById(R.id.butYes);
            Button mButNo = (Button) confrimDeleteDialog.findViewById(R.id.butNo);


            mButNo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    confrimDeleteDialog.dismiss();

                    myPlayListAdapter.notifyDataSetChanged();


                }
            });

            mButYes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    // showing snack bar with Undo option

                    deletePlayList(getMyPlayListResponseData.get(viewHolder.getAdapterPosition()).getId());

                    myPlayListAdapter.removeItem(viewHolder.getAdapterPosition());
//            snackbar.setAction("UNDO", new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    // undo is selected, restore the deleted item
//
//                }
//            });
//            snackbar.setActionTextColor(Color.YELLOW);
                    //snackbar.show();

                }
            });


        }

    }

    public void deletePlayList(String plyID) {

        Utility.getSharedInstance().showProgressDialog(getActivity());


        if (Utility.getSharedInstance().isConnectedToInternet(getContext())) {

            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");

            // id="1";
            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");

            RequestDeletePlayList requestGetMyPlayList = new RequestDeletePlayList();

            requestGetMyPlayList.setPlaylist_id(plyID);


            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().deletePlayList(clientService,X_API_KEY,requestGetMyPlayList);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());

                    if (responseData != null) {


                        confrimDeleteDialog.dismiss();
                        Utility.getSharedInstance().dismissProgressDialog();
                        Toast.makeText(getActivity(), "Removed from playlist", Toast.LENGTH_SHORT).show();

                        myPlayListAdapter.notifyDataSetChanged();


                    } else {
                        Utility.getSharedInstance().dismissProgressDialog();
                        Toast.makeText(getActivity(), "Api issue", Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                    Utility.getSharedInstance().dismissProgressDialog();

                    Toast.makeText(getActivity(), "Api failure", Toast.LENGTH_SHORT).show();
                }


            });
        } else {

        }
    }


    public void playPoase(int postion) {
        PostionOfPlay = postion;
        try {
            if (getMyPlayListResponseData != null)
                for (int i = 0; i < getMyPlayListResponseData.size(); i++) {
                    if (i != postion)
                        getMyPlayListResponseData.get(i).setPlayStop(false);

                }
            if (getMyPlayListResponseData.get(postion).getPlayStop()) {
                getMyPlayListResponseData.get(postion).setPlayStop(false);
            } else {
                getMyPlayListResponseData.get(postion).setPlayStop(true);
            }

            myPlayListAdapter.notifyDataSetChanged();
        } catch (Exception e){
            e.printStackTrace();
        }

    }
}