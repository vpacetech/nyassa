package com.music.nyasaa.fragments.meditation_center;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.music.nyasaa.R;
import com.music.nyasaa.utility.Utility;

public class FragmentMediatitationTch  extends Fragment {

    private View myView;
    private static final String ARG_KEY_NUMBER = "tab_number";

    private static Context context;


    public static FragmentMediatitationTch newInstance(int number) {
        Bundle args = new Bundle();
        args.putInt(ARG_KEY_NUMBER, number);
        FragmentMediatitationTch frag = new FragmentMediatitationTch();
        frag.setArguments(args);
        return frag;
    }


    public FragmentMediatitationTch() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        myView = inflater.inflate(R.layout.fragment_md_techni, container, false);
        initView(myView);
        return myView;
    }


    private void initView(View myView) {

//        myView.getRootView().setBackgroundColor(Color.BLACK);
        myView.getRootView().setBackgroundColor(getResources().getColor(R.color.orBLACK));

       String URL=  "file:///android_asset/meditation.html";
        WebView mWebview = (WebView) myView.findViewById(R.id.webView);
        mWebview.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);

        mWebview.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view, String url) {
                Utility.getSharedInstance().dismissProgressDialog();
            }
        });
        //String url = "http://docs.google.com/gview?embedded=true&url=" + URL;
        mWebview.loadUrl(URL);
        Utility.getSharedInstance().showProgressDialog(getActivity());

//        mGridAlbumview =(RecyclerView)myView.findViewById(R.id.listViewAlbum);
    }

}