package com.music.nyasaa.fragments.community;

import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.music.nyasaa.R;
import com.music.nyasaa.adapter.CommunityAdapter;
import com.music.nyasaa.controller.MyApplication;
import com.music.nyasaa.models.Community_DAO;
import com.music.nyasaa.models.ResponseModel.getCommunity.GetCommunityResponse;
import com.music.nyasaa.models.ResponseModel.getCommunity.GetCommunityResponseData;
import com.music.nyasaa.utility.SharedPreferenceManager;
import com.music.nyasaa.utility.Utility;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.music.nyasaa.retrofit.Config.X_API_KEY;

public class FragmentCommunity extends Fragment {

    private View myView;
    private static final String ARG_KEY_NUMBER = "tab_number";
    private RecyclerView mGridAlbumview;
    private static Context context;
    List<Community_DAO> community_dao;
    MyApplication myApplication;
    CommunityAdapter connectionAdapter;
    private Message messageobj;
    private SwipyRefreshLayout swipyRefreshLayoutDirection;
    private String userID;

    private TextView mTxtWarn;




    public static FragmentCommunity newInstance(int number) {
        Bundle args = new Bundle();
        args.putInt(ARG_KEY_NUMBER, number);
        FragmentCommunity frag = new FragmentCommunity();
        frag.setArguments(args);
        return frag;
    }

    public static FragmentCommunity newInstance(Context mContext) {
        FragmentCommunity f = new FragmentCommunity();
        context = mContext;
        return f;
    }

    public FragmentCommunity() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        myView = inflater.inflate(R.layout.fragment_connection, container, false);


        myApplication = new MyApplication();
        initView(myView);
        return myView;

    }


    private void initView(View myView) {

        userID = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "none");
//        myView.getRootView().setBackgroundColor(getResources().getColor(R.color.black));
//        myView.getRootView().setBackgroundResource(R.drawable.black_gradient);
        myView.getRootView().setBackgroundColor(getResources().getColor(R.color.orBLACK));

        mTxtWarn = (TextView) myView.findViewById(R.id.txtWarn);
        mTxtWarn.setVisibility(View.GONE);

        swipyRefreshLayoutDirection = (SwipyRefreshLayout) myView.findViewById(R.id.swipe_refresh_layout);
        swipyRefreshLayoutDirection.setDirection(SwipyRefreshLayoutDirection.BOTH);
        mGridAlbumview = (RecyclerView) myView.findViewById(R.id.listViewAlbum);
        //RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        mGridAlbumview.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        mGridAlbumview.setItemAnimator(new DefaultItemAnimator());

        community_dao = new ArrayList<>();


        mGridAlbumview.invalidate();

//        getAllCoummity();

        swipyRefreshLayoutDirection.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                getAllCommunity();
            }
        });


    }

    public void getAllCommunity() {

        Utility.getSharedInstance().showProgressDialog(getActivity());


        if (Utility.getSharedInstance().isConnectedToInternet(getContext())) {

            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");

            // id="1";
            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");

            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().allcombyusrid(id, X_API_KEY);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());


                    // Config.getPlanDAO = new GetPlanDAO();
                    Gson gson = new Gson();

                    if(responseData!=null) {


                        Type listType = new TypeToken<List<Community_DAO>>() {
                        }.getType();
                        if (responseData.get("status").getAsString().equalsIgnoreCase("true")) {


                            Utility.getSharedInstance().dismissProgressDialog();


                            // JsonObject jsonObject=responseData.getAsJsonObject("respon");


                            GetCommunityResponse getCommunityResponse = gson.fromJson(responseData, GetCommunityResponse.class);

                            List<GetCommunityResponseData> getCommunityResponseData = getCommunityResponse.getResultObject();

                            List<GetCommunityResponseData> sortedCommunityList = new ArrayList<>();


                            for (int i = 0; i < getCommunityResponseData.size(); i++)


                            {
                                if (getCommunityResponseData.get(i).getAction().equalsIgnoreCase("1") ||getCommunityResponseData.get(i).getAction().equalsIgnoreCase("2") || userID.equalsIgnoreCase(String.valueOf(getCommunityResponseData.get(i).getParentUserId()))) {
                                    GetCommunityResponseData getCommunityResponseData1 = new GetCommunityResponseData();
                                    getCommunityResponseData1.setAction(getCommunityResponseData.get(i).getAction());
                                    getCommunityResponseData1.setCommunityName(getCommunityResponseData.get(i).getCommunityName());
                                    getCommunityResponseData1.setCreatedAt(getCommunityResponseData.get(i).getCreatedAt());
                                    getCommunityResponseData1.setId(getCommunityResponseData.get(i).getId());
                                    getCommunityResponseData1.setImageUrl(getCommunityResponseData.get(i).getImageUrl());
                                    getCommunityResponseData1.setParentUserId(getCommunityResponseData.get(i).getParentUserId());
                                    getCommunityResponseData1.setLongInfo(getCommunityResponseData.get(i).getLongInfo());
                                    getCommunityResponseData1.setShortInfo(getCommunityResponseData.get(i).getShortInfo());
                                    getCommunityResponseData1.setUsername(getCommunityResponseData.get(i).getUsername());
                                    getCommunityResponseData1.setUserId(getCommunityResponseData.get(i).getUserId());
                                    sortedCommunityList.add(getCommunityResponseData1);
                                }


                            }

                            if (sortedCommunityList.size() > 0) {
                                connectionAdapter = new CommunityAdapter(getActivity(), sortedCommunityList);
                                mGridAlbumview.setAdapter(connectionAdapter);
                                mTxtWarn.setVisibility(View.GONE);
                                mGridAlbumview.setVisibility(View.VISIBLE);
                            } else {
                                mTxtWarn.setVisibility(View.VISIBLE);
                                mGridAlbumview.setVisibility(View.GONE);
                            }


                            swipyRefreshLayoutDirection.setRefreshing(false);

                        } else {
                            swipyRefreshLayoutDirection.setRefreshing(false);
                            mTxtWarn.setVisibility(View.VISIBLE);
                            mGridAlbumview.setVisibility(View.GONE);
                            Utility.getSharedInstance().dismissProgressDialog();
                        }
                    }
                    else {
                        swipyRefreshLayoutDirection.setRefreshing(false);
                        mTxtWarn.setVisibility(View.VISIBLE);
                        mGridAlbumview.setVisibility(View.GONE);
                        Utility.getSharedInstance().dismissProgressDialog();
                    }

                    // Utility.getSharedInstance().Snackbar(toolbarLayout, "Password Changed Sucessfully");


                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                    Utility.getSharedInstance().dismissProgressDialog();
                    mTxtWarn.setVisibility(View.VISIBLE);
                    mGridAlbumview.setVisibility(View.GONE);

                }


            });
        } else {

            mTxtWarn.setText("Opps no internet connection");

            Utility.getSharedInstance().dismissProgressDialog();
            mTxtWarn.setVisibility(View.VISIBLE);
            mGridAlbumview.setVisibility(View.GONE);

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getAllCommunity();
    }
}

