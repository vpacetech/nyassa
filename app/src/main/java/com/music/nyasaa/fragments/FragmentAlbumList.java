package com.music.nyasaa.fragments;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.music.nyasaa.R;
import com.music.nyasaa.adapter.GetAlbumListAdapter;
import com.music.nyasaa.controller.MyApplication;

import com.music.nyasaa.fragments.mysongs.FragmentMySongs;
import com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity;
import com.music.nyasaa.models.RequestModel.SearchMedia.RequestSearchMedia;
import com.music.nyasaa.models.ResponseModel.GetAlbumList;
import com.music.nyasaa.models.ResponseModel.GetAlbumsListData;
import com.music.nyasaa.models.SongDetail;
import com.music.nyasaa.models.User_DAO;
import com.music.nyasaa.utility.SharedPreferenceManager;
import com.music.nyasaa.utility.Utility;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity.mSongssSeries;
import static com.music.nyasaa.retrofit.Config.X_API_KEY;
import static com.music.nyasaa.retrofit.Config.clientService;

public class FragmentAlbumList extends Fragment {
    FragmentManager fragmentManager;
    private View myView;
    private static final String ARG_KEY_NUMBER = "tab_number";
    public static ArrayList<SongDetail> mSongss = new ArrayList<>();
    private static Context context;

    private TextView mTxtTerms, mTxtMain;

    private static final String BACK_STACK_ROOT_TAG = "root_fragment";
    private RecyclerView mListAlbum;


    public static FragmentAlbumList newInstance(int number) {
        Bundle args = new Bundle();
        args.putInt(ARG_KEY_NUMBER, number);
        FragmentAlbumList frag = new FragmentAlbumList();
        frag.setArguments(args);
        return frag;
    }


    public FragmentAlbumList() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        myView = inflater.inflate(R.layout.album_fragment, container, false);
        initView(myView);
        getAlbums();
        return myView;
    }


    private void initView(View myView) {


        mListAlbum = (RecyclerView) myView.findViewById(R.id.albumList);

        mTxtTerms = myView.findViewById(R.id.txtData);
//        mTxtMain = myView.findViewById(R.id.txtMain);


//        mTxtMain.setText("Albums");

        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        mListAlbum.setLayoutManager(linearLayoutManager);
        mListAlbum.setItemAnimator(new DefaultItemAnimator());

    }


    /// get all Images


    @RequiresApi(api = Build.VERSION_CODES.O)
    public void callMysongs() {


        SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_DRAWER_TYPE, "1");
        //  dashBoardActivity.openshorAudio();
        ((DashBoardActivity) getActivity()).hideBadge();
        SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_TXT_MORE, "more");

//                ((DashBoardActivity) getActivity()).toolbarStatusBar(" Audio");
        ((DashBoardActivity) getActivity()).toolbarStatusBar(" AudioNew");

        fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.popBackStack("AudioNew", FragmentManager.POP_BACK_STACK_INCLUSIVE);
//        fragmentManager.popBackStack(BACK_STACK_ROOT_TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);

        // Add the new tab fragment
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .replace(R.id.fragment, FragmentMySongs.newInstance(0), String.valueOf(0))
//                .replace(R.id.fragment, FragmentAlbumList.newInstance(0), String.valueOf(0))
                .addToBackStack(BACK_STACK_ROOT_TAG)
                // .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
        getActivity().overridePendingTransition(R.anim.left, R.anim.right);
    }

    public void getAlbums() {

        MyApplication myApplication = new MyApplication();

        User_DAO user_dao = new User_DAO();
        user_dao.setUser_id(SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, ""));

        if (Utility.getSharedInstance().isConnectedToInternet(getActivity())) {


            Utility.getSharedInstance().showProgressDialog(getActivity());

            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().get_albums(clientService, X_API_KEY, user_dao);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    // JsonObject responseData = response.body();
                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());

                    if (responseData != null) {


                        Gson gson = new Gson();

                        GetAlbumList getAlbumList = gson.fromJson(responseData, GetAlbumList.class);

                        List<GetAlbumsListData> getAlbumsListData = new ArrayList<>();

                        getAlbumsListData = getAlbumList.getRespon();

                        GetAlbumListAdapter getAlbumListAdapter = new GetAlbumListAdapter(FragmentAlbumList.this, getAlbumsListData);
                        mListAlbum.setAdapter(getAlbumListAdapter);


                        Utility.getSharedInstance().dismissProgressDialog();

//                        Toast.makeText(getActivity(), "Please scroll from LEFT TO RIGHT", Toast.LENGTH_SHORT).show();


                    } else {
                        Utility.getSharedInstance().dismissProgressDialog();
                    }

                }
                // Utility.getSharedInstance().Snackbar(toolbarLayout, "Password Changed Sucessfully");


                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Utility.getSharedInstance().dismissProgressDialog();

                }
            });
        } else {
            Utility.getSharedInstance().dismissProgressDialog();

            Toast.makeText(getActivity(), "No internet connection available", Toast.LENGTH_SHORT).show();
        }
    }

    public void downloadAllSongsInAlbum() {
        MyApplication myApplication = new MyApplication();

        RequestSearchMedia requestSearchMedia = new RequestSearchMedia();
        requestSearchMedia.setLimit("100000");
        requestSearchMedia.setQuedisc(1);
        requestSearchMedia.setShould_orderby_name(false);
        requestSearchMedia.setUser_id(SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,""));
        requestSearchMedia.setAlbum_id_new(SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_LANG_ID,"1"));

        requestSearchMedia.setMedia_type_id(SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT_ORIGINAL, "none"));
        requestSearchMedia.setOffset(String.valueOf(0));
        if (Utility.getSharedInstance().isConnectedToInternet(getContext())) {

            Utility.getSharedInstance().showProgressDialog(getActivity());

            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().getSongsNew(clientService,X_API_KEY, requestSearchMedia);
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @RequiresApi(api = Build.VERSION_CODES.O)
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    JsonObject responseData = response.body();
                    if (responseData != null) {
                        if (responseData.get("respon").getAsJsonArray() != null) {
                            Gson gson = new Gson();
                            Type listType = new TypeToken<List<SongDetail>>() {
                            }.getType();
                            if (responseData.get("respon") != null) {

                                JsonArray jsonArray = responseData.get("respon").getAsJsonArray();

                                if (jsonArray.size() > 0) {
                                    mSongss = gson.fromJson(responseData.get("respon"), listType);
                                    mSongssSeries.clear();
                                    for(int i=0;i<mSongss.size();i++)
                                    {
                                        mSongssSeries.add(mSongss.get(i));
                                    }

                                    DashBoardActivity.context.downloadfromOtherFragmentSerise(mSongss.get(0));
                                } else {
                                }
                            }
                        } else {
                        }
                    }
                    Utility.getSharedInstance().dismissProgressDialog();
//                    Toast.makeText(getActivity(), "No more songs", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Utility.getSharedInstance().dismissProgressDialog();
                    Toast.makeText(getActivity(), "Server error .. Please try again after some time", Toast.LENGTH_SHORT).show();
                }


            });
        } else {
            Utility.getSharedInstance().dismissProgressDialog();
            Toast.makeText(getActivity(), "Server error .. Please try again after some time", Toast.LENGTH_SHORT).show();
        }
    }
}

