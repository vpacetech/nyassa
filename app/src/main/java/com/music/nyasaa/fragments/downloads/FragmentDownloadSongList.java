package com.music.nyasaa.fragments.downloads;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.music.nyasaa.R;
import com.music.nyasaa.adapter.DownloadPlayListAdapter;
import com.music.nyasaa.front_end_layer.activities.download_in_appscope_enc.DataSQLHelper;
import com.music.nyasaa.front_end_layer.activities.download_in_appscope_enc.Table_Details;
import com.music.nyasaa.models.SongDetail;
import com.music.nyasaa.utility.SharedPreferenceManager;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity.PostionOfPlay;
import static com.music.nyasaa.front_end_layer.activities.download_in_appscope_enc.Table_Details.JSON_COL;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentDownloadSongList extends Fragment {

    private View myView;
    private static final String ARG_KEY_NUMBER = "tab_number";

    private static Context context;
    public static List<SongDetail> songDetailDownloadList = new ArrayList<SongDetail>();

    private RecyclerView mMySongsListView;
    private TextView mTxtWarn;
    DownloadPlayListAdapter connectionAdapter;
    ProgressBar loader;
    AVLoadingIndicatorView avi;
    public static FragmentDownloadSongList fragmentDownloadSongList;


    public static FragmentDownloadSongList newInstance(int number) {
        Bundle args = new Bundle();
        args.putInt(ARG_KEY_NUMBER, number);
        FragmentDownloadSongList frag = new FragmentDownloadSongList();
        frag.setArguments(args);
        return frag;
    }


    public FragmentDownloadSongList() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        myView = inflater.inflate(R.layout.fragment_downloads, container, false);
        initView(myView);
        fragmentDownloadSongList = this;
        songDetailDownloadList = new ArrayList<SongDetail>();
        getDownlodedSong();
//        getDownlodedSongAlbums();

        return myView;
    }

    public void getDownlodedSong() {
        songDetailDownloadList = new ArrayList<SongDetail>();

        DataSQLHelper dataSQLHelper = new DataSQLHelper(getContext());
        SQLiteDatabase sqLiteDatabase;
        String getJson = "";
        sqLiteDatabase = dataSQLHelper.getWritableDatabase();
        //  Cursor cursor = dataSQLHelper.getvalue_By_colums(sqLiteDatabase, Table_Details.TABLE_NAME, null, null);

        String albumname = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_ALBUM_NAME, "");
        String queryToGetUniqueRecords = "SELECT * from " + Table_Details.TABLE_NAME;
        if(!albumname.isEmpty())
            queryToGetUniqueRecords = "SELECT * from " + Table_Details.TABLE_NAME + " where album_name = '"+ albumname+"'";
        Cursor cursor = dataSQLHelper.getvalueWithQuery(sqLiteDatabase, queryToGetUniqueRecords);

        while (cursor.moveToNext()) {
            int index1 = cursor.getColumnIndex(JSON_COL);
            getJson = cursor.getString(index1);

            System.out.println("get String json.." + getJson);
            try {
                JSONObject track = new JSONObject(getJson);
                SongDetail songDetails = new SongDetail();
                songDetails.setFile(track.getString("File_Name")); //= track.getString("device_type");
                songDetails.setTitle(track.getString("File_Name")); //= track.getString("device_model");
                songDetails.setCover(track.optString("Image")); //= track.getString("device_firmware");
                songDetails.setTypeOfSong(track.optString("Type"));
                songDetails.setName(track.optString("name"));
                songDetails.setId(track.optString("id"));
                songDetails.setAlbum_name(track.optString("Album_name"));
                // songDetails.setTitle(track.optString("name"));

                File fileo = new File(getActivity().getApplicationContext().getFilesDir(), track.getString("File_Name"));
                String outPath = fileo.getAbsolutePath();
                songDetails.setPath(outPath);

                songDetailDownloadList.add(songDetails);

            } catch (Exception e) {
            }
        }

        if (songDetailDownloadList.size() > 0) {
            connectionAdapter = new DownloadPlayListAdapter(getActivity(), songDetailDownloadList);

            mMySongsListView.setAdapter(connectionAdapter);

            mTxtWarn.setVisibility(View.GONE);
        } else {
            mTxtWarn.setVisibility(View.VISIBLE);
        }
    }




    private void initView(View myView) {

//        myView.getRootView().setBackgroundColor(Color.BLACK);
        myView.getRootView().setBackgroundColor(getResources().getColor(R.color.md_black_1000));
//        myView.getRootView().setBackgroundResource(R.drawable.black_background);

        avi = (AVLoadingIndicatorView) myView.findViewById(R.id.avi);
        mMySongsListView = (RecyclerView) myView.findViewById(R.id.listViewAlbum);
        mTxtWarn = (TextView) myView.findViewById(R.id.txtWarn);
        loader = (ProgressBar) myView.findViewById(R.id.loader);
        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        mMySongsListView.setLayoutManager(linearLayoutManager);
        mMySongsListView.setItemAnimator(new DefaultItemAnimator());
        connectionAdapter = new DownloadPlayListAdapter(getActivity(), songDetailDownloadList);
        if (songDetailDownloadList.size() > 0) {
            mMySongsListView.setAdapter(connectionAdapter);
            mTxtWarn.setVisibility(View.GONE);
            mMySongsListView.setVisibility(View.VISIBLE);
        } else {
            mTxtWarn.setVisibility(View.GONE);
            mMySongsListView.setVisibility(View.VISIBLE);
        }
    }

    public void startLoader() {
//        avi.setVisibility(View.VISIBLE);
//        final Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                //Do something after 100ms
//                avi.setVisibility(View.GONE);
//            }
//        }, 3000);
    }

    public void playPoase(int postion) {


        try {
            PostionOfPlay = postion;

            if (songDetailDownloadList != null)
                for (int i = 0; i < songDetailDownloadList.size(); i++) {
                    if (i != postion)
                        songDetailDownloadList.get(i).setPlayStop(false);

                }
            if (songDetailDownloadList.get(postion).getPlayStop()) {
                songDetailDownloadList.get(postion).setPlayStop(false);
            } else {
                songDetailDownloadList.get(postion).setPlayStop(true);
            }

            connectionAdapter.notifyDataSetChanged();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
