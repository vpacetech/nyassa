package com.music.nyasaa.fragments.myvideos;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;

;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;


import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.github.pedrovgs.DraggableView;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import com.music.nyasaa.R;
import com.music.nyasaa.adapter.GetAllVideosAdapter;
import com.music.nyasaa.controller.MyApplication;


import com.music.nyasaa.front_end_layer.activities.download_in_appscope_enc.DataSQLHelper;

import com.music.nyasaa.front_end_layer.activities.download_in_appscope_enc.DownloadService;
import com.music.nyasaa.models.RequestModel.SearchMedia.RequestSearchMedia;
import com.music.nyasaa.models.ResponseModel.GetAllVideos.GetVideoResponse;
import com.music.nyasaa.models.ResponseModel.GetAllVideos.GetVideoResponseData;

import com.music.nyasaa.models.SongDetail;

import com.music.nyasaa.retrofit.Config;
import com.music.nyasaa.utility.RecyclerItemClickListener;
import com.music.nyasaa.utility.SharedPreferenceManager;
import com.music.nyasaa.utility.Utility;
import com.music.nyasaa.utility.video_view_helper.ResizeSurfaceView;
import com.music.nyasaa.utility.video_view_helper.VideoControllerView;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


import static com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity.PostionOfPlay;
import static com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity.isdownloading;
import static com.music.nyasaa.front_end_layer.activities.download_in_appscope_enc.Table_Details.TABLE_NAME;

import static com.music.nyasaa.retrofit.Config.BASE_VIDEO_URL;
import static com.music.nyasaa.retrofit.Config.X_API_KEY;

public class FragmentMyVideos extends Fragment  {

    private View myView;
    private EditText mEditSearch;
    public static FragmentMyVideos fragmentMyVideoss;
    private static final String ARG_KEY_NUMBER = "tab_number";
    private static Context context;
    private RecyclerView mMySongsListView;
    private DraggableView draggableView;
    private boolean dragFlag = true;
    private final static String TAG = "MainActivity";
    ResizeSurfaceView mVideoSurface;
    MediaPlayer mMediaPlayer;
    VideoControllerView controller;
    private int mVideoWidth;
    private int mVideoHeight;
    private View mContentView;
    private TextView mTxtLang;
    private View mLoadingView;
    private boolean mIsComplete;
    GetAllVideosAdapter getAllVideosAdapter;
    private Dialog subscribe;
    private Button mSubscribeBut;
    private ImageView mImgFullScreen;
    private SwipyRefreshLayout swipyRefreshLayoutDirection;
    private TextView mTxtWarn;
    public static List<GetVideoResponseData> getEventResponseDataRespon = new ArrayList<>();
    //    private BetterVideoPlayer videoView;
    private int currentPage = 0;
    boolean isSearch = false;

    public static FragmentMyVideos newInstance(int number) {
        Bundle args = new Bundle();
        args.putInt(ARG_KEY_NUMBER, number);
        FragmentMyVideos frag = new FragmentMyVideos();
        frag.setArguments(args);
        return frag;
    }


    public FragmentMyVideos() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        myView = inflater.inflate(R.layout.fragment_myvideos, container, false);

        initView(myView);
        fragmentMyVideoss = FragmentMyVideos.this;

        return myView;
    }


    private void initView(View myView) {

        mTxtLang = (TextView) myView.findViewById(R.id.txtLang);

        mVideoSurface = (ResizeSurfaceView) myView.findViewById(R.id.videoSurface);
        mContentView = myView.findViewById(R.id.video_container);
        mLoadingView = myView.findViewById(R.id.loading);
        mTxtWarn = (TextView) myView.findViewById(R.id.txtWarn);
        mMySongsListView = (RecyclerView) myView.findViewById(R.id.listViewAlbum);
        mTxtWarn.setVisibility(View.GONE);

        swipyRefreshLayoutDirection = (SwipyRefreshLayout) myView.findViewById(R.id.swipe_refresh_layout);
        swipyRefreshLayoutDirection.setDirection(SwipyRefreshLayoutDirection.BOTH);

        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        mMySongsListView.setLayoutManager(linearLayoutManager);
        mMySongsListView.setItemAnimator(new DefaultItemAnimator());

        mImgFullScreen = (ImageView) myView.findViewById(R.id.fullscreenImg);


        currentPage = 0;

        mEditSearch = (EditText) myView.findViewById(R.id.editSearch);
        mEditSearch.setText("");
        mEditSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                /*if (mEditSearch.getText().toString().trim().length() >= 3) {
                    getSearchedVideos(mEditSearch.getText().toString().trim());
                }*/
                currentPage=0;
                isSearch = true;
                if (mEditSearch.getText().toString().length() == 0)
                    getSearchedVideos(mEditSearch.getText().toString().trim(),0);
                else if (mEditSearch.getText().toString().length() == 1)
                    getSearchedVideos(mEditSearch.getText().toString().trim(),0);
                else if (mEditSearch.getText().toString().length() > 0)
                    getSearchedVideos(mEditSearch.getText().toString().trim(),0);
                else if (mEditSearch.getText().toString().length() > 1)
                    getSearchedVideos(mEditSearch.getText().toString().trim(),0);
                else if (mEditSearch.getText().toString().length() > 2)
                    getSearchedVideos(mEditSearch.getText().toString().trim(),0);
                else if (mEditSearch.getText().toString().length() > 3)
                    getSearchedVideos(mEditSearch.getText().toString().trim(),0);
                else if (mEditSearch.getText().toString().length() > 4)
                    getSearchedVideos(mEditSearch.getText().toString().trim(),0);
                else if (mEditSearch.getText().toString().length() > 5)
                    getSearchedVideos(mEditSearch.getText().toString().trim(),0);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        currentPage = 0;
        firstTimeCall(0);


        swipyRefreshLayoutDirection.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                if (direction == SwipyRefreshLayoutDirection.TOP) {
//                    firstTimeCall(0);
                    if(isSearch)
                        getSearchedVideos(mEditSearch.getText().toString().trim(),0);
                    else
                        getSearchedVideos("",0);
                    currentPage = 0;
                } else {
//                    firstTimeCall(currentPage);
                    if(isSearch)
                        getSearchedVideos(mEditSearch.getText().toString().trim(),0);
                    else
                        getSearchedVideos("",currentPage);
                }
            }
        });

        subscribe = new Dialog(getActivity());
        subscribe.requestWindowFeature(Window.FEATURE_NO_TITLE);
        subscribe.setContentView(R.layout.dialog_subscribe);
        subscribe.getWindow().getAttributes().width = ActionBar.LayoutParams.FILL_PARENT;
        subscribe.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        subscribe.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mSubscribeBut = (Button) subscribe.findViewById(R.id.butNo);

    }


    /// API


    public void getAllVideos(final int currentPage) {

        MyApplication myApplication = new MyApplication();
        Utility.getSharedInstance().showProgressDialog(getActivity());


        if (Utility.getSharedInstance().isConnectedToInternet(getActivity())) {

            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");

            // id="1";
            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");

            String cat = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT, "1");
            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().getAllVideosByCategory(X_API_KEY, cat, "10", String.valueOf(currentPage));
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    // JsonObject responseData = response.body();
                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());

                    if(responseData!=null)
                    {}

                    if (responseData.get("error").getAsInt() == 0) {
                        // Config.getPlanDAO = new GetPlanDAO();
                        Gson gson = new Gson();
                        Utility.getSharedInstance().dismissProgressDialog();

                        if (responseData.get("respon") != null) {

                            JsonArray jsonArray = responseData.get("respon").getAsJsonArray();
                            // JsonObject jsonObject=responseData.getAsJsonObject("respon");
                            if (jsonArray.size() > 0) {
                                mTxtWarn.setVisibility(View.GONE);
                                mMySongsListView.setVisibility(View.VISIBLE);
                                //connectionAdapter = new CommunityAdapter(getActivity(), community_dao);
                                // mMySongsListView.setAdapter(connectionAdapter);
                                if (currentPage == 0) {
                                    GetVideoResponse getEventResponseData = gson.fromJson(responseData, GetVideoResponse.class);
                                    getEventResponseDataRespon = getEventResponseData.getRespon();
                                    getVideoList(getEventResponseDataRespon);
                                    swipyRefreshLayoutDirection.setRefreshing(false);
                                    getList(getEventResponseDataRespon);
                                } else {
                                    GetVideoResponse getEventResponseData = gson.fromJson(responseData, GetVideoResponse.class);
                                    List<GetVideoResponseData> datumList = getEventResponseData.getRespon();
                                    getEventResponseDataRespon.addAll(getEventResponseDataRespon.size(), datumList);
                                    swipyRefreshLayoutDirection.setRefreshing(false);
                                    getUpdatedList();
                                }

                                if(getEventResponseDataRespon!=null) {
                                    if(getEventResponseDataRespon.size()<0) {
                                        swipyRefreshLayoutDirection.setRefreshing(false);
                                        mTxtWarn.setVisibility(View.VISIBLE);
                                        mMySongsListView.setVisibility(View.GONE);
                                    }
                                }
                            } else {
                                swipyRefreshLayoutDirection.setRefreshing(false);
                                if (getEventResponseDataRespon != null && getEventResponseDataRespon.size() == 0) {
                                    mTxtWarn.setVisibility(View.VISIBLE);
                                    mMySongsListView.setVisibility(View.GONE);
                                } else {
                                    mTxtWarn.setVisibility(View.GONE);
                                    mMySongsListView.setVisibility(View.VISIBLE);
                                    Toast.makeText(getActivity(), "No more videos", Toast.LENGTH_SHORT).show();
                                }
                                Utility.getSharedInstance().dismissProgressDialog();
                            }
                        }
                        // Utility.getSharedInstance().Snackbar(toolbarLayout, "Password Changed Sucessfully");
                    } else {
                        swipyRefreshLayoutDirection.setRefreshing(false);
                        Utility.getSharedInstance().dismissProgressDialog();
                        mTxtWarn.setVisibility(View.VISIBLE);
                        mMySongsListView.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Utility.getSharedInstance().dismissProgressDialog();
                    mTxtWarn.setVisibility(View.VISIBLE);
                    mMySongsListView.setVisibility(View.GONE);
                }
            });
        } else {

            Toast.makeText(getActivity(), "No internet connection available", Toast.LENGTH_SHORT).show();
        }
    }

    private void getList(List<GetVideoResponseData> getVideoResponseDataList) {
        getAllVideosAdapter = new GetAllVideosAdapter(getActivity(), getVideoResponseDataList);
        mMySongsListView.setAdapter(getAllVideosAdapter);

        if (getAllVideosAdapter != null)
            getAllVideosAdapter.notifyDataSetChanged();
        currentPage += 10;
    }

    private void getUpdatedList() {
        if (getAllVideosAdapter != null)
            getAllVideosAdapter.notifyDataSetChanged();
        currentPage += 10;
    }



    public void videoDataNew(final String url) {

//        });
//
    }



    public void playPoase(int postion) {
        PostionOfPlay = postion;
        try {
            if (getEventResponseDataRespon != null)
                for (int i = 0; i < getEventResponseDataRespon.size(); i++) {
                    if (i != postion)
                        getEventResponseDataRespon.get(i).setPlayStop(false);

                }
            if (getEventResponseDataRespon.get(postion).getPlayStop()) {
                getEventResponseDataRespon.get(postion).setPlayStop(false);

//            videoView.reset();
            } else {

                getEventResponseDataRespon.get(postion).setPlayStop(true);
                // videoView.start();
            }

            getAllVideosAdapter.notifyDataSetChanged();
        } catch (Exception e){
            e.printStackTrace();
        }

    }


    private void getVideoList(final List<GetVideoResponseData> getVideoResponseDataList) {

        getAllVideosAdapter = new GetAllVideosAdapter(getActivity(), getVideoResponseDataList);
        mMySongsListView.setAdapter(getAllVideosAdapter);

        if (getVideoResponseDataList.size() > 0) {
//            videoView.reset();
//            videoDataNew(BASE_VIDEO_URL + getVideoResponseDataList.get(0).getFile());
        } else {

//            videoView.stop();
//            videoDataNew("");
        }

        // mMySongsListView


        mMySongsListView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, final int position) {


                        //  videoDataPlayer(getVideoResponseDataList, position);
                        try {


                            //videoDataNew(BASE_VIDEO_URL + getVideoResponseDataList.get(position).getFile());
                        } catch (Exception e) {

                        }


                    }
                }));

    }
//
//<<<<<<< HEAD
//Utility.getSharedInstance().showProgressDialog(getActivity());
//                        //  videoDataPlayer(getVideoResponseDataList, position);
//                        mLoadingView.setVisibility(View.VISIBLE);
//                        videoView.setVisibility(View.GONE);
//=======
//    public void playVDO(GetVideoResponseData getVideoResponseData) {
//
//        mLoadingView.setVisibility(View.VISIBLE);
//        videoView.setVisibility(View.GONE);
//>>>>>>> 53484448aca5d074b2f07a351e7c7246bde8baea
//
//        videoDataNew(BASE_VIDEO_URL + getVideoResponseData.getFile());
//
//    }


    public void downloadVDO(GetVideoResponseData getVideoResponseData) {

           if(isdownloading==false) {
               if (checkAvaiblity(getVideoResponseData)) {

                   Toast.makeText(getActivity(), "Saving Video Offline...", Toast.LENGTH_SHORT).show();
                   String file_Name = getVideoResponseData.getFile();
                   Intent intent = new Intent(getActivity(), DownloadService.class);
                   String URL = BASE_VIDEO_URL + getVideoResponseData.getFile();

                   intent.putExtra("id", getVideoResponseData.getId() + "VDO"); // ADO Or VDO

                   intent.putExtra("name", getVideoResponseData.getName().replace(" ", "%20"));
                   intent.putExtra("Image", getVideoResponseData.getCover().replace(" ", "%20"));
                   intent.putExtra("Image_URL", (Config.BASE_VIDEO_URL + "cover/" + getVideoResponseData.getCover()).replace(" ", "%20"));


                   intent.putExtra("File_Name", file_Name.replace(" ", "%20"));
                   intent.putExtra("Type", "mp3"); // ADO Or VDO
                   intent.putExtra("URL", URL.replace(" ", "%20"));
                   // intent.putExtra("Image", "http://olavideos.s3.amazonaws.com/" + getVideoResponseData.getCover());

                   getActivity().startService(intent);
               } else {
                   Toast.makeText(getActivity(), "Already Downloaded", Toast.LENGTH_SHORT).show();

               }
           }
           else {
               Toast.makeText(getActivity(), "Downloading please wait..", Toast.LENGTH_SHORT).show();

           }


    }

    private boolean checkAvaiblity(GetVideoResponseData forDOWNLOadSongDetail) {
        boolean notAvaible = true;
        try {


            DataSQLHelper dataSQLHelper = new DataSQLHelper(context.getApplicationContext());
            SQLiteDatabase sqLiteDatabase;
            sqLiteDatabase = dataSQLHelper.getWritableDatabase();
            String getJson = "";

            String query = "SELECT * FROM " + TABLE_NAME + " WHERE id='" + forDOWNLOadSongDetail.getId()+"VDO" + "'";
            Cursor cursor = sqLiteDatabase.rawQuery(query, null);

            if (cursor.getCount() > 0) {
                notAvaible = false;

            } else {
                notAvaible = true;
            }
        } catch (Exception e) {

        }

        return notAvaible;
    }

//  /////////////////////////////////////////////////  API  ///////////////////////4


    /////////////////////GET SERACHED SONGS//////////////////////////


    public void getSearchedVideos(final String searchText, final int currentPage) {

        String lang = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_LANG_ID, "none");
        if (lang.equalsIgnoreCase("1")) {
            mTxtLang.setText("English");
        } else if(lang.equalsIgnoreCase("2")) {
            mTxtLang.setText("Hindi");
        }
        if(currentPage == 0 && searchText.equalsIgnoreCase("")){
            getEventResponseDataRespon = new ArrayList<>();
            isSearch = false;
        }

        MyApplication myApplication = new MyApplication();

        //Utility.getSharedInstance().showProgressDialog(getActivity());


        RequestSearchMedia requestSearchMedia = new RequestSearchMedia();

        if(isSearch) {
            requestSearchMedia.setLimit("100");
        } else{
            requestSearchMedia.setLimit("1000");
        }
        requestSearchMedia.setQuedisc(1);
        requestSearchMedia.setShould_orderby_name(false);
       /* if (searchText.length() > 0) {
//            requestSearchMedia.setAlbum_id(SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_LANG_ID,"none"));
        } else {
            requestSearchMedia.setAlbum_id(SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_LANG_ID, "none"));
        }*/
        requestSearchMedia.setAlbum_id(SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_LANG_ID,"null"));
        requestSearchMedia.setMedia_type_id(SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT_ORIGINAL, "none"));
        requestSearchMedia.setOffset(String.valueOf(currentPage));
        requestSearchMedia.setSearchterm(searchText);


        if (Utility.getSharedInstance().isConnectedToInternet(getContext())) {

//            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");
//            String cat = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT, "1");
//            // id="1";
//            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");

            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().searchVideoQA(X_API_KEY, requestSearchMedia);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());

                    if (responseData.get("error").getAsInt() == 0) {
                        // Config.getPlanDAO = new GetPlanDAO();
                        Gson gson = new Gson();

                        //Utility.getSharedInstance().dismissProgressDialog();


                        Type listType = new TypeToken<List<SongDetail>>() {
                        }.getType();
                        if (responseData.get("respon") != null) {

                            JsonArray jsonArray = responseData.get("respon").getAsJsonArray();


                            // JsonObject jsonObject=responseData.getAsJsonObject("respon");

                            if (jsonArray.size() > 0) {
                                //connectionAdapter = new CommunityAdapter(getActivity(), community_dao);
                                // mMySongsListView.setAdapter(connectionAdapter);
                                mTxtWarn.setVisibility(View.GONE);
                                mMySongsListView.setVisibility(View.VISIBLE);
                                //connectionAdapter = new CommunityAdapter(getActivity(), community_dao);
                                // mMySongsListView.setAdapter(connectionAdapter);
                                if (currentPage == 0) {
                                    GetVideoResponse getEventResponseData = gson.fromJson(responseData, GetVideoResponse.class);
                                    getEventResponseDataRespon = getEventResponseData.getRespon();
                                    //getVideoList(getEventResponseDataRespon);
                                    swipyRefreshLayoutDirection.setRefreshing(false);
                                    getList(getEventResponseDataRespon);
                                } else {
                                    GetVideoResponse getEventResponseData = gson.fromJson(responseData, GetVideoResponse.class);
                                    List<GetVideoResponseData> datumList = getEventResponseData.getRespon();
                                    getEventResponseDataRespon.addAll(getEventResponseDataRespon.size(), datumList);
                                    swipyRefreshLayoutDirection.setRefreshing(false);
                                    getUpdatedList();
                                }

                                if(getEventResponseDataRespon!=null) {
                                    if(getEventResponseDataRespon.size()<0) {
                                        swipyRefreshLayoutDirection.setRefreshing(false);
                                        mTxtWarn.setVisibility(View.VISIBLE);
                                        mMySongsListView.setVisibility(View.GONE);
                                    }
                                }

                            } else {
                                swipyRefreshLayoutDirection.setRefreshing(false);
                                if (getEventResponseDataRespon != null && getEventResponseDataRespon.size() > 0 && searchText.equalsIgnoreCase("")) {
                                    mTxtWarn.setVisibility(View.GONE);
                                    mMySongsListView.setVisibility(View.VISIBLE);
                                    Toast.makeText(getActivity(), "No more videos", Toast.LENGTH_SHORT).show();
                                } else {
                                    mTxtWarn.setVisibility(View.VISIBLE);
                                    mMySongsListView.setVisibility(View.GONE);
                                }
                                //Utility.getSharedInstance().dismissProgressDialog();
                            }


                        }


                        // Utility.getSharedInstance().Snackbar(toolbarLayout, "Password Changed Sucessfully");


                    } else {
                        swipyRefreshLayoutDirection.setRefreshing(false);
                        //Utility.getSharedInstance().dismissProgressDialog();
                        mTxtWarn.setVisibility(View.VISIBLE);
                        mMySongsListView.setVisibility(View.GONE);
//                        videoView.setVisibility(View.GONE);

                    }

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    //Utility.getSharedInstance().dismissProgressDialog();
                    /*mTxtWarn.setVisibility(View.VISIBLE);
                    mMySongsListView.setVisibility(View.GONE);*/
                    swipyRefreshLayoutDirection.setRefreshing(false);
                    Toast.makeText(getActivity(), "Server error .. Please try again after some time", Toast.LENGTH_SHORT).show();
                }


            });
        } else {
            swipyRefreshLayoutDirection.setRefreshing(false);
            Toast.makeText(getActivity(), "No internet connection available", Toast.LENGTH_SHORT).show();
        }
    }

/// FIRST TIME CALL

    public void firstTimeCall(final int currentPage) {
        mTxtLang.setText("English");
        SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_LANG_ID, "1");
        MyApplication myApplication = new MyApplication();

        RequestSearchMedia requestSearchMedia = new RequestSearchMedia();

        requestSearchMedia.setLimit("10");
        requestSearchMedia.setQuedisc(1);
        requestSearchMedia.setShould_orderby_name(false);
        requestSearchMedia.setAlbum_id("1");

        requestSearchMedia.setMedia_type_id(SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT_ORIGINAL, "1"));
        requestSearchMedia.setOffset(String.valueOf(currentPage));
        requestSearchMedia.setSearchterm("");

        String cat = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT, "0");
//        String cat= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT,"0");
//        requestSearchMedia.setCategory(cat);

        if (Utility.getSharedInstance().isConnectedToInternet(getContext())) {
            Utility.getSharedInstance().showProgressDialog(getActivity());
//            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");
//            String cat = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT, "1");
//            // id="1";
//            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");

            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().searchVideoQA(X_API_KEY, requestSearchMedia);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());

                    if (responseData.get("error").getAsInt() == 0) {
                        // Config.getPlanDAO = new GetPlanDAO();
                        Gson gson = new Gson();

                        Utility.getSharedInstance().dismissProgressDialog();


                        Type listType = new TypeToken<List<SongDetail>>() {
                        }.getType();
                        if (responseData.get("respon") != null) {

                            JsonArray jsonArray = responseData.get("respon").getAsJsonArray();


                            // JsonObject jsonObject=responseData.getAsJsonObject("respon");

                            if (jsonArray.size() > 0) {


                              /*  //connectionAdapter = new CommunityAdapter(getActivity(), community_dao);
                                // mMySongsListView.setAdapter(connectionAdapter);

                                GetVideoResponse getEventResponseData = gson.fromJson(responseData, GetVideoResponse.class);
                                getEventResponseDataRespon = getEventResponseData.getRespon();
                                getVideoList(getEventResponseDataRespon);

                                mTxtWarn.setVisibility(View.GONE);
                                mMySongsListView.setVisibility(View.VISIBLE);

                                swipyRefreshLayoutDirection.setRefreshing(false);*/

                                mTxtWarn.setVisibility(View.GONE);
                                mMySongsListView.setVisibility(View.VISIBLE);
                                //connectionAdapter = new CommunityAdapter(getActivity(), community_dao);
                                // mMySongsListView.setAdapter(connectionAdapter);
                                if (currentPage == 0) {
                                    GetVideoResponse getEventResponseData = gson.fromJson(responseData, GetVideoResponse.class);
                                    getEventResponseDataRespon = getEventResponseData.getRespon();
                                    //getVideoList(getEventResponseDataRespon);
                                    swipyRefreshLayoutDirection.setRefreshing(false);
                                    getList(getEventResponseDataRespon);
                                } else {
                                    GetVideoResponse getEventResponseData = gson.fromJson(responseData, GetVideoResponse.class);
                                    List<GetVideoResponseData> datumList = getEventResponseData.getRespon();
                                    getEventResponseDataRespon.addAll(getEventResponseDataRespon.size(), datumList);
                                    swipyRefreshLayoutDirection.setRefreshing(false);
                                    getUpdatedList();
                                }

                                if(getEventResponseDataRespon!=null) {
                                    if(getEventResponseDataRespon.size()<0) {
                                        swipyRefreshLayoutDirection.setRefreshing(false);
                                        mTxtWarn.setVisibility(View.VISIBLE);
                                        mMySongsListView.setVisibility(View.GONE);
                                    }
                                }

                            } else {
                                swipyRefreshLayoutDirection.setRefreshing(false);
                                if (getEventResponseDataRespon != null && getEventResponseDataRespon.size() == 0) {
                                    mTxtWarn.setVisibility(View.VISIBLE);
                                    mMySongsListView.setVisibility(View.GONE);
                                } else {
                                    mTxtWarn.setVisibility(View.GONE);
                                    mMySongsListView.setVisibility(View.VISIBLE);
                                    Toast.makeText(getActivity(), "No more videos", Toast.LENGTH_SHORT).show();
                                }
                                Utility.getSharedInstance().dismissProgressDialog();
                            }


                        }


                        // Utility.getSharedInstance().Snackbar(toolbarLayout, "Password Changed Sucessfully");


                    } else {
                        swipyRefreshLayoutDirection.setRefreshing(false);
                        Utility.getSharedInstance().dismissProgressDialog();
                        mTxtWarn.setVisibility(View.VISIBLE);
                        mMySongsListView.setVisibility(View.GONE);
//                        videoView.setVisibility(View.GONE);

                    }

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Utility.getSharedInstance().dismissProgressDialog();
                    swipyRefreshLayoutDirection.setRefreshing(false);
                    /*mTxtWarn.setVisibility(View.VISIBLE);
                    mMySongsListView.setVisibility(View.GONE);*/
                    Toast.makeText(getActivity(), "Server error .. Please try again after some time", Toast.LENGTH_SHORT).show();
                }


            });
        } else {
            swipyRefreshLayoutDirection.setRefreshing(false);
        }
    }







    //// END
    public void getCateGoryVideos(String catID) {


        MyApplication myApplication = new MyApplication();

        Utility.getSharedInstance().showProgressDialog(getActivity());


        if (Utility.getSharedInstance().isConnectedToInternet(getContext())) {

            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");
            String cat = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT, "1");
            // id="1";
            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");

            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().getVideosByCatID(X_API_KEY, catID, "10", "0");
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());

                    if (responseData.get("error").getAsInt() == 0) {
                        // Config.getPlanDAO = new GetPlanDAO();
                        Gson gson = new Gson();

                        Utility.getSharedInstance().dismissProgressDialog();


                        Type listType = new TypeToken<List<SongDetail>>() {
                        }.getType();
                        if (responseData.get("respon") != null) {

                            JsonArray jsonArray = responseData.get("respon").getAsJsonArray();


                            // JsonObject jsonObject=responseData.getAsJsonObject("respon");

                            if (jsonArray.size() > 0) {


                                //connectionAdapter = new CommunityAdapter(getActivity(), community_dao);
                                // mMySongsListView.setAdapter(connectionAdapter);

                                GetVideoResponse getEventResponseData = gson.fromJson(responseData, GetVideoResponse.class);

                                getEventResponseDataRespon = getEventResponseData.getRespon();

                                getVideoList(getEventResponseDataRespon);


                                mTxtWarn.setVisibility(View.GONE);
                                mMySongsListView.setVisibility(View.VISIBLE);

                                swipyRefreshLayoutDirection.setRefreshing(false);

                            } else {
//                                videoView.stop();
                                swipyRefreshLayoutDirection.setRefreshing(false);
                                mTxtWarn.setVisibility(View.VISIBLE);
                                mMySongsListView.setVisibility(View.GONE);
//                                videoView.setVisibility(View.GONE);
                                Utility.getSharedInstance().dismissProgressDialog();
                            }


                        }


                        // Utility.getSharedInstance().Snackbar(toolbarLayout, "Password Changed Sucessfully");


                    } else {
                        swipyRefreshLayoutDirection.setRefreshing(false);
                        Utility.getSharedInstance().dismissProgressDialog();
                        mTxtWarn.setVisibility(View.VISIBLE);
                        mMySongsListView.setVisibility(View.GONE);

                    }

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                    Utility.getSharedInstance().dismissProgressDialog();
                    mTxtWarn.setVisibility(View.VISIBLE);
                    mMySongsListView.setVisibility(View.GONE);

                }


            });
        } else {

        }
    }

}