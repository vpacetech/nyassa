package com.music.nyasaa.fragments.playlistSongs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.music.nyasaa.R;
import com.music.nyasaa.adapter.PlayListMySongsAdapter;
import com.music.nyasaa.controller.MyApplication;
import com.music.nyasaa.fragments.about_us.FragmentAboutUs;
import com.music.nyasaa.models.RequestModel.GetMyPlayList.RequestGetMyPlayList;
import com.music.nyasaa.models.ResponseModel.GetPlayListSongs.GetPlayListSongsList;
import com.music.nyasaa.models.SongDetail;
import com.music.nyasaa.utility.SharedPreferenceManager;
import com.music.nyasaa.utility.Utility;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity.PostionOfPlay;
import static com.music.nyasaa.retrofit.Config.X_API_KEY;
import static com.music.nyasaa.retrofit.Config.clientService;

public class FragmentPlayListSongs extends Fragment {

    private View myView;
    private static final String ARG_KEY_NUMBER = "tab_number";
    private static Context context;


    private RecyclerView mMyPlayList;
   public static FragmentPlayListSongs FragmentPlayListSongss;
    private TextView mTxtWarn;
    private MyApplication myApplication = new MyApplication();
    private PlayListMySongsAdapter myPlayListAdapter;

    public static   List<SongDetail> getMyPlayListResponseDataPlayList;
    private Dialog confrimDeleteDialog;

    private SwipyRefreshLayout swipyRefreshLayoutDirection;
    private String playLisID="";


    public static FragmentPlayListSongs newInstance(int number) {
        Bundle args = new Bundle();
        args.putInt(ARG_KEY_NUMBER, number);
        FragmentPlayListSongs frag = new FragmentPlayListSongs();
        frag.setArguments(args);
        return frag;
    }

    public static FragmentAboutUs newInstance(Context mContext) {
        FragmentAboutUs f = new FragmentAboutUs();
        context = mContext;
        return f;
    }

    public FragmentPlayListSongs() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        myView = inflater.inflate(R.layout.fragment_songs_playlist, container, false);
        initView(myView);
        FragmentPlayListSongss=FragmentPlayListSongs.this;
        return myView;
    }


    private void initView(View myView) {


        playLisID=SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_PLAYLIST_ID,"0");

//        myView.getRootView().setBackgroundColor(Color.BLACK);
        myView.getRootView().setBackgroundColor(getResources().getColor(R.color.md_black_1000));
//        myView.getRootView().setBackgroundResource(R.drawable.black_gradient);
        mTxtWarn = (TextView) myView.findViewById(R.id.txtWarn);
        swipyRefreshLayoutDirection = (SwipyRefreshLayout) myView.findViewById(R.id.swipe_refresh_layout);
        swipyRefreshLayoutDirection.setDirection(SwipyRefreshLayoutDirection.BOTTOM);

        mTxtWarn.setVisibility(View.GONE);
        myView.getRootView().setBackgroundColor(Color.BLACK);
        mMyPlayList = (RecyclerView) myView.findViewById(R.id.listViewAlbum);
        //RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        mMyPlayList.setLayoutManager(linearLayoutManager);
        mMyPlayList.setItemAnimator(new DefaultItemAnimator());


        swipyRefreshLayoutDirection.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                getAllMySongs(playLisID);
            }
        });


        getAllMySongs(playLisID);
    }


    public void getAllMySongs(String playLisID ) {

        Utility.getSharedInstance().showProgressDialog(getActivity());


        RequestGetMyPlayList requestGetMyPlayList = new RequestGetMyPlayList();

        requestGetMyPlayList.setPlaylist_id(playLisID);
        if (Utility.getSharedInstance().isConnectedToInternet(getContext())) {

            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");

            // id="1";
            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");

            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().getMySongsFromPlayList(clientService,X_API_KEY,requestGetMyPlayList);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());


                    try{


                        if (responseData!=null) {
                            // Config.getPlanDAO = new GetPlanDAO();
                            Gson gson = new Gson();

                            Utility.getSharedInstance().dismissProgressDialog();


                            if (responseData.get("respon") != null) {

                                JsonArray jsonArray = responseData.get("respon").getAsJsonArray();




                                mTxtWarn.setVisibility(View.GONE);
                                mMyPlayList.setVisibility(View.VISIBLE);

                                swipyRefreshLayoutDirection.setRefreshing(false);

//                                Toast.makeText(getActivity(), "Suces", Toast.LENGTH_SHORT).show();


                                gson = new Gson();
                                GetPlayListSongsList getMyPlayListResponse = gson.fromJson(responseData, GetPlayListSongsList.class);

                                if(getMyPlayListResponse.getRespon().get(0).getTracks()!=null)
                                {
                                    getMyPlayListResponseDataPlayList = getMyPlayListResponse.getRespon().get(0).getTracks();
                                    myPlayListAdapter = new PlayListMySongsAdapter(getActivity(), getMyPlayListResponseDataPlayList);
                                    mMyPlayList.setAdapter(myPlayListAdapter);
                                }
                                else {

                                    swipyRefreshLayoutDirection.setRefreshing(false);
                                    mTxtWarn.setVisibility(View.VISIBLE);
                                    mMyPlayList.setVisibility(View.GONE);
                                }



                            }

                            else {

                                swipyRefreshLayoutDirection.setRefreshing(false);
                                mTxtWarn.setVisibility(View.VISIBLE);
                                mMyPlayList.setVisibility(View.GONE);
                                Utility.getSharedInstance().dismissProgressDialog();


                                // JsonObject jsonObject=responseData.getAsJsonObject("respon");

                            }
                            // Utility.getSharedInstance().Snackbar(toolbarLayout, "Password Changed Sucessfully");


                        } else {
                            swipyRefreshLayoutDirection.setRefreshing(false);
                            Utility.getSharedInstance().dismissProgressDialog();
                            mTxtWarn.setVisibility(View.VISIBLE);
                            mMyPlayList.setVisibility(View.GONE);

                        }
                    }
                    catch ( Exception e)
                    {
                        Utility.getSharedInstance().dismissProgressDialog();
                        mTxtWarn.setVisibility(View.VISIBLE);
                        mMyPlayList.setVisibility(View.GONE);
                    }


                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                    Utility.getSharedInstance().dismissProgressDialog();
                    mTxtWarn.setVisibility(View.VISIBLE);
                    mMyPlayList.setVisibility(View.GONE);

                }


            });
        } else {

        }
    }

    public void  playPoase(int postion)
    {
        PostionOfPlay=postion;

        if(getMyPlayListResponseDataPlayList!=null)
            for(int i=0;i<getMyPlayListResponseDataPlayList.size();i++)
            {
                if(i!=postion)
                    getMyPlayListResponseDataPlayList.get(i).setPlayStop(false);

            }
            try {
                if(getMyPlayListResponseDataPlayList.get(postion).getPlayStop())
                {
                    getMyPlayListResponseDataPlayList.get(postion).setPlayStop(false);
                }
                else
                {
                    getMyPlayListResponseDataPlayList.get(postion).setPlayStop(true);
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }


        if(myPlayListAdapter!=null)
        myPlayListAdapter.notifyDataSetChanged();



    }
}
