package com.music.nyasaa.fragments.interviews;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.music.nyasaa.R;
import com.music.nyasaa.adapter.InterViewMoreAdapter;
import com.music.nyasaa.controller.MyApplication;
import com.music.nyasaa.models.RequestModel.SearchMedia.RequestSearchMedia;
import com.music.nyasaa.models.SongDetail;
import com.music.nyasaa.utility.SharedPreferenceManager;
import com.music.nyasaa.utility.Utility;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity.PostionOfPlay;
import static com.music.nyasaa.retrofit.Config.X_API_KEY;

public class FragmentInterview extends Fragment implements View.OnClickListener {

    private View myView;
    public static FragmentInterview fragmentInterview;
    private static final String ARG_KEY_NUMBER = "tab_number";
    private static Context context;
    public static ArrayList<SongDetail> mSongss;
    private SwipyRefreshLayout swipyRefreshLayoutDirection;
    private TextView mTxtWarn;
    InterViewMoreAdapter interViewMoreAdapter;
    private RecyclerView mMySongsListView;
    private FragmentInterview fragmentMySongs;
    private EditText mEditSearch;
    private int currentPage = 0;
    boolean isSearch = false;
    private String albumID = "";


    private TextView mTxtLang;

    private LinearLayout mLayMembers, mLayRequest, mLayMainBar, mLayPost;
    boolean post = false, request = false;
    private TextView mTxtMember, mTxtRequest, mTxtPost, mTxtShowTech;

    public static FragmentInterview newInstance(int number) {
        Bundle args = new Bundle();
        args.putInt(ARG_KEY_NUMBER, number);
        FragmentInterview frag = new FragmentInterview();
        frag.setArguments(args);
        return frag;
    }


    public FragmentInterview() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        myView = inflater.inflate(R.layout.fragment_interview, container, false);
        initView(myView);
        onClick();
        firstColor();
        fragmentInterview = this;
        return myView;
    }


    private void onClick() {


        mLayRequest.setOnClickListener(this);
        mLayMembers.setOnClickListener(this);
        mLayPost.setOnClickListener(this);
    }
    private void initView(View myView) {


        mLayMainBar = (LinearLayout) myView.findViewById(R.id.menuLayBar);
        mLayMembers = (LinearLayout) myView.findViewById(R.id.layMember);
        mLayRequest = (LinearLayout) myView.findViewById(R.id.layRequest);

        mLayPost = (LinearLayout) myView.findViewById(R.id.layPost);


        mTxtMember = (TextView) myView.findViewById(R.id.txtMembers);

        mTxtRequest = (TextView) myView.findViewById(R.id.txtRequest);
        mTxtLang = (TextView) myView.findViewById(R.id.txtLang);
        mTxtPost = (TextView) myView.findViewById(R.id.txtPost);

        albumID = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_LANG_ID, "1");


//        myView.getRootView().setBackgroundColor(Color.BLACK);
        myView.getRootView().setBackgroundColor(getResources().getColor(R.color.orBLACK));
        mTxtWarn = (TextView) myView.findViewById(R.id.txtWarn);
        mTxtWarn.setVisibility(View.GONE);
        swipyRefreshLayoutDirection = (SwipyRefreshLayout) myView.findViewById(R.id.swipe_refresh_layout);
        swipyRefreshLayoutDirection.setDirection(SwipyRefreshLayoutDirection.BOTH);
        swipyRefreshLayoutDirection.setVisibility(View.GONE);
        mMySongsListView = (RecyclerView) myView.findViewById(R.id.listViewAlbum);
        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        mMySongsListView.setLayoutManager(linearLayoutManager);
        mMySongsListView.setItemAnimator(new DefaultItemAnimator());

        // MyPlayListAdapter connectionAdapter = new MyPlayListAdapter(getActivity(),img,name);
        // mMySongsListView.setAdapter(connectionAdapter);
//        mMySongsListView =(RecyclerView)myView.findViewById(R.id.listViewAlbum);
        currentPage = 0;

        mEditSearch = (EditText) myView.findViewById(R.id.editSearch);
        mEditSearch.setText("");
        mEditSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                /*if (mEditSearch.getText().toString().trim().length() >= 3) {
                    getSearchedSOngs(mEditSearch.getText().toString().trim());
                }*/
                isSearch = true;
//                if (mEditSearch.getText().toString().length() == 0)
//                    getSearchedSOngs(mEditSearch.getText().toString().trim(), albumID);
//                else if (mEditSearch.getText().toString().length() == 1)
//                    getSearchedSOngs(mEditSearch.getText().toString().trim(), albumID);
//                else if (mEditSearch.getText().toString().length() > 0)
//                    getSearchedSOngs(mEditSearch.getText().toString().trim(), albumID);
//                else if (mEditSearch.getText().toString().length() > 1)
//                    getSearchedSOngs(mEditSearch.getText().toString().trim(), albumID);
//                else if (mEditSearch.getText().toString().length() > 2)
//                    getSearchedSOngs(mEditSearch.getText().toString().trim(), albumID);
//                else if (mEditSearch.getText().toString().length() > 3)
//                    getSearchedSOngs(mEditSearch.getText().toString().trim(), albumID);
//                else if (mEditSearch.getText().toString().length() > 4)
//                    getSearchedSOngs(mEditSearch.getText().toString().trim(), albumID);
//                else if (mEditSearch.getText().toString().length() > 5)
//                    getSearchedSOngs(mEditSearch.getText().toString().trim(), albumID);

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        String more = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_TXT_MORE, "none");

        if (more.equalsIgnoreCase("more")) {
            getSearchedSOngs("", albumID);
        } else {
            getSearchedSOngs("", albumID);
        }


        swipyRefreshLayoutDirection.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                if (direction == SwipyRefreshLayoutDirection.TOP) {
                    if (isSearch)
                        getSearchedSOngs(mEditSearch.getText().toString().trim(), albumID);
                    else
                        getAllMusics(0);
                    currentPage = 0;
                } else {
                    if (isSearch)
                        getSearchedSOngs(mEditSearch.getText().toString().trim(), albumID);
                    else
                        getAllMusics(currentPage);
                }
            }
        });

    }


    public void getAllMusics(final int currentPage) {

        MyApplication myApplication = new MyApplication();

        if (Utility.getSharedInstance().isConnectedToInternet(getContext())) {
            Utility.getSharedInstance().showProgressDialog(getActivity());
            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");
            String cat = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT, "1");
            // id="1";
            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");

            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().getInterviwsBycatergory(X_API_KEY, cat, "10", String.valueOf(currentPage));
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());
                    if (responseData != null) {
                        if (responseData.get("error").getAsInt() == 0) {
                            // Config.getPlanDAO = new GetPlanDAO();
                            Gson gson = new Gson();
                            Utility.getSharedInstance().dismissProgressDialog();
                            Type listType = new TypeToken<List<SongDetail>>() {
                            }.getType();

                            if (responseData.get("respon") != null) {
                                JsonArray jsonArray = responseData.get("respon").getAsJsonArray();
                                if (jsonArray.size() > 0) {
                                    mTxtWarn.setVisibility(View.GONE);
                                    mMySongsListView.setVisibility(View.VISIBLE);
                                    if (currentPage == 0) {
                                        mSongss = gson.fromJson(responseData.get("respon"), listType);
                                        swipyRefreshLayoutDirection.setRefreshing(false);
                                        getMusicList(mSongss);
                                    } else {
                                        ArrayList<SongDetail> datumList = gson.fromJson(responseData.get("respon"), listType);
                                        mSongss.addAll(mSongss.size(), datumList);
                                        swipyRefreshLayoutDirection.setRefreshing(false);
                                        getUpdatedList();
                                    }

                                    if (mSongss != null) {
                                        if (mSongss.size() < 0) {
                                            swipyRefreshLayoutDirection.setRefreshing(false);
                                            mTxtWarn.setVisibility(View.VISIBLE);
                                            mMySongsListView.setVisibility(View.GONE);
                                        }
                                    }
                                } else {
                                    swipyRefreshLayoutDirection.setRefreshing(false);
                                    Utility.getSharedInstance().dismissProgressDialog();
                                    if (mSongss != null && mSongss.size() > 0) {
                                        mTxtWarn.setVisibility(View.GONE);
                                        mMySongsListView.setVisibility(View.VISIBLE);
                                        Toast.makeText(getActivity(), "No more interviews", Toast.LENGTH_SHORT).show();
                                    } else {
                                        mTxtWarn.setVisibility(View.VISIBLE);
                                        mMySongsListView.setVisibility(View.GONE);
                                    }
                                }
                            }
                            // Utility.getSharedInstance().Snackbar(toolbarLayout, "Password Changed Sucessfully");
                        } else {
                            swipyRefreshLayoutDirection.setRefreshing(false);
                            Utility.getSharedInstance().dismissProgressDialog();
                            mTxtWarn.setVisibility(View.VISIBLE);
                            mMySongsListView.setVisibility(View.GONE);
                        }

                    } else {
                        swipyRefreshLayoutDirection.setRefreshing(false);
                        Utility.getSharedInstance().dismissProgressDialog();
                        mTxtWarn.setVisibility(View.VISIBLE);
                        mMySongsListView.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Utility.getSharedInstance().dismissProgressDialog();
                    mTxtWarn.setVisibility(View.VISIBLE);
                    mMySongsListView.setVisibility(View.GONE);
                    swipyRefreshLayoutDirection.setRefreshing(false);
                    //Toast.makeText(getActivity(), "Server error .. Please try again after some time", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            swipyRefreshLayoutDirection.setRefreshing(false);
            Toast.makeText(getActivity(), "No internet connection available", Toast.LENGTH_SHORT).show();
        }
    }

    private void getMusicList(ArrayList<SongDetail> songDetailList) {
//        interViewMoreAdapter = new InterViewMoreAdapter(getActivity(), songDetailList, FragmentInterview.this);
//        mMySongsListView.setAdapter(interViewMoreAdapter);

        if (interViewMoreAdapter != null)
            interViewMoreAdapter.notifyDataSetChanged();
        currentPage = currentPage + 10;
    }

    private void getUpdatedList() {
        if (interViewMoreAdapter != null)
            interViewMoreAdapter.notifyDataSetChanged();
        currentPage = currentPage + 10;
    }

    /////////////////////GET SERACHED SONGS//////////////////////////


    public void getSearchedSOngs(String searchText, String albumID) {
        albumID = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_LANG_ID, "1");
        if (albumID.equalsIgnoreCase("1")) {
            mTxtLang.setText("English");



        } else {
            mTxtLang.setText("Hindi");

        }

        MyApplication myApplication = new MyApplication();

        //Utility.getSharedInstance().showProgressDialog(getActivity());


        RequestSearchMedia requestSearchMedia = new RequestSearchMedia();

        requestSearchMedia.setLimit("1200");
        requestSearchMedia.setAlbum_id(albumID);
        requestSearchMedia.setOffset("0");
        requestSearchMedia.setSearchterm(searchText);

        String cat = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT, "0");
//        requestSearchMedia.setCategory(cat);


        if (Utility.getSharedInstance().isConnectedToInternet(getContext())) {

//            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");
//            String cat = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT, "1");
//            // id="1";
//            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");

            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().searchinterviews(X_API_KEY, requestSearchMedia);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();




                    JsonObject responseData = response.body();

                    if(responseData!=null) {
                        // Log.e("Response::", responseData.toString());

                        if (responseData.get("error").getAsInt() == 0) {
                            // Config.getPlanDAO = new GetPlanDAO();
                            Gson gson = new Gson();

                            //Utility.getSharedInstance().dismissProgressDialog();


                            Type listType = new TypeToken<List<SongDetail>>() {
                            }.getType();
                            if (responseData.get("respon") != null) {

                                JsonArray jsonArray = responseData.get("respon").getAsJsonArray();


                                // JsonObject jsonObject=responseData.getAsJsonObject("respon");

                                if (jsonArray.size() > 0) {

                                    mSongss = gson.fromJson(responseData.get("respon"), listType);
                                    mTxtWarn.setVisibility(View.GONE);
                                    mMySongsListView.setVisibility(View.VISIBLE);
                                    swipyRefreshLayoutDirection.setRefreshing(false);

                                    if (mSongss != null) {
                                        if (mSongss.size() < 0) {
                                            swipyRefreshLayoutDirection.setRefreshing(false);
                                            mTxtWarn.setVisibility(View.VISIBLE);
                                            mMySongsListView.setVisibility(View.GONE);
                                        }
                                    }

//                                AudioSerachSongsAdapter interViewMoreAdapter = new AudioSerachSongsAdapter(getActivity(), mSongs);

                                    interViewMoreAdapter = new InterViewMoreAdapter(getActivity(), mSongss, FragmentInterview.this,0);
                                    mMySongsListView.setAdapter(interViewMoreAdapter);
                                } else {
                                    swipyRefreshLayoutDirection.setRefreshing(false);
                                    mTxtWarn.setVisibility(View.VISIBLE);
                                    mMySongsListView.setVisibility(View.GONE);
                                    //Utility.getSharedInstance().dismissProgressDialog();
                                }


                            }


                            // Utility.getSharedInstance().Snackbar(toolbarLayout, "Password Changed Sucessfully");


                        } else {
                            swipyRefreshLayoutDirection.setRefreshing(false);
                            //Utility.getSharedInstance().dismissProgressDialog();
                            mTxtWarn.setVisibility(View.VISIBLE);
                            mMySongsListView.setVisibility(View.GONE);

                        }
                    }
                    else {
                        swipyRefreshLayoutDirection.setRefreshing(false);
                        //Utility.getSharedInstance().dismissProgressDialog();
                        mTxtWarn.setVisibility(View.VISIBLE);
                        mMySongsListView.setVisibility(View.GONE);
                    }

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                    //Utility.getSharedInstance().dismissProgressDialog();
                    mTxtWarn.setVisibility(View.VISIBLE);
                    mMySongsListView.setVisibility(View.GONE);
                }
            });
        } else {
            swipyRefreshLayoutDirection.setRefreshing(false);
            Toast.makeText(getActivity(), "No internet connection available", Toast.LENGTH_SHORT).show();
        }
    }


    /// videos


    public void getSearchedVideso(String searchText, String albumID) {
        albumID = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_LANG_ID, "1");
        if (albumID.equalsIgnoreCase("1")) {
            mTxtLang.setText("English");



        } else {
            mTxtLang.setText("Hindi");

        }

        MyApplication myApplication = new MyApplication();

        //Utility.getSharedInstance().showProgressDialog(getActivity());


        RequestSearchMedia requestSearchMedia = new RequestSearchMedia();

        requestSearchMedia.setLimit("1200");
        requestSearchMedia.setAlbum_id(albumID);
        requestSearchMedia.setOffset("0");
        requestSearchMedia.setSearchterm(searchText);

        String cat = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT, "0");
//        requestSearchMedia.setCategory(cat);


        if (Utility.getSharedInstance().isConnectedToInternet(getContext())) {

//            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");
//            String cat = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT, "1");
//            // id="1";
//            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");

            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().searchinterviewsVideos(X_API_KEY, requestSearchMedia);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();
                    if(responseData!=null) {
                        // Log.e("Response::", responseData.toString());

                        if (responseData.get("error").getAsInt() == 0) {
                            // Config.getPlanDAO = new GetPlanDAO();
                            Gson gson = new Gson();

                            //Utility.getSharedInstance().dismissProgressDialog();


                            Type listType = new TypeToken<List<SongDetail>>() {
                            }.getType();
                            if (responseData.get("respon") != null) {

                                JsonArray jsonArray = responseData.get("respon").getAsJsonArray();


                                // JsonObject jsonObject=responseData.getAsJsonObject("respon");

                                if (jsonArray.size() > 0) {

                                    mSongss = gson.fromJson(responseData.get("respon"), listType);
                                    mTxtWarn.setVisibility(View.GONE);
                                    mMySongsListView.setVisibility(View.VISIBLE);
                                    swipyRefreshLayoutDirection.setRefreshing(false);

                                    if (mSongss != null) {
                                        if (mSongss.size() < 0) {
                                            swipyRefreshLayoutDirection.setRefreshing(false);
                                            mTxtWarn.setVisibility(View.VISIBLE);
                                            mMySongsListView.setVisibility(View.GONE);
                                        }
                                    }

//                                AudioSerachSongsAdapter interViewMoreAdapter = new AudioSerachSongsAdapter(getActivity(), mSongs);

                                    interViewMoreAdapter = new InterViewMoreAdapter(getActivity(), mSongss, FragmentInterview.this,1);
                                    mMySongsListView.setAdapter(interViewMoreAdapter);
                                } else {
                                    swipyRefreshLayoutDirection.setRefreshing(false);
                                    mTxtWarn.setVisibility(View.VISIBLE);
                                    mMySongsListView.setVisibility(View.GONE);
                                    //Utility.getSharedInstance().dismissProgressDialog();
                                }


                            }


                            // Utility.getSharedInstance().Snackbar(toolbarLayout, "Password Changed Sucessfully");


                        } else {
                            swipyRefreshLayoutDirection.setRefreshing(false);
                            //Utility.getSharedInstance().dismissProgressDialog();
                            mTxtWarn.setVisibility(View.VISIBLE);
                            mMySongsListView.setVisibility(View.GONE);

                        }
                    }
                    else {
                        swipyRefreshLayoutDirection.setRefreshing(false);
                        //Utility.getSharedInstance().dismissProgressDialog();
                        mTxtWarn.setVisibility(View.VISIBLE);
                        mMySongsListView.setVisibility(View.GONE);
                    }

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                    //Utility.getSharedInstance().dismissProgressDialog();
                    mTxtWarn.setVisibility(View.VISIBLE);
                    mMySongsListView.setVisibility(View.GONE);
                }
            });
        } else {
            swipyRefreshLayoutDirection.setRefreshing(false);
            Toast.makeText(getActivity(), "No internet connection available", Toast.LENGTH_SHORT).show();
        }
    }


    /// end of videos


    public void getCateGorySongs(String catID) {


        MyApplication myApplication = new MyApplication();

        Utility.getSharedInstance().showProgressDialog(getActivity());


        if (Utility.getSharedInstance().isConnectedToInternet(getContext())) {

            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");
            String cat = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT, "1");
            // id="1";
            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");

            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().getSongsByCatID(X_API_KEY, catID, "10", "0");
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());

                    if (responseData.get("error").getAsInt() == 0) {
                        // Config.getPlanDAO = new GetPlanDAO();
                        Gson gson = new Gson();

                        Utility.getSharedInstance().dismissProgressDialog();


                        Type listType = new TypeToken<List<SongDetail>>() {
                        }.getType();
                        if (responseData.get("respon") != null) {

                            JsonArray jsonArray = responseData.get("respon").getAsJsonArray();


                            // JsonObject jsonObject=responseData.getAsJsonObject("respon");

                            if (jsonArray.size() > 0) {

                                mSongss = gson.fromJson(responseData.get("respon"), listType);
                                mTxtWarn.setVisibility(View.GONE);
                                mMySongsListView.setVisibility(View.VISIBLE);
                                swipyRefreshLayoutDirection.setRefreshing(false);


//                                interViewMoreAdapter = new InterViewMoreAdapter(getActivity(), mSongss, FragmentInterview.this);
//                                mMySongsListView.setAdapter(interViewMoreAdapter);
                            } else {
                                swipyRefreshLayoutDirection.setRefreshing(false);
                                mTxtWarn.setVisibility(View.VISIBLE);
                                mMySongsListView.setVisibility(View.GONE);
                                Utility.getSharedInstance().dismissProgressDialog();
                            }


                        }


                        // Utility.getSharedInstance().Snackbar(toolbarLayout, "Password Changed Sucessfully");


                    } else {
                        swipyRefreshLayoutDirection.setRefreshing(false);
                        Utility.getSharedInstance().dismissProgressDialog();
                        mTxtWarn.setVisibility(View.VISIBLE);
                        mMySongsListView.setVisibility(View.GONE);

                    }

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                    Utility.getSharedInstance().dismissProgressDialog();
                    mTxtWarn.setVisibility(View.VISIBLE);
                    mMySongsListView.setVisibility(View.GONE);

                }


            });
        } else {

        }
    }


    public void playPoase(int postion) {
        PostionOfPlay = postion;
        try {
            if (mSongss != null)
                for (int i = 0; i < mSongss.size(); i++) {
                    if (i != postion)
                        mSongss.get(i).setPlayStop(false);

                }
            if (mSongss.get(postion).getPlayStop()) {
                mSongss.get(postion).setPlayStop(false);
            } else {
                mSongss.get(postion).setPlayStop(true);
            }

            interViewMoreAdapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void firstColor() {


        if (albumID.equalsIgnoreCase("1")) {
            mTxtLang.setText("English");



        } else {
            mTxtLang.setText("Hindi");

        }
        SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_TRACK_TYPE, "4");
        SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_INETR_TAB,"1");

        getSearchedSOngs("", albumID);

        mTxtRequest.setTextColor(getResources().getColor(R.color.white));
        mTxtMember.setTextColor(getResources().getColor(R.color.black));

        mTxtPost.setTextColor(getResources().getColor(R.color.white));
        mLayPost.setBackgroundColor(getResources().getColor(R.color.darker_grey));
        mLayMembers.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        mLayRequest.setBackgroundColor(getResources().getColor(R.color.darker_grey));

    }


    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {


            case R.id.layMember:
                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_INETR_TAB,"1");
                getSearchedSOngs("",albumID);

                mEditSearch.setText("");
                mTxtRequest.setTextColor(getResources().getColor(R.color.white));
                mTxtMember.setTextColor(getResources().getColor(R.color.black));

                mTxtPost.setTextColor(getResources().getColor(R.color.white));
                mLayPost.setBackgroundColor(getResources().getColor(R.color.darker_grey));
                mLayMembers.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                mLayRequest.setBackgroundColor(getResources().getColor(R.color.darker_grey));

                break;
            case R.id.layRequest:
                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_INETR_TAB,"2");
                getSearchedVideso("",albumID);


                mEditSearch.setText("");


//                getRequetstData(userID);
                mTxtRequest.setTextColor(getResources().getColor(R.color.black));
                mTxtMember.setTextColor(getResources().getColor(R.color.white));

                mTxtPost.setTextColor(getResources().getColor(R.color.white));
                mLayPost.setBackgroundColor(getResources().getColor(R.color.darker_grey));

                mLayMembers.setBackgroundColor(getResources().getColor(R.color.darker_grey));
                mLayRequest.setBackgroundColor(getResources().getColor(R.color.colorPrimary));


                break;




            default:

                break;

        }

    }
}

