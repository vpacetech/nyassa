package com.music.nyasaa.fragments.profile;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.music.nyasaa.R;
import com.music.nyasaa.controller.MyApplication;
import com.music.nyasaa.models.RequestModel.RequestUpdateProfile;
import com.music.nyasaa.models.ResponseModel.GetImageUrl.GetImageResponse;
import com.music.nyasaa.models.ResponseModel.GetImageUrl.GetImageResponseData;
import com.music.nyasaa.retrofit.Config;
import com.music.nyasaa.utility.SharedPreferenceManager;
import com.music.nyasaa.utility.Utility;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.listeners.IPickCancel;
import com.vansuita.pickimage.listeners.IPickResult;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.music.nyasaa.retrofit.Config.X_API_KEY;

public class FragmentProfile extends Fragment implements View.OnClickListener {

    private View myView;
    private static final String ARG_KEY_NUMBER = "tab_number";

    private RequestBody mFile;

    private ImageView imgProfile;
    private TextView mTxtProfileName, mTxtEmail;
    private EditText medtProfileName;
    private int PERMISSION_ALL = 1;
    private String imgURL = "";
    private String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
    private String getPath = "";
    private Button mButUpdate;
    String strUserName="";
    String strEmail="";
    String strSanyasName="";
    String strPhoneNumber="";
    String strCountryId="";
    String strStateId="";
    String strCityId="";
    String strProfilePhoto="";


    public static FragmentProfile newInstance(int number) {
        Bundle args = new Bundle();
        args.putInt(ARG_KEY_NUMBER, number);
        FragmentProfile frag = new FragmentProfile();
        frag.setArguments(args);
        return frag;
    }


    public FragmentProfile() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        myView = inflater.inflate(R.layout.fragment_profile, container, false);


        initView(myView);
        return myView;
    }


    private void initView(View myView) {

        mTxtEmail = (TextView) myView.findViewById(R.id.txtEmail);
        mTxtProfileName = (TextView) myView.findViewById(R.id.txtprofileName);
        medtProfileName = myView.findViewById(R.id.edtprofileName);
        imgProfile = myView.findViewById(R.id.profileImag);
//        myView.getRootView().setBackgroundColor(Color.BLACK);
//        myView.getRootView().setBackgroundColor(getResources().getColor(R.color.black));
        myView.getRootView().setBackgroundResource(R.drawable.black_gradient);


        mTxtProfileName.setOnClickListener(this);

        mTxtProfileName.setText(SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_NAME,""));

        medtProfileName.setOnClickListener(this);

        mButUpdate = (Button) myView.findViewById(R.id.butUpdate);
        String imgURL = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_PROFILE_PHOTO, "");

        Picasso.with(getActivity()).load(Config.BASE_PROFILE_PIC + imgURL).error(R.drawable.default_music).into(imgProfile);

//        mGridAlbumview =(RecyclerView)myView.findViewById(R.id.listViewAlbum);

        getUserInfo();

        mButUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getPath != "") {
                    sendFile();
                } else {

                 /* //    updateProfile();

                   Toast.makeText(getActivity(), "Please click on Image to update profile pic", Toast.LENGTH_SHORT).show();
*/


                    updateProfile();

                   // Toast.makeText(getActivity(), "Please click on Image to update profile pic", Toast.LENGTH_SHORT).show();
                }
            }
        });

        imgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               //  uploadAvatar();
                onSelectImageClick(view);
            }
        });
    }


    public void getUserInfo() {


        MyApplication myApplication = new MyApplication();

        // Utility.getSharedInstance().showProgressDialog(getActivity());


        if (Utility.getSharedInstance().isConnectedToInternet(getContext())) {

            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");
            String cat = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT, "1");
            // id="1";
            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");

            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().getUserInfo(X_API_KEY, id);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();

                    if (responseData != null) {


                         imgURL = responseData.get("profile_photo").getAsString();

                        if(responseData.get("email")!=null)
                        {
                            strEmail=responseData.get("email").getAsString();

                            mTxtEmail.setText("" + responseData.get("email").getAsString());
                        }

                        if(responseData.get("username")!=null)
                        {
                            mTxtProfileName.setText("" + responseData.get("username").getAsString());
                            medtProfileName.setText(responseData.get("username").getAsString());
                            SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_USER_NAME,responseData.get("username").getAsString());
                        }

                        if(responseData.get("sanyas_name")!=null)
                        {
                            strSanyasName=responseData.get("sanyas_name").getAsString();
                        }

                        if(responseData.get("phone_number")!=null)
                        {
                            strPhoneNumber=responseData.get("phone_number").getAsString();
                        }

                        if(responseData.get("country_id")!=null)
                        {
                            strCountryId=responseData.get("country_id").getAsString();
                        }

                        if(responseData.get("state_id")!=null)
                        {
                            strStateId=responseData.get("state_id").getAsString();
                        }

                        if(responseData.get("city")!=null)
                        {
                            strCityId=responseData.get("city").getAsString();
                        }

                        if(responseData.get("profile_photo")!=null)
                        {
                            strProfilePhoto=responseData.get("profile_photo").getAsString();
                            imgURL=responseData.get("profile_photo").getAsString();
                        }
                    }
                    // Log.e("Response::", responseData.toString());


                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                    //  Utility.getSharedInstance().dismissProgressDialog();
                    /*mTxtWarn.setVisibility(View.VISIBLE);
                    mMySongsListView.setVisibility(View.GONE);*/
                    Toast.makeText(getActivity(), "Server error .. Please try again after some time", Toast.LENGTH_SHORT).show();
                }


            });
        } else {

        }
    }


    public void uploadAvatar() {
        getPath = "";
        if (!Utility.hasPermissions(getActivity(), PERMISSIONS)) {
            ActivityCompat.requestPermissions(getActivity(), PERMISSIONS, PERMISSION_ALL);
        } else {
            PickImageDialog.build(new PickSetup())
                    .setOnPickResult(new IPickResult() {
                        @Override
                        public void onPickResult(PickResult r) {

                            try {
                                imgProfile.setImageURI(null);

                                //Setting the real returned image.
                                imgProfile.setImageURI(r.getUri());

                                //PickImageDialog.build().dismiss();
                                //If you want the Bitmap.
                                imgProfile.setImageBitmap(r.getBitmap());
                                imgProfile.setVisibility(View.VISIBLE);


                                // bitmapToBase64(r.getBitmap());
                                //getPath(r.getUri());
                                getPath = r.getPath();

//                                sendFile();

                                // videoFilePath = (r.getUri()).getData();
                                //videoFilePath=(r.getUri()).getPath();
//
//                                //PickImageDialog.build().dismiss();
//                                //If you want the Bitmap.
//                                imageView.setImageBitmap(r.getBitmap());
                                // Toast.makeText(AddPostActivity.this, r.getError().getMessage(), Toast.LENGTH_LONG).show();
                            } catch (OutOfMemoryError | Exception e) {
                                e.printStackTrace();
                            }
                        }
                    })
                    .setOnPickCancel(new IPickCancel() {
                        @Override
                        public void onCancelClick() {

                        }
                    }).show(getActivity());
        }
    }


    private void sendFile() {
        File file = null;

        MyApplication myApplication = new MyApplication();
        Utility.getSharedInstance().showProgressDialog(getActivity());

        try {
            file = new File(getPath);
        } catch (Exception e) {

        }

        if (getPath != null && !getPath.equals("")) {
            file = new File(getPath);
            if (file == null)
                file = new File(getPath);
        }

        mFile = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("file", file.getName(), mFile);

        Call<JsonObject> callbackLogin = (Call<JsonObject>) myApplication.getAPIInstance().uploadSignUp(X_API_KEY, fileToUpload, mFile);
        callbackLogin.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                JsonObject responseData = response.body();

                if (responseData != null) {
                    Gson gson = new Gson();
                    GetImageResponse getImageResponse = gson.fromJson(responseData, GetImageResponse.class);
                    GetImageResponseData getImageResponseData = getImageResponse.getUploadData();

                    imgURL = Uri.encode(getImageResponseData.getFileName());
                    getPath="";

                    updateProfile();

                    Utility.getSharedInstance().dismissProgressDialog();
                } else {
                    Utility.getSharedInstance().dismissProgressDialog();
                    Toast.makeText(getActivity(), "Failed to attach image", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Utility.getSharedInstance().dismissProgressDialog();
                Toast.makeText(getActivity(), "Excepion Error", Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void updateProfile() {

        MyApplication myApplication = new MyApplication();


        if (Utility.getSharedInstance().isConnectedToInternet(getActivity())) {

            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");

            // id="1";
            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");

            RequestUpdateProfile requestGetMyPlayList = new RequestUpdateProfile();

            requestGetMyPlayList.setId(id);
            requestGetMyPlayList.setxAPIKEY(X_API_KEY);
            requestGetMyPlayList.setUsername(medtProfileName.getText().toString());
            requestGetMyPlayList.setProfilePhoto(imgURL);
            requestGetMyPlayList.setCity(strCityId);
            requestGetMyPlayList.setCountryId(strCountryId);
            requestGetMyPlayList.setStateId(strStateId);
            requestGetMyPlayList.setSanyasName(strSanyasName);
            requestGetMyPlayList.setPhoneNumber(strPhoneNumber);
            requestGetMyPlayList.setEmail(strEmail);



            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().updateProfileImage(requestGetMyPlayList);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());


                    if (responseData != null) {


                        if (responseData.get("message").getAsInt() == 0) {
                            Utility.getSharedInstance().dismissProgressDialog();


                            Toast.makeText(getActivity(), "Failed to update ", Toast.LENGTH_SHORT).show();

                        } else {
                            Utility.getSharedInstance().dismissProgressDialog();
                            Toast.makeText(getActivity(), "Profile updated ", Toast.LENGTH_SHORT).show();
                            SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_PROFILE_PHOTO, imgURL);
                        }


                    } else {
                        Utility.getSharedInstance().dismissProgressDialog();
                    }


                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                    Utility.getSharedInstance().dismissProgressDialog();


                }


            });
        } else {
            Utility.getSharedInstance().dismissProgressDialog();

        }
    }


    public void CropImage() {
        CropImage.activity()
                .start(getContext(), this);
    }


    /**
     * Start pick image activity with chooser.
     */
    public void onSelectImageClick(View view) {
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setActivityTitle("My Crop")
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .setCropMenuCropButtonTitle("Done")
                .setRequestedSize(400, 400)
               // .setCropMenuCropButtonIcon(R.drawable.ic_action_search)
                .start(getContext(), this);
    }


    /*@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }*/


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                imgProfile.setImageURI(result.getUri());

                String strImagePath=result.getUri().toString();



                getPath=result.getUri().toString();

             /*
// or (prefer using uri for performance and better user experience)
                imgProfile.setImageBitmap(result.getBitmap());

                imgProfile.setVisibility(View.VISIBLE);
*/


                File f = new File(getActivity().getApplicationContext().getFilesDir(), "profilepic.png");
//Convert bitmap to byte array
                Bitmap bitmap = ((BitmapDrawable)imgProfile.getDrawable()).getBitmap();
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 0 , bos);
                byte[] bitmapdata = bos.toByteArray();

//write the bytes in file

                try {
                    FileOutputStream fos = new FileOutputStream(f);
                    fos.write(bitmapdata);
                    fos.flush();
                    fos.close();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }


                // bitmapToBase64(r.getBitmap());
                getPath= f.getAbsolutePath();
              //  getPath = result.getPath();



                sendFile();


                Toast.makeText(getContext(), "Updating profile pic please wait!!", Toast.LENGTH_LONG).show();
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(getContext(), "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
            }
        }
    }


    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.txtprofileName:

                String profileName = mTxtProfileName.getText().toString().trim();

                mTxtProfileName.setVisibility(View.GONE);
                medtProfileName.setVisibility(View.VISIBLE);
                medtProfileName.setText(profileName);

                break;
        }
    }

    }