package com.music.nyasaa.fragments.terms_and_conditions;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.music.nyasaa.R;
import com.music.nyasaa.utility.Utility;

//import com.github.barteksc.pdfviewer.PDFView;

public class ActivityTermsAndConditions extends AppCompatActivity implements View.OnClickListener {

    private static String URL = "file:///android_asset/index.html";
    private TextView mTxtMember, mTxtRequest, mTxtPost;
    private LinearLayout mLayMembers, mLayRequest, mLayPost;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_terms_and_conditions);
        initView();
        firstColor();
    }

    private void initView() {

        mLayMembers = (LinearLayout) findViewById(R.id.layMember);
        mLayRequest = (LinearLayout) findViewById(R.id.layRequest);
        mLayPost = (LinearLayout) findViewById(R.id.layPost);
        mTxtMember = (TextView) findViewById(R.id.txtMembers);
        mTxtRequest = (TextView) findViewById(R.id.txtRequest);
        mTxtPost = (TextView) findViewById(R.id.txtPost);

        mLayRequest.setOnClickListener(this);
        mLayMembers.setOnClickListener(this);
        mLayPost.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {

        int id = view.getId();

        switch (id) {

            case R.id.layMember:
                termsCond();
                mTxtRequest.setTextColor(getResources().getColor(R.color.white));
                mTxtMember.setTextColor(getResources().getColor(R.color.black));
                mTxtPost.setTextColor(getResources().getColor(R.color.white));
                mLayPost.setBackgroundColor(getResources().getColor(R.color.darker_grey));
                mLayMembers.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                mLayRequest.setBackgroundColor(getResources().getColor(R.color.darker_grey));
                break;
            case R.id.layRequest:
                copyRight();
                mTxtRequest.setTextColor(getResources().getColor(R.color.black));
                mTxtMember.setTextColor(getResources().getColor(R.color.white));
                mTxtPost.setTextColor(getResources().getColor(R.color.white));
                mLayPost.setBackgroundColor(getResources().getColor(R.color.darker_grey));
                mLayMembers.setBackgroundColor(getResources().getColor(R.color.darker_grey));
                mLayRequest.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                break;

            case R.id.layPost:
                copyRight();
                mTxtRequest.setTextColor(getResources().getColor(R.color.white));
                mTxtMember.setTextColor(getResources().getColor(R.color.white));
                mTxtPost.setTextColor(getResources().getColor(R.color.black));
                mLayMembers.setBackgroundColor(getResources().getColor(R.color.darker_grey));
                mLayRequest.setBackgroundColor(getResources().getColor(R.color.darker_grey));
                mLayPost.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                break;


            default:

                break;

        }

    }


    private void firstColor() {
        mTxtRequest.setTextColor(getResources().getColor(R.color.white));
        mTxtMember.setTextColor(getResources().getColor(R.color.black));

        mTxtPost.setTextColor(getResources().getColor(R.color.white));
        mLayPost.setBackgroundColor(getResources().getColor(R.color.darker_grey));
        mLayMembers.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        mLayRequest.setBackgroundColor(getResources().getColor(R.color.darker_grey));
        termsCond();

    }

    private void termsCond() {
        URL = "file:///android_asset/terms.html";
        WebView mWebview = (WebView) findViewById(R.id.webView);
        mWebview.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);

        mWebview.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view, String url) {
                Utility.getSharedInstance().dismissProgressDialog();
            }
        });
        //String url = "http://docs.google.com/gview?embedded=true&url=" + URL;
        mWebview.loadUrl(URL);
        Utility.getSharedInstance().showProgressDialog(ActivityTermsAndConditions.this);
    }

    private void copyRight() {
        URL = "file:///android_asset/index.html";
        WebView mWebview = (WebView) findViewById(R.id.webView);
        mWebview.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);

        mWebview.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view, String url) {
                Utility.getSharedInstance().dismissProgressDialog();
            }
        });
        //String url = "http://docs.google.com/gview?embedded=true&url=" + URL;
        mWebview.loadUrl(URL);
        Utility.getSharedInstance().showProgressDialog(ActivityTermsAndConditions.this);
    }
}

