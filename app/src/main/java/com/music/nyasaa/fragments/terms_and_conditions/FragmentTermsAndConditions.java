package com.music.nyasaa.fragments.terms_and_conditions;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

//import com.github.barteksc.pdfviewer.PDFView;
import com.google.gson.JsonObject;
import com.music.nyasaa.R;
import com.music.nyasaa.controller.MyApplication;
import com.music.nyasaa.utility.SharedPreferenceManager;
import com.music.nyasaa.utility.Utility;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.music.nyasaa.retrofit.Config.X_API_KEY;
import static com.music.nyasaa.retrofit.Config.clientService;

public class FragmentTermsAndConditions extends Fragment {

    private View myView;
    private static final String ARG_KEY_NUMBER = "tab_number";

    private static Context context;

    private TextView mTxtTerms,mTxtMain;
    private Call<JsonObject> callbackLogin;


    String about="";



    public static FragmentTermsAndConditions newInstance(int number) {
        Bundle args = new Bundle();
        args.putInt(ARG_KEY_NUMBER, number);
        FragmentTermsAndConditions frag = new FragmentTermsAndConditions();
        frag.setArguments(args);
        return frag;
    }


    public FragmentTermsAndConditions() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        myView = inflater.inflate(R.layout.layout_terms, container, false);
        initView(myView);
        about= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_ABOUT,"");
        getPolicry();
        return myView;
    }


    private void initView(View myView) {


        about= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_ABOUT,"");

        mTxtTerms=myView.findViewById(R.id.txtData);
        mTxtMain=myView.findViewById(R.id.txtMain);




        if(about.equalsIgnoreCase("0"))
            mTxtMain.setText("About");
        else
            mTxtMain.setText("Contact us");

    }


    /// get all Images


    public void getPolicry() {

        MyApplication myApplication = new MyApplication();




        if (Utility.getSharedInstance().isConnectedToInternet(getActivity())) {

            Utility.getSharedInstance().showProgressDialog(getActivity());
            if(about.equalsIgnoreCase("0"))
            {
                callbackLogin = myApplication.getAPIInstance().about(clientService,X_API_KEY);
            }else {
               callbackLogin = myApplication.getAPIInstance().supportcontact(clientService,X_API_KEY);
            }

            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    // JsonObject responseData = response.body();
                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());

                    if (responseData != null) {


                        if(responseData.get("status").getAsInt()==200)
                        {

                            String s = responseData.get("message").getAsString();
                            s = s.replace("<p>", "");
                            s = s.replace("</p>", "");
                            mTxtTerms.setText("" + s);

                        }



                        Utility.getSharedInstance().dismissProgressDialog();

//                        Toast.makeText(getActivity(), "Please scroll from LEFT TO RIGHT", Toast.LENGTH_SHORT).show();


                    } else {
                        Utility.getSharedInstance().dismissProgressDialog();
                    }

                }
                // Utility.getSharedInstance().Snackbar(toolbarLayout, "Password Changed Sucessfully");


                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Utility.getSharedInstance().dismissProgressDialog();

                }
            });
        } else {
            Utility.getSharedInstance().dismissProgressDialog();

            Toast.makeText(getActivity(), "No internet connection available", Toast.LENGTH_SHORT).show();
        }
    }
}

