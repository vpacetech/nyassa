package com.music.nyasaa.fragments.discourses;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.music.nyasaa.R;
import com.music.nyasaa.adapter.DisCourserVideoAdapter;
import com.music.nyasaa.controller.MyApplication;
import com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity;
import com.music.nyasaa.front_end_layer.activities.download_in_appscope_enc.DataSQLHelper;
import com.music.nyasaa.front_end_layer.activities.download_in_appscope_enc.DownloadService;
import com.music.nyasaa.models.RequestModel.SearchMedia.RequestSearchMedia;
import com.music.nyasaa.models.ResponseModel.GetAllVideos.GetVideoResponse;
import com.music.nyasaa.models.ResponseModel.GetAllVideos.GetVideoResponseData;
import com.music.nyasaa.models.SongDetail;
import com.music.nyasaa.retrofit.Config;
import com.music.nyasaa.utility.RecyclerItemClickListener;
import com.music.nyasaa.utility.SharedPreferenceManager;
import com.music.nyasaa.utility.Utility;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity.PostionOfPlay;
import static com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity.getEventResponseDataResponQue;
import static com.music.nyasaa.front_end_layer.activities.download_in_appscope_enc.Table_Details.TABLE_NAME;
import static com.music.nyasaa.retrofit.Config.BASE_VIDEO_URL;
import static com.music.nyasaa.retrofit.Config.X_API_KEY;

public class FragmentVideoDiscoureses extends Fragment {

    private View myView;
    private static final String ARG_KEY_NUMBER = "tab_number";
    private static Context context;
    public static FragmentVideoDiscoureses fragmentVideoDiscoureses;
    public static List<GetVideoResponseData> getEventResponseDataRespon = new ArrayList<>();
    private ImageView imgDisc;
    private RecyclerView mMySongsListView;
    private TextView mtxtDisCousName,mTxtWarn;
    TextView DownloadAll;

    int currentPage =0;

    private DisCourserVideoAdapter disCourserVideoAdapter;
    private TextView mTxtLang;

    private ImageView imgOpenDrawer;
    private TextView txtDescription;

    public static FragmentVideoDiscoureses newInstance(int number) {
        Bundle args = new Bundle();
        args.putInt(ARG_KEY_NUMBER, number);
        FragmentVideoDiscoureses frag = new FragmentVideoDiscoureses();
        frag.setArguments(args);
        return frag;
    }


    public FragmentVideoDiscoureses() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        myView = inflater.inflate(R.layout.fragment_discoures, container, false);
        initView(myView);

        fragmentVideoDiscoureses = FragmentVideoDiscoureses.this;
        return myView;
    }


    private void initView(View myView) {

//        myView.getRootView().setBackgroundColor(Color.BLACK);
        myView.getRootView().setBackgroundColor(getResources().getColor(R.color.orBLACK));
        mTxtLang = (TextView) myView.findViewById(R.id.txtLang);
        mMySongsListView = (RecyclerView) myView.findViewById(R.id.FragmentAudioCommon);
        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        mMySongsListView.setLayoutManager(linearLayoutManager);
        mMySongsListView.setItemAnimator(new DefaultItemAnimator());

        imgDisc=(ImageView)myView.findViewById(R.id.disImg);
        mtxtDisCousName=(TextView)myView.findViewById(R.id.txtDiscousename);
        mTxtWarn=(TextView)myView.findViewById(R.id.txtWarn);

        DownloadAll=(TextView)myView.findViewById(R.id.DownloadAll);

        txtDescription=(TextView)myView.findViewById(R.id.txtDescription);

        txtDescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DownloadAllVDO();
            }
        });

        DownloadAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DownloadAllVDO();
            }
        });

        currentPage = 0;

        thread();
        imgOpenDrawer =(ImageView)myView.findViewById(R.id.imgOpenDrawer);



        imgOpenDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((DashBoardActivity)getActivity()).openDrawerTab();
            }
        });



//        mGridAlbumview =(RecyclerView)myView.findViewById(R.id.listViewAlbum);
    }

    private void DownloadAllVDO() {
        if(getEventResponseDataRespon!=null){
            if(getEventResponseDataRespon.size()>0)
            {
               // callQuecFunction

                getEventResponseDataResponQue.clear();
                for(int i=0;i<getEventResponseDataRespon.size();i++)
                {
                    getEventResponseDataResponQue.add(getEventResponseDataRespon.get(i));
                }
               // getEventResponseDataResponQue=getEventResponseDataRespon;
                downloadVDOSerise(getEventResponseDataResponQue.get(0));
            }
        }


    }


    /// First Time Screen


    public void getSearchedVideos(String searchText) {

        String lang = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_LANG_ID, "none");
        /*if (lang.equalsIgnoreCase("1")) {
            mTxtLang.setText("English");

        } else {
            mTxtLang.setText("Hindi");
        }*/


        MyApplication myApplication = new MyApplication();

        //Utility.getSharedInstance().showProgressDialog(getActivity());


        RequestSearchMedia requestSearchMedia = new RequestSearchMedia();

        requestSearchMedia.setLimit("10");
        requestSearchMedia.setQuedisc(1);
        requestSearchMedia.setShould_orderby_name(false);
        if (searchText.length() > 0) {
//            requestSearchMedia.setAlbum_id(SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_LANG_ID,"none"));
        } else {
            requestSearchMedia.setAlbum_id(SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_LANG_ID, "none"));
        }

       // requestSearchMedia.setMedia_type_id(SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT_ORIGINAL, "none"));
        requestSearchMedia.setOffset("0");
        requestSearchMedia.setSearchterm(searchText);


        if (Utility.getSharedInstance().isConnectedToInternet(getContext())) {

//            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");
//            String cat = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT, "1");
//            // id="1";
//            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");

            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().searchVideoQA(X_API_KEY, requestSearchMedia);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());

                    if (responseData.get("error").getAsInt() == 0) {
                        // Config.getPlanDAO = new GetPlanDAO();
                        Gson gson = new Gson();

                        //Utility.getSharedInstance().dismissProgressDialog();


                        Type listType = new TypeToken<List<SongDetail>>() {
                        }.getType();
                        if (responseData.get("respon") != null) {

                            JsonArray jsonArray = responseData.get("respon").getAsJsonArray();


                            // JsonObject jsonObject=responseData.getAsJsonObject("respon");

                            if (jsonArray.size() > 0) {


                                //connectionAdapter = new CommunityAdapter(getActivity(), community_dao);
                                // mMySongsListView.setAdapter(connectionAdapter);

                                GetVideoResponse getEventResponseData = gson.fromJson(responseData, GetVideoResponse.class);

                                getEventResponseDataRespon = getEventResponseData.getRespon();

                                getVideoList(getEventResponseDataRespon);


                                mTxtWarn.setVisibility(View.GONE);
                                mMySongsListView.setVisibility(View.VISIBLE);

//                                swipyRefreshLayoutDirection.setRefreshing(false);

                            } else {
//                                swipyRefreshLayoutDirection.setRefreshing(false);
                                mTxtWarn.setVisibility(View.VISIBLE);
                                mMySongsListView.setVisibility(View.GONE);
                                //Utility.getSharedInstance().dismissProgressDialog();
                            }


                        }


                        // Utility.getSharedInstance().Snackbar(toolbarLayout, "Password Changed Sucessfully");


                    } else {
//                        swipyRefreshLayoutDirection.setRefreshing(false);
                        //Utility.getSharedInstance().dismissProgressDialog();
                        mTxtWarn.setVisibility(View.VISIBLE);
                        mMySongsListView.setVisibility(View.GONE);
//                        videoView.setVisibility(View.GONE);

                    }

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                    //Utility.getSharedInstance().dismissProgressDialog();
                    mTxtWarn.setVisibility(View.VISIBLE);
                    mMySongsListView.setVisibility(View.GONE);

                }


            });
        } else {

        }
    }

/// FIRST TIME CALL



    public void videoDataNew(String url) {

//        });
//
    }

    private void getVideoList(final List<GetVideoResponseData> getVideoResponseDataList) {

        disCourserVideoAdapter = new DisCourserVideoAdapter(getActivity(), getVideoResponseDataList);
        mMySongsListView.setAdapter(disCourserVideoAdapter);

        if (getVideoResponseDataList.size() > 0) {
//            videoView.reset();
//            videoDataNew(BASE_VIDEO_URL + getVideoResponseDataList.get(0).getFile());
        } else {

//            videoView.stop();
//            videoDataNew("");
        }

        // mMySongsListView


        mMySongsListView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, final int position) {


                        //  videoDataPlayer(getVideoResponseDataList, position);
                        try {


                            //videoDataNew(BASE_VIDEO_URL + getVideoResponseDataList.get(position).getFile());
                        } catch (Exception e) {

                        }


                    }
                }));

    }
//




    public void firstTimeCall() {

       /* if (lang.equalsIgnoreCase("1")) {
            mTxtLang.setText("English");

        } else {
            mTxtLang.setText("Hindi");
        }*/

        MyApplication myApplication = new MyApplication();

        //Utility.getSharedInstance().showProgressDialog(getActivity());
        String lang = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_LANG_ID, "1");

        RequestSearchMedia requestSearchMedia = new RequestSearchMedia();

        requestSearchMedia.setLimit("200");
        requestSearchMedia.setAlbum_id(lang);
        requestSearchMedia.setOffset("0");
        requestSearchMedia.setQuedisc(2);
        requestSearchMedia.setShould_orderby_name(false);
//        requestSearchMedia.setMedia_type_id(SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT_ORIGINAL,"none"));

        requestSearchMedia.setSearchterm(SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_DISCOURSE_AUDIO));
        String cat= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT,"0");
//        String cat= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT,"0");
//        requestSearchMedia.setCategory(cat);

        if (Utility.getSharedInstance().isConnectedToInternet(getContext())) {

//            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");
//            String cat = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT, "1");
//            // id="1";
//            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");

            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().searchVideoQA(X_API_KEY, requestSearchMedia);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());

                    if (responseData.get("error").getAsInt() == 0) {
                        // Config.getPlanDAO = new GetPlanDAO();
                        Gson gson = new Gson();

                        //Utility.getSharedInstance().dismissProgressDialog();


                        Type listType = new TypeToken<List<SongDetail>>() {
                        }.getType();
                        if (responseData.get("respon") != null) {

                            JsonArray jsonArray = responseData.get("respon").getAsJsonArray();


                            // JsonObject jsonObject=responseData.getAsJsonObject("respon");

                            if (jsonArray.size() > 0) {


                                //connectionAdapter = new CommunityAdapter(getActivity(), community_dao);
                                // mMySongsListView.setAdapter(connectionAdapter);

                                GetVideoResponse getEventResponseData = gson.fromJson(responseData, GetVideoResponse.class);

                                getEventResponseDataRespon = getEventResponseData.getRespon();

                                getVideoList(getEventResponseDataRespon);

                                mtxtDisCousName.setText(getEventResponseDataRespon.get(0).getName());

                                mTxtWarn.setVisibility(View.GONE);
                                mMySongsListView.setVisibility(View.VISIBLE);


                                Picasso.with(context)
                                        .load(Config.BASE_VIDEO_URL_COVER+""+ Uri.encode(getEventResponseDataRespon.get(0).getCover()))
                                        .placeholder(R.drawable.default_music)
                                        .error(R.drawable.default_music)
                                        .into(imgDisc);

//                                swipyRefreshLayoutDirection.setRefreshing(false);

                            } else {

                                Toast.makeText(DashBoardActivity.context, "No episodes found for selected language", Toast.LENGTH_SHORT).show();
//                                swipyRefreshLayoutDirection.setRefreshing(false);
                                mTxtWarn.setVisibility(View.VISIBLE);
                                mMySongsListView.setVisibility(View.GONE);
                                //Utility.getSharedInstance().dismissProgressDialog();
                            }


                        }


                        // Utility.getSharedInstance().Snackbar(toolbarLayout, "Password Changed Sucessfully");


                    } else {
//                        swipyRefreshLayoutDirection.setRefreshing(false);
                        //Utility.getSharedInstance().dismissProgressDialog();
                        mTxtWarn.setVisibility(View.VISIBLE);
                        mMySongsListView.setVisibility(View.GONE);
//                        videoView.setVisibility(View.GONE);

                    }

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                    //Utility.getSharedInstance().dismissProgressDialog();
                    mTxtWarn.setVisibility(View.VISIBLE);
                    mMySongsListView.setVisibility(View.GONE);

                }


            });
        } else {

        }
    }





    public void playPoase(int postion) {
        PostionOfPlay = postion;
        try {
            if (getEventResponseDataRespon != null)
                for (int i = 0; i < getEventResponseDataRespon.size(); i++) {
                    if (i != postion)
                        getEventResponseDataRespon.get(i).setPlayStop(false);

                }
            if (getEventResponseDataRespon.get(postion).getPlayStop()) {
                getEventResponseDataRespon.get(postion).setPlayStop(false);

//            videoView.reset();
            } else {

                getEventResponseDataRespon.get(postion).setPlayStop(true);
                // videoView.start();
            }

            disCourserVideoAdapter.notifyDataSetChanged();
        } catch (Exception e){
            e.printStackTrace();
        }

    }

    public void downloadVDOSerise(GetVideoResponseData getVideoResponseData) {

          if(getEventResponseDataResponQue!=null)
            if(getEventResponseDataResponQue.size()>0) {
                if (checkAvaiblity(getVideoResponseData)) {
                    Toast.makeText(getActivity(), "Saving Video Offline...", Toast.LENGTH_SHORT).show();
                    String file_Name = Uri.encode(getVideoResponseData.getFile());
                    Intent intent = new Intent(getActivity(), DownloadService.class);
                    String URL = BASE_VIDEO_URL + Uri.encode(getVideoResponseData.getFile());

                    intent.putExtra("id", getVideoResponseData.getId() + "VDO"); // ADO Or VDO
                    intent.putExtra("File_Name", file_Name);

//            intent.putExtra("File_Name", file_Name.replace(" ", "%20"));
                    intent.putExtra("Type", "mp3"); // ADO Or VDO
                    intent.putExtra("URL", URL);

                    intent.putExtra("name", getVideoResponseData.getName());
                    intent.putExtra("Image", getVideoResponseData.getCover());
                    intent.putExtra("Image_URL", Config.BASE_VIDEO_URL_COVER + Uri.encode( getVideoResponseData.getCover()));
                    intent.putExtra("Image", getVideoResponseData.getCover());

                    getActivity().startService(intent);
                } else {

                    if (getEventResponseDataResponQue != null)
                        if (getEventResponseDataResponQue.size() > 0) {
                            getEventResponseDataResponQue.remove(0);
                        }
                    if (getEventResponseDataResponQue != null) {
                        if (getEventResponseDataResponQue.size() > 0) {
                            downloadVDOSerise(getEventResponseDataResponQue.get(0));
                        }
                    }


                }

            }
    }

    public void downloadVDO(GetVideoResponseData getVideoResponseData) {


        if (checkAvaiblity(getVideoResponseData)) {
            Toast.makeText(getActivity(), "Saving Video Offline...", Toast.LENGTH_SHORT).show();
            String file_Name = Uri.encode(getVideoResponseData.getFile());
            Intent intent = new Intent(getActivity(), DownloadService.class);
            String URL = BASE_VIDEO_URL + Uri.encode(getVideoResponseData.getFile());

            intent.putExtra("id",getVideoResponseData.getId()+"VDO"); // ADO Or VDO
            intent.putExtra("File_Name", file_Name);

//            intent.putExtra("File_Name", file_Name.replace(" ", "%20"));
            intent.putExtra("Type", "mp3"); // ADO Or VDO
            intent.putExtra("URL", URL);

            intent.putExtra("name", getVideoResponseData.getName());
            intent.putExtra("Image",getVideoResponseData.getCover());
            intent.putExtra("Image_URL", Config.BASE_VIDEO_URL_COVER +Uri.encode( getVideoResponseData.getCover()));
            intent.putExtra("Image", getVideoResponseData.getCover());

            getActivity().startService(intent);
        } else {
            Toast.makeText(getActivity(), "Already Downloaded", Toast.LENGTH_SHORT).show();

        }


    }

    private boolean checkAvaiblity(GetVideoResponseData forDOWNLOadSongDetail) {
        boolean notAvaible = true;
        try {


            DataSQLHelper dataSQLHelper = new DataSQLHelper(context.getApplicationContext());
            SQLiteDatabase sqLiteDatabase;
            sqLiteDatabase = dataSQLHelper.getWritableDatabase();
            String getJson = "";

            String query = "SELECT * FROM " + TABLE_NAME + " WHERE id='" + forDOWNLOadSongDetail.getId()+"VDO" + "'";
            Cursor cursor = sqLiteDatabase.rawQuery(query, null);

            if (cursor.getCount() > 0) {
                notAvaible = false;

            } else {
                notAvaible = true;
            }
        } catch (Exception e) {

        }

        return notAvaible;
    }


    private void thread()
    {




        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {

                firstTimeCall();


            }
        });
    }
}