package com.music.nyasaa.fragments.events;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.music.nyasaa.R;
import com.music.nyasaa.adapter.EventAdapter;
import com.music.nyasaa.controller.MyApplication;
import com.music.nyasaa.front_end_layer.activities.add_events.AddEventActivity;
import com.music.nyasaa.models.GetEventsModel;
import com.music.nyasaa.utility.SharedPreferenceManager;
import com.music.nyasaa.utility.Utility;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.music.nyasaa.retrofit.Config.X_API_KEY;
import static com.music.nyasaa.retrofit.Config.clientService;

public class FragmentEvents extends Fragment {

    private View myView;
    private static final String ARG_KEY_NUMBER = "tab_number";

    private static Context context;

    private Button btnAddEvent;


    private RecyclerView mMySongsListView;

    private SwipyRefreshLayout swipyRefreshLayoutDirection;
    private String userId;

    private TextView mTxtWarn;
    private LinearLayout layoutEvent;
    private EditText mEditEventSearch;
    private EventAdapter connectionAdapter;


    public static FragmentEvents newInstance(int number) {
        Bundle args = new Bundle();
        args.putInt(ARG_KEY_NUMBER, number);
        FragmentEvents frag = new FragmentEvents();
        frag.setArguments(args);
        return frag;
    }


    public FragmentEvents() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        myView = inflater.inflate(R.layout.fragment_events, container, false);
        initView(myView);
        return myView;
    }


    private void initView(View myView) {

        userId = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");
//        myView.getRootView().setBackgroundColor(Color.BLACK);
        myView.getRootView().setBackgroundColor(getResources().getColor(R.color.orBLACK));
        mMySongsListView = (RecyclerView) myView.findViewById(R.id.listViewAlbum);
        btnAddEvent = myView.findViewById(R.id.btnAddEvent);
        layoutEvent = myView.findViewById(R.id.layoutEvent);
        mEditEventSearch = (EditText) myView.findViewById(R.id.editSearch);
        //RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
//        mMySongsListView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL, true));
//        mMySongsListView.setItemAnimator(new DefaultItemAnimator());


        mTxtWarn = (TextView) myView.findViewById(R.id.txtWarn);
        mTxtWarn.setVisibility(View.GONE);

        swipyRefreshLayoutDirection = (SwipyRefreshLayout) myView.findViewById(R.id.swipe_refresh_layout);
        swipyRefreshLayoutDirection.setDirection(SwipyRefreshLayoutDirection.BOTH);

        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        mMySongsListView.setLayoutManager(linearLayoutManager);
        mMySongsListView.setItemAnimator(new DefaultItemAnimator());

//        getAllEvents();

        swipyRefreshLayoutDirection.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                getAllEvents();
            }
        });

        btnAddEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), AddEventActivity.class);
                startActivity(intent);
            }
        });

        // listening to search query text change

        mEditEventSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

               // connectionAdapter.getFilter().filter(mEditEventSearch.getText().toString().trim());


            }

            @Override
            public void afterTextChanged(Editable s) {
               // connectionAdapter.getFilt//er().filter(mEditEventSearch.getText().toString().trim());

            }
        });


    }


    public void getAllEvents() {

        MyApplication myApplication = new MyApplication();


        if (Utility.getSharedInstance().isConnectedToInternet(getContext())) {
            Utility.getSharedInstance().showProgressDialog(getActivity());
            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");

            // id="1";
            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");
//            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().getAllEvents( X_API_KEY,"10","0");
            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().getAllEventsByUSerID(clientService, X_API_KEY);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();

                    if (responseData != null) {
                        Utility.getSharedInstance().dismissProgressDialog();

                        Gson gson = new Gson();

                        List<GetEventsModel> getEvents = null;
                        getEvents = (List<GetEventsModel>) gson.fromJson(responseData.get("respon"), new TypeToken<List<GetEventsModel>>() {
                        }.getType());
                        if (getEvents != null) {
                            EventAdapter eventAdapter = new EventAdapter(getActivity(), getEvents);
                            mMySongsListView.setAdapter(eventAdapter);


                            mMySongsListView.setVisibility(View.VISIBLE);
                            mTxtWarn.setVisibility(View.GONE);
                        }
                    } else {
                        Utility.getSharedInstance().dismissProgressDialog();
                    }
                    // Log.e("Response::", responseData.toString());


                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                    Utility.getSharedInstance().dismissProgressDialog();
                    mTxtWarn.setVisibility(View.VISIBLE);
                    mMySongsListView.setVisibility(View.GONE);
                    swipyRefreshLayoutDirection.setRefreshing(false);

                }


            });
        } else {
            swipyRefreshLayoutDirection.setRefreshing(false);
            Utility.getSharedInstance().dismissProgressDialog();
        }
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.menu_add_event, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.addEvent) {
            // startActivity(new Intent(this, FragmentSearch.class));
            // overridePendingTransition(R.anim.slide_in_right_speed, R.anim.slide_out_left_speed);
            return true;

        } else if (item.getItemId() == android.R.id.home) {
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        getAllEvents();
    }
}