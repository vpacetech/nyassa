package com.music.nyasaa.fragments;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;


import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;

import com.github.pedrovgs.DraggablePanel;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.music.nyasaa.R;

import com.music.nyasaa.adapter.DashboardAudioAdapter;
import com.music.nyasaa.adapter.DashboardAudioDisAdapter;
import com.music.nyasaa.adapter.DashboardMusicAdapter;
import com.music.nyasaa.adapter.DashboardInterviewAdapter;
import com.music.nyasaa.adapter.DashboardMeditationAdapter;
import com.music.nyasaa.controller.MyApplication;
import com.music.nyasaa.dbhandler.MusicSQLiteHelper;
import com.music.nyasaa.fragments.fragment_common.FragmentAudioCommon;
import com.music.nyasaa.fragments.interviews.FragmentInterview;
import com.music.nyasaa.fragments.meditation.FragmentMeditation;
import com.music.nyasaa.fragments.music.FragmentMusic;
import com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity;
import com.music.nyasaa.models.AlbumModel;
import com.music.nyasaa.models.LanguageModel;
import com.music.nyasaa.models.RequestModel.SearchMedia.RequestSearchMedia;
import com.music.nyasaa.models.SectionDataModel;
import com.music.nyasaa.models.Silder_DAO;
import com.music.nyasaa.models.SongDetail;
import com.music.nyasaa.phonemidea.PhoneMediaControl;
import com.music.nyasaa.retrofit.Config;
import com.music.nyasaa.utility.SharedPreferenceManager;
import com.music.nyasaa.utility.TbsindUtil;
import com.music.nyasaa.utility.Utility;
import com.music.nyasaa.widgets.CustomSliderView;


import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity.PostionOfPlay;
import static com.music.nyasaa.retrofit.Config.clientService;
import static com.music.nyasaa.retrofit.Config.X_API_KEY;

public class FragmentDashBoard extends Fragment implements View.OnTouchListener, BaseSliderView.OnSliderClickListener {
    private static final String ARG_KEY_NUMBER = "tab_number";


    DashboardAudioAdapter dashboardAudioAdapter;
    DashboardAudioDisAdapter dashboardAudioDisAdapter;
    DashboardMusicAdapter dashboardMusicAdapter;
    DashboardInterviewAdapter dashboardInterviewAdapter;
    DashboardMeditationAdapter dashboardMeditationAdapter;
    String TAG = "AllSongsFragment";
    ArrayList<SectionDataModel> allSampleData;
    ArrayList<SongDetail> longADOallSampleData;
    ArrayList<SongDetail> shortVDOallSampleData;
    public static ArrayList<SongDetail> mSongss, mSongsInt, mSongsMedi;
    public static FragmentDashBoard fragmentDashBoards;
    FragmentManager fragmentManager;
    ArrayList<SongDetail> longVDOallSampleData;
    private static final String BACK_STACK_ROOT_TAG = "root_fragment";
    //  ImageView btnNext;
    // ImageView btnPrevious;

    int i = 0;
    ArrayList<LanguageModel> languageList;
    PhoneMediaControl mPhoneMediaControl;
    public static ArrayList<SongDetail> mSongsAudio, mSongsAudioDis;
    public static YouTubePlayer player;
    TextView fullscreen_button;
    ProgressBar progressLoadList;
    private RecyclerView recycler_songslist;
    private TbsindUtil util;
    List<String> videoList;
    TextView lblError;
    TextView lblLatest;
    DraggablePanel draggablePanel;

    private ImageView mImgAd;
    private TextView mTxtAdText;
    private Button mButAds;
    public static ScrollView mScrollView;
    private List<Silder_DAO> adsResponseResultObject;

    private static SongDetail mDeatilGOTDATA;
    private static DashBoardActivity musicPlayerBaseActivit;

    private boolean dragFlag = true;
    private FrameLayout mConatainer;

    private SliderLayout sliderLayout;
    MyApplication myApplication;

    private RelativeLayout mShowYouLayout;

    private ProgressBar loader, loaderAudioDis, loaderMusic, loaderMeditation, loaderInterview;
    private TextView mTxtMore, mTxtMoreAudioDis, mTxtMoreMediation, mTxtMoreInterView, mTxtMoreMusic;
    private LinearLayout mAudioLayout, mAudioDisLayout, mMusicLayout, mMeditationLayout, mInterviewLayout;
    private RecyclerView single_song_list, single_ado_list_long, single_audioDis_list, single_song_VDO_list, single_VDO_list_long;

    DashBoardActivity dashBoardActivity;
    View v = null;

    public static FragmentDashBoard newInstance(int number) {
        Bundle args = new Bundle();
        args.putInt(ARG_KEY_NUMBER, number);
        FragmentDashBoard frag = new FragmentDashBoard();
        frag.setArguments(args);
        return frag;
    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        fragmentDashBoards = this;
        myApplication = new MyApplication();
        v = inflater.inflate(R.layout.fragment_dashboard, container, false);

        dashBoardActivity = new DashBoardActivity();
//        v.getRootView().setBackgroundColor(Color.BLACK);
//        v.getRootView().setBackgroundResource(R.drawable.black_gradient);
        videoList = new ArrayList<>();
        // btnNext = (ImageView) v.findViewById(R.id.btnNext);
        //btnPrevious = (ImageView) v.findViewById(R.id.btnPrevious);
        fullscreen_button = (TextView) v.findViewById(R.id.fullscreen_button);
        sliderLayout = (SliderLayout) v.findViewById(R.id.slider);
        lblError = (TextView) v.findViewById(R.id.lblError);
        lblLatest = (TextView) v.findViewById(R.id.lblLatest);
        util = new TbsindUtil(getActivity());

        mTxtAdText = (TextView) v.findViewById(R.id.txtAdsDec);
        mButAds = (Button) v.findViewById(R.id.butInstall);
        mImgAd = (ImageView) v.findViewById(R.id.imgAd);

        mConatainer = (FrameLayout) v.findViewById(R.id.container);
        mScrollView = (ScrollView) v.findViewById(R.id.scrollViewDash);
        musicPlayerBaseActivit = new DashBoardActivity();
        draggablePanel = (DraggablePanel) v.findViewById(R.id.draggable_panel);

        mTxtMore = (TextView) v.findViewById(R.id.txtMore);
        mTxtMoreAudioDis = (TextView) v.findViewById(R.id.txtMoreAudioDis);
        mTxtMoreMediation = (TextView) v.findViewById(R.id.txtMoreMeditation);
        mTxtMoreMusic = (TextView) v.findViewById(R.id.txtMoreMusic);
        mTxtMoreInterView = (TextView) v.findViewById(R.id.txtMoreInterview);
        mAudioLayout = v.findViewById(R.id.audioLayout);
        mAudioDisLayout = v.findViewById(R.id.audioDisLayout);
        mMusicLayout = v.findViewById(R.id.musicLayout);
        mMeditationLayout = v.findViewById(R.id.meditationLayout);
        mInterviewLayout = v.findViewById(R.id.interviewLayout);

        SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_MUSIC_TAB, "1");
        SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_INETR_TAB, "1");

        mTxtMoreMediation.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                //  dashBoardActivity.openshorAudio();
                ((DashBoardActivity) getActivity()).hideBadge();
                ((DashBoardActivity) getActivity()).toolbarStatusBar(" Meditation");

                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_TXT_MORE, "more");

                //  getActivity().toolbarStatusBar("Ask O.S.H.O.");

                fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.popBackStack(BACK_STACK_ROOT_TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);

                // Add the new tab fragment
                fragmentManager.beginTransaction()
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .replace(R.id.fragment, FragmentMeditation.newInstance(0), String.valueOf(0))
                        .addToBackStack(BACK_STACK_ROOT_TAG)
                        // .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .commit();
                getActivity().overridePendingTransition(R.anim.left, R.anim.right);


            }
        });


        mTxtMoreMusic.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                //  dashBoardActivity.openshorAudio();
                ((DashBoardActivity) getActivity()).hideBadge();
                fragmentManager = getActivity().getSupportFragmentManager();

                ((DashBoardActivity) getActivity()).toolbarStatusBar(" Music");
                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_TXT_MORE, "more");
                fragmentManager.popBackStack(BACK_STACK_ROOT_TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);

                // Add the new tab fragment
                fragmentManager.beginTransaction()
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .replace(R.id.fragment, FragmentMusic.newInstance(0), String.valueOf(0))
                        .addToBackStack(BACK_STACK_ROOT_TAG)
                        // .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .commit();
                getActivity().overridePendingTransition(R.anim.left, R.anim.right);


            }
        });


        mTxtMoreInterView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_TXT_MORE, "more");
                ((DashBoardActivity) getActivity()).hideBadge();
                //  dashBoardActivity.openshorAudio();
                ((DashBoardActivity) getActivity()).toolbarStatusBar(" Interviews");

                fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.popBackStack(BACK_STACK_ROOT_TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);

                // Add the new tab fragment
                fragmentManager.beginTransaction()
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .replace(R.id.fragment, FragmentInterview.newInstance(0), String.valueOf(0))
                        .addToBackStack(BACK_STACK_ROOT_TAG)
                        // .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .commit();
                getActivity().overridePendingTransition(R.anim.left, R.anim.right);


            }
        });


        mTxtMore.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_DRAWER_TYPE, "1");
                //  dashBoardActivity.openshorAudio();
                ((DashBoardActivity) getActivity()).hideBadge();
                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_TXT_MORE, "more");

//                ((DashBoardActivity) getActivity()).toolbarStatusBar(" Audio");
                ((DashBoardActivity) getActivity()).toolbarStatusBar(" Album");

                fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.popBackStack(BACK_STACK_ROOT_TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);

                // Add the new tab fragment
                fragmentManager.beginTransaction()
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
//                        .replace(R.id.fragment, FragmentMySongs.newInstance(0), String.valueOf(0))
                        .replace(R.id.fragment, FragmentAlbumList.newInstance(0), String.valueOf(0))
                        .addToBackStack(BACK_STACK_ROOT_TAG)
                        // .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .commit();
                getActivity().overridePendingTransition(R.anim.left, R.anim.right);


            }
        });

        mTxtMoreAudioDis.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_DRAWER_TYPE, "1");
                //  dashBoardActivity.openshorAudio();
                ((DashBoardActivity) getActivity()).hideBadge();
                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_TXT_MORE, "more");

                ((DashBoardActivity) getActivity()).toolbarStatusBar(" Audio");
                fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.popBackStack(BACK_STACK_ROOT_TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);

                // Add the new tab fragment
                fragmentManager.beginTransaction()
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .replace(R.id.fragment, FragmentAudioCommon.newInstance(0, "dis"), String.valueOf(0))
                        .addToBackStack(BACK_STACK_ROOT_TAG)
                        // .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .commit();
                getActivity().overridePendingTransition(R.anim.left, R.anim.right);
            }
        });


        getOfferDATA();

        if (TbsindUtil.isNull(util.getLangName())) {
            Log.d("TAG", "value for preferance " + util.getLangName());
            util.saveLang("2", "Hindi");
        }
        languageList = new ArrayList<>();
        progressLoadList = (ProgressBar) v.findViewById(R.id.progressLoadList);
        mShowYouLayout = (RelativeLayout) v.findViewById(R.id.youLayout);
        setupInitialViews(v);
        //setListItemsShortADO("1", "10", "0");
//        setListItemsLongADO("1","10","0");
//        setListItemsShortVDO("1","10","0");
//        setListItemsLongVDO("1","10","0");
        threads();

        //youTubePlayerFragment.setUserVisibleHint(false);


        mShowYouLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dragFlag == true) {
                    initializeDraggablePanel();
                    dragFlag = false;
                } else {
                    draggablePanel.maximize();
                }

            }
        });


        return v;

    }

    public void onDestroy() {
        super.onDestroy();
    }

    public void onPause() {
        super.onPause();
    }

    public void onResume() {
        super.onResume();


    }


    private void setupInitialViews(View inflatreView) {
        mPhoneMediaControl = PhoneMediaControl.getInstance();
        recycler_songslist = (RecyclerView) inflatreView.findViewById(R.id.recycler_allSongs);
        recycler_songslist.setNestedScrollingEnabled(false);
        single_song_list = (RecyclerView) inflatreView.findViewById(R.id.single_song_list);
        loader = (ProgressBar) inflatreView.findViewById(R.id.loader);
        loaderAudioDis = (ProgressBar) inflatreView.findViewById(R.id.loaderAudioDis);
        loaderMusic = (ProgressBar) inflatreView.findViewById(R.id.loaderMusic);

        loaderInterview = (ProgressBar) inflatreView.findViewById(R.id.loaderInterView);
        loaderMeditation = (ProgressBar) inflatreView.findViewById(R.id.loaderMeditation);
        single_ado_list_long = (RecyclerView) inflatreView.findViewById(R.id.single_ado_list_long);
        single_audioDis_list = (RecyclerView) inflatreView.findViewById(R.id.single_audioDis_list);
        single_song_VDO_list = (RecyclerView) inflatreView.findViewById(R.id.single_song_VDO_list);
        single_VDO_list_long = (RecyclerView) inflatreView.findViewById(R.id.single_VDO_list_long);


        allSampleData = new ArrayList<>();
        longVDOallSampleData = new ArrayList<>();
        shortVDOallSampleData = new ArrayList<>();
        longVDOallSampleData = new ArrayList<>();
        mSongsAudio = new ArrayList<>();
        mSongsAudioDis = new ArrayList<>();
        System.out.println("count languages " + MusicSQLiteHelper.countLanguage());
        languageList = MusicSQLiteHelper.getLanguages();
        getSlider();
    }


    private void setListItemsShortADO(String id, String Limit, String Offset) {
        {

//        Utility.getSharedInstance().ShowProgressBar(LoginActivity.this, "Please wait", "Signing in");

            //startAnim();


//        loginDAO.setEmailId(mEditEmail.getText().toString().trim());
            // loginDAO.setEmail(edt_Email.getText().toString().trim());
            //  loginDAO.setX_API_KEY(Config.X_API_KEY);
            //  loginDAO.setPassword(edt_Password.getText().toString().trim());


            loader.setVisibility(View.VISIBLE);
            Map<String, String> params = new HashMap<String, String>();
            params.put("id", "1");
            // Call<JsonObject> callbackLogin = myApplication.getAPIInstance().alltrackbystyle("1", "testkey");
            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().getAllTracksByCategory("testkey", id, Limit, Offset);

            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    JsonObject responseData = response.body();
                    ArrayList<AlbumModel> singleItem = new ArrayList<>();
                    // stopAnim();
                    if (responseData != null) {
                        if (responseData.get("error").getAsInt() == 0) {
                            JsonObject userJsonObject = responseData;

                            if (userJsonObject != null) {
                                Gson gson = new Gson();
                                Type listType = new TypeToken<List<SongDetail>>() {
                                }.getType();
                                mSongsAudio = gson.fromJson(responseData.get("respon"), listType);

                                loader.setVisibility(View.GONE);

                                // Utility.getSharedInstance().dismissProgressDialog();
                                single_song_list.setVisibility(View.VISIBLE);
                                single_song_list.setAdapter(new DashboardAudioAdapter(getActivity(), mSongsAudio));
                                progressLoadList.setVisibility(View.GONE);
                                recycler_songslist.setVisibility(View.GONE);
                                recycler_songslist.setHasFixedSize(true);
//                                RecyclerViewDataAdapter adapter = new RecyclerViewDataAdapter(getActivity(), allSampleData);
//                                recycler_songslist.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
//                                recycler_songslist.setAdapter(adapter);


                            }
                        }
                    }


                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    //stopAnim();
                    // Utility.getSharedInstance().DismissProgressBar();
                    loader.setVisibility(View.GONE);
                    //   Utility.getSharedInstance().Snackbar(toolbarLayout, "Something went wrong!");

                }


            });
        }
    }


    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
        // ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Hindu Devotional");
    }

//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        inflater.inflate(R.menu.language_menu, menu);
//        MenuItem item = menu.findItem(R.id.language);
//        MenuItemCompat.setActionView(item, R.layout.mm);
//        final TextView actionView = (TextView) MenuItemCompat.getActionView(item).findViewById(R.id.selectLanguage);
//        actionView.setText(util.getLangName());
//        actionView.setOnClickListener(new OnClickListener() {
//            public void onClick(View v) {
//                final Dialog dialog = new Dialog(getActivity());
//                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                dialog.setCancelable(false);
//                dialog.setContentView(R.layout.dialog_language);
//                dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
//                ListView langList = (ListView) dialog.findViewById(R.id.langList);
//                final LanguageAdapter adapter = new LanguageAdapter(getActivity(), languageList);
//                langList.setAdapter(adapter);
//                langList.setOnItemClickListener(new OnItemClickListener() {
//                    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
//                        Log.d("TAG", "Languag id" + languageList.get(position).getId());
//                        util.saveLang((languageList.get(position)).getId(), (languageList.get(position)).getLangName());
//                        adapter.setSelectedIndex(position);
//                        adapter.notifyDataSetChanged();
//                        actionView.setText((languageList.get(position)).getLangName());
//                        setListItems();
//                    }
//                });
//                Button btnOk = (Button) dialog.findViewById(R.id.btnLangOk);
//                dialog.findViewById(R.id.btnLangCancel).setOnClickListener(new OnClickListener() {
//                    public void onClick(View v) {
//                        dialog.dismiss();
//                    }
//                });
//                btnOk.setOnClickListener(new OnClickListener() {
//                    public void onClick(View v) {
//                        dialog.dismiss();
//                    }
//                });
//                dialog.show();
//            }
//        });
//
//    }
//
//    public boolean onOptionsItemSelected(MenuItem item) {
//        Log.d("TAG", "Item id: " + item.getItemId() + " | Save id: ");
//        return true;
//    }


    private void getAds() {

        MyApplication applicationController = new MyApplication();


        Call<JsonObject> callbackLogin = (Call<JsonObject>) applicationController.getAPIInstance().getAds();
        //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
        callbackLogin.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                JsonObject responseData = response.body();


                if (responseData != null) {

                    if (responseData.get("status").getAsBoolean() == true) {


                        Gson gson = new Gson();

                        Silder_DAO adsResponse = gson.fromJson(responseData, Silder_DAO.class);

                        //  getAdsList(adsResponse);


                        // Toast.makeText(DashBoardActivity.this,"Sucess",Toast.LENGTH_SHORT).show();


                    } else if (responseData.get("status").getAsBoolean() == false) {

                        // Toast.makeText(DashBoardActivity.this,"Failure",Toast.LENGTH_SHORT).show();

//

                    } else {
//
                    }
                } else {

                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {


            }


        });
    }


    private void getSlider() {

        MyApplication applicationController = new MyApplication();


        Call<JsonObject> callbackLogin = (Call<JsonObject>) applicationController.getAPIInstance().getSlider("nyasaa-app", "nyasaa-api");
        //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
        callbackLogin.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                JsonObject responseData = response.body();


                if (responseData != null) {

//                    if (responseData.get("status").getAsBoolean() == true) {


                    Gson gson = new Gson();

                    Silder_DAO adsResponse = gson.fromJson(responseData, Silder_DAO.class);
                    //  Silder_DAO adsResponses = gson.fromJson(responseData, Silder_DAO.class);

                    try {
                        setSlider(adsResponse);
                    } catch (Exception e) {

                    }

                    // getAdsList(adsResponse);


                    // Toast.makeText(DashBoardActivity.this,"Sucess",Toast.LENGTH_SHORT).show();


                } else {

                    // Toast.makeText(DashBoardActivity.this,"Failure",Toast.LENGTH_SHORT).show();

//

                }
//                } else {
//
//                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e("", "" + t);

            }


        });
    }

    private void setSlider(final Silder_DAO adsResponses) {


        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {

                // Stuff that updates the UI


                try {


                    if (adsResponses != null) {

                        for (int i = 0; i < adsResponses.getResultObject().size(); i++) {

                            if (adsResponses.getResultObject().get(i).getFile() != null
                                    ) {
                                CustomSliderView textSliderView = new CustomSliderView(getContext());
                                textSliderView
                                        //.description(adsResponses.getResultObject().get(i).getName())
                                        .image(Config.PIC_URL_silder + adsResponses.getResultObject().get(i).getFile())
                                        // .image(imgs[i])
                                        .setScaleType(BaseSliderView.ScaleType.Fit);

//                                textSliderView.bundle(new Bundle());
//            textSliderView.getBundle()
//                    .putString("extra", data.get(i).getPlanName());
                                sliderLayout.addSlider(textSliderView);
                            }
                        }

                        sliderLayout.setPresetTransformer(SliderLayout.Transformer.Accordion);
                        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
                        sliderLayout.setIndicatorVisibility(PagerIndicator.IndicatorVisibility.Invisible);
//                        sliderLayout.setCustomAnimation(new DescriptionAnimation());
                        sliderLayout.setDuration(3000);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });


    }


//    private void getAdsList(Silder_DAO adsResponse) {
//
//        adsResponseResultObject = adsResponse.getResultObject();
//
//        //  http://52.8.218.210/music_beta/admin/uploads/advertisement
//
//
//        mButAds.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                String getUrl = "";
//
//                getUrl = adsResponseResultObject.get(0).getLinks();
//
//                if (getUrl == null || getUrl.equalsIgnoreCase("")) {
//                    Toast.makeText(getActivity(), "Link Not Provided", Toast.LENGTH_SHORT).show();
//                } else {
//                    Uri uri = Uri.parse(getUrl); // missing 'http://' will cause crashed
//                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
//                    startActivity(intent);
//                }
//
//
//            }
//        });
//
//
//        mTxtAdText.setText(adsResponseResultObject.get(0).getDescription());
////        Picasso.with(getContext()).load("http://52.8.218.210/music_beta/admin/uploads/advertisement/" + adsResponseResultObject.get(0).getMedia()).into(mImgAd);
//
//
//    }


    public static void audioSTOPSettingPause() {

        com.music.nyasaa.manager.MediaController.getInstance().isAudioPaused();
    }


    private void initializeDraggablePanel() throws Resources.NotFoundException {
//        draggablePanel.setFragmentManager(getFragmentManager());
//
//        // draggablePanel.setFragmentManager();
//        draggablePanel.setTopFragment(new YouTubePlayerFragment());
//        draggablePanel.setBottomFragment(new YouTubeListFragment());
//
//
//        draggablePanel.setTopViewHeight(450);
//
////        draggablePanel.setTopFragment(youTubePlayerFragment);
////        draggablePanel.setBottomFragment(youTubeListFragment);
//        draggablePanel.initializeView();

    }


    private void getOfferDATA() {


        int imgs[] = {R.drawable.osho_two, R.drawable.osho_one, R.drawable.osho_six, R.drawable.osho_five};

        ArrayList<String> stringArrayList = new ArrayList<>();

        stringArrayList.add("The OSHO ");
        stringArrayList.add("The OSHO Ashram pune");
        stringArrayList.add("The OSHO Ashram songs");
        stringArrayList.add("The Buddha");


//        for (int i = 0; i < stringArrayList.size(); i++) {
//
//            TextSliderView textSliderView = new TextSliderView(getContext());
//            textSliderView
//                    .description(stringArrayList.get(i).toString())
////                    .image(Config.APP_BASEURL_HOST + data.get(i).getImageUrl())
//                    .image(imgs[i])
//                    .setScaleType(BaseSliderView.ScaleType.Fit)
//                    .setOnSliderClickListener(this);
//            textSliderView.bundle(new Bundle());
////            textSliderView.getBundle()
////                    .putString("extra", data.get(i).getPlanName());
//            sliderLayout.addSlider(textSliderView);
//        }
//
//        sliderLayout.setPresetTransformer(SliderLayout.Transformer.Accordion);
//        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
//        sliderLayout.setCustomAnimation(new DescriptionAnimation());
//        sliderLayout.setDuration(3000);
        //sliderLayout.addOnPageChangeListener(getContext());

    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

//
//   private void blink()
//   {
//     //  Utility.getSharedInstance().showProgressDialog(getActivity());
//       new Handler().postDelayed(new Runnable() {
//           @Override
//           public void run() {
//             Utility.getSharedInstance().dismissProgressDialog();
//           }
//       }, 2000);
//   }


    ///// Serach Music

    public void getMusic() {


//        loaderMusic.set


        loaderMusic.setVisibility(View.VISIBLE);


        MyApplication myApplication = new MyApplication();

//        Utility.getSharedInstance().showProgressDialog(getActivity());


        RequestSearchMedia requestSearchMedia = new RequestSearchMedia();

        requestSearchMedia.setLimit("10");
        requestSearchMedia.setOffset("0");
        requestSearchMedia.setSearchterm("");


        if (Utility.getSharedInstance().isConnectedToInternet(getContext())) {

//            Utility.getSharedInstance().showProgressDialog(getActivity());

//            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");
//            String cat = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT, "1");
//            // id="1";
//            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");

            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().searchMusic(X_API_KEY, requestSearchMedia);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();
//                    Utility.getSharedInstance().dismissProgressDialog();

                    // Log.e("Response::", responseData.toString());

                    if (responseData.get("error").getAsInt() == 0) {
                        // Config.getPlanDAO = new GetPlanDAO();
                        Gson gson = new Gson();

//                        Utility.getSharedInstance().dismissProgressDialog();


                        Type listType = new TypeToken<List<SongDetail>>() {
                        }.getType();
                        if (responseData.get("respon") != null) {

                            JsonArray jsonArray = responseData.get("respon").getAsJsonArray();
                            // JsonObject jsonObject=responseData.getAsJsonObject("respon");
                            if (jsonArray.size() > 0) {
                                loaderMusic.setVisibility(View.GONE);
                                mMusicLayout.setVisibility(View.VISIBLE);
                                single_ado_list_long.setVisibility(View.VISIBLE);
                                mSongss = gson.fromJson(responseData.get("respon"), listType);
                                dashboardMusicAdapter = new DashboardMusicAdapter(getActivity(), mSongss);
                                single_ado_list_long.setAdapter(dashboardMusicAdapter);
                            } else {
                                loaderMusic.setVisibility(View.GONE);
                                mMusicLayout.setVisibility(View.GONE);
                                single_ado_list_long.setVisibility(View.VISIBLE);
                            }
                        }


                        // Utility.getSharedInstance().Snackbar(toolbarLayout, "Password Changed Sucessfully");


                    } else {

//                        Utility.getSharedInstance().dismissProgressDialog();


                    }

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

//                    Utility.getSharedInstance().dismissProgressDialog();


                }


            });
        } else {
            Toast.makeText(getActivity(), "No internet connection available", Toast.LENGTH_SHORT).show();

            loaderMusic.setVisibility(View.GONE);
            mMusicLayout.setVisibility(View.GONE);
        }
    }


    /////// Get Mediration


    ///// Serach Music

    public void getMeditation() {


        MyApplication myApplication = new MyApplication();

        loaderMeditation.setVisibility(View.VISIBLE);

        RequestSearchMedia requestSearchMedia = new RequestSearchMedia();

        requestSearchMedia.setLimit("10");
        requestSearchMedia.setOffset("0");
        requestSearchMedia.setSearchterm("");


        if (Utility.getSharedInstance().isConnectedToInternet(getContext())) {

//            Utility.getSharedInstance().showProgressDialog(getActivity());

//            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");
//            String cat = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT, "1");
//            // id="1";
//            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");

            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().searchMeditation(X_API_KEY, requestSearchMedia);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());

//                    Utility.getSharedInstance().dismissProgressDialog();

                    if (responseData.get("error").getAsInt() == 0) {
                        // Config.getPlanDAO = new GetPlanDAO();
                        Gson gson = new Gson();
                        Type listType = new TypeToken<List<SongDetail>>() {
                        }.getType();
                        if (responseData.get("respon") != null) {

                            JsonArray jsonArray = responseData.get("respon").getAsJsonArray();
                            // JsonObject jsonObject=responseData.getAsJsonObject("respon");

                            if (jsonArray.size() > 0) {

                                loaderMeditation.setVisibility(View.GONE);
                                mMeditationLayout.setVisibility(View.VISIBLE);
                                single_song_VDO_list.setVisibility(View.VISIBLE);

                                mSongsMedi = gson.fromJson(responseData.get("respon"), listType);
                                dashboardMeditationAdapter = new DashboardMeditationAdapter(getActivity(), mSongsMedi);
                                single_song_VDO_list.setAdapter(dashboardMeditationAdapter);

                            } else {
                                loaderMeditation.setVisibility(View.GONE);
                                mMeditationLayout.setVisibility(View.GONE);
                                single_song_VDO_list.setVisibility(View.GONE);
                            }


                        }


                        // Utility.getSharedInstance().Snackbar(toolbarLayout, "Password Changed Sucessfully");


                    } else {


                    }

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {


                }


            });
        } else {
            Toast.makeText(getActivity(), "No internet connection available", Toast.LENGTH_SHORT).show();
        }
    }


    /// Get Intervies


    public void getInreViews() {


        MyApplication myApplication = new MyApplication();


        RequestSearchMedia requestSearchMedia = new RequestSearchMedia();

        requestSearchMedia.setLimit("10");
        requestSearchMedia.setOffset("0");
        requestSearchMedia.setSearchterm("");

        loaderInterview.setVisibility(View.VISIBLE);


        if (Utility.getSharedInstance().isConnectedToInternet(getContext())) {
//            Utility.getSharedInstance().showProgressDialog(getActivity());


//            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");
//            String cat = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT, "1");
//            // id="1";
//            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");

            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().searchinterviews(X_API_KEY, requestSearchMedia);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());

//                    Utility.getSharedInstance().dismissProgressDialog();
                    if (responseData != null) {
                        if (responseData.get("error").getAsInt() == 0) {
                            // Config.getPlanDAO = new GetPlanDAO();
                            Gson gson = new Gson();


                            Type listType = new TypeToken<List<SongDetail>>() {
                            }.getType();
                            if (responseData.get("respon") != null) {

                                JsonArray jsonArray = responseData.get("respon").getAsJsonArray();


                                // JsonObject jsonObject=responseData.getAsJsonObject("respon");

                                if (jsonArray.size() > 0) {
                                    loaderInterview.setVisibility(View.GONE);
                                    mInterviewLayout.setVisibility(View.VISIBLE);
                                    single_VDO_list_long.setVisibility(View.VISIBLE);
                                    mSongsInt = gson.fromJson(responseData.get("respon"), listType);

                                    dashboardInterviewAdapter = new DashboardInterviewAdapter(getActivity(), mSongsInt);
                                    single_VDO_list_long.setAdapter(dashboardInterviewAdapter);

                                } else {
                                    loaderInterview.setVisibility(View.GONE);
                                    mInterviewLayout.setVisibility(View.GONE);
                                    single_VDO_list_long.setVisibility(View.GONE);
                                }


                            }


                            // Utility.getSharedInstance().Snackbar(toolbarLayout, "Password Changed Sucessfully");


                        }

                    } else {


                    }

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {


//                    Utility.getSharedInstance().dismissProgressDialog();

                }


            });
        } else {
            Toast.makeText(getActivity(), "No internet connection available", Toast.LENGTH_SHORT).show();

            loaderMeditation.setVisibility(View.GONE);
            mMeditationLayout.setVisibility(View.GONE);
        }
    }


    ///  Audio All


    public void getSearchedSOngs(String searchText) {


        String uId=SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");

        MyApplication myApplication = new MyApplication();

        RequestSearchMedia requestSearchMedia = new RequestSearchMedia();


        requestSearchMedia.setUser_id(uId);


        if (Utility.getSharedInstance().isConnectedToInternet(getContext())) {


            loader.setVisibility(View.VISIBLE);

//

            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().getSongs(clientService, X_API_KEY, requestSearchMedia);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());

                    if (responseData != null) {

                        // Config.getPlanDAO = new GetPlanDAO();
                        if (responseData.get("respon") != null) {

                            JsonArray jsonArray = responseData.get("respon").getAsJsonArray();


                            // JsonObject jsonObject=responseData.getAsJsonObject("respon");

                            if (jsonArray.size() > 0) {
                                mAudioLayout.setVisibility(View.VISIBLE);
                                single_song_list.setVisibility(View.VISIBLE);
                                Gson gson = new Gson();
                                Type listType = new TypeToken<List<SongDetail>>() {
                                }.getType();
                                mSongsAudio = gson.fromJson(responseData.get("respon"), listType);

                                loader.setVisibility(View.GONE);
                                dashboardAudioAdapter = new DashboardAudioAdapter(getActivity(), mSongsAudio);

                                // Utility.getSharedInstance().dismissProgressDialog();
                                single_song_list.setVisibility(View.VISIBLE);
                                single_song_list.setAdapter(dashboardAudioAdapter);
                                progressLoadList.setVisibility(View.GONE);
                                recycler_songslist.setVisibility(View.GONE);
//
                            } else {
                                loader.setVisibility(View.GONE);
                                mAudioLayout.setVisibility(View.GONE);
                                single_song_list.setVisibility(View.VISIBLE);
                            }

                            // Utility.getSharedInstance().Snackbar(toolbarLayout, "Password Changed Sucessfully");

                        }

                    } else {

                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                }

            });
        } else {
            loader.setVisibility(View.GONE);
            mAudioLayout.setVisibility(View.GONE);

        }
    }

    public void getSearchedSOngsDiscourses(String searchText) {

        MyApplication myApplication = new MyApplication();

        RequestSearchMedia requestSearchMedia = new RequestSearchMedia();

        requestSearchMedia.setLimit("10");
        requestSearchMedia.setOffset("0");
        requestSearchMedia.setQuedisc(2);
        requestSearchMedia.setShould_orderby_name(true);
        requestSearchMedia.setSearchterm(searchText);


        if (Utility.getSharedInstance().isConnectedToInternet(getContext())) {


            loaderAudioDis.setVisibility(View.VISIBLE);

//            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");
//            String cat = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT, "1");
//            // id="1";
//            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");

            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().searchAudioQA(X_API_KEY, requestSearchMedia);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());

                    if (responseData != null) {
                        if (responseData.get("error").getAsInt() == 0) {
                            // Config.getPlanDAO = new GetPlanDAO();
                            if (responseData.get("respon") != null) {

                                JsonArray jsonArray = responseData.get("respon").getAsJsonArray();


                                // JsonObject jsonObject=responseData.getAsJsonObject("respon");

                                if (jsonArray.size() > 0) {
                                    mAudioDisLayout.setVisibility(View.VISIBLE);
                                    single_audioDis_list.setVisibility(View.VISIBLE);
                                    Gson gson = new Gson();
                                    Type listType = new TypeToken<List<SongDetail>>() {
                                    }.getType();
                                    mSongsAudioDis = gson.fromJson(responseData.get("respon"), listType);

                                    loaderAudioDis.setVisibility(View.GONE);
                                    dashboardAudioDisAdapter = new DashboardAudioDisAdapter(getActivity(), mSongsAudioDis);

                                    // Utility.getSharedInstance().dismissProgressDialog();
                                    single_audioDis_list.setVisibility(View.VISIBLE);
                                    single_audioDis_list.setAdapter(dashboardAudioDisAdapter);
                                    progressLoadList.setVisibility(View.GONE);
                                    recycler_songslist.setVisibility(View.GONE);
                                } else {
                                    loaderAudioDis.setVisibility(View.GONE);
                                    mAudioDisLayout.setVisibility(View.GONE);
                                    single_audioDis_list.setVisibility(View.VISIBLE);
                                }


                            }


                            // Utility.getSharedInstance().Snackbar(toolbarLayout, "Password Changed Sucessfully");


                        }

                    } else {


                    }

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {


                }


            });
        } else {
            single_audioDis_list.setVisibility(View.GONE);
            loaderAudioDis.setVisibility(View.GONE);
            mAudioDisLayout.setVisibility(View.GONE);

        }
    }

    public void playPoaseAudio(int position) {


        try {
            PostionOfPlay = position;

            if (mSongsAudio != null)
                for (int i = 0; i < mSongsAudio.size(); i++) {
                    if (i != position)
                        mSongsAudio.get(i).setPlayStop(false);

                }
            if (mSongsAudio.get(position).getPlayStop()) {
                mSongsAudio.get(position).setPlayStop(false);
            } else {
                mSongsAudio.get(position).setPlayStop(true);
            }

            dashboardAudioAdapter.notifyDataSetChanged();

        } catch (Exception e) {
        }

        // reset remain

        try {
            // PostionOfPlay = position;


            if (mSongsMedi != null)
                for (int i = 0; i < mSongsMedi.size(); i++) {

                    mSongsMedi.get(i).setPlayStop(false);

                }
            dashboardMeditationAdapter.notifyDataSetChanged();

            if (mSongss != null)
                for (int i = 0; i < mSongss.size(); i++) {

                    mSongss.get(i).setPlayStop(false);

                }
            dashboardMusicAdapter.notifyDataSetChanged();


            if (mSongsInt != null)
                for (int i = 0; i < mSongsInt.size(); i++) {

                    mSongsInt.get(i).setPlayStop(false);

                }
            dashboardInterviewAdapter.notifyDataSetChanged();

            if (mSongsAudioDis != null)
                for (int i = 0; i < mSongsAudioDis.size(); i++) {

                    mSongsAudioDis.get(i).setPlayStop(false);

                }
            dashboardAudioDisAdapter.notifyDataSetChanged();


//            if (mSongsAudio != null)
//                for (int i = 0; i < mSongsAudio.size(); i++) {
//
//                    mSongsAudio.get(i).setPlayStop(false);
//
//                }
//            dashboardAudioAdapter.notifyDataSetChanged();


        } catch (Exception e) {
        }

    }

    public void playPoaseAudioDis(int position) {


        try {
            PostionOfPlay = position;

            if (mSongsAudioDis != null)
                for (int i = 0; i < mSongsAudioDis.size(); i++) {
                    if (i != position)
                        mSongsAudioDis.get(i).setPlayStop(false);

                }
            if (mSongsAudioDis.get(position).getPlayStop()) {
                mSongsAudioDis.get(position).setPlayStop(false);
            } else {
                mSongsAudioDis.get(position).setPlayStop(true);
            }

            dashboardAudioDisAdapter.notifyDataSetChanged();

        } catch (Exception e) {
        }

        // reset remain

        try {
            // PostionOfPlay = position;


            if (mSongsMedi != null)
                for (int i = 0; i < mSongsMedi.size(); i++) {

                    mSongsMedi.get(i).setPlayStop(false);

                }
            dashboardMeditationAdapter.notifyDataSetChanged();

            if (mSongss != null)
                for (int i = 0; i < mSongss.size(); i++) {

                    mSongss.get(i).setPlayStop(false);

                }
            dashboardMusicAdapter.notifyDataSetChanged();


            if (mSongsInt != null)
                for (int i = 0; i < mSongsInt.size(); i++) {

                    mSongsInt.get(i).setPlayStop(false);

                }
            dashboardInterviewAdapter.notifyDataSetChanged();

            if (mSongsAudio != null)
                for (int i = 0; i < mSongsAudio.size(); i++) {

                    mSongsAudio.get(i).setPlayStop(false);

                }
            dashboardAudioAdapter.notifyDataSetChanged();


//            if (mSongsAudio != null)
//                for (int i = 0; i < mSongsAudio.size(); i++) {
//
//                    mSongsAudio.get(i).setPlayStop(false);
//
//                }
//            dashboardAudioAdapter.notifyDataSetChanged();


        } catch (Exception e) {
        }

    }

    public void playPoaseInterview(int position) {

        try {
            PostionOfPlay = position;

            if (mSongsInt != null)
                for (int i = 0; i < mSongsInt.size(); i++) {
                    if (i != position)
                        mSongsInt.get(i).setPlayStop(false);

                }
            if (mSongsInt.get(position).getPlayStop()) {
                mSongsInt.get(position).setPlayStop(false);
            } else {
                mSongsInt.get(position).setPlayStop(true);
            }

            dashboardInterviewAdapter.notifyDataSetChanged();

        } catch (Exception e) {
        }


        // reset remain

        try {
            // PostionOfPlay = position;


            if (mSongsMedi != null)
                for (int i = 0; i < mSongsMedi.size(); i++) {

                    mSongsMedi.get(i).setPlayStop(false);

                }
            dashboardMeditationAdapter.notifyDataSetChanged();

            if (mSongss != null)
                for (int i = 0; i < mSongss.size(); i++) {

                    mSongss.get(i).setPlayStop(false);

                }
            dashboardMusicAdapter.notifyDataSetChanged();


//            if (mSongsInt != null)
//                for (int i = 0; i < mSongsInt.size(); i++) {
//
//                    mSongsInt.get(i).setPlayStop(false);
//
//                }
//            dashboardInterviewAdapter.notifyDataSetChanged();


            if (mSongsAudio != null)
                for (int i = 0; i < mSongsAudio.size(); i++) {

                    mSongsAudio.get(i).setPlayStop(false);

                }
            dashboardAudioAdapter.notifyDataSetChanged();

            if (mSongsAudioDis != null)
                for (int i = 0; i < mSongsAudioDis.size(); i++) {

                    mSongsAudioDis.get(i).setPlayStop(false);

                }
            dashboardAudioDisAdapter.notifyDataSetChanged();


        } catch (Exception e) {
        }

    }


    public void playPoaseMusic(int position) {

        try {
            PostionOfPlay = position;

            if (mSongss != null)
                for (int i = 0; i < mSongss.size(); i++) {
                    if (i != position)
                        mSongss.get(i).setPlayStop(false);

                }
            if (mSongss.get(position).getPlayStop()) {
                mSongss.get(position).setPlayStop(false);
            } else {
                mSongss.get(position).setPlayStop(true);
            }

            dashboardMusicAdapter.notifyDataSetChanged();

        } catch (Exception e) {
        }


        // reset remain

        try {
            // PostionOfPlay = position;


            if (mSongsMedi != null)
                for (int i = 0; i < mSongsMedi.size(); i++) {

                    mSongsMedi.get(i).setPlayStop(false);

                }
            dashboardMeditationAdapter.notifyDataSetChanged();

//            if (mSongss != null)
//                for (int i = 0; i < mSongss.size(); i++) {
//
//                    mSongss.get(i).setPlayStop(false);
//
//                }
//            dashboardMusicAdapter.notifyDataSetChanged();


            if (mSongsInt != null)
                for (int i = 0; i < mSongsInt.size(); i++) {

                    mSongsInt.get(i).setPlayStop(false);

                }
            dashboardInterviewAdapter.notifyDataSetChanged();


            if (mSongsAudio != null)
                for (int i = 0; i < mSongsAudio.size(); i++) {

                    mSongsAudio.get(i).setPlayStop(false);

                }
            dashboardAudioAdapter.notifyDataSetChanged();

            if (mSongsAudioDis != null)
                for (int i = 0; i < mSongsAudioDis.size(); i++) {

                    mSongsAudioDis.get(i).setPlayStop(false);

                }
            dashboardAudioDisAdapter.notifyDataSetChanged();

        } catch (Exception e) {
        }


    }

    public void playPoaseMidition(int position) {

        try {
            PostionOfPlay = position;

            if (mSongsMedi != null)
                for (int i = 0; i < mSongsMedi.size(); i++) {
                    if (i != position)
                        mSongsMedi.get(i).setPlayStop(false);

                }
            if (mSongsMedi.get(position).getPlayStop()) {
                mSongsMedi.get(position).setPlayStop(false);
            } else {
                mSongsMedi.get(position).setPlayStop(true);
            }

            dashboardMeditationAdapter.notifyDataSetChanged();

        } catch (Exception e) {
        }


        // reset remain

        try {
            // PostionOfPlay = position;


//            if (mSongsMedi != null)
//                for (int i = 0; i < mSongsMedi.size(); i++) {
//
//                    mSongsMedi.get(i).setPlayStop(false);
//
//                }
//            dashboardMeditationAdapter.notifyDataSetChanged();

            if (mSongss != null)
                for (int i = 0; i < mSongss.size(); i++) {

                    mSongss.get(i).setPlayStop(false);

                }
            dashboardMusicAdapter.notifyDataSetChanged();


            if (mSongsInt != null)
                for (int i = 0; i < mSongsInt.size(); i++) {

                    mSongsInt.get(i).setPlayStop(false);

                }
            dashboardInterviewAdapter.notifyDataSetChanged();


            if (mSongsAudio != null)
                for (int i = 0; i < mSongsAudio.size(); i++) {

                    mSongsAudio.get(i).setPlayStop(false);

                }
            dashboardAudioAdapter.notifyDataSetChanged();

            if (mSongsAudioDis != null)
                for (int i = 0; i < mSongsAudioDis.size(); i++) {

                    mSongsAudioDis.get(i).setPlayStop(false);

                }
            dashboardAudioDisAdapter.notifyDataSetChanged();


        } catch (Exception e) {
        }


    }


    private void threads() {


        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {

                // Stuff that updates the UI

                getSearchedSOngs("");
                //  getSearchedSOngsDiscourses("");
                //getMusic();
                // getMeditation();
                // getInreViews();
//

            }
        });

    }
}
