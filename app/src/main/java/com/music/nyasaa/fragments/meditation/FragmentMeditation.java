package com.music.nyasaa.fragments.meditation;

import android.content.Context;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.music.nyasaa.R;
import com.music.nyasaa.adapter.MediatationMoreAdapter;
import com.music.nyasaa.controller.MyApplication;
import com.music.nyasaa.fragments.interviews.FragmentInterview;
import com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity;
import com.music.nyasaa.models.RequestModel.SearchMedia.RequestSearchLangMediation;
import com.music.nyasaa.models.RequestModel.SearchMedia.RequestSearchMedia;
import com.music.nyasaa.models.SongDetail;
import com.music.nyasaa.utility.SharedPreferenceManager;
import com.music.nyasaa.utility.Utility;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity.PostionOfPlay;
import static com.music.nyasaa.retrofit.Config.X_API_KEY;

public class FragmentMeditation extends Fragment implements View.OnClickListener {

    private View myView;

    public static FragmentMeditation fragmentMeditation;
    private static final String ARG_KEY_NUMBER = "tab_number";
    private static Context context;
    public static ArrayList<SongDetail> mSongss;
    private SwipyRefreshLayout swipyRefreshLayoutDirection;
    private TextView mTxtWarn, mTxtLang;
    private ImageView langImg;
    MediatationMoreAdapter mediatationMoreAdapter;
    private RecyclerView mMySongsListView;
    private FragmentInterview fragmentMySongs;
    private EditText mEditSearch;
    private int currentPage = 0;

    private LinearLayout mLayMembers, mLayRequest, mLayMainBar, mLayPost;
    boolean post=false,request=false;
    private TextView mTxtMember, mTxtRequest, mTxtPost,mTxtShowTech;


    public static FragmentMeditation newInstance(int number) {
        Bundle args = new Bundle();
        args.putInt(ARG_KEY_NUMBER, number);
        FragmentMeditation frag = new FragmentMeditation();
        frag.setArguments(args);
        return frag;
    }


    public FragmentMeditation() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        myView = inflater.inflate(R.layout.fragment_meditation, container, false);
        initView(myView);
        fragmentMeditation = this;

        firstColor();
        return myView;
    }


    private void initView(View myView) {


//        myView.getRootView().setBackgroundColor(Color.BLACK);
        myView.getRootView().setBackgroundColor(getResources().getColor(R.color.orBLACK));

//        myView.getRootView().setBackgroundResource(R.drawable.black_gradient);
        mLayMainBar = (LinearLayout) myView.findViewById(R.id.menuLayBar);
        mLayMembers = (LinearLayout) myView.findViewById(R.id.layMember);
        mLayRequest = (LinearLayout) myView.findViewById(R.id.layRequest);

        mLayPost = (LinearLayout) myView.findViewById(R.id.layPost);




        mTxtMember = (TextView) myView.findViewById(R.id.txtMembers);

        mTxtRequest = (TextView) myView.findViewById(R.id.txtRequest);

        mTxtPost = (TextView) myView.findViewById(R.id.txtPost);


        mTxtShowTech = (TextView) myView.findViewById(R.id.txtShow);
        mTxtShowTech.setPaintFlags(mTxtShowTech.getPaintFlags() |   Paint.UNDERLINE_TEXT_FLAG);
        mTxtWarn = (TextView) myView.findViewById(R.id.txtWarn);
        langImg = (ImageView) myView.findViewById(R.id.langImg);
        mTxtLang = (TextView) myView.findViewById(R.id.txtLang);
        mTxtWarn.setVisibility(View.GONE);

        swipyRefreshLayoutDirection = (SwipyRefreshLayout) myView.findViewById(R.id.swipe_refresh_layout);
        swipyRefreshLayoutDirection.setDirection(SwipyRefreshLayoutDirection.BOTH);
        mMySongsListView = (RecyclerView) myView.findViewById(R.id.listViewAlbum);
        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        mMySongsListView.setLayoutManager(linearLayoutManager);
        mMySongsListView.setItemAnimator(new DefaultItemAnimator());

        int type = SharedPreferenceManager.getInstance().readInteger(SharedPreferenceManager.KEY_MED_TYPE_ID, 1);


        // MyPlayListAdapter connectionAdapter = new MyPlayListAdapter(getActivity(),img,name);
        // mMySongsListView.setAdapter(connectionAdapter);
//        mMySongsListView =(RecyclerView)myView.findViewById(R.id.listViewAlbum);
        currentPage = 0;

        mEditSearch = (EditText) myView.findViewById(R.id.editSearch);


        mLayRequest.setOnClickListener(this);
        mLayMembers.setOnClickListener(this);
        mLayPost.setOnClickListener(this);


        mEditSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {


                /*if (mEditSearch.getText().toString().trim().length() >= 3) {
                    getSearchedSOngs(mEditSearch.getText().toString().trim());
                }*/
                if (mEditSearch.getText().toString().length() == 0)
                    getSearchedSOngs(mEditSearch.getText().toString().trim());
                else if (mEditSearch.getText().toString().length() == 1)
                    getSearchedSOngs(mEditSearch.getText().toString().trim());
                else if (mEditSearch.getText().toString().length() > 0)
                    getSearchedSOngs(mEditSearch.getText().toString().trim());
                else if (mEditSearch.getText().toString().length() > 1)
                    getSearchedSOngs(mEditSearch.getText().toString().trim());
                else if (mEditSearch.getText().toString().length() > 2)
                    getSearchedSOngs(mEditSearch.getText().toString().trim());
                else if (mEditSearch.getText().toString().length() > 3)
                    getSearchedSOngs(mEditSearch.getText().toString().trim());
                else if (mEditSearch.getText().toString().length() > 4)
                    getSearchedSOngs(mEditSearch.getText().toString().trim());
                else if (mEditSearch.getText().toString().length() > 5)
                    getSearchedSOngs(mEditSearch.getText().toString().trim());


            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

//        getSearchedSOngs("");
        String more = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_TXT_MORE, "none");


//        getSearchedSOngs("");


        swipyRefreshLayoutDirection.setEnabled(false);
//        swipyRefreshLayoutDirection.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh(SwipyRefreshLayoutDirection direction) {
//                if (direction == SwipyRefreshLayoutDirection.TOP) {
//                    getSearchedSOngs("");
//                    currentPage = 0;
//                } else {
//                    getSearchedSOngs("");
//                }
//            }
//        });
    }


    public void getAllMusics(final int currentPage) {

        MyApplication myApplication = new MyApplication();
        Utility.getSharedInstance().showProgressDialog(getActivity());

        if (Utility.getSharedInstance().isConnectedToInternet(getContext())) {

            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");
            String cat = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT, "1");
            // id="1";
            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");

            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().getAllMeditationByCategory(X_API_KEY, cat, "10", String.valueOf(currentPage));
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    // JsonObject responseData = response.body();
                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());

                    if (responseData.get("error").getAsInt() == 0) {
                        // Config.getPlanDAO = new GetPlanDAO();
                        Gson gson = new Gson();
                        Utility.getSharedInstance().dismissProgressDialog();

                        Type listType = new TypeToken<List<SongDetail>>() {
                        }.getType();
                        if (responseData.get("respon") != null) {

                            JsonArray jsonArray = responseData.get("respon").getAsJsonArray();

                            if (jsonArray.size() > 0) {
                                mTxtWarn.setVisibility(View.GONE);
                                mMySongsListView.setVisibility(View.VISIBLE);
                                if (currentPage == 0) {
                                    mSongss = gson.fromJson(responseData.get("respon"), listType);
                                    swipyRefreshLayoutDirection.setRefreshing(false);
                                    getMusicList(mSongss);
                                } else {
                                    ArrayList<SongDetail> datumList = gson.fromJson(responseData.get("respon"), listType);
                                    mSongss.addAll(mSongss.size(), datumList);
                                    swipyRefreshLayoutDirection.setRefreshing(false);
                                    getUpdatedList();
                                }
                                if (mSongss != null) {
                                    if (mSongss.size() < 0) {
                                        swipyRefreshLayoutDirection.setRefreshing(false);
                                        mTxtWarn.setVisibility(View.VISIBLE);
                                        mMySongsListView.setVisibility(View.GONE);
                                    }
                                }

                                /*mediatationMoreAdapter = new MediatationMoreAdapter(getActivity(), mSongss, FragmentMeditation.this);
                                mMySongsListView.setAdapter(mediatationMoreAdapter);*/
                            } else {
                                if (mSongss == null) {
                                    mTxtWarn.setVisibility(View.VISIBLE);
                                    mMySongsListView.setVisibility(View.GONE);
                                }
                                swipyRefreshLayoutDirection.setRefreshing(false);
                                Utility.getSharedInstance().dismissProgressDialog();
                            }
                        }
                        // Utility.getSharedInstance().Snackbar(toolbarLayout, "Password Changed Sucessfully");
                    } else {
                        swipyRefreshLayoutDirection.setRefreshing(false);
                        Utility.getSharedInstance().dismissProgressDialog();
                        mTxtWarn.setVisibility(View.VISIBLE);
                        mMySongsListView.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Utility.getSharedInstance().dismissProgressDialog();
                    swipyRefreshLayoutDirection.setRefreshing(false);
                    /*mTxtWarn.setVisibility(View.VISIBLE);
                    mMySongsListView.setVisibility(View.GONE);*/
                    Toast.makeText(getActivity(), "Server error .. Please try again after some time", Toast.LENGTH_SHORT).show();
                }

            });
        } else {

        }
    }

    private void getMusicList(ArrayList<SongDetail> songDetailList) {
        mediatationMoreAdapter = new MediatationMoreAdapter(getActivity(), songDetailList, FragmentMeditation.this);
        mMySongsListView.setAdapter(mediatationMoreAdapter);

        if (mediatationMoreAdapter != null)
            mediatationMoreAdapter.notifyDataSetChanged();
        currentPage = currentPage + 10;
    }

    private void getUpdatedList() {
        if (mediatationMoreAdapter != null)
            mediatationMoreAdapter.notifyDataSetChanged();
        currentPage = currentPage + 10;
    }


    /////////////////////GET SERACHED SONGS//////////////////////////


    public void getSearchedSOngs(String searchText) {
//        langImg.setVisibility(View.GONE);
//        mTxtLang.setVisibility(View.GONE);



        MyApplication myApplication = new MyApplication();

        //Utility.getSharedInstance().showProgressDialog(getActivity());

        int type = SharedPreferenceManager.getInstance().readInteger(SharedPreferenceManager.KEY_MED_TYPE_ID, 0);
        if (type == 2) {
            mTxtLang.setVisibility(View.VISIBLE);
            langImg.setVisibility(View.VISIBLE);

        } else {
            langImg.setVisibility(View.GONE);
            mTxtLang.setVisibility(View.GONE);
        }

        if(type==3)
        {
            mTxtShowTech.setVisibility(View.VISIBLE);
            mTxtShowTech.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((DashBoardActivity)getActivity()).openMedTEch();
                }
            });
        }
        else {
            mTxtShowTech.setVisibility(View.GONE);
        }

        RequestSearchMedia requestSearchMedia = new RequestSearchMedia();

        requestSearchMedia.setLimit("1500");
        requestSearchMedia.setOffset("0");
        requestSearchMedia.setSearchterm(searchText);
        requestSearchMedia.setType(type);


        if (Utility.getSharedInstance().isConnectedToInternet(getContext())) {

//            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");
//            String cat = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT, "1");
//            // id="1";
//            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");

            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().searchMeditation(X_API_KEY, requestSearchMedia);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());

                    if (responseData.get("error").getAsInt() == 0) {
                        // Config.getPlanDAO = new GetPlanDAO();
                        Gson gson = new Gson();

                        //Utility.getSharedInstance().dismissProgressDialog();


                        Type listType = new TypeToken<List<SongDetail>>() {
                        }.getType();
                        if (responseData.get("respon") != null) {

                            JsonArray jsonArray = responseData.get("respon").getAsJsonArray();


                            // JsonObject jsonObject=responseData.getAsJsonObject("respon");

                            if (jsonArray.size() > 0) {

                                mSongss = gson.fromJson(responseData.get("respon"), listType);
                                mTxtWarn.setVisibility(View.GONE);
                                mMySongsListView.setVisibility(View.VISIBLE);
                                swipyRefreshLayoutDirection.setRefreshing(false);


//                                AudioSerachSongsAdapter mediatationMoreAdapter = new AudioSerachSongsAdapter(getActivity(), mSongs);

                                mediatationMoreAdapter = new MediatationMoreAdapter(getActivity(), mSongss, FragmentMeditation.this);
                                mMySongsListView.setAdapter(mediatationMoreAdapter);

                                if (mSongss != null) {
                                    if (mSongss.size() < 0) {
                                        swipyRefreshLayoutDirection.setRefreshing(false);
                                        mTxtWarn.setVisibility(View.VISIBLE);
                                        mMySongsListView.setVisibility(View.GONE);
                                    }
                                }
                            } else {
                                swipyRefreshLayoutDirection.setRefreshing(false);
                                mTxtWarn.setVisibility(View.VISIBLE);
                                mMySongsListView.setVisibility(View.GONE);
                                //Utility.getSharedInstance().dismissProgressDialog();
                            }


                        }


                        // Utility.getSharedInstance().Snackbar(toolbarLayout, "Password Changed Sucessfully");


                    } else {
                        swipyRefreshLayoutDirection.setRefreshing(false);
                        //Utility.getSharedInstance().dismissProgressDialog();
                        mTxtWarn.setVisibility(View.VISIBLE);
                        mMySongsListView.setVisibility(View.GONE);

                    }

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                    //Utility.getSharedInstance().dismissProgressDialog();
                    mTxtWarn.setVisibility(View.VISIBLE);
                    mMySongsListView.setVisibility(View.GONE);

                }


            });
        } else {

        }
    }


    /// on Lang Selected


    /////////////////////GET SERACHED SONGS//////////////////////////


    public void getMedOnLangSelectd(String langID) {


        int rLang = 1;
        String lang = langID;
        if (langID.equalsIgnoreCase("1")) {
            mTxtLang.setText("English");
            rLang = 1;


        } else {
            mTxtLang.setText("Hindi");
            rLang = 2;
        }

        MyApplication myApplication = new MyApplication();

        //Utility.getSharedInstance().showProgressDialog(getActivity());

        int type = SharedPreferenceManager.getInstance().readInteger(SharedPreferenceManager.KEY_MED_TYPE_ID, 1);

        RequestSearchLangMediation requestSearchMedia = new RequestSearchLangMediation();

        requestSearchMedia.setLimit("150");
        requestSearchMedia.setOffset("0");
//        requestSearchMedia.setSearchterm("");
//        requestSearchMedia.setType(type);
        requestSearchMedia.setAlbum_id(rLang);


        if (Utility.getSharedInstance().isConnectedToInternet(getContext())) {

//            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");
//            String cat = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT, "1");
//            // id="1";
//            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");

            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().searchMeditationNew(X_API_KEY, requestSearchMedia);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());

                    if (responseData.get("error").getAsInt() == 0) {
                        // Config.getPlanDAO = new GetPlanDAO();
                        Gson gson = new Gson();

                        //Utility.getSharedInstance().dismissProgressDialog();


                        Type listType = new TypeToken<List<SongDetail>>() {
                        }.getType();
                        if (responseData.get("respon") != null) {

                            JsonArray jsonArray = responseData.get("respon").getAsJsonArray();


                            // JsonObject jsonObject=responseData.getAsJsonObject("respon");

                            if (jsonArray.size() > 0) {

                                mSongss = gson.fromJson(responseData.get("respon"), listType);
                                mTxtWarn.setVisibility(View.GONE);
                                mMySongsListView.setVisibility(View.VISIBLE);
                                swipyRefreshLayoutDirection.setRefreshing(false);


//                                AudioSerachSongsAdapter mediatationMoreAdapter = new AudioSerachSongsAdapter(getActivity(), mSongs);

                                mediatationMoreAdapter = new MediatationMoreAdapter(getActivity(), mSongss, FragmentMeditation.this);
                                mMySongsListView.setAdapter(mediatationMoreAdapter);

                                if (mSongss != null) {
                                    if (mSongss.size() < 0) {
                                        swipyRefreshLayoutDirection.setRefreshing(false);
                                        mTxtWarn.setVisibility(View.VISIBLE);
                                        mMySongsListView.setVisibility(View.GONE);
                                    }
                                }
                            } else {
                                swipyRefreshLayoutDirection.setRefreshing(false);
                                mTxtWarn.setVisibility(View.VISIBLE);
                                mMySongsListView.setVisibility(View.GONE);
                                //Utility.getSharedInstance().dismissProgressDialog();
                            }


                        }


                        // Utility.getSharedInstance().Snackbar(toolbarLayout, "Password Changed Sucessfully");


                    } else {
                        swipyRefreshLayoutDirection.setRefreshing(false);
                        //Utility.getSharedInstance().dismissProgressDialog();
                        mTxtWarn.setVisibility(View.VISIBLE);
                        mMySongsListView.setVisibility(View.GONE);

                    }

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                    //Utility.getSharedInstance().dismissProgressDialog();
                    mTxtWarn.setVisibility(View.VISIBLE);
                    mMySongsListView.setVisibility(View.GONE);

                }


            });
        } else {

        }
    }


    // end on Land selected

    public void getCateGorySongs(String catID) {


        MyApplication myApplication = new MyApplication();

        Utility.getSharedInstance().showProgressDialog(getActivity());


        if (Utility.getSharedInstance().isConnectedToInternet(getContext())) {

            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");
            String cat = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT, "1");
            // id="1";
            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");

            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().getSongsByCatID(X_API_KEY, catID, "10", "0");
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());

                    if (responseData.get("error").getAsInt() == 0) {
                        // Config.getPlanDAO = new GetPlanDAO();
                        Gson gson = new Gson();

                        Utility.getSharedInstance().dismissProgressDialog();


                        Type listType = new TypeToken<List<SongDetail>>() {
                        }.getType();
                        if (responseData.get("respon") != null) {

                            JsonArray jsonArray = responseData.get("respon").getAsJsonArray();


                            // JsonObject jsonObject=responseData.getAsJsonObject("respon");

                            if (jsonArray.size() > 0) {

                                mSongss = gson.fromJson(responseData.get("respon"), listType);
                                mTxtWarn.setVisibility(View.GONE);
                                mMySongsListView.setVisibility(View.VISIBLE);
                                swipyRefreshLayoutDirection.setRefreshing(false);


                                mediatationMoreAdapter = new MediatationMoreAdapter(getActivity(), mSongss, FragmentMeditation.this);
                                mMySongsListView.setAdapter(mediatationMoreAdapter);
                            } else {
                                swipyRefreshLayoutDirection.setRefreshing(false);
                                mTxtWarn.setVisibility(View.VISIBLE);
                                mMySongsListView.setVisibility(View.GONE);
                                Utility.getSharedInstance().dismissProgressDialog();
                            }


                        }


                        // Utility.getSharedInstance().Snackbar(toolbarLayout, "Password Changed Sucessfully");


                    } else {
                        swipyRefreshLayoutDirection.setRefreshing(false);
                        Utility.getSharedInstance().dismissProgressDialog();
                        mTxtWarn.setVisibility(View.VISIBLE);
                        mMySongsListView.setVisibility(View.GONE);

                    }

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                    Utility.getSharedInstance().dismissProgressDialog();
                    mTxtWarn.setVisibility(View.VISIBLE);
                    mMySongsListView.setVisibility(View.GONE);

                }


            });
        } else {

        }
    }


    public void playPoase(int postion) {
        PostionOfPlay = postion;
        try {
            if (mSongss != null)
                for (int i = 0; i < mSongss.size(); i++) {
                    if (i != postion)
                        mSongss.get(i).setPlayStop(false);

                }
            if (mSongss.get(postion).getPlayStop()) {
                mSongss.get(postion).setPlayStop(false);
            } else {
                mSongss.get(postion).setPlayStop(true);
            }

            mediatationMoreAdapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void getSearchedSongsWithPagination(final int currentPage) {


        MyApplication myApplication = new MyApplication();

        Utility.getSharedInstance().showProgressDialog(getActivity());


        RequestSearchMedia requestSearchMedia = new RequestSearchMedia();

        requestSearchMedia.setLimit("10");
        requestSearchMedia.setOffset(String.valueOf(currentPage));
        requestSearchMedia.setSearchterm("");


        if (Utility.getSharedInstance().isConnectedToInternet(getContext())) {

//            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");
//            String cat = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT, "1");
//            // id="1";
//            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");

            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().searchMeditation(X_API_KEY, requestSearchMedia);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());

                    if (responseData.get("error").getAsInt() == 0) {
                        // Config.getPlanDAO = new GetPlanDAO();
                        Gson gson = new Gson();

                        Utility.getSharedInstance().dismissProgressDialog();


                        Type listType = new TypeToken<List<SongDetail>>() {
                        }.getType();
                        if (responseData.get("respon") != null) {

                            JsonArray jsonArray = responseData.get("respon").getAsJsonArray();


                            // JsonObject jsonObject=responseData.getAsJsonObject("respon");

                            if (jsonArray.size() > 0) {

                                /*mSongss = gson.fromJson(responseData.get("respon"), listType);
                                mTxtWarn.setVisibility(View.GONE);
                                mMySongsListView.setVisibility(View.VISIBLE);
                                swipyRefreshLayoutDirection.setRefreshing(false);


//                                AudioSerachSongsAdapter mediatationMoreAdapter = new AudioSerachSongsAdapter(getActivity(), mSongs);

                                mediatationMoreAdapter = new MediatationMoreAdapter(getActivity(), mSongss, FragmentMeditation.this);
                                mMySongsListView.setAdapter(mediatationMoreAdapter);*/
                                mTxtWarn.setVisibility(View.GONE);
                                mMySongsListView.setVisibility(View.VISIBLE);
                                if (currentPage == 0) {
                                    mSongss = gson.fromJson(responseData.get("respon"), listType);
                                    swipyRefreshLayoutDirection.setRefreshing(false);
                                    getMusicList(mSongss);
                                } else {
                                    ArrayList<SongDetail> datumList = gson.fromJson(responseData.get("respon"), listType);
                                    mSongss.addAll(mSongss.size(), datumList);
                                    swipyRefreshLayoutDirection.setRefreshing(false);
                                    getUpdatedList();
                                }
                            } else {
                                if (mSongss == null) {
                                    mTxtWarn.setVisibility(View.VISIBLE);
                                    mMySongsListView.setVisibility(View.GONE);
                                }
                                swipyRefreshLayoutDirection.setRefreshing(false);
                            }


                        }


                        // Utility.getSharedInstance().Snackbar(toolbarLayout, "Password Changed Sucessfully");


                    } else {
                        swipyRefreshLayoutDirection.setRefreshing(false);
                        Utility.getSharedInstance().dismissProgressDialog();
                        mTxtWarn.setVisibility(View.VISIBLE);
                        mMySongsListView.setVisibility(View.GONE);

                    }

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                    Utility.getSharedInstance().dismissProgressDialog();
                    Toast.makeText(getActivity(), "Server error .. Please try again after some time", Toast.LENGTH_SHORT).show();
                   /* mTxtWarn.setVisibility(View.VISIBLE);
                    mMySongsListView.setVisibility(View.GONE);*/

                }


            });
        } else {

        }
    }


    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {

            case R.id.layMember:
                SharedPreferenceManager.getInstance().writeInteger(SharedPreferenceManager.KEY_MED_TYPE_ID, 1);
                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_MED_TOOLNAME," Meditation");
                ((DashBoardActivity)getActivity()).openMedtation(1);
                getSearchedSOngs("");
                mEditSearch.setText("");
                post=false;
                request=false;



                post=false;
                mTxtRequest.setTextColor(getResources().getColor(R.color.white));
                mTxtMember.setTextColor(getResources().getColor(R.color.black));

                mTxtPost.setTextColor(getResources().getColor(R.color.white));
                mLayPost.setBackgroundColor(getResources().getColor(R.color.darker_grey));
                mLayMembers.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                mLayRequest.setBackgroundColor(getResources().getColor(R.color.darker_grey));

                break;
            case R.id.layRequest:
                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_MED_TOOLNAME," Workshop");
                SharedPreferenceManager.getInstance().writeInteger(SharedPreferenceManager.KEY_MED_TYPE_ID, 2);
                ((DashBoardActivity)getActivity()).openMedtation(2);
                getSearchedSOngs("");
                mEditSearch.setText("");
                post=false;
                request=true;


//                getRequetstData(userID);
                mTxtRequest.setTextColor(getResources().getColor(R.color.black));
                mTxtMember.setTextColor(getResources().getColor(R.color.white));

                mTxtPost.setTextColor(getResources().getColor(R.color.white));
                mLayPost.setBackgroundColor(getResources().getColor(R.color.darker_grey));

                mLayMembers.setBackgroundColor(getResources().getColor(R.color.darker_grey));
                mLayRequest.setBackgroundColor(getResources().getColor(R.color.colorPrimary));


                break;

            case R.id.layPost:

                SharedPreferenceManager.getInstance().writeInteger(SharedPreferenceManager.KEY_MED_TYPE_ID, 3);
                SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_MED_TOOLNAME," Meditation Tec..");
                ((DashBoardActivity)getActivity()).openMedtation(3);
                getSearchedSOngs("");

                request=false;
                mEditSearch.setText("");
//                getCommunityPost(0);
                post=true;
//                getRequetstData(userID);
                mTxtRequest.setTextColor(getResources().getColor(R.color.white));
                mTxtMember.setTextColor(getResources().getColor(R.color.white));
                mTxtPost.setTextColor(getResources().getColor(R.color.black));

                mLayMembers.setBackgroundColor(getResources().getColor(R.color.darker_grey));
                mLayRequest.setBackgroundColor(getResources().getColor(R.color.darker_grey));
                mLayPost.setBackgroundColor(getResources().getColor(R.color.colorPrimary));



                break;


            default:

                break;

        }

    }

    private void firstColor() {
        SharedPreferenceManager.getInstance().writeInteger(SharedPreferenceManager.KEY_MED_TYPE_ID, 1);

        mTxtRequest.setTextColor(getResources().getColor(R.color.white));
        mTxtMember.setTextColor(getResources().getColor(R.color.black));

        mTxtPost.setTextColor(getResources().getColor(R.color.white));
        mLayPost.setBackgroundColor(getResources().getColor(R.color.darker_grey));
        mLayMembers.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        mLayRequest.setBackgroundColor(getResources().getColor(R.color.darker_grey));
        SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_MED_TOOLNAME," Meditation");
        ((DashBoardActivity)getActivity()).openMedtation(1);

        getSearchedSOngs("");
    }
}

