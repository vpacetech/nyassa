package com.music.nyasaa.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.music.nyasaa.R;

public class FragmentHowToUse extends Fragment {

    private View myView;
    private static final String ARG_KEY_NUMBER = "tab_number";

    private static Context context;
    private RecyclerView mRecycleView;


    public static FragmentHowToUse newInstance(int number) {
        Bundle args = new Bundle();
        args.putInt(ARG_KEY_NUMBER, number);
        FragmentHowToUse frag = new FragmentHowToUse();
        frag.setArguments(args);
        return frag;
    }


    public FragmentHowToUse() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        myView = inflater.inflate(R.layout.fragment_how_to_use, container, false);
        initView(myView);
//        getImges();
        return myView;
    }


    private void initView(View myView) {


        myView.getRootView().setBackgroundColor(getResources().getColor(R.color.orBLACK));

        mRecycleView = (RecyclerView) myView.findViewById(R.id.howTouseRecycleView);
        mRecycleView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, true));
       ;

    }


    /// get all Images
//
//
//    public void getImges() {
//
//        MyApplication myApplication = new MyApplication();
//
//
//        if (Utility.getSharedInstance().isConnectedToInternet(getActivity())) {
//
//            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");
//            Utility.getSharedInstance().showProgressDialog(getActivity());
//            // id="1";
//            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");
//
//            String cat = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT, "1");
//            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().getAllImages(X_API_KEY, "16", "0");
//            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
//            callbackLogin.enqueue(new Callback<JsonObject>() {
//                @Override
//                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//                    // JsonObject responseData = response.body();
//                    JsonObject responseData = response.body();
//                    // Log.e("Response::", responseData.toString());
//
//                    if (responseData != null) {
//
//                        Gson gson = new Gson();
//
//                        GetTuturialModelRespnse getTuturialModelRespnse = gson.fromJson(responseData, GetTuturialModelRespnse.class);
//                        ArrayList<GetTuturialModelRespnseData> getTuturialModelRespnseData = getTuturialModelRespnse.getRespon();
//
//                        HowToUseAdapter howToUseAdapter = new HowToUseAdapter(getActivity(), getTuturialModelRespnseData);
//                        mRecycleView.setAdapter(howToUseAdapter);
//                        mRecycleView.smoothScrollToPosition(0);
//
//                        Utility.getSharedInstance().dismissProgressDialog();
//
////                        Toast.makeText(getActivity(), "Please scroll from LEFT TO RIGHT", Toast.LENGTH_SHORT).show();
//
//
//                    } else {
//                        Utility.getSharedInstance().dismissProgressDialog();
//                    }
//
//                }
//                // Utility.getSharedInstance().Snackbar(toolbarLayout, "Password Changed Sucessfully");
//
//
//                @Override
//                public void onFailure(Call<JsonObject> call, Throwable t) {
//                    Utility.getSharedInstance().dismissProgressDialog();
//
//                }
//            });
//        } else {
//            Utility.getSharedInstance().dismissProgressDialog();
//
//            Toast.makeText(getActivity(), "No internet connection available", Toast.LENGTH_SHORT).show();
//        }
//    }
}