package com.music.nyasaa.fragments.mysongs;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.music.nyasaa.R;
import com.music.nyasaa.adapter.ShortSongsAdapter;
import com.music.nyasaa.controller.MyApplication;
import com.music.nyasaa.models.RequestModel.SearchMedia.RequestSearchMedia;
import com.music.nyasaa.models.SongDetail;
import com.music.nyasaa.utility.SharedPreferenceManager;
import com.music.nyasaa.utility.Utility;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.music.nyasaa.front_end_layer.activities.dashbaord.DashBoardActivity.PostionOfPlay;
import static com.music.nyasaa.retrofit.Config.X_API_KEY;
import static com.music.nyasaa.retrofit.Config.clientService;

public class FragmentMySongs extends Fragment {

    private View myView;
    public static FragmentMySongs fragmentMySongss;
    private static final String ARG_KEY_NUMBER = "tab_number";
    private static Context context;
    public static ArrayList<SongDetail> mSongss = new ArrayList<>();
    private SwipyRefreshLayout swipyRefreshLayoutDirection;
    private TextView mTxtWarn;
    ShortSongsAdapter shortSongsAdapter;
    private RecyclerView mMySongsListView;
    private FragmentMySongs fragmentMySongs;
    private EditText mEditSearch;
    private TextView mTxtLang;
    private int currentPage = 0;
    boolean isSearch = false;

    public static FragmentMySongs newInstance(int number) {
        Bundle args = new Bundle();
        args.putInt(ARG_KEY_NUMBER, number);
        FragmentMySongs frag = new FragmentMySongs();
        frag.setArguments(args);
        return frag;
    }


    public FragmentMySongs() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        myView = inflater.inflate(R.layout.fragment_mysongs, container, false);
        initView(myView);
        fragmentMySongss = this;
        return myView;
    }


    private void initView(View myView) {

//        myView.getRootView().setBackgroundColor(Color.BLACK);
        myView.getRootView().setBackgroundColor(getResources().getColor(R.color.md_black_1000));

//        myView.getRootView().setBackgroundResource(R.drawable.black_gradient);
        mTxtLang = (TextView) myView.findViewById(R.id.txtLang);
        mTxtWarn = (TextView) myView.findViewById(R.id.txtWarn);
        mTxtWarn.setVisibility(View.GONE);
        swipyRefreshLayoutDirection = (SwipyRefreshLayout) myView.findViewById(R.id.swipe_refresh_layout);
        swipyRefreshLayoutDirection.setDirection(SwipyRefreshLayoutDirection.TOP);
        mMySongsListView = (RecyclerView) myView.findViewById(R.id.listViewAlbum);
        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        mMySongsListView.setLayoutManager(linearLayoutManager);
        mMySongsListView.setItemAnimator(new DefaultItemAnimator());

        mMySongsListView.setItemViewCacheSize(20);
        mMySongsListView.setDrawingCacheEnabled(true);
        mMySongsListView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        // MyPlayListAdapter connectionAdapter = new MyPlayListAdapter(getActivity(),img,name);
        // mMySongsListView.setAdapter(connectionAdapter);
//        mMySongsListView =(RecyclerView)myView.findViewById(R.id.listViewAlbum);
        currentPage = 0;
        mEditSearch = (EditText) myView.findViewById(R.id.editSearch);
        mEditSearch.setText("");

        mEditSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                //SharedPreferenceManager.getInstance().writeString(SharedPreferenceManager.KEY_LANG_ID,"");
                //getSearchedSOngs(mEditSearch.getText().toString().trim());
                currentPage=0;
                isSearch = true;
                if (mEditSearch.getText().toString().length() == 0)
                    getSearchedSOngs(mEditSearch.getText().toString().trim(), 0);
                else if (mEditSearch.getText().toString().length() == 1)
                    getSearchedSOngs(mEditSearch.getText().toString().trim(), 0);
                else if (mEditSearch.getText().toString().length() > 0)
                    getSearchedSOngs(mEditSearch.getText().toString().trim(), 0);
                else if (mEditSearch.getText().toString().length() > 1)
                    getSearchedSOngs(mEditSearch.getText().toString().trim(), 0);
                else if (mEditSearch.getText().toString().length() > 2)
                    getSearchedSOngs(mEditSearch.getText().toString().trim(), 0);
                else if (mEditSearch.getText().toString().length() > 3)
                    getSearchedSOngs(mEditSearch.getText().toString().trim(), 0);
                else if (mEditSearch.getText().toString().length() > 4)
                    getSearchedSOngs(mEditSearch.getText().toString().trim(), 0);
                else if (mEditSearch.getText().toString().length() > 5)
                    getSearchedSOngs(mEditSearch.getText().toString().trim(), 0);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        String more = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_TXT_MORE, "none");

//        if (more.equalsIgnoreCase("more")) {
//
//            getSearchedSOngs("");
//        } else {
//            getAllSongs(0);
//        }
        currentPage = 0;
        firstTimeAPICALL(0);


        swipyRefreshLayoutDirection.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                if (direction == SwipyRefreshLayoutDirection.TOP) {
                    //firstTimeAPICALL(0);
                    if(isSearch)
                        getSearchedSOngs(mEditSearch.getText().toString().trim(),0);
                    else
                        getSearchedSOngs("",0);
                    currentPage = 0;
                } else {
                    //firstTimeAPICALL(currentPage);
                    if(isSearch)
                        getSearchedSOngs(mEditSearch.getText().toString().trim(),0);
//                    else
//                      //  getSearchedSOngs("",currentPage);
                }
            }
        });
    }


    public void getAllSongs(final int currentPage) {


        MyApplication myApplication = new MyApplication();

        Utility.getSharedInstance().showProgressDialog(getActivity());


        if (Utility.getSharedInstance().isConnectedToInternet(getContext())) {

            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");
            String cat = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT, "1");
            // id="1";
            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");

            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().getAllTracksByCategory(X_API_KEY, cat, "10", String.valueOf(currentPage));
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    // JsonObject responseData = response.body();
                    JsonObject responseData = response.body();

                    if (responseData.get("error").getAsInt() == 0) {
                        // Config.getPlanDAO = new GetPlanDAO();
                        Gson gson = new Gson();
                        Utility.getSharedInstance().dismissProgressDialog();
                        Type listType = new TypeToken<List<SongDetail>>() {
                        }.getType();

                        if (responseData.get("respon") != null) {
                            JsonArray jsonArray = responseData.get("respon").getAsJsonArray();

                            if (jsonArray.size() > 0) {
                                mTxtWarn.setVisibility(View.GONE);
                                mMySongsListView.setVisibility(View.VISIBLE);
                                if (currentPage == 0) {
                                    mSongss = gson.fromJson(responseData.get("respon"), listType);
                                    swipyRefreshLayoutDirection.setRefreshing(false);
                                    getMusicList(mSongss);
                                } else {
                                    ArrayList<SongDetail> datumList = gson.fromJson(responseData.get("respon"), listType);
                                    mSongss.addAll(mSongss.size(), datumList);
                                    swipyRefreshLayoutDirection.setRefreshing(false);
                                    getUpdatedList();
                                }

                                if (mSongss != null) {
                                    if (mSongss.size() < 0) {
                                        swipyRefreshLayoutDirection.setRefreshing(false);
                                        mTxtWarn.setVisibility(View.VISIBLE);
                                        mMySongsListView.setVisibility(View.GONE);
                                    }
                                }
                            } else {
                                swipyRefreshLayoutDirection.setRefreshing(false);
                                if (mSongss != null && mSongss.size() > 0) {
                                    mTxtWarn.setVisibility(View.VISIBLE);
                                    mMySongsListView.setVisibility(View.GONE);
                                }
                                Utility.getSharedInstance().dismissProgressDialog();
                            }
                        }
                        // Utility.getSharedInstance().Snackbar(toolbarLayout, "Password Changed Sucessfully");
                    } else {
                        swipyRefreshLayoutDirection.setRefreshing(false);
                        Utility.getSharedInstance().dismissProgressDialog();
                        mTxtWarn.setVisibility(View.VISIBLE);
                        mMySongsListView.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Utility.getSharedInstance().dismissProgressDialog();
                    /*mTxtWarn.setVisibility(View.VISIBLE);
                    mMySongsListView.setVisibility(View.GONE);*/
                    swipyRefreshLayoutDirection.setRefreshing(false);
                    Toast.makeText(getActivity(), "Server error .. Please try again after some time", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            swipyRefreshLayoutDirection.setRefreshing(false);

        }
    }

    private void getMusicList(ArrayList<SongDetail> songDetailList) {
        shortSongsAdapter = new ShortSongsAdapter(getActivity(), songDetailList, FragmentMySongs.this);
        mMySongsListView.setAdapter(shortSongsAdapter);

        if (shortSongsAdapter != null)
            shortSongsAdapter.notifyDataSetChanged();
        currentPage += 10;
    }

    private void getUpdatedList() {
//        if (shortSongsAdapter != null)
//            shortSongsAdapter.notifyDataSetChanged();
//        currentPage += 10;
    }

    /////////////////////GET SERACHED SONGS//////////////////////////


    public void getSearchedSOngs(final String searchText, final int currentPage) {

        String lang = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_LANG_ID, "1");
        if (lang.equalsIgnoreCase("4")) {
            mTxtLang.setText("English");
        } else if(lang.equalsIgnoreCase("3")){
            mTxtLang.setText("Hindi");
        }
        else if(lang.equalsIgnoreCase("1")){
            mTxtLang.setText("Marathi");
        }
        else if(lang.equalsIgnoreCase("2")){
            mTxtLang.setText("Telugu");
        }

        if(currentPage == 0 && searchText.equalsIgnoreCase("")){
            mSongss = new ArrayList<>();
            isSearch = false;
        }

        MyApplication myApplication = new MyApplication();

        //Utility.getSharedInstance().showProgressDialog(getActivity());


        RequestSearchMedia requestSearchMedia = new RequestSearchMedia();
        if(isSearch) {
            requestSearchMedia.setLimit("100000");
        } else{
            requestSearchMedia.setLimit("1000000");
        }

        requestSearchMedia.setQuedisc(1);
        requestSearchMedia.setShould_orderby_name(false);
        /*if (searchText.length() > 0) {
//            requestSearchMedia.setAlbum_id(SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_LANG_ID,"none"));
        }
        else {
            requestSearchMedia.setAlbum_id(SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_LANG_ID,"1"));

        }*/
        requestSearchMedia.setAlbum_id_new(SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_LANG_ID,"1"));
        requestSearchMedia.setUser_id(SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,""));
        requestSearchMedia.setMedia_type_id(SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT_ORIGINAL, "none"));
        requestSearchMedia.setOffset(String.valueOf(currentPage));
        requestSearchMedia.setSearchterm(searchText);
        String cat = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT, "0");
//        String cat= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT,"0");
//        requestSearchMedia.setCategory(cat);


        if (Utility.getSharedInstance().isConnectedToInternet(getContext())) {

            //Utility.getSharedInstance().showProgressDialog(getActivity());

//            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");
//            String cat = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT, "1");
//            // id="1";
//            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");

            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().getSongsNew(clientService,X_API_KEY, requestSearchMedia);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();


                    if (responseData != null) {

                        if (responseData.get("respon").getAsJsonArray()!=null) {
                            // Config.getPlanDAO = new GetPlanDAO();
                            Gson gson = new Gson();

//                            Utility.getSharedInstance().dismissProgressDialog();


                            Type listType = new TypeToken<List<SongDetail>>() {
                            }.getType();
                            if (responseData.get("respon") != null) {

                                JsonArray jsonArray = responseData.get("respon").getAsJsonArray();

                                if (jsonArray.size() > 0) {
                                    mTxtWarn.setVisibility(View.GONE);
                                    mMySongsListView.setVisibility(View.VISIBLE);
                                    if (currentPage == 0) {
                                        mSongss = gson.fromJson(responseData.get("respon"), listType);
                                        swipyRefreshLayoutDirection.setRefreshing(false);
                                        getMusicList(mSongss);
                                    } else {
                                        ArrayList<SongDetail> datumList = gson.fromJson(responseData.get("respon"), listType);
                                        mSongss.addAll(mSongss.size(), datumList);
                                        swipyRefreshLayoutDirection.setRefreshing(false);
                                        getUpdatedList();
                                    }

                                    if (mSongss != null) {
                                        if (mSongss.size() < 0) {
                                            swipyRefreshLayoutDirection.setRefreshing(false);
                                            mTxtWarn.setVisibility(View.VISIBLE);
                                            mMySongsListView.setVisibility(View.GONE);
                                        }
                                    }
                                } else {
                                    swipyRefreshLayoutDirection.setRefreshing(false);
                                    if (mSongss != null && mSongss.size() > 0 && searchText.equalsIgnoreCase("")) {
                                        mTxtWarn.setVisibility(View.GONE);
                                        mMySongsListView.setVisibility(View.VISIBLE);
                                        Toast.makeText(getActivity(), "No more songs", Toast.LENGTH_SHORT).show();
                                    } else {
                                        mTxtWarn.setVisibility(View.VISIBLE);
                                        mMySongsListView.setVisibility(View.GONE);
                                    }
                                    /*if (mSongss != null && mSongss.size() == 0) {
                                        mTxtWarn.setVisibility(View.VISIBLE);
                                        mMySongsListView.setVisibility(View.GONE);
                                    } else {
                                        mTxtWarn.setVisibility(View.GONE);
                                        mMySongsListView.setVisibility(View.VISIBLE);
                                        Toast.makeText(getActivity(), "No more songs", Toast.LENGTH_SHORT).show();
                                    }*/
//                                    Utility.getSharedInstance().dismissProgressDialog();
                                }
                            }
                            // Utility.getSharedInstance().Snackbar(toolbarLayout, "Password Changed Sucessfully");
                        }


                    } else {
                        swipyRefreshLayoutDirection.setRefreshing(false);
//                        Utility.getSharedInstance().dismissProgressDialog();
                        mTxtWarn.setVisibility(View.VISIBLE);
                        mMySongsListView.setVisibility(View.GONE);

                    }

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

//                    Utility.getSharedInstance().dismissProgressDialog();
                    mTxtWarn.setVisibility(View.VISIBLE);
                    mMySongsListView.setVisibility(View.GONE);
                    //Toast.makeText(getActivity(), "Server error .. Please try again after some time", Toast.LENGTH_SHORT).show();
                    swipyRefreshLayoutDirection.setRefreshing(false);
                }


            });
        } else {
            swipyRefreshLayoutDirection.setRefreshing(false);
        }
    }


    /// First Time Screen


    public void firstTimeAPICALL(final int currentPage) {

        String lang = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_LANG_ID, "1");
        if (lang.equalsIgnoreCase("4")) {
            mTxtLang.setText("English");
        } else if(lang.equalsIgnoreCase("3")){
            mTxtLang.setText("Hindi");
        }
        else if(lang.equalsIgnoreCase("1")){
            mTxtLang.setText("Marathi");
        }
        else if(lang.equalsIgnoreCase("2")){
            mTxtLang.setText("Telugu");
        }



        MyApplication myApplication = new MyApplication();

        //Utility.getSharedInstance().showProgressDialog(getActivity());


        RequestSearchMedia requestSearchMedia = new RequestSearchMedia();
        if(isSearch) {
            requestSearchMedia.setLimit("100000");
        } else{
            requestSearchMedia.setLimit("100000");
        }

        requestSearchMedia.setQuedisc(1);
        requestSearchMedia.setShould_orderby_name(false);
        /*if (searchText.length() > 0) {
//            requestSearchMedia.setAlbum_id(SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_LANG_ID,"none"));
        }
        else {
            requestSearchMedia.setAlbum_id(SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_LANG_ID,"1"));

        }*/
        requestSearchMedia.setUser_id(SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,""));
        requestSearchMedia.setAlbum_id_new(SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_LANG_ID,"1"));

        requestSearchMedia.setMedia_type_id(SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT_ORIGINAL, "none"));
        requestSearchMedia.setOffset(String.valueOf(currentPage));

        String cat = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT, "0");
//        String cat= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT,"0");
//        requestSearchMedia.setCategory(cat);

        if (Utility.getSharedInstance().isConnectedToInternet(getContext())) {

            Utility.getSharedInstance().showProgressDialog(getActivity());

//            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");
//            String cat = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT, "1");
//            // id="1";
//            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");

            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().getSongsNew(clientService,X_API_KEY, requestSearchMedia);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();


                    if (responseData != null) {


                        if (responseData.get("respon").getAsJsonArray() != null) {
                            // Config.getPlanDAO = new GetPlanDAO();
                            Gson gson = new Gson();

//                            Utility.getSharedInstance().dismissProgressDialog();


                            Type listType = new TypeToken<List<SongDetail>>() {
                            }.getType();
                            if (responseData.get("respon") != null) {

                                JsonArray jsonArray = responseData.get("respon").getAsJsonArray();

                                if (jsonArray.size() > 0) {
                                    mTxtWarn.setVisibility(View.GONE);
                                    Utility.getSharedInstance().dismissProgressDialog();

                                    mMySongsListView.setVisibility(View.VISIBLE);
                                    if (currentPage == 0) {
                                        mSongss = gson.fromJson(responseData.get("respon"), listType);
                                        swipyRefreshLayoutDirection.setRefreshing(false);
                                        getMusicList(mSongss);
                                    } else {
                                        ArrayList<SongDetail> datumList = gson.fromJson(responseData.get("respon"), listType);
                                        mSongss.addAll(mSongss.size(), datumList);
                                        swipyRefreshLayoutDirection.setRefreshing(false);
                                        getUpdatedList();
                                    }

                                    if (mSongss != null) {
                                        if (mSongss.size() < 0) {
                                            swipyRefreshLayoutDirection.setRefreshing(false);
                                            mTxtWarn.setVisibility(View.VISIBLE);
                                            mMySongsListView.setVisibility(View.GONE);
                                        }
                                    }
                                } else {
                                    swipyRefreshLayoutDirection.setRefreshing(false);
                                    if (mSongss != null && mSongss.size() > 0) {
                                        mTxtWarn.setVisibility(View.GONE);
                                        mMySongsListView.setVisibility(View.VISIBLE);

                                        Toast.makeText(getActivity(), "No more songs", Toast.LENGTH_SHORT).show();
                                    } else {
                                        mTxtWarn.setVisibility(View.VISIBLE);
                                        mMySongsListView.setVisibility(View.GONE);
                                        Utility.getSharedInstance().dismissProgressDialog();
                                    }
                                    /*if (mSongss != null && mSongss.size() == 0) {
                                        mTxtWarn.setVisibility(View.VISIBLE);
                                        mMySongsListView.setVisibility(View.GONE);
                                    } else {
                                        mTxtWarn.setVisibility(View.GONE);
                                        mMySongsListView.setVisibility(View.VISIBLE);
                                        Toast.makeText(getActivity(), "No more songs", Toast.LENGTH_SHORT).show();
                                    }*/
//                                    Utility.getSharedInstance().dismissProgressDialog();
                                }
                            }
                            // Utility.getSharedInstance().Snackbar(toolbarLayout, "Password Changed Sucessfully");


                        } else {
                            swipyRefreshLayoutDirection.setRefreshing(false);
                            Utility.getSharedInstance().dismissProgressDialog();
                            mTxtWarn.setVisibility(View.VISIBLE);
                            mMySongsListView.setVisibility(View.GONE);

                        }
                    }

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                    Utility.getSharedInstance().dismissProgressDialog();
                    /*mTxtWarn.setVisibility(View.VISIBLE);
                    mMySongsListView.setVisibility(View.GONE);*/
                    swipyRefreshLayoutDirection.setRefreshing(false);
                    Toast.makeText(getActivity(), "Server error .. Please try again after some time", Toast.LENGTH_SHORT).show();
                }


            });
        } else {
            swipyRefreshLayoutDirection.setRefreshing(false);
        }
    }


    public void getCateGorySongs(final int currentPage) {


        MyApplication myApplication = new MyApplication();

        Utility.getSharedInstance().showProgressDialog(getActivity());


        if (Utility.getSharedInstance().isConnectedToInternet(getContext())) {

            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");
            String cat = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT_ORIGINAL, "1");
            // id="1";
            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");

            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().getSongsByCatID(X_API_KEY, cat, "10", String.valueOf(currentPage));
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.c().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();
                    // Log.e("Response::", responseData.toString());

                    if (responseData != null) {

                        if (responseData.get("error").getAsInt() == 0) {
                            // Config.getPlanDAO = new GetPlanDAO();
                            Gson gson = new Gson();

                            Utility.getSharedInstance().dismissProgressDialog();


                            Type listType = new TypeToken<List<SongDetail>>() {
                            }.getType();
                            if (responseData.get("respon") != null) {

                                JsonArray jsonArray = responseData.get("respon").getAsJsonArray();

                                if (jsonArray.size() > 0) {
                                    mTxtWarn.setVisibility(View.GONE);
                                    mMySongsListView.setVisibility(View.VISIBLE);
                                    if (currentPage == 0) {
                                        mSongss = gson.fromJson(responseData.get("respon"), listType);
                                        swipyRefreshLayoutDirection.setRefreshing(false);
                                        getMusicList(mSongss);
                                    } else {
                                        ArrayList<SongDetail> datumList = gson.fromJson(responseData.get("respon"), listType);
                                        mSongss.addAll(mSongss.size(), datumList);
                                        swipyRefreshLayoutDirection.setRefreshing(false);
                                        getUpdatedList();
                                    }

                                    if (mSongss != null) {
                                        if (mSongss.size() < 0) {
                                            swipyRefreshLayoutDirection.setRefreshing(false);
                                            mTxtWarn.setVisibility(View.VISIBLE);
                                            mMySongsListView.setVisibility(View.GONE);
                                        }
                                    }
                                } else {
                                    swipyRefreshLayoutDirection.setRefreshing(false);

                                    if (mSongss != null && mSongss.size() == 0) {
                                        mTxtWarn.setVisibility(View.VISIBLE);
                                        mMySongsListView.setVisibility(View.GONE);
                                    }
                                    Utility.getSharedInstance().dismissProgressDialog();
                                }
                            }
                            // Utility.getSharedInstance().Snackbar(toolbarLayout, "Password Changed Sucessfully");
                        } else {
                            swipyRefreshLayoutDirection.setRefreshing(false);
                            Utility.getSharedInstance().dismissProgressDialog();
                            mTxtWarn.setVisibility(View.VISIBLE);
                            mMySongsListView.setVisibility(View.GONE);
                        }

                    } else {
                        swipyRefreshLayoutDirection.setRefreshing(false);
                        Utility.getSharedInstance().dismissProgressDialog();
                        mTxtWarn.setVisibility(View.VISIBLE);
                        mMySongsListView.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                    Utility.getSharedInstance().dismissProgressDialog();
                    /*mTxtWarn.setVisibility(View.VISIBLE);
                    mMySongsListView.setVisibility(View.GONE);*/
                    Toast.makeText(getActivity(), "Server error .. Please try again after some time", Toast.LENGTH_SHORT).show();
                }


            });
        } else {

        }
    }


    public void playPoase(int postion) {
        PostionOfPlay = postion;
        try {
            if (mSongss != null)
                for (int i = 0; i < mSongss.size(); i++) {
                    if (i != postion)
                        mSongss.get(i).setPlayStop(false);

                }
            if (mSongss.get(postion).getPlayStop()) {
                mSongss.get(postion).setPlayStop(false);
            } else {
                mSongss.get(postion).setPlayStop(true);
            }

            if (shortSongsAdapter != null)
                shortSongsAdapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

}

