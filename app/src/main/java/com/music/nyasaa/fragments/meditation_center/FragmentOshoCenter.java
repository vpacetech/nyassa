package com.music.nyasaa.fragments.meditation_center;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.music.nyasaa.R;
import com.music.nyasaa.adapter.MediationCenterAdapter;

import static com.music.nyasaa.retrofit.Config.X_API_KEY;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import com.google.gson.reflect.TypeToken;
import com.music.nyasaa.models.OshoCenterDAO;
import com.music.nyasaa.utility.Utility;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FragmentOshoCenter extends Fragment {

    private View myView;
    private static final String ARG_KEY_NUMBER = "tab_number";

    private static Context context;
    List<OshoCenterDAO> AllContryList;
    List<OshoCenterDAO> AllOshoCenterList;

    Spinner spinnerContry,spinnerState;

    List<OshoCenterDAO> AllStateList;

    OshoCenterDAO selectedConrty;
    OshoCenterDAO selectedState;


    //Getting the instance of AutoCompleteTextView
    /*AutoCompleteTextView actv ;
    AutoCompleteTextView state;*/

    String contryId="";
    String Stateid="";

    List<String> CountryWithSelect = new ArrayList<>();
    List<String> StateWithSelect = new ArrayList<>();
    TextView txtWarn;



    RecyclerView list;
    String[] NameList = { "Osho Meditation Centre","Osho Astro Meditation Centre","Osho Amritdham","Osho Silence Valley","Osho Prabhu Kripal Ashram","Osho Prem Dhyan Kendra" };
    String[] AddressList = { "Ladowali Road, Near Alaska Chowk, Jalandhar","old Vijay Nagar Colony Behind Agra Public School, Uttar Pradesh, India","1883, Medical Road Devtal, Garha, Jabalpur, Madhya Pradesh 482003, India","Village Arniala Shahpur, Backside of JCT Mills, Daramshala-Chintpurni Road, Hoshiarpur, Punjab 146001, India"," village Algatpur, PO Ghaghasara bazar, Dist.- Gorakhpur, UP.","Kachnar, Jabalpur, MP" };
    String[] ContactList = { "Not Provided","Swami Dev","Swami Shikhar","Not Provided","Nisang Teerth","Sw Dhyan Diwana" };
    String[] NumberList = { "Not Provided","9219634387","9926549359","75080 80802","9839501228","7803932399" };
    String[] StateList = { "Punjab","UP","MP","Punjab","UP","MP" };
    String[] WebsiteList = { "Not Provided","http://oamcenter.in/","http://www.oshoamritdham.in/","http://oshosilencevalley.com/","under construction","Not Provided" };

    MediationCenterAdapter mediationCenterAdapter;
    private ArrayAdapter<String> adapter;
    private String[] Contry;
    private String[] states;
    private ArrayAdapter<String> adapterState;


    public static FragmentOshoCenter newInstance(int number) {
        Bundle args = new Bundle();
        args.putInt(ARG_KEY_NUMBER, number);
        FragmentOshoCenter frag = new FragmentOshoCenter();
        frag.setArguments(args);
        return frag;
    }


    public FragmentOshoCenter() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        myView = inflater.inflate(R.layout.activity_mediation, container, false);
        initView(myView);

        /*actv = (AutoCompleteTextView) myView.findViewById(R.id.contry);
        state= (AutoCompleteTextView) myView.findViewById(R.id.state);*/
        spinnerContry = myView.findViewById(R.id.contry);
        spinnerState= myView.findViewById(R.id.state);
        txtWarn = myView.findViewById(R.id.txtWarn);


        /*state.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String ss= String.valueOf(s);
                String sts= String.valueOf(s);

               if(AllStateList!=null)
                 if(AllStateList.size()>0)
                for(int i=0;i<AllStateList.size();i++)
                {
                    if(sts.toLowerCase().equals(AllStateList.get(i).getName().toLowerCase()))
                    {
                        selectedState=AllStateList.get(i);

                        getOshocenter(selectedState.getId());
                    }
                }


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        actv.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String ss= String.valueOf(s);
                String sts= String.valueOf(s);
                selectedState=null;
                state.setText("");
                    if(AllContryList!=null)
                    if(AllContryList.size()>0)
                for(int i=0;i<AllContryList.size();i++)
                {
                    if(sts.toLowerCase().equals(AllContryList.get(i).getName().toLowerCase()))
                    {
                        selectedConrty=AllContryList.get(i);
                        getState(selectedConrty.getId());
                    }
                }


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });*/
        getContry();
        return myView;
    }

    private void getOshocenter(String id) {

        com.music.nyasaa.controller.MyApplication myApplication = new com.music.nyasaa.controller.MyApplication();

        //Utility.getSharedInstance().showProgressDialog(getActivity());



        String lang = com.music.nyasaa.utility.SharedPreferenceManager.getInstance().readString(com.music.nyasaa.utility.SharedPreferenceManager.KEY_LANG_ID, "none");
        /*if (lang.equalsIgnoreCase("1")) {
            mTxtLang.setText("English");

        } else {
            mTxtLang.setText("Hindi");
        }*/

        com.music.nyasaa.models.OshoCenterDAO  requestSearchMedia = new com.music.nyasaa.models.OshoCenterDAO();

        requestSearchMedia.setId(id);

        if (Utility.getSharedInstance().isConnectedToInternet(getContext())) {

            Utility.getSharedInstance().showProgressDialog(getActivity());

//            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");
//            String cat = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT, "1");
//            // id="1";
//            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");

            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().getAllAshramByStateId(X_API_KEY,requestSearchMedia);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    JsonObject responseData = response.body();


                    if(responseData!=null) {

                        if (responseData.get("status").getAsBoolean()) {
                            // Config.getPlanDAO = new GetPlanDAO();



                            Gson gson = new Gson();


                            Type listType = new TypeToken<List<OshoCenterDAO>>() {
                            }.getType();
                            if (responseData.get("status").getAsString().equalsIgnoreCase("true")) {


                                Utility.getSharedInstance().dismissProgressDialog();


                                // JsonObject jsonObject=responseData.getAsJsonObject("respon");

                                OshoCenterDAO getCommunityResponse = gson.fromJson(responseData, OshoCenterDAO.class);

                                AllOshoCenterList = getCommunityResponse.getResultObject();
                               // states=null;
                                if(AllOshoCenterList!=null)
                                    if(AllOshoCenterList.size()>0) {
                                        list.setVisibility(View.VISIBLE);
                                        txtWarn.setVisibility(View.GONE);
                                        showOshoCeneter();
                                    } else {
                                        list.setVisibility(View.GONE);
                                        txtWarn.setVisibility(View.VISIBLE);
                                    }



                            }
                            Utility.getSharedInstance().dismissProgressDialog();

                            // Utility.getSharedInstance().Snackbar(toolbarLayout, "Password Changed Sucessfully");
                        }



                    }
                    else {
//                        swipyRefreshLayoutDirection.setRefreshing(false);
                        Utility.getSharedInstance().dismissProgressDialog();

                    }

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                    Utility.getSharedInstance().dismissProgressDialog();


//                    swipyRefreshLayoutDirection.setRefreshing(false);
                }


            });
        } else {
//            swipyRefreshLayoutDirection.setRefreshing(false);
        }


    }

    private void showOshoCeneter() {

        String[] NameList = new String[AllOshoCenterList.size()];
        String[] AddressList= new String[AllOshoCenterList.size()];
        String[] ContactList=new String[AllOshoCenterList.size()];
        String[] NumberList=new String[AllOshoCenterList.size()];
        String[] StateList=new String[AllOshoCenterList.size()];
        String[] WebsiteList= new String[AllOshoCenterList.size()];

        for(int i=0;i< AllOshoCenterList.size();i++)
        {
             NameList[i] = AllOshoCenterList.get(i).getName();
            AddressList[i]=AllOshoCenterList.get(i).getAddress();
            ContactList[i]=AllOshoCenterList.get(i).getContactName();
            NumberList[i]=AllOshoCenterList.get(i).getContactNumber();
            StateList[i]="";
            WebsiteList[i]=AllOshoCenterList.get(i).getWebsite();


        }
        List<String> input = new ArrayList<>();
        Collections.addAll(input, NameList);

        List<String> input1 = new ArrayList<>();
        Collections.addAll(input1, AddressList);

        List<String> input2 = new ArrayList<>();
        Collections.addAll(input2, ContactList);

        List<String> input3 = new ArrayList<>();
        Collections.addAll(input3, NumberList);

        List<String> input4 = new ArrayList<>();
        Collections.addAll(input4, StateList);

        List<String> input5 = new ArrayList<>();
        Collections.addAll(input5, WebsiteList);

        mediationCenterAdapter = new MediationCenterAdapter(getActivity(),AllOshoCenterList,FragmentOshoCenter.this);


        if(AllOshoCenterList.size()==0)
        {
            list.setVisibility(View.GONE);
            txtWarn.setVisibility(View.VISIBLE);
        }
        else {
            list.setVisibility(View.VISIBLE);
            txtWarn.setVisibility(View.GONE);
        }


        if(AllOshoCenterList!=null)
        {
            list.setAdapter(mediationCenterAdapter);
        }



    }


    private void initView(View myView) {

//        myView.getRootView().setBackgroundColor(Color.BLACK);
        myView.getRootView().setBackgroundColor(getResources().getColor(R.color.orBLACK));

//        myView.getRootView().setBackgroundResource(R.drawable.black_gradient);
        list = (RecyclerView) myView.findViewById(R.id.list);


        list.setLayoutManager(new LinearLayoutManager(getActivity()));


        ;


//        mGridAlbumview =(RecyclerView)myView.findViewById(R.id.listViewAlbum);
    }


    public void getState(String id) {


        com.music.nyasaa.controller.MyApplication myApplication = new com.music.nyasaa.controller.MyApplication();

        //Utility.getSharedInstance().showProgressDialog(getActivity());



        String lang = com.music.nyasaa.utility.SharedPreferenceManager.getInstance().readString(com.music.nyasaa.utility.SharedPreferenceManager.KEY_LANG_ID, "none");
        /*if (lang.equalsIgnoreCase("1")) {
            mTxtLang.setText("English");

        } else {
            mTxtLang.setText("Hindi");
        }*/

        com.music.nyasaa.models.OshoCenterDAO  requestSearchMedia = new com.music.nyasaa.models.OshoCenterDAO();

       requestSearchMedia.setId(id);

        if (Utility.getSharedInstance().isConnectedToInternet(getContext())) {

            Utility.getSharedInstance().showProgressDialog(getActivity());

//            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");
//            String cat = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT, "1");
//            // id="1";
//            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");

            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().getAllStateByCountryId(X_API_KEY,requestSearchMedia);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    JsonObject responseData = response.body();


                    if(responseData!=null) {

                        if (responseData.get("status").getAsBoolean()) {
                            // Config.getPlanDAO = new GetPlanDAO();



                            Gson gson = new Gson();


                            Type listType = new TypeToken<List<OshoCenterDAO>>() {
                            }.getType();
                            if (responseData.get("status").getAsString().equalsIgnoreCase("true")) {


                                Utility.getSharedInstance().dismissProgressDialog();


                                // JsonObject jsonObject=responseData.getAsJsonObject("respon");

                                StateWithSelect.clear();


                                OshoCenterDAO getCommunityResponse = gson.fromJson(responseData, OshoCenterDAO.class);

                                AllStateList = getCommunityResponse.getResultObject();
                                states=null;
                                if(AllStateList!=null)
                                    if(AllStateList.size()>0) {
                                        states = new String[AllStateList.size()];

                                        StateWithSelect.add("Select State");

                                        for (int i = 0; i < AllStateList.size(); i++) {
                                            states[i]=AllStateList.get(i).getName();
                                            StateWithSelect.add(AllStateList.get(i).getName());

                                        }
                                        setAapterInforSate();
                                    }



                            }
                            Utility.getSharedInstance().dismissProgressDialog();

                            // Utility.getSharedInstance().Snackbar(toolbarLayout, "Password Changed Sucessfully");
                        }



                    }
                    else {
//                        swipyRefreshLayoutDirection.setRefreshing(false);
                        Utility.getSharedInstance().dismissProgressDialog();

                    }

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                    Utility.getSharedInstance().dismissProgressDialog();


//                    swipyRefreshLayoutDirection.setRefreshing(false);
                }


            });
        } else {
//            swipyRefreshLayoutDirection.setRefreshing(false);
        }
    }

    private void setAapterInforSate() {

        /*adapterState = new ArrayAdapter<String>
                (getContext(), android.R.layout.select_dialog_item, states);
        //Getting the instance of AutoCompleteTextView
        //  AutoCompleteTextView actv = (AutoCompleteTextView) findViewById(R.id.contry);
        state.setThreshold(1);//will start working from first character
        state.setAdapter(adapterState);*/

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, StateWithSelect);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerState.setAdapter(dataAdapter);
        spinnerState.setPrompt("Select Name");
        spinnerState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                /*if (item.equalsIgnoreCase("Select")) {
                    textViewName.setText("");
                } else {
                    textViewName.setText(item);
                }*/


                if(AllStateList!=null)
                {
                    if(AllStateList.size()>0 && position!=0)
                    {
                        selectedState=AllStateList.get(position-1);

                        getOshocenter(selectedState.getId());
                    }

                }
                list.setVisibility(View.GONE);
                txtWarn.setVisibility(View.VISIBLE);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    public void getContry() {


        com.music.nyasaa.controller.MyApplication myApplication = new com.music.nyasaa.controller.MyApplication();

        //Utility.getSharedInstance().showProgressDialog(getActivity());



        String lang = com.music.nyasaa.utility.SharedPreferenceManager.getInstance().readString(com.music.nyasaa.utility.SharedPreferenceManager.KEY_LANG_ID, "none");
        /*if (lang.equalsIgnoreCase("1")) {
            mTxtLang.setText("English");

        } else {
            mTxtLang.setText("Hindi");
        }*/



        if (Utility.getSharedInstance().isConnectedToInternet(getContext())) {

            Utility.getSharedInstance().showProgressDialog(getActivity());

//            String id = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID, "");
//            String cat = SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_CAT, "1");
//            // id="1";
//            // id= SharedPreferenceManager.getInstance().readString(SharedPreferenceManager.KEY_USER_ID,"");

            Call<JsonObject> callbackLogin = myApplication.getAPIInstance().getAllCountry(X_API_KEY);
            //Call<JsonObject> callbackLogin = myApplication.getAPIInstance().userLogin(mEditEmail.getText().toString().trim(), mEditPassword.getText().toString().trim());
            callbackLogin.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    // JsonObject responseData = response.body();

                    JsonObject responseData = response.body();


                    if(responseData!=null) {

                        if (responseData.get("status").getAsBoolean()) {
                            // Config.getPlanDAO = new GetPlanDAO();



                            Gson gson = new Gson();


                            Type listType = new TypeToken<List<OshoCenterDAO>>() {
                            }.getType();
                            if (responseData.get("status").getAsString().equalsIgnoreCase("true")) {


                                Utility.getSharedInstance().dismissProgressDialog();


                                // JsonObject jsonObject=responseData.getAsJsonObject("respon");


                                OshoCenterDAO getCommunityResponse = gson.fromJson(responseData, OshoCenterDAO.class);

                                AllContryList = getCommunityResponse.getResultObject();
                                Contry=null;
                                if(AllContryList!=null)
                                    if(AllContryList.size()>0) {
                                        Contry = new String[AllContryList.size()];

                                        CountryWithSelect.add("Select Country");

                                        for (int i = 0; i < AllContryList.size(); i++) {

                                            Contry[i]=AllContryList.get(i).getName();
                                            CountryWithSelect.add(AllContryList.get(i).getName());
                                            }



                                        setAapterInforContry();
                                    }



                            }
                                Utility.getSharedInstance().dismissProgressDialog();

                            // Utility.getSharedInstance().Snackbar(toolbarLayout, "Password Changed Sucessfully");
                        }


                    }
                    else {
//                        swipyRefreshLayoutDirection.setRefreshing(false);
                        Utility.getSharedInstance().dismissProgressDialog();

                    }

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                    Utility.getSharedInstance().dismissProgressDialog();


//                    swipyRefreshLayoutDirection.setRefreshing(false);
                }


            });
        } else {
//            swipyRefreshLayoutDirection.setRefreshing(false);
        }
    }

    private void setAapterInforContry() {

        /*adapter = new ArrayAdapter<String>
                (getContext(), android.R.layout.select_dialog_item, Contry);
        //Getting the instance of AutoCompleteTextView
        //  AutoCompleteTextView actv = (AutoCompleteTextView) findViewById(R.id.contry);
        actv.setThreshold(1);//will start working from first character
        actv.setAdapter(adapter);*/

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, CountryWithSelect);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerContry.setAdapter(dataAdapter);
        spinnerContry.setPrompt("Select Name");
        spinnerContry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                /*if (item.equalsIgnoreCase("Select")) {
                    textViewName.setText("");
                } else {
                    textViewName.setText(item);
                }*/

                    /*states = new String[1];
                states[0]="Select State";*/

                StateWithSelect=new ArrayList<>();
                StateWithSelect.add("Select State");
                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, StateWithSelect);
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerState.setAdapter(dataAdapter);
                spinnerState.setPrompt("Select State");



                if(AllContryList!=null)
                {
                    if(AllContryList.size()>0 && position!=0)
                    {
                        selectedConrty=AllContryList.get(position-1);
                        getState(selectedConrty.getId());
                    }

                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }


    public void makePhoneCall(String number) {
        String contact_number = number;
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + contact_number));
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            isPhoneCallGranted();
            return;
        }
        startActivity(callIntent);
    }

    public boolean isPhoneCallGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (getActivity().checkSelfPermission(Manifest.permission.CALL_PHONE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("", "Permission is granted");
                return true;
            } else {

                Log.v("", "Permission is revoked");
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installatioxn
            Log.v("", "Permission is granted");
            return true;
        }
    }




}