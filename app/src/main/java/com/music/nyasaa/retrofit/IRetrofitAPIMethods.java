package com.music.nyasaa.retrofit;


import com.google.gson.JsonObject;
import com.music.nyasaa.models.Community_DAO;
import com.music.nyasaa.models.OshoCenterDAO;
import com.music.nyasaa.models.Member_of_Community_DAO;
import com.music.nyasaa.models.RequestModel.AddCommunityModel;
import com.music.nyasaa.models.RequestModel.AddEventRequest;
import com.music.nyasaa.models.RequestModel.AddToCommunity.RequestShareCommunityPost;
import com.music.nyasaa.models.RequestModel.AddToCommunity.RequestToAcceptCommunity;
import com.music.nyasaa.models.RequestModel.AddToCommunity.RequestToAddMember;
import com.music.nyasaa.models.RequestModel.AddToCommunity.UpdateCommunity;
import com.music.nyasaa.models.RequestModel.DeleteMember.RequestDeleteMember;
import com.music.nyasaa.models.RequestModel.DeletePlayList.RequestDeletePlayList;
import com.music.nyasaa.models.RequestModel.GetMyPlayList.RequestGetMyPlayList;
import com.music.nyasaa.models.RequestModel.Create_myplaylist.RequestCreatePlayList;
import com.music.nyasaa.models.RequestModel.OTPmodel;
import com.music.nyasaa.models.RequestModel.RequestUpdateProfile;
import com.music.nyasaa.models.RequestModel.SaveToPlayList.RequestSaveToPlayList;
import com.music.nyasaa.models.RequestModel.SearchMedia.RequestSearchLangMediation;
import com.music.nyasaa.models.RequestModel.SearchMedia.RequestSearchMedia;
import com.music.nyasaa.models.RequestModel.SelectPackageModel;
import com.music.nyasaa.models.RequestModel.SerachUsers.RequestToSearchUser;
import com.music.nyasaa.models.RequestModel.Subscription.AddSubscriptionRequest;
import com.music.nyasaa.models.RequestModel.evnt.RequestParticiptateEvent;
import com.music.nyasaa.models.UpdateEventRequest;
import com.music.nyasaa.models.User_DAO;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Anand on 03//17.
 */

public interface IRetrofitAPIMethods {


    //Admins getUserName Suggestions
    @GET("getAddLiveLink")
    Call<JsonObject> getAds();

    @Headers({"Content-Type: application/json"})
    @GET("slider")
    Call<JsonObject> getSlider(@Header("Client-Service") String Service,@Header("Auth-Key") String key);



    @GET("getBottomsBanner")
    Call<JsonObject> getBanners();

    // @GET("track/alltrackbystyle?id={id}")
    @Headers({"Content-Type: application/json"})
    @GET("track/alltrackbystyle")
    Call<JsonObject> alltrackbystyle(@Query("id") String id, @Query("X-API-KEY") String key);

    // http://52.8.218.210/av_application/endpoint/appusers/login

//    @GET("users/getAllChoiceNo")
//    Call<JsonObject> getAllChoiceNumbers();
//
//    //@FormUrlEncoded getRetailerDATA


//    @Headers({"Content-Type: application/json"})



    @FormUrlEncoded
    @POST("auth/login")
    Call<JsonObject> userLogin(@Header("Client-Service") String client,@Header("Auth-Key") String key,@Field("username") String username,@Field("password") String password);

//
//    @Headers({"Content-Type: application/json"})
//    @POST("appusers/add")
//    Call<JsonObject> userSignUp(@Body User_DAO getAllTimeDAO);


    @Headers({"Content-Type: application/json"})
    @POST("signup")
    Call<JsonObject> userSignUp(@Header("Client-Service")String client,@Header("Auth-Key")String key,@Body User_DAO getAllTimeDAO);



    @Headers({"Content-Type: application/json"})
    @POST("resend-otp")
    Call<JsonObject> resend_otp(@Header("Client-Service")String client,@Header("Auth-Key")String key,@Body User_DAO getAllTimeDAO);





    @Headers({"Content-Type: application/json"})
    @POST("select-package")
    Call<JsonObject> SelectPackage(@Header("Client-Service")String client,@Header("Auth-Key")String key,@Body SelectPackageModel getAllTimeDAO);

    @Headers({"Content-Type: application/json"})
    @POST("otp-status")
    Call<JsonObject> OTP(@Header("Client-Service")String client,@Header("Auth-Key")String key,@Body OTPmodel getAllTimeDAO);





    @Headers({"Content-Type: application/json"})
    @POST("event/addeventuser")
    Call<JsonObject> attendEvent(@Header("X-API-KEY") String key,@Body RequestParticiptateEvent requestParticiptateEvent);



    @Headers({"Content-Type: application/json"})
    @POST("event/removeeventuser")
    Call<JsonObject> removeEvent(@Header("X-API-KEY") String key,@Body RequestParticiptateEvent requestParticiptateEvent);




    // Serach Users


    @Headers({"Content-Type: application/json"})
    @POST("appusers/search")
    Call<JsonObject> searchUsers(@Header("X-API-KEY") String key, @Body RequestToSearchUser requestToSearchUser);



    // Search Community



    // Add Member to Community


    @Headers({"Content-Type: application/json"})
    @POST("community/addusrtocom")
    Call<JsonObject> addMemberToComm(@Header("X-API-KEY") String key, @Body RequestToAddMember requestToAddMember);


    // Add Comm
    @Headers({"Content-Type: application/json"})
    @POST("Community/addcom")
    Call<JsonObject> addcom(@Body Community_DAO getAllTimeDAO);

    // update Community


    // Add Comm
    @Headers({"Content-Type: application/json"})
    @POST("Community/updatecom")
    Call<JsonObject> updateComm(@Header("X-API-KEY") String key,@Body UpdateCommunity updateCommunity);


    // get all tracks by category
    @Headers({"Content-Type: application/json"})
    @GET("track/alltrackbycategory")
    Call<JsonObject> getAllTracksByCategory(@Query("X-API-KEY") String key, @Query("id") String id, @Query("limit") String limit, @Query("offset") String offset);


    // Forgot and Reset Password


    @Headers({"Content-Type: application/json"})
    @POST("forgot-password")
    Call<JsonObject> forgotPassword(@Header("Client-Service") String client,@Header("Auth-Key") String key,@Body User_DAO user_dao);



    @Headers({"Content-Type: application/json"})
    @POST("/reset-password")
    Call<JsonObject> resetPass(@Header("Client-Service") String client,@Header("Auth-Key") String key,@Body User_DAO user_dao);





    // get all interView by category
    @Headers({"Content-Type: application/json"})
    @GET("interview/allinterviewbycategory")
    Call<JsonObject> getInterviwsBycatergory(@Query("X-API-KEY") String key, @Query("type") String type, @Query("limit") String limit, @Query("offset") String offset);




    // get all Meditation by category
    @Headers({"Content-Type: application/json"})
    @GET("meditation/allmeditationbycategory")
    Call<JsonObject> getMeditationBycatergory(@Query("X-API-KEY") String key, @Query("id") String id, @Query("limit") String limit, @Query("offset") String offset);



    // get all Music by category
    @Headers({"Content-Type: application/json"})
    @GET("music/allmusicbycategory")
    Call<JsonObject> getMusicBycatergory(@Query("X-API-KEY") String key, @Query("id") String id, @Query("limit") String limit, @Query("offset") String offset);



    // get all tracks by category
    @Headers({"Content-Type: application/json"})
    @GET("meditation/allmeditationbycategory")
    Call<JsonObject> getAllMeditationByCategory(@Query("X-API-KEY") String key, @Query("id") String id, @Query("limit") String limit, @Query("offset") String offset);





    // get all tracks by category
    @Headers({"Content-Type: application/json"})
    @GET("music/allmusicbycategory")
    Call<JsonObject> getAllMusicByCategory(@Query("X-API-KEY") String key, @Query("id") String id, @Query("limit") String limit, @Query("offset") String offset);


    @Headers({"Content-Type: application/json"})
    @GET("interview/allinterviewbycategory")
    Call<JsonObject> getAllInterViewByCategory(@Query("X-API-KEY") String key, @Query("id") String id, @Query("limit") String limit, @Query("offset") String offset);
// Get Category Songs by ID

    @Headers({"Content-Type: application/json"})
    @GET("videocast/allmediatype")
    Call<JsonObject> getCategoriesOSHO(@Query("X-API-KEY") String key, @Query("limit") String limit, @Query("offset") String offset);


    // GET  All Langiages


    @Headers({"Content-Type: application/json"})
    @GET("getlanguage")
    Call<JsonObject> getAllLangiages(@Header("Client-Service") String client,@Header("Auth-Key") String key);


    //    get all search item Songs
    @Headers({"Content-Type: application/json"})
    @POST("track/searchquedisc")
    Call<JsonObject> searchAudioQA(@Header("X-API-KEY") String key, @Body RequestSearchMedia requestSearchMedia);

    @Headers({"Content-Type: application/json"})
    @POST("gettrack")
    Call<JsonObject> getSongs(@Header("Client-Service") String client,@Header("Auth-Key") String key, @Body RequestSearchMedia requestSearchMedia);



    @Headers({"Content-Type: application/json"})
    @POST("get-album-track")
    Call<JsonObject> getSongsNew(@Header("Client-Service") String client,@Header("Auth-Key") String key, @Body RequestSearchMedia requestSearchMedia);


    //    get all search item Songs
    @Headers({"Content-Type: application/json"})
    @POST("track/search")
    Call<JsonObject> searchAudio(@Header("X-API-KEY") String key, @Body RequestSearchMedia requestSearchMedia);


    // Community Search

    @Headers({"Content-Type: application/json"})
    @POST("community/search/")
    Call<JsonObject> searchCommunity(@Header("X-API-KEY") String key, @Body RequestSearchMedia requestSearchMedia);



    // search Music

    @Headers({"Content-Type: application/json"})
    @POST("music/search/")
    Call<JsonObject> searchMusic(@Header("X-API-KEY") String key, @Body RequestSearchMedia requestSearchMedia);


    // Search Bajan



    @Headers({"Content-Type: application/json"})
    @POST("bhajan/search/")
    Call<JsonObject> searchBhajan(@Header("X-API-KEY") String key, @Body RequestSearchMedia requestSearchMedia);

    // search Satsang



    @Headers({"Content-Type: application/json"})
    @POST("satsang/search/")
    Call<JsonObject> searchSatsang(@Header("X-API-KEY") String key, @Body RequestSearchMedia requestSearchMedia);

    // search Jokes

    @Headers({"Content-Type: application/json"})
    @POST("joke/search/")
    Call<JsonObject> searchJokes(@Header("X-API-KEY") String key, @Body RequestSearchMedia requestSearchMedia);



    // search Medititaion

    @Headers({"Content-Type: application/json"})
    @POST("meditation/search/")
    Call<JsonObject> searchMeditation(@Header("X-API-KEY") String key, @Body RequestSearchMedia requestSearchMedia);


    @Headers({"Content-Type: application/json"})
    @POST("meditation/search/")
    Call<JsonObject> searchMeditationNew(@Header("X-API-KEY") String key, @Body RequestSearchLangMediation requestSearchMedia);



    // search InterViews

    @Headers({"Content-Type: application/json"})
    @POST("interview/search/")
    Call<JsonObject> searchinterviews(@Header("X-API-KEY") String key, @Body RequestSearchMedia requestSearchMedia);

    // search InterViews Videos

    @Headers({"Content-Type: application/json"})
    @POST("interviewvideos/search/")
    Call<JsonObject> searchinterviewsVideos(@Header("X-API-KEY") String key, @Body RequestSearchMedia requestSearchMedia);

// Serach Audio


    //    get all search item Video
    @Headers({"Content-Type: application/json"})
    @POST("videocast/search/")
    Call<JsonObject> searchVideo(@Header("X-API-KEY") String key, @Body RequestSearchMedia requestSearchMedia);



    //    get all search item Video
    @Headers({"Content-Type: application/json"})
    @POST("videocast/searchquedisc/")
    Call<JsonObject> searchVideoQA(@Header("X-API-KEY") String key, @Body RequestSearchMedia requestSearchMedia);

    // Get All Videos

    @Headers({"Content-Type: application/json"})
    @GET("videocast/allvideo")
    Call<JsonObject> getAllVideos(@Header("X-API-KEY") String key, @Query("limit") String limit, @Query("offset") String offset);


    @Headers({"Content-Type: application/json"})
    @POST("return-policy")
    Call<JsonObject> getReturnPolicy(@Header("Client-Service") String Service,@Header("Auth-Key") String key);

    @Headers({"Content-Type: application/json"})
    @POST("terms-and-condition")
    Call<JsonObject> terms(@Header("Client-Service") String Service,@Header("Auth-Key") String key);


    @Headers({"Content-Type: application/json"})
    @POST("supportcontact")
    Call<JsonObject> supportcontact(@Header("Client-Service") String Service,@Header("Auth-Key") String key);

    @Headers({"Content-Type: application/json"})
    @POST("about")
    Call<JsonObject> about(@Header("Client-Service") String Service,@Header("Auth-Key") String key);


    @Headers({"Content-Type: application/json"})
    @POST("get-albums")
    Call<JsonObject> get_albums(@Header("Client-Service") String Service,@Header("Auth-Key") String key,@Body User_DAO user_dao );


    @Headers({"Content-Type: application/json"})
    @POST("privacy-policy")
    Call<JsonObject> privacy(@Header("Client-Service") String Service,@Header("Auth-Key") String key);


    @Headers({"Content-Type: application/json"})
    @GET("videocast/allvideobycategory")
    Call<JsonObject> getAllVideosByCategory(@Header("X-API-KEY") String key, @Query("id") String id, @Query("limit") String limit, @Query("offset") String offset);



//    getTracksBy CategoryID


    @Headers({"Content-Type: application/json"})
    @GET("track/alltrackbymediatype")
    Call<JsonObject> getSongsByCatID(@Header("X-API-KEY") String key, @Query("id") String id, @Query("limit") String limit, @Query("offset") String offset);

    // get Cat videos




    @Headers({"Content-Type: application/json"})
    @GET("videocast/allvideobymediatype")
    Call<JsonObject> getVideosByCatID(@Header("X-API-KEY") String key, @Query("id") String id, @Query("limit") String limit, @Query("offset") String offset);
    // Get All Events
    @Headers({"Content-Type: application/json"})
    @GET("event/all")
    Call<JsonObject> getAllEvents(@Header("X-API-KEY") String key, @Query("limit") String limit, @Query("offset") String offset);



    // Get All Events by user ID
//    @Headers({"Content-Type: application/json"})
    @POST("events")
    Call<JsonObject> getAllEventsByUSerID(@Header("Client-Service") String Service,@Header("Auth-Key") String key);

// Get Package List
    @POST("package-list")
    Call<JsonObject> getAllPackages(@Header("Client-Service") String Service,@Header("Auth-Key") String key);

    // Check User Info
    @Headers({"Content-Type: application/json"})
    @GET("appusers/userinfo/{id}")
    Call<JsonObject> checkUserAvaliableOrNot(@Header("X-API-KEY") String key, @Path("id") String id);


    // GET ALL COMMUNITY REQUEST

    //
    @Headers({"Content-Type: application/json"})
    @GET("community/allcomusrbycomid")
    Call<JsonObject> getCommunitySentRequest(@Header("X-API-KEY") String key, @Query("id") String id, @Query("limit") String limit, @Query("offset") String offset);



    // Get Users for particular Event

    @Headers({"Content-Type: application/json"})
    @GET("event/alluserbyeventid/{id}")
    Call<JsonObject> getALluserforParticluarEvent(@Header("X-API-KEY") String key, @Path("id") String id);


    @Headers({"Content-Type: application/json"})
    @GET("community/allcomusrbyprntusrid/{id}")
    Call<JsonObject> allcomusrbyprntusrid(@Path("id") String id, @Query("X-API-KEY") String key);

    @Headers({"Content-Type: application/json"})
    @POST("community/addusrtocom")
    Call<JsonObject> addusrtocom(@Body Member_of_Community_DAO getAllTimeDAO);

    @Headers({"Content-Type: application/json"})
    @GET("community/allcombyusrid/{id}")
    Call<JsonObject> allcombyusrid(@Path("id") String id, @Query("X-API-KEY") String key);

    // get my playlist

    @Headers({"Content-Type: application/json"})
    @POST("getplaylist")
    Call<JsonObject> getMyPlayList(@Body RequestGetMyPlayList requestGetMyPlayList);

    @Headers({"Content-Type: application/json"})
    @POST("getplaylist")
    Call<JsonObject> getMyPlayList( @Header("Client-Service") String Service,@Header("Auth-Key") String key,@Body RequestGetMyPlayList requestGetMyPlayList);

    // update Profile Image


    @Headers({"Content-Type: application/json"})
    @POST("appusers/update/")
    Call<JsonObject> updateProfileImage(@Body RequestUpdateProfile requestUpdateProfile);


    //get My Songs of PlayList


    @Headers({"Content-Type: application/json"})
    @GET("playtrack/alltrack/}")
    Call<JsonObject> getMySongsFromPlayList(@Header("X-API-KEY") String key, @Query("id") String id);

    @Headers({"Content-Type: application/json"})
    @POST("getplaylist-track")
    Call<JsonObject> getMySongsFromPlayList(@Header("Client-Service") String Service,@Header("Auth-Key") String key,@Body RequestGetMyPlayList requestGetMyPlayList);


    // add Song to myPLayLis

    @Headers({"Content-Type: application/json"})
    @POST("playtrack/add/")
    Call<JsonObject> saveToPlayList(@Header("X-API-KEY") String key, @Body RequestSaveToPlayList requestGetMyPlayList);


    @Headers({"Content-Type: application/json"})
    @POST("add-playlist-track")
    Call<JsonObject> saveToPlayList(@Header("Client-Service") String Service,@Header("Auth-Key") String key, @Body RequestSaveToPlayList requestGetMyPlayList);


    // create my play list

    @Headers({"Content-Type: application/json"})
    @POST("playlist/add")
    Call<JsonObject> createMyPlayList(@Header("X-API-KEY") String key, @Body RequestCreatePlayList requestCreatePlayList);



    @Headers({"Content-Type: application/json"})
    @POST("playlist")
    Call<JsonObject> createMyPlayList(@Header("Client-Service") String Service,@Header("Auth-Key") String key, @Body RequestCreatePlayList requestCreatePlayList);

    // Accept / Reject Community Request



    @Headers({"Content-Type: application/json"})
    @POST("community/acceptrejectcomrequest")
    Call<JsonObject> acceptOrRejectRequest(@Header("X-API-KEY") String key, @Body RequestToAcceptCommunity requestCreatePlayList);

    // Delete PlayLis


    @Headers({"Content-Type: application/json"})
    @POST("playlist/delete")
    Call<JsonObject> deletePlayList(@Body RequestDeletePlayList requestGetMyPlayList);

    @Headers({"Content-Type: application/json"})
    @POST("deleteplaylist")
    Call<JsonObject> deletePlayList(@Header("Client-Service") String Service,@Header("Auth-Key") String key,@Body RequestDeletePlayList requestGetMyPlayList);

    // Delete member from community


    @Headers({"Content-Type: application/json"})
    @POST("community/deleteusrfromcom")
    Call<JsonObject> deleteMembers(@Header("X-API-KEY") String key, @Body RequestDeleteMember requestDeleteMember);



    // Community Add Post


    @Headers({"Content-Type: application/json"})
    @POST("community/addcompost")
    Call<JsonObject> addPostToCommunity(@Header("X-API-KEY") String key, @Body RequestShareCommunityPost requestSearchMedia);


    // POST COMMENT IN COMMUNITY



    @Headers({"Content-Type: application/json"})
    @POST("community/addcomcommentpost")
    Call<JsonObject> postComments(@Header("X-API-KEY") String key, @Body RequestShareCommunityPost requestSearchMedia);


    // get Community Post
    @Headers({"Content-Type: application/json"})
    @GET("community/allcommentpostbycommunityid")
    Call<JsonObject> getallComments(@Header("X-API-KEY") String key, @Query("id") String id, @Query("limit") String limit, @Query("offset") String offset);

// get ALL Comments


    // end

    @Headers({"Content-Type: application/json"})
    @GET("community/allpostbycommunityid")
    Call<JsonObject> getCommunityPost(@Header("X-API-KEY") String key, @Query("id") String id, @Query("limit") String limit, @Query("offset") String offset);


    //  get Community members

    @Headers({"Content-Type: application/json"})
    @GET("community/allcomusrbycomid")
    Call<JsonObject> getallCommunityMembers(@Header("X-API-KEY") String key, @Query("id") String id, @Query("limit") String limit, @Query("offset") String offset);


    //    add community
    @Headers({"Content-Type: application/json"})
    @POST("community/addcom")
    Call<JsonObject> addCommunity(@Header("X-API-KEY") String key, @Body AddCommunityModel addCommunityModel);

    //    add event
    @Headers({"Content-Type: application/json"})
    @POST("event/addevent")
    Call<JsonObject> addEvent(@Header("X-API-KEY") String key, @Body AddEventRequest addEventRequest);

    //    update event
    @Headers({"Content-Type: application/json"})
    @POST("event/updateevent")
    Call<JsonObject> updatEevent(@Header("X-API-KEY") String key, @Body UpdateEventRequest updateEventRequest);


    //    send Image

    @Multipart
    @POST("community/upload/")
    Call<JsonObject> uploadDocument(@Header("X-API-KEY") String key,
                                    @Part MultipartBody.Part file, @Part("name") RequestBody name);



    //    send Image Event

    @Multipart
    @POST("event/upload/")
    Call<JsonObject> uploadEventImage(@Header("X-API-KEY") String key,
                                    @Part MultipartBody.Part file, @Part("name") RequestBody name);




    /// upload Sign Up


    @Multipart
    @POST("appusers/upload/")
    Call<JsonObject> uploadSignUp(@Header("X-API-KEY") String key,
                                    @Part MultipartBody.Part file, @Part("name") RequestBody name);



    @Headers({"Content-Type: application/json"})
    @GET("appusers/userinfo/{id}")
    Call<JsonObject> getUserInfo(@Header("X-API-KEY") String key, @Path("id") String id);

    // Subscription

    @Headers({"Content-Type: application/json"})
    @POST("Subscription/addSubscription")
    Call<JsonObject> addSubscription(@Header("X-API-KEY") String key, @Body AddSubscriptionRequest addSubscriptionRequest);

    //Get All Countries
    @Headers({"Content-Type: application/json"})
    @GET("country/getAllCountries")
    Call<JsonObject> getAllCountries(@Header("X-API-KEY") String key);

    //Get All Countries
    @Headers({"Content-Type: application/json"})
    @GET("country/getAllCountry")
    Call<JsonObject> getAllCountry(@Header("X-API-KEY") String key);



   

    //Get States by id
    @Headers({"Content-Type: application/json"})
    @POST("playtrack/getAllStateByCountryId")
    Call<JsonObject> getAllStateByCountryId(@Header("X-API-KEY") String key, @Field("id") String id);

    //Get Cities by id
    @Headers({"Content-Type: application/json"})
    @POST("country/getAllCityByStateId")
    Call<JsonObject> getAllCityByStateId(@Header("X-API-KEY") String key, @Body OshoCenterDAO requestSearchMedia);


      //    get all search item Songs
    @Headers({"Content-Type: application/json"})
    @POST("country/getAllStateByCountryId")
    Call<JsonObject> getAllStateByCountryId(@Header("X-API-KEY") String key, @Body OshoCenterDAO requestSearchMedia);

    //    get all search item Songs
    @Headers({"Content-Type: application/json"})
    @POST("country/getAllAshramByStateId")
    Call<JsonObject> getAllAshramByStateId(@Header("X-API-KEY") String key, @Body OshoCenterDAO requestSearchMedia);

    ///event/alluserbyeventid


    // Get All Events by event ID
    @Headers({"Content-Type: application/json"})
    @GET("event/alluserbyeventid/{id}")
    Call<JsonObject> getAllEventsByEventID(@Header("X-API-KEY") String key, @Path("id") String id);

}
