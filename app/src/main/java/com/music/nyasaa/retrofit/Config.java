package com.music.nyasaa.retrofit;

import android.support.v4.util.LruCache;

import com.google.gson.GsonBuilder;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Anand on 03/10/17.
 */

public class Config {

    public static String ImageBseUrL = "http://daduf.cloudapp.net:8090/Web_Source/Media/";
    public static String ImageUploadURL = "http://daduf.cloudapp.net:8090/";
////    public static String BASE_URL_AV="http://52.8.218.210/av_application/endpoint/";
//
////
//o https://ask-osho.net/https://s3.ap-south-1.amazonaws.com/nyasaa.org

    //
    public static String BASE_URL_AV = "https://api.nyasaa.org/";
    //    public static String BASE_URL_AV = "http://api.nyasaa.org/projects/nyasaa-api/";
    public static String BASE_URL_FOR_DOWNLOAD = "https://s3.ap-south-1.amazonaws.com/nyasaa.org/";
    public static String BASE_URL_FOR_PLAY = "";

    public static String BASE_VIDEO_URL = "https://avapplication.s3.amazonaws.com/videos/video/";
    public static String BASE_VIDEO_URL_COVER = "https://avapplication.s3.amazonaws.com/videos/cover/";
//    public static String PIC_URL = "http://api.nyasaa.org/projects/nyasaa-api/uploads/tracks/";
    public static String PIC_URL = "https://s3.ap-south-1.amazonaws.com/nyasaa.org/cover/";
    public static String PIC_URL_PACKAGE = "http://api.nyasaa.org/projects/nyasaa-api/uploads/package";


    public static String PIC_URL_EVENT = "https://avapplication.s3.amazonaws.com/event/";
    public static String PIC_URL_COMMUNITY = "https:/jaintowels/avapplication.s3.amazonaws.com/community/images/";
    public static String PIC_URL_silder = "https://s3.ap-south-1.amazonaws.com/nyasaa.org/slider/";
    public static String BASE_PROFILE_PIC = "https://avapplication.s3.amazonaws.com/userprofile/";
    public static String BASE_CAT_PIC = "https://ask-osho.net/uploads/media_images/";
    public static String BASE_TUT_IMGE = "http://ask-osho.net/uploads/tutorials_images/";


    //
//    public static String BASE_URL_AV = "http://35.154.125.187/endpoint/";
//    public static String BASE_URL_FOR_DOWNLOAD = "http://avapplicationstaging.s3.amazonaws.com/";
//    public static String BASE_URL_FOR_PLAY = "http://avapplicationstaging.s3.amazonaws.com/audios/track/";
//    public static String BASE_URL_FOR_PLAY_MUSIC = "http://avapplicationstaging.s3.amazonaws.com/audios/track/";
//    public static String BASE_URL_FOR_PLAY_MEDITITATION = "http://avapplicationstaging.s3.amazonaws.com/audios/track/";
//    public static String BASE_URL_FOR_PLAY_INTERVIEW = "http://avapplicationstaging.s3.amazonaws.com/audios/track/";
//    public static String BASE_VIDEO_URL = "http://avapplicationstaging.s3.amazonaws.com/videos/video/";
//    public static String BASE_VIDEO_URL_COVER = "http://avapplicationstaging.s3.amazonaws.com/videos/cover/";
//    public static String PIC_URL = "http://avapplicationstaging.s3.amazonaws.com/audios/";
//    public static String PIC_URL_EVENT = "http://avapplicationstaging.s3.amazonaws.com/event/";
//    public static String PIC_URL_COMMUNITY = "http://avapplicationstaging.s3.amazonaws.com/community/images/";
//    public static String PIC_URL_silder = "http://avapplicationstaging.s3.amazonaws.com/slider/";
//    public static String BASE_PROFILE_PIC = "http://avapplicationstaging.s3.amazonaws.com/userprofile/";
//    public static String BASE_CAT_PIC = "http://35.154.125.187/uploads/media_images/";
//    public static String BASE_TUT_IMGE = "http://35.154.125.187/uploads/tutorials_images/";


    // http://olavideos.s3.amazonaws.com/music/DMND%20-%20Navv%20Inder(MyMp3Song).mp3

    public static String X_API_KEY = "nyasaa-api";
    public static String clientService = "nyasaa-app";


    //    publc static final String YOUTUBE_API_KEY = "AIzaSyCfotktLI7AQLfc0Ud3KX6iNSqrPc0zunM";//youtube API Key
    private IRetrofitAPIMethods apiService;
    private LruCache<Class<?>, Observable<?>> apiObservables;

    public Config() {
        OkHttpClient okHttpClient = buildClient();
        apiObservables = new LruCache<>(10);


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL_AV)
                .client(okHttpClient)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder()
                        .excludeFieldsWithoutExposeAnnotation().create())).build();

        apiService = retrofit.create(IRetrofitAPIMethods.class);


    }

    public IRetrofitAPIMethods getDukanwaleApiService() {
        return apiService;
    }

    /**
     * Method to build and return an OkHttpClient so we can set/get
     * headers quickly and efficiently.
     *
     * @return
     */
    private OkHttpClient buildClient() {

        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                // Do anything with response here
                //if we ant to grab a specific cookie or something..
                return chain.proceed(chain.request());
            }
        });

        builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                //this is where we will add whatever we want to our request headers.
                Request request = chain.request().newBuilder().addHeader("Content-Type", "application/json").build();
                //Log.w("Retrofit@Response", request.body().toString());
                return chain.proceed(request);
            }
        });

        return builder.build();
    }


    /**
     * Method to either return a cached observable or prepare a new one.
     *
     * @param unPreparedObservable
     * @param clazz
     * @param cacheObservable
     * @param useCache
     * @return Observable ready to be subscribed to
     */
    public Observable<?> getPreparedObservable(Observable<?> unPreparedObservable, Class<?> clazz, boolean cacheObservable, boolean useCache) {

        Observable<?> preparedObservable = null;
        if (useCache)//this way we don't reset anything in the cache if this is the only instance of us not wanting to use it.
            preparedObservable = apiObservables.get(clazz);
        if (preparedObservable != null)
            return preparedObservable;
        //we are here because we have never created this observable before or we didn't want to use the cache...
        preparedObservable = unPreparedObservable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
        if (cacheObservable) {
            preparedObservable = preparedObservable.cache();
            apiObservables.put(clazz, preparedObservable);
        }

        return preparedObservable;
    }

    /**
     * Method to clear the entire cache of observables
     */
    public void clearCache() {
        apiObservables.evictAll();
    }
}
