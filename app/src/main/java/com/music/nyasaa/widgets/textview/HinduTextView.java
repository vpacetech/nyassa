package com.music.nyasaa.widgets.textview;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

public class HinduTextView extends android.support.v7.widget.AppCompatTextView {

    public HinduTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public HinduTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public HinduTextView(Context context) {
        super(context);
        init();
    }

    public void init() {

        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "k010.TTF");

        setTypeface(tf ,1);

    }

}
