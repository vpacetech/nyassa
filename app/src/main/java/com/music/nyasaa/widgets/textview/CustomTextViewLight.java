package com.music.nyasaa.widgets.textview;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

/**
 * Created by monica on 12/15/2015.
 */
public class CustomTextViewLight extends android.support.v7.widget.AppCompatTextView {

    public CustomTextViewLight(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomTextViewLight(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomTextViewLight(Context context) {
        super(context);
        init();
    }

    public void init() {
//
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "Quicksand-Light.otf");;

        setTypeface(tf ,1);

    }

}
