package com.music.nyasaa.widgets.edittext;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by monica on 12/15/2015.
 */
public class CustomEdittextRegular extends EditText {


    public CustomEdittextRegular(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomEdittextRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomEdittextRegular(Context context) {
        super(context);
        init();
    }

    public void init() {
        if (!isInEditMode()) {

            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "Quicksand-Light.otf");
            setTypeface(tf);
        }

    }
}
