package com.music.nyasaa.models.RequestModel.AddToCommunity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RequestToAddMember {

    @SerializedName("parent_user_id")
    @Expose
    public String parentUserId;
    @SerializedName("community_id")
    @Expose
    public String communityId;
    @SerializedName("user_id")
    @Expose
    public String userId;




    @SerializedName("request")
    @Expose
    public String request;


    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getParentUserId() {
        return parentUserId;
    }

    public void setParentUserId(String parentUserId) {
        this.parentUserId = parentUserId;
    }

    public String getCommunityId() {
        return communityId;
    }

    public void setCommunityId(String communityId) {
        this.communityId = communityId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
