package com.music.nyasaa.models.ResponseModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetAlbumList {

    @SerializedName("respon")
    @Expose
    public List<GetAlbumsListData> respon = null;

    public List<GetAlbumsListData> getRespon() {
        return respon;
    }

    public void setRespon(List<GetAlbumsListData> respon) {
        this.respon = respon;
    }
}
