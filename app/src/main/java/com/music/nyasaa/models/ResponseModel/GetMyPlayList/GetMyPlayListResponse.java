package com.music.nyasaa.models.ResponseModel.GetMyPlayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetMyPlayListResponse {

    @SerializedName("respon")
    @Expose
    public List<GetMyPlayListResponseData> respon = null;
    @SerializedName("error")
    @Expose
    public int error;

    public List<GetMyPlayListResponseData> getRespon() {
        return respon;
    }

    public void setRespon(List<GetMyPlayListResponseData> respon) {
        this.respon = respon;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }
}
