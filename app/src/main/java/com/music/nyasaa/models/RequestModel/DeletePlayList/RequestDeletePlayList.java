package com.music.nyasaa.models.RequestModel.DeletePlayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RequestDeletePlayList {

    @SerializedName("X-API-KEY")
    @Expose
    public String xAPIKEY;
    @SerializedName("id")
    @Expose
    public String id;

    @SerializedName("playlist_id")
    @Expose
    public String playlist_id;

    public String getPlaylist_id() {
        return playlist_id;
    }

    public void setPlaylist_id(String playlist_id) {
        this.playlist_id = playlist_id;
    }

    public String getxAPIKEY() {
        return xAPIKEY;
    }

    public void setxAPIKEY(String xAPIKEY) {
        this.xAPIKEY = xAPIKEY;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
