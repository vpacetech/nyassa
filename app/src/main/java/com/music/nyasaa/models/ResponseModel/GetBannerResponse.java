package com.music.nyasaa.models.ResponseModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetBannerResponse {

    @SerializedName("status")
    @Expose
    public Boolean status;
    @SerializedName("resultObject")
    @Expose
    public List<GetBannerResponseData> resultObject = null;
    @SerializedName("message")
    @Expose
    public String message;


    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<GetBannerResponseData> getResultObject() {
        return resultObject;
    }

    public void setResultObject(List<GetBannerResponseData> resultObject) {
        this.resultObject = resultObject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
