package com.music.nyasaa.models.ResponseModel.GetImageUrl;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetImageResponse {

    @SerializedName("upload_data")
    @Expose
    public GetImageResponseData uploadData;
    @SerializedName("image")
    @Expose
    public String image;

    public GetImageResponseData getUploadData() {
        return uploadData;
    }

    public void setUploadData(GetImageResponseData uploadData) {
        this.uploadData = uploadData;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
