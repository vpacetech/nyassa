package com.music.nyasaa.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Silder_DAO {

//    	"id": "2",
//                "name": "Test1",
//                "file": "aa.jpg ",
//                "createdAt": "2017-11-21 00:00:00",
//                "updatedAt": "2018-07-05 19:56:43",
//                "status": "1"

    @SerializedName("respon")
    @Expose
    public List<Silder_DAO> resultObject = null;

    public List<Silder_DAO> getResultObject() {
        return resultObject;
    }

    public void setResultObject(List<Silder_DAO> resultObject) {
        this.resultObject = resultObject;
    }





    @SerializedName("title")
    @Expose
    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    @SerializedName("image")
    @Expose
    String file;


}
