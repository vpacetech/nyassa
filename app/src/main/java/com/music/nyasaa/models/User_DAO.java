package com.music.nyasaa.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User_DAO {

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAbout_me() {
        return about_me;
    }

    public void setAbout_me(String about_me) {
        this.about_me = about_me;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getSanyas_name() {
        return sanyas_name;
    }

    public void setSanyas_name(String sanyas_name) {
        this.sanyas_name = sanyas_name;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getProfile_photo() {
        return profile_photo;
    }

    public void setProfile_photo(String profile_photo) {
        this.profile_photo = profile_photo;
    }

    public String getIs_banned() {
        return is_banned;
    }

    public void setIs_banned(String is_banned) {
        this.is_banned = is_banned;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAdded() {
        return added;
    }

    public void setAdded(String added) {
        this.added = added;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }


    public String getX_API_KEY() {
        return X_API_KEY;
    }

    public void setX_API_KEY(String x_API_KEY) {
        X_API_KEY = x_API_KEY;
    }


    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    @SerializedName("X-API-KEY")
    @Expose
    String X_API_KEY;

    @SerializedName("id")
    @Expose
    String id;

    @SerializedName("city")
    @Expose
    String city;


    @SerializedName("sanyas_name")
    @Expose
    String sanyas_name;

    @SerializedName("phone_number")
    @Expose
    String phone_number;

    @SerializedName("username")
    @Expose
    String username;

    @SerializedName("password")
    @Expose
    String password;

    @SerializedName("email")
    @Expose
    String email;

    @SerializedName("about_me")
    @Expose
    String about_me;

    @SerializedName("profile_photo")
    @Expose
    String profile_photo;

    @SerializedName("is_banned")
    @Expose
    String is_banned;

    @SerializedName("status")
    @Expose
    String status;

    @SerializedName("added")
    @Expose
    String added;

    @SerializedName("updated")
    @Expose
    String updated;

    @SerializedName("country_id")
    @Expose
    public String countryId;
    @SerializedName("state_id")
    @Expose
    public String stateId;

    @SerializedName("mobile")
    @Expose
    public String mobile;
    @SerializedName("first_name")
    @Expose
    public String firstName;
    @SerializedName("last_name")
    @Expose
    public String lastName;


    @SerializedName("user_id")
    @Expose
    public String user_id;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
