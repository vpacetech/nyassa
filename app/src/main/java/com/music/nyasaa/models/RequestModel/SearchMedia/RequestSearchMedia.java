package com.music.nyasaa.models.RequestModel.SearchMedia;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RequestSearchMedia {

    @SerializedName("user_id")
    @Expose
    public String user_id;

    @SerializedName("searchterm")
    @Expose
    public String searchterm;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    @SerializedName("limit")
    @Expose
    public String limit;
    @SerializedName("offset")
    @Expose
    public String offset;


    @SerializedName("type")
    @Expose
    public int type;


    @SerializedName("lang_id")
    @Expose
    public String album_id;



    @SerializedName("album_id")
    @Expose
    public String album_id_new;


    public String getAlbum_id_new() {
        return album_id_new;
    }

    public void setAlbum_id_new(String album_id_new) {
        this.album_id_new = album_id_new;
    }

    @SerializedName("should_orderby_name")
    @Expose
    public boolean should_orderby_name;




    @SerializedName("media_type_id")
    @Expose
    public String media_type_id;

    @SerializedName("quedisc")
    @Expose
    public int quedisc;











    @SerializedName("category")
    @Expose
    public String category;
    public String getSearchterm() {
        return searchterm;
    }

    public void setSearchterm(String searchterm) {
        this.searchterm = searchterm;
    }

    public String getLimit() {
        return limit;
    }

    public void setLimit(String limit) {
        this.limit = limit;
    }

    public String getAlbum_id() {
        return album_id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public boolean isShould_orderby_name() {
        return should_orderby_name;
    }

    public void setAlbum_id(String album_id) {
        this.album_id = album_id;
    }

    public boolean getShould_orderby_name() {
        return should_orderby_name;
    }

    public void setShould_orderby_name(boolean should_orderby_name) {
        this.should_orderby_name = should_orderby_name;
    }

    public String getMedia_type_id() {

        return media_type_id;
    }

    public void setMedia_type_id(String media_type_id) {
        this.media_type_id = media_type_id;
    }

    public int getQuedisc() {
        return quedisc;
    }

    public void setQuedisc(int quedisc) {
        this.quedisc = quedisc;
    }

    public String getOffset() {
        return offset;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setOffset(String offset) {
        this.offset = offset;
    }
}
