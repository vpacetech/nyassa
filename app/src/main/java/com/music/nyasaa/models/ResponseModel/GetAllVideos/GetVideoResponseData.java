package com.music.nyasaa.models.ResponseModel.GetAllVideos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetVideoResponseData {


    public Boolean getPlayStop() {
        return PlayStop;
    }

    public void setPlayStop(Boolean playStop) {
        PlayStop = playStop;
    }

    public Boolean PlayStop=false;

    @SerializedName("quedisc")
    @Expose
    public String quedisc;


    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("file")
    @Expose
    public String file;

    @SerializedName("file_name")
    @Expose
    public String file_name;
    @SerializedName("category")
    @Expose
    public String category;
    @SerializedName("cover")
    @Expose
    public String cover;
    @SerializedName("style")
    @Expose
    public String style;
    @SerializedName("album")
    @Expose
    public String album;
    @SerializedName("artist")
    @Expose
    public String artist;
    @SerializedName("plays")
    @Expose
    public String plays;
    @SerializedName("added")
    @Expose
    public String added;
    @SerializedName("isPublish")
    @Expose
    public String isPublish;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCover() {
        return cover;
    }

    public String getFile_name() {
        return file_name;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getQuedisc() {
        return quedisc;
    }

    public void setQuedisc(String quedisc) {
        this.quedisc = quedisc;
    }

    public String getPlays() {
        return plays;
    }

    public void setPlays(String plays) {
        this.plays = plays;
    }

    public String getAdded() {
        return added;
    }

    public void setAdded(String added) {
        this.added = added;
    }

    public String getIsPublish() {
        return isPublish;
    }

    public void setIsPublish(String isPublish) {
        this.isPublish = isPublish;
    }
}
