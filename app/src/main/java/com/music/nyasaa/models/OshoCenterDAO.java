package com.music.nyasaa.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OshoCenterDAO {



    @SerializedName("phonecode")
    @Expose
    private String phonecode;

    @SerializedName("sortname")
    @Expose
    private String sortname;

    public String getPhonecode() {
        return phonecode;
    }

    public void setPhonecode(String phonecode) {
        this.phonecode = phonecode;
    }

    public String getSortname() {
        return sortname;
    }

    public void setSortname(String sortname) {
        this.sortname = sortname;
    }

    @SerializedName("country_id")
    @Expose
    private String country_id;

    public String getCountry_id() {
        return country_id;
    }

    public void setCountry_id(String country_id) {
        this.country_id = country_id;
    }

    @SerializedName("id")
@Expose
private String id;
@SerializedName("name")
@Expose
private String name;
@SerializedName("address")
@Expose
private String address;
@SerializedName("contact_name")
@Expose
private String contactName;
@SerializedName("contact_number")
@Expose
private String contactNumber;
@SerializedName("website")
@Expose
private String website;
@SerializedName("state_id")
@Expose
private String stateId;
@SerializedName("status")
@Expose
private String status;
@SerializedName("createdAt")
@Expose
private String createdAt;
@SerializedName("updatedAt")
@Expose
private String updatedAt;

public String getId() {
return id;
}

public void setId(String id) {
this.id = id;
}

public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

public String getAddress() {
return address;
}

public void setAddress(String address) {
this.address = address;
}

public String getContactName() {
return contactName;
}

public void setContactName(String contactName) {
this.contactName = contactName;
}

public String getContactNumber() {
return contactNumber;
}

public void setContactNumber(String contactNumber) {
this.contactNumber = contactNumber;
}

public String getWebsite() {
return website;
}

public void setWebsite(String website) {
this.website = website;
}

public String getStateId() {
return stateId;
}

public void setStateId(String stateId) {
this.stateId = stateId;
}

public String getStatus() {
return status;
}

public void setStatus(String status) {
this.status = status;
}

public String getCreatedAt() {
return createdAt;
}

public void setCreatedAt(String createdAt) {
this.createdAt = createdAt;
}

public String getUpdatedAt() {
return updatedAt;
}

public void setUpdatedAt(String updatedAt) {
this.updatedAt = updatedAt;
}

    @SerializedName("resultObject")
    @Expose
    List<OshoCenterDAO> getResultObject;
    public List<OshoCenterDAO> getResultObject() {

        return getResultObject;
    }


    public String toString() {
        return name;
    }
}