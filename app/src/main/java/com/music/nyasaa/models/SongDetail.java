package com.music.nyasaa.models;

import android.text.TextUtils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SongDetail {

    public Boolean getPlayStop() {
        return PlayStop;
    }

    public void setPlayStop(Boolean playStop) {
        PlayStop = playStop;
    }

    public Boolean PlayStop=false;
    public int action=0;
    public int album_id=0;
    public String album_name;
    //public String artist;
    public float audioProgress = 0.0f;
    public int audioProgressSec = 0;
    public String display_name="1";
    public String duration="40000";
  //  public int id;
    public String lyrics="1";
    public String path="1";
    public String title="Title";
    public String type="1";
    public String url="1";
    public String videoUrl="1";
    public String cover_image="";

    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    }

    public String getTypeOfSong() {
        return TypeOfSong;
    }

    public void setTypeOfSong(String typeOfSong) {
        TypeOfSong = typeOfSong;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String TypeOfSong;

    @SerializedName("id")
    @Expose
    String id;



    @SerializedName("createdAt")
    @Expose
    String createdAt;

    @SerializedName("track_url")
    @Expose
    String track_url;
    @SerializedName("user_id")
    @Expose
    public String userId;

    public String getTrack_url() {
        return track_url;
    }

    public void setTrack_url(String track_url) {
        this.track_url = track_url;
    }

    @SerializedName("username")
    @Expose
    public String username;

    @SerializedName("track_name")
    @Expose
    public String track_name;



    @SerializedName("community_id")
    @Expose
    public String communityId;

    @SerializedName("media_type_id")
    @Expose
    String media_type_id;

    @SerializedName("track_type")
    @Expose
    public String trackType;

    @SerializedName("name")
    @Expose
    String name;

    @SerializedName("file")
    @Expose
    String file;

    @SerializedName("file_name")
    @Expose
    String file_name;

    public String getFile_name() {
        return file_name;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    @SerializedName("cover")
    @Expose
    String cover;

    @SerializedName("style")
    @Expose
    String style;


    @SerializedName("from_date")
    @Expose
    String from_date;


    @SerializedName("to_date")
    @Expose
    String to_date;

    @SerializedName("album")
    @Expose
    String album;

    @SerializedName("artist")
    @Expose
    public String artist;

    @SerializedName("plays")
    @Expose
    public String plays;

    @SerializedName("added")
    @Expose
    public String added;

    @SerializedName("isPublish")
    @Expose
    public String isPublish;

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    @SerializedName("quedisc")
    @Expose
    public String quedisc;

    public float getAudioProgress() {
        return audioProgress;
    }

    public String getTrack_name() {
        return track_name;
    }

    public void setTrack_name(String track_name) {
        this.track_name = track_name;
    }

    public void setAudioProgress(float audioProgress) {
        this.audioProgress = audioProgress;
    }

    public int getAudioProgressSec() {
        return audioProgressSec;
    }

    public void setAudioProgressSec(int audioProgressSec) {
        this.audioProgressSec = audioProgressSec;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCommunityId() {
        return communityId;
    }

    public void setCommunityId(String communityId) {
        this.communityId = communityId;
    }

    public String getTrackType() {
        return trackType;
    }

    public void setTrackType(String trackType) {
        this.trackType = trackType;
    }

    public String getMedia_type_id() {
        return media_type_id;
    }

    public void setMedia_type_id(String media_type_id) {
        this.media_type_id = media_type_id;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getPlays() {
        return plays;
    }

    public void setPlays(String plays) {
        this.plays = plays;
    }

    public String getAdded() {
        return added;
    }

    public String getQuedisc() {
        return quedisc;
    }

    public void setQuedisc(String quedisc) {
        this.quedisc = quedisc;
    }

    public void setAdded(String added) {
        this.added = added;
    }

    public String getIsPublish() {
        return isPublish;
    }

    public void setIsPublish(String isPublish) {
        this.isPublish = isPublish;
    }

    public SongDetail() {
    }

    public SongDetail(int _id, int aLBUM_ID, String _artist, String _title, String _path, String _display_name, String _duration) {
        this.id = String.valueOf(_id);
        this.album_id = aLBUM_ID;
        this.artist = _artist;
        this.title = _title;
        this.path = _path;
        this.display_name = _display_name;
        this.duration = TextUtils.isEmpty(_duration) ? "0" : String.valueOf(Long.valueOf(_duration).longValue() / 1000);
    }

    public int getId() {
        return Integer.parseInt(this.id);
    }

    public String getIds() {
        return this.id;
    }


    public void setId(int id) {
        this.id = String.valueOf(id);
    }

    public int getAlbum_id() {
        return this.album_id;
    }

    public void setAlbum_id(int album_id) {
        this.album_id = album_id;
    }

    public String getArtist() {
        return this.artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDisplay_name() {
        return this.display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public String getDuration() {
        return this.duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getPath() {
        return this.path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getUrl() {
        return this.url;
    }

    public String getFrom_date() {
        return from_date;
    }

    public void setFrom_date(String from_date) {
        this.from_date = from_date;
    }

    public String getTo_date() {
        return to_date;
    }

    public void setTo_date(String to_date) {
        this.to_date = to_date;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLyrics() {
        return this.lyrics;
    }

    public void setLyrics(String lyrics) {
        this.lyrics = lyrics;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }


    public String getAlbum_name() {
        return album_name;
    }

    public void setAlbum_name(String album_name) {
        this.album_name = album_name;
    }

    public String getCover_image() {
        return cover_image;
    }

    public void setCover_image(String cover_image) {
        this.cover_image = cover_image;
    }

}
