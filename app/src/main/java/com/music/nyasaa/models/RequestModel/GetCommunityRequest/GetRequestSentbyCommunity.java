package com.music.nyasaa.models.RequestModel.GetCommunityRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetRequestSentbyCommunity {

    @SerializedName("status")
    @Expose
    public Boolean status;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("resultObject")
    @Expose
    public List<GetRequestSentbyCommunityData> resultObject = null;


    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<GetRequestSentbyCommunityData> getResultObject() {
        return resultObject;
    }

    public void setResultObject(List<GetRequestSentbyCommunityData> resultObject) {
        this.resultObject = resultObject;
    }
}
