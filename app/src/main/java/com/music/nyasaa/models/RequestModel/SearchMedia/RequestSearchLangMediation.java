package com.music.nyasaa.models.RequestModel.SearchMedia;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RequestSearchLangMediation {




    @SerializedName("limit")
    @Expose
    public String limit;
    @SerializedName("offset")
    @Expose
    public String offset;




    @SerializedName("album_id")
    @Expose
    public int album_id;


    public void setAlbum_id(int album_id) {
        this.album_id = album_id;
    }

    public String getLimit() {
        return limit;
    }

    public void setLimit(String limit) {
        this.limit = limit;
    }

    public int getAlbum_id() {
        return album_id;
    }




    public String getOffset() {
        return offset;
    }


    public void setOffset(String offset) {
        this.offset = offset;
    }
}
