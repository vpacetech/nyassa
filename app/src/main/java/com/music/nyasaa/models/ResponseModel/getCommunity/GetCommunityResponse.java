package com.music.nyasaa.models.ResponseModel.getCommunity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetCommunityResponse {

    @SerializedName("status")
    @Expose
    public boolean status;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("resultObject")
    @Expose
    public List<GetCommunityResponseData> resultObject = null;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<GetCommunityResponseData> getResultObject() {
        return resultObject;
    }

    public void setResultObject(List<GetCommunityResponseData> resultObject) {
        this.resultObject = resultObject;
    }
}



