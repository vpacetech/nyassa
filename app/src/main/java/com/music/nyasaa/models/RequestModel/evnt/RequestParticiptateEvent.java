package com.music.nyasaa.models.RequestModel.evnt;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RequestParticiptateEvent {


    @SerializedName("user_id")
    @Expose
    public String userId;
    @SerializedName("event_id")
    @Expose
    public String eventId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }
}
