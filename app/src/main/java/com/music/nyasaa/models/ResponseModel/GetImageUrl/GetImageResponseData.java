package com.music.nyasaa.models.ResponseModel.GetImageUrl;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetImageResponseData {

    @SerializedName("file_name")
    @Expose
    public String fileName;
    @SerializedName("file_type")
    @Expose
    public String fileType;
    @SerializedName("file_path")
    @Expose
    public String filePath;
    @SerializedName("full_path")
    @Expose
    public String fullPath;
    @SerializedName("raw_name")
    @Expose
    public String rawName;
    @SerializedName("orig_name")
    @Expose
    public String origName;
    @SerializedName("client_name")
    @Expose
    public String clientName;
    @SerializedName("file_ext")
    @Expose
    public String fileExt;
    @SerializedName("file_size")
    @Expose
    public Float fileSize;
    @SerializedName("is_image")
    @Expose
    public Boolean isImage;
    @SerializedName("image_width")
    @Expose
    public Object imageWidth;
    @SerializedName("image_height")
    @Expose
    public Object imageHeight;
    @SerializedName("image_type")
    @Expose
    public String imageType;
    @SerializedName("image_size_str")
    @Expose
    public String imageSizeStr;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFullPath() {
        return fullPath;
    }

    public void setFullPath(String fullPath) {
        this.fullPath = fullPath;
    }

    public String getRawName() {
        return rawName;
    }

    public void setRawName(String rawName) {
        this.rawName = rawName;
    }

    public String getOrigName() {
        return origName;
    }

    public void setOrigName(String origName) {
        this.origName = origName;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getFileExt() {
        return fileExt;
    }

    public void setFileExt(String fileExt) {
        this.fileExt = fileExt;
    }

    public Float getFileSize() {
        return fileSize;
    }

    public void setFileSize(Float fileSize) {
        this.fileSize = fileSize;
    }

    public Boolean getImage() {
        return isImage;
    }

    public void setImage(Boolean image) {
        isImage = image;
    }

    public Object getImageWidth() {
        return imageWidth;
    }

    public void setImageWidth(Object imageWidth) {
        this.imageWidth = imageWidth;
    }

    public Object getImageHeight() {
        return imageHeight;
    }

    public void setImageHeight(Object imageHeight) {
        this.imageHeight = imageHeight;
    }

    public String getImageType() {
        return imageType;
    }

    public void setImageType(String imageType) {
        this.imageType = imageType;
    }

    public String getImageSizeStr() {
        return imageSizeStr;
    }

    public void setImageSizeStr(String imageSizeStr) {
        this.imageSizeStr = imageSizeStr;
    }
}
