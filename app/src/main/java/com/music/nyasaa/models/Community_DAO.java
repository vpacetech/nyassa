package com.music.nyasaa.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Community_DAO {
    @SerializedName("parent_user_id")
    @Expose
    String parent_user_id;

    @SerializedName("id")
    @Expose
    String id;



    @SerializedName("type")
    @Expose
    String type;

    @SerializedName("name")
    @Expose
    String name;

    @SerializedName("short_info")
    @Expose
    String short_info;

    @SerializedName("long_info")
    @Expose
    String long_info;

    @SerializedName("image_url")
    @Expose
    String image_url;

    public String getParent_user_id() {
        return parent_user_id;
    }

    public void setParent_user_id(String parent_user_id) {
        this.parent_user_id = parent_user_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShort_info() {
        return short_info;
    }

    public void setShort_info(String short_info) {
        this.short_info = short_info;
    }

    public String getLong_info() {
        return long_info;
    }

    public void setLong_info(String long_info) {
        this.long_info = long_info;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }
}
