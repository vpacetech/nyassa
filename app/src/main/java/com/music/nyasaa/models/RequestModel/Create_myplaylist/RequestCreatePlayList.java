package com.music.nyasaa.models.RequestModel.Create_myplaylist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RequestCreatePlayList {

    @SerializedName("user_id")
    @Expose
    public int userId;

    @SerializedName("playlist_name")
    @Expose
    public String playlist_name;


    public String getPlaylist_name() {
        return playlist_name;
    }

    public void setPlaylist_name(String playlist_name) {
        this.playlist_name = playlist_name;
    }

    @SerializedName("name")
    @Expose
    public String name;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
