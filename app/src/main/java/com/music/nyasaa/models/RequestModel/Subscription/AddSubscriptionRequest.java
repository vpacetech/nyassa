package com.music.nyasaa.models.RequestModel.Subscription;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddSubscriptionRequest {

    @SerializedName("userId")
    @Expose
    public String userId;
    @SerializedName("tokenId")
    @Expose
    public String tokenId;
    @SerializedName("currencyName")
    @Expose
    public String currencyName;
    @SerializedName("status")
    @Expose
    public String status;


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTokenId() {
        return tokenId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
