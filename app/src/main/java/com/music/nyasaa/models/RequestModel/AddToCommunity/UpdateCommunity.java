package com.music.nyasaa.models.RequestModel.AddToCommunity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateCommunity {

    @SerializedName("parent_user_id")
    @Expose
    public int parentUserId;
    @SerializedName("community_id")
    @Expose
    public int communityId;
    @SerializedName("type")
    @Expose
    public Integer type;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("short_info")
    @Expose
    public String shortInfo;
    @SerializedName("long_info")
    @Expose
    public String longInfo;
    @SerializedName("image_url")
    @Expose
    public String imageUrl;

    public int getParentUserId() {
        return parentUserId;
    }

    public void setParentUserId(int parentUserId) {
        this.parentUserId = parentUserId;
    }

    public int getCommunityId() {
        return communityId;
    }

    public void setCommunityId(int communityId) {
        this.communityId = communityId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortInfo() {
        return shortInfo;
    }

    public void setShortInfo(String shortInfo) {
        this.shortInfo = shortInfo;
    }

    public String getLongInfo() {
        return longInfo;
    }

    public void setLongInfo(String longInfo) {
        this.longInfo = longInfo;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
