package com.music.nyasaa.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Member_of_Community_DAO
{
    @SerializedName("id")
    @Expose
    String id;

    @SerializedName("parent_user_id")
    @Expose
    String parent_user_id;

    @SerializedName("community_id")
    @Expose
    String community_id;

    @SerializedName("user_id")
    @Expose
    String user_id;

    @SerializedName("createdAt")
    @Expose
    String createdAt;

    @SerializedName("updatedAt")
    @Expose
    String updatedAt;





}
