package com.music.nyasaa.models.ResponseModel.GetPlayListSongs;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.music.nyasaa.models.ResponseModel.GetTracks;

import java.util.List;

public class GetPlayListSongsList {
    @SerializedName("respon")
    @Expose
    public List<GetTracks> respon = null;

    @SerializedName("error")
    @Expose
    public int error;

    public List<GetTracks> getRespon() {
        return respon;
    }

    public void setRespon(List<GetTracks> respon) {
        this.respon = respon;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }
}
