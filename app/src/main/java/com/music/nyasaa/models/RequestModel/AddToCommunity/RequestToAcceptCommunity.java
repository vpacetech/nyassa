package com.music.nyasaa.models.RequestModel.AddToCommunity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RequestToAcceptCommunity {

    @SerializedName("community_id")
    @Expose
    public String communityId;
    @SerializedName("user_id")
    @Expose
    public String userId;
    @SerializedName("action")
    @Expose
    public String action;


    public String getCommunityId() {
        return communityId;
    }

    public void setCommunityId(String communityId) {
        this.communityId = communityId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
