package com.music.nyasaa.models.ResponseModel.GetEventPeople;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetEventPeopleResponse {

    @SerializedName("status")
    @Expose
    public boolean status;
    @SerializedName("resultObject")
    @Expose
    public List<GetEventPeopleResponseData> resultObject = null;
    @SerializedName("message")
    @Expose
    public String message;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<GetEventPeopleResponseData> getResultObject() {
        return resultObject;
    }

    public void setResultObject(List<GetEventPeopleResponseData> resultObject) {
        this.resultObject = resultObject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
