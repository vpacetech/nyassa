package com.music.nyasaa.models.ResponseModel.getCommunity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetSerachedCommunity {

    @SerializedName("respon")
    @Expose
    public List<GetSearchedCommunityData> respon = null;
    @SerializedName("error")
    @Expose
    public String error;


    public List<GetSearchedCommunityData> getRespon() {
        return respon;
    }

    public void setRespon(List<GetSearchedCommunityData> respon) {
        this.respon = respon;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
