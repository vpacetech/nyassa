package com.music.nyasaa.models.ResponseModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.music.nyasaa.models.SongDetail;

import java.util.List;

public class GetTracks {

    @SerializedName("tracks")
    @Expose
    public List<SongDetail> tracks = null;


    public List<SongDetail> getTracks() {
        return tracks;
    }

    public void setTracks(List<SongDetail> tracks) {
        this.tracks = tracks;
    }
}
