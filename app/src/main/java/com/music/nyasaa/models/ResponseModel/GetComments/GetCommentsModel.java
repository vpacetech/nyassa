package com.music.nyasaa.models.ResponseModel.GetComments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class GetCommentsModel {

    @SerializedName("status")
    @Expose
    public Boolean status;
    @SerializedName("resultObject")
    @Expose
    public ArrayList<GetCommentsModelData> resultObject = null;
    @SerializedName("message")
    @Expose
    public String message;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public ArrayList<GetCommentsModelData> getResultObject() {
        return resultObject;
    }

    public void setResultObject(ArrayList<GetCommentsModelData> resultObject) {
        this.resultObject = resultObject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
