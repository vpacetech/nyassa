package com.music.nyasaa.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetPackageList {


    @SerializedName("package_id")
    @Expose
    public String packageId;
    @SerializedName("title")
    @Expose
    public String title;
    @SerializedName("price")
    @Expose
    public String price;
    @SerializedName("dis_price")
    @Expose
    public String dis_price;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("image_url")
    @Expose
    public String image_url;
    @SerializedName("duration")
    @Expose
    public String duration;
    @SerializedName("albums")
    @Expose
    public List<Album> albums = null;


    public String getPackageId() {
        return packageId;
    }

    public String getDis_price() {
        return dis_price;
    }

    public void setDis_price(String dis_price) {
        this.dis_price = dis_price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public void setPackageId(String packageId) {
        this.packageId = packageId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public List<Album> getAlbums() {
        return albums;
    }

    public void setAlbums(List<Album> albums) {
        this.albums = albums;
    }
}
