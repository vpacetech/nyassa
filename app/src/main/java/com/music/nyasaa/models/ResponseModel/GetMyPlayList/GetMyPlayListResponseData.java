package com.music.nyasaa.models.ResponseModel.GetMyPlayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetMyPlayListResponseData {

    public Boolean getPlayStop() {
        return PlayStop;
    }

    public void setPlayStop(Boolean playStop) {
        PlayStop = playStop;
    }

    public Boolean PlayStop=false;

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("user_id")
    @Expose
    public String userId;
    @SerializedName("playlist_name")
    @Expose
    public String name;
    @SerializedName("added")
    @Expose
    public String added;
    @SerializedName("is_private")
    @Expose
    public String isPrivate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdded() {
        return added;
    }

    public void setAdded(String added) {
        this.added = added;
    }

    public String getIsPrivate() {
        return isPrivate;
    }

    public void setIsPrivate(String isPrivate) {
        this.isPrivate = isPrivate;
    }
}
