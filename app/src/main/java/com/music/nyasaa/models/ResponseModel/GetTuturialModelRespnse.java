package com.music.nyasaa.models.ResponseModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class GetTuturialModelRespnse {

    @SerializedName("respon")
    @Expose
    public ArrayList<GetTuturialModelRespnseData> respon = null;
    @SerializedName("error")
    @Expose
    public int error;


    public ArrayList<GetTuturialModelRespnseData> getRespon() {
        return respon;
    }

    public void setRespon(ArrayList<GetTuturialModelRespnseData> respon) {
        this.respon = respon;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }
}
