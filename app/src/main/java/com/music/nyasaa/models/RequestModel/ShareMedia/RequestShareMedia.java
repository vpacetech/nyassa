package com.music.nyasaa.models.RequestModel.ShareMedia;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RequestShareMedia {


    @SerializedName("user_id")
    @Expose
    public String userId;
    @SerializedName("community_id")
    @Expose
    public String communityId;
    @SerializedName("track_id")
    @Expose
    public String trackId;
    @SerializedName("track_type")
    @Expose
    public String trackType;
}
