package com.music.nyasaa.models.RequestModel.GetMyPlayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RequestGetMyPlayList {

    @SerializedName("X-API-KEY")
    @Expose
    public String xAPIKEY;
    @SerializedName("user_id")
    @Expose
    public String userId;

    public String getPlaylist_id() {
        return playlist_id;
    }

    public void setPlaylist_id(String playlist_id) {
        this.playlist_id = playlist_id;
    }

    @SerializedName("playlist_id")
    @Expose
    public String playlist_id;


    public String getxAPIKEY() {
        return xAPIKEY;
    }

    public void setxAPIKEY(String xAPIKEY) {
        this.xAPIKEY = xAPIKEY;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
