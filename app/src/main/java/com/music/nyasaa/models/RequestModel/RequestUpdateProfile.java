package com.music.nyasaa.models.RequestModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RequestUpdateProfile {


    @SerializedName("X-API-KEY")
    @Expose
    public String xAPIKEY;
    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("username")
    @Expose
    public String username;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("sanyas_name")
    @Expose
    public String sanyasName;
    @SerializedName("phone_number")
    @Expose
    public String phoneNumber;
    @SerializedName("country_id")
    @Expose
    public String countryId;
    @SerializedName("state_id")
    @Expose
    public String stateId;
    @SerializedName("city")
    @Expose
    public String city;
    @SerializedName("profile_photo")
    @Expose
    public String profilePhoto;


    public String getxAPIKEY() {
        return xAPIKEY;
    }

    public void setxAPIKEY(String xAPIKEY) {
        this.xAPIKEY = xAPIKEY;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSanyasName() {
        return sanyasName;
    }

    public void setSanyasName(String sanyasName) {
        this.sanyasName = sanyasName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProfilePhoto() {
        return profilePhoto;
    }

    public void setProfilePhoto(String profilePhoto) {
        this.profilePhoto = profilePhoto;
    }
}
