package com.music.nyasaa.models.RequestModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OTPmodel {

    @SerializedName("user_id")
    @Expose
    public String userId;
    @SerializedName("otp_status")
    @Expose
    public String otp_status;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOtp_status() {
        return otp_status;
    }

    public void setOtp_status(String otp_status) {
        this.otp_status = otp_status;
    }
}
