package com.music.nyasaa.models.ResponseModel.GetEvents;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetEventResponse {
    @SerializedName("status")
    @Expose
    public boolean status;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("userobject")
    @Expose
    public List<GetEventResponseData> resultObject = null;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<GetEventResponseData> getResultObject() {
        return resultObject;
    }

    public void setResultObject(List<GetEventResponseData> resultObject) {
        this.resultObject = resultObject;
    }
}
