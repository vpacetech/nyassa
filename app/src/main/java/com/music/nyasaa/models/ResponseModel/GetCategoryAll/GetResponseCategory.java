package com.music.nyasaa.models.ResponseModel.GetCategoryAll;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetResponseCategory {

    @SerializedName("respon")
    @Expose
    public List<GetResponseCategoryData> respon = null;
    @SerializedName("error")
    @Expose
    public int error;

    public List<GetResponseCategoryData> getRespon() {
        return respon;
    }

    public void setRespon(List<GetResponseCategoryData> respon) {
        this.respon = respon;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }
}
