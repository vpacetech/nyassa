package com.music.nyasaa.models;

import java.io.Serializable;
import java.util.ArrayList;


public class SectionDataModel implements Serializable{
    private String headerTitle;
    private ArrayList<AlbumModel> allItemsInSection;


    public SectionDataModel() {

    }

    public SectionDataModel(String headerTitle, ArrayList<AlbumModel> allItemsInSection) {
        this.headerTitle = headerTitle;
        this.allItemsInSection = allItemsInSection;
    }


    public String getHeaderTitle() {
        return headerTitle;
    }

    public void setHeaderTitle(String headerTitle) {
        this.headerTitle = headerTitle;
    }

    public ArrayList<AlbumModel> getAllItemsInSection() {
        return allItemsInSection;
    }

    public void setAllItemsInSection(ArrayList<AlbumModel> allItemsInSection) {
        this.allItemsInSection = allItemsInSection;
    }
}


