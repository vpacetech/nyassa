package com.music.nyasaa.models.ResponseModel.GetAllVideos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetVideoResponse {

    @SerializedName("respon")
    @Expose
    public List<GetVideoResponseData> respon = null;
    @SerializedName("error")
    @Expose
    public int error;

    public List<GetVideoResponseData> getRespon() {
        return respon;
    }

    public void setRespon(List<GetVideoResponseData> respon) {
        this.respon = respon;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }
}
