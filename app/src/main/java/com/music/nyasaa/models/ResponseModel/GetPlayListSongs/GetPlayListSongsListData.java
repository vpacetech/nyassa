package com.music.nyasaa.models.ResponseModel.GetPlayListSongs;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetPlayListSongsListData {

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("file")
    @Expose
    public String file;
    @SerializedName("category")
    @Expose
    public String category;
    @SerializedName("cover")
    @Expose
    public String cover;
    @SerializedName("style")
    @Expose
    public String style;
    @SerializedName("album")
    @Expose
    public String album;
    @SerializedName("artist")
    @Expose
    public String artist;
    @SerializedName("plays")
    @Expose
    public String plays;
    @SerializedName("added")
    @Expose
    public String added;
    @SerializedName("isPublish")
    @Expose
    public String isPublish;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getPlays() {
        return plays;
    }

    public void setPlays(String plays) {
        this.plays = plays;
    }

    public String getAdded() {
        return added;
    }

    public void setAdded(String added) {
        this.added = added;
    }

    public String getIsPublish() {
        return isPublish;
    }

    public void setIsPublish(String isPublish) {
        this.isPublish = isPublish;
    }
}
