package com.music.nyasaa.models.ResponseModel.GetSeacrchedMembers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetSearchMemberResponse {

    @SerializedName("respon")
    @Expose
    public List<GetSearchMemberResponseData> respon = null;
    @SerializedName("error")
    @Expose
    public int error;

    public List<GetSearchMemberResponseData> getRespon() {
        return respon;
    }

    public void setRespon(List<GetSearchMemberResponseData> respon) {
        this.respon = respon;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }
}
