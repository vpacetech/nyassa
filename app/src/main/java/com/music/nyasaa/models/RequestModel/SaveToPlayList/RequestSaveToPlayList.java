package com.music.nyasaa.models.RequestModel.SaveToPlayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RequestSaveToPlayList {


    @SerializedName("playlist_id")
    @Expose
    public String playlistId;

    @SerializedName("track_type")
    @Expose
    public String track_type;

    public String getTrack_type() {
        return track_type;
    }

    public void setTrack_type(String track_type) {
        this.track_type = track_type;
    }

    @SerializedName("track_id")
    @Expose
    public String trackId;

    public String getPlaylistId() {
        return playlistId;
    }

    public void setPlaylistId(String playlistId) {
        this.playlistId = playlistId;
    }

    public String getTrackId() {
        return trackId;
    }

    public void setTrackId(String trackId) {
        this.trackId = trackId;
    }



}
