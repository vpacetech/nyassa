package com.music.nyasaa.models.RequestModel.SerachUsers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RequestToSearchUser {
    @SerializedName("searchterm")
    @Expose
    public String searchterm;
    @SerializedName("limit")
    @Expose
    public String limit;
    @SerializedName("offset")
    @Expose
    public String offset;


    public String getSearchterm() {
        return searchterm;
    }

    public void setSearchterm(String searchterm) {
        this.searchterm = searchterm;
    }

    public String getLimit() {
        return limit;
    }

    public void setLimit(String limit) {
        this.limit = limit;
    }

    public String getOffset() {
        return offset;
    }

    public void setOffset(String offset) {
        this.offset = offset;
    }
}
