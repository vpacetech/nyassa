package com.music.nyasaa.models.RequestModel.DeleteMember;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RequestDeleteMember {

    @SerializedName("parent_user_id")
    @Expose
    public String parentUserId;
    @SerializedName("community_id")
    @Expose
    public String communityId;
    @SerializedName("user_id")
    @Expose
    public String userId;

    public String getParentUserId() {
        return parentUserId;
    }

    public void setParentUserId(String parentUserId) {
        this.parentUserId = parentUserId;
    }

    public String getCommunityId() {
        return communityId;
    }

    public void setCommunityId(String communityId) {
        this.communityId = communityId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
