package com.music.nyasaa.controller;

import android.content.Context;
import android.os.Environment;

import java.io.File;

/**
 * Created by android on 1/9/2018.
 */

public class SongCacheUtil {
    public static final String SDCARD_ROOT_PATH = Environment.getExternalStorageDirectory().getPath();
    public static final String SDCARD_BASE_PATH = (SDCARD_ROOT_PATH + "/musicDevotional");
    private static SongCacheUtil instance;

    public static SongCacheUtil instance() {
        if (instance == null) {
            instance = new SongCacheUtil();
        }
        return instance;
    }

    public void createFolder(Context context) {

        File f = new File(SDCARD_BASE_PATH);
        if (!f.exists()) {
            f.mkdirs();
        }
    }


    public String getFolderPath() {
        return SDCARD_BASE_PATH;
    }

}
