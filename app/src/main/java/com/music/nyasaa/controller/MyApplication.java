
package com.music.nyasaa.controller;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Handler;
import android.os.Vibrator;

import android.support.multidex.MultiDex;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ConnectionQuality;
import com.androidnetworking.interfaces.ConnectionQualityChangeListener;
import com.google.gson.GsonBuilder;
import com.music.nyasaa.R;
import com.music.nyasaa.dbhandler.HinduMusicDBHelper;
import com.music.nyasaa.dbhandler.MusicSQLiteHelper;
import com.music.nyasaa.retrofit.Config;
import com.music.nyasaa.retrofit.IRetrofitAPIMethods;
import com.music.nyasaa.utility.SharedPreferenceManager;

import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


@ReportsCrashes(
//        formUri = "http://www.backendofyourchoice.com/reportpath",
//        formKey = "", // will not be used
        mailTo = "prathamesh.myana@barquecontech.com", // my emails here
        mode = ReportingInteractionMode.TOAST,
        resToastText = R.string.crash_toast_text

)

public class MyApplication extends Application{
    private static Retrofit __retrofit;
    private static IRetrofitAPIMethods api;
    public static Context applicationContext = null;
    public static volatile Handler applicationHandler = null;
    public static Point displaySize = new Point();
    public static float density = 1;
    public static final String TAG = MyApplication.class
            .getSimpleName();
    public static Vibrator v;

    public static Context context;
    public int SeekTo=0;
    private static MyApplication mInstance;
    private MusicSQLiteHelper dbHelper;
    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
//        MultiDex.install(this);
//        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
//                .detectAll()
//                .penaltyLog()
//                .penaltyDeath()
//                .build());

//       The following line triggers the initialization of ACRA
      //  ACRA.init(this);
        MusicSQLiteHelper.init(getApplicationContext());
        AndroidNetworking.initialize(getApplicationContext());
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPurgeable = true;
        AndroidNetworking.setBitmapDecodeOptions(options);
        AndroidNetworking.enableLogging();
        AndroidNetworking.setConnectionQualityChangeListener(new ConnectionQualityChangeListener() {
            @Override
            public void onChange(ConnectionQuality currentConnectionQuality, int currentBandwidth) {
                Log.d(TAG, "onChange: currentConnectionQuality : " + currentConnectionQuality + " currentBandwidth : " + currentBandwidth);
            }
        });
        dbHelper = new MusicSQLiteHelper(getApplicationContext());
        applicationContext = getApplicationContext();
        applicationHandler = new Handler(applicationContext.getMainLooper());

        /**
         * Data base initialize
         */
        initilizeDB();
        /*
         * Display Density Calculation so that Application not problem with All
		 * resolution.
		 */
        checkDisplaySize();
        density = applicationContext.getResources().getDisplayMetrics().density;


        mInstance = new MyApplication();
        this.context = getApplicationContext();

        SharedPreferenceManager sharedPreferenceManager = new SharedPreferenceManager(context);
        sharedPreferenceManager.initializeInstance(context);


		/*
         * Imageloader initialize
		 */
       // initImageLoader(applicationContext);

    }



    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }


    /**
     * Initialize Image Loader.
     */



    public static int dp(float value) {
        return (int) Math.ceil(density * value);
    }

    public static void checkDisplaySize() {
        try {
            WindowManager manager = (WindowManager) applicationContext.getSystemService(Context.WINDOW_SERVICE);
            if (manager != null) {
                Display display = manager.getDefaultDisplay();
                if (display != null) {
                    display.getSize(displaySize);
                }
            }
        } catch (Exception e) {
        }
    }

    /**
     * Related to Data Base.
     */
    public HinduMusicDBHelper DB_HELPER;

    private void initilizeDB() {
        if (DB_HELPER == null) {
            DB_HELPER = new HinduMusicDBHelper(MyApplication.this);
        }
        try {
            DB_HELPER.getWritableDatabase();
            DB_HELPER.openDataBase();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void closeDB() {
        try {
            DB_HELPER.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static synchronized MyApplication getInstance() {
        return mInstance;
    }


    public Retrofit getRetrofitInstance() {
        if (__retrofit == null)
            __retrofit = new Retrofit.Builder().baseUrl(Config.BASE_URL_AV).addConverterFactory(GsonConverterFactory.create(new GsonBuilder().setLenient().create())).build();

        return __retrofit;
    }

    public IRetrofitAPIMethods getAPIInstance() {
        if (api == null)
            api = getRetrofitInstance().create(IRetrofitAPIMethods.class);
        return api;
    }
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }



}
